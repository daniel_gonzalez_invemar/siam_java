package utilidades;


import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;


public class Cutilidades {
  /**
  Metodo constructor de la clase
  */
  public void Cutilidades(){
  }
  public String mes(int mes1){
    String resp = new String();
    switch (mes1) {
            case 1:{
              resp = "Enero";
              break;
            }
          case 2:{
              resp = "Febrero";
              break;
            }
          case 3:{
              resp = "Marzo";
              break;
            }
          case 4:{
              resp = "Abril";
              break;
            }
          case 5:{
              resp = "Mayo";
              break;
            }
          case 6:{
              resp = "Junio";
              break;
            }
          case 7:{
              resp = "Julio";
              break;
            }
          case 8:{
              resp = "Agosto";
              break;
            }
          case 9:{
              resp = "Septiembre";
              break;
            }
          case 10:{
              resp = "Octubre";
              break;
            }
          case 11:{
              resp = "Noviembre";
              break;
            }
          case 12:{
              resp = "Diciembre";
              break;
            }
    }
    return  resp;
  }
  /**
  * Carga El siguiente valor de la secuencia de imagenes de la <br<
  * base de datos
  */
  public String imagen(Connection conn1)
  {
    String resp = "";
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        stmt = conn1.prepareStatement("SELECT SIMAGENES.NEXTVAL FROM DUAL" );

        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = "";
        else
        {
          resp = rs.getString(1);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();

     }
      catch(SQLException e)
     {
       resp = e.getMessage();
     }
     return resp;
   }
}




