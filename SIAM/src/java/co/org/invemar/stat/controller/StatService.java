package co.org.invemar.stat.controller;

import co.org.invemar.catalogo.model.DAOFactory;

import co.org.invemar.catalogo.model.EstadisticaDAO;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;

import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.http.*;

public class StatService extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private DAOFactory  df;
    private Connection  connection;
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    /**Process the HTTP doGet request.
     */
    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
                      
        String page=request.getParameter("page");
        
        String ip=request.getRemoteAddr();
        
        System.out.println("Estadisticas:.. ip: "+ip+" page: "+page);
        df = new DAOFactory();
        EstadisticaDAO statdao=df.getStatDAO();

        try {
            connection=df.createConnection();
            
            int p=Integer.parseInt(page);
           
            statdao.insertStat(connection,133,ip,p);
            
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().write("Ok");     
            
        } catch (SQLException e) {
             //send an error message to the browser.
             response.sendError( HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
             "There was some problem with the database. Please report it to the webmaster. ");
        }
        
        
       
    }

    /**Process the HTTP doPost request.
     */
    public void doPost(HttpServletRequest request, 
                       HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        doGet(request,response);
    }
}
