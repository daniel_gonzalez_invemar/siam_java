/**
 * 
 */
package co.org.invemar.sipein.model;

import java.util.HashMap;

/**
 * @author Administrador
 *
 */
public class ColorSchema {
	
	public static HashMap colors = new HashMap();
	static{
		colors.put(0, "#1c4a7e");
		colors.put(1, "#bb5b3d");
		colors.put(2, "#3a8133");
		colors.put(3, "#813379");
		colors.put(4, "#99cc00");
		colors.put(5, "#99cc99");  
		colors.put(6, "#cc6633");
		colors.put(7, "#6699ff");
		colors.put(8, "#cccc66");
		colors.put(9, "#99cccc");
		colors.put(10,"#9999cc");
		colors.put(11,"#9999ff");
		colors.put(12,"#cc9999");
		colors.put(13,"#ccccff");
		colors.put(14,"#0066cc");
		colors.put(15,"#6699ff");
		colors.put(16,"#009966");
		colors.put(17,"#99cc00");
		
	}
	
	
}
