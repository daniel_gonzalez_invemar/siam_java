/**
 * 
 */
package co.org.invemar.sipein.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;





/**
 * @author Julian Jose Pizarro Pertuz
 *
 */
public class SIPEINDataManager {
	
	public String loadArtePesca(Connection conn,String column, String table) throws SQLException, JSONException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		JSONObject resp = null;
		JSONArray options = new JSONArray();
		String value=null;
		
		String sql= "select distinct "+column+
					" from "+table+
					" order by 1";
		
		/*String sql="select distinct v."+column+", c.descripcion || ' ' || case when c.descripcion=v."+column+" then '' else v."+column+" end arte "+
				   " from  sipein.codigos_listas c, (select descripcion, "+column+", codigoup "+
				   			" from sipein.codigos_listas ,  "+table+" "+ 
				   			" where tabla=2 and descripcion="+column+" ) v "+
				   			" where v.codigoup=c.codigo and  c.tabla=1 "+
				   			" order by 2,1 ";*/
		
		System.out.println("SQL loadArtePesca: "+sql);
		
		try{
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				resp = new JSONObject();
				value=rs.getString(1);
				resp.put("value", value);
				resp.put("name", value);
				options.put(resp);
			}
			return options.toString();
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
	}
	public String findEspeciesByArte(Connection conn, String arte, String table,String column,String wcolumn)throws SQLException, JSONException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		JSONObject resp = null;
		JSONArray options = new JSONArray();
		String value=null;
		
		String sql= "select "+column +
		            " from "+table+
		            " where "+wcolumn+" = ? order by 1";
		
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, arte);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				resp = new JSONObject();
				value=rs.getString(1);
				resp.put("value", value);
				resp.put("name", value);
				options.put(resp);
			}
			return options.toString();
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
	}
	
	public String findEspeciesByTaxon(Connection conn, String idTaxon)throws SQLException, JSONException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		JSONObject resp = null;
		JSONArray options = new JSONArray();
		String value=null;
		
		String sql= "SELECT nombrecomun "+ 
					" from sipein.vm_capturaporespecie "+
					" where taxon=? "+
					" order by 1";
		
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, idTaxon);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				resp = new JSONObject();
				value=rs.getString(1);
				resp.put("value", value);
				resp.put("name", value);
				options.put(resp);
			}
			return options.toString();
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
	}
	
	public String getDates(Connection conn, String table) throws SQLException, JSONException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		JSONObject resp = null;
		JSONArray options = new JSONArray();
		String value=null;
		
		String sql="select substr(column_name,5) anos FROM all_tab_columns " +
				"WHERE table_name=upper(?) and column_name like 'ANO\\_%' escape '\\' order by 1";
		System.out.println("SQL; getDates() "+sql);
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, table);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				resp = new JSONObject();
				value=rs.getString(1);
				resp.put("value", value);
				resp.put("name", value);
				options.put(resp);
			}
			return options.toString();
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
	}
	public String loadDataForArtesAndSpecie(Connection conn,String arte, String specie,String iperiodo, String fperiodo)
	  throws SQLException, JSONException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		
		int iperiod=Integer.parseInt(iperiodo);
		int fperiod=Integer.parseInt(fperiodo);
		
		String[] col=new String[((fperiod-iperiod)+1)];
		col[0]=iperiodo;
		
		StringBuffer columnsName=new StringBuffer();
		columnsName.append("round(ANO_"+iperiodo+",2) "+"ANO_"+iperiodo);
		
		for(int i=1;i<=(fperiod-iperiod);i++){
			columnsName.append(",round(ANO_"+(iperiod+i)+",2) "+"ANO_"+(iperiod+i));
			col[i]=String.valueOf((iperiod+i));
		}
		
		String sql=/*"select artepesca, "+columnsName.toString()+
					" from sipein.vm_capturaporarteyespecie"+
					" where UPPER(artepesca)=UPPER(?) and UPPER(nomesp)=UPPER(?)"+
					" order by 1"; */
					"SELECT   cl.descripcion phylum, esp.clave, vm.artepesca, vm.nomcien, vm.nomesp, "+columnsName.toString()+
					" FROM sipein.vm_capturaporarteyespecie vm, sipein.especies esp, sipein.codigos_listas cl "+
					" WHERE vm.nomesp = esp.nomesp "+
					"   AND vm.nomcien = esp.nomcien "+
					"   AND cl.codigo = esp.codgru "+
					"   AND cl.tabla = 20 "+
					"   AND UPPER(vm.artepesca)=UPPER(?) and UPPER(vm.nomesp)=UPPER(?) "+
					" ORDER BY 1 ";
		
		System.out.println("SQL:.. "+sql);
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, arte);
			pstmt.setString(2, specie);
			
			rs = pstmt.executeQuery();
			
			JSONObject obj=new JSONObject();
			JSONObject artesobj = new JSONObject();
    		JSONArray artesarr = null;
    		JSONArray aSubarr=null;
    		
    		
			while (rs.next()) {
				artesarr =  new JSONArray();
				aSubarr=new JSONArray();
				for(int j=0;j<col.length;j++){
				   if(rs.getString("ANO_"+col[j])!=null){
					   aSubarr.put(j);
					   aSubarr.put(rs.getDouble("ANO_"+col[j]));
					   artesarr.put(aSubarr);
					   aSubarr=new JSONArray();
				   }else{
					   aSubarr.put(j);
					   aSubarr.put(0);
					   artesarr.put(aSubarr);
					   aSubarr=new JSONArray();
				   }		
				   
				}
				artesobj.put(specie.toUpperCase(), artesarr);
				obj.put("phylum",rs.getString(1));
				obj.put("clave", rs.getString(2));
				obj.put("especie", rs.getString(4));
			}
			
			JSONObject ticks=new JSONObject();
			JSONObject xtickobj = new JSONObject();
    		JSONArray xtickarr =  new JSONArray();
    		
			for(int j=0;j<col.length;j++){
				xtickobj.put("v", j);
    			xtickobj.put("label", col[j]);
    		    xtickarr.put(xtickobj);
    		    xtickobj = new JSONObject();
    		
			}
			ticks.put("xTicks",xtickarr);
			
			obj.put("ticks", ticks);
			obj.put("dataset", artesobj);
			
			System.out.println("obj: "+obj.toString());
			return obj.toString();
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
	}
	public String loadDataToArtes(Connection conn, String artes, String iperiodo, String fperiodo) throws SQLException, JSONException{
			
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		
		int iperiod=Integer.parseInt(iperiodo);
		int fperiod=Integer.parseInt(fperiodo);
		
		String[] metodos=artes.split(",");
		ColorSchema schema=new ColorSchema();
		
		String[] col=new String[((fperiod-iperiod)+1)];
		col[0]=iperiodo;
		
		StringBuffer columnsName=new StringBuffer();
		columnsName.append("round(ANO_"+iperiodo+",2) "+"ANO_"+iperiodo);
		
		for(int i=1;i<=(fperiod-iperiod);i++){
			columnsName.append(",round(ANO_"+(iperiod+i)+",2) "+"ANO_"+(iperiod+i));
			col[i]=String.valueOf((iperiod+i));
		}
		
		String sql="select metodo, "+columnsName.toString()+
					" from sipein.vm_capturaporarte"+
					" where metodo in ("+artes+")"+
					" order by 1";
		System.out.println("SQL:.. "+sql);
		try{
			pstmt = conn.prepareStatement(sql);
			
			rs = pstmt.executeQuery();
			
			JSONObject obj=new JSONObject();
			JSONObject artesobj = new JSONObject();
    		JSONArray artesarr = null;
    		JSONArray aSubarr=null;
    		JSONObject artesColor= new JSONObject();
    		int k=0;
			while (rs.next()) {
				artesarr =  new JSONArray();
				aSubarr=new JSONArray();
				for(int j=0;j<col.length;j++){
				   if(rs.getString("ANO_"+col[j])!=null){
					   aSubarr.put(j);
					   aSubarr.put(rs.getDouble("ANO_"+col[j]));
					   artesarr.put(aSubarr);
					   aSubarr=new JSONArray();
				   }else{
					   aSubarr.put(j);
					   aSubarr.put(0);
					   artesarr.put(aSubarr);
					   aSubarr=new JSONArray();
				   }	
				   
				}
				artesobj.put(rs.getString(1), artesarr);
				if(metodos.length>1){
					artesColor.put(rs.getString(1),ColorSchema.colors.get(k++) );
					//k++;
				}
			}
			
			JSONObject ticks=new JSONObject();
			JSONObject xtickobj = new JSONObject();
    		JSONArray xtickarr =  new JSONArray();
    		
			for(int j=0;j<col.length;j++){
				xtickobj.put("v", j);
    			xtickobj.put("label", col[j]);
    		    xtickarr.put(xtickobj);
    		    xtickobj = new JSONObject();
    		
			}
			ticks.put("xTicks",xtickarr);
			
			obj.put("ticks", ticks);
			obj.put("dataset", artesobj);
			obj.put("colorSchema", artesColor);
			
			System.out.println("obj: "+obj.toString());
			return obj.toString();
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
	}
	public String loadDataForCPUE(Connection conn,String arte,String specie,String iperiodo,String fperiodo)throws SQLException, JSONException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		
		int iperiod=Integer.parseInt(iperiodo);
		int fperiod=Integer.parseInt(fperiodo);
		
		String[] col=new String[((fperiod-iperiod)+1)];
		col[0]=iperiodo;
		
		StringBuffer columnsName=new StringBuffer();
		columnsName.append("round(ANO_"+iperiodo+",2) "+"ANO_"+iperiodo);
		
		for(int i=1;i<=(fperiod-iperiod);i++){
			columnsName.append(",round(ANO_"+(iperiod+i)+",2) "+"ANO_"+(iperiod+i));
			col[i]=String.valueOf((iperiod+i));
		}
		
		String sql="select nombrecomun, "+columnsName.toString()+
					" from sipein.vm_cpueporarteyespecie "+
					" where UPPER(metodo)=UPPER(?) and UPPER(nombrecomun)=UPPER(?)"+
					" order by 1";
		
		System.out.println("SQL:.. "+sql);
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, arte);
			pstmt.setString(2, specie);
			
			rs = pstmt.executeQuery();
			
			JSONObject obj=new JSONObject();
			JSONObject artesobj = new JSONObject();
    		JSONArray artesarr = null;
    		JSONArray aSubarr=null;
    		
    		
			while (rs.next()) {
				artesarr =  new JSONArray();
				aSubarr=new JSONArray();
				for(int j=0;j<col.length;j++){
				   if(rs.getString("ANO_"+col[j])!=null){
					   aSubarr.put(j);
					   aSubarr.put(rs.getDouble("ANO_"+col[j]));
					   artesarr.put(aSubarr);
					   aSubarr=new JSONArray();
				   }	
				   
				}
				artesobj.put(rs.getString(1), artesarr);
			}
			
			JSONObject ticks=new JSONObject();
			JSONObject xtickobj = new JSONObject();
    		JSONArray xtickarr =  new JSONArray();
    		
			for(int j=0;j<col.length;j++){
				xtickobj.put("v", j);
    			xtickobj.put("label", col[j]);
    		    xtickarr.put(xtickobj);
    		    xtickobj = new JSONObject();
    		
			}
			ticks.put("xTicks",xtickarr);
			
			obj.put("ticks", ticks);
			obj.put("dataset", artesobj);
			
			System.out.println("obj: "+obj.toString());
			return obj.toString();
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
	}
	
	public String loadDataForCDE(Connection conn,String specie,String iperiodo,String fperiodo)throws SQLException, JSONException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		
		int iperiod=Integer.parseInt(iperiodo);
		int fperiod=Integer.parseInt(fperiodo);
		
		String[] col=new String[((fperiod-iperiod)+1)];
		col[0]=iperiodo;
		
		StringBuffer columnsName=new StringBuffer();
		columnsName.append("round(ANO_"+iperiodo+",2) "+"ANO_"+iperiodo);
		
		for(int i=1;i<=(fperiod-iperiod);i++){
			columnsName.append(",round(ANO_"+(iperiod+i)+",2) "+"ANO_"+(iperiod+i));
			col[i]=String.valueOf((iperiod+i));
		}
		
		String sql=" SELECT cl.descripcion phylum, esp.clave, vm.nombrecomun, "+
				    " vm.especie, "+columnsName.toString()+
					" FROM sipein.vm_capturaporespecie vm, sipein.especies esp, sipein.codigos_listas cl "+
					" WHERE vm.nombrecomun = esp.nomesp "+
					" AND vm.especie = esp.nomcien "+
					" AND cl.codigo = taxon "+
					" AND cl.tabla = 20 "+
					" AND UPPER(nombrecomun)= UPPER(?) "+
					" ORDER BY 1";
		
		System.out.println("SQL:.. "+sql);
		try{
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, specie);
			
			rs = pstmt.executeQuery();
			
			JSONObject obj=new JSONObject();
			JSONObject artesobj = new JSONObject();
    		JSONArray artesarr = null;
    		JSONArray aSubarr=null;
    		JSONObject phylumobj = new JSONObject();
    		JSONObject claveobj = new JSONObject();
    		JSONObject especieobj = new JSONObject();
    		
			while (rs.next()) {
				artesarr =  new JSONArray();
				aSubarr=new JSONArray();
				for(int j=0;j<col.length;j++){
				   if(rs.getString("ANO_"+col[j])!=null){
					   aSubarr.put(j);
					   aSubarr.put(rs.getDouble("ANO_"+col[j]));
					   artesarr.put(aSubarr);
					   aSubarr=new JSONArray();
				   }else{
					   aSubarr.put(j);
					   aSubarr.put(0);
					   artesarr.put(aSubarr);
					   aSubarr=new JSONArray();
				   }		
				   
				}
				artesobj.put(rs.getString(3), artesarr);
				obj.put("phylum",rs.getString(1));
				obj.put("clave", rs.getString(2));
				obj.put("especie", rs.getString(4));
			}
			
			JSONObject ticks=new JSONObject();
			JSONObject xtickobj = new JSONObject();
    		JSONArray xtickarr =  new JSONArray();
    		
			for(int j=0;j<col.length;j++){
				xtickobj.put("v", j);
    			xtickobj.put("label", col[j]);
    		    xtickarr.put(xtickobj);
    		    xtickobj = new JSONObject();
    		
			}
			ticks.put("xTicks",xtickarr);
			
			obj.put("ticks", ticks);
			obj.put("dataset", artesobj);
			
			System.out.println("obj: "+obj.toString());
			return obj.toString();
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
	}
	
	public String loadDataForCDGrupo(Connection conn,String grupos,String iperiodo,String fperiodo)throws SQLException, JSONException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		
		ColorSchema schema=new ColorSchema();
		String[] gruposTax=grupos.split(",");
		int iperiod=Integer.parseInt(iperiodo);
		int fperiod=Integer.parseInt(fperiodo);
		
		String[] col=new String[((fperiod-iperiod)+1)];
		col[0]=iperiodo;
		
		StringBuffer columnsName=new StringBuffer();
		columnsName.append("round(ANO_"+iperiodo+",2) "+"ANO_"+iperiodo);
		
		for(int i=1;i<=(fperiod-iperiod);i++){
			columnsName.append(",round(ANO_"+(iperiod+i)+",2) "+"ANO_"+(iperiod+i));
			col[i]=String.valueOf((iperiod+i));
		}
		String sql="select grupo, "+columnsName.toString()+
					" from sipein.vm_capturaporgrupo "+
					" where codigo in ("+grupos+")"+
					" order by 1";
		
		System.out.println("SQL:.. "+sql);
		try{
			pstmt = conn.prepareStatement(sql);
			
			rs = pstmt.executeQuery();
			
			JSONObject obj=new JSONObject();
			JSONObject artesobj = new JSONObject();
    		JSONArray artesarr = null;
    		JSONArray aSubarr=null;
    		JSONObject artesColor= new JSONObject();
    		int k=0;
			while (rs.next()) {
				artesarr =  new JSONArray();
				aSubarr=new JSONArray();
				for(int j=0;j<col.length;j++){
				   if(rs.getString("ANO_"+col[j])!=null){
					   aSubarr.put(j);
					   aSubarr.put(rs.getDouble("ANO_"+col[j]));
					   artesarr.put(aSubarr);
					   aSubarr=new JSONArray();
				   }else{
					   aSubarr.put(j);
					   aSubarr.put(0);
					   artesarr.put(aSubarr);
					   aSubarr=new JSONArray();
				   }		
				   
				}
				artesobj.put(rs.getString(1), artesarr);
				if(gruposTax.length>1){
					artesColor.put(rs.getString(1),ColorSchema.colors.get(k++) );
					//k++;
				}
			}
			
			JSONObject ticks=new JSONObject();
			JSONObject xtickobj = new JSONObject();
    		JSONArray xtickarr =  new JSONArray();
    		
			for(int j=0;j<col.length;j++){
				xtickobj.put("v", j);
    			xtickobj.put("label", col[j]);
    		    xtickarr.put(xtickobj);
    		    xtickobj = new JSONObject();
    		
			}
			ticks.put("xTicks",xtickarr);
			
			obj.put("ticks", ticks);
			obj.put("dataset", artesobj);
			obj.put("colorSchema", artesColor);
			
			System.out.println("obj: "+obj.toString());
			return obj.toString();
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
	}
	
}