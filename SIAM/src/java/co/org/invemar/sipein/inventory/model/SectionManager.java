/**
 * 
 */
package co.org.invemar.sipein.inventory.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;



/**
 * @author Julian Jose Pizarro Pertuz
 * 
 */
public class SectionManager {

	private List<Section> sections;
	private Section sect=null;

	private Document readXML(String file) {

		SAXBuilder builder = new SAXBuilder();
		Document doc = null;
		try {
			doc = builder.build(file);

		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		return doc;
	}
	
	private Section makeSection(Element section){
		
		Section sect=new Section();
		Attribute name=section.getAttribute("name");
		/*Nombre de la section*/
		sect.setName(name.getValue());
		
		 List<Element> items=section.getChildren();
	     Iterator<Element> it=items.iterator();
	     Item item=null;
	     List<Item> itms=new ArrayList<Item>();
	     while(it.hasNext()){
	    	 Element elm=it.next();
	    	 //item=new Item();
	    	 /*columnsName elements*/
	    	 Attribute nameItem=elm.getAttribute("name");
	    	 List<Element> columnsName=elm.getChildren("colunmName");
	    	 Iterator<Element> cit=columnsName.iterator();
	    	 List<String> columns=new ArrayList<String>();
	    	 while(cit.hasNext()){
	    		 columns.add(cit.next().getValue());
	    	 }
	    	 
	    	 String query=elm.getChildText("query");
	    	 item=new Item(nameItem.getValue(), columns,query);
	    	 itms.add(item);
	     }
	     /*Items de la section*/
	     sect.setItems(itms);
	        
		return sect;
	}
	
	 public void createSections(String file){
	        
	        Document doc= readXML(file);
	        Element root=doc.getRootElement();
	        //System.out.println("Nombre del root element:.. "+root.getName());
	        /*Lista de todos los sections del archivo*/
	        List<Element> childrenroot=root.getChildren();
	        Iterator<Element> iterator=childrenroot.iterator();
	        //System.out.println("\nNumber of Entidades: "+childrenroot.size());
	        
	        sections=new ArrayList<Section>();
	        while (iterator.hasNext()){
	            Element section=iterator.next();
	            sections.add(makeSection(section));
	        }
	        
	        
	        
	    }
	 
		 
	 public List<Section> getSections(){
		 return sections;
	 }
	 
	 
	 public static void main(String[] args ){
         SectionManager sm=new SectionManager();
         sm.createSections("/home/rafalastra/web/webapps/siam/sipein/inv/sipein_stats.xml");
         List<Section> scs=sm.getSections();
         Iterator<Section> itr=scs.iterator();
         while(itr.hasNext()){
                 //System.out.println("======+++++++++++++++++++++++++++++=====");
                 //Entity ent=entm.getEntity(3);
                 Section sc=itr.next();

                 System.out.println("name:... "+sc.getName());
                 System.out.println("Nro de items "+sc.getItems().size());
                
                 
                 List<Item> itms=sc.getItems();
                 Iterator<Item> it=itms.iterator();
                 while(it.hasNext()){
                         Item i=it.next();
                         System.out.println("^^^^^^^^^^^ Items ^^^^^^^^^^");
                         System.out.println("name:.. "+i.getName());
                         System.out.println("Nro Column:.. "+i.getColumns().size());
                         System.out.println("Query:.. "+i.getQuery());
                         System.out.println("HTML: "+i.executeHTML());
                         
                 }
         }
 }

}
