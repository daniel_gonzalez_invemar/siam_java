/**
 * 
 */
package co.org.invemar.sipein.inventory.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import co.org.invemar.sipein.util.ConnectionFactory;

/**
 * @author Julian Jose Pizarro
 *
 */
public class Item {
	
	private List columns=null;
	private String query=null;
	private String name=null;
	private int total=0;
	
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	public Item(String name,List columns, String query) {
		this.name=name;
		this.columns = columns;
		this.query = query;
		
	}
	/**
	 * @return the columns
	 */
	public List getColumns() {
		return columns;
	}
	/**
	 * @param columns the columns to set
	 */
	public void setColumns(List columns) {
		this.columns = columns;
	}
	/**
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}
	/**
	 * @param query the query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}
	
	public String executeHTML() {
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		ConnectionFactory cFactory=null;
		Connection conn=null;
		StringBuffer html=new StringBuffer();
		StringBuffer TR=new StringBuffer();
		StringBuffer TD=null;
		
		int k=columns.size();
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!");
		
		try {
			System.out.println("===============================");
			cFactory=new ConnectionFactory();
			conn=cFactory.createConnection();
			System.out.println("!!!!!!!!!!executeHTML() Query: "+this.query);
			
			pstmt = conn.prepareStatement(this.query);
			
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				System.out.println("RS");
				TD=new StringBuffer();
				for(int i=1;i<=k;i++){
					TD.append("<td>"+rs.getString(i)+"</td>");
					if(i==k){
						total+=Integer.parseInt(rs.getString(i));
					}
				}
				TR.append("<tr>");
				TR.append(TD.toString());
				TR.append("</tr>");
				System.out.println("TR: .. "+TR.toString());
			}
			
			
		}catch(Exception e){
			System.out.println("Error!! executeHTML method. Success: "+e);
		}finally{
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}
		}
		return TR.toString();
	}
	
	public int getTotal(){
		return total;
	}
	
	
		
	public String toString(){
		String ret;
		//
		ret="columns:.. "+columns.toString()+"\n query:.. "+query;
		
		return ret;
	}
	
	
}
