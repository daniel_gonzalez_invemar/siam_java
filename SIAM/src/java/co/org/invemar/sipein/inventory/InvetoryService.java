package co.org.invemar.sipein.inventory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.org.invemar.sipein.inventory.model.Section;
import co.org.invemar.sipein.inventory.model.SectionManager;

public class InvetoryService extends HttpServlet {
	private javax.servlet.ServletContext servletContext;
	
	public void init(ServletConfig config) throws ServletException {
		servletContext = config.getServletContext();
		super.init(config);
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		
		String view="/sipein/inv/inventario.jsp";
		String pathConfig=servletContext.getRealPath("sipein/inv/conf");
		System.out.println("sipein config: "+pathConfig);
		SectionManager sm=new SectionManager();
        sm.createSections(pathConfig+File.separator+"sipein_stats.xml");
        List<Section> scs=sm.getSections();
        
        request.setAttribute("SECTIONS", scs);
        
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher ( view ) ;
    	if ( dispatcher == null ) {
    		throw new ServletException ( "Unable to find " + view );
    		}
    	//	Pass control to the ViewsFlash servlet, which will write directly to the output stream.
    	dispatcher.forward (request, response);	
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request,response);
	}

}
