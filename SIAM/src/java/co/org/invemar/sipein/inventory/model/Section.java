/**
 * 
 */
package co.org.invemar.sipein.inventory.model;

import java.util.List;

/**
 * @author Julian Jose Pizarro
 * 
 */
public class Section {
	private String name;
	private List<Item> items;

	/**
	 * @return the items
	 */
	public List<Item> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List<Item> items) {
		this.items = items;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
}
