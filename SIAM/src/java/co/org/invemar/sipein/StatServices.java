package co.org.invemar.sipein;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.org.invemar.sipein.model.SIPEINDataManager;
import co.org.invemar.sipein.util.ConnectionFactory;

public class StatServices extends HttpServlet {

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		
		SIPEINDataManager dm=null;
		ConnectionFactory cFactory=null;
		Connection conn=null;
		
		
		
		String action=request.getParameter("action");
        System.out.println("ACTION:.. "+action);
        
        String resp=null;
        
        if(action.equalsIgnoreCase("fillartepesca")){
        	String page=request.getParameter("page");
        	String obj=null;
        	dm=new SIPEINDataManager();
  		    cFactory=new ConnectionFactory();
        	 if(page.equalsIgnoreCase("AP")){
	        	  
	    		  try {
					conn = cFactory.createConnection();
					obj=dm.loadArtePesca(conn,"metodo","sipein.vm_capturaporarte");
	      		    
				  } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  }finally{
						if(conn!=null){
							ConnectionFactory.closeConnection(conn);
						}
					}
        	 }else if(page.equalsIgnoreCase("AyE")){
        		 
	    		  try {
					conn = cFactory.createConnection();
					obj=dm.loadArtePesca(conn,"artepesca","sipein.vm_capturaporarteyespecie");
	      		    
				  } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  }finally{
						if(conn!=null){
							ConnectionFactory.closeConnection(conn);
						}
					}
        	 }else if(page.equalsIgnoreCase("CPUE")){
        		 
	    		  try {
					conn = cFactory.createConnection();
					obj=dm.loadArtePesca(conn,"metodo","sipein.vm_cpueporarteyespecie");
	      		    
				  } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  }finally{
						if(conn!=null){
							ConnectionFactory.closeConnection(conn);
						}
					}
       	     }	  	  	
        	 
        	 response.setContentType("text/text");
				response.setHeader("Cache-Control", "no-cache");
				response.getWriter().write(obj.toString());
  	    }
        
        if(action.equalsIgnoreCase("fillstarttime")){
        	
        	String page=request.getParameter("page");
        	String obj=null;
        	dm=new SIPEINDataManager();
    		  cFactory=new ConnectionFactory();
        	  if(page.equalsIgnoreCase("AP")){
        		  
          		  try {
					conn = cFactory.createConnection();
					obj=dm.getDates(conn, "vm_capturaporarte");
	        		
				  } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  }finally{
						if(conn!=null){
							ConnectionFactory.closeConnection(conn);
						}
					}
        		 
        	  }else if(page.equalsIgnoreCase("AyE")){
        		 
          		  try {
					conn = cFactory.createConnection();
					obj=dm.getDates(conn, "vm_capturaporarteyespecie");
	        		
				  } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  }finally{
						if(conn!=null){
							ConnectionFactory.closeConnection(conn);
						}
					}
        	  }else if(page.equalsIgnoreCase("CPUE")){
        		 
          		  try {
					conn = cFactory.createConnection();
					obj=dm.getDates(conn, "vm_cpueporarteyespecie");
	        		
				  } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  }finally{
						if(conn!=null){
							ConnectionFactory.closeConnection(conn);
						}
					}
        	  }else if(page.equalsIgnoreCase("CDE")){
        		 
          		  try {
					conn = cFactory.createConnection();
					obj=dm.getDates(conn, "vm_capturaporespecie");
	        		
				  } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  }finally{
						if(conn!=null){
							ConnectionFactory.closeConnection(conn);
						}
					}
        	  }else if(page.equalsIgnoreCase("CDG")){
        		 
          		  try {
					conn = cFactory.createConnection();
					obj=dm.getDates(conn, "vm_capturaporgrupo");
	        		
				  } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  }finally{
						if(conn!=null){
							ConnectionFactory.closeConnection(conn);
						}
					}
        	  }
        	
        	
        	  response.setContentType("text/text");
				response.setHeader("Cache-Control", "no-cache");
				response.getWriter().write(obj.toString());
        }
        if(action.equalsIgnoreCase("especiebytaxon")){
        	String obj=null;
        	dm=new SIPEINDataManager();
   		  	cFactory=new ConnectionFactory();
   		  	String idTaxon=request.getParameter("value");
   		  	
   		  	  try {
				conn = cFactory.createConnection();
				obj=dm.findEspeciesByTaxon(conn, idTaxon);
				
				response.setContentType("text/text");
				response.setHeader("Cache-Control", "no-cache");
				response.getWriter().write(obj.toString());
				
			  } catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			  } catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			  }finally{
					if(conn!=null){
						ConnectionFactory.closeConnection(conn);
					}
				}
        }
        if(action.equalsIgnoreCase("especie")){
        	String obj=null;
        	String page=request.getParameter("page");
        	String arte=request.getParameter("value");
        	dm=new SIPEINDataManager();
   		  	cFactory=new ConnectionFactory();
        	if(page.equalsIgnoreCase("CPUE")){
        	
	   		  try {
					conn = cFactory.createConnection();
					obj=dm.findEspeciesByArte(conn, arte,"sipein.vm_cpueporarteyespecie","NOMBRECOMUN","METODO");
	     		   
				  } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  }finally{
						if(conn!=null){
							ConnectionFactory.closeConnection(conn);
						}
					}
        	}if(page.equalsIgnoreCase("AyE")){
        		 try {
 					conn = cFactory.createConnection();
 					obj=dm.findEspeciesByArte(conn, arte,"sipein.vm_capturaporarteyespecie","nomesp","artepesca");
 	     		   
 				  } catch (SQLException e) {
 					// TODO Auto-generated catch block
 					e.printStackTrace();
 				  } catch (JSONException e) {
 					// TODO Auto-generated catch block
 					e.printStackTrace();
 				  }finally{
 						if(conn!=null){
 							ConnectionFactory.closeConnection(conn);
 						}
 					}
        	}
        	
        	response.setContentType("text/text");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(obj.toString());
        	
        }
        
        
        if(action.equalsIgnoreCase("toGraphic")){
        	
        	String page=request.getParameter("page");
        	String iperiodo=request.getParameter("iperiodo");
    		String fperiodo=request.getParameter("fperiodo");
    		
    		dm=new SIPEINDataManager();
    		cFactory=new ConnectionFactory();
    		String obj=null;
    		
        	if(page.equalsIgnoreCase("AP")){
        		String artes=request.getParameter("artes");
        		
        		System.out.println("ARTES: "+artes);
        		
        		
				try {
					conn = cFactory.createConnection();
					obj=dm.loadDataToArtes(conn, artes, iperiodo, fperiodo);
	        		
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					if(conn!=null){
						ConnectionFactory.closeConnection(conn);
					}
				}
        		 

        	}else if(page.equalsIgnoreCase("AyE")){
        		
        		String arte=request.getParameter("arte");
        		String specie=request.getParameter("especie");
        		
        		        		
				try {
					conn = cFactory.createConnection();
					obj=dm.loadDataForArtesAndSpecie(conn, arte, specie, iperiodo, fperiodo);
	        		
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					if(conn!=null){
						ConnectionFactory.closeConnection(conn);
					}
				}
				
        		
        	}else if(page.equalsIgnoreCase("CPUE")){
        		  	  	  	  	  	  	  	  

        		String arte=request.getParameter("arte");
        		String specie=request.getParameter("especie");
        		
        		        		
				try {
					conn = cFactory.createConnection();
					obj=dm.loadDataForCPUE(conn, arte, specie, iperiodo, fperiodo);
	        		
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					if(conn!=null){
						ConnectionFactory.closeConnection(conn);
					}
				}
        	} else if(page.equalsIgnoreCase("CDE")){
        		  	  	  	  	  	  	  	  

        		String specie=request.getParameter("especie");
        		
        		        		
				try {
					conn = cFactory.createConnection();
					obj=dm.loadDataForCDE(conn, specie, iperiodo, fperiodo);
	        		
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					if(conn!=null){
						ConnectionFactory.closeConnection(conn);
					}
				}
        	}else if(page.equalsIgnoreCase("CDG")){
        		  	  	  	  	  	  	  	  

        		String grupos=request.getParameter("grupos");
        		
        		        		
				try {
					conn = cFactory.createConnection();
					obj=dm.loadDataForCDGrupo(conn, grupos, iperiodo, fperiodo);
	        		
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					if(conn!=null){
						ConnectionFactory.closeConnection(conn);
					}
				}
        	}

        	
        	response.setContentType("text/text");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(obj.toString());
        	
        	
        }
        
        
        
		
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request,response);
		
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occure
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
