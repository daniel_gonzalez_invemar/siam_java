/*
 * ConnectionFactory.java
 *
 * Created on 5 de junio de 2007, 03:41 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package co.org.invemar.sipein.util;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * 
 * @author Julian Jose Pizarro Pertuz
 */
public class ConnectionFactory {

	static InitialContext initialContext;

	static {
		try {
			initialContext = new InitialContext();

		} catch (NamingException ne) {

			System.out.println("Error!! No se ha podido crear el Objeto InitialContext "+ ne);
		}
	}

	public synchronized DataSource getDataSource() {
		DataSource data;
		Context envContext;
		try {
			envContext = (Context) initialContext.lookup("java:comp/env");
			data = (DataSource) envContext.lookup("jdbc/sibm");

			System.out.println("DataSource obtenido exitosamente");
			return data;
		} catch (Exception ex) {
			System.out.println("ERROR: en getDataSource::::::::::::::::::. "
					+ ex);
			return null;
		}
	}

	public synchronized Connection createConnection() throws SQLException {
		Connection con = getDataSource().getConnection();
		return con;
	}
	
	public static void closeConnection(Connection connection)
    {
    
    if (connection != null) {        
        try {
            connection.close();
            
        } catch (SQLException e) {
            System.out.println("Error closeConnection "+e);
        }   
       
    }
    
}


}
