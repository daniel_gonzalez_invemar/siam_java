/**
 * 
 */
package co.org.invemar.siam.sibm.controller.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import co.org.invemar.siam.model.ListadoEspExcel;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

/**
 * @author Administrador
 *
 */
public class DownloadLEspecieAction implements Action {
	private HttpServletRequest pRequest=null;
	private HSSFWorkbook wb = null;
	static HashMap criterios = new HashMap();
	static {
	  criterios.put("clase", "f.CLASE");
	  criterios.put("orden", "f.ORDEN");
	  criterios.put("familia", "f.FAMILIA");
	  criterios.put("genero", "f.GENERO");
	  criterios.put("especie", "f.ESPECIE");
	}

	static HashMap icriterios=new HashMap();
	static{
		 icriterios.put("scientificname", "f.especie");
	     icriterios.put("commonname", "f.NOMBRE_COMUN");
		 icriterios.put("genus", "f.GENERO");
	}

	static HashMap ioperators=new HashMap();
	static{
		ioperators.put("begin","like");
		ioperators.put("igual","=");
		ioperators.put("contains","like");
		

	}

	static HashMap fields = new HashMap();
	static {

	  fields.put("familia", "familia");
	  fields.put("genero", "genero");	
	  fields.put("especie", "especie");
	  fields.put("vulgar", "nombre_comun");
	  fields.put("fechamin", "fechamin");
	  fields.put("fechamax", "fechamax");
	  fields.put("frecuencia", "frecuencia");
	  
	}


	
	
	public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ConnectionFactory cf=new ConnectionFactory();
		this.pRequest=request;
		Connection  connection=null;
		
		ListadoEspExcel ls=new ListadoEspExcel ();
		
		
		ArrayList params = new ArrayList();
		String especie = request.getParameter("nespecie");
		String genero = param("ngenero", "");
		String division = param("phylum", "");
		String criterioParam = param("criterio", "");
		String criterio = (String)criterios.get(criterioParam);
		String valor = param("valor", "");

//		Criterios de busquedas de la pagina index.htm
		String pcriterio=param("icriterio","");
		String icriterio=(String)icriterios.get(pcriterio);
		String poperator=param("ioperator","");
		String ioperator=(String)ioperators.get(poperator);
		String ivalue=param("ivalue","");


		String cites = param("ncites", "");
		String librorojo = param("nlibrorojo", "");
		String catlibrorojo = param("ncatlibrorojo", "");
		String ano1 = param("ano1", "");
		String ano2 = param("ano2", "");

		String lat1 = param("lat1", "");
		String lat2 = param("lat2", "");
		String lng1 = param("lng1", "");
		String lng2 = param("lng2", "");

		String prof1 = param("nprof1", "");
		String prof2 = param("nprof2", "");
		String vulgar = param("vulgar2", "");
		String ambiente = param("ambiente", "");
		String zona = param("zona", "");
		String region = param("nreg", "");
		String ecoregion = param("ecoregion", "");
		String proyecto=param("proyecto","");
		String estacion=param("estacion","");

		String otherCriteria=param("othercriterios","");
		String otherColumns=param("columns","");

		String pages=param("pages","");

		boolean hasEspecie = StringUtils.isNotEmpty(especie);
		System.out.println("hasEspecie:.. "+hasEspecie);
		boolean hasGenero = StringUtils.isNotEmpty(genero);
		System.out.println("hasGenero:.. "+hasGenero);
		boolean hasDivision = StringUtils.isNotEmpty(division);
		boolean hasCriterio = StringUtils.isNotEmpty(criterio);
		System.out.println("hasCriterio:.. "+hasCriterio);
		boolean hasValor = StringUtils.isNotEmpty(valor);

//		verificamos que se tengan criterios de busquedas para la busqueda simple en index.htm
		boolean hasICriterio=StringUtils.isNotEmpty(icriterio);
		boolean hasIOperator=StringUtils.isNotEmpty(ioperator);
		boolean hasIValue=StringUtils.isNotEmpty(ivalue);

		boolean hasCites = StringUtils.isNotEmpty(cites);
		boolean hasLibrorojo = StringUtils.isNotEmpty(librorojo);
		boolean hasCatlibrorojo = StringUtils.isNotEmpty(catlibrorojo);
		/*boolean hasAno1 = StringUtils.isNotEmpty(ano1);
		boolean hasAno2 = StringUtils.isNotEmpty(ano2);*/


		boolean hasLat1 = StringUtils.isNotEmpty(lat1);
		boolean hasLat2 = StringUtils.isNotEmpty(lat2);
		boolean hasLng1 = StringUtils.isNotEmpty(lng1);
		boolean hasLng2 = StringUtils.isNotEmpty(lng2);


		boolean hasProf1 = StringUtils.isNotEmpty(prof1);
		boolean hasProf2 = StringUtils.isNotEmpty(prof2);
		boolean hasVulgar = StringUtils.isNotEmpty(vulgar);
		boolean hasAmbiente = StringUtils.isNotEmpty(ambiente);
		boolean hasZona = StringUtils.isNotEmpty(zona);
		boolean hasRegion = StringUtils.isNotEmpty(region);
		boolean hasEcoregion = StringUtils.isNotEmpty(ecoregion);
		boolean hasProyecto=StringUtils.isNotEmpty(proyecto);
		boolean hasEstacion=StringUtils.isNotEmpty(estacion);

		boolean hasOtherCriteria=StringUtils.isNotEmpty(otherCriteria);
		boolean hasOtherColumns=StringUtils.isNotEmpty(otherColumns);

		if (hasDivision) params.add(division);

		if (hasGenero){ 
			params.add("%" + genero + "%");
			params.add("%" + genero + "%");
		}

		if (hasEspecie)
		{ 
			params.add("%" + especie + "%");
			params.add("%" + especie + "%");
		}
//		parameter de la busqueda simple
		if(hasICriterio && hasIOperator && hasIValue){
			
				if(poperator.equalsIgnoreCase("begin")){
					if(!pcriterio.equalsIgnoreCase("commonname")){
						params.add(ivalue+"%");
					}
					
					params.add(ivalue+"%");
				}else if(poperator.equalsIgnoreCase("contains")){
					if(!pcriterio.equalsIgnoreCase("commonname")){
						params.add("%"+ivalue+"%");
					}
					params.add("%"+ivalue+"%");
				}else {
					if(!pcriterio.equalsIgnoreCase("commonname")){
						params.add(ivalue);
					}
					params.add(ivalue);
				}
		}


		if (hasCriterio && hasValor && !criterio.equalsIgnoreCase("f.clase")) {
			
			params.add("%" + valor + "%");
			params.add("%" + valor + "%");
		}else if (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.clase")) {
			params.add("%" + valor + "%");
		}


		Pattern p = Pattern.compile(",");
		Matcher m;
		if (hasLat1)
		{
			m = p.matcher(lat1);
			lat1=m.replaceAll(".");
			System.out.println("LAT1: "+lat1);
			params.add(Double.parseDouble(lat1));
		 }
		if (hasLat2){ 
			m = p.matcher(lat2);
			lat2=m.replaceAll(".");
			System.out.println("LAT2: "+lat2);
			
			params.add(Double.parseDouble(lat2));
		}
		if (hasLng1){
			m = p.matcher(lng1);
			lng1=m.replaceAll(".");
			System.out.println("LNG1: "+lng1);
			
			 params.add(Double.parseDouble(lng1));
		}
		if (hasLng2){
			m = p.matcher(lng2);
			lng2=m.replaceAll(".");
			System.out.println("LNG2: "+lng2);
			
		 	params.add(Double.parseDouble(lng2));
		 }
		System.out.println("================");

		if (hasProf1) params.add(prof1);
		if (hasProf2) params.add(prof2);

		if (hasVulgar) params.add("%" + vulgar + "%");
		if (hasAmbiente) params.add(ambiente);


		/*if (hasAno1) params.add(ano1);
		if (hasAno2) params.add(ano2);*/


		if (hasZona) params.add(zona);

		if (hasEcoregion) params.add(ecoregion);

		if (hasRegion) params.add(region + "%");

		if (hasLibrorojo && !librorojo.equals("N") && hasCatlibrorojo) params.add(catlibrorojo);

		if(hasProyecto) params.add(proyecto);
		if(hasEstacion) params.add(estacion);

		int position = intParam("position", 0);
		int length = 20;
		String field = param("field", "especie");
		boolean reversed = StringUtils.isNotBlank(request.getParameter("reversed"));
		if (!fields.containsKey(field)) field = "familia,genero,especie";



		String query=" SELECT   f.familia AS familia,f.especie AS nombrecientifico, f.genero, f.nombre_comun,f.UICN_NACIONAL,f.CITES, "+
			         " f.clave AS clave,TO_CHAR(min(f.fechacap),'MM/DD/YYYY') fechamin,TO_CHAR(max(f.fechacap),'MM/DD/YYYY') fechamax, count(1) frecuencia  "
		+" FROM VM_FICHAC f "
		+" where ROUND(MOD(TRUNC((NODOS_ACTIVOS/1),1),10),0) = 1  "

		+ (hasDivision? " AND  f.division = ?": "") 

		+ (hasGenero? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "") 

		+(hasEspecie? " AND UPPER(f.EPITETO) IN (SELECT vigente FROM (SELECT NULL anterior, especie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT especie, epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")

		+ (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.especie") ? " AND UPPER(f.especie) IN (SELECT vigente FROM (SELECT NULL anterior, genero ||' ' || especie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero ||' ' || especie, genero_vigente ||' ' || epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior "+ioperator +" UPPER (?) OR vigente "+ioperator +" UPPER (?))": "") 
		+ (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.GENERO") ? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior "+ioperator +" UPPER (?) OR vigente "+ioperator +" UPPER (?))": "") 
		+ (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.NOMBRE_COMUN") ?  " AND  UPPER(f.NOMBRE_COMUN) "+ioperator +" UPPER(?)": "") 
		/*+ (hasCriterio && hasValor? " AND " + criterio + " like UPPER(?)": "")*/
		+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.genero") ? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
		+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.especie") ? " AND UPPER(f.especie) IN (SELECT vigente FROM (SELECT NULL anterior, genero ||' ' || especie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero ||' ' || especie, genero_vigente ||' ' || epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
		+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.orden") ? " AND UPPER (f.orden) IN ( SELECT vigente FROM (SELECT NULL anterior, orden vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT orden, orden_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
		+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.familia") ? " AND UPPER (f.familia) IN ( SELECT vigente FROM (SELECT NULL anterior, familia vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT familia, familia_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "") 
		+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.clase") ? " AND UPPER (f.clase) like UPPER(?)": "")  

		  /* + (hasAno1? " AND f.ANOCAPTURA >= ?": "") + (hasAno2? " AND f.ANOCAPTURA <= ?": "")*/ 
		 + (hasLat1? " AND f.LATITUD >= ?": "") 
		 + (hasLat2? " AND f.LATITUD <= ?": "") 
		 + (hasLng1? " AND f.LONGITUD >= ?": "") 
		 + (hasLng2? " AND f.LONGITUD <= ?": "")  
		  + (hasProf1? " AND f.PROF_CAPTURA >= ?": "") 
		  + (hasProf2? " AND f.PROF_CAPTURA <= ?": "") 
		  + (hasVulgar? " AND UPPER(f.NOMBRE_COMUN) LIKE UPPER(?)": "") 
		  + (hasAmbiente? " AND f.AMBIENTE = ?": "") 
		  + (hasZona? " AND f.ZONA = ?": "") 
		  //+ (hasAmbiente? " AND f.AMBIENTE = ?": "") 
		  //+ (hasZona? " AND f.ZONA = ?": "") 
		  + (hasEcoregion? " AND f.ECORREGION LIKE ?": "") 
		  + (hasRegion? " AND f.REGION LIKE ?": "")
		   + (hasCites? " AND  f.CITES IS "  + (cites.equals("N")? "NULL": "NOT NULL"): "")
		   +(hasLibrorojo? " AND f.UICN_NACIONAL IS " + (librorojo.equals("N")? "NULL": "NOT NULL" + (hasCatlibrorojo? " AND substr(f.UICN_NACIONAL,1,2) = ?": "")): "")
		   + (hasProyecto? " AND f.proyecto = ?":"")
		   + (hasEstacion? " AND f.estacion=?":"")
		  + (hasOtherCriteria? " AND "+otherCriteria: "") 
		  + " GROUP BY f.clave, f.familia, f.genero, f.especie, f.nombre_comun,f.UICN_NACIONAL,f.CITES "
		 + " ORDER BY " + fields.get(field) + " " + (reversed? "DESC": "ASC");
		 
		System.out.println("QUERY:.. "+query);
		System.out.println("PARAMS SIZE:.. "+params.size());
		Object[] parameters = params.toArray();
		
		try {
			connection =cf.createConnection();
			wb=ls.findListadoEspeciesEXCEL(connection,query,parameters);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\""
				+ "ListadoEspeciesAreaGeografica.xls"+ "\"");
		response.setHeader("Pragma", "no-cache");

		ServletOutputStream outs = response.getOutputStream();
		wb.write(outs);
		outs.flush();
		outs.close();
		
		return null;
	
	}
	
	String param(String name, String def) {
		  HttpServletRequest request = this.pRequest;
		  return StringUtils.defaultIfEmpty(request.getParameter(name), def);
		}

		/*
		Igual a la anterior, pero el valor lo devuelve como un numero entero
		*/
	int intParam(String name, int def) {
		  return Integer.parseInt(param(name, String.valueOf(def)));
	}


}
