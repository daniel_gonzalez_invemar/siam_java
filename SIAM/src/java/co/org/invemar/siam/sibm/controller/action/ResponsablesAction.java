/**
 * 
 */
package co.org.invemar.siam.sibm.controller.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.org.invemar.siam.sibm.model.FichaCatalogoDM;
import co.org.invemar.siam.sibm.vo.Responsable;
import co.org.invemar.sipein.util.ConnectionFactory;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

/**
 * @author Administrador
 *
 */
public class ResponsablesAction implements Action{

	public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// TODO Auto-generated method stub
		String nroInvemar = request.getParameter("numero");
		FichaCatalogoDM fc=new FichaCatalogoDM();
		ConnectionFactory cFactory = null;
		Connection connection = null;
		cFactory = new ConnectionFactory();
		List<Responsable> responsables=null;
		try {
			connection = cFactory.createConnection();
			responsables=fc.findResponsables(connection, nroInvemar);
			System.out.println("responsables: "+responsables.size());
			
			request.setAttribute("LISTRESPONSABLES", responsables);
			//request.setAttribute("", );
			
			return new ActionRouter("/sibm/regbiolresponsables.jsp?"+request.getQueryString());
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		return new ActionRouter("/sibm/ficha_catalogo.jsp?"+request.getQueryString());
	}

}
