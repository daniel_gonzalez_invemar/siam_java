/**
 * 
 */
package co.org.invemar.siam.sibm.util;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Administrador
 *
 */
public class DBUtil {
	  private QueryRunner db;
	  public Object query(String query, ResultSetHandler rsh) throws SQLException {
	    return query(query, null, rsh, true);
	  }
	  public Object query(String query, ResultSetHandler rsh, boolean json) throws SQLException {
	    return query(query, null, rsh, json);
	  }
	  public Object query(String query, Object[] params, ResultSetHandler rsh) throws SQLException {
	    return query(query, params, rsh, true);
	  }
	  public Object query(String query, Object[] params, ResultSetHandler rsh, boolean json) throws SQLException {
	    Object res=null;
//	    try{
	    	res = db.query(query, params, rsh);
//	    }catch(SQLException e){
//	    	throw new SQLException("Error Interno en la aplicacion");
	    
//	    }
	    return json? res instanceof List? new JSONArray((List)res): res instanceof String? JSONObject.quote((String)res): res instanceof Map? new JSONObject((Map)res): res: res;
	  }
	}
