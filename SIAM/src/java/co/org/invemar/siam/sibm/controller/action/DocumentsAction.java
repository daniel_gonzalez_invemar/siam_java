package co.org.invemar.siam.sibm.controller.action;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONException;

import co.org.invemar.siam.sibm.model.DocumentsDM;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

public class DocumentsAction implements Action{
	private DocumentsDM docsdm;
	private ConnectionFactory cFactory = null;
	private Connection connection = null;
	
	public ActionRouter execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		// TODO Auto-generated method stub
		docsdm=new DocumentsDM();
		
		cFactory = new ConnectionFactory();
		String result=null;
		String method=request.getParameter("m");
		if(method.equals("topics")){
			result=topics();
		}else if(method.equals("ldocs")){
			
			result=lastDocs();
		}else if(method.equals("docs")){
			
			result=docs(request);
		}
		
		response.setContentType("text/text; charset=ISO-8859-1");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(result);
		
		return null;
	}
	
	public String topics() {
		 String result=null;
		try {
			connection = cFactory.createConnection();
			result=docsdm.findTopics(connection);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		return result;
		
	
	}
	
	public String lastDocs() {
		 String result=null;
		 try {
				connection = cFactory.createConnection();
				result=docsdm.findLastDocs(connection);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				if(connection!=null){
					ConnectionFactory.closeConnection(connection);
				}
			}
			return result;
			

	}
	
	public String docs(HttpServletRequest request) throws IOException, ServletException{
		String result=null;
		try {	System.out.println("ENCODING.. "+request.getParameter("val"));
				System.out.println("safe ENCODING.."+getParamterValue(request,"val","UTF-8"));
				System.out.println("request.getQueryString() "+request.getQueryString());
				connection = cFactory.createConnection();
				result=docsdm.findDocs(connection,getParamterValue(request,"val","UTF-8")/*getParamterValue(request,"val","UTF-8")*/);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				if(connection!=null){
					ConnectionFactory.closeConnection(connection);
				}
			}
			return result;
		

	}
	
	public String getParamterValue(HttpServletRequest request,String param,String encode) throws IOException, ServletException{
		String query=null;
		
		query=URLDecoder.decode(request.getQueryString(),encode);
		
		String paramValue=null;
		StringTokenizer st = new StringTokenizer(query, "&");
		while (st.hasMoreTokens()) {
			String pair = (String)st.nextToken();
			 int pos = pair.indexOf('=');
			 if (pos == -1) {
				throw new IllegalArgumentException();
		    }
			 String key = pair.substring(0, pos);
			 if(key.equalsIgnoreCase(param)){
				 paramValue = pair.substring(pos+1, pair.length());
				 
			 }
			
		}
		
		return paramValue;
	}
}
