package co.org.invemar.siam.sibm.legacy;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.lang.StringUtils;

public class UtilsLegacy {
	static DBAccess sibm = new DBAccess();
	static DBAccess sismac = new DBAccess();
	static DataSource sibmDS;
	static DataSource sismacDS;
	public static DBAccess poolConnectBD(){
		try {
		    
		    Context envContext  = (Context)new InitialContext().lookup("java:/comp/env");
		    sibmDS = (DataSource)envContext.lookup("jdbc/sibm");
		    sibm.db = new QueryRunner(sibmDS);
		    //sismacDS = (DataSource)sibmDS;//(DataSource)envContext.lookup("jdbc/sismac");
		    //sismac.db = new QueryRunner(sismacDS);
		    
		  }
		  catch (Exception e) {
		    e.printStackTrace();
		  }
		return sibm;
	}	
	
	public static String param(String parameter, String def) {
		   return StringUtils.defaultIfEmpty(parameter, def);
	}

	public static int intParam(String name, int def) {
		  return Integer.parseInt(param(name, String.valueOf(def)));
	}
	
	/*
	Modifica el String SQL para que devuelva solo una fraccion de los registros
	*/
	public static String limit(String query, int begin, int length) {
	  return "SELECT * FROM (SELECT a.*, ROWNUM AS LIMIT FROM (" + query + ") a) WHERE LIMIT BETWEEN " + begin + " AND " + (begin + length - 1);
	}

	/*
	Modifica el String SQL para que devuelva el numero total de registros de la consulta
	*/
	public static String count(String query) {
	  return "SELECT COUNT(*) FROM (" + query + ")";
	}
	
	public static void poolDesconectBD(){
		sismac.db = null;
		  sibm.db = null;
		  sismacDS = null;
		  sibmDS = null;
	}
}
