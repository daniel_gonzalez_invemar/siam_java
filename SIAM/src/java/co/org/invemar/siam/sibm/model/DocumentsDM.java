package co.org.invemar.siam.sibm.model;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DocumentsDM {

    public String findTopics(Connection conn) throws SQLException, JSONException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        JSONObject objsData = null;
        JSONArray arrayData = new JSONArray();

        String query = "select descripcion_lov from codigoslov where tabla_lov=38";
        try {
            pstmt = conn.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                objsData = new JSONObject();
                objsData.put("value", rs.getString("descripcion_lov"));
                objsData.put("name", rs.getString("descripcion_lov"));

                arrayData.put(objsData);
            }

        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
        return arrayData.toString();
    }

    public String findLastDocs(Connection conn) throws SQLException, JSONException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        JSONObject objsData = null;
        JSONArray arrayData = new JSONArray();

        String query = "SELECT "
                + "  narticulo_ID, "
                + "  2, "
                + "  UPPER(titulo) titulo, "
                + "  fecha "
                + "FROM "
                + "  ( "
                + "    SELECT "
                + "      ar.narticulo_ID, "
                + "      2, "
                + "      ar.titulo, "
                + "      'cat' categoria, "
                + "      ar.claves pclaves, "
                + "      ar.fecha, "
                + "      RANK() OVER (ORDER BY ar.fecha DESC ) AS ranking "
                + "    FROM "
                + "      narticulos ar "
                + "    WHERE "
                + "      lower(ar.claves) LIKE lower('%BEM*%') "
                + "  ) "
                + "WHERE "
                + "  ranking < 4 "
                + "AND ROWNUM<4 "
                + "ORDER BY "
                + "  fecha DESC";
        try {
            pstmt = conn.prepareStatement(query);
           // System.out.println("query:" + query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                objsData = new JSONObject();
                objsData.put("articulo", rs.getString("NARTICULO_ID"));
                objsData.put("idcat", rs.getString("ID_CAT"));
                objsData.put("titulo", rs.getString("TITULO"));
                objsData.put("fecha", rs.getString("fecha"));

                arrayData.put(objsData);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
        return arrayData.toString();
    }

    public String findDocs(Connection conn, String topics) throws SQLException, JSONException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        JSONObject objsData = null;
        JSONArray arrayData = new JSONArray();
     
        String query = " SELECT ar.narticulo_ID,2 as id_cat,ar.titulo "
                + " FROM narticulos ar"
                + " WHERE lower(ar.claves) like lower(?) ";
       // System.out.println("SQL: " + query);
       try {
        System.out.println("topics: " + topics);
        String codific = URLEncoder.encode(topics, "UTF-8");
        System.out.println("nuevo topic:"+URLDecoder.decode(codific, "UTF-8"));      
        
    
        
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, "%" + topics + "***%");
            rs = pstmt.executeQuery();
            while (rs.next()) {
                objsData = new JSONObject();
                objsData.put("articulo", rs.getString("NARTICULO_ID"));
                objsData.put("idcat", rs.getString("ID_CAT"));
                objsData.put("titulo", rs.getString("TITULO"));
				//objsData.put("fecha", rs.getString("fecha"));

                arrayData.put(objsData);
            }
        }catch(Exception e){
            System.out.println("Error:"+e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
        return arrayData.toString();
    }

}
