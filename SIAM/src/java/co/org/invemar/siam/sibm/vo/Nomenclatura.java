/**
 * 
 */
package co.org.invemar.siam.sibm.vo;

/**
 * @author Administrador
 *
 */
public class Nomenclatura {
	private String nroInvemar;
	private String name;
	private String fecha;
	private String actual;
	private String notas;
	private String tecnica;
	/**
	 * 
	 */
	public Nomenclatura() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param nroInvemar
	 * @param name
	 * @param fecha
	 * @param actual
	 * @param notas
	 * @param tecnica
	 */
	public Nomenclatura(String nroInvemar, String name, String fecha, String actual, String notas, String tecnica) {

		this.nroInvemar = nroInvemar;
		this.name = name;
		this.fecha = fecha;
		this.actual = actual;
		this.notas = notas;
		this.tecnica = tecnica;
	}
	/**
	 * @return the actual
	 */
	public String getActual() {
		return actual;
	}
	/**
	 * @param actual the actual to set
	 */
	public void setActual(String actual) {
		this.actual = actual;
	}
	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the notas
	 */
	public String getNotas() {
		return notas;
	}
	/**
	 * @param notas the notas to set
	 */
	public void setNotas(String notas) {
		this.notas = notas;
	}
	/**
	 * @return the nroInvemar
	 */
	public String getNroInvemar() {
		return nroInvemar;
	}
	/**
	 * @param nroInvemar the nroInvemar to set
	 */
	public void setNroInvemar(String nroInvemar) {
		this.nroInvemar = nroInvemar;
	}
	/**
	 * @return the tecnica
	 */
	public String getTecnica() {
		return tecnica;
	}
	/**
	 * @param tecnica the tecnica to set
	 */
	public void setTecnica(String tecnica) {
		this.tecnica = tecnica;
	}
	
	
	
	
	
	
	
}
