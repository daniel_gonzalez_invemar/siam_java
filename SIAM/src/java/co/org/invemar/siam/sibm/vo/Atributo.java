/**
 * 
 */
package co.org.invemar.siam.sibm.vo;

/**
 * @author Administrador
 *
 */
public class Atributo {
	
	private String atributo;
	private String nroInvemar;
	/**
	 * @return the nroInvemar
	 */
	public String getNroInvemar() {
		return nroInvemar;
	}

	/**
	 * @param nroInvemar the nroInvemar to set
	 */
	public void setNroInvemar(String nroInvemar) {
		this.nroInvemar = nroInvemar;
	}

	/**
	 * 
	 */
	public Atributo() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the atributo
	 */
	public String getAtributo() {
		return atributo;
	}

	/**
	 * @param atributo the atributo to set
	 */
	public void setAtributo(String atributo) {
		this.atributo = atributo;
	}
	
	
}
