/**
 * 
 */
package co.org.invemar.siam.sibm.vo;

/**
 * @author Administrador
 *
 */
public class Notas {
	
	private String notasCap;
	private String notasId;
	private String notasMuestra;
	private String notasEs;
	private String notasCr;
	private String notasCon;
	private String nroInvemar;
	/**
	 * @return the nroInvemar
	 */
	public String getNroInvemar() {
		return nroInvemar;
	}
	/**
	 * @param nroInvemar the nroInvemar to set
	 */
	public void setNroInvemar(String nroInvemar) {
		this.nroInvemar = nroInvemar;
	}
	/**
	 * 
	 */
	public Notas() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the notasCap
	 */
	public String getNotasCap() {
		return notasCap;
	}
	/**
	 * @param notasCap the notasCap to set
	 */
	public void setNotasCap(String notasCap) {
		this.notasCap = notasCap;
	}
	/**
	 * @return the notasCon
	 */
	public String getNotasCon() {
		return notasCon;
	}
	/**
	 * @param notasCon the notasCon to set
	 */
	public void setNotasCon(String notasCon) {
		this.notasCon = notasCon;
	}
	/**
	 * @return the notasCr
	 */
	public String getNotasCr() {
		return notasCr;
	}
	/**
	 * @param notasCr the notasCr to set
	 */
	public void setNotasCr(String notasCr) {
		this.notasCr = notasCr;
	}
	/**
	 * @return the notasEs
	 */
	public String getNotasEs() {
		return notasEs;
	}
	/**
	 * @param notasEs the notasEs to set
	 */
	public void setNotasEs(String notasEs) {
		this.notasEs = notasEs;
	}
	/**
	 * @return the notasId
	 */
	public String getNotasId() {
		return notasId;
	}
	/**
	 * @param notasId the notasId to set
	 */
	public void setNotasId(String notasId) {
		this.notasId = notasId;
	}
	/**
	 * @return the notasMuestra
	 */
	public String getNotasMuestra() {
		return notasMuestra;
	}
	/**
	 * @param notasMuestra the notasMuestra to set
	 */
	public void setNotasMuestra(String notasMuestra) {
		this.notasMuestra = notasMuestra;
	}
	
	

}
