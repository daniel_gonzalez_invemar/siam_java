/**
 * 
 */
package co.org.invemar.siam.sibm.vo;

import java.util.List;

/**
 * @author Administrador
 *
 */
public class Proyecto {
	private int codigo;
	private int metadato;
	private String titulo;
	private String nombreAlterno;
	private String fechaInicio;
	private String fechaFinalizo;
	private String ejecutor;
	
	private List<Localidad> estaciones;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}



	public List<Localidad> getEstaciones() {
		return estaciones;
	}

	public void setEstaciones(List<Localidad> estaciones) {
		this.estaciones = estaciones;
	}

	public String getEjecutor() {
		return ejecutor;
	}

	public void setEjecutor(String ejecutor) {
		this.ejecutor = ejecutor;
	}

	public String getFechaFinalizo() {
		return fechaFinalizo;
	}

	public void setFechaFinalizo(String fechaFinalizo) {
		this.fechaFinalizo = fechaFinalizo;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public int getMetadato() {
		return metadato;
	}

	public void setMetadato(int metadato) {
		this.metadato = metadato;
	}

	public String getNombreAlterno() {
		return nombreAlterno;
	}

	public void setNombreAlterno(String nombreAlterno) {
		this.nombreAlterno = nombreAlterno;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	
	
}
