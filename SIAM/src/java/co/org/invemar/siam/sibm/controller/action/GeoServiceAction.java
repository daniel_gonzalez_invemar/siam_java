package co.org.invemar.siam.sibm.controller.action;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;

import co.org.invemar.siam.sibm.legacy.UtilsLegacy;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

public class GeoServiceAction implements Action{
	public ActionRouter execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
				
		HashMap criterios = new HashMap();
		criterios.put("clase", "f.CLASE");
		criterios.put("orden", "f.ORDEN");
		criterios.put("familia", "f.FAMILIA");
		criterios.put("genero", "f.GENERO");
		criterios.put("especie", "f.ESPECIE");
		  
		HashMap icriterios=new HashMap();
		icriterios.put("scientificname", "f.especie");
	    icriterios.put("commonname", "f.NOMBRE_COMUN");
		icriterios.put("genus", "f.GENERO");
		 
		HashMap ioperators=new HashMap();
		ioperators.put("begin","like");
		ioperators.put("igual","=");
		ioperators.put("contains","like");
		
		HashMap fields = new HashMap();
	    fields.put("phylum", "phylum");
	    fields.put("familia", "familia");
	    fields.put("lote", "lote");
	    fields.put("especie", "especie");
	    fields.put("autores", "autores");
	    fields.put("vulgar", "vulgar");
	  
	    List columns = new ArrayList();
	    columns.add("PHYLUM");
	    columns.add("DIVISION");
	    columns.add("FAMILIA");
	    columns.add("SECCION");
	    columns.add("NOCAT");
	    columns.add("NOINV");
	    columns.add("ESPECIE");
	    columns.add("AUTOR");
	    columns.add("NOMBRE_COMUN");
	    columns.add("CLAVE");
		
		ArrayList params = new ArrayList();
		String especie = request.getParameter("nespecie");
		String genero = UtilsLegacy.param(request.getParameter("ngenero"), "");
		String division = UtilsLegacy.param(request.getParameter("phylum"), "");
		String criterioParam = UtilsLegacy.param(request.getParameter("criterio"), "");
		String criterio = (String)criterios.get(criterioParam);
		String valor = UtilsLegacy.param(request.getParameter("valor"), "");

	  //Criterios de busquedas de la pagina index.htm
		String pcriterio=UtilsLegacy.param(request.getParameter("icriterio"),"");
		String icriterio=(String)icriterios.get(pcriterio);
		String poperator=UtilsLegacy.param(request.getParameter("ioperator"),"");
		String ioperator=(String)ioperators.get(poperator);
		String ivalue=UtilsLegacy.param(request.getParameter("ivalue"),"");

	  
		String cites = UtilsLegacy.param(request.getParameter("ncites"), "");
		String librorojo = UtilsLegacy.param(request.getParameter("nlibrorojo"), "");
		String catlibrorojo = UtilsLegacy.param(request.getParameter("ncatlibrorojo"), "");
		String ano1 = UtilsLegacy.param(request.getParameter("ano1"), "");
		String ano2 = UtilsLegacy.param(request.getParameter("ano2"), "");

		String lat1 = UtilsLegacy.param(request.getParameter("lat1"), "");
		String lat2 = UtilsLegacy.param(request.getParameter("lat2"), "");
		String lng1 = UtilsLegacy.param(request.getParameter("lng1"), "");
		String lng2 = UtilsLegacy.param(request.getParameter("lng2"), "");

	  //String lat1 = request.getParameter("lat1");
	  //String lat2 = request.getParameter("lat2");
	  //String lng1 = request.getParameter("lng1");
	  //String lng2 = request.getParameter("lng2");

		String prof1 = UtilsLegacy.param(request.getParameter("nprof1"), "");
		String prof2 = UtilsLegacy.param(request.getParameter("nprof2"), "");
		String vulgar = UtilsLegacy.param(request.getParameter("vulgar2"), "");
		String ambiente = UtilsLegacy.param(request.getParameter("ambiente"), "");
		String zona = UtilsLegacy.param(request.getParameter("zona"), "");
		String region = UtilsLegacy.param(request.getParameter("nreg"), "");
		String ecoregion = UtilsLegacy.param(request.getParameter("ecoregion"), "");
		String proyecto=UtilsLegacy.param(request.getParameter("proyecto"),"");
		String estacion=UtilsLegacy.param(request.getParameter("estacion"),"");

		String otherCriteria=UtilsLegacy.param(request.getParameter("othercriterios"),"");
		String otherColumns=UtilsLegacy.param(request.getParameter("columns"),"");

		String pages=UtilsLegacy.param(request.getParameter("pages"),"");

		boolean hasEspecie = StringUtils.isNotEmpty(especie);
	  //System.out.println("hasEspecie:.. "+hasEspecie);
		boolean hasGenero = StringUtils.isNotEmpty(genero);
	  //System.out.println("hasGenero:.. "+hasGenero);
		boolean hasDivision = StringUtils.isNotEmpty(division);
		boolean hasCriterio = StringUtils.isNotEmpty(criterio);
	  //System.out.println("hasCriterio:.. "+hasCriterio);
		boolean hasValor = StringUtils.isNotEmpty(valor);

	  //verificamos que se tengan criterios de busquedas para la busqueda simple en index.htm
		boolean hasICriterio=StringUtils.isNotEmpty(icriterio);
		boolean hasIOperator=StringUtils.isNotEmpty(ioperator);
		boolean hasIValue=StringUtils.isNotEmpty(ivalue);

		boolean hasCites = StringUtils.isNotEmpty(cites);
		boolean hasLibrorojo = StringUtils.isNotEmpty(librorojo);
		boolean hasCatlibrorojo = StringUtils.isNotEmpty(catlibrorojo);
	 
		boolean hasLat1 = StringUtils.isNotEmpty(lat1);
		boolean hasLat2 = StringUtils.isNotEmpty(lat2);
		boolean hasLng1 = StringUtils.isNotEmpty(lng1);
		boolean hasLng2 = StringUtils.isNotEmpty(lng2);

		boolean hasProf1 = StringUtils.isNotEmpty(prof1);
		boolean hasProf2 = StringUtils.isNotEmpty(prof2);
		boolean hasVulgar = StringUtils.isNotEmpty(vulgar);
		boolean hasAmbiente = StringUtils.isNotEmpty(ambiente);
		boolean hasZona = StringUtils.isNotEmpty(zona);
		boolean hasRegion = StringUtils.isNotEmpty(region);
		boolean hasEcoregion = StringUtils.isNotEmpty(ecoregion);
		boolean hasProyecto=StringUtils.isNotEmpty(proyecto);
		boolean hasEstacion=StringUtils.isNotEmpty(estacion);

		boolean hasOtherCriteria=StringUtils.isNotEmpty(otherCriteria);
		boolean hasOtherColumns=StringUtils.isNotEmpty(otherColumns);

		if (hasDivision) params.add(division);

		if (hasGenero){ 
			params.add("%" + genero + "%");
			params.add("%" + genero + "%");
		}

		if (hasEspecie)
		{ 
			params.add("%" + especie + "%");
			params.add("%" + especie + "%");
		}
	  //parameter de la busqueda simple
		if(hasICriterio && hasIOperator && hasIValue){
	  	
	  		if(poperator.equalsIgnoreCase("begin")){
	  			if(!pcriterio.equalsIgnoreCase("commonname")){
	  				params.add(ivalue+"%");
	  			}
	  			
	  			params.add(ivalue+"%");
	  		}else if(poperator.equalsIgnoreCase("contains")){
	  			if(!pcriterio.equalsIgnoreCase("commonname")){
	  				params.add("%"+ivalue+"%");
	  			}
	  			params.add("%"+ivalue+"%");
	  		}else {
	  			if(!pcriterio.equalsIgnoreCase("commonname")){
	  				params.add(ivalue);
	  			}
	  			params.add(ivalue);
	  		}
		}


		if (hasCriterio && hasValor && !criterio.equalsIgnoreCase("f.clase")) {
	  			params.add("%" + valor + "%");
	  			params.add("%" + valor + "%");
		}else if (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.clase")) {
			params.add("%" + valor + "%");
		}

	  
		Pattern p = Pattern.compile(",");
		Matcher m;
		if (hasLat1)
		{
			m = p.matcher(lat1);
			lat1=m.replaceAll(".");
	  		params.add(Double.parseDouble(lat1));
		}
		if (hasLat2){ 
			m = p.matcher(lat2);
			lat2=m.replaceAll(".");
	  		params.add(Double.parseDouble(lat2));
		}
		if (hasLng1){
			m = p.matcher(lng1);
			lng1=m.replaceAll(".");
	  		params.add(Double.parseDouble(lng1));
		}
		if (hasLng2){
			m = p.matcher(lng2);
			lng2=m.replaceAll(".");
	  		params.add(Double.parseDouble(lng2));
		}
	 

		if (hasProf1) params.add(prof1);
		if (hasProf2) params.add(prof2);
		if (hasVulgar) params.add("%" + vulgar + "%");
		if (hasAmbiente) params.add(ambiente);
		if (hasZona) params.add(zona);
		if (hasEcoregion) params.add(ecoregion);
		if (hasRegion) params.add(region + "%");
		if (hasLibrorojo && !librorojo.equals("N") && hasCatlibrorojo) params.add(catlibrorojo);
		if(hasProyecto) params.add(proyecto);
		if(hasEstacion) params.add(estacion);

		String field = UtilsLegacy.param("field", "especie");
		boolean reversed = StringUtils.isNotBlank(request.getParameter("reversed"));
		if (!fields.containsKey(field)) field = "especie";
		
		//String query="SELECT f.PHYLUM AS cod_phylum, f.division AS phylum, f.FAMILIA AS familia, f.SECCION || '-' || f.NOCAT AS lote, f.NOINV AS numero, f.ESPECIE AS especie,f.basedelregistro as baseregistro, f.AUTOR AS autores, f.NOMBRE_COMUN AS vulgar, f.CLAVE AS clave, f.latitud as latitud, f.longitud as longitud "
		String query="SELECT f.clave as clave, f.ultimocambio as ultimocambio, f.codigocoleccion as coleccion, " +
					 " f.numerocatalogo as catalogo, f.especie as especie, f.reino as reino, f.division as phylum, f.PHYLUM AS cod_phylum, " +
					 " f.clase as clase, f.orden as orden, f.familia as familia, f.genero as genero, f.identificado_por as identificado, " +
					 " f.colectado_por as colectado, f.anocaptura as anyocaptura, f.depto as departamento, f.localidad as localidad, " +
					 " trunc(f.longitud,7) as longitud, trunc(f.latitud,7) as latitud, f.prof_captura as profcaptura, f.AUTOR AS autores "
					+" FROM VM_FICHAC f "
					+" where ROUND(MOD(TRUNC((NODOS_ACTIVOS/1),1),10),0) = 1  "
	    + (hasDivision? " AND  f.division = ?": "") 
	    + (hasGenero? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "") 
	    +(hasEspecie? " AND UPPER(f.EPITETO) IN (SELECT vigente FROM (SELECT NULL anterior, especie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT especie, epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
	    + (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.especie") ? " AND UPPER(replace(f.especie,' ','')) IN (SELECT UPPER(replace(vigente,' ','')) FROM ( SELECT NULL anterior, genero ||decode(subgenero, NULL,'',' ') ||subgenero ||' ' || decode(especie,NULL,'SP.',especie)||decode(subespecie, NULL,'',' ') ||subespecie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero ||' ' || especie, genero_vigente ||' ' || epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior "+ioperator +" UPPER (?) OR vigente "+ioperator +" UPPER (?)) ": "") 
	    + (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.GENERO") ? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior "+ioperator +" UPPER (?) OR vigente "+ioperator +" UPPER (?))": "") 
	    + (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.NOMBRE_COMUN") ?  " AND  UPPER(f.NOMBRE_COMUN) "+ioperator +" UPPER(?)": "") 
	    + (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.genero") ? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
	    + (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.especie") ? " AND UPPER(replace(f.especie,' ','')) IN (SELECT UPPER(replace(vigente,' ','')) FROM ( SELECT NULL anterior, genero ||decode(subgenero, NULL,'',' ') ||subgenero ||' ' || decode(especie,NULL,'SP.',especie)||decode(subespecie, NULL,'',' ') ||subespecie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero ||' ' || especie, genero_vigente ||' ' || epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?)) ": "")
	    + (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.orden") ? " AND UPPER (f.orden) IN ( SELECT vigente FROM (SELECT NULL anterior, orden vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT orden, orden_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
	    + (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.familia") ? " AND UPPER (f.familia) IN ( SELECT vigente FROM (SELECT NULL anterior, familia vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT familia, familia_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "") 
	    + (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.clase") ? " AND UPPER (f.clase) like UPPER(?)": "")  
	    + (hasLat1? " AND f.LATITUD >= ?": "") 
	    + (hasLat2? " AND f.LATITUD <= ?": "") 
	    + (hasLng1? " AND f.LONGITUD >= ?": "") 
	    + (hasLng2? " AND f.LONGITUD <= ?": "")  
	    + (hasProf1? " AND f.PROF_CAPTURA >= ?": "") 
	    + (hasProf2? " AND f.PROF_CAPTURA <= ?": "") 
	    + (hasVulgar? " AND UPPER(f.NOMBRE_COMUN) LIKE UPPER(?)": "") 
	    + (hasAmbiente? " AND f.AMBIENTE = ?": "") 
	    + (hasZona? " AND f.ZONA = ?": "") 
	    + (hasEcoregion? " AND f.ECORREGION LIKE ?": "") 
	    + (hasRegion? " AND f.REGION LIKE ?": "")
	    + (hasCites? " AND  f.CITES IS "  + (cites.equals("N")? "NULL": "NOT NULL"): "")
	    +(hasLibrorojo? " AND f.UICN_NACIONAL IS " + (librorojo.equals("N")? "NULL": "NOT NULL" + (hasCatlibrorojo? " AND substr(f.UICN_NACIONAL,1,2) = ?": "")): "")
	    + (hasProyecto? " AND f.proyecto = ?":"")
	    + (hasEstacion? " AND f.estacion=?":"")
	    + (hasOtherCriteria? " AND "+otherCriteria: "") 
	    + " ORDER BY " + fields.get(field) + " " + (reversed? "DESC": "ASC");
	   
	 // System.out.println("QUERY: "+query);
		Object[] parameters = params.toArray();
		//List list = (List)sibm.query(query, parameters, new MapListHandler(), false);
		List list = null;
		try {
			list = (List)UtilsLegacy.poolConnectBD().query(query, parameters, new MapListHandler(), false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			UtilsLegacy.poolDesconectBD();
		}
		
		
		StringBuffer xml=new StringBuffer("<?xml version='1.0' encoding=\"ISO-8859-1\" ?>");
		xml.append("<wfs:FeatureCollection xmlns:invemar=\"http://geoportal.invemar.org.co/xmlns\" xmlns:wfs=\"http://www.opengis.net/wfs\" xmlns:gml=\"http://www.opengis.net/gml\"> ");
		xml.append(" <gml:boundedBy> ");
		xml.append("  <gml:Box srsName=\"EPSG:4326\"> ");
		xml.append("   <gml:coordinates>-80.45,-4.57, -65.69,13.36</gml:coordinates> ");
		xml.append("  </gml:Box> ");
		xml.append(" </gml:boundedBy> ");
		
		StringBuffer xmlleg=new StringBuffer();
		
		List cods=new ArrayList();
		
		StringBuffer xmlfm=new StringBuffer();
		int cod=-1;
		Iterator it=list.iterator();
		while(it.hasNext()){
			Map map = (Map)it.next();
			//if(cod!=Integer.parseInt(StringUtils.defaultString(String.valueOf(map.get("cod_phylum"))))){
			if(!cods.contains(StringUtils.defaultString(String.valueOf(map.get("cod_phylum"))))){
				xmlleg.append(" <invemar:item> ");
				xmlleg.append("  <invemar:nombre>");
				xmlleg.append(StringUtils.defaultString((String)map.get("phylum")));
				xmlleg.append("  </invemar:nombre>");
				xmlleg.append("  <invemar:imagen>gsiam/images/icons/"+StringUtils.defaultString((String)map.get("phylum"))+".png</invemar:imagen>");
				xmlleg.append(" </invemar:item> ");
				//cod=Integer.parseInt(StringUtils.defaultString(String.valueOf(map.get("cod_phylum"))));
				cods.add(StringUtils.defaultString(String.valueOf(map.get("cod_phylum"))));
			}
			xmlfm.append(" <gml:featureMember> ");
			xmlfm.append("  <invemar:resultado> ");
			xmlfm.append("   <gml:boundedBy> ");
			xmlfm.append("    <gml:Box srsName=\"EPSG:4326\"> ");
			xmlfm.append("     <gml:coordinates>");
			xmlfm.append(StringUtils.defaultString(String.valueOf(map.get("longitud")))+","+StringUtils.defaultString(String.valueOf(map.get("latitud")))+" "+StringUtils.defaultString(String.valueOf(map.get("longitud")))+","+StringUtils.defaultString(String.valueOf(map.get("latitud"))));
			xmlfm.append("</gml:coordinates>");
			xmlfm.append("    </gml:Box>");
			xmlfm.append("   </gml:boundedBy>");
			xmlfm.append("   <invemar:imagen>gsiam/images/icons/"+StringUtils.defaultString((String)map.get("phylum"))+".png</invemar:imagen>");
			xmlfm.append("<invemar:texto>");
			xmlfm.append("<invemar:especie>");
			xmlfm.append(" <invemar:clave etiqueta=\"Clave:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("clave")));
			xmlfm.append(" </invemar:clave>");
			xmlfm.append(" <invemar:nombre etiqueta=\"Nombre:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("especie")));
			xmlfm.append(" </invemar:nombre>");
			xmlfm.append(" <invemar:reino etiqueta=\"Reino:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("reino")));
			xmlfm.append(" </invemar:reino>");
			xmlfm.append(" <invemar:division etiqueta=\"Phylum:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("phylum")));
			xmlfm.append(" </invemar:division>");
			xmlfm.append(" <invemar:clase etiqueta=\"Clase:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("clase")));
			xmlfm.append(" </invemar:clase>");
			xmlfm.append(" <invemar:orden etiqueta=\"Orden:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("orden")));
			xmlfm.append(" </invemar:orden>");
			xmlfm.append(" <invemar:familia etiqueta=\"Familia:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("familia")));
			xmlfm.append(" </invemar:familia>");
			xmlfm.append("</invemar:especie>");
			xmlfm.append("<invemar:general>");
			xmlfm.append(" <invemar:ultimoCambio etiqueta=\"�ltimo Cambio:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("ultimocambio")));
			xmlfm.append(" </invemar:ultimoCambio>");
			xmlfm.append(" <invemar:codigoColeccion etiqueta=\"Codigo Colecci�n:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("codigocoleccion")));
			xmlfm.append(" </invemar:codigoColeccion>");
			xmlfm.append(" <invemar:numeroCatalogo etiqueta=\"N�mero Catalogo:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("catalogo")));
			xmlfm.append(" </invemar:numeroCatalogo>");
			xmlfm.append(" <invemar:anyoCaptura etiqueta=\"A�o Captura:\">");
			xmlfm.append(StringUtils.defaultString(String.valueOf(map.get("anyocaptura"))));
			xmlfm.append(" </invemar:anyoCaptura>");
			xmlfm.append(" <invemar:profCaptura etiqueta=\"Prof. Captura:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("profcaptura")));
			xmlfm.append(" </invemar:profCaptura>");
			xmlfm.append(" <invemar:lugar etiqueta=\"Lugar:\">");
			xmlfm.append(StringUtils.defaultString(String.valueOf(map.get("localidad"))));
			xmlfm.append(" </invemar:lugar>");
			xmlfm.append(" <invemar:latitud etiqueta=\"Latitud:\">");
			xmlfm.append(StringUtils.defaultString(String.valueOf(map.get("latitud"))));
			xmlfm.append(" </invemar:latitud>");
			xmlfm.append(" <invemar:longitud etiqueta=\"Longitud:\">");
			xmlfm.append(StringUtils.defaultString(String.valueOf(map.get("longitud"))));
			xmlfm.append(" </invemar:longitud>");
			xmlfm.append("</invemar:general>");
			xmlfm.append("<invemar:investigador>");
			xmlfm.append(" <invemar:identificadoPor etiqueta=\"Identificado por:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("identificado")));
			xmlfm.append(" </invemar:identificadoPor>");
			xmlfm.append(" <invemar:colectadoPor etiqueta=\"Colectado por:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("colectado")));
			xmlfm.append(" </invemar:colectadoPor>");
			xmlfm.append("</invemar:investigador>");
			xmlfm.append("</invemar:texto>");
			xmlfm.append("<invemar:msGeometry> ");
			xmlfm.append("    <gml:Point srsName=\"EPSG:4326\">");
			xmlfm.append("	 <gml:coordinates>");
			xmlfm.append(""+StringUtils.defaultString(String.valueOf(map.get("longitud")))+","+StringUtils.defaultString(String.valueOf(map.get("latitud"))));
			xmlfm.append("</gml:coordinates>");
			xmlfm.append("    </gml:Point>");
			xmlfm.append("   </invemar:msGeometry>");
			xmlfm.append("  </invemar:resultado>");
			xmlfm.append(" </gml:featureMember>  ");
			
			//System.out.println("Phylum: "+StringUtils.defaultString((String)map.get("phylum")));
			//System.out.println("especie: "+StringUtils.defaultString((String)map.get("especie")));
			//System.out.println("autores: "+StringUtils.defaultString((String)map.get("autores")));
			
			
		}
		
		xml.append(" <gml:featureMember>");
		xml.append("  <invemar:resultado>");
		xml.append("	<invemar:nombre>REGISTROS BIOLOGICOS</invemar:nombre>  ");
		xml.append("	<invemar:titulo>REGISTROS BIOLOGICOS</invemar:titulo>");
		xml.append("    <invemar:leyenda> ");
		xml.append(xmlleg);
		xml.append(" 	</invemar:leyenda>");
		xml.append("  </invemar:resultado>");
		xml.append(" </gml:featureMember>");
		
		xml.append(xmlfm.toString());
		
		xml.append("</wfs:FeatureCollection>");
		//System.out.println("return XML: ");
		//System.out.println(xml.toString());
		
		response.setContentType("text/text; charset=ISO-8859-1");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(xml.toString());
	  return null;
		
		
	}
	
	private static int searchCP(int v[],int data){
		
		int N=v.length;
		int pos=-1;
		
		for(int i=0;(i<N) && (pos==-1);i++){
			if(v[i]==data){
				pos=i;
			}
		}
		return pos;
	}
	

}
