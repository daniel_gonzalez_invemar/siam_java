package co.org.invemar.siam.sibm.controller.action;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;

import co.org.invemar.siam.sibm.legacy.UtilsLegacy;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

public class CamGeoServiceAction implements Action{
	public ActionRouter execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
				
	  //Criterios de busquedas de la pagina index.htm
		String nvariable = UtilsLegacy.param(request.getParameter("nvariable"), "");
		String ano = UtilsLegacy.param(request.getParameter("ano"), "");
		String ntemporada = UtilsLegacy.param(request.getParameter("ntemporada"), "");

		//String pages=UtilsLegacy.param(request.getParameter("pages"),"");

	  //verificamos que se tengan criterios de busquedas para la busqueda simple en index.htm

		boolean hasNvariable = StringUtils.isNotEmpty(nvariable);
		boolean hasAno = StringUtils.isNotEmpty(ano);
		boolean hasNtemporada = StringUtils.isNotEmpty(ntemporada);
	 
 
		Pattern p = Pattern.compile(",");
		Matcher m;
	 
		ArrayList params = new ArrayList();
		if (hasNvariable) params.add(nvariable);
		if (hasAno) params.add(ano);
		if (hasNtemporada) params.add(ntemporada);

		boolean reversed = StringUtils.isNotBlank(request.getParameter("reversed"));
		
		//String query="SELECT f.PHYLUM AS cod_phylum, f.division AS phylum, f.FAMILIA AS familia, f.SECCION || '-' || f.NOCAT AS lote, f.NOINV AS numero, f.ESPECIE AS especie,f.basedelregistro as baseregistro, f.AUTOR AS autores, f.NOMBRE_COMUN AS vulgar, f.CLAVE AS clave, f.latitud as latitud, f.longitud as longitud "

		String query="select * from cam_admon.VISTA_CAMGEOSERVICE e where clase is not null"

/*
"SELECT a.latitud_st latitud, a.longitud_st longitud, e.codest, e.nomest, e.depto, e.ano, e.temporada muestreo, e.variable, e.prom, e.unidad, e.tipo_sustrato, e.sustrato, CAM_ADMON.cam_api.fclases_var(e.variable, e.tipo_sustrato, e.prom) clase "
		+ "FROM CAM_ADMON.aestadisticas e, CAM_ADMON.aestacion a WHERE (e.nivel = 1 AND a.vigencia_st = 'S') AND e.codest = a.codigo_st "
		+ "AND e.prj = 'REDCAM' AND (ano >= 2001) AND (   e.VARIABLE = 'TEM' "
		+ "OR e.VARIABLE = 'SAL' OR e.VARIABLE = 'OD' OR e.VARIABLE = 'PH' OR e.VARIABLE = 'NH4' OR e.VARIABLE = 'NO2' OR e.VARIABLE = 'NO3' OR e.VARIABLE = 'PO4' "
		+ "OR e.VARIABLE = 'SST' OR e.VARIABLE = 'CTE' OR e.VARIABLE = 'CTT' OR e.VARIABLE = 'HDD' OR e.VARIABLE = 'OCT' OR e.VARIABLE = 'CD' OR e.VARIABLE = 'CR' "
		+ "OR e.VARIABLE = 'PB' OR e.VARIABLE = 'EFE' ) AND e.tipo_sustrato = 'SA' AND e.profundidad = 'S' and CAM_ADMON.cam_api.fclases_var(e.variable, e.tipo_sustrato, e.prom) is not null"					
*/
		
	    + (hasNvariable? " AND  e.variable = ?": "") 
	    + (hasAno? " AND  e.ano = ?": "") 
	    + (hasNtemporada? " AND  e.muestreo = ?": "") ;

	 // System.out.println("QUERY: "+query);
		Object[] parameters = params.toArray();
		//List list = (List)sibm.query(query, parameters, new MapListHandler(), false);
		List list = null;
		try {
			list = (List)UtilsLegacy.poolConnectBD().query(query, parameters, new MapListHandler(), false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			UtilsLegacy.poolDesconectBD();
		}
		
		
		StringBuffer xml=new StringBuffer("<?xml version='1.0' encoding=\"ISO-8859-1\" ?>");
		xml.append("<wfs:FeatureCollection xmlns:invemar=\"http://geoportal.invemar.org.co/xmlns\" xmlns:wfs=\"http://www.opengis.net/wfs\" xmlns:gml=\"http://www.opengis.net/gml\"> ");
		xml.append(" <gml:boundedBy> ");
		xml.append("  <gml:Box srsName=\"EPSG:4326\"> ");
		xml.append("   <gml:coordinates>-80.45,-4.57, -65.69,13.36</gml:coordinates> ");
		xml.append("  </gml:Box> ");
		xml.append(" </gml:boundedBy> ");
		
		StringBuffer xmlleg=new StringBuffer();
		
		List cods=new ArrayList();
		
		StringBuffer xmlfm=new StringBuffer();
		int cod=-1;
		Iterator it=list.iterator();
		while(it.hasNext()){
			Map map = (Map)it.next();
			//if(cod!=Integer.parseInt(StringUtils.defaultString(String.valueOf(map.get("cod_phylum"))))){

			// esta es la leyenda
			if(!cods.contains(StringUtils.defaultString(String.valueOf(map.get("clase"))))){
				xmlleg.append(" <invemar:item> ");
				xmlleg.append("  <invemar:nombre>");

			// esta es la etiqueta de leyenda
				xmlleg.append(StringUtils.defaultString(String.valueOf(map.get("label"))));
				xmlleg.append("  </invemar:nombre>");

			// esta es la simbolog�a de la leyenda
				xmlleg.append("  <invemar:imagen>gsiam/images/icons/"+StringUtils.defaultString(String.valueOf(map.get("clase")))+".png</invemar:imagen>");
				xmlleg.append(" </invemar:item> ");
				//cod=Integer.parseInt(StringUtils.defaultString(String.valueOf(map.get("cod_phylum"))));
				cods.add(StringUtils.defaultString(String.valueOf(map.get("clase"))));
			}


			// en este bloque es donde se pintan los puntos
			xmlfm.append(" <gml:featureMember> ");
			xmlfm.append("  <invemar:resultado> ");
			xmlfm.append("   <gml:boundedBy> ");
			xmlfm.append("    <gml:Box srsName=\"EPSG:4326\"> ");
			xmlfm.append("     <gml:coordinates>");
			xmlfm.append(StringUtils.defaultString(String.valueOf(map.get("longitud")))+","+StringUtils.defaultString(String.valueOf(map.get("latitud")))+" "+StringUtils.defaultString(String.valueOf(map.get("longitud")))+","+StringUtils.defaultString(String.valueOf(map.get("latitud"))));
			xmlfm.append("</gml:coordinates>");
			xmlfm.append("    </gml:Box>");
			xmlfm.append("   </gml:boundedBy>");
			xmlfm.append("   <invemar:imagen>gsiam/images/icons/"+StringUtils.defaultString(String.valueOf(map.get("clase")))+".png</invemar:imagen>");
			xmlfm.append("<invemar:texto>");

			xmlfm.append("<invemar:general>");
			xmlfm.append("<invemar:estacion etiqueta=\"Estacion:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("codest"))+" - "+StringUtils.defaultString((String)map.get("nomest")));
			xmlfm.append("</invemar:estacion>");
			xmlfm.append("<invemar:depto etiqueta=\"Departamento:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("depto")));
			xmlfm.append("</invemar:depto>");

			xmlfm.append("<invemar:year etiqueta=\"A�o:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("ano")));
			xmlfm.append("</invemar:year>");

			xmlfm.append("<invemar:muestreo etiqueta=\"Muestreo:\">");
			xmlfm.append(StringUtils.defaultString((String)map.get("muestreo")));
			xmlfm.append("</invemar:muestreo>");
			
			xmlfm.append("<invemar:variable etiqueta=\"Variable:\">");
			xmlfm.append(StringUtils.trim(StringUtils.defaultString((String)map.get("variable"))));
			xmlfm.append("</invemar:variable>");

			xmlfm.append("<invemar:valor etiqueta=\"Valor:\">");
			xmlfm.append(StringUtils.defaultString(String.valueOf(map.get("prom"))));
			xmlfm.append("</invemar:valor>");
			
			xmlfm.append("<invemar:unidad etiqueta=\"Unidad:\">");
			xmlfm.append(StringUtils.defaultString(String.valueOf(map.get("unidad"))));
			xmlfm.append("</invemar:unidad>");

			xmlfm.append("<invemar:sustrato etiqueta=\"Sustrato:\">");
			xmlfm.append(StringUtils.defaultString(String.valueOf(map.get("sustrato"))));
			xmlfm.append("</invemar:sustrato>");
			
			xmlfm.append("</invemar:general>");
			xmlfm.append("</invemar:texto>");

			xmlfm.append("<invemar:msGeometry> ");
			xmlfm.append("    <gml:Point srsName=\"EPSG:4326\">");
			xmlfm.append("	 <gml:coordinates>");
			xmlfm.append(""+StringUtils.defaultString(String.valueOf(map.get("longitud")))+","+StringUtils.defaultString(String.valueOf(map.get("latitud"))));
			xmlfm.append("</gml:coordinates>");
			xmlfm.append("    </gml:Point>");
			xmlfm.append("   </invemar:msGeometry>");
			xmlfm.append("  </invemar:resultado>");
			xmlfm.append(" </gml:featureMember>  ");
			
			//System.out.println("Phylum: "+StringUtils.defaultString((String)map.get("phylum")));
			//System.out.println("especie: "+StringUtils.defaultString((String)map.get("especie")));
			//System.out.println("autores: "+StringUtils.defaultString((String)map.get("autores")));
			
			
		}
		
		xml.append(" <gml:featureMember>");
		xml.append("  <invemar:resultado>");
		xml.append("	<invemar:nombre>RedCAM</invemar:nombre>  ");
		xml.append("	<invemar:titulo>RedCAM</invemar:titulo>");
		xml.append("    <invemar:leyenda> ");
		xml.append(xmlleg);
		xml.append(" 	</invemar:leyenda>");
		xml.append("  </invemar:resultado>");
		xml.append(" </gml:featureMember>");
		
		xml.append(xmlfm.toString());
		
		xml.append("</wfs:FeatureCollection>");
		//System.out.println("return XML: "); 
		//System.out.println(xml.toString());
		
		response.setContentType("text/text; charset=ISO-8859-1");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(xml.toString());
	  return null;
		
		
	}
	
	private static int searchCP(int v[],int data){
		
		int N=v.length;
		int pos=-1;
		
		for(int i=0;(i<N) && (pos==-1);i++){
			if(v[i]==data){
				pos=i;
			}
		}
		return pos;
	}
	

}
