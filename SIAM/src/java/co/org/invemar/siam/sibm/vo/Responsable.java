/**
 * 
 */
package co.org.invemar.siam.sibm.vo;

/**
 * @author Administrador
 *
 */
public class Responsable {
	  
     private String nroInvemar;
	 private String nombre;
	 private String seccion;
	 private String fecha;
	 private String tarea;
	 private String orden;
	/**
	 * 
	 */
	public Responsable() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param nroInvemar
	 * @param nombre
	 * @param seccion
	 * @param fecha
	 * @param tarea
	 * @param orden
	 */
	public Responsable(String nroInvemar, String nombre, String seccion, String fecha, String tarea, String orden) {
		this.nroInvemar = nroInvemar;
		this.nombre = nombre;
		this.seccion = seccion;
		this.fecha = fecha;
		this.tarea = tarea;
		this.orden = orden;
	}
	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the nroInvemar
	 */
	public String getNroInvemar() {
		return nroInvemar;
	}
	/**
	 * @param nroInvemar the nroInvemar to set
	 */
	public void setNroInvemar(String nroInvemar) {
		this.nroInvemar = nroInvemar;
	}
	/**
	 * @return the orden
	 */
	public String getOrden() {
		return orden;
	}
	/**
	 * @param orden the orden to set
	 */
	public void setOrden(String orden) {
		this.orden = orden;
	}
	/**
	 * @return the seccion
	 */
	public String getSeccion() {
		return seccion;
	}
	/**
	 * @param seccion the seccion to set
	 */
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	/**
	 * @return the tarea
	 */
	public String getTarea() {
		return tarea;
	}
	/**
	 * @param tarea the tarea to set
	 */
	public void setTarea(String tarea) {
		this.tarea = tarea;
	}
	 
	 

}
