/**
 * 
 */
package co.org.invemar.siam.sibm.model;

import java.sql.Connection;
import java.sql.SQLException;

import co.org.invemar.sipein.util.ConnectionFactory;

/**
 * @author Administrador
 *
 */
public class CountFacade {
	ConnectionFactory cFactory = null;
	Connection connection = null;
	FichaCatalogoDM fc=null;
	ProyectosDM pc=null;
	
	public int countProyectos(){
		pc=new ProyectosDM();
		cFactory = new ConnectionFactory();
		int rtn=0;
		try {
			connection=cFactory.createConnection();
			rtn=pc.count(connection);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		return rtn;
	}
	
	public int countDiagnosis(String nroInvemar){
		fc=new FichaCatalogoDM();
		cFactory = new ConnectionFactory();
		int rtn=0;
		try {
			connection=cFactory.createConnection();
			rtn=fc.countDiagnosis(connection, nroInvemar);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		return rtn;
	}
	
	public int countImages(String nroInvemar)throws SQLException{
		fc=new FichaCatalogoDM();
		cFactory = new ConnectionFactory();
		int rtn=0;
		try {
			connection=cFactory.createConnection();
			rtn=fc.countImages(connection, nroInvemar);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		return rtn;
	}
	public int countAutores(String nroInvemar)throws SQLException{
		fc=new FichaCatalogoDM();
		cFactory = new ConnectionFactory();
		int rtn=0;
		try {
			connection=cFactory.createConnection();
			rtn=fc.countAutores(connection, nroInvemar);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		return rtn;
	}
	public int countObjetoRelacionados(String nroInvemar){
		fc=new FichaCatalogoDM();
		cFactory = new ConnectionFactory();
		int rtn=0;
		try {
			connection=cFactory.createConnection();
			rtn=fc.countObjetoRelacionados(connection, nroInvemar);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		return rtn;
	}
	
	public int countNomenclaturas(String nroInvemar){
		fc=new FichaCatalogoDM();
		cFactory = new ConnectionFactory();
		int rtn=0;
		try {
			connection=cFactory.createConnection();
			rtn=fc.countNomenclaturas(connection, nroInvemar);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		return rtn;
	}
	
	public int countResponsables(String nroInvemar){
		fc=new FichaCatalogoDM();
		cFactory = new ConnectionFactory();
		int rtn=0;
		try {
			connection=cFactory.createConnection();
			rtn=fc.countResponsables(connection, nroInvemar);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		return rtn;
	}
	
	public int countAtributos(String nroInvemar){
		fc=new FichaCatalogoDM();
		cFactory = new ConnectionFactory();
		int rtn=0;
		try {
			connection=cFactory.createConnection();
			rtn=fc.countAtributos(connection, nroInvemar);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		return rtn;
	}
	public int countNotas(String nroInvemar){
		fc=new FichaCatalogoDM();
		cFactory = new ConnectionFactory();
		int rtn=0;
		try {
			connection=cFactory.createConnection();
			rtn=fc.countNotas(connection, nroInvemar);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		return rtn;
	}
	
}
