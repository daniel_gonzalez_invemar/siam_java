/**
 * 
 */
package co.org.invemar.siam.sibm.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.org.invemar.siam.sibm.vo.Localidad;
import co.org.invemar.siam.sibm.vo.Proyecto;

/**
 * @author Administrador
 *
 */
public class ProyectosDM {
	
	public int count(Connection connection)throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		int rtn=0;
		String SQL="  select count(*) from clst_proyectos ";
		try{
			pstmt = connection.prepareStatement(SQL);
			
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				rtn=rs.getInt(1);
			}
			
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		return rtn;
	}
	
	public List<Proyecto> findProjects(Connection connection) throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		List<Proyecto> projects=null;
		String query=" SELECT pro.codigo, titulo, nombre_alterno,pro.ejecutor, "
				   + " finicio || ' - ' || ffinalizo periodo_ejecucion, pro.id_metadato_pro "
				   + " FROM clst_proyectos pro "
				   + " order by nombre_alterno";
		/*String query=" SELECT pro.codigo, pro.titulo, pro.nombre_alterno,pro.ejecutor, "
			   + " pro.finicio || ' - ' || pro.ffinalizo periodo_ejecucion "
			   + " FROM clst_proyectos pro "
			   + " order by nombre_alterno";*/
		
		try{
			pstmt = connection.prepareStatement(query);
			
			rs = pstmt.executeQuery();
			projects=new ArrayList<Proyecto>();
			Proyecto project=null;
			if(!rs.next()){
				return null;
			}
			
			do{
				project=new Proyecto();
				project.setCodigo(rs.getInt("codigo"));
				project.setTitulo(nullValue(rs.getString("titulo")));
				project.setEjecutor(nullValue(rs.getString("ejecutor")));
				project.setNombreAlterno(nullValue(rs.getString("nombre_alterno")));
				project.setFechaInicio(nullValue(rs.getString("periodo_ejecucion")));
				//proyecto.setFechaFinalizo(rs.getString(""));
				project.setMetadato(rs.getInt("id_metadato_pro"));
				
				projects.add(project);
			}while(rs.next());
			
		}
		finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		return projects;
		
	}
	public List<Proyecto> findProjectsByCodigoFuente(Connection connection, String fuenteDatos) throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		List<Proyecto> projects=null;
		String query=" SELECT pro.codigo, titulo, nombre_alterno,pro.ejecutor, "
				   + " finicio || ' - ' || ffinalizo periodo_ejecucion, pro.id_metadato_pro "
				   + " FROM clst_proyectos pro "
				   + " WHERE bitand(pro.sistema,?) > 0 "
				   + " order by nombre_alterno";
		/*String query=" SELECT pro.codigo, pro.titulo, pro.nombre_alterno,pro.ejecutor, "
			   + " pro.finicio || ' - ' || pro.ffinalizo periodo_ejecucion "
			   + " FROM clst_proyectos pro "
			   + " order by nombre_alterno";*/
		
		try{
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1,fuenteDatos );
			rs = pstmt.executeQuery();
			projects=new ArrayList<Proyecto>();
			Proyecto project=null;
			if(!rs.next()){
				return null;
			}
			
			do{
				project=new Proyecto();
				project.setCodigo(rs.getInt("codigo"));
				project.setTitulo(nullValue(rs.getString("titulo")));
				project.setEjecutor(nullValue(rs.getString("ejecutor")));
				project.setNombreAlterno(nullValue(rs.getString("nombre_alterno")));
				project.setFechaInicio(nullValue(rs.getString("periodo_ejecucion")));
				//proyecto.setFechaFinalizo(rs.getString(""));
				project.setMetadato(rs.getInt("id_metadato_pro"));
				
				projects.add(project);
			}while(rs.next());
			
		}
		finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		return projects;
		
	}
	public List<Localidad> findStationByProject(Connection connection,int proy) throws SQLException{
		List<Localidad> estaciones=null;
		PreparedStatement pstmt = null;
		Localidad estacion=null;
		ResultSet rs=null;
		String query=" SELECT loc.codigo_estacion_loc, loc.descripcion_estacion_loc, loc.pais_loc, "
					+" loc.marrio_loc, loc.salodulce_loc, loc.lugar, "
					+" TRUNC(loc.latitudinicio_loc,8) as latitudinicio_loc, TRUNC(loc.latitudfin_loc,8) as latitudfin_loc, " 
					+" GEOGRAFICOS.FLATITUD(TRUNC(loc.latitudinicio_loc,8)) as latitudiniciogms, GEOGRAFICOS.FLATITUD(TRUNC(loc.latitudfin_loc,8))  as latitudfingms, "
					+ "TRUNC(loc.longitudinicio_loc,8) as longitudinicio_loc, TRUNC(loc.longitudfin_loc,8) as longitudfin_loc,  " 
					+" GEOGRAFICOS.FLONGITUD(TRUNC(loc.longitudinicio_loc,8)) as longitudiniciogms , GEOGRAFICOS.FLONGITUD(TRUNC(loc.longitudfin_loc,8)) as longitudfingms, " 
					+" loc.cuerpo_agua_loc, loc.distanciacosta_loc, " 
					+" loc.tipocosta_loc, loc.hondoagua_loc,zn.descripcion zonaprotegida , " 
					+" top.toponimo_texto toponimo, loc.prefijo_cdg_est_loc, loc.barco_loc, " 
					+" loc.campana_loc, sus.descripcion_lov sustrato, " 
					+" amb.descripcion_lov ambiente, loc.notas_loc, ec.descripcion ecorregion, " 
					+" loc.prof_min_loc, loc.prof_max_loc, loc.vigente_loc " 
					+" FROM  vestaciones_siam loc, clst_ecorregion ec, codigoslov sus, codigoslov amb, " 
					+" clst_toponimia top, " 
					+" cmlst_zonas_protegidas zn " 
					+" WHERE loc.proyecto_loc=? "
					+" AND  ec.consecutivo(+) = loc.ecorregion_loc " 
					+" AND sus.codigo_lov(+) = loc.sustrato_loc " 
					+" AND sus.tabla_lov(+) = 8 " 
					+" AND amb.codigo_lov(+) = loc.ambiente_loc "
					+" AND amb.tabla_lov(+) = 7 " 
					+" AND top.toponimo_tp(+) = loc.toponimia_loc " 
					+" AND zn.codigo(+)=loc.zona_protegida " 
					+" order by loc.prefijo_cdg_est_loc asc, loc.codigo_estacion_loc asc "; 
		
		try{
			pstmt = connection.prepareStatement(query);
			pstmt.setInt(1, proy);
			rs = pstmt.executeQuery();
		
			estaciones=new ArrayList<Localidad>();
			if(!rs.next()){
				return null;
			}
			do{
				estacion=new Localidad();
//				estacion.setLatitudInicio(rs.getString("latitudinicio_loc"));
//				estacion.setLatitudFin(rs.getString("latitudfin_loc"));
//				estacion.setLongitudInicio(rs.getString("longitudinicio_loc"));
//				estacion.setLongitudFin(rs.getString("longitudfin_loc"));
//				estacion.setLugar(rs.getString("lugar"));
//				estacion.setCodigoEstacion(nullValue(rs.getString("codigo_estacion_loc")));
//				estacion.setPrefijoEstacion(nullValue(rs.getString("prefijo_cdg_est_loc")));
				
				estacion.setCodigoEstacion(nullValue(rs.getString("codigo_estacion_loc")));
				estacion.setDescripcionEstacion(nullValue(rs.getString("descripcion_estacion_loc")));
				estacion.setPais(nullValue(rs.getString("pais_loc")));
				estacion.setMarrio(nullValue(rs.getString("marrio_loc")));
				estacion.setSalodulce(nullValue(rs.getString("salodulce_loc")));
				estacion.setLugar(nullValue(rs.getString("lugar")));
				estacion.setLatitudInicio(nullValue(rs.getString("latitudinicio_loc")));
				estacion.setLatitudInicioGMS(nullValue(rs.getString("latitudiniciogms")));
				estacion.setLatitudFin(nullValue(rs.getString("latitudfin_loc")));
				estacion.setLatitudFinGMS(nullValue(rs.getString("latitudfingms")));
				estacion.setLongitudInicio(nullValue(rs.getString("longitudinicio_loc")));
				estacion.setLongitudInicioGMS(nullValue(rs.getString("longitudiniciogms")));
				estacion.setLongitudFin(nullValue(rs.getString("longitudfin_loc")));
				estacion.setLongitudFinGMS(nullValue(rs.getString("longitudfingms")));
				estacion.setCuerpoAgua(nullValue(rs.getString("cuerpo_agua_loc")));
				estacion.setDistanciaCosta(nullValue(rs.getString("distanciacosta_loc")));
				estacion.setTipoCosta(nullValue(rs.getString("tipocosta_loc")));
				estacion.setHondoAgua(nullValue(rs.getString("hondoagua_loc")));
				estacion.setZonaProtegida(nullValue(rs.getString("zonaprotegida")));
				estacion.setToponimo(nullValue(rs.getString("toponimo")));
				estacion.setPrefijoEstacion(nullValue(rs.getString("prefijo_cdg_est_loc")));
				estacion.setBarco(nullValue(rs.getString("barco_loc")));
				estacion.setCampana(nullValue(rs.getString("campana_loc")));
				estacion.setSustrato(nullValue(rs.getString("sustrato")));
				estacion.setAmbiente(nullValue(rs.getString("ambiente")));
				estacion.setNotas(nullValue(rs.getString("notas_loc")));
				estacion.setEcorregion(nullValue(rs.getString("ecorregion")));
				estacion.setProfMin(nullValue(rs.getString("prof_min_loc")));
				estacion.setProfMax(nullValue(rs.getString("prof_max_loc")));
				estacion.setVigente(nullValue(rs.getString("vigente_loc")));
				estaciones.add(estacion);
			}while(rs.next());
		}
		finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}	
		return estaciones;
	}
	public List<Localidad> findEstationsByPoryect(Connection connection,int proy) throws SQLException{
		List<Localidad> estaciones=null;
		PreparedStatement pstmt = null;
		Localidad estacion=null;
		ResultSet rs=null;
		String query=" SELECT prefijo_cdg_est_loc, codigo_estacion_loc,descripcion_estacion_loc,lugar, TRUNC(latitudinicio_loc,8) latitudinicio_loc,TRUNC(latitudfin_loc,8) latitudfin_loc , TRUNC(longitudinicio_loc,8) longitudinicio_loc, TRUNC(longitudfin_loc,8) longitudfin_loc, vigente_loc "+
					 " FROM vestaciones_siam "+
					 " where proyecto_loc=?"; 
		
		try{
			pstmt = connection.prepareStatement(query);
			pstmt.setInt(1, proy);
			rs = pstmt.executeQuery();
		
			estaciones=new ArrayList<Localidad>();
			if(!rs.next()){
				return null;
			}
			do{
				estacion=new Localidad();
				estacion.setLatitudInicio(rs.getString("latitudinicio_loc"));
				estacion.setLatitudFin(rs.getString("latitudfin_loc"));
				estacion.setLongitudInicio(rs.getString("longitudinicio_loc"));
				estacion.setLongitudFin(rs.getString("longitudfin_loc"));
				estacion.setLugar(rs.getString("lugar"));
				estacion.setCodigoEstacion(nullValue(rs.getString("codigo_estacion_loc")));
				estacion.setPrefijoEstacion(nullValue(rs.getString("prefijo_cdg_est_loc")));
				estacion.setVigente(nullValue(rs.getString("vigente_loc")));
				estaciones.add(estacion);
			}while(rs.next());
		}
		finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}	
		return estaciones;
	}
	
	public List<Proyecto> findProyects(Connection connection, int begin, int length) throws SQLException{
		
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		List<Proyecto> proyectos=null;
		List<Proyecto> rtnProyectos=null;
		Proyecto proyecto=null;
		List<Localidad> estaciones=null;
		Localidad estacion=null;
		String query=" SELECT pro.codigo, titulo, nombre_alterno,pro.ejecutor, "+
					" finicio || ' - ' || ffinalizo periodo_ejecucion, id_metadato_pro, "+
					" loc.codigo_estacion_loc, loc.descripcion_estacion_loc, loc.pais_loc, "+
					" loc.marrio_loc, loc.salodulce_loc, loc.lugar, "+
					" TRUNC(loc.latitudinicio_loc,8) as latitudinicio_loc, TRUNC(loc.latitudfin_loc,8) as latitudfin_loc, TRUNC(loc.longitudinicio_loc,8) as longitudinicio_loc, "+
					" TRUNC(loc.longitudfin_loc,8) as longitudfin_loc, loc.cuerpo_agua_loc, loc.distanciacosta_loc, "+
					" loc.tipocosta_loc, loc.hondoagua_loc,zn.descripcion zonaprotegida , "+
					" top.toponimo_texto toponimo, loc.prefijo_cdg_est_loc, loc.barco_loc, "+
					" loc.campana_loc, sus.descripcion_lov sustrato, "+
					" amb.descripcion_lov ambiente, loc.notas_loc, ec.descripcion ecorregion, "+
					" loc.prof_min_loc, loc.prof_max_loc "+
					" FROM clst_proyectos pro, "+
					" vestaciones_siam loc, "+
					" clst_ecorregion ec, "+
					" codigoslov sus, "+
					" codigoslov amb, "+
					" clst_toponimia top, "+	
					" cmlst_zonas_protegidas zn "+
					" WHERE pro.codigo = loc.proyecto_loc "+
					" AND ec.consecutivo(+) = loc.ecorregion_loc "+
					" AND sus.codigo_lov(+) = loc.sustrato_loc "+
					" AND sus.tabla_lov(+) = 8 "+
					" AND amb.codigo_lov(+) = loc.ambiente_loc "+
					" AND amb.tabla_lov(+) = 7 "+
					" AND top.toponimo_tp(+) = loc.toponimia_loc "+
					" and zn.codigo(+)=loc.zona_protegida "+
					" order by pro.nombre_alterno asc, loc.prefijo_cdg_est_loc asc, loc.codigo_estacion_loc asc";
			
		//String sqlPagin="SELECT * FROM (SELECT a.*, ROWNUM AS LIMIT FROM (" + query + ") a) WHERE LIMIT BETWEEN " + begin + " AND " + (begin + length - 1);
		//System.out.println("sqlPagin: "+sqlPagin);
		try{
			pstmt = connection.prepareStatement(query);
			rs = pstmt.executeQuery();
			int oldCodigo=-99999999;
			int newCodigo;
			int i=0;
			int log=0;
			proyectos=new ArrayList<Proyecto>();
			
			if(!rs.next()){
				return null;
			}
			
			do{
				
				newCodigo=rs.getInt("codigo");
				if(newCodigo!=oldCodigo){
					//System.out.println("log "+log);
					oldCodigo=newCodigo;
					
					proyecto=new Proyecto();
					proyecto.setCodigo(newCodigo);
					proyecto.setTitulo(nullValue(rs.getString("titulo")));
					proyecto.setEjecutor(nullValue(rs.getString("ejecutor")));
					proyecto.setNombreAlterno(nullValue(rs.getString("nombre_alterno")));
					proyecto.setFechaInicio(nullValue(rs.getString("periodo_ejecucion")));
					//proyecto.setFechaFinalizo(rs.getString(""));
					proyecto.setMetadato(rs.getInt("id_metadato_pro"));
					
					proyectos.add(proyecto);
					
					estacion=new Localidad();
					estacion.setCodigoEstacion(nullValue(rs.getString("codigo_estacion_loc")));
					estacion.setDescripcionEstacion(nullValue(rs.getString("descripcion_estacion_loc")));
					estacion.setPais(nullValue(rs.getString("pais_loc")));
					estacion.setMarrio(nullValue(rs.getString("marrio_loc")));
					estacion.setSalodulce(nullValue(rs.getString("salodulce_loc")));
					estacion.setLugar(nullValue(rs.getString("lugar")));
					estacion.setLatitudInicio(nullValue(rs.getString("latitudinicio_loc")));
					estacion.setLatitudFin(nullValue(rs.getString("latitudfin_loc")));
					estacion.setLongitudInicio(nullValue(rs.getString("longitudinicio_loc")));
					estacion.setLongitudFin(nullValue(rs.getString("longitudfin_loc")));
					estacion.setCuerpoAgua(nullValue(rs.getString("cuerpo_agua_loc")));
					estacion.setDistanciaCosta(nullValue(rs.getString("distanciacosta_loc")));
					estacion.setTipoCosta(nullValue(rs.getString("tipocosta_loc")));
					estacion.setHondoAgua(nullValue(rs.getString("hondoagua_loc")));
					estacion.setZonaProtegida(nullValue(rs.getString("zonaprotegida")));
					estacion.setToponimo(nullValue(rs.getString("toponimo")));
					estacion.setPrefijoEstacion(nullValue(rs.getString("prefijo_cdg_est_loc")));
					estacion.setBarco(nullValue(rs.getString("barco_loc")));
					estacion.setCampana(nullValue(rs.getString("campana_loc")));
					estacion.setSustrato(nullValue(rs.getString("sustrato")));
					estacion.setAmbiente(nullValue(rs.getString("ambiente")));
					estacion.setNotas(nullValue(rs.getString("notas_loc")));
					estacion.setEcorregion(nullValue(rs.getString("ecorregion")));
					estacion.setProfMin(nullValue(rs.getString("prof_min_loc")));
					estacion.setProfMax(nullValue(rs.getString("prof_max_loc")));
					
					
					
					
					if(estaciones!=null){
						//System.out.println("pesta length: "+estaciones.size());
						
						proyectos.get(i).setEstaciones(estaciones);
						
						//System.out.println("i: "+i);
						i++;
						estaciones=null;
					}
					if(estaciones==null){
						//System.out.println("Create estaciones");
						estaciones=new ArrayList<Localidad>();
						
						//System.out.println("proy "+proyectos.get(i).getNombreAlterno());
						estaciones.add(estacion);
						//System.out.println("estaciones.add(estacion) if "+estaciones.size());
					}
					
					
					
				}else{
					//System.out.println("log: "+log++);
					estacion=new Localidad();
					estacion.setCodigoEstacion(nullValue(rs.getString("codigo_estacion_loc")));
					estacion.setDescripcionEstacion(nullValue(rs.getString("descripcion_estacion_loc")));
					estacion.setPais(nullValue(rs.getString("pais_loc")));
					estacion.setMarrio(nullValue(rs.getString("marrio_loc")));
					estacion.setSalodulce(nullValue(rs.getString("salodulce_loc")));
					estacion.setLugar(nullValue(rs.getString("lugar")));
					estacion.setLatitudInicio(nullValue(rs.getString("latitudinicio_loc")));
					estacion.setLatitudFin(nullValue(rs.getString("latitudfin_loc")));
					estacion.setLongitudInicio(nullValue(rs.getString("longitudinicio_loc")));
					estacion.setLongitudFin(nullValue(rs.getString("longitudfin_loc")));
					estacion.setCuerpoAgua(nullValue(rs.getString("cuerpo_agua_loc")));
					estacion.setDistanciaCosta(nullValue(rs.getString("distanciacosta_loc")));
					estacion.setTipoCosta(nullValue(rs.getString("tipocosta_loc")));
					estacion.setHondoAgua(nullValue(rs.getString("hondoagua_loc")));
					estacion.setZonaProtegida(nullValue(rs.getString("zonaprotegida")));
					estacion.setToponimo(nullValue(rs.getString("toponimo")));
					estacion.setPrefijoEstacion(nullValue(rs.getString("prefijo_cdg_est_loc")));
					estacion.setBarco(nullValue(rs.getString("barco_loc")));
					estacion.setCampana(nullValue(rs.getString("campana_loc")));
					estacion.setSustrato(nullValue(rs.getString("sustrato")));
					estacion.setAmbiente(nullValue(rs.getString("ambiente")));
					estacion.setNotas(nullValue(rs.getString("notas_loc")));
					estacion.setEcorregion(nullValue(rs.getString("ecorregion")));
					estacion.setProfMin(nullValue(rs.getString("prof_min_loc")));
					estacion.setProfMax(nullValue(rs.getString("prof_max_loc")));
					
					estaciones.add(estacion);
					//System.out.println("estaciones.add(estacion) "+estaciones.size());
					
				}
				
				
			}while(rs.next());
			//System.out.println("AQUIIIIIIIIIII=== "+i);
			proyectos.get(i).setEstaciones(estaciones);
			rtnProyectos=new ArrayList<Proyecto>();
			for(int k=begin;k<begin+length;k++){
				rtnProyectos.add(proyectos.get(k));
			}
			
			
		}
		finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		return rtnProyectos;
	}
	
	private String nullValue(String value){
		return value==null?"":value;
	}
}
