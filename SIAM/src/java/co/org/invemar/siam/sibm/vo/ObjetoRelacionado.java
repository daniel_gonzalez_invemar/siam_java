/**
 * 
 */
package co.org.invemar.siam.sibm.vo;

/**
 * @author Administrador
 *
 */
public class ObjetoRelacionado {
	private String nroInvemar;
	private String objeto;
	private String unidades;
	private String notas;
	private String ubicacion;
	/**
	 * @param nroInvemar
	 * @param objeto
	 * @param unidades
	 * @param notas
	 * @param ubicacion
	 */
	public ObjetoRelacionado(String nroInvemar, String objeto, String unidades, String notas, String ubicacion) {
		super();
		this.nroInvemar = nroInvemar;
		this.objeto = objeto;
		this.unidades = unidades;
		this.notas = notas;
		this.ubicacion = ubicacion;
	}
	/**
	 * 
	 */
	public ObjetoRelacionado() {
	// TODO Auto-generated constructor stub
	}
	/**
	 * @return the notas
	 */
	public String getNotas() {
		return notas;
	}
	/**
	 * @param notas the notas to set
	 */
	public void setNotas(String notas) {
		this.notas = notas;
	}
	/**
	 * @return the nroInvemar
	 */
	public String getNroInvemar() {
		return nroInvemar;
	}
	/**
	 * @param nroInvemar the nroInvemar to set
	 */
	public void setNroInvemar(String nroInvemar) {
		this.nroInvemar = nroInvemar;
	}
	/**
	 * @return the objeto
	 */
	public String getObjeto() {
		return objeto;
	}
	/**
	 * @param objeto the objeto to set
	 */
	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}
	/**
	 * @return the ubicacion
	 */
	public String getUbicacion() {
		return ubicacion;
	}
	/**
	 * @param ubicacion the ubicacion to set
	 */
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	/**
	 * @return the unidades
	 */
	public String getUnidades() {
		return unidades;
	}
	/**
	 * @param unidades the unidades to set
	 */
	public void setUnidades(String unidades) {
		this.unidades = unidades;
	}
	
	
	
	
}
