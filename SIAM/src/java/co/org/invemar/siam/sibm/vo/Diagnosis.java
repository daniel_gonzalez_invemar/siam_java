/**
 * 
 */
package co.org.invemar.siam.sibm.vo;

/**
 * @author Administrador
 *
 */
public class Diagnosis {
	private String nroInvemar;
	private String item;
	private String valor;
	/**
	 * @param nroInvemar
	 * @param item
	 * @param valor
	 */
	public Diagnosis(String nroInvemar, String item, String valor) {
		super();
		this.nroInvemar = nroInvemar;
		this.item = item;
		this.valor = valor;
	}
	/**
	 * 
	 */
	public Diagnosis() {
		// TODO Auto-generated constructor stub
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getNroInvemar() {
		return nroInvemar;
	}
	public void setNroInvemar(String nroInvemar) {
		this.nroInvemar = nroInvemar;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
	
}
