/**
 * 
 */
package co.org.invemar.siam.sibm.controller.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.org.invemar.siam.sibm.model.ProyectosDM;
import co.org.invemar.siam.sibm.vo.Localidad;
import co.org.invemar.siam.sibm.vo.Proyecto;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

/**
 * @author Administrador
 *
 */
public class ProyectoAction implements Action{

	public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		ConnectionFactory cFactory = null;
		Connection connection = null;
		cFactory = new ConnectionFactory();
		
		List<Proyecto> proyectos=null;
		//Proyecto proyecto=null;
		//List<Localidad> estaciones=null;
		//Localidad estacion=null;
		ProyectosDM pdm=new ProyectosDM();
		//int position=Integer.parseInt(request.getParameter("position"));
		//int length=Integer.parseInt(request.getParameter("length"));
		String codFuente=request.getParameter("cdo");
		//System.out.println("codFuente: "+codFuente);
		if(codFuente.equalsIgnoreCase("all")){
			codFuente=String.valueOf(((int)Math.pow(2, 16)-1));
			//System.out.println("(Math.pow(2, 16)-1): "+((int)Math.pow(2, 16)-1));
		}
		
		
		try {
			connection = cFactory.createConnection();
			proyectos=pdm.findProjectsByCodigoFuente(connection,codFuente);
			request.setAttribute("PROYS", proyectos);
			/*for(Proyecto proyecto:proyectos ){
				System.out.println("proyecto: "+proyecto.getNombreAlterno());
				System.out.println("codigo "+proyecto.getCodigo());
				//System.out.println("estaciones size "+proyecto.getEstaciones().size());
				estaciones=proyecto.getEstaciones();
				int i=1;
				for(Localidad estacion:estaciones){
					System.out.println("estacion "+estacion.getLatitudInicio()+" i= "+i++);
					
				}
				
			}*/
			return new ActionRouter("/sibm/proyectos.jsp"); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		return new ActionRouter("/sibm/proyectos.jsp");
		
	}
	
}
