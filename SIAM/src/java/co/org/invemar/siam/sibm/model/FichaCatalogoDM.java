/**
 * 
 */
package co.org.invemar.siam.sibm.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.org.invemar.siam.sibm.vo.Atributo;
import co.org.invemar.siam.sibm.vo.Diagnosis;
import co.org.invemar.siam.sibm.vo.Nomenclatura;
import co.org.invemar.siam.sibm.vo.Notas;
import co.org.invemar.siam.sibm.vo.ObjetoRelacionado;
import co.org.invemar.siam.sibm.vo.Responsable;

/**
 * @author Administrador
 *
 */
public class FichaCatalogoDM {
	
	public int countImages(Connection connection, String nroInvemar)throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		int rtn=0;
		String SQL=" select count(*) from cimagenes where nroinvemar_im =? ";
		try{
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				rtn=rs.getInt(1);
			}
			
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		return rtn;
	}
	
	public int countAutores(Connection connection, String nroInvemar)throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		int rtn=0;
		String SQL=" select count(*) from cautores where nroinvemar_au =? ";
		try{
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				rtn=rs.getInt(1);
			}
			
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		return rtn;
	}
	
	
	
	public int countDiagnosis(Connection connection, String nroInvemar)throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		int rtn=0;
		String SQL=" select count(*) "+
		   " from CMDIAGNOSIS d "+
		   " where d.NROINVEMAR_DI=? ";
		try{
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				rtn=rs.getInt(1);
			}
			
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		return rtn;
	}
	public List<Diagnosis> findDiagnosis(Connection connection, String nroInvemar) throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		
		String SQL=" select li.descripcion_item, d.valor "+
				   " from CMLST_ITEMS_DIAGNOSIS li, CMDIAGNOSIS d "+
				   " where d.ID_ITEM_DI=li.id_item "+
				   " and d.NROINVEMAR_DI=? "+
				   " order by d.orden_di";
		List<Diagnosis> diagnosis=null;
		Diagnosis diagn=null;
		try {
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			diagnosis=new ArrayList<Diagnosis>();
			
			while(rs.next()){
				diagn=new Diagnosis();
				diagn.setNroInvemar(nroInvemar);
				diagn.setItem(nullValue(rs.getString("descripcion_item")));
				diagn.setValor(nullValue(rs.getString("valor")));
				diagnosis.add(diagn);
			}
	
		} finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		
		return diagnosis;
	}
	
	
	
	public int countObjetoRelacionados(Connection connection, String nroInvemar)throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		int rtn=0;
		String SQL=" select count(*) "+
		   " from cmobjetos obj "+
		   " where obj.nroinvemar_ob = ? ";
		try{
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				rtn=rs.getInt(1);
			}
			
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		return rtn;
	}
	
	
	public List<ObjetoRelacionado> findObjetosRelacionados(Connection connection, String nroInvemar) throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		
		String SQL=" SELECT   cd.descripcion_lov, obj.unidades_ob, obj.notas_ob, obj.ubicacion_ob "+
				   " FROM cmobjetos obj, codigoslov cd "+
				   " WHERE obj.objeto_ob = cd.codigo_lov "+
				   " AND cd.tabla_lov = 5 "+
				   " AND obj.nroinvemar_ob = ?"+
				   " ORDER BY obj.unidades_ob asc ";
		List<ObjetoRelacionado> objrels=null;
		ObjetoRelacionado objrel=null;
		try {
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			objrels=new ArrayList<ObjetoRelacionado>();
			
			while(rs.next()){
				objrel=new ObjetoRelacionado();
				objrel.setNroInvemar(nroInvemar);
				objrel.setObjeto(nullValue(rs.getString("descripcion_lov")));
				objrel.setUnidades(nullValue(rs.getString("unidades_ob")));
				objrel.setNotas(nullValue(rs.getString("notas_ob")));
				objrel.setUbicacion(nullValue(rs.getString("ubicacion_ob")));
				
				objrels.add(objrel);
				
			}
	
		} finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		
		return objrels;
	}
	
	public int countNomenclaturas(Connection connection, String nroInvemar)throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		int rtn=0;
		String SQL=" SELECT count(*) "+
				   " FROM cmnomenclaturas n, csinonimias s, codigoslov cd "+
				   " WHERE n.id_sinonimia_no = s.id_sn and n.id_sinonimia_no is not null AND s.tipo_sn <> 0 AND cd.tabla_lov = 2" +
				   " AND cd.codigo_lov = n.tecnica_no" +
				   "  AND nroinvemar_no = ? ";
		try{
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				rtn=rs.getInt(1);
			}
			
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		return rtn;
	}
	public List<Nomenclatura> findNomenclaturas(Connection connection, String nroInvemar) throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		
		String SQL=" SELECT s.descripcion_sn, n.fecha_no, n.actual_no, n.notas_no,  cd.descripcion_lov "+
				   " FROM cmnomenclaturas n, csinonimias s, codigoslov cd "+
				   " WHERE n.id_sinonimia_no = s.id_sn and n.id_sinonimia_no is not null AND s.tipo_sn <> 0 AND cd.tabla_lov = 2" +
				   " AND cd.codigo_lov = n.tecnica_no" +
				   "  AND nroinvemar_no = ? ";
		List<Nomenclatura> nomenclaturas=null;
		Nomenclatura nomenclatura=null;
		try {
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			nomenclaturas=new ArrayList<Nomenclatura>();
			
			while(rs.next()){
				nomenclatura=new Nomenclatura();
				nomenclatura.setNroInvemar(nroInvemar);
				nomenclatura.setName(nullValue(rs.getString("descripcion_sn")));
				nomenclatura.setFecha(nullValue(rs.getString("fecha_no")));
				nomenclatura.setActual(nullValue(rs.getInt("actual_no")==0?"NO":"SI"));
				nomenclatura.setNotas(nullValue(rs.getString("notas_no")));
				nomenclatura.setTecnica(nullValue(rs.getString("descripcion_lov")));
				nomenclaturas.add(nomenclatura);
				
			}
	
		} finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		
		return nomenclaturas;
	}
	
	public int countResponsables(Connection connection, String nroInvemar)throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		int rtn=0;
		String SQL=" select count(*) "+
		   " from cmresponsables "+
		   " where nroinvemar_re = ?  ";
		try{
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				rtn=rs.getInt(1);
			}
			
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		return rtn;
	}
	public List<Responsable> findResponsables(Connection connection, String nroInvemar)throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		
		String SQL=" SELECT   apellidos_co || ' ' || nombre_co nombre, sec.descripcion_lov seccion, "+
         		  " TO_CHAR (cmr.fecha_re, 'dd/mm/yyyy') fecha, "+
         		  " lov.descripcion_lov || ' ' || lov.descripciontabla_lov tarea, "+
         		  " cmr.orden_re "+
         		  " FROM cdircolector dir, curador.cmresponsables cmr, codigoslov lov, codigoslov sec "+
         		  " WHERE   cmr.nroinvemar_re = ? "+
         		  " AND cmr.codigo_co = dir.codigo_co "+
         		  " AND (cmr.tarea_re = lov.codigo_lov) "+
         		  " AND (lov.tabla_lov = 15 OR lov.tabla_lov = 12) "+
         		  " AND (cmr.seccion_re = sec.codigo_lov) "+
         		  " AND sec.tabla_lov = 29 "+
         		  " ORDER BY lov.tabla_lov ASC, fecha_re DESC, cmr.ORDEN_RE asc ";
		
		List<Responsable> responsables=null;
		Responsable responsable=null;
		try {
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			responsables=new ArrayList<Responsable>();
			
			while(rs.next()){
				responsable=new Responsable();
				responsable.setNroInvemar(nroInvemar);
				responsable.setNombre(nullValue(rs.getString("nombre")));
				responsable.setSeccion(nullValue(rs.getString("seccion")));
				responsable.setFecha(nullValue(rs.getString("fecha")));
				responsable.setTarea(nullValue(rs.getString("tarea")));
				responsable.setOrden(nullValue(rs.getString("orden_re")));
				responsables.add(responsable);
				
			}
	
		} finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		
		return responsables;
	}
	
	public int countAtributos(Connection connection, String nroInvemar)throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		int rtn=0;
		String SQL=" select count(*) "+
		   " from cmatributos_significativos "+
		   " where nroinvemar_at = ?  ";
		try{
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				rtn=rs.getInt(1);
			}
			
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		return rtn;
	}
	
	public List<Atributo> findAtributos(Connection connection, String nroInvemar) throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		
		String SQL=" SELECT cd.descripcion_lov atributo "+
				   " FROM cmatributos_significativos atr, codigoslov cd "+
				   " WHERE cd.tabla_lov = 17 "+
				   " AND atr.atributo = cd.codigo_lov "+
				   " AND atr.nroinvemar_at = ?";
		List<Atributo> atributos=null;
		Atributo atributo=null;
		try {
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			atributos=new ArrayList<Atributo>();
			
			while(rs.next()){
				atributo=new Atributo();
				atributo.setNroInvemar(nroInvemar);
				atributo.setAtributo(nullValue(rs.getString("atributo")));
				
				
				atributos.add(atributo);
				
			}
	
		} finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		
		return atributos;
	}
	
	public int countNotas(Connection connection, String nroInvemar)throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		int rtn=0;
		String SQL=" SELECT count(*) "+
				   " FROM vm_fichac "+
				   " WHERE noinv = ? "+
				   " AND (   notas_muestra IS NOT NULL "+
				   " OR notas_captura IS NOT NULL "+
				   " OR notas_id IS NOT NULL "+
				   " OR notas_es IS NOT NULL "+
				   " OR notas_cr IS NOT NULL "+
				   " OR notas_con IS NOT NULL )";
		try{
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				rtn=rs.getInt(1);
			}
			
		}finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		return rtn;
	}
	
	public Notas findNotas(Connection connection, String nroInvemar) throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		
		String SQL=" SELECT notas_muestra, notas_captura, notas_id, notas_es, notas_cr, notas_con "+
				   " FROM vm_fichac "+
				   " WHERE noinv = ?";
		Notas notas=null;
		try {
			pstmt = connection.prepareStatement(SQL);
			pstmt.setString(1, nroInvemar);
			rs = pstmt.executeQuery();
			
			
			
			if(rs.next()){
				notas=new Notas();
				notas.setNroInvemar(nroInvemar);
				notas.setNotasMuestra(nullValue(rs.getString("notas_muestra")));
				notas.setNotasCap(nullValue(rs.getString("notas_captura")));
				notas.setNotasId(nullValue(rs.getString("notas_id")));
				notas.setNotasEs(nullValue(rs.getString("notas_es")));
				notas.setNotasCr(nullValue(rs.getString("notas_cr")));
				notas.setNotasCon(nullValue(rs.getString("notas_con")));
				
				
				
			}
	
		} finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		
		return notas;
	}
	
	 
	
	private String nullValue(String value){
		return value==null?"":value;
	}
	
	
	
}
