package co.org.invemar.siam.controller;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionFactory;
import co.org.invemar.util.actions.ActionRouter;

public class ServiciosSIAM extends HttpServlet {
	/**
	 * 
	 */
	
	private final String CLASSPACKAGE="co.org.invemar.siam.";
	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		doPost(request,response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");		
                String action=request.getParameter("action");
               // System.out.println("action:"+action);
                String componente = request.getParameter("componente");
              //  System.out.println("Componente:"+componente);
                String className=null;
                
		if(componente.equalsIgnoreCase("manglares"))
                {                   
                   className=CLASSPACKAGE+componente+".controller.action."+action+"Action"; 
                    //System.out.println("1");
                }else if(componente.equalsIgnoreCase("redcam"))
                {                   
                   className=CLASSPACKAGE+componente+".controller.action."+action+"Action"; 
                   //System.out.println("3");
                }else{
                  className=CLASSPACKAGE+componente+".controller."+action+"Action";
                 // System.out.println("4");
                }
		
		//System.out.println("Servlet enlazaado:"+className);
		try {
			Action actions = ActionFactory.getAction(className);
			
			ActionRouter router = actions.execute(request, response);
			if(router!=null)
				router.route(this, request, response);
			
			
		} catch(Exception e) {
			throw new ServletException(e);
		}
                 
		
	}

}
