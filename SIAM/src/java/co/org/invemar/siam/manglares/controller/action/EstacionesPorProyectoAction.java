/**
 *
 */
package co.org.invemar.siam.manglares.controller.action;

import co.org.invemar.siam.manglares.model.DatosEstructurales;
import co.org.invemar.siam.manglares.vo.Datos;
import co.org.invemar.siam.manglares.vo.Departamento;
import co.org.invemar.siam.manglares.vo.Parcelas;
import co.org.invemar.siam.manglares.vo.Transectos;
import co.org.invemar.util.actions.Action;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import co.org.invemar.util.actions.ActionRouter;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Administrador
 *
 */
public class EstacionesPorProyectoAction implements Action{

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        response.setContentType("text/html charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();
        
        String nombreproyecto = request.getParameter("proyecto"); 
       
        
        //nombresector=sectordecodificado;
        //System.out.println("nombre sector:"+nombresector);
        
        DatosEstructurales estructuramangle = new DatosEstructurales();
        ArrayList estaciones = estructuramangle.getEstacionesPorProyecto(nombreproyecto);
        Iterator it = estaciones.iterator();
        String html="<select id='nivelestacion' name='nivelestacion' onchange='getComboBoxTransectos()' >";
         html=html+"<option value='-'>-</option>";
        while(it.hasNext()){
            Datos dato = (Datos) it.next();
             html=html+"<option value='"+dato.getCodigo()+"'>"+dato.getNombre()+"</option>";
            
        }
        html+="</select>";      
        
       
        pw.write(html);
        //System.out.println("html:"+html);
        return null;//new ActionRouter("/sibm/ficha_catalogo.jsp?" + request.getQueryString());
    }
}
