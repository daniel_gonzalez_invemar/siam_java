/**
 *
 */
package co.org.invemar.siam.manglares.controller.action;

import co.org.invemar.siam.manglares.model.DatosEstructurales;
import co.org.invemar.siam.manglares.vo.Parcelas;
import co.org.invemar.siam.manglares.vo.Transectos;
import co.org.invemar.util.actions.Action;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import co.org.invemar.util.actions.ActionRouter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Administrador
 *
 */
public class ParcelasPorIDTransectoAction implements Action{

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        response.setContentType("text/html charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();
        
        String nombretransecto = request.getParameter("niveltransecto"); 
        String codigoEstacion  =  request.getParameter("codigoestacion"); 

        DatosEstructurales estructuramangle = new DatosEstructurales();
        ArrayList parcelas = estructuramangle.getParcelas(codigoEstacion,nombretransecto);
        Iterator it = parcelas.iterator();
        String html="<select id='nivelparcelas' name='nivelparcelas'>";
        html=html+"<option value='-'>-</option>";
        while(it.hasNext()){
            Parcelas parcela = (Parcelas) it.next();
             html=html+"<option value='"+parcela.getCodigo()+"'>"+parcela.getNombre()+"</option>";
            
        }
        html+="</select>";      
        
       
        pw.write(html);
        //System.out.println("html:"+html);
        return null;//new ActionRouter("/sibm/ficha_catalogo.jsp?" + request.getQueryString());
    }
}
