package co.org.invemar.siam.manglares.model.presiones;


public class TablaIndicatorByDepartament {
   private  String departament;
   private  String sector;
   private  double indicatorValue;
   private int anio;
   private  String unidadManejo;
        
    public TablaIndicatorByDepartament() {
        super();
    }

    public void setUnidadManejo(String unidadManejo) {
        this.unidadManejo = unidadManejo;
    }

    public String getUnidadManejo() {
        return unidadManejo;
    }

    public String getDepartament() {
        return departament;
    }

    public void setDepartament(String departament) {
        this.departament = departament;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public double getIndicatorValue() {
        return indicatorValue;
    }

    public void setIndicatorValue(double indicatorValue) {
        this.indicatorValue = indicatorValue;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }
}
