/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.manglares.model.presiones;

import co.org.invemar.library.ushahidi.Category;
import co.org.invemar.library.ushahidi.CustomFields;
import co.org.invemar.library.ushahidi.Incident;
import co.org.invemar.library.ushahidi.JSONProcessing;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author usrsig15
 */
public class EgretaProcessing {

    private ArrayList<Report> reportList;
    private ArrayList<TablaIndicatorByDepartament> listIndicatorByDepartament =new ArrayList<TablaIndicatorByDepartament>();
    private Set<String> sectors = new HashSet();
    private Set<String> sites = new HashSet();


    private Set<Integer> anios = new HashSet();
    private Set<String> listMonth = new HashSet<String>();
    private List<Site> listSiteIndicators = new ArrayList<Site>();
    private List<DataIndicatorBySite> listPresionIndicators = new ArrayList<DataIndicatorBySite>();
  


    public EgretaProcessing() {
        ProcesingIncidents();
        buscarInicidentes(); 
    }


    private String convertToStringMonth(int month) {
        String m = "";

      /*  switch (month) {
        case Calendar.JANUARY:
            m = "Enero";
            break;
        case Calendar.FEBRUARY:
            m = "Febrero";
            break;
        case Calendar.MARCH:
            m = "Marzo";
            break;
        case Calendar.APRIL:
            m = "Abril";
            break;
        case Calendar.MAY:
            m = "Mayo";
            break;
        case Calendar.JUNE:
            m = "Junio";
            break;
        case Calendar.JULY:
            m = "Julio";
            break;
        case Calendar.AUGUST:
            m = "Agosto";
            break;
        case Calendar.SEPTEMBER:
            m = "Septiembre";
            break;
        case Calendar.OCTOBER:
            m = "Octubre";
            break;
        case Calendar.NOVEMBER:
            m = "Noviembre";
            break;
        case Calendar.DECEMBER:
            m = "Diciembre";
            break;
        }*/
        //System.out.println("imput month:"+month);
        return m;
    }

    private void createLOVAnios(int month) {
        /*switch (month) {
        case Calendar.JANUARY:
            listMonth.add("Enero");
            break;
        case Calendar.FEBRUARY:
            listMonth.add("Febrero");
            break;
        case Calendar.MARCH:
            listMonth.add("Marzo");

            break;
        case Calendar.APRIL:
            listMonth.add("Abril");
            break;
        case Calendar.MAY:
            listMonth.add("Mayo");
            break;
        case Calendar.JUNE:
            listMonth.add("Junio");
            break;
        case Calendar.JULY:
            listMonth.add("Julio");
            break;
        case Calendar.AUGUST:
            listMonth.add("Agosto");

            break;
        case Calendar.SEPTEMBER:
            listMonth.add("Septiembre");

            break;
        case Calendar.OCTOBER:
            listMonth.add("Octubre");
            break;
        case Calendar.NOVEMBER:
            listMonth.add("Noviembre");
            break;
        case Calendar.DECEMBER:
            listMonth.add("Diciembre");

            break;

        }*/
    }

    public void ProcesingIncidents() {
        reportList = new ArrayList<Report>();
        JSONProcessing jSONProcessing = new JSONProcessing("http://cinto.invemar.org.co/egreta/api?task=incidents");
        ArrayList<Incident> incidentsList = jSONProcessing.JSONToObject();

        if (incidentsList != null) {


            Iterator<Incident> iterator = incidentsList.iterator();
            int incidentsAcounter = 0;

            while (iterator.hasNext()) {
                Incident incident = iterator.next();
                incidentsAcounter++;
                //System.out.println("Incident name:" + incident.getTitle() + " date: " + incident.getDate());

                //System.out.println("incidentsAcounter:" + incidentsAcounter);
                Report r = new Report();
                r.setReport(incident);

                ArrayList<Category> categories = incident.getCategory();
                Iterator<Category> itCategory = categories.iterator();

                Pression presion = new Pression();
                if (itCategory.hasNext()) {
                    Category category = itCategory.next();
                    //System.out.println("--Category name:" + category.getTitle());

                    presion.setId(category.getId());
                    presion.setTitle(category.getTitle());

                }

                ArrayList<CustomFields> customsFieldList = incident.getCustomsFieldList();
                Iterator<CustomFields> itCustomsFieldList = customsFieldList.iterator();
                while (itCustomsFieldList.hasNext()) {
                    CustomFields customsField = itCustomsFieldList.next();
                    //                    System.out.println("---CustomsField:" + customsField.getFieldName() + " :value:" + customsField.getFieldResponse());

                    if (customsField.getFieldName() != null &&
                        customsField.getFieldName().equalsIgnoreCase("Intensidad")) {

                        if (customsField.getFieldResponse().equalsIgnoreCase("Fuerte:Da�os graves y evidentes")) {
                            presion.setIntensity(3);
                        } else if (customsField.getFieldResponse().equalsIgnoreCase("Moderado: Da�os moderados pero evidentes")) {
                            presion.setIntensity(2);
                        } else if (customsField.getFieldResponse().equalsIgnoreCase("D�bil: Da�os imperceptibles")) {
                            presion.setIntensity(1);
                        } else {
                            System.out.println("no ha sido configurdo intensidad: ************* " +
                                               customsField.getFieldResponse());
                        }
                        presion.setIntensityDescripcion(customsField.getFieldResponse());

                    }

                    if (customsField.getFieldName() != null &&
                        customsField.getFieldName().equalsIgnoreCase("Intensidad")) {

                        if (customsField.getFieldResponse().equalsIgnoreCase("Fuerte:Da�os graves y evidentes")) {
                            presion.setIntensity(3);
                        } else if (customsField.getFieldResponse().equalsIgnoreCase("Moderado: Da�os moderados pero evidentes")) {
                            presion.setIntensity(2);
                        } else if (customsField.getFieldResponse().equalsIgnoreCase("D�bil: Da�os imperceptibles")) {
                            presion.setIntensity(1);
                        } else {
                            System.out.println("no ha sido configurdo la intensidad: ************* " +
                                               customsField.getFieldResponse());
                        }
                        presion.setIntensityDescripcion(customsField.getFieldResponse());

                    }

                    if (customsField.getFieldName() != null &&
                        customsField.getFieldName().equalsIgnoreCase("Irreversibilidad")) {

                        if (customsField.getFieldResponse().equalsIgnoreCase("Total:requiere manejo en el corto plazo")) {
                            presion.setIrreversibilty(1);
                        } else if (customsField.getFieldResponse().equalsIgnoreCase("Parcial: Implica  manejo en el largo y mediano plazo")) {
                            presion.setIrreversibilty(2);
                        } else if (customsField.getFieldResponse().equalsIgnoreCase("Nula:Totalmente irreversible")) {
                            presion.setIrreversibilty(3);
                        } else {
                            System.out.println("no ha sido configurdo  irreversibilidad: ************* " +
                                               customsField.getFieldResponse());
                        }
                        presion.setIrreversibilityDescription(customsField.getFieldResponse());

                    }

                    if (customsField.getFieldName() != null &&
                        customsField.getFieldName().equalsIgnoreCase("Extensi�n")) {

                        if (customsField.getFieldResponse().equalsIgnoreCase("Baja:Menos del 30% del �rea")) {
                            presion.setExtension(1);
                        } else if (customsField.getFieldResponse().equalsIgnoreCase("Media:Entre el 30% y el 60% del �rea")) {
                            presion.setExtension(2);
                        } else if (customsField.getFieldResponse().equalsIgnoreCase("Alta:M�s del 60% del �rea")) {
                            presion.setExtension(3);
                        } else {
                            System.out.println("no ha sido configurdo extension: ************* " +
                                               customsField.getFieldResponse());
                        }
                        presion.setExtensioDescription(customsField.getFieldResponse());
                    }

                    if (customsField.getFieldName() != null &&
                        customsField.getFieldName().equalsIgnoreCase("Ocurrencia")) {

                        if (customsField.getFieldResponse().equalsIgnoreCase("Baja:Poca o ninguna probabilidad")) {
                            presion.setOcurrencia(1);
                        } else if (customsField.getFieldResponse().equalsIgnoreCase("Media:Probable si no se toman medidas de manejo adecuado")) {
                            presion.setOcurrencia(2);
                        } else if (customsField.getFieldResponse().equalsIgnoreCase("Alto:Totalmente probable")) {
                            presion.setOcurrencia(3);
                        } else {
                            System.out.println("no ha sido configurdo ocurrencia: ************* " +
                                               customsField.getFieldResponse());
                        }
                        presion.setOcurrenciaDescription(customsField.getFieldResponse());
                    }

                    if (customsField.getFieldName() != null && customsField.getFieldName().equalsIgnoreCase("sector")) {

                        r.setSector(customsField.getFieldResponse());
                    }

                    if (customsField.getFieldName() != null && customsField.getFieldName().equalsIgnoreCase("Unidad de Manejo")) {

                        r.setSitio(customsField.getFieldResponse());
                    }

                    if (customsField.getFieldName() != null &&
                        customsField.getFieldName().equalsIgnoreCase("Departamentos")) {

                        r.setDepartamento(customsField.getFieldResponse());
                    }


                }
                r.setPresion(presion);

                anios.add(r.getYear());
                sectors.add(r.getSector());
                sites.add(r.getSitio());
                createLOVAnios(r.getMonth());
                reportList.add(r);
            }
        }

    }
    public void buscarInicidentes(){
       
        System.out.println("-------------------------------------------All Report-****------------------");
        Iterator<Report> iterator = getReportList().iterator();
        int contador = 0;
        while (iterator.hasNext()) {
            Report myReport = iterator.next();
            contador++;
            System.out.println("Contador:" + contador + " Departamento: " + myReport.getDepartamento() + " Month:" +
                               myReport.getMonth() + " Year report: " + myReport.getYear() + " Report Unidad Manejo: " +
                               myReport.getSitio() + " Sector: " + myReport.getSector() + " Report id: " +
                               myReport.getId() + " report name: " + myReport.getTitle() + " Presion: " +
                               myReport.getPresion().getTitle() + " - intensidad:" +
                               myReport.getPresion().getIntensity() + " Irreversibilidad:" +
                               myReport.getPresion().getIrreversibilty() + " -Extension:" +
                               myReport.getPresion().getExtension() + "- Ocurrencia:" +
                               myReport.getPresion().getOcurrencia());
        }
    }

    public static void main(String[] args) {
        EgretaProcessing egretaProcessing = new EgretaProcessing();
        System.out.println("-------------------------------------------All Report-****------------------");
        Iterator<Report> iterator =egretaProcessing.getReportList().iterator();
        int contador = 0;
        while (iterator.hasNext()) {
            Report myReport = iterator.next();
            contador++;
            System.out.println("Contador:" + contador + " Departamento: " + myReport.getDepartamento() + " Month:" +
                               myReport.getMonth() + " Year report: " + myReport.getYear() + " Report Unidad Manejo: " +
                               myReport.getSitio() + " Sector: " + myReport.getSector() + " Report id: " +
                               myReport.getId() + " report name: " + myReport.getTitle() + " Presion: " +
                               myReport.getPresion().getTitle() + " - intensidad:" +
                               myReport.getPresion().getIntensity() + " Irreversibilidad:" +
                               myReport.getPresion().getIrreversibilty() + " -Extension:" +
                               myReport.getPresion().getExtension() + "- Ocurrencia:" +
                               myReport.getPresion().getOcurrencia());
        }
        egretaProcessing.Indicator("ATL_Atl�ntico",2014);
        System.out.println("/************************************************************");
        System.out.println("/************************************************************");
        System.out.println("/************************************************************");
        egretaProcessing.Indicator("Magdalena","CGSM",2014);

    }


    public void Indicator(String departamento,int anio) {
     
       System.out.println("--------------------------------------------Indicator calculus-1111-----------------");   

        List<DatosIndicator> IndicatorList = null;
        ArrayList<Report> reportFiltered = new ArrayList<Report>();
        String deparmentSectorSelected;
        try {
            if (departamento != null) {        
               
               Iterator<Report> iterator = getReportList().iterator();
               System.out.println("tamanio:" +getReportList().size());
               sectors.clear();

                 while (iterator.hasNext()) {
                    Report myReport = iterator.next();
                    if (departamento.equalsIgnoreCase(myReport.getDepartamento())) {
                        if (anio == myReport.getYear()) {
                            //if (unidadManejo.equalsIgnoreCase(myReport.getSitio())){
                                System.out.println("Departamento: " + myReport.getDepartamento() + " Month:" +
                                                   myReport.getMonth() + " Year report: " + myReport.getYear() +
                                                   " Report Unidad de manejo: " + myReport.getSitio() + " Sector: " +
                                                   myReport.getSector() + " Report id: " + myReport.getId() +
                                                   " report name: " + myReport.getTitle() + "  Presion:  " +
                                                   myReport.getPresion().getTitle() + " id Tipo de presion:" +
                                                   myReport.getPresion().getId() + "  - intensidad:" +
                                                   myReport.getPresion().getIntensity() + " Irreversibilidad:" +
                                                   myReport.getPresion().getIrreversibilty() + " -Extension:" +
                                                   myReport.getPresion().getExtension() + " Ocurrencia:" +
                                                   myReport.getPresion().getOcurrencia());
                                deparmentSectorSelected = myReport.getDepartamento();
                                sectors.add(myReport.getSector());
                                reportFiltered.add(myReport); 
                          //  }
                        

                        }

                    }
                } 


                listIndicatorByDepartament.clear();                
                Iterator<String> sectorIteratorIt = sectors.iterator();
                double sumatotal =0;
                double indicator = 0;               
                String nameSector="";
                
                while (sectorIteratorIt.hasNext()) {
                    
                    String sector = sectorIteratorIt.next();
                    System.out.println("Sector:" + sector);

                    Iterator<Report> reportFilteredIterator = reportFiltered.iterator();
                    int sizeReportFiltered = reportFiltered.size();
                    double criterionSum = 0;                   
                   
                    Report r = null;
                      while (reportFilteredIterator.hasNext()) {
                        r = reportFilteredIterator.next();
                        departamento= r.getDepartamento();
                        if (r.getSector().equalsIgnoreCase(sector)) {
                            sumatotal =sumatotal+r.getPresion().getIntensity() + r.getPresion().getIrreversibilty() +r.getPresion().getExtension() + r.getPresion().getOcurrencia();                                               
                            indicator = sumatotal / sizeReportFiltered;
                            //nameSector=sector;
                        }
                        
                    }
                    System.out.println("sumatotal:"+sumatotal);
                    TablaIndicatorByDepartament tid = new TablaIndicatorByDepartament();
                    tid.setAnio(anio);
                    tid.setDepartament(departamento);                
                    tid.setIndicatorValue(indicator);      
                    tid.setSector(sector);
                    listIndicatorByDepartament.add(tid); 

                } 
                
              

                System.out.println("Verificando el arreglo.....................");
                 Iterator<TablaIndicatorByDepartament> itt = listIndicatorByDepartament.iterator();
                 
//                while (itt.hasNext()) {
//                    TablaIndicatorByDepartament tibd = itt.
//                   // System.out.println("Departamento tibd:" + tibd.getDepartament() + " -Sector:"+ tibd.getSector()+" -value:" + tibd.getIndicatorValue());
//                } 
                
                
            }

        } catch (Exception e) {
            System.out.println("Error calculando el indicador:" + e.getMessage());
        }
       
    }
    
    public void Indicator(String departamento,String sectorbusqueda, int anio) {
     
       System.out.println("--------------------------------------------Indicator calculus Sector por Unidad de manejo-----------------");   

        List<DatosIndicator> IndicatorList = null;
        ArrayList<Report> reportFiltered = new ArrayList<Report>();
        String deparmentSectorSelected;
        try {
            if (departamento != null) {        
               
               Iterator<Report> iterator = getReportList().iterator();
               System.out.println("incidentes reportados:" +getReportList().size());
               sectors.clear();

                 while (iterator.hasNext()) {
                    Report myReport = iterator.next();
                    if (departamento.equalsIgnoreCase(myReport.getDepartamento())) {
                        if (anio == myReport.getYear()) {
                              if (sectorbusqueda.equalsIgnoreCase(myReport.getSector())){
                                System.out.println("Departamento: " + myReport.getDepartamento() + " Month:" +
                                                   myReport.getMonth() + " Year report: " + myReport.getYear() +
                                                   " Report Unidad de manejo: " + myReport.getSitio() + " Sector: " +
                                                   myReport.getSector() + " Report id: " + myReport.getId() +
                                                   " report name: " + myReport.getTitle() + "  Presion:  " +
                                                   myReport.getPresion().getTitle() + " id Tipo de presion:" +
                                                   myReport.getPresion().getId() + "  - intensidad:" +
                                                   myReport.getPresion().getIntensity() + " Irreversibilidad:" +
                                                   myReport.getPresion().getIrreversibilty() + " -Extension:" +
                                                   myReport.getPresion().getExtension() + " Ocurrencia:" +
                                                   myReport.getPresion().getOcurrencia());
                                deparmentSectorSelected = myReport.getDepartamento();
                                sectors.add(myReport.getSector());
                                reportFiltered.add(myReport); 
                            }                        

                        }

                    }
                } 


                listIndicatorByDepartament.clear();                
                Iterator<String> sectorIteratorIt = sectors.iterator();
                double sumatotal =0;
                double indicator = 0;               
                String nameManagmentUnit="";
                
                while (sectorIteratorIt.hasNext()) {
                    
                    String sector = sectorIteratorIt.next();
                    System.out.println("Sector:" + sector);

                    Iterator<Report> reportFilteredIterator = reportFiltered.iterator();
                    int sizeReportFiltered = reportFiltered.size();
                    double criterionSum = 0;                   
                    nameManagmentUnit="";
                    Report r = null;
                      while (reportFilteredIterator.hasNext()) {
                        r = reportFilteredIterator.next();
                        departamento= r.getDepartamento();
                        if (r.getSector().equalsIgnoreCase(sector)) {
                            sumatotal =sumatotal+r.getPresion().getIntensity() + r.getPresion().getIrreversibilty() +r.getPresion().getExtension() + r.getPresion().getOcurrencia();                                               
                            indicator = sumatotal / sizeReportFiltered;
                            nameManagmentUnit=r.getSitio();
                            System.out.println("sumatotal:"+sumatotal);
                            TablaIndicatorByDepartament tid = new TablaIndicatorByDepartament();
                            tid.setAnio(anio);
                            tid.setDepartament(departamento);                
                            tid.setIndicatorValue(indicator);      
                            tid.setSector(sector);
                            tid.setUnidadManejo(r.getSitio());
                            listIndicatorByDepartament.add(tid);  
                        }
                       
                    }
                   

                } 
                
              

                System.out.println("Verificando el arreglo.....................");
                 Iterator<TablaIndicatorByDepartament> itt = listIndicatorByDepartament.iterator();
                while (itt.hasNext()) {
                    TablaIndicatorByDepartament tibd = itt.next();
                    System.out.println("Departamento tibd:" + tibd.getDepartament() + " -Sector:"+ tibd.getSector()+" Unidad de manejo:"+tibd.getUnidadManejo() +"-value:" + tibd.getIndicatorValue());
                } 
                
                
            }

        } catch (Exception e) {
            System.out.println("Error calculando el indicador:" + e.getMessage());
        }
       
    }
        
    



    public ArrayList<Report> getReportList() {
        return reportList;
    }

    public void setReportList(ArrayList<Report> reportList) {
        this.reportList = reportList;
    }

    public Set<String> getSectors() {
        return sectors;
    }

    public void setSectors(Set<String> sectors) {
        this.sectors = sectors;
    }

    public Set<String> getSites() {
        return sites;
    }

    public void setSites(Set<String> sites) {
        this.sites = sites;
    }

    public void setAnios(Set<Integer> anios) {
        this.anios = anios;
    }

    public Set<Integer> getAnios() {
        return anios;
    }

    public void setListMonth(Set<String> listMonth) {
        this.listMonth = listMonth;
    }

    public Set<String> getListMonth() {
        return listMonth;
    }

    public void setListPresionIndicators(List<DataIndicatorBySite> listPresionIndicators) {
        this.listPresionIndicators = listPresionIndicators;
    }

    public List<DataIndicatorBySite> getListPresionIndicators() {
        return listPresionIndicators;
    }

    public void setListSiteIndicators(List<Site> listSiteIndicators) {
        this.listSiteIndicators = listSiteIndicators;
    }

    public List<Site> getListSiteIndicators() {
        return listSiteIndicators;
    }

    public void setListIndicatorByDepartament(ArrayList<TablaIndicatorByDepartament> listIndicatorByDepartament) {
        this.listIndicatorByDepartament = listIndicatorByDepartament;
    }

    public ArrayList<TablaIndicatorByDepartament> getListIndicatorByDepartament() {
        return listIndicatorByDepartament;
    }

}
