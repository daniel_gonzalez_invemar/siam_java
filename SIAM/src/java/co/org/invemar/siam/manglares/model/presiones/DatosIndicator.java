package co.org.invemar.siam.manglares.model.presiones;


public class DatosIndicator {  
    private String sitio;
    private double value;
    private String departamento;

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDepartamento() {
        return departamento;
    }


    public DatosIndicator(){
        
    }
    
   
    public DatosIndicator(String site, double value){
        this.sitio = site;
        this.value= value;
    }

    public String getSitio() {
        return sitio;
    }

    public void setSitio(String sitio) {
        this.sitio = sitio;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
