/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.manglares.model;

/**
 *
 * @author usrsig15
 */
import co.org.invemar.siam.manglares.vo.Datos;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import org.primefaces.model.chart.CartesianChartModel;

@ManagedBean(name = "graficador")
public class GraficadorEstadisticas implements Serializable {

    private CartesianChartModel categoryModel;
    private Map<String, String> especies = new HashMap<String, String>();
    private Map<String, String> variables = new HashMap<String, String>();
    private DatosEstructurales de;
    private String especie;
    private String variable;
    private boolean especieSeleccionada = false;
    private boolean varialesSeleccionada = false;
    private boolean aplicacionIniciada = true;
    private boolean renderizarGrafica = false;
    
    private ArrayList<Datos> estadisticas;
    private ArrayList anos;

    public void manejadorSeleccionEspecie() {
        /*try {
            //if (especie != null && !especie.equals("")) {
              //  try {
                //    System.out.println("Especie seleccionada:" + especie);
                    variables = de.getVariables();
                   // ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                    // session.setAttribute("especie", especie);
                   // session.setAttribute("especie", especie);
                //} catch (Exception ex) {
                //    Logger.getLogger(GraficadorEstadisticas.class.getName()).log(Level.SEVERE, null, ex);
               // }
            //}
        } catch (Exception ex) {
            Logger.getLogger(GraficadorEstadisticas.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }
/*
    public void manejadorSeleccionVariable() {

       // if (variable != null && !variable.equals("")) {
            try {                
                System.out.println("variable seleccionada:" + variable);                
                
                /*ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                HttpSession session = (HttpSession) ec.getSession(false);
                
                String especie = (String)session.getAttribute("especie");*/
         /*       
                
            } catch (Exception ex) {
                  System.out.println("Error seleccionando variables:"+ex.getMessage());
            }
        //}

    }

    public GraficadorEstadisticas() {

        try {

            de = new DatosEstructurales();
            especies = de.getEspecies();
            createCategoryModel();

        } catch (Exception ex) {
            System.out.println("Error al generar La gr�fica:" + ex.toString());
        }
    }

    public void createCategoryModel() {
        categoryModel = new CartesianChartModel();
        
        if (especie != null && variable != null) {
            System.out.println("Parametros nulos.");
            try {
                            
                estadisticas = de.estadisticasVariables(this.especie, this.variable, "2");
                anos = de.getAnos();
                Iterator<String> it = anos.iterator();

                while (it.hasNext()) {
                    String ano = it.next();

                    ChartSeries categoria = new ChartSeries();
                    categoria.setLabel(ano);
                    // System.out.println("Ano:" + ano);

                    Iterator<Datos> itestadisticas = estadisticas.iterator();
                    while (itestadisticas.hasNext()) {
                        Datos dato = itestadisticas.next();
                        if (ano.equals(dato.getAno())) {

                            // System.out.println("estacion:" + dato.getNombreEstacion());

                            ArrayList categorias = de.getCategorias();
                            Iterator<String> categoriasIt = categorias.iterator();
                            while (categoriasIt.hasNext()) {
                                String cat = categoriasIt.next();
                                if (cat.equalsIgnoreCase(dato.getNombreEstacion())) {
                                    categoria.set(dato.getNombreEstacion(), Double.parseDouble(dato.getTotalVariable()));
                                    // System.out.println(" est:"+dato.getNombreEstacion() +" valor:"+ dato.getTotalVariable());
                                } else {
                                    categoria.set(cat, 0.0);
                                    //  System.out.println(" est:"+cat +" valor:"+ 0.0);
                                }
                            }
                        }
                    }
                    categoryModel.addSeries(categoria);
                }
            } catch (Exception ex) {
                System.out.println("Error generando la gr�fica:" + ex.toString());
            }
        }
    }

    public void setVariables(Map<String, String> variables) {
        this.variables = variables;
    }

    public void setEspecies(Map<String, String> especies) {
        this.especies = especies;
    }

    public boolean isRenderizarGrafica() {
        return renderizarGrafica;
    }

    public void setEspecieSeleccionada(boolean especieSeleccionada) {
        this.especieSeleccionada = especieSeleccionada;
    }

    public void setRenderizarGrafica(boolean renderizarGrafica) {
        this.renderizarGrafica = renderizarGrafica;
    }

    public boolean isVarialesSeleccionada() {
        return varialesSeleccionada;
    }

    public boolean isAplicacionIniciada() {
        return aplicacionIniciada;
    }

    public void setAplicacionIniciada(boolean aplicacionIniciada) {
        this.aplicacionIniciada = aplicacionIniciada;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public boolean isEspecieSeleccionada() {
        return especieSeleccionada;
    }

    public void setVarialesSeleccionada(boolean varialesSeleccionada) {
        this.varialesSeleccionada = varialesSeleccionada;
    }

    public Map<String, String> getVariables() {
        return variables;
    }

    public String getVariable() {
        return variable;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public Map<String, String> getEspecies() {
        return especies;
    }

    public CartesianChartModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CartesianChartModel categoryModel) {
        this.categoryModel = categoryModel;
    }
*/

}
