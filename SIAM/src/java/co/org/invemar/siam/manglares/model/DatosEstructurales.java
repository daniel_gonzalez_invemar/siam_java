/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.manglares.model;

import co.org.invemar.siam.CconexionPrueba;
import co.org.invemar.siam.ConnectionFactory;
import co.org.invemar.siam.manglares.vo.Datos;
import co.org.invemar.siam.manglares.vo.Departamento;
import co.org.invemar.siam.manglares.vo.Especie;
import co.org.invemar.siam.manglares.vo.Proyectos;
import co.org.invemar.siam.manglares.vo.Region;
import co.org.invemar.siam.manglares.vo.Estacion;
import co.org.invemar.siam.manglares.vo.Parcelas;
import co.org.invemar.siam.manglares.vo.Sectores;
import co.org.invemar.siam.manglares.vo.Transectos;
import co.org.invemar.siam.manglares.vo.Variable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class DatosEstructurales {

    private ArrayList anos;
    private ArrayList categorias;
    private Map<String, String> especies;
    private Map<String, String> variables;
    private String especie;
    private String variable;
    private ArrayList arrayColumnas;
    

    public ArrayList getArrayColumnas() {
        return arrayColumnas;
    }

    public ArrayList getArrayDatos() {
        return arrayDatos;
    }
    private ArrayList arrayDatos;

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public ArrayList getCategorias() {
        return categorias;
    }

    public String getEspecie() {
        return especie;
    }

    public ArrayList getAnos() {
        ConnectionFactory cf;

        Connection con;
        anos = new ArrayList();
        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

            PreparedStatement pstmt;
            ResultSet rs;

            String sql = "SELECT distinct ano FROM rc_manglar_eb m WHERE m.nivel=2  ORDER BY ano";
            //System.out.println("SQL ejecutado:" + sql);
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                anos.add(rs.getString("ano"));
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            System.out.println("Error consultando los a�os en rc_manglar_eb:" + ex.toString());
        } catch (Exception ex) {
        }
        return anos;
    }

    public ArrayList getProyectos() {

        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList listaProyectos = new ArrayList();

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

            /*CconexionPrueba conexion = new CconexionPrueba();
            con = conexion.getConn();*/

            String sql = "select distinct prj from rc_manglar_eb";
            pstmt = con.prepareStatement(sql);
            // System.out.println("sql execute proyectos:" + sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Proyectos proyecto = new Proyectos();
                proyecto.setNombre(rs.getString("PRJ"));
                proyecto.setCodigo(rs.getString("PRJ"));
                listaProyectos.add(proyecto);
            }
            if (pstmt != null) {
                pstmt.close();

            }
            if (con != null) {
                con.close();
            }

        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo los proyectos:" + e.toString());
        }

        return listaProyectos;
    }

    public ArrayList getProyectos(String sector) {

        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList listaProyectos = new ArrayList();

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

            /*CconexionPrueba conexion = new CconexionPrueba();
            con = conexion.getConn();*/

            String sql = "select distinct prj from rc_manglar_eb where sector='"+sector+"'";
            pstmt = con.prepareStatement(sql);
            // System.out.println("sql execute proyectos:" + sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Proyectos proyecto = new Proyectos();
                proyecto.setNombre(rs.getString("PRJ"));
                proyecto.setCodigo(rs.getString("PRJ"));
                listaProyectos.add(proyecto);
            }
            if (pstmt != null) {
                pstmt.close();

            }
            if (con != null) {
                con.close();
            }

        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo los proyectos por sector:" + e.toString());
        }

        return listaProyectos;
    }

    public ArrayList getRegiones(String nombreProyecto) {

        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList listaRegiones = new ArrayList();

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

            /*CconexionPrueba conexion = new CconexionPrueba();
            con = conexion.getConn();*/

            String sql = "select distinct region  from rc_manglar_eb where prj='" + nombreProyecto + "'";
            pstmt = con.prepareStatement(sql);
            // System.out.println("sql execute regiones:" + sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Region region = new Region();
                region.setNombre(rs.getString("region"));
                region.setCodigo(rs.getString("region"));
                listaRegiones.add(region);
            }
            if (pstmt != null) {
                pstmt.close();

            }
            if (con != null) {
                con.close();
            }

        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo las regiones:" + e.toString());
        }

        return listaRegiones;
    }

    public ArrayList getRegiones() {

        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList listaRegiones = new ArrayList();

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

            /*CconexionPrueba conexion = new CconexionPrueba();
            con = conexion.getConn();*/

            String sql = "select distinct region  from rc_manglar_eb ";
            pstmt = con.prepareStatement(sql);
            // System.out.println("sql execute regiones:" + sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Region region = new Region();
                region.setNombre(rs.getString("region"));
                region.setCodigo(rs.getString("region"));
                listaRegiones.add(region);
            }
            if (pstmt != null) {
                pstmt.close();

            }
            if (con != null) {
                con.close();
            }

        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo todas las regiones:" + e.toString());
        }

        return listaRegiones;
    }

    public ArrayList getDepartamentos(String nombreRegion) {

        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList listaDepartamentos = new ArrayList();

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

            /*CconexionPrueba conexion = new CconexionPrueba();
            con = conexion.getConn();*/

            String sql = "select distinct depto  from rc_manglar_eb where region='" + nombreRegion + "'";
            pstmt = con.prepareStatement(sql);
            // System.out.println("sql execute departamentos:" + sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Departamento departamento = new Departamento();
                departamento.setNombre(rs.getString("depto"));
                departamento.setCodigo(rs.getString("depto"));
                listaDepartamentos.add(departamento);
            }
            if (pstmt != null) {
                pstmt.close();

            }
            if (con != null) {
                con.close();
            }

        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo los departamentos:" + e.toString());
        }

        return listaDepartamentos;
    }

    public ArrayList getSectores(String nombreDepartamento) {
        ArrayList listaSectores = null;
        try {

            String sql = "select DISTINCT SECTOR as nombre, sector as codigo  from rc_manglar_eb where depto='" + nombreDepartamento + "'";
            //System.out.println("sql execute sectores:" + sql);
            listaSectores = getRegistros(sql);

        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo los sectores del departamento:" + e.toString());
        }

        return listaSectores;
    }

    public ArrayList getRegistros(String sql) throws SQLException, Exception {

        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList listadatos = new ArrayList();


        cf = new ConnectionFactory();
        con = cf.createConnection("argos");

        /*CconexionPrueba conexion = new CconexionPrueba();
        con = conexion.getConn();*/


        pstmt = con.prepareStatement(sql);
        rs = pstmt.executeQuery();

        listadatos = getVectorDatos(rs);
        if (pstmt != null) {
            pstmt.close();

        }
        if (con != null) {
            con.close();
        }



        return listadatos;
    }

    private ArrayList getVectorDatos(ResultSet rs) throws SQLException {

        ArrayList listadatos = new ArrayList();
        while (rs.next()) {
            Datos dato = new Datos();
            dato.setNombre(rs.getString("nombre"));
            dato.setCodigo(rs.getString("codigo"));
            listadatos.add(dato);
        }
        Iterator it = listadatos.iterator();
        while (it.hasNext()) {
            Datos dato = (Datos) it.next();
            //System.out.print("Dato nombre:" + dato.getCodigo() + "-");
           // System.out.println("Dato codigo::" + dato.getNombre());



        }
        return listadatos;


    }

    public ArrayList getVariables() {
        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList variables = new ArrayList();
        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");
            //if(con!=null) System.out.println("con no es null..............");

            String sql = "SELECT NOMBRE, COLUMN_NAME FROM V_ESTADISTICOS_ES_MANGLE order by nombre";
            //System.out.println("consulta ejecutada:" + sql);
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Variable v = new Variable();
                v.setNombre(rs.getString("NOMBRE"));
                v.setCodigo(rs.getString("COLUMN_NAME"));
                variables.add(v);
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (con != null) {
                con.close();
            }
        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo las variables:" + e.toString());
        } finally {
        }

        return variables;
    }

    public ArrayList getVariablesDeCalculosTotales() {
        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList variables = new ArrayList();
        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

            String sql = "SELECT NOMBRE, COLUMN_NAME FROM V_ESTADISTICOS_MANGLE_TOTALES order by nombre";
            //System.out.println("consulta ejecutada:" + sql);
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Variable v = new Variable();
                v.setNombre(rs.getString("NOMBRE"));
                v.setCodigo(rs.getString("COLUMN_NAME"));
                variables.add(v);
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (con != null) {
                con.close();
            }
        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo las variables:" + e.toString());
        } finally {
        }

        return variables;
    }

    public ArrayList getEstaciones() {
        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList estaciones = new ArrayList();

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

            /*CconexionPrueba conexion = new CconexionPrueba();
            con = conexion.getConn();*/

            String sql = "SELECT  distinct codest,nomest   FROM rc_manglar_eb m";
            pstmt = con.prepareStatement(sql);
            // System.out.println("sql execute:" + sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Estacion estacion = new Estacion();
                estacion.setNombre(rs.getString("nomest"));
                estacion.setCodigo(rs.getString("codest"));
                estaciones.add(estacion);
            }
            if (pstmt != null) {
                pstmt.close();

            }
            if (con != null) {
                con.close();
            }

        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo las estaciones:" + e.toString());
        }

        return estaciones;
    }

    public ArrayList getEstaciones(String nombreSector) {

        ArrayList estaciones = new ArrayList();
        try {

            String sql = "SELECT  distinct codest as codigo,nomest as nombre   FROM rc_manglar_eb m where sector='" + nombreSector + "' and codest is not null";
             //System.out.println("SQL estaciones:" + sql);
            estaciones = getRegistros(sql);

        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo las estaciones:" + e.toString());
        }

        return estaciones;
    }
       public ArrayList getEstacionesPorProyecto(String proyecto) {

        ArrayList estaciones = new ArrayList();
        try {

            String sql = "SELECT  distinct codest as codigo,nomest as nombre   FROM rc_manglar_eb m where prj='" + proyecto + "' and codest is not null";
            // System.out.println("SQL estaciones por proyecto:" + sql);
            estaciones = getRegistros(sql);

        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo las estaciones por  proyecto:" + e.toString());
        }

        return estaciones;
    }
    

    public ArrayList getTransectos(String codigoEstacion) {
        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList transectos = new ArrayList();

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

            /*CconexionPrueba conexion = new CconexionPrueba();
            con = conexion.getConn();*/

            String sql = "SELECT  distinct (substr(parcela,1,1)) as transecto  FROM v_datos_manglares  where codest=" + codigoEstacion + " order by transecto";
            pstmt = con.prepareStatement(sql);
            //System.out.println("sql execute:" + sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Transectos transecto = new Transectos();
                transecto.setNombre(rs.getString("transecto"));
                transecto.setCodigo(rs.getString("transecto"));
                transectos.add(transecto);
            }
            if (pstmt != null) {
                pstmt.close();

            }
            if (con != null) {
                con.close();
            }

        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo los transectos:" + e.toString());
        } finally {
        }
        return transectos;
    }

    public ArrayList getParcelas(String codigoEstacion, String inicialtransecto) {
        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList parcelas = new ArrayList();

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

            /*CconexionPrueba conexion = new CconexionPrueba();
            con = conexion.getConn();*/

            String sql = "SELECT  distinct parcela  FROM v_datos_manglares  where codest=" + codigoEstacion + " and parcela like '%" + inicialtransecto + "%' order by parcela";
            pstmt = con.prepareStatement(sql);
            // System.out.println("sql execute:" + sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Parcelas parcela = new Parcelas();
                parcela.setNombre(rs.getString("parcela"));
                parcela.setCodigo(rs.getString("parcela"));
                parcelas.add(parcela);
            }
            if (pstmt != null) {
                pstmt.close();

            }
            if (con != null) {
                con.close();
            }

        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo las parcelas:" + e.toString());
        } finally {
        }
        return parcelas;
    }

    public ArrayList getEspecies() {
        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList especies = new ArrayList();

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

            String sql = "SELECT  distinct especie, codesp  FROM rc_manglar_eb m where especie is not null";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Especie especie = new Especie();
                especie.setNombre(rs.getString("especie"));
                especie.setCodigo(rs.getString("codesp"));
                especies.add(especie);
            }
            if (pstmt != null) {
                pstmt.close();

            }
            if (con != null) {
                con.close();
            }


        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo las especies:" + e.toString());
        } finally {
        }



        return especies;
    }

    public ArrayList<Datos> estadisticasVariables(String codEspecie, String nombreVariable, String nivelBusqueda) throws Exception {
        ArrayList<Datos> listaResultados = new ArrayList<Datos>();

        ConnectionFactory cf;

        Connection con = null;

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");


        } catch (Exception e) {
        }

        PreparedStatement pstmt;
        ResultSet rs;



        String sql = "SELECT especie, codesp,ano, codest,variable,ab_mh AS valor,nomest FROM rc_manglar_eb m WHERE m.nivel=" + nivelBusqueda + " and codesp   ='" + codEspecie + "'   and variable = '" + nombreVariable + "' ORDER BY ano";
        // System.out.println("SQL ejecutado:" + sql);
        pstmt = con.prepareStatement(sql);
        rs = pstmt.executeQuery();

        HashSet anos_ = new HashSet();
        HashSet categoriasHashSet = new HashSet();

        while (rs.next()) {
            anos_.add(rs.getString("ano"));
            categoriasHashSet.add(rs.getString("nomest"));
            //listaResultados.add(new Datos(rs.getString("ano"), rs.getString("codest"), rs.getString("valor"), rs.getString("nomest")));

        }

        Iterator<String> it = anos_.iterator();
        anos = new ArrayList();

        while (it.hasNext()) {
            anos.add(it.next());

        }
        Collections.sort(anos);

        Iterator<String> itCategoriasHashSet = categoriasHashSet.iterator();
        categorias = new ArrayList();

        while (itCategoriasHashSet.hasNext()) {
            String ca = itCategoriasHashSet.next();
            categorias.add(ca);

        }

        if (pstmt != null) {
            pstmt.close();

        }
        if (con != null) {
            con.close();
        }

        return listaResultados;
    }

    public boolean getDatosEstadisticos(String codigoEspecie, String variable, String nivelEstadistica, String estacion, String transecto, String parcela) {

        boolean terminadoconexito = false;
        org.json.JSONArray jsonArray = new org.json.JSONArray();
        org.json.JSONObject jodatos = new org.json.JSONObject();

        ConnectionFactory cf;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList especies = new ArrayList();

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");
            /*CconexionPrueba conexion = new CconexionPrueba();
            con = conexion.getConn();*/

            String sql = "select EspresionParaPivotManglar('prueba') as expresionparaIN from dual";
            //System.out.println("SQL ejecutado:"+sql);
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            String expresionIn = null;

            if (rs.next()) {
                expresionIn = rs.getString("expresionparaIN");
            }
            int nivel = Integer.parseInt(nivelEstadistica);
            switch (nivel) {
                case 3:
                case 2:
                    sql = "SELECT * "
                            + "FROM "
                            + "  (SELECT nomest, "
                            + "    ano, "
                            + "    round(" + variable + ",3) as " + variable + " "
                            + "  FROM rc_manglar_eb "
                            + "  WHERE codesp                        ='" + codigoEspecie + "' "
                            + "  AND VARIABLE                        ='AB'"
                            + "  and nivel=" + nivelEstadistica + " "
                            + "  ) pivot (SUM(NVL(" + variable + ",0)) FOR ano IN (" + expresionIn + ")) ";

                    break;
                case 4:
                    sql = "SELECT * "
                            + "FROM "
                            + "  (SELECT nomest, "
                            + "    ano, "
                            + "    round(" + variable + ",3) as " + variable + " "
                            + "  FROM rc_manglar_eb "
                            + "  WHERE codesp                        ='" + codigoEspecie + "' "
                            + "  AND VARIABLE                        ='AB'"
                            + "  and codest=" + estacion + " "
                            + "  and transecto='" + transecto + "' "
                            + "  and nivel=" + nivelEstadistica + " "
                            + "  ) pivot (SUM(NVL(" + variable + ",0)) FOR ano IN (" + expresionIn + ")) ";
                    break;
                case 1:
                    sql = "SELECT * "
                            + "FROM "
                            + "  (SELECT nomest, "
                            + "    ano, "
                            + "    round(" + variable + ",3) as " + variable + " "
                            + "  FROM rc_manglar_eb "
                            + "  WHERE codesp                        ='" + codigoEspecie + "' "
                            + "  AND VARIABLE                        ='AB'"
                            + "  and codest=" + estacion + " "
                            + "  and transecto='" + transecto + "' "
                            + "  and nivel=" + nivelEstadistica + " "
                            + "  and parcela='" + parcela + "'"
                            + "  ) pivot (SUM(NVL(" + variable + ",0)) FOR ano IN (" + expresionIn + ")) ";
                    break;
                case 5:
                    sql = "SELECT * "
                            + "FROM "
                            + "  (SELECT nomest, "
                            + "    ano, "
                            + "    round(" + variable + ",3) as " + variable + " "
                            + "  FROM rc_manglar_eb "
                            + "  WHERE VARIABLE='AB'"
                            + "  and nivel=" + nivelEstadistica + " "
                            + "  and unidad='cm' "            
                            + "  ) pivot (SUM(NVL(" + variable + ",0)) FOR ano IN (" + expresionIn + ")) ";
                    break;
                case 6:
                    sql = "SELECT * "
                            + "FROM "
                            + "  (SELECT nomest, "
                            + "    ano, "
                            + "    round(" + variable + ",3) as " + variable + " "
                            + "  FROM rc_manglar_eb "
                            + "  WHERE VARIABLE='AB'"
                            + "  and nivel=" + nivelEstadistica + " "
                            + "  ) pivot (SUM(NVL(" + variable + ",0)) FOR ano IN (" + expresionIn + ")) ";
                    break;
                case 7:
                    sql = "SELECT * "
                            + "FROM "
                            + "  (SELECT nomest, "
                            + "    ano, "
                            + "    round(" + variable + ",3) as " + variable + " "
                            + "  FROM rc_manglar_eb "
                            + "  WHERE VARIABLE='AB'"
                            + "  and nivel=" + nivelEstadistica + " "
                            + "  and transecto='" + transecto + "' "
                            + "  and codest=" + estacion + " "
                            + "  ) pivot (SUM(NVL(" + variable + ",0)) FOR ano IN (" + expresionIn + ")) ";
                    break;
                case 8:
                    sql = "SELECT * "
                            + "FROM "
                            + "  (SELECT nomest, "
                            + "    ano, "
                            + "    round(" + variable + ",3) as " + variable + " "
                            + "  FROM rc_manglar_eb "
                            + "  WHERE VARIABLE='AB'"
                            + "  and nivel=" + nivelEstadistica + " "
                            + "  and parcela='" + parcela + "'"
                            + "  and codest=" + estacion + " "
                            + "  ) pivot (SUM(NVL(" + variable + ",0)) FOR ano IN (" + expresionIn + ")) ";
                    break;

            }

            pstmt = con.prepareStatement(sql);
            //System.out.println("SQL ejecutado pivot:" + sql);
            rs = pstmt.executeQuery();



            int tamanocolumnas = rs.getMetaData().getColumnCount();
            //System.out.println("tamaniocolumnas"+tamanocolumnas);
            String cadena = "";

            arrayColumnas = new ArrayList();

            for (int i = 1; i <= tamanocolumnas; i++) {
                String columname = rs.getMetaData().getColumnName(i);
                arrayColumnas.add(columname);
                //System.out.println("coluname:" + columname);                      
            }

            arrayDatos = new ArrayList();
            while (rs.next()) {

                Iterator it = arrayColumnas.iterator();
                String Auxcadena = "";
                while (it.hasNext()) {
                    String nombrecolumna = (String) it.next();
                    //System.out.print("nombre columna en array:"+nombrecolumna);
                    String dato = rs.getString(nombrecolumna);

                    if (dato == null) {
                        dato = "0";
                    }

                    //System.out.println(" dato:"+dato);
                    Auxcadena += dato + ",";
                    // System.out.println(" registro:" + Auxcadena); 


                }
                arrayDatos.add(Auxcadena);
                //System.out.println("registro:" + Auxcadena);
            }

            Iterator it1 = arrayDatos.iterator();
            while (it1.hasNext()) {
                String registro = (String) it1.next();
                //System.out.println("registro:" + registro);
            }

            if (pstmt != null) {
                pstmt.close();

            }
            if (con != null) {
                con.close();
            }
            terminadoconexito = true;
        } catch (Exception e) {
            System.out.println("Componente Manglares. Error obteniendo la tabla de valores graficados:" + e.toString());

        } finally {
        }



        return terminadoconexito;
    }

    public static void main(String[] args) {
        DatosEstructurales datosestructurales = new DatosEstructurales();
        //boolean resultadoproceso = de.getDatosEstadisticos("2040.55.685", "IVI");

        /*ArrayList lista = datosestructurales.getEstaciones();
         Iterator it = lista.iterator();

         while (it.hasNext()) {
         Estacion estaciones = (Estacion) it.next();
         System.out.println("<option value='" + estaciones.getCodigo() + "'>" + estaciones.getNombre() + "</option>");
         }*/

        /*ArrayList lista = datosestructurales.getTransectos("15611");
         Iterator it = lista.iterator();

         while (it.hasNext()) {
         Transectos transectos = (Transectos) it.next();
         System.out.println("<option value='" + transectos.getCodigo() + "'>" + transectos.getNombre() + "</option>");
         }*/

        ArrayList lista = datosestructurales.getParcelas("15611", "A");
        Iterator it = lista.iterator();

        while (it.hasNext()) {
            Parcelas parcela = (Parcelas) it.next();
            System.out.println("<option value='" + parcela.getCodigo() + "'>" + parcela.getNombre() + "</option>");
        }

        /*String html = "<table>";
         if(resultadoproceso){
         ArrayList arraycolumnas= de.getArrayColumnas();
         html+="<tr>";
         for (int i = 1; i < arraycolumnas.size(); i++) {
         String nombrecolumna = (String) arraycolumnas.get(i);
         html+="<td>"+nombrecolumna+"</td>";
         }
         html+="</tr>";
         }else{
         html+= "<tr><td>Un Error ha ocurrido en la generacion de la tabla resumen.</td></tr>";
         }
         html+="</table>";
         System.out.println(html);*/
    }

    public Connection getConnection() {
        ConnectionFactory cf;
        Connection con = null;

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

        } catch (Exception e) {
            System.out.println("Componente Manglares. Error conectandose a la base de datos de monitoreo:" + e.toString());
        }
        return con;
    }
}
