package co.org.invemar.siam.manglares.model;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.*;

public class DashBoardEstadisticasManglares {

  
    private Connection con = null;   
    private co.org.invemar.siam.ConnectionFactory conexion=null;


    public DashBoardEstadisticasManglares() {
        super();
        try {
            conexion = new co.org.invemar.siam.ConnectionFactory();
            con = conexion.createConnection("argos");
        } catch (SQLException ex) {
            System.out.println("Error conectandose a la db argos:"+ex.toString());
        }
        

    }

    public String getDataDashboardManglares() {
        String result = null;
        String sql = "SELECT NIVEL, "
                + "  ANO, "
                + "  TEMPORADA, "
                + "  DEPTO, "
                + "  SECTOR, "
                + "  NOMEST, "
                + "  VARIABLE, "
                + "  PROM, "
                + "  round(NUM,2) as NUM, "
                + "  round(MAX,2) as MAX, "
                + "  round(MIN,2) as MIN, "
                + "  ROUND(STDDV,2) AS STDDV, "
                + "  PRJ, "
                + "  REGION, "
                + "  CODEST, "               
                + "  VARIANZA, "                        
                + "  TIPO_SUSTRATO, "
                + "  PARCELA, "
                + "  SUSTRATO, "               
                + "  UNIDAD, "
                + "  CODESP, "
                + "  ESPECIE, "
                + "  ROUND(DENSIDAD,2) AS DENSIDAD, "
                + "  ROUND(AB_MH,2) AS  AB_MH,"
                + "  ROUND(ABUND_REL,2) AS ABUND_REL, "
                + "  ROUND(FREC_REL, 2) AS FREC_REL,"
                + "  ROUND(DOM_REL,2) AS DOM_REL, "
                + "  ROUND(IVI,2) AS IVI, "
                + "  ROUND(FREC_ABS,2) AS FREC_ABS, "
                + "  ROUND(NUM_TOTAL,2) AS NUM_TOTAL,VARIABLES "
                + "FROM RC_MANGLAR_EB where nivel=2  and (variable='AB' or variable='DAP') and ab_mh is not null  ";
        PreparedStatement pstmt = null;
        result = "[['NIVEL','ANO','TEMPORADA','DEPTO','SECTOR','NOMEST','VARIABLE','PROM',"
                + "'NUM','MAX','MIN',"
                + "'STDDV','PRJ','REGION','CODEST',"
                + "'PARCELA','ESPECIE','DENSIDAD','AB_MH','ABUND_REL','FREC_REL','DOM_REL','IVI','FREC_ABS','NUM_TOTAL','VARIABLES'],";
        //result="[['ID_ESTACION','ESTACION','SECTOR','ID_PROYECTO','NOMPROYECTO','ZONA'],";

        try {           
            //con = conexion.createConnection("argos");
            pstmt = con.prepareStatement(sql);
           // System.out.println("sql:"+sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                // System.out.println("valor en revision::::::::::"+rs.getString("FA_< 0,063 mm"));

                result += "['" + rs.getString("NIVEL") + "'," + rs.getString("ANO") + ","
                        + "'" + rs.getString("TEMPORADA") + "','" + rs.getString("DEPTO") + "','" + rs.getString("SECTOR") + "',"
                        + "'" + rs.getString("NOMEST") + "','" + rs.getString("VARIABLE") + "'," + rs.getString("PROM") + ","
                        + "" + rs.getString("NUM") + "," + rs.getString("MAX") + ","
                        + "" + rs.getString("MIN") + "," + rs.getString("STDDV") + ","
                        + "'" + rs.getString("PRJ") + "','" + rs.getString("REGION") + "',"
                        + "" + rs.getString("CODEST") + "," + rs.getString("PARCELA") + ","
                        + "'" + rs.getString("ESPECIE") + "'," + rs.getString("DENSIDAD") + ","
                        + rs.getString("AB_MH") + "," + rs.getString("ABUND_REL") + "," + rs.getString("FREC_REL") + ", "
                        + rs.getString("DOM_REL") + "," + rs.getString("IVI") + "," + rs.getString("FREC_ABS") + "," + rs.getString("NUM_TOTAL")+",'"+rs.getString("VARIABLES")+"'],";

                // result="["+rs.getString("ID_ESTACION")+",'"+rs.getString("ESTACION")+"','"+rs.getString("SECTOR")+"',"+rs.getString("ID_PROYECTO")+",'"+rs.getString("NOMPROYECTO")+"','"+rs.getString("ZONA")+"'],";

            }


            int posicion = result.length() - 1;
            result = result.substring(0, posicion) + "]";
            //System.out.println(result);
            //System.out.println("tamano cadena:"+result.length());
        } catch (SQLException e) {
            System.out.println("Error:" + e.getMessage());

        } finally {

            try {
                con.close();
                con = null;
            } catch (SQLException e) {
                System.out.println("Error:" + e.getMessage());
            }


        }

        return result;
    }
    /*public String getVariables(){
        DatosEstructurales de = new DatosEstructurales();
        return de.getJSONVariables();
    }*/
    public static void main(String[] args) {
        DashBoardEstadisticasManglares dashBoardEstadisticasCostero = new DashBoardEstadisticasManglares();
       // dashBoardEstadisticasCostero.getVariables();
        //System.out.println(dashBoardEstadisticasCostero.getDataDashboardManglares());
    }
}
