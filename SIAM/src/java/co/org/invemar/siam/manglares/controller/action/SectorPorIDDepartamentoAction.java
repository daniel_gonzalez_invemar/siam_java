/**
 *
 */
package co.org.invemar.siam.manglares.controller.action;

import co.org.invemar.siam.manglares.model.DatosEstructurales;
import co.org.invemar.siam.manglares.vo.Datos;
import co.org.invemar.siam.manglares.vo.Departamento;
import co.org.invemar.siam.manglares.vo.Parcelas;
import co.org.invemar.siam.manglares.vo.Sectores;
import co.org.invemar.siam.manglares.vo.Transectos;
import co.org.invemar.util.actions.Action;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import co.org.invemar.util.actions.ActionRouter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Administrador
 *
 */
public class SectorPorIDDepartamentoAction implements Action{

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        response.setContentType("text/html charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();
        
        String nombredepartamento = request.getParameter("nombredepartamento"); 
        //System.out.println("nombredepartamento:"+nombredepartamento);

        DatosEstructurales estructuramangle = new DatosEstructurales();
        ArrayList departamentos = estructuramangle.getSectores(nombredepartamento);
        Iterator it = departamentos.iterator();
        String html="<select id='nivelsector' name='nivelsector' onchange='getProyectos()'>";
         html=html+"<option value='-'>-</option>";
        while(it.hasNext()){
             Datos departamento = (Datos) it.next();
             html=html+"<option value='"+departamento.getCodigo()+"'>"+departamento.getNombre()+"</option>";
            
        }
        html+="</select>";      
        
       
        pw.write(html);
        //System.out.println("html:"+html);
        return null;//new ActionRouter("/sibm/ficha_catalogo.jsp?" + request.getQueryString());
    }
}
