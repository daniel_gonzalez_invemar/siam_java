/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.manglares.vo;

import java.io.Serializable;

/**
 *
 * @author usrsig15
 */
public class Datos  implements Serializable{
    protected String nombre;
    protected String codigo;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    
   public Datos(){
       
   }

   public Datos(String nombre, String codigo){
       this.nombre = nombre;
       this.codigo = codigo;
   }

    
    
   
}
