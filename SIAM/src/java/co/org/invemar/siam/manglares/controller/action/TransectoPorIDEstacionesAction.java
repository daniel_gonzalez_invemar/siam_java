/**
 *
 */
package co.org.invemar.siam.manglares.controller.action;

import co.org.invemar.siam.manglares.model.DatosEstructurales;
import co.org.invemar.siam.manglares.vo.Transectos;
import co.org.invemar.util.actions.Action;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import co.org.invemar.util.actions.ActionRouter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Administrador
 *
 */
public  class TransectoPorIDEstacionesAction implements Action{

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        response.setContentType("text/html charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();
        
        String codigoEstaciones = request.getParameter("codestacion");        

        DatosEstructurales estructuramangle = new DatosEstructurales();
        ArrayList transectos = estructuramangle.getTransectos(codigoEstaciones);
        Iterator it = transectos.iterator();
        String html="<select id='niveltransecto' name='niveltransecto' onchange='getComboBoxParcelas()'>";
        html=html+"<option value='-'>-</option>";
        while(it.hasNext()){
            Transectos transecto = (Transectos) it.next();
             html=html+"<option value='"+transecto.getCodigo()+"'>"+transecto.getNombre()+"</option>";
            
        }
        html+="</select>";      
        
       
        pw.write(html);
        //System.out.println("html:"+html);
        return null;//new ActionRouter("/sibm/ficha_catalogo.jsp?" + request.getQueryString());
    }
}
