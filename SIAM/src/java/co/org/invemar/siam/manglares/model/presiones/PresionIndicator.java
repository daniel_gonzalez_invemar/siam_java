package co.org.invemar.siam.manglares.model.presiones;


public class PresionIndicator {
    private int idPresion;
    private String presionName;
    private int frecuency;
    private double value;

    public PresionIndicator(){
        
    }
    public PresionIndicator(String presionName, double value){
        this.presionName= presionName;
        this.value= value;
    }

    public int calculusFrecuency(int numberPresionFinded) {
        if (numberPresionFinded > 1 && numberPresionFinded <= 5) {
            this.frecuency = 2;
        } else if (numberPresionFinded > 5) {
            this.frecuency = 3;
        } else {
            this.frecuency = 1;
        }
        return frecuency;
    }

    public int getFrecuency() {
        return frecuency;
    }

    public void setFrecuency(int frecuency) {
        this.frecuency = frecuency;
    }

    public int getIdPresion() {
        return idPresion;
    }

    public void setIdPresion(int idPresion) {
        this.idPresion = idPresion;
    }

    public String getPresionName() {
        return presionName;
    }

    public void setPresionName(String presionName) {
        this.presionName = presionName;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
