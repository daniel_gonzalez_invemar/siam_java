/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.manglares.controller.action;

import co.org.invemar.siam.manglares.model.DatosEstructurales;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usrsig15
 */
public class TablaEstadisticosDeVariableAction implements Action {

    public ActionRouter execute(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        response.setContentType("text/html charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();

        String variable = request.getParameter("variable");
        String codigoespecie = request.getParameter("codigoespecie");
        String nivelEstadistica = request.getParameter("nivelestadistica");
        String estacion = request.getParameter("estacion");
        String transecto= request.getParameter("transecto");
        String parcela= request.getParameter("parcela");
        
        DatosEstructurales estructuramangle = new DatosEstructurales();

        boolean resultadoproceso = estructuramangle.getDatosEstadisticos(codigoespecie,variable,nivelEstadistica,estacion,transecto,parcela);
        String html = "<div id='paneltabla'>";
        html = html+"<table id='tabladatos'  cellSpacing='1' cellPadding='1'>";
        if (resultadoproceso) {
            ArrayList arrayColumnas = estructuramangle.getArrayColumnas();
            html += "<tr  id='headertabla' >";
            Iterator it = arrayColumnas.iterator();
            while (it.hasNext()) {
                String nombrecolumna = (String) it.next();
                html += "<th>" + nombrecolumna + "</th>";
            }
            html += "</tr>";

            ArrayList arrayDatos = estructuramangle.getArrayDatos();
            Iterator itdatos = arrayDatos.iterator();

            while (itdatos.hasNext()) {
                html += "<tr class='rowtabla'>";
                String registro = (String) itdatos.next();
                String columnadatos[] = registro.split(",");
                for (int i = 0; i < columnadatos.length; i++) {
                    String dato = columnadatos[i];
                    html += "<td>" + dato + "</td>";
                }
                html += "</tr>";
            }


        } else {
            html += "<tr><td>Un Error ha ocurrido en la generacion de la tabla resumen.</td></tr>";
        }
        html += "</table></div>";
        //System.out.println(html);
        pw.println(html);

        return null;
    }
}
