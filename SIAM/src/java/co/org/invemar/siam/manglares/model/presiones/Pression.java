/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.siam.manglares.model.presiones;

import co.org.invemar.library.ushahidi.Category;

/**
 *
 * @author usrsig15
 */
public class Pression extends Category {
    private int Intensity = 0;
    private String intensityDescripcion;
    private int irreversibilty = 0;
    private String irreversibilityDescription;
    private int extension = 0;
    private String extensioDescription;
    private int ocurrencia = 0;
    private String ocurrenciaDescription;


    public int getIntensity() {
        return Intensity;
    }

    public void setIntensity(int Intensity) {
        this.Intensity = Intensity;
    }

    public int getIrreversibilty() {
        return irreversibilty;
    }

    public void setIrreversibilty(int irreversibilty) {
        this.irreversibilty = irreversibilty;
    }

    public int getExtension() {
        return extension;
    }

    public void setExtension(int extension) {
        this.extension = extension;
    }

    public int getOcurrencia() {
        return ocurrencia;
    }

    public void setOcurrencia(int ocurrencia) {
        this.ocurrencia = ocurrencia;
    }

    public String getIntensityDescripcion() {
        return intensityDescripcion;
    }

    public void setIntensityDescripcion(String intensityDescripcion) {
        this.intensityDescripcion = intensityDescripcion;
    }

    public String getIrreversibilityDescription() {
        return irreversibilityDescription;
    }

    public void setIrreversibilityDescription(String irreversibilityDescription) {
        this.irreversibilityDescription = irreversibilityDescription;
    }

    public String getExtensioDescription() {
        return extensioDescription;
    }

    public void setExtensioDescription(String extensioDescription) {
        this.extensioDescription = extensioDescription;
    }

    public String getOcurrenciaDescription() {
        return ocurrenciaDescription;
    }

    public void setOcurrenciaDescription(String ocurrenciaDescription) {
        this.ocurrenciaDescription = ocurrenciaDescription;
    }

}
