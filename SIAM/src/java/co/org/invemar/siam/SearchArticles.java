package co.org.invemar.siam;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import co.org.invemar.siam.model.SearchDataManager;
import co.org.invemar.util.ConnectionFactory;

public class SearchArticles extends HttpServlet {

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		ConnectionFactory cFactory=null;
		Connection conn=null;
		SearchDataManager dm=new SearchDataManager();
		
		cFactory=new ConnectionFactory();
		
		String result=null;
		String action=request.getParameter("action");
		if(action.equals("search")){
			String text1=request.getParameter("text1");
			//System.out.println("text1: "+text1);
			String text2=request.getParameter("text2");
			//System.out.println("text2: "+text2);
			String operator=request.getParameter("operator");
			try {
				conn = cFactory.createConnection();
				result=dm.searchDatas(conn, text1, text2, operator);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				if(conn!=null){
					ConnectionFactory.closeConnection(conn);
				}
			}
		}else if(action.equals("allrecord")){
			try {
				conn = cFactory.createConnection();
				result=dm.countAllRecord(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				if(conn!=null){
					ConnectionFactory.closeConnection(conn);
				}
			}
		}else if(action.equals("ldocs")){
			try {
				conn = cFactory.createConnection();
				result=dm.findLastDocs(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				if(conn!=null){
					ConnectionFactory.closeConnection(conn);
				}
			}
		}
		response.setContentType("text/text; charset=ISO-8859-1");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(result);
		
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request,response);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occure
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
