package co.org.invemar.siam.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ListadoEspExcel {
		
	public HSSFWorkbook findListadoEspeciesEXCEL(Connection conn,String query,Object[] parameters)
			throws SQLException {
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("Hoja de datos");
		HSSFRow row = null;
		int i = 1;
		int j = 0;
		
		preparedStatement = conn.prepareStatement(query);
		this.fillStatement(preparedStatement, parameters);
		
		
		try {
			result = preparedStatement.executeQuery();
			row = sheet.createRow((short) 0);
			
			createCell("FAMILIA", wb, row, (short) j++);
			createCell("GENERO", wb, row, (short) j++);
			createCell("NOMBRE CIENTIFICO", wb, row, (short) j++);
			createCell("NOMBRE COMUN", wb, row, (short) j++);
			createCell("UICN NACIONAL", wb, row, (short) j++);
			createCell("CITES", wb, row, (short) j++);
			createCell("PRIMERA FECHA CAPTURA", wb, row, (short) j++);
			createCell("ULTIMA FECHA CAPTURA", wb, row, (short) j++);
			createCell("FRECUENCIA", wb, row, (short) j++);
			
			while (result.next()) {
				row = sheet.createRow((short) i);
				j = 0;
				createCell(isNull(result.getString("FAMILIA")), wb, row,
						(short) j++);
				createCell(isNull(result.getString("GENERO")), wb, row,
						(short) j++);
				createCell(isNull(result.getString("nombrecientifico")), wb, row,
						(short) j++);
				createCell(isNull(result.getString("nombre_comun")), wb, row,
						(short) j++);
				createCell(isNull(result.getString("UICN_NACIONAL")), wb, row,
						(short) j++);
				createCell(isNull(result.getString("CITES")), wb, row,
						(short) j++);
				createCell(isNull(result.getString("fechamin")), wb, row,
						(short) j++);
				createCell(isNull(result.getString("fechamax")), wb, row,
						(short) j++);
				createCell(result.getInt("frecuencia"), wb, row, (short) j++);
				

				i++;
			}
			
		} finally {
			if (result != null)
				result.close();
			if (preparedStatement != null)
				preparedStatement.close();

		}

		return wb;

	}
	
	
	private void fillStatement(PreparedStatement stmt, Object[] params)
    throws SQLException {

    if (params == null) {
        return;
    }

    for (int i = 0; i < params.length; i++) {
        if (params[i] != null) {
            stmt.setObject(i + 1, params[i]);
        } else {
            // VARCHAR works with many drivers regardless
            // of the actual column type.  Oddly, NULL and 
            // OTHER don't work with Oracle's drivers.
            stmt.setNull(i + 1, Types.VARCHAR);
        }
    }
	}
	
	/**
	 * Creates a cell and aligns it a certain way.
	 * 
	 * @param wb
	 *            the workbook
	 * @param row
	 *            the row to create the cell in
	 * @param column
	 *            the column number to create the cell in
	 * @param align
	 *            the alignment for the cell.
	 */
	private static void createCell(String value, HSSFWorkbook wb, HSSFRow row,
			short column) {
		HSSFCell cell = row.createCell(column);
		cell.setCellValue(new HSSFRichTextString(value));
		/*
		 * HSSFCellStyle cellStyle = wb.createCellStyle();
		 * cellStyle.setAlignment(align); cell.setCellStyle(cellStyle);
		 */
	}
	private static void createCell(int value, HSSFWorkbook wb, HSSFRow row,
			short column/* , short align */) {
		HSSFCell cell = row.createCell(column);
		cell.setCellValue(value);
		/*
		 * HSSFCellStyle cellStyle = wb.createCellStyle();
		 * cellStyle.setAlignment(align); cell.setCellStyle(cellStyle);
		 */
	}

	private String isNull(String str) {
		String cadena = "";
		if (str != null) {
			cadena = str;
		}
		return cadena;
	}

	public boolean isEmpty(String str) {
		return str == null || str.length() == 0 || str.equals("null");
	}

}
