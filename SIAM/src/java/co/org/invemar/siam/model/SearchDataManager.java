/**
 * 
 */
package co.org.invemar.siam.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Julian Jose Pizarro Pertuz
 *
 */
public class SearchDataManager {
	
	public String searchDatas(Connection conn,String text1, String text2, String operator) throws SQLException, JSONException{
		
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		JSONObject objsData = null;
		JSONArray arrayData = new JSONArray();
		
		String s="select ar.narticulo_ID,ar.id_cat,ar.titulo,  substr(ar.claves, 9) pclaves "+ 
				 "	from narticulos ar "+
				 "  where lower(ar.claves) like 'siam**%'  ";
		StringBuffer SQL=new StringBuffer(s);
		//SQL.append()
		
		
		if((text1!=null && text2!=null) && (text1!="" && text2!="")){
				//System.out.println("diferentes ambos");
			SQL.append("and (lower(ar.resumen) like lower('%"+text1+"%') " +operator+ " lower(ar.resumen) like lower ('%"+text2+"%')  "+
                              "or lower(ar.titulo) like lower('%"+text1+"%') "+operator+" lower(ar.resumen) like lower ('%"+text2+"%') "+
                               "or lower(ar.claves) like lower('%"+text1+"%') "+operator+" lower(ar.resumen) like lower ('%"+text2+"%'))");
		}else if (text1!=null && text1!=""){
			//System.out.println("diferentes text1");
			SQL.append("and (lower(ar.resumen) like lower('%"+text1+"%') or lower(ar.titulo) like lower('%"+text1+"%') "+ 
						     " or lower(ar.claves) like lower('%"+text1+"%'))");
		} else if (text2!=null && text2!=""){
			//System.out.println("diferentes text2");
			SQL.append("and (lower(ar.resumen) like lower('%"+text2+"%') or lower(ar.titulo) like lower('%"+text2+"%') "+ 
				     " or lower(ar.claves) like lower('%"+text2+"%'))");
		}
		
		SQL.append(" order by ar.titulo ");
		System.out.println("SQL: "+SQL.toString());
		try {
			pstmt = conn.prepareStatement(SQL.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				objsData=new JSONObject();
				objsData.put("articulo", rs.getString("NARTICULO_ID"));
				//objsData.put("idcat", rs.getString("ID_CAT"));
				objsData.put("titulo", rs.getString("TITULO"));
				//objsData.put("categoria", rs.getString("CATEGORIA"));
				objsData.put("pcalves", rs.getString("PCLAVES"));
				
				arrayData.put(objsData);
			}
			
			
			
		} finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		
		
		
		return arrayData.toString();
	}
	
	public String countAllRecord(Connection conn) throws SQLException, JSONException{
		
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		JSONObject objsData = null;
		
		String sql="  select count(*) allrecord "+
					" from narticulos ar "+
					" where lower(ar.claves) like 'siam**%' ";
                System.out.println("count all record sql:"+sql);
		
		try {
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				objsData=new JSONObject();
				objsData.put("allrecord", rs.getString(1));
			}else return "[]";
			//System.out.println("return countAllRecord:.. "+objsData.toString());
			return objsData.toString();
		} finally{
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
	}
	
	public String findLastDocs(Connection conn) throws SQLException, JSONException{
		
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		JSONObject objsData = null;
		JSONArray arrayData = new JSONArray();
		
		String query="SELECT narticulo_ID, id_cat,UPPER(titulo) titulo,fecha "
					+" FROM ( "  
					+"   SELECT ar.narticulo_ID,ar.id_cat,ar.titulo, ar.claves pclaves,ar.fecha, " 
					+"		RANK() OVER (ORDER BY ar.fecha DESC  ) as ranking "
					+"	 FROM narticulos ar "
					+"	 WHERE lower(ar.claves) like lower('siam**%')  ) " 
					+" WHERE ranking < 4 and ROWNUM<4 "
					+" ORDER BY fecha DESC ";
                System.out.println("fin last doc sql:"+query);
		try {
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				objsData=new JSONObject();
				objsData.put("articulo", rs.getString("NARTICULO_ID"));
				objsData.put("idcat", rs.getString("ID_CAT"));
				objsData.put("titulo", rs.getString("TITULO"));
				objsData.put("fecha", rs.getString("fecha"));
				
				arrayData.put(objsData);
			}
		} finally{
			if (rs != null)	rs.close();
			if (pstmt != null)pstmt.close();
		}
		return arrayData.toString();
	}

}
