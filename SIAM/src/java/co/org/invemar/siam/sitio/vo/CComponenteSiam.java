// Decompiled by DJ v3.12.12.96 Copyright 2011 Atanas Neshkov  Date: 13/03/2012 02:20:34 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   CComponenteSiam.java

package co.org.invemar.siam.sitio.vo;

import java.io.Serializable;

public class CComponenteSiam
    implements Serializable
{

    public CComponenteSiam()
    {
    }

    public String getAreaTrabajo()
    {
        return areaTrabajo;
    }

    public void setAreaTrabajo(String areaTrabajo)
    {
        this.areaTrabajo = areaTrabajo;
    }

    public String getCargo()
    {
        return cargo;
    }

    public void setCargo(String cargo)
    {
        this.cargo = cargo;
    }

    public Long getComponentePrincipal()
    {
        return componentePrincipal;
    }

    public void setComponentePrincipal(Long componentePrincipal)
    {
        this.componentePrincipal = componentePrincipal;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Long getEmpleadoId()
    {
        return empleadoId;
    }

    public void setEmpleadoId(Long empleadoId)
    {
        this.empleadoId = empleadoId;
    }

    public String getExtension()
    {
        return extension;
    }

    public void setExtension(String extension)
    {
        this.extension = extension;
    }

    public String getFuncionario()
    {
        return funcionario;
    }

    public void setFuncionario(String funcionario)
    {
        this.funcionario = funcionario;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCompleto()
    {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto)
    {
        this.nombreCompleto = nombreCompleto;
    }

    public String getNombreCorto()
    {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto)
    {
        this.nombreCorto = nombreCorto;
    }

    public String getNombredep()
    {
        return nombredep;
    }

    public void setNombredep(String nombredep)
    {
        this.nombredep = nombredep;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public void setUrlConceptual(String newurlConceptual)
    {
        urlConceptual = newurlConceptual;
    }

    public String getUrlConceptual()
    {
        return urlConceptual;
    }

    public String getHtmlnoticia1()
    {
        return htmlnoticia1;
    }

    public void setHtmlnoticia1(String htmlnoticia1)
    {
        this.htmlnoticia1 = htmlnoticia1;
    }

    public String getTitulonoticia1()
    {
        return titulonoticia1;
    }

    public void setTitulonoticia1(String titulonoticia1)
    {
        this.titulonoticia1 = titulonoticia1;
    }

    public String getHtmlnoticia2()
    {
        return htmlnoticia2;
    }

    public void setHtmlnoticia2(String htmlnoticia2)
    {
        this.htmlnoticia2 = htmlnoticia2;
    }

    public String getTitulonotica2()
    {
        return titulonotica2;
    }

    public void setTitulonotica2(String titulonotica2)
    {
        this.titulonotica2 = titulonotica2;
    }

    public String getInformacionProyecto()
    {
        return informacionProyecto;
    }

    public void setInformacionProyecto(String informacionProyecto)
    {
        this.informacionProyecto = informacionProyecto;
    }

    public String getCodigoUA()
    {
        return codigoUA;
    }

    public void setCodigoUA(String codigoUA)
    {
        this.codigoUA = codigoUA;
    }

    public String getNombreFilial()
    {
        return nombreFilial;
    }

    public void setNombreFilial(String nombreFilial)
    {
        this.nombreFilial = nombreFilial;
    }

    public String getUrlAyudaComponentes()
    {
        return urlAyudaComponentes;
    }

    public void setUrlAyudaComponentes(String urlAyudaComponentes)
    {
        this.urlAyudaComponentes = urlAyudaComponentes;
    }

    public String getFechaultimaActualizacionComponente()
    {
        return fechaultimaActualizacionComponente;
    }

    public void setFechaultimaActualizacionComponente(String fechaultimaActualizacionComponente)
    {
        this.fechaultimaActualizacionComponente = fechaultimaActualizacionComponente;
    }

    public String getUrlAyuda()
    {
        return urlAyuda;
    }

    public void setUrlAyuda(String urlAyuda)
    {
        this.urlAyuda = urlAyuda;
    }

    public String getContador() {
        return contador;
    }

    public void setContador(String contador) {
        this.contador = contador;
    }
    
    private String areaTrabajo;
    private String cargo;
    private Long componentePrincipal;
    private String email;
    private Long empleadoId;
    private String extension;
    private String funcionario;
    private Long id;
    private String nombreCompleto;
    private String nombreCorto;
    private String nombredep;
    private String url;
    private String urlConceptual;
    private String htmlnoticia1;
    private String titulonoticia1;
    private String htmlnoticia2;
    private String titulonotica2;
    private String informacionProyecto;
    private String codigoUA;
    private String nombreFilial;
    private String urlAyudaComponentes;
    private String fechaultimaActualizacionComponente;
    private String urlAyuda;
    private String contador;

    
    
}
