// Decompiled by DJ v3.12.12.96 Copyright 2011 Atanas Neshkov  Date: 13/03/2012 02:26:05 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   CComponentensSiamDAO.java
package co.org.invemar.siam.sitio.model;

import co.org.invemar.siam.sitio.vo.CComponenteSiam;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CComponentensSiamDAO {

    public CComponentensSiamDAO() {
        
    }

    public ArrayList listaContactos(String idSitio) {
        String sql;
        ArrayList lista;
        sql = (new StringBuilder()).append("select * from VSIAM_COMPONENTES where componente_principal=").append(idSitio).toString();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        lista = new ArrayList();

        try {
            co.org.invemar.siam.ConnectionFactory cf = new co.org.invemar.siam.ConnectionFactory();
            con = cf.createConnection("sibm");
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                lista.add(cargaRS(rs));

            }

        } catch (Exception ex) {
            System.out.println("Error obteniendo la lista de conctactos:" + ex.toString());
        }


        try {
            if (con != null) {


                con.close();
                con = null;
            }
        } catch (Exception ex) {
            System.out.println((new StringBuilder()).append("Error cerrando la conexion:").append(ex.getMessage()).toString());
        }

        return lista;
    }

    public ArrayList AdministradoresComponente(String idSitio) {
        String sql;

        sql = "select * from VSIAM_COMPONENTES where id=" + idSitio;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList lista = new ArrayList();
        try {
            co.org.invemar.siam.ConnectionFactory cf = new co.org.invemar.siam.ConnectionFactory();
            con = cf.createConnection("sibm");
            //System.out.println("sql execute:"+sql);
            //System.out.println("size:"+lista.size());
            pstmt = con.prepareStatement(sql);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                lista.add(cargaRS(rs));
            }
            //System.out.println("size:"+lista.size());


        } catch (Exception ex) {
            System.out.println("Error obteniendo los datos del componente siam:" + ex.toString());
            /*
             * if (e.getErrorCode() == 28002) { System.out.println((new
             * StringBuilder()).append("Error
             * codigo:").append(e.getErrorCode()).toString());
             }
             */
        }

        try {
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (Exception ex) {
            System.out.println((new StringBuilder()).append("Error cerrando la conexion:").append(ex.getMessage()).toString());
        }

        return lista;
    }

    private CComponenteSiam cargaRS(ResultSet rs) {
        CComponenteSiam componentesiam = new CComponenteSiam();
        try {
            componentesiam = new CComponenteSiam();
            componentesiam.setFuncionario(rs.getString("funcionario"));
            componentesiam.setAreaTrabajo(rs.getString("area_trabajo"));
            componentesiam.setCargo(rs.getString("cargo"));
            componentesiam.setEmail(rs.getString("email"));
            componentesiam.setExtension(rs.getString("extension"));
            componentesiam.setNombreCompleto(rs.getString("nombre_completo"));
            componentesiam.setNombreCorto(rs.getString("nombre_corto"));
            componentesiam.setUrl(rs.getString("url"));
            componentesiam.setNombredep(rs.getString("NOMBREDEP"));
            componentesiam.setUrlConceptual(rs.getString("URL_CONCEPTUAL"));
            componentesiam.setTitulonoticia1(rs.getString("narticulo01_titulo"));
            componentesiam.setHtmlnoticia1(rs.getString("narticulo01_url"));
            componentesiam.setTitulonotica2(rs.getString("narticulo02_titulo"));
            componentesiam.setHtmlnoticia2(rs.getString("narticulo02_url"));
            componentesiam.setInformacionProyecto(rs.getString("inf_proyecto"));
            componentesiam.setCodigoUA(rs.getString("codigo_ua"));
            componentesiam.setNombreFilial(getNombreFilial(rs.getString("NOMBRE_FILIAL").split(",")));
            SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MM/yyyy");
            StringBuilder fechaactualizacion = new StringBuilder(dateformatYYYYMMDD.format(rs.getDate("FACTUALIZACION")));
            componentesiam.setFechaultimaActualizacionComponente(fechaactualizacion.toString());
            componentesiam.setUrlAyuda(rs.getString("URL_AYUDA"));
            //componentesiam.setContador(rs.getString("contador"));
        } catch (SQLException e) {
            System.out.println("Error cargando resulset:" + e.toString());
        }
        return componentesiam;
    }

    public static void main(String args[]) {
        CComponentensSiamDAO cComponentensSIamDAO = new CComponentensSiamDAO();
        ArrayList list = cComponentensSIamDAO.AdministradoresComponente("120");
        /*CComponenteSiam componente = (CComponenteSiam) list.get(0);
         System.out.println((new StringBuilder()).append("fecha de actualizacion:").append(componente.getFechaultimaActualizacionComponente()).toString());
         System.out.println((new StringBuilder()).append("url ayuda:").append(componente.getUrlAyuda()).toString());
         componente = (CComponenteSiam) cComponentensSIamDAO.listaContactos("0").get(0);
         System.out.println((new StringBuilder()).append("contacto:").append(componente.getFuncionario()).toString());*/
    }

    public String getMessage() {
        return message;
    }

    private String getNombreFilial(String v[]) {
        StringBuffer result = new StringBuffer();
        if (v.length == 1) {
            result.append(v[0]);
        } else if (v.length > 1) {
            for (int i = 1; i < v.length; i++) {
                result.append(" ");
                result.append(v[i]);
                result.append("<br/> ");
            }

        }
        return result.toString();
    }
   
    private String message;
    private Connection con;
}
