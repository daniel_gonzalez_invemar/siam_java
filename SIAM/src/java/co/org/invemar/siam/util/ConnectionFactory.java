// Decompiled by DJ v3.12.12.96 Copyright 2011 Atanas Neshkov  Date: 13/03/2012 04:14:02 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ConnectionFactory.java

package co.org.invemar.siam.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.*;
import javax.sql.DataSource;

public class ConnectionFactory
{
    private String nombrearchivocrendenciales=null;

    public ConnectionFactory()
    {
    }

    public synchronized DataSource getDataSource()
    {
        try
        {
            Context envContext = (Context)initialContext.lookup("java:comp/env");
            DataSource data = (DataSource)envContext.lookup("jdbc/sibm");
            return data;
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    public synchronized Connection createConnection()
    {
        Connection con = null;
        String usuario = null;
        String password = null;
        //String url      = null;
        try
        {
            //Properties prop = new Properties();
           // InputStream in = this.getClass().getResourceAsStream(nombrearchivocrendenciales + ".properties");
            //prop.load(in);

            /*url = prop.getProperty("url");
            usuario = prop.getProperty("usuario");
            idperfil = prop.getProperty("password");
            lectorfeed = new CLectorFeedGoogle();*/
            
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@192.168.3.203:1521:sci";
            con = DriverManager.getConnection(url, "publico", "publico");
        }
        catch(ClassNotFoundException e)
        {
            System.out.println("Error conexion:"+e.toString());
        }        
        catch(Exception e)
        {
            System.out.println((new StringBuilder()).append("Error conection:").append(e.getMessage()).toString());
        }
        if(con != null)
            System.out.println("con no es null");
        return con;
    }

    public static void closeConnection(Connection connection)
    {
        if(connection != null)
        {
            try
            {
                connection.close();
            }
            catch(SQLException e) { }
            connection = null;
        }
    }

    static InitialContext initialContext;
    private String Error;

    static 
    {
        try
        {
            initialContext = new InitialContext();
        }
        catch(NamingException ne) { }
    }
}
