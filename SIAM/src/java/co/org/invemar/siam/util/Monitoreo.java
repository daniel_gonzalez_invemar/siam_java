/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.util;

import java.io.Serializable;

/**
 *
 * @author usrsig15
 */
public class Monitoreo implements Serializable{
    private String fecha;
    private String idEstacion;
    
    private String Latitud;
    private String longitud;
    private String nombreEstacion;
    private String metodoMuestreo;
    private String tematica;
    private String variable;
    private String valor;
    private String valorDos;
    private String metodoAnalitico;
    private String idProyecto;
    private String nombreProyecto;
    private String claseSustrato;
    private String nombreClaseSustrato;
    private String Sustrato;
    private String nombreSustrato;
    private String unidad;
    private String observacion;
    private String responsable;

    public String getIdEstacion() {
        return idEstacion;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setIdEstacion(String idEstacion) {
        this.idEstacion = idEstacion;
    }

    public String getLatitud() {
        return Latitud;
    }

    public void setLatitud(String Latitud) {
        this.Latitud = Latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getNombreEstacion() {
        return nombreEstacion;
    }

    public void setNombreEstacion(String nombreEstacion) {
        this.nombreEstacion = nombreEstacion;
    }

    public String getMetodoMuestreo() {
        return metodoMuestreo;
    }

    public void setMetodoMuestreo(String metodoMuestreo) {
        this.metodoMuestreo = metodoMuestreo;
    }

    public String getTematica() {
        return tematica;
    }

    public void setTematica(String tematica) {
        this.tematica = tematica;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getValorDos() {
        return valorDos;
    }

    public void setValorDos(String valorDos) {
        this.valorDos = valorDos;
    }

    public String getMetodoAnalitico() {
        return metodoAnalitico;
    }

    public void setMetodoAnalitico(String metodoAnalitico) {
        this.metodoAnalitico = metodoAnalitico;
    }

    public String getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(String idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public String getClaseSustrato() {
        return claseSustrato;
    }

    public void setClaseSustrato(String claseSustrato) {
        this.claseSustrato = claseSustrato;
    }

    public String getNombreClaseSustrato() {
        return nombreClaseSustrato;
    }

    public void setNombreClaseSustrato(String nombreClaseSustrato) {
        this.nombreClaseSustrato = nombreClaseSustrato;
    }

    public String getSustrato() {
        return Sustrato;
    }

    public void setSustrato(String Sustrato) {
        this.Sustrato = Sustrato;
    }

    public String getNombreSustrato() {
        return nombreSustrato;
    }

    public void setNombreSustrato(String nombreSustrato) {
        this.nombreSustrato = nombreSustrato;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
}
