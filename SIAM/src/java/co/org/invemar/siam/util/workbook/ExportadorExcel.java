/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.util.workbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.ArrayList;
import org.apache.hadoop.fs.Path;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author usrsig15
 */
public class ExportadorExcel {

    private String nombreArhivo;
    private String pathArchivo;
    private ArrayList arrayDatos = null;
    private ArrayList columnaDatos = null;
    private HSSFWorkbook workbook = null;
    private int cantidadColumnas = 0;
    HSSFSheet firstSheet = null;

    
    public ExportadorExcel(String sheetName, ArrayList arrayDatos, ArrayList columnaDatos,String pathArchivo) {
        this.columnaDatos = columnaDatos;
        this.arrayDatos = arrayDatos;
        this.pathArchivo = pathArchivo;


        workbook = new HSSFWorkbook();
        firstSheet = workbook.createSheet(sheetName);


    }

    public  HSSFWorkbook generaArchivo() {

        String path = null;
        HSSFRow row = null;
        byte[] archivo=null;
        FileOutputStream fos = null;
        try {

           
            for (int i = 0; i < columnaDatos.size(); i++) {
                String nombreColumna = (String) columnaDatos.get(i);
                row = firstSheet.createRow(0);
                HSSFCell cell = row.createCell((short) i);
                cell.setCellValue(new HSSFRichTextString(nombreColumna));
            }
             
            for (int i = 0; i < arrayDatos.size(); i++) {
                row = firstSheet.createRow(i + 1);
                ArrayList columnaDatos = (ArrayList) arrayDatos.get(i);

                for (int j = 0; j < columnaDatos.size(); j++) {
                    String datoColumna = (String) columnaDatos.get(j);
                    HSSFCell cell = row.createCell((short) j);                   
                    cell.setCellValue(new HSSFRichTextString(datoColumna));
                }

            }                 
            
            Date date = new Date();
            
            path = this.pathArchivo + Path.SEPARATOR+"ArchivoDatos_"+date.getTime()+".xls";
            File file = new File(path);


            fos = new FileOutputStream(file);
            workbook.write(fos);
            //archivo = workbook.getBytes();
            file.delete();
            
        } catch (IOException e) {
            System.out.println("Error exportando archivos:" + e.toString());
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        return workbook;
    }
}
