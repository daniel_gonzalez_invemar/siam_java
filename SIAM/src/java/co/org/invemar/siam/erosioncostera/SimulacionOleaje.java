/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.erosioncostera;

import co.org.invemar.siam.ConnectionFactory;
import co.org.invemar.siam.util.Monitoreo;
import co.org.invemar.siam.util.workbook.ExportadorExcel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author usrsig15
 */
public class SimulacionOleaje {

    private int cantidadDatos=0;

    public int getCantidadDatos() {
        return cantidadDatos;
    }
    

    public HSSFWorkbook getByteArchivoDatosSimulacionEstacion(String path,String codigoEstacion) {
        byte[] archivo = null;
        
        ArrayList columnas = new ArrayList();
        
        columnas.add("FECHA");
        columnas.add("ID_ESTACION");
        columnas.add("ESTACION");
        columnas.add("LAT");
        columnas.add("LON");
        columnas.add("METODO_MUESTREO");
        columnas.add("METODO_ANALITICO");
        columnas.add("OBSERVACIONES");
        columnas.add("VARIABLE");
        columnas.add("VALOR");
        columnas.add("VALOR_NUM");
        columnas.add("RESPONSABLE");       
        
        
        ArrayList datosoleaje = getDatosSimulacionEstacion(codigoEstacion);
        cantidadDatos= datosoleaje.size();
        
        ExportadorExcel exportador = new ExportadorExcel("DatosOleaje", datosoleaje, columnas, path);
        HSSFWorkbook wb = exportador.generaArchivo();      
        
        return wb;
    }

    public ArrayList getDatosSimulacionEstacion(String codigoEstacion) {
        ArrayList datos = new ArrayList();
        ArrayList rowexcel = new ArrayList();
        ConnectionFactory cf;
        Connection con = null;

        try {
            cf = new ConnectionFactory();
            con = cf.createConnection("argos");

            PreparedStatement pstmt;
            ResultSet rs;

            String sql = "SELECT * FROM VM_DATOS_MONITOREO where id_estacion="+codigoEstacion;
           // System.out.println("SQL ejecutado:" + sql);
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            rowexcel = new ArrayList();
            while (rs.next()) {
                 rowexcel = new ArrayList();
                //Monitoreo oleaje = new Monitoreo();
                //oleaje.setFecha(rs.getString("FECHA"));
                rowexcel.add(rs.getString("FECHA"));
                
               // oleaje.setIdEstacion(rs.getString("ID_ESTACION"));
                rowexcel.add(rs.getString("ID_ESTACION"));
                
               // oleaje.setNombreEstacion(rs.getString("ESTACION"));
                rowexcel.add(rs.getString("ESTACION"));
                
               // oleaje.setLatitud(rs.getString("LAT"));
                rowexcel.add(rs.getString("LAT"));
                
                
                //oleaje.setLongitud(rs.getString("LON"));
                rowexcel.add(rs.getString("LON"));
                
               // oleaje.setMetodoMuestreo(rs.getString("METODO_MUESTREO"));
                rowexcel.add(rs.getString("METODO_MUESTREO"));
                
                //oleaje.setMetodoAnalitico(rs.getString("METODO_ANALITICO"));
                rowexcel.add(rs.getString("METODO_ANALITICO"));
                
               // oleaje.setObservacion(rs.getString("OBSERVACIONES"));
                rowexcel.add(rs.getString("OBSERVACIONES"));
                
               // oleaje.setVariable(rs.getString("VARIABLE"));
                rowexcel.add(rs.getString("VARIABLE"));
                
               // oleaje.setValor(rs.getString("VALOR"));
                rowexcel.add(rs.getString("VALOR"));
                
               // oleaje.setValorDos(rs.getString("VALOR_NUM"));
                rowexcel.add(rs.getString("VALOR_NUM"));
                
               // oleaje.setResponsable(rs.getString("RESPONSABLE"));
                rowexcel.add(rs.getString("RESPONSABLE"));
                
               
                //rowexcel.add(datos);
                datos.add(rowexcel);

            }
            


        } catch (SQLException ex) {
            System.out.println("Error obteniendo datos simulación de la estacion:Sistema:SIAM.ErosionCostera:" + ex.toString());
        } catch (Exception ex) {
        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(SimulacionOleaje.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        return datos;

    }
}
