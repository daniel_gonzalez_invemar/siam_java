package co.org.invemar.siam.redcam.coneccam;

import co.org.invemar.siam.ConnectionFactory;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase de conexion a la base dedatos autor: Rafael E. Lastra C.<br> Version:
 * 1.0 <br> Fecha: 21-03-2001.
 */
public class ConexionCam {

    private Connection aCon;
    public String Error;

    public ConexionCam() {
        try {
//            Class.forName("oracle.jdbc.driver.OracleDriver");
//            String url = "jdbc:oracle:thin:@192.168.3.203:1521:sci";
//            aCon = DriverManager.getConnection(url, "cam_admon", "mugre*");
            ConnectionFactory cf = new ConnectionFactory();
            aCon = cf.createConnection("redcam");

        } catch (Exception e) {
            Error = e.getMessage();
            System.out.println("Error conectandose a redcam:"+Error);
        }
    }

    public Connection getConn() {
        return aCon;
    }

    public void close() {
        try {
            aCon.close();
        } catch (Exception e) {
            Error = e.getMessage();
        }
    }
}
