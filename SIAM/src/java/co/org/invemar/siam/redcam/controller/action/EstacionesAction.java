/**
 *
 */
package co.org.invemar.siam.redcam.controller.action;

import co.org.invemar.siam.redcam.coneccam.ConexionCam;
import co.org.invemar.siam.redcam.estadisticas.cvariablegeog.ContVariableGeog;
import co.org.invemar.siam.redcam.estadisticas.cvariablegeog.VariableGeog;
import co.org.invemar.util.actions.Action;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import co.org.invemar.util.actions.ActionRouter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Administrador
 *
 */
public class EstacionesAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        response.setContentType("text/test charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();

        VariableGeog est = new VariableGeog();
        ContVariableGeog cest = new ContVariableGeog();
        ConexionCam con2 = new ConexionCam();
        Connection conn2 = null;
        conn2 = con2.getConn();
        String codigosector = request.getParameter("sect");

        //cest.contenedorestac(conn2, URLDecoder.decode(new String(request.getParameter("sect").getBytes("ISO-8859-1"), "UTF-8")), request.getParameter("nproy"), request.getParameter("nuser"), request.getParameter("opcion"));
        cest.contenedorestac(conn2, codigosector,"REDCAM","ljarias", "2");
        int i = 0;
        if (con2 != null) {
            con2.close();
        }
         JSONArray jsonArray = new JSONArray();
        
         for (i = 0; i < cest.gettamano(); i++) {
            try {
                JSONObject jsonobject = new JSONObject();
                est = cest.getvariablegeog(i);
                jsonobject.put("codestaciones",est.get_codest());
                jsonobject.put("nomestaciones",est.get_nomest());
                jsonArray.put(jsonobject);
            }
           
            catch (JSONException ex) {
                Logger.getLogger(EstacionesAction.class.getName()).log(Level.SEVERE, "Error generating json array of stations", ex.getMessage());
            }     
                            
         }
          pw.write(jsonArray.toString());

        return null;
    }
}
