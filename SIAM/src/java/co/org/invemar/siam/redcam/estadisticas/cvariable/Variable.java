package co.org.invemar.siam.redcam.estadisticas.cvariable;

import java.sql.*;
import java.lang.*;

public class Variable{
   private String tipo;
   private String cod;
   private String nombre;
   private String unidad;
   private String Error;
   private String sustrato;
   

   /**
  Metodo constructor de la clase
  */
  public void Cvariable(){
  }
 


/** Carga el atributo ano al objeto */
  public void settipo(String atipo)
  {
    tipo = atipo;
  }
  public void setcod(String acod)
  {
    cod = acod;
  }
  public void setnombre(String anombre)
  {
    nombre = anombre;
  }
  public void setunidad(String aunidad)
  {
    unidad = aunidad;
  }


  /**  Retorna el atributo a�o del objeto  */

  public String get_tipo()
  {
    return tipo;
  }
  public String get_cod()
  {
    return cod;
  }
  public String get_nombre()
  {
    return nombre;
  }
  public String get_unidad()
  {
    return unidad;
  }


 /** Carga una instancia del objeto de la base de datos dependiendo del
 atributo a�o que este cargado en el objeto  */

  public boolean variable(Connection conn2)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        stmt = conn2.prepareStatement("SELECT * FROM VVARIABLE" );

        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
        tipo    = rs.getString(1);
        cod     = rs.getString(2);
        nombre  = rs.getString(3);
        unidad  = rs.getString(4);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }

    public String getSustrato() {
        return sustrato;
    }

    public void setSustrato(String sustrato) {
        this.sustrato = sustrato;
    }
}
