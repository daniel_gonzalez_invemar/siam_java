/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam;

import co.org.invemar.siam.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class ServicioNivelesEstadisticos {

    public ArrayList getTododNiveles(String proyecto) {
        ArrayList list = new ArrayList();
        Connection con = null;
        try {

            String sql = "select * from aniveles_estadisticas where proyecto='" + proyecto + "' AND ACTIVO='S' order by codnivel";
         //  System.out.println("sql niveles estadisticos filtradas:" + sql);
            PreparedStatement pstmt = null;
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                list.add(poblarNivelEstadistico(rs));
            }


        } catch (SQLException ex) {
            System.out.println("Modulo estadística REDCAM:Error obteniendo los niveles estadísticos:" + ex.getMessage());
        } finally {
             try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceProfundidad.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return list;
    }

    private NivelesEstadisticos poblarNivelEstadistico(ResultSet rs) {
        NivelesEstadisticos estadistico = new NivelesEstadisticos();
        try {
            estadistico.setCod(rs.getString("codnivel"));
            estadistico.setDescripcion(rs.getString("descripcion"));
            estadistico.setAtributos_minimos(rs.getString("ATRIBUTOS_MINIMOS"));

        } catch (SQLException ex) {
            Logger.getLogger(ServicioNivelesEstadisticos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return estadistico;
    }

    public NivelesEstadisticos getNivelPorCodNivelAndProyecto(String proyecto, int codNivel) {
        NivelesEstadisticos estadistico = null;
        Connection con = null;
        ConnectionFactory cf=null;
        try {

            String sql = "select * from aniveles_estadisticas where codnivel=" + codNivel + " and proyecto='" + proyecto + "'";
          //  System.out.println("sql niveles estadisticos por proyecto y nivel:" + sql);
            PreparedStatement pstmt = null;
            cf = new ConnectionFactory();
            con = cf.createConnection("redcam");
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                estadistico = poblarNivelEstadistico(rs);
            }



        } catch (SQLException ex) {
            System.out.println("Modulo estadística REDCAM:Error obteniendo el nivel por nivel y proyecto:" + ex.getMessage());
        } finally {
           
             try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceProfundidad.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return estadistico;
    }

    public NivelesEstadisticos getNivelPorCodNivel(int codNivel) {
        NivelesEstadisticos estadistico = null;
        Connection con = null;

        try {

            String sql = "select * from aniveles_estadisticas where codnivel=" + codNivel;
            //System.out.println("sql niveles estadisticos:" + sql);
            PreparedStatement pstmt = null;
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                estadistico = poblarNivelEstadistico(rs);
            }



        } catch (SQLException ex) {
            System.out.println("Modulo estadística REDCAM:Error obteniendo el nivel estadísticos:" + ex.getMessage());
        } finally {
              try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceProfundidad.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return estadistico;
    }

    private void CerrarConexion(Connection con) {
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
