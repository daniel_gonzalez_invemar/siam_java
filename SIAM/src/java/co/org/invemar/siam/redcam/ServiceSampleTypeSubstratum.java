/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam;

import co.org.invemar.siam.ConnectionFactory;
import co.org.invemar.siam.redcam.coneccam.ConexionCam;
import co.org.invemar.util.actions.Action;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class ServiceSampleTypeSubstratum {

    private String splitVariables(String stringVariables) {
        StringBuffer newStringVariables = new StringBuffer();
        String newString;
        String[] variablesvector = stringVariables.split(",");
        for (int i = 0; i < variablesvector.length; i++) {
            String string = variablesvector[i];
            newStringVariables.append("'" + string + "',");

        }
//        System.out.println("New String variable"+newStringVariables.toString());
        newString = newStringVariables.substring(0, newStringVariables.length() - 1);
        return newString;
    }

    public String getSampleSubtratum(String year1, String year2, String codStation, String codVar) {
        Connection con = null;
        ConexionCam conexioncam = null;
        JSONArray jsonarray = new JSONArray();
        String sql = "SELECT DISTINCT tipo_muestreo as  tipo_muestreo,DECODE(tipo_muestreo,'SS','Sustrato sedimento','SA','Sustrato agua') AS label_tipo_muestreo "
                + "FROM DATOS_RDCAM_ESTACION "
                + "WHERE VARIABLE='"+codVar+"' "
                + "AND codest    ='"+codStation+"' "
                + "AND ano BETWEEN "+year1 +" AND "+year2+" and tipo_muestreo is not null ";
        try {

            /*CconexionPrueba conexionprueba = new CconexionPrueba();
             con=conexionprueba.getConn();*/
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");
            PreparedStatement pstmt = null;
            pstmt = con.prepareStatement(sql);
            /*pstmt.setString(1, codVar);
             pstmt.setString(2,codStation);
             pstmt.setString(3,year1);
             pstmt.setString(4,year2);*/
            System.out.println("sql:" + sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                JSONObject jsonobject = new JSONObject();
                jsonobject.put("id", rs.getString("tipo_muestreo"));
                jsonobject.put("name", rs.getString("label_tipo_muestreo"));
                jsonarray.put(jsonobject);
            }

        } catch (Exception ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, "Error getting the SampleSubtratum:", ex.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ServiceSampleTypeSubstratum.class.getName()).log(Level.SEVERE, null, ex);
            }
            con = null;

        }

        return jsonarray.toString();
    }

    public String getTypeSubtratumByYear(String variables) {
        Connection con = null;
        ConexionCam conexioncam = null;
        JSONArray jsonarray = new JSONArray();
        String sql = "select distinct sustrato_muestra,  decode(sustrato_muestra,'SS','Sustrato sedimento','SA','Sustrato agua') as tipoSustratoMuestra \n"
                + "from  VM_VARIABLEGEOG where  nomvar in (" + splitVariables(variables) + ")";
        try {

//            CconexionPrueba conexionprueba = new CconexionPrueba();
//            con=conexionprueba.getConn();
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");
            PreparedStatement pstmt = null;
            pstmt = con.prepareStatement(sql);
            // pstmt.setString(1, sector);
            // System.out.println("sql:" + sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                JSONObject jsonobject = new JSONObject();
                jsonobject.put("id", rs.getString("sustrato_muestra"));
                jsonobject.put("name", rs.getString("tipoSustratoMuestra"));
                jsonarray.put(jsonobject);
            }

        } catch (Exception ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, "Error getting the TypeSubtratumByYear:", ex.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ServiceSampleTypeSubstratum.class.getName()).log(Level.SEVERE, null, ex);
            }
            con = null;

        }
        return jsonarray.toString();
    }

    private void CerrarConexion(Connection con) {
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
