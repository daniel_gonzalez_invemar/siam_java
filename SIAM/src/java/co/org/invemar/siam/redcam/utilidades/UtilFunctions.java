package co.org.invemar.siam.redcam.utilidades;

import java.io.*;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 *
 * @author Victor Adri�n Santaf� Torres.
 *
 *
 */
public class UtilFunctions {

    public Document readXML(String file) {

        SAXBuilder builder = new SAXBuilder();
        Document doc = null;
        try {
            doc = builder.build(file);

        } catch (JDOMException e) {
            System.out.println("Error en readxml:" + e.toString());
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("Error en readxml:" + e.toString());
        }

        return doc;
    }

    
}
