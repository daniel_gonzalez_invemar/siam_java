/**
 *
 */
package co.org.invemar.siam.redcam.controller.action;

import co.org.invemar.siam.redcam.ServiceZones;
import co.org.invemar.util.actions.Action;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import co.org.invemar.util.actions.ActionRouter;
import java.io.PrintWriter;

/**
 * @author Administrador
 *
 */
public class ZonesAction implements Action{

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        response.setContentType("text/text; charset=ISO-8859-1");
	response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();     
        
        ServiceZones servicezones = new ServiceZones();
        String sector = request.getParameter("sector");        
        pw.write( servicezones.getZonesOfSector(sector));
        
        return null;
    }
}
