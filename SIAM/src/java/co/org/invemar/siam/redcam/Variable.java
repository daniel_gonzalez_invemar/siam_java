/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam;

/**
 *
 * @author usrsig15
 */
public class Variable {
   private String tipo;
   private String cod;
   private String nombre;
   private String unidad;
   private String Error;
   private boolean isvigente;

    public boolean isIsvigente() {
        return isvigente;
    }

    public void setIsvigente(boolean isvigente) {
        this.isvigente = isvigente;
    }

    public String getError() {
        return Error;
    }

    public void setError(String Error) {
        this.Error = Error;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

}
