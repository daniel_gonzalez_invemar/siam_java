package co.org.invemar.siam.redcam.cargadatos;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
import co.org.invemar.siam.ConnectionFactory;
import co.org.invemar.siam.redcam.utilidades.UtilFunctions;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.jdom.Document;
import org.jdom.Element;
import org.json.JSONException;
import org.json.JSONObject;

import org.primefaces.event.FileUploadEvent;

@ManagedBean(name = "controladorarchivoscargados")
public class ControladorArchivoCargados {

    private String sqlLoaderPath;
    private String DBconnectionString;
    private String userId = null;
    private String schema = null;
    private String fileName = null;
    private String tableName = null;
    private String controlFileName = null;
    private String userUploadPath = null;
    private String controlFilePath = null;
    private String userLogFilePath = null;
    private String userDiscardFilePath = null;
    private String userBadFilePath = null;
    private String userRelativePath = null;
    private ServletContext servletContext;
    private UtilFunctions util = new UtilFunctions();
    private File archivocargado = null;
    private File archivomuestras = null;
    private File archivodetallesmuestras = null;
    private String nombreTablaMuestras = null;
    private String nombreTablaDetalleMuestra = null;
    private String badfieldFile;
    private String mistyleErrores = "display:none";
    private String mystyleExito = "display:none";
    private String myStyleLinkDescarga = "display:none";
    private String myStyleMensajeDescarga = "display:none";
    private String nombrearchivo;
    private JSONObject jsonobject;
    private InputStream file;
    private String rutaPlantillaDatos;
    private String rutaDatosReferencia;
    private String mensajeError;

    public String getMensajeError() {
        return mensajeError;
    }

    public String getMyStyleMensajeDescarga() {
        return myStyleMensajeDescarga;
    }

    public String getRutaDatosReferencia() {
        return rutaDatosReferencia;
    }

    public String getRutaPlantillaDatos() {
        return rutaPlantillaDatos;
    }

    public String getMyStyleLinkDescarga() {
        return myStyleLinkDescarga;
    }

    public String getMystyleExito() {
        return mystyleExito;
    }

    public String getMistyleErrores() {
        return mistyleErrores;
    }

    public String getBadfieldFile() {
        return badfieldFile;
    }
    private String logFile;
    private boolean esmensajeerrorrenderizado = false;

    public boolean isEsmensajeerrorrenderizado() {
        return esmensajeerrorrenderizado;
    }

    public String getLogFile() {
        return logFile;
    }

    public ControladorArchivoCargados() {
        controlSession();
        initializeConfiguration();

    }

    private void initializeConfiguration() {

        // this.userId = "dgonzalez";





        System.out.println("user id:" + this.userId);
        nombreTablaMuestras = "AMUESTRA";
        nombreTablaDetalleMuestra = "AEVALUACION_VARIABLE";
//        System.out.println("/////////////// Initialization....//////////////////////////////////////");
        try {

            this.servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();

            this.rutaDatosReferencia = servletContext.getRealPath("/redcam/cargadordatos/plantilladatos/") + File.separator + "Referentes.xlsm";
            System.out.println("Ruta de plantilla de datos de referencia:" + this.rutaDatosReferencia);
            this.rutaPlantillaDatos = servletContext.getContextPath() + "/redcam/cargadordatos/plantilladatos/PlantillaRedCam.xlsm";
            System.out.println("Ruta de plantilla de datos:" + this.rutaPlantillaDatos);



            System.out.println("ruta datos de referencia" + this.rutaDatosReferencia);

            String dbconfigxml = this.servletContext.getRealPath("WEB-INF") + File.separator + "DBREDCAMConfig.xml";

            Document dbConfigDoc = util.readXML(dbconfigxml);

            Element root = dbConfigDoc.getRootElement();


            this.sqlLoaderPath = root.getAttributeValue("sqlloaderpath");
            this.DBconnectionString = root.getAttributeValue("schema");
            this.schema = root.getAttributeValue("nameschema");

            System.out.println("Schema " + this.schema);

            //CONFIGURE ALL PATHES

            //user upload path

            userUploadPath = servletContext.getRealPath("/redcam/cargadordatos/useruploads") + File.separator + this.userId;
            System.out.println("user upload path" + userUploadPath);


            if (!(new File(userUploadPath)).exists()) {
                new File(userUploadPath).mkdir();
            }


            //control file path
            controlFilePath = this.servletContext.getRealPath("WEB-INF") + File.separator + "xmlconfig" + File.separator + "cam" + File.separator + "controlfiles";


            //user log file path
            this.userLogFilePath = userUploadPath + File.separator + "logfile";
            //user discard file path
            this.userDiscardFilePath = userUploadPath + File.separator + "discardfile";
            //user bad file path
            this.userBadFilePath = userUploadPath + File.separator + "badfile";

            //check directory existence for each path
            if (!(new File(userLogFilePath)).exists()) {
                new File(userLogFilePath).mkdir();
                System.out.println("llegue ...1");
            }
            // System.out.println(userLogFilePath);
            if (!(new File(userDiscardFilePath)).exists()) {
                new File(userDiscardFilePath).mkdir();
                System.out.println("llegue ...2");
            }
            // System.out.println(userDiscardFilePath);
            if (!(new File(userBadFilePath)).exists()) {
                new File(userBadFilePath).mkdir();
                System.out.println("llegue ...3");
            }
            // System.out.println(userBadFilePath);

            //user relative path
            userRelativePath = "useruploads/" + userId;
            System.out.println("Relative path:" + userRelativePath);
//            System.out.println("///////////////Fin inicializacion////////////////////////////////");
        } catch (Exception ex) {
            System.out.println("error:" + ex.toString());
        }
    }

    public void actualizarPlantillaDatos() {
        try {



            System.out.println("xxxxxxxxxxxx:" + this.rutaDatosReferencia);
            // this.rutaDatosReferencia=null;
            GeneradorReferentes gf = new GeneradorReferentes(this.rutaDatosReferencia);
            gf.actualizarEstaciones();
            gf.actualizarInvestigadores();
            gf.actualizarSustratos();
            gf.actualizarUnidades();
            gf.actualizarMetodosAnaliticos();
            gf.actualizarProyectos();
            gf.actualizarProfundidad();
            gf.actualizarEntidades();
            gf.guardarCambios();
            myStyleLinkDescarga = "display:inline;";
            this.rutaDatosReferencia = this.servletContext.getContextPath() + "/redcam/cargadordatos/plantilladatos/" + "Referentes.xlsm";
            System.out.println("ruta archivo de referentes:"+this.rutaDatosReferencia);
        } catch (Exception ex) {
            System.out.println("Error generando el archivo de referentes:" + ex.toString());
            this.myStyleMensajeDescarga = "display:inline";
            this.mensajeError = "Un error ha ocurrido consulte con el administrador";

        }
    }

    public void controlSession() {
        System.out.println("control session ejecutado...");
        try {

            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            HttpSession session = (HttpSession) ec.getSession(true);
            ServletContext servletContext = (ServletContext) ec.getContext();

            if (session == null) {
                System.out.println("session value es null");
                ec.redirect(servletContext.getContextPath() + "/redcam/siam_cam.jsp?salir=ok");
            } else {
                this.userId = (String) session.getAttribute("usu_id1");
                System.out.println(this.userId);
                if (this.userId == null) {
                    ec.redirect(servletContext.getContextPath() + "/redcam/siam_cam.jsp?salir=ok");

                }
            }

        } catch (IllegalStateException e) {
            System.out.println("Error en control de login:" + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error en control de login:" + e.getMessage());
        }
    }

    private void ErrorCargandoDatos() {
        try {

            System.out.println("no se ha podido cargar los datos...");
            System.out.println("llegue aca 3");
            esmensajeerrorrenderizado = true;
            mistyleErrores = "display:inline";



            this.logFile = servletContext.getContextPath() + "/redcam/cargadordatos/" + jsonobject.getString("logfile");
            this.badfieldFile = servletContext.getContextPath() + "/redcam/cargadordatos/" + jsonobject.getString("badfile");


            nombreTablaDetalleMuestra = "AEVALUACION_VARIABLE";
            this.tableName = this.nombreTablaDetalleMuestra;
            this.rollBack(this.tableName);

            nombreTablaMuestras = "AMUESTRA";
            this.tableName = this.nombreTablaMuestras;
            this.rollBack(this.tableName);


            BorrarArchivosDeMuestras();
            System.out.println("Archivo no cargados con exito");


        } catch (JSONException ex) {
            System.out.println("Error JSON procesando generando archivo de error:" + ex.toString());
            System.out.println("Archivo no cargados con exito");
        } catch (SQLException ex) {
            System.out.println("Error procesando generando archivo de error:" + ex.toString());
            System.out.println("Archivo no cargados con exito");
        }
    }

    public void precesarArchivo() {
        //try {       
            /*FacesMessage msg = new FacesMessage("El archivo ", event.getFile().getFileName() + " ha sido cargado con exito.");
         FacesContext.getCurrentInstance().addMessage(null, msg);*/
        System.out.println("ejecutando upload....");

        try {


            this.controlFileName = controlFilePath + File.separator + this.fileName + ".ctl";

            System.out.println("control file name:" + this.controlFileName);

            jsonobject = processFile();
            if (jsonobject != null) {

                System.out.println("json object no es null");

                int statusmuestra = Integer.parseInt(jsonobject.getString("filestatus"));
                int statusmuestreo = 0;

                nombreTablaMuestras = "AMUESTRA";
                this.tableName = this.nombreTablaMuestras;
                boolean actualizacionUploadFlag = updateUploadFile(this.tableName, this.archivomuestras, "codigo_muestra_ms");
                System.out.println("ActualizacionUploadFlag:" + actualizacionUploadFlag);

                if (statusmuestra == 1) {
                    System.out.println("llegue aca 1");



                    this.tableName = "aevaluacion_variable";
                    this.fileName = "aevaluacion_variable";
                    this.controlFileName = controlFilePath + File.separator + this.fileName + ".ctl";

                    jsonobject = processFile();

                    if (jsonobject != null && actualizacionUploadFlag == true) {

                        statusmuestreo = Integer.parseInt(jsonobject.getString("filestatus"));
//
//                        System.out.println("statusmuestra:" + statusmuestra);
//                        System.out.println("status muestreo:" + statusmuestreo);

                        nombreTablaDetalleMuestra = "AEVALUACION_VARIABLE";
                        this.tableName = this.nombreTablaDetalleMuestra;
                        actualizacionUploadFlag = updateUploadFile(this.tableName, this.archivomuestras, "codigo_muestra_ev");
                        System.out.println("Actualizacion uploadflag adetalles muestra:" + actualizacionUploadFlag);

                        if (statusmuestra == 1 && statusmuestreo == 1) {



//                            System.out.println("llegue aca 2");
                            mystyleExito = "display:inline";
                            nombreTablaMuestras = "AMUESTRA";
                            this.tableName = this.nombreTablaMuestras;
                            commit(this.tableName);

                            nombreTablaDetalleMuestra = "AEVALUACION_VARIABLE";
                            this.tableName = this.nombreTablaDetalleMuestra;
                            commit(this.tableName);

                            System.out.println("Archivos cargados con exito");
                            BorrarArchivosDeMuestras();

                        } else {
                            ErrorCargandoDatos();
                        }

                    } else {
                        ErrorCargandoDatos();
                    }
                } else {
                    ErrorCargandoDatos();
                }

            } else {
                ErrorCargandoDatos();
            }
            System.out.println("Fin del process file........................");
        } catch (JSONException ex) {
            System.out.println("Error generando el json object" + ex.toString());
        } catch (Exception ex) {
            System.out.println("Error generando el json object" + ex.toString());
        }

    }

    public void upload(FileUploadEvent event) {

        try {
            System.out.println("ejecutando upload....");
            this.tableName = "amuestra";
            this.fileName = "amuestra";
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
            precesarArchivo();

        } catch (IOException ex) {
            Logger.getLogger(ControladorArchivoCargados.class.getName()).log(Level.SEVERE, null, ex);
        }



    }

    private boolean updateUploadFile(String tablename, File f, String campocomparacion) {
        boolean result = false;
        BufferedReader in = null;
        String linea = null;
        System.out.println("iniciando updateupload file:" + tablename);
        System.out.println("file:" + f.getName());

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            in = new BufferedReader(new FileReader(f));
            String fields[] = null;
            int contadorlineas = 0;
            ArrayList list = new ArrayList();
            while ((linea = in.readLine()) != null) {
                System.out.println("Linea:" + linea);
                fields = linea.split(";");
                list.add(fields[0]);
                //commitUserId(tableName, fields[0], campocomparacion);
                contadorlineas++;
                System.out.println("Contador" + contadorlineas);
            }
            Iterator it = list.iterator();
            ConnectionFactory cf = new ConnectionFactory();
            conn = cf.createConnection("redcam");

            String sql = "";
            while (it.hasNext()) {
                String codigomuestra = (String) it.next();
                System.out.println("Cod muestra:" + codigomuestra);
                sql = "UPDATE " + tablename + " SET UPLOADFLAG='" + this.userId + "_0' WHERE " + campocomparacion + "=" + codigomuestra + "";
                System.out.println("sql update " + tablename + ".... :" + sql);
                pstmt = conn.prepareStatement(sql);
                pstmt.executeUpdate();
            }
            System.out.println("contador de lineas:" + contadorlineas);
            System.out.println("termine ciclo de actualizacion...");
            result = true;

            

        }/* catch (SQLException ex) {
         System.out.println("SQL excepption en updatefile:" + ex.toString());
         }*/ catch (IOException ex) {
            System.out.println("IO excepption en updatefile:" + ex.toString());
        } catch (Exception ex) {
            System.out.println("Exception:" + ex.toString());
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorArchivoCargados.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (conn != null) {
                conn = null;
            }
        }
        return result;

    }

    private void rollBack(String table) throws SQLException {

        PreparedStatement pstmt = null;

        Connection conn = null;
        try {

            ConnectionFactory cf = new ConnectionFactory();
            conn = cf.createConnection("redcam");
            String sql = "DELETE FROM " + table + " WHERE UPLOADFLAG='" + this.userId + "_0'";
            System.out.println("sql delete:" + sql);
            pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();
            conn.commit();

          BorrarArchivosDeMuestras();

        } catch (Exception e) {
            System.out.println("Error en rollback de la base de datos:" + e.toString());

        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                //concam.close();
                conn = null;
            }

        }

    }

    public void commit(String table) throws SQLException {
        PreparedStatement pstmt = null;

        Connection conn = null;
        String sql = "";
        try {

            ConnectionFactory cf = new ConnectionFactory();
            conn = cf.createConnection("redcam");
            sql = "UPDATE " + table + " SET UPLOADFLAG='" + this.userId + "' WHERE UPLOADFLAG='" + this.userId + "_0' ";
            System.out.println("sql update:" + sql);
            pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();
            conn.commit();
        } catch (Exception e) {
            System.out.println("Error en commit la base de datos" + e.toString());

        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                conn = null;
            }

        }
    }
    /*
     * update upload flag from tables when upload process was success
     */

    /*private void commitUserId(String table, String codigomuestra, String campocomparacion) throws Exception {

     PreparedStatement pstmt = null;

     Connection conn = null;
     String sql = "";

     ConnectionFactory cf = new ConnectionFactory();
     conn = cf.createConnection("redcam");
     sql = "UPDATE " + table + " SET UPLOADFLAG='" + this.userId + "_0' WHERE " + campocomparacion + "=" + codigomuestra + "";
     System.out.println("sql update " + table + ".... :" + sql);
     pstmt = conn.prepareStatement(sql);
     pstmt.executeUpdate();
     //conn.commit();

     if (pstmt != null) {
     pstmt.close();
     }
     if (conn != null) {
     conn = null;
     }


     }*/
    public JSONObject processFile() {

        JSONObject resp = new JSONObject();

        //CONFIGURE ARGS FOR SQLLOADER
        String[] args = new String[8];

        args[0] = sqlLoaderPath == null ? "" : sqlLoaderPath + "sqlldr".trim();
        //System.out.println("DBconnectionString:------------------------ " + DBconnectionString);
        args[1] = "userid=" + DBconnectionString;
        args[2] = "control=" + this.controlFileName;
        args[3] = "log=" + userLogFilePath + File.separator + fileName + ".log";
        args[4] = "bad=" + userBadFilePath + File.separator + fileName + ".bad";
        args[5] = "discard=" + userDiscardFilePath + File.separator + fileName + ".dsc";
        args[6] = "data=" + userUploadPath + File.separator + fileName + ".csv";
        //args[7]="skip=1";
        args[7] = "ERRORS=555";

        try {

            //run SQLLoader
            // System.out.println("///////////////////////");
            //System.out.println("Parametros" + args[0] + " " + args[1] + " " + args[2] + " " + args[3] + " " + args[4] + " " + args[5] + " " + args[6] + " " + args[7]);//+" "+args[8]);
            //   System.out.println("/////////////////////");
            ProcessBuilder pb = null;
            pb = new ProcessBuilder(args);

            
            // evn.put("ORACLE_HOME", "/usr/lib/oracle/10.2.0.3/client");//Version anterior de sqldr.
            // /usr/lib/oracle/11.2/client64/bin/sqlldr
            // evn.put("LD_LIBRARY_PATH",evn.get("LD_LIBRARY_PATH")+":/usr/lib/oracle/10.2.0.3/client/lib");//Version anterior de sqldr.
            
            Map<String, String> evn = pb.environment();           
            evn.put("ORACLE_HOME", "/usr/lib/oracle/12.2/client64");//Version actual para correr en cinto          
            evn.put("LD_LIBRARY_PATH", evn.get("LD_LIBRARY_PATH") + ":/usr/lib/oracle/12.2/client64/lib");//Version actual para correr en cinto
            evn.put("TNS_ADMIN", "/usr/lib/oracle/12.2/client64");//Version actual para correr en cinto
            
            
            //evn.put("ORACLE_HOME", "C:\\oraclexe\\app\\oracle\\product\\11.2.0\\server\\bin\\");//Version actual para correr en cinto          
            //evn.put("LD_LIBRARY_PATH", "C:\\oraclexe\\app\\oracle\\product\\11.2.0\\server\\lib\\");//Version actual para correr en cinto
           // evn.put("TNS_ADMIN", "C:\\oraclexe\\app\\oracle\\product\\11.2.0\\server\\network\\ADMIN");//Version actual para correr en cinto
            
            Process process = pb.start();

            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;

            System.out.printf("corriendo sqlldr %s :", Arrays.toString(args));

            while ((line = br.readLine()) != null) {
                System.out.println(line);

            }
            //take it out - it makes java to work slower

            ControladorArchivoCargados.StringLog[] logs = readValuesOfFile(userLogFilePath + File.separator + fileName + ".log", schema.toUpperCase(), tableName.toUpperCase());

            //filesName.put(fileLoaded);

            resp.put("filename", fileName);

            if (logs == null) {
                resp.put("logfile", userRelativePath + "/logfile/" + fileName + ".log");
                resp.put("filestatus", "5");
            } else {

                if (logs[0].value != 0 && logs[1].value == 0 && logs[2].value == 0 && logs[3].value == 0) {
                    resp.put("logfile", userRelativePath + "/logfile/" + fileName + ".log");
                    resp.put("filestatus", "1");
                    resp.put("logobservation", "table: " + tableName + ", nrows: " + logs[0].getValue());
                } else if (logs[1].value != 0 && logs[2].value != 0) {
                    resp.put("logfile", userRelativePath + "/logfile/" + fileName + ".log");
                    resp.put("badfile", userRelativePath + "/badfile/" + fileName + ".bad");
                    resp.put("discardfile", userRelativePath + "/discardfile/" + fileName);
                    resp.put("filestatus", "2");
                } else if (logs[1].value != 0) {
                    resp.put("logfile", userRelativePath + "/logfile/" + fileName + ".log");
                    resp.put("badfile", userRelativePath + "/badfile/" + fileName + ".bad");
                    resp.put("filestatus", "3");
                } else if (logs[2].value != 0) {
                    resp.put("logfile", userRelativePath + "/logfile/" + fileName + ".log");
                    resp.put("discardfile", userRelativePath + "/discardfile/" + fileName);
                    resp.put("filestatus", "4");
                } else if (logs[0].value == 0 && logs[1].value == 0 && logs[2].value == 0 && logs[3].value == 0) {
                    resp.put("logfile", userRelativePath + "/logfile/" + fileName + ".log");
                    resp.put("filestatus", "5");
                }
            }
            System.out.println("Fin de process file...");

        } catch (Exception e) {
            System.out.println("Error en el metodo de invocacion del sqlldr on en la lectura del log:" + e.toString());
        }

        return resp;

    }

    /*
     * process content of log file generated by SQLLoader
     */
    public ControladorArchivoCargados.StringLog[] readValuesOfFile(String fileName, String schema, String table) {
        System.out.println("ejecutando: controlador archivos cargados");
        ControladorArchivoCargados.StringLog[] values;

        try {
            //System.out.println("fileName= "+fileName);
            String[] valuesOfFile = readLogFile(fileName, schema, table);

            values = readValuesOfLog(valuesOfFile);
            System.out.println("fin tarea read values of file....");
            return values;

        } catch (IOException e) {
            System.out.println("Error leyendo valores del archivo:" + e);
            return null;
        }

    }

    /*
     * read de log file generated by SQLLoader
     */
    public String[] readLogFile(String fileName, String schema, String table) throws IOException {
        System.out.println("ejecutando: read log file");
        String[] logs = new String[0];
        String fileLine = null;
        String searchString = null;
        String optionalSearchString = null;

        try {


            BufferedReader entrada = new BufferedReader(new FileReader(fileName));
            searchString = "Tabla " + schema.toUpperCase() + "." + table.toUpperCase() + ":";
            optionalSearchString = "Table " + schema.toUpperCase() + "." + table.toUpperCase() + ":";



            while ((fileLine = entrada.readLine()) != null) {

                // System.out.println(fileLine);

                if (fileLine.equalsIgnoreCase(searchString) || fileLine.equalsIgnoreCase(optionalSearchString)) {
                    logs = new String[4];
                    for (int i = 0; i < 4; i++) {
                        logs[i] = entrada.readLine();;
                        //  System.out.println("log " + i + ":" + logs[i]);
                        //System.out.println("readLogFile log::::::::::::::::::::::::::::::::::::::::::::::::::::::: "+logs[i]);
                    }

                    break;
                }
            }
            entrada.close();
            System.out.println("Fin tarea read log file");

        } catch (java.io.FileNotFoundException fnfex) {
            System.out.println("Error leyendo el archivo log:" + fnfex.toString());
        } catch (Exception fnfex) {
            System.out.println("Error leyendo el archivo log:" + fnfex.toString());
        }
        //System.out.println("long: "+logs.length);

        if (logs.length == 4) {
            //System.out.println("return logs ...");
            return logs;
        } else {
            return null;
        }
    }


    /*
     * parses read information from log file generated by SQLLoader
     */
    public ControladorArchivoCargados.StringLog[] readValuesOfLog(String[] log) {

        if (log == null) {
            return null;
        }

        ControladorArchivoCargados.StringLog[] stringLog = new ControladorArchivoCargados.StringLog[log.length];
        String value;
        String name;

        //System.out.println("========================");
        //System.out.println("=    readValueOfLog    =");
        //System.out.println("========================");

        for (int i = 0; i < log.length; i++) {

            String text = log[i];
            text = text.substring(text.indexOf("  ") + 2);


            value = text.substring(0, text.indexOf(" "));
            name = text.substring(text.indexOf(" ") + 1, text.length());
            // System.out.println("texto=" + value + " ---" + name);
            try {
                stringLog[i] = new ControladorArchivoCargados.StringLog(Integer.parseInt(value), name);
            } catch (NumberFormatException e) {
                //System.out.println("readValuesOfLog: .. "+e);
                return null;

            }
        }
        //System.out.println("========================");
        System.out.println("Fin valores de readValuesOfLog");
        return stringLog;



    }

    private class StringLog {

        private int value;
        private String name;

        /**
         * @param value
         * @param name
         */
        public StringLog(int value, String name) {
            super();
            this.value = value;
            this.name = name;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the value
         */
        public int getValue() {
            return value;
        }

        /**
         * @param value the value to set
         */
        public void setValue(int value) {
            this.value = value;
        }
    }

    public boolean BorrarArchivosDeMuestras() {

        boolean resultado = false;
        archivocargado.delete();
        System.out.println("Componente REDCAM.Borrando archivo cargado por el usuario:" + archivodetallesmuestras.getName());
        
        archivodetallesmuestras.delete();
        System.out.println("Componente REDCAM.Borrando archivo detalle de muetras:" + archivodetallesmuestras.getName());
        archivomuestras.delete();
        System.out.println("Componente REDCAM.Borrando archivo de muestras :" + archivomuestras.getName());
        try {
            resultado = true;
        } catch (Exception e) {
            System.out.println("Componente REDCAM. Error borrando los archivos de muestra." + e.toString());

        }
        return resultado;
    }

    public void separaArchivoCSV(File f, String separadorarchivo) {
        BufferedReader in = null;
        StringBuffer data = null;
        String linea = "";
        BufferedWriter bwmuestras = null;
        BufferedWriter bwdetallemuestras = null;
        try {

            in = new BufferedReader(new FileReader(f));

            archivomuestras = new File(userUploadPath + File.separator + this.fileName + ".csv");
            archivodetallesmuestras = new File(userUploadPath + File.separator + "aevaluacion_variable.csv");

            bwmuestras = new BufferedWriter(new FileWriter(archivomuestras));
            bwdetallemuestras = new BufferedWriter(new FileWriter(archivodetallesmuestras));

            // linea = in.readLine();
            // bwmuestras.write(linea + "\n");
            int contador = 0;

            while ((linea = in.readLine()) != null) {

                //System.out.println(linea);
                if (linea.charAt(0) != '*' && contador == 0) {                 // bwmuestras.write(data.toString());
                    bwmuestras.write(linea + "\n");
                    // System.out.println("llegue aca 0");
                } else if (linea.charAt(0) == '*') {
                    contador = 1;
                    bwmuestras.close();
                    //System.out.println("llegue aca");

                } else if (linea.charAt(0) != '*' && contador > 0) {
                    bwdetallemuestras.write(linea + "\n");
                }
            }
            
           


        } catch (Exception e) {
            System.out.println("Error separando archivos:" + e.toString());

        } finally {
            try {

                bwdetallemuestras.close();
            } catch (IOException ex) {
                System.out.println("Error cerrando el archivo de detalles de variables." + ex.toString());
            }
        }




    }

    public void copyFile(String fileName, InputStream in) {
        System.out.println("Copying file in server....");
        try {


            // write the inputStream to a FileOutputStream
            archivocargado = new File(userUploadPath + File.separator + fileName);
            OutputStream out = new FileOutputStream(archivocargado);

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();
            separaArchivoCSV(archivocargado, "*");


        } catch (IOException e) {
            System.out.println("Error copiando archivo:" + e.toString());
        }

    }
}
