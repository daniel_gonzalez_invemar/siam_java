/**
 *
 */
package co.org.invemar.siam.redcam.controller.action;

import co.org.invemar.siam.redcam.Variable;
import co.org.invemar.siam.redcam.ServiceVariableRedCam;
import co.org.invemar.util.actions.Action;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import co.org.invemar.util.actions.ActionRouter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Administrador
 *
 */
public class VariablesAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        response.setContentType("text/text charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        request.setCharacterEncoding("utf-8");
        PrintWriter pw = response.getWriter();
        
        String vari = request.getParameter("vari");
        
        
        ServiceVariableRedCam serviciosvariables = new ServiceVariableRedCam();
        

        ArrayList variables = serviciosvariables.getVariablesPorTipo(vari);
       
        JSONArray jsonArray = new JSONArray();
        Iterator it = variables.iterator();
        while(it.hasNext()){
            try {
                JSONObject jsonvariable = new JSONObject();
                Variable variable = (Variable)it.next();
                jsonvariable.put("nombre", variable.getNombre());
                jsonArray.put(jsonvariable);
            } catch (JSONException ex) {
                Logger.getLogger(VariablesAction.class.getName()).log(Level.SEVERE,"Error generating the json arrray of variables", ex.getMessage());
            }
        }              
       
        pw.write(jsonArray.toString());
        return null;

    }
}
