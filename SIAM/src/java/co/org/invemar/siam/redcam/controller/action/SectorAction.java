/**
 *
 */
package co.org.invemar.siam.redcam.controller.action;

import co.org.invemar.siam.manglares.model.DatosEstructurales;
import co.org.invemar.siam.manglares.vo.Departamento;
import co.org.invemar.siam.redcam.ServiceSectoresRedCam;
import co.org.invemar.siam.redcam.ServiceZones;
import co.org.invemar.util.actions.Action;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import co.org.invemar.util.actions.ActionRouter;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Administrador
 *
 */
public class SectorAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        response.setContentType("text/text; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();

        ServiceSectoresRedCam serviceSectores = new ServiceSectoresRedCam();
        String vari = request.getParameter("depart");
        //System.out.println("vari:************:"+vari);
        
        //String depart = URLDecoder.decode(new String(vari.getBytes("iso-8859-1"), "UTF-8"));   
       // System.out.println("Depart:************:"+depart);
        pw.write( serviceSectores.getSectorOfDepartament(vari, "REDCAM"));

        return null;
    }
}
