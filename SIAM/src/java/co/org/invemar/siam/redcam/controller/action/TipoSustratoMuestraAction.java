/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.siam.redcam.controller.action;

import co.org.invemar.siam.redcam.ServiceSampleTypeSubstratum;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usrsig15
 */
public class TipoSustratoMuestraAction  implements Action{
    
    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        response.setContentType("text/html charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();    
        
        String variables = request.getParameter("v");
        
        ServiceSampleTypeSubstratum sstp = new ServiceSampleTypeSubstratum();
        String json = sstp.getTypeSubtratumByYear(variables);
     
        pw.write(json);
       
        
        
       
       
        return null;
    }
}
