package co.org.invemar.siam.redcam.estadisticas.cvariablegeog;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;


public class ContUserProj{

  private String Error;
  private int tamano;
  private Vector resultados;
  public ContUserProj(){}



  /**
  * carga todos los proyectos de un usuario
  */
  public boolean contenedornproyectos(Connection conn2, String nuser)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    UserProj prj = new UserProj();
    String select = new String();
    select = "select up.cod_proyecto,p.nombre_pr, up.cod_usuario from ausuario_x_proyecto up, pproyecto p where p.codigo_pr = up.cod_proyecto and up.cod_usuario is not null";
    if(nuser != null && !nuser.equals("")){
      select = select + " and cod_usuario = '" + nuser + "'";
    }
    Error = select;
    try
      {

        stmt = conn2.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              prj = new UserProj();
              prj.setproyecto(rs.getString(1));
              prj.setnomproyecto(rs.getString(2));
              prj.setusuario(rs.getString(3));
              resultados.addElement(prj);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }




  /** retorna una instancia del contenedor de UserProj dada su posicion en el
vector */
  public UserProj getuserproj(int i) {
    return ((UserProj)resultados.elementAt(i));
  }

  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }
}
