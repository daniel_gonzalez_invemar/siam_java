/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam;

import co.org.invemar.library.siam.Utilidades;
import co.org.invemar.library.siam.exportadores.Data;
import co.org.invemar.library.siam.exportadores.ExportXLS;
import co.org.invemar.library.siam.exportadores.ExportadorExcel;
import co.org.invemar.library.siam.exportadores.Rows;
import co.org.invemar.siam.ConnectionFactory;
import java.io.File;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class ServicioEstadistica {

    public Vector resultados = null;
    private String Error;
    private int tamano;
    private HSSFWorkbook Archivo = null;
    private byte[] filebytes;
    private String pathFile = null;
    private NivelesEstadisticos nivelestadistico = null;
    private int sumatoriaParaObtenerNivelEstadistico = 0;

    public int getSumatoriaParaObtenerNivelEstadistico() {
        return sumatoriaParaObtenerNivelEstadistico;
    }

    public void setSumatoriaParaObtenerNivelEstadistico(int sumatoriaParaObtenerNivelEstadistico) {
        this.sumatoriaParaObtenerNivelEstadistico = sumatoriaParaObtenerNivelEstadistico;
    }

    public NivelesEstadisticos getNivelestadistico() {
        return nivelestadistico;
    }

    public void setNivelestadistico(NivelesEstadisticos nivelestadistico) {
        this.nivelestadistico = nivelestadistico;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public HSSFWorkbook getArchivo() {
        return Archivo;
    }

    public int gettamano() {
        return tamano;
    }

    /**
     * retorna el error generado por la basse de datos
     */
    public String geterror() {
        return Error;
    }

    public VariableGeog getvariablegeog(int i) {
        return ((VariableGeog) resultados.elementAt(i));
    }

    public static void main(String[] args) {
        ServicioEstadistica se = new ServicioEstadistica();
        System.out.println("Resultado:" + se.actualizarVistaVariableGEO());
    }

    public boolean actualizarVistaVariableGEO() {
        ConnectionFactory cf = new ConnectionFactory();
        boolean resultadoTransaction = false;
        Connection con = null;
        try {
            CconexionPrueba conexion = new CconexionPrueba();
            con = conexion.getConn();
            //con = cf.createConnection("redcam");
            String sql = "{call ACTUALIZARVARIABLEGEO() }";
            CallableStatement st = con.prepareCall(sql);
            st.executeQuery();

            if (st != null) {
                st.close();

            }
            if (con != null) {
                con.close();
            }
            resultadoTransaction = true;
        } catch (SQLException ex) {
            System.out.println("Componente REDCAM.Error actualizando la vista VariableGEO:" + ex.getMessage());
        }
        return resultadoTransaction;

    }

    public boolean generaEstadistica(int nivel, String nuser, String nproj,
            String ntipo, String ndepar,
            String nsector, String ntemporada, String ano1,
            String ano2, Vector variables,
            String nprofundidad,
            String nsustrato,
            Vector estaciones, String nregion, String zona, String sampletypesubstratum) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        Vector cod_nivel = new Vector();
        Vector valor_nivel = new Vector();
        String texto = new String();
        int mes = 0;
        int ano, mes1, dia, valor_decimal = 0;

        select = "select * from aestadisticas ae,  V_VARIABLETIPO vt,ASECTOR s where  ae.VARIABLE = VT.COD_VAR and s.descripcion_sc = ae.sector ";

        /**
         * Llenado del vector que contiene los valores decimales de las
         * combinaciones de parametros en binario
         */
        cod_nivel.addElement("1");
        cod_nivel.addElement("1");
        cod_nivel.addElement("1");
        cod_nivel.addElement("2");
        cod_nivel.addElement("2");
        cod_nivel.addElement("2");
        cod_nivel.addElement("3");
        cod_nivel.addElement("3");
        cod_nivel.addElement("4");
        cod_nivel.addElement("5");
        cod_nivel.addElement("5");
        cod_nivel.addElement("5");
        cod_nivel.addElement("6");
        cod_nivel.addElement("6");
        cod_nivel.addElement("6");
        cod_nivel.addElement("7");
        cod_nivel.addElement("7");
        cod_nivel.addElement("8");
        cod_nivel.addElement("29");
        cod_nivel.addElement("29");
        cod_nivel.addElement("29");
        cod_nivel.addElement("30");
        cod_nivel.addElement("30");
        cod_nivel.addElement("30");
        cod_nivel.addElement("31");
        cod_nivel.addElement("31");
        cod_nivel.addElement("32");
        cod_nivel.addElement("33");

        valor_nivel.addElement("7");
        valor_nivel.addElement("31");
        valor_nivel.addElement("63");
        valor_nivel.addElement("11");
        valor_nivel.addElement("27");
        valor_nivel.addElement("59");
        valor_nivel.addElement("19");
        valor_nivel.addElement("51");
        valor_nivel.addElement("35");
        valor_nivel.addElement("6");
        valor_nivel.addElement("30");
        valor_nivel.addElement("62");
        valor_nivel.addElement("10");
        valor_nivel.addElement("26");
        valor_nivel.addElement("58");
        valor_nivel.addElement("18");
        valor_nivel.addElement("50");
        valor_nivel.addElement("34");
        valor_nivel.addElement("5");
        valor_nivel.addElement("29");
        valor_nivel.addElement("61");
        valor_nivel.addElement("9");
        valor_nivel.addElement("25");
        valor_nivel.addElement("57");
        valor_nivel.addElement("17");
        valor_nivel.addElement("49");
        valor_nivel.addElement("33");
        valor_nivel.addElement("0");

        if (nregion != null && !nregion.equals("") && !nregion.equals("-")) {
            valor_decimal = valor_decimal + 32;
            //System.out.println("llegue a region");
            select = select + " and region = '" + nregion + "'";
        }

        if (ndepar != null && !ndepar.equals("") && !ndepar.equals("null")) {
            valor_decimal = valor_decimal + 16;
            //System.out.println("llegue a departamento");
            select = select + " and depto = '" + ndepar + "'";
        }
        if (nsector != null && !nsector.equals("")) {
            valor_decimal = valor_decimal + 8;
            //System.out.println("llegue a sector");
            select = select + " and s.codigo_sc  = '" + nsector + "'";
        }
        String codigoEstaciones = "";

        if (estaciones != null && estaciones.size() > 0) {
            valor_decimal = valor_decimal + 4;
            //System.out.println("llegue a estaciones");
            select = select + " and (";
            for (int k = 0; k < estaciones.size(); k++) {
                if (k == 0) {
                    codigoEstaciones = "and ( codest = '" + (String) estaciones.elementAt(k) + "'";
                    select = select + " codest = '" + (String) estaciones.elementAt(k) + "'";

                } else {
                    codigoEstaciones = codigoEstaciones + " or codest = '" + (String) estaciones.elementAt(k) + "'";
                    select = select + "  or codest = '" + (String) estaciones.elementAt(k) + "'";

                }
            }
            select = select + ")";
            codigoEstaciones = codigoEstaciones + ")";

        }
        if (ano1 != null && ano1.equals("null") && !ano1.equals("") && ano2.equals("")) {
            valor_decimal = valor_decimal + 2;
            //System.out.println("llegue a a�o");
            select = select + " and ano = '" + ano1 + "' ";
        }

        if (ano1 != null && !ano1.equals("") && !ano1.equals("null") && ano2 != null && !ano2.equals("") && !ano2.equals("null")) {
            valor_decimal = valor_decimal + 2;
            //System.out.println("llegue a a�o...");
            select = select + " and ano >= '" + ano1 + "' and ano <= '" + ano2 + "' ";
        } else {
            select = select + " and ano >= '2001' ";
        }

        if (ntemporada != null && !ntemporada.equals("")) {
            valor_decimal = valor_decimal + 1;
            //System.out.println("llegue a temporada");
            select = select + " and temporada = '" + ntemporada + "'";
        }

        if (zona != null && !zona.equals("")) {
            valor_decimal = 38;
        }

        if (ntipo != null && !ntipo.equals("")) {
            select = select + " and VT.TIPO_VR   = '" + ntipo + "'";
        }
        if (nprofundidad != null && !nprofundidad.equals("")) {
            select = select + " and profundidad = '" + nprofundidad + "'";
        }
        if (nsustrato != null && !nsustrato.equals("")) {
            select = select + " and tipo_sustrato = '" + nsustrato + "'";
        }
        if (nproj != null && !nproj.equals("")) {
            select = select + " and prj = '" + nproj + "'";
        }

        String cadenavariables = " ";

        if (variables != null && variables.size() > 0) {
            select = select + " and (";
            cadenavariables = " and (";

            for (int k = 0; k < variables.size(); k++) {
                if (k == 0) {
                    select = select + " nom_var = '" + (String) variables.elementAt(k) + "'";
                    cadenavariables += " nom_var = '" + (String) variables.elementAt(k) + "'";
                } else {
                    select = select + " or nom_var = '" + (String) variables.elementAt(k) + "'";
                    cadenavariables += " or nom_var = '" + (String) variables.elementAt(k) + "'";
                }
            }
            select = select + ")";
            cadenavariables += ")";
        }

        int bandera = 0;
        int valorNivel = 0;
        for (int i = 0; i < valor_nivel.size(); i++) {
            texto = (String) valor_nivel.elementAt(i);
            if (valor_decimal == Integer.parseInt(texto)) {
                String v = (String) cod_nivel.elementAt(i);
                valorNivel = Integer.parseInt(v);
                if (bandera == 0) {

                    select = select + " and ( nivel = " + valorNivel + " ";
                    bandera = 1;
                } else {
                    select = select + " or nivel = " + valorNivel + " ";
                }

            }
        }
        if (bandera == 1) {
            select = select + " ) ";
            //bandera = 0;
        }

// Si no se seleccino ningun valor de las combinaciones validas
        if (valor_decimal == 0) {
            select = select + " and nivel = 33 ";
        }

// Si se seleccino una combinacion de parametros no valida
        if (valor_decimal != 0 && bandera == 0) {

            select = select + " and rownum = 0 ";

            select = select + " order by ano, asc ";

        }
        select = select + " order by ano";
        if (nivel == 33) {
            select = "SELECT * "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt "
                    + "WHERE ae.VARIABLE = VT.COD_VAR "
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + "AND profundidad   = 'S' "
                    + "AND tipo_sustrato = 'SA' "
                    //  + " and tipo_muestreo='"+sampletypesubstratum+"' "
                    + "AND prj           = 'REDCAM' "
                    + cadenavariables
                    + "AND ( nivel       = 33 ) ";

        }
        if (nivel == 2) {
            select = "select * from aestadisticas ae,  V_VARIABLETIPO vt,ASECTOR s where  ae.VARIABLE = VT.COD_VAR and s.descripcion_sc = ae.sector  "
                    + " and region = '" + nregion + "'"
                    + " and depto = '" + ndepar + "'"
                    + "and ano >= '" + ano1 + "' and ano <= '" + ano2 + "' "
                    + " and s.codigo_sc  = '" + nsector + "' "
                    + "and temporada = '" + ntemporada + "'"
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + "and profundidad = 'S' "
                    + "and tipo_sustrato = 'SA' "
                    + " and tipo_muestreo='" + sampletypesubstratum + "' "
                    + "and prj = 'REDCAM' "
                    + cadenavariables
                    + "and ( nivel = 2  ) "
                    + " order by ano";
        }
        if (nivel == 6) {
            select = "select * from aestadisticas ae,  V_VARIABLETIPO vt,ASECTOR s where  ae.VARIABLE = VT.COD_VAR and s.descripcion_sc = ae.sector "
                    + " and region = '" + nregion + "'"
                    + " and depto = '" + ndepar + "'"
                    + " and s.codigo_sc  = '" + nsector + "'"
                    + "and ano >= '" + ano1 + "' and ano <= '" + ano2 + "' "
                    + " and s.codigo_sc  = '" + nsector + "'"
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + "and profundidad = 'S' "
                    + "and tipo_sustrato = 'SA' "
                    + " and tipo_muestreo='" + sampletypesubstratum + "' "
                    + "and prj = 'REDCAM' "
                    + cadenavariables
                    + "and ( nivel = 6  ) "
                    + " order by ano";
        }
        // valor decimal 31
        if (nivel == 31) {
            select = "SELECT * "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt "
                    + "WHERE ae.VARIABLE = VT.COD_VAR "
                    + " and region = '" + nregion + "'"
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + "AND profundidad   = 'S' "
                    + "AND tipo_sustrato = 'SA' "
                    //    + " and tipo_muestreo='"+sampletypesubstratum+"' "
                    + "AND prj           = 'REDCAM' "
                    + " and depto = '" + ndepar + "'"
                    + cadenavariables
                    + "AND ( nivel       = 31 ) ";

        }

        //valor_decimal == 34
        if (nivel == 8) {
            select = "SELECT * "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt "
                    + "WHERE ae.VARIABLE = VT.COD_VAR "
                    + " and region = '" + nregion + "'"
                    + "and ano >= '" + ano1 + "' and ano <= '" + ano2 + "' "
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + "AND profundidad   = 'S' "
                    + "AND tipo_sustrato = 'SA' "
                    + " and tipo_muestreo='" + sampletypesubstratum + "' "
                    + "AND prj           = 'REDCAM' "
                    + cadenavariables
                    + "AND ( nivel       = 8 ) order by ano";

        }

        //valor_decimal == 35
        if (nivel == 4) {
            select = "SELECT * "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt "
                    + "WHERE ae.VARIABLE = VT.COD_VAR "
                    + " and region = '" + nregion + "'"
                    + "and ano >= '" + ano1 + "' and ano <= '" + ano2 + "' "
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + "AND profundidad   = 'S' "
                    + "AND tipo_sustrato = 'SA' "
                    + " and tipo_muestreo='" + sampletypesubstratum + "' "
                    + "AND prj           = 'REDCAM' "
                    + cadenavariables
                    + "AND ( nivel       = 4 ) order by ano";

        }
        //valor decimal 50 
        if (nivel == 7) {
            select = "SELECT * "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt "
                    + "WHERE ae.VARIABLE = VT.COD_VAR "
                    + " and region = '" + nregion + "'"
                    + " and depto = '" + ndepar + "'"
                    + "and ano >= '" + ano1 + "' and ano <= '" + ano2 + "' "
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + "AND profundidad   = 'S' "
                    + "AND tipo_sustrato = 'SA' "
                    + " and tipo_muestreo='" + sampletypesubstratum + "' "
                    + "AND prj           = 'REDCAM' "
                    + cadenavariables
                    + "AND ( nivel       = 7 ) order by ano";

        }
        //nivel 51
        if (nivel == 3) {
            select = "SELECT * "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt "
                    + "WHERE ae.VARIABLE = VT.COD_VAR "
                    + " and region = '" + nregion + "'"
                    + " and depto = '" + ndepar + "'"
                    + "and ano >= '" + ano1 + "' and ano <= '" + ano2 + "' "
                    + "and temporada = '" + ntemporada + "'"
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + "AND profundidad   = 'S' "
                    + " AND tipo_sustrato = 'SA' "
                    + " and tipo_muestreo='" + sampletypesubstratum + "' "
                    + "AND prj           = 'REDCAM' "
                    + cadenavariables
                    + "AND ( nivel       = 3 ) order by ano";

        }

        //valor_decimal == 49
        if (nivel == 31) {
            select = "SELECT * "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt "
                    + "WHERE ae.VARIABLE = VT.COD_VAR "
                    + " and region = '" + nregion + "'"
                    + " and depto = '" + ndepar + "'"
                    + "and temporada = '" + ntemporada + "'"
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + "AND profundidad   = 'S' "
                    + "AND tipo_sustrato = 'SA' "
                    + " and tipo_muestreo='" + sampletypesubstratum + "' "
                    + "AND prj           = 'REDCAM' "
                    + cadenavariables
                    + "AND ( nivel       = 31  ) ";
        }

        //valor_decimal == 57
        if (nivel == 30) {
            select = "SELECT * "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt "
                    + "WHERE ae.VARIABLE = VT.COD_VAR "
                    + " and region = '" + nregion + "'"
                    + " and depto = '" + ndepar + "'"
                    + "and temporada = '" + ntemporada + "'"
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + cadenavariables
                    + "AND profundidad   = 'S' "
                    + "AND tipo_sustrato = 'SA' "
                    + " and tipo_muestreo='" + sampletypesubstratum + "' "
                    + "AND prj           = 'REDCAM' "
                    + "AND ( nivel=30 ) ";
        }

        //valor_decimal == 33
        if (nivel == 32) {
            select = "SELECT * "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt "
                    + "WHERE ae.VARIABLE = VT.COD_VAR "
                    + " and region = '" + nregion + "'"
                    + "and temporada = '" + ntemporada + "'"
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + "AND profundidad   = 'S' "
                    + "AND tipo_sustrato = 'SA' "
                    + " and tipo_muestreo='" + sampletypesubstratum + "' "
                    + "AND prj           = 'REDCAM' "
                    + cadenavariables
                    + "AND ( nivel=32 ) ";
        }
        //System.out.println("valor decimal ........:"+valor_decimal);
        //valor_decimal == 32  
        //valor_decimal == 61
        if (nivel == 29) {
            select = "SELECT * "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt "
                    + "WHERE ae.VARIABLE = VT.COD_VAR "
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + " and region = '" + nregion + "'"
                    + " and depto = '" + ndepar + "'"
                    + "and temporada = '" + ntemporada + "'"
                    + cadenavariables
                    + codigoEstaciones
                    + "AND profundidad   = 'S' "
                    + "AND tipo_sustrato = 'SA' "
                    + " and tipo_muestreo='" + sampletypesubstratum + "' "
                    + "AND prj           = 'REDCAM' "
                    + "AND ( nivel=29 ) ";
        }
        //valor_decimal == 38
        if (nivel == 38) {
            select = "SELECT * "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt "
                    + "WHERE ae.VARIABLE = VT.COD_VAR "
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + " and region = '" + nregion + "'"
                    + " and depto = '" + ndepar + "'"
                    + "and ano >= '" + ano1 + "' and ano <= '" + ano2 + "' "
                    + "and temporada = '" + ntemporada + "'"
                    + cadenavariables
                    + codigoEstaciones
                    + "AND profundidad   = 'S' "
                    + "AND IDZONA='" + zona + "' "
                    + " AND tipo_sustrato = 'SA' "
                    // + " and tipo_muestreo='"+sampletypesubstratum+"' "
                    + "AND prj           = 'REDCAM' "
                    + "AND ( nivel=38 ) ";

        }

        if (nivel == 1) {
            select = "SELECT * "
                    + "FROM (aestadisticas ae inner join V_VARIABLETIPO vt on ae.VARIABLE = VT.COD_VAR ) inner join  ASECTOR s on s.descripcion_sc= ae.sector "
                    + "and VT.TIPO_VR   = '" + ntipo + "'"
                    + " and region = '" + nregion + "'"
                    + " and depto = '" + ndepar + "'"
                    + "and ano >= '" + ano1 + "' and ano <= '" + ano2 + "' "
                    + " and codigo_sc  = '" + nsector + "'"
                    + "and temporada = '" + ntemporada + "'"
                    + cadenavariables
                    + codigoEstaciones
                    + "AND profundidad   = 'S' "
                    + "AND tipo_sustrato = 'SA' "
                    + " and tipo_muestreo='" + sampletypesubstratum + "' "
                    + "AND prj           = 'REDCAM' "
                    + "AND ( nivel=1 ) ";

        }

        /**
         * ****************************
         */
        System.out.println("sql estadisticas:" + select);
        Error = select;

        ArrayList columnas = new ArrayList();

        ArrayList arrayDatoExportar = new ArrayList();
        ArrayList<Rows> arrayColumnaDatosExportar = new ArrayList<Rows>();
        Connection con = null;

        Utilidades utilidades = new Utilidades();
        File f = null;
        try {
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");
            stmt = con.createStatement();
            rs = stmt.executeQuery(select);

            while (rs.next()) {
                var = new VariableGeog();

                var.setregion(rs.getString(14));
                var.setdepto(rs.getString(4));
                var.setsector(rs.getString(5));
                var.setZona(rs.getString("NOMBREZONA"));
                var.setcodest(rs.getString(15));
                var.setnomest(rs.getString(6));
                var.setano(rs.getString(2));
                var.settemporada(rs.getString(3));
                var.setsustrato(rs.getString(24));
                var.setnombre(rs.getString(7));
                var.setprom(rs.getDouble(8));
                var.setnum(rs.getDouble(9));
                var.setmax(rs.getDouble(10));
                var.setmin(rs.getDouble(11));
                var.setstddv(rs.getDouble(12));
                var.seterror(rs.getDouble(19));
                var.setmediana(rs.getDouble(17));
                var.setmoda(rs.getDouble(16));
                var.setvarianza(rs.getDouble(18));
                var.setcuart10(rs.getDouble(25));
                var.setcuart25(rs.getDouble(20));
                var.setcuart75(rs.getDouble(21));
                var.setcuart90(rs.getDouble(26));
                var.setSampletypesubstratum(labelTypeSubstratum(rs.getString("tipo_sustrato")));

                Rows r = new Rows();
                r.addData(new Data(utilidades.valueByDefault(var.get_region())));
                r.addData(new Data(utilidades.valueByDefault(var.get_depto())));
                r.addData(new Data(utilidades.valueByDefault(var.get_sector())));
                r.addData(new Data(utilidades.valueByDefault(var.getZona())));
                r.addData(new Data(utilidades.valueByDefault(var.get_codest())));
                r.addData(new Data(utilidades.valueByDefault(var.get_nomest())));
                r.addData(new Data(utilidades.valueByDefault(var.get_ano())));
                r.addData(new Data(utilidades.valueByDefault(var.get_temporada())));
                r.addData(new Data(utilidades.valueByDefault(var.get_sustrato())));
                r.addData(new Data(utilidades.valueByDefault(var.getSampletypesubstratum())));
                r.addData(new Data(utilidades.valueByDefault(var.get_nombre())));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_prom()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_num()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_max()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_min()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_stddv()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_error()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_mediana()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_moda()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_varianza()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_cuart10()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_cuart10()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_cuart25()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_cuart75()))));
                r.addData(new Data(utilidades.valueByDefault(String.valueOf(var.get_cuart90()))));
                arrayColumnaDatosExportar.add(r);

                resultados.addElement(var);
                tamano++;
            }

            ArrayList<String> columnasExcel = new ArrayList<String>();
            columnasExcel.add("Region");
            columnasExcel.add("Departamento");
            columnasExcel.add("Sector");
            columnasExcel.add("Zona");
            columnasExcel.add("Cod Estacion");
            columnasExcel.add("Estacion");
            columnasExcel.add("Ano");
            columnasExcel.add("Temporada");
            columnasExcel.add("Sustrato");
            columnasExcel.add("Tipo Sustrato Muestra");
            columnasExcel.add("Variable");
            columnasExcel.add("Promedio");
            columnasExcel.add("N");
            columnasExcel.add("Max");
            columnasExcel.add("Min");
            columnasExcel.add("Desviacion Estandar");
            columnasExcel.add("Error");
            columnasExcel.add("Moda");
            columnasExcel.add("Varianza");
            columnasExcel.add("Cuartil_10");
            columnasExcel.add("Cuartil_25");
            columnasExcel.add("Cuartil_75");
            columnasExcel.add("Cuartil_90");

            if (this.getPathFile() != null) {
                String fileName = this.getPathFile() + File.separator + "redcamdata" + Calendar.getInstance().getTimeInMillis() + ".xls";
                ExportXLS exportxls = new ExportXLS(fileName, columnasExcel, arrayColumnaDatosExportar);
                f = new File(fileName);

                filebytes = exportxls.generateExcelFile();

            }
            ServicioNivelesEstadisticos servicionivelestadistico = new ServicioNivelesEstadisticos();
            nivelestadistico = servicionivelestadistico.getNivelPorCodNivel(nivel);
            this.sumatoriaParaObtenerNivelEstadistico = valor_decimal;

            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;
        } catch (SQLException e) {
            System.out.println("Error en la generacion de estadisticas:" + e.getMessage());
            Error = e.getMessage() + "," + select;
            resp = false;
        } finally {
            try {
                if (f != null && f.exists()) {
                    //f.delete();
                }
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ServicioEstadistica.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return resp;
    }

    public String getAllYearByDepartmentViewMicroData(String department) {
        ArrayList list = new ArrayList();
        Connection con = null;
        String data = "";
        String sql = "SELECT DISTINCT ano "
                + "FROM v_redcam, "
                + "  PTIPO_VARIABLE TV, "
                + "  ASECTOR S "
                + "WHERE codvar                                        IS NOT NULL "
                + "AND tipo                                             = TV.DESCRIPCION_TP "
                + "AND S.DESCRIPCION_SC                                 =SECTOR "
                + "AND depto                                            ='"+department+"' "
                + "AND EXTRACT(YEAR FROM TO_DATE(fecha, 'DD/MM/YYYY')) >= 2001 "
                + "ORDER BY ano";
        try {
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");
            PreparedStatement pstmt = null;
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
           // System.out.println("sql:" + sql);          
          
            JSONArray jsonarrayData = new JSONArray();
            
            while (rs.next()) {
                JSONObject jo = new JSONObject();
                jo.put("year",rs.getString("ano"));
                jsonarrayData.put(jo);
            }

           data=jsonarrayData.toString();
           

        } catch (Exception ex) {
            Logger.getLogger(ServiceDepartamentos.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceDepartamentos.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }

        }
        return data;
    }

    public String getAllDataRedCamStationView(String department, String year) {
        ArrayList list = new ArrayList();
        Connection con = null;
        String data = "";
        String sql = "SELECT depto, "
                + "  sector, "
                + "  codest, "
                + "  nomest, "
                + "  muestra, "
                + "  fecha, "
                + "  sustrato,"
                + "(select nombre_ts from atipo_sustrato where CODIGO_TIPO_SUSTRATO_TS=sustrato_muestra) as sustrato_muestra,"
                + "  DECODE(tempo,'1','Seca','Lluviosa')                     AS temporada, "
                + "  DECODE(prof,'S','Superficial','M','Intermedio','Fondo') AS profundidad, "
                + "  tipo, "
                + "  codvar, "
                + "  nomvar, "
                + "  valor, "
                + "  unidad, "
                + "  fecha_real, "
                + "  ano, "
                + "  mes, "
                + "  lat, "
                + "  lon, "
                + "  proyecto, "
                + "  codigo_tp, "
                + "  descripcion_tp, "
                + "  codigo_sc, "
                + "  descripcion_sc "
                + "FROM v_redcam, "
                + "  PTIPO_VARIABLE TV, "
                + "  ASECTOR S "
                + "WHERE codvar       IS NOT NULL "
                + "AND tipo            = TV.DESCRIPCION_TP "
                + "AND S.DESCRIPCION_SC=SECTOR  and depto='" + department + "' and ano=" + year + " and EXTRACT(YEAR FROM TO_DATE(fecha, 'DD/MM/YYYY')) >= 2001"
                + " order by fecha";
        try {
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");
            PreparedStatement pstmt = null;
            pstmt = con.prepareStatement(sql);
            //  pstmt.setString(1,department);

            ResultSet rs = pstmt.executeQuery();
           
            StringBuffer sb = new StringBuffer();
            sb.append("[");

            while (rs.next()) {

                sb.append("['" + rs.getString("depto") + "','" + rs.getString("sector") + "','" + rs.getString("codest") + " - " + rs.getString("nomest") + "','" + rs.getString("ano") + "','" + rs.getString("codvar") + " - " + rs.getString("nomvar") + "','" + rs.getString("valor") + "','" + rs.getString("unidad") + "','" + rs.getString("sustrato_muestra") + "','" + rs.getString("sustrato") + "','" + rs.getString("profundidad") + "','" + rs.getString("temporada") + "'],");

            }

            sb.append("]");
            int posicion = sb.toString().length() - 2;
            data = sb.toString().substring(0, posicion) + "]";
            // System.out.println("data:"+data);

        } catch (Exception ex) {
            Logger.getLogger(ServiceDepartamentos.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceDepartamentos.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }

        }
        return data;
    }

    private String labelTypeSubstratum(String typeSubstratum) {

        String labeltypeSubstratum;
        if (typeSubstratum.equalsIgnoreCase("SS")) {
            labeltypeSubstratum = "Sustrato sedimento";
        }
        if (typeSubstratum.equalsIgnoreCase("SA")) {
            labeltypeSubstratum = "Sustrato Agua";
        } else {
            labeltypeSubstratum = "--";
        }
        return labeltypeSubstratum;

    }

    private void CerrarConexion(Connection con) {
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public byte[] getFilebytes() {
        return filebytes;
    }

    public void setFilebytes(byte[] filebytes) {
        this.filebytes = filebytes;
    }
}
