/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam;

import co.org.invemar.siam.ConnectionFactory;
import co.org.invemar.siam.redcam.coneccam.ConexionCam;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class ServiceVariableRedCam {

    private Connection con = null;
    private ConexionCam conexioncam = null;

    public ServiceVariableRedCam() {
    }

    public Connection getCon() {
        return con;
    }

    public ArrayList getVariablesPorTipo(String vari) {
        ArrayList variables = new ArrayList();
        Statement stmt = null;
        ResultSet rs = null;
        String select = new String();
        ConnectionFactory cf = new ConnectionFactory();
        //  select = "select distinct v.nomvar from vm_variablegeog v, ausuario_x_proyecto u  where tipo = '" + vari + "' and v.PROYECTO = u.COD_PROYECTO";// previus version sql.
        select = "SELECT  distinct nomvar  FROM vm_variablegeog v, ausuario_x_proyecto u,ptipo_variable tv, pvariable pv WHERE tv.codigo_tp ='" + vari + "' and pv.codigo_vr= v.codvar  and tv.descripcion_tp=tipo AND v.PROYECTO  = u.COD_PROYECTO and proyecto = 'REDCAM' and pv.vigencia_vr='S' order by nomvar";
        //System.out.println("sql:" + select);

        try {
            con = cf.createConnection("redcam");
            stmt = con.createStatement();
            rs = stmt.executeQuery(select);
            while (rs.next()) {
                Variable v = new Variable();
                v.setNombre(rs.getString(1));
                variables.add(v);

            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }


        } catch (SQLException e) {
            System.out.println("Error vizualiando las variables:" + e.toString());
        } finally {
              try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceProfundidad.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return variables;

    }

    public ArrayList getVariablesMedidasPorEstacion(String codigoEstacion, String proyecto) {
        ArrayList variables = new ArrayList();
        Statement stmt = null;
        ResultSet rs = null;
        String select = new String();
        ConnectionFactory cf = new ConnectionFactory();
        //  select = "select distinct v.nomvar from vm_variablegeog v, ausuario_x_proyecto u  where tipo = '" + vari + "' and v.PROYECTO = u.COD_PROYECTO";// previus version sql.
        select = "SELECT DISTINCT tipo "
                + "  ||'- ' "
                + "  ||nomvar AS nombre, "
                + "  codvar "
                + "FROM vm_variablegeog v, "
                + "  pvariable pv "
                + "WHERE pv.codigo_vr =v.codvar "
                + "AND v.proyecto     = '" + proyecto + "' "
                + "AND v.CODEST       = '" + codigoEstacion + "' "
                + "AND pv.vigencia_vr ='S' "
                + "ORDER BY nombre";

        try {
            con = cf.createConnection("redcam");
            stmt = con.createStatement();
            rs = stmt.executeQuery(select);
            while (rs.next()) {
                Variable v = new Variable();
                v.setCod(rs.getString("codvar"));
                v.setNombre(rs.getString("nombre"));
                variables.add(v);

            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }


        } catch (SQLException e) {
            System.out.println("Error recuperando variables medidad por estaciones:" + e.toString());
        } finally {
             try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceProfundidad.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return variables;
    }

    public ArrayList getTiposVariablesCAM() {
        ArrayList list = new ArrayList();
        ConnectionFactory cf = new ConnectionFactory();
        try {
            String sql = "SELECT DISTINCT CODIGO_TP,"
                    + " DESCRIPCION_TP"
                    + " FROM PTIPO_VARIABLE TV, "
                    + " PVARIABLE PV WHERE  PV.TIPO_VR=TV.CODIGO_TP "
                    + " AND PV.VIGENCIA_VR       ='S' "
                    + " ORDER BY descripcion_tp ";
            PreparedStatement pstmt = null;
            con = cf.createConnection("redcam");
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                TipoVariable tipovariable = new TipoVariable();
                tipovariable.setNombre(rs.getString("DESCRIPCION_TP"));
                tipovariable.setTipo(rs.getString("CODIGO_TP"));
                list.add(tipovariable);
            }


        } catch (SQLException ex) {
            System.out.println("Error obteniendo los tipos de variables redcam:" + ex.getMessage());
        } finally {
             try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceProfundidad.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return list;
    }

    public ArrayList getVariablesRedCam(String tipoVariable, String varActivas, String varDesactivas) {
        ArrayList list = new ArrayList();
        ConnectionFactory cf = new ConnectionFactory();

        String sqlcomplement = null;

        if (varActivas != null && varDesactivas != null && (varActivas.equalsIgnoreCase("true") && varDesactivas.equalsIgnoreCase("true"))) {
            sqlcomplement = "('S','N')";
        } else if (varActivas != null && varDesactivas != null && (varActivas.equalsIgnoreCase("false") && varDesactivas.equalsIgnoreCase("true"))) {
            sqlcomplement = "('N')";
        } else if (varActivas != null && varDesactivas != null && (varActivas.equalsIgnoreCase("true") && varDesactivas.equalsIgnoreCase("false"))) {
            sqlcomplement = "('S')";
        } else if (varActivas != null && varDesactivas != null && (varActivas.equalsIgnoreCase("false") && varDesactivas.equalsIgnoreCase("false"))) {
            sqlcomplement = "('S','N')";
        }

        try {

            String sql = "SELECT DISTINCT v.nomvar  FROM vm_variablegeog v, PVARIABLE VAR WHERE var.tipo_vr   ='" + tipoVariable + "'   AND VAR.CODIGO_VR = V.CODVAR AND proyecto = 'REDCAM' AND VAR.VIGENCIA_VR in " + sqlcomplement + " ORDER BY nomvar";
            //System.out.println("sql variables filtradas:"+sql);
            PreparedStatement pstmt = null;
            con = cf.createConnection("redcam");
            pstmt = con.prepareStatement(sql);
            //pstmt.setString(1, tipoVariable);    

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Variable v = new Variable();
                v.setNombre(rs.getString("nomvar"));
                list.add(v);
            }


        } catch (SQLException ex) {
            System.out.println("Error obteniendo las variables con  tipo en proyecto redcam:" + ex.getMessage());
        } finally {
              try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceProfundidad.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return list;
    }

    private void CerrarConexion(Connection con) {
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        ServiceVariableRedCam service = new ServiceVariableRedCam();

        ArrayList list = service.getVariablesRedCam("Físico/Químicas", "false", "false");
        System.out.println("List size:" + list.size());

        Iterator it = list.iterator();
        while (it.hasNext()) {
            Variable v = (Variable) it.next();
            System.out.println("variable:" + v.getNombre() + "Vigente:" + v.isIsvigente());
        }
    }
}
