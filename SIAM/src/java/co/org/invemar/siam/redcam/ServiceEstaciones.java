/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam;

import co.org.invemar.siam.ConnectionFactory;
import co.org.invemar.siam.redcam.coneccam.ConexionCam;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class ServiceEstaciones {

    private Connection con = null;
    private ConexionCam conexioncam = null;

    public ServiceEstaciones() {
    }

    public ArrayList getEstacionesPorVigencia(String codigoSector, String proyecto, String varActivas, String varDesactivas) {
        ArrayList list = new ArrayList();

        String sqlcomplement = null;
//         System.out.println("varActivas:"+varActivas);
//         System.out.println("varDesactivas:"+varDesactivas);
//          System.out.println("codigo del sector"+codigoSector);


        if (varActivas != null && varDesactivas != null && (varActivas.equalsIgnoreCase("true") && varDesactivas.equalsIgnoreCase("true"))) {
            sqlcomplement = "('S','N')";
        } else if (varActivas != null && varDesactivas != null && (varActivas.equalsIgnoreCase("false") && varDesactivas.equalsIgnoreCase("true"))) {
            sqlcomplement = "('N')";
        } else if (varActivas != null && varDesactivas != null && (varActivas.equalsIgnoreCase("true") && varDesactivas.equalsIgnoreCase("false"))) {
            sqlcomplement = "('S')";
        } else if (varActivas != null && varDesactivas != null && (varActivas.equalsIgnoreCase("false") && varDesactivas.equalsIgnoreCase("false"))) {
            sqlcomplement = "('S','N')";
        }

        try {

            String sql = "SELECT  distinct v.codest, v.nomest FROM vm_variablegeog v,asector s,aestacion e where proyecto=" + proyecto + " and e.codigo_st=v.codest and v.sector=s.descripcion_sc and codigo_sc='" + codigoSector + "' and vigencia_st in " + sqlcomplement + " order by  v.nomest";
           // System.out.println("sql estaciones filtradas:"+sql);
            PreparedStatement pstmt = null;
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");
            pstmt = con.prepareStatement(sql);
            //pstmt.setString(1, tipoVariable);    

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                EstacionesVo estacionvo = new EstacionesVo();
                estacionvo.setCodigo(rs.getString("codest"));
                estacionvo.setNombre(rs.getString("nomest"));
                list.add(estacionvo);
            }


        } catch (SQLException ex) {
            System.out.println("Error obteniendo las estaciones por sectores:" + ex.getMessage());
        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return list;
    }

    private void CerrarConexion(Connection con) {
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
