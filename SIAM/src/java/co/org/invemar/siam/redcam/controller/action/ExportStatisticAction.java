/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam.controller.action;

import co.org.invemar.library.siam.Utilidades;
import co.org.invemar.siam.redcam.NivelesEstadisticos;
import co.org.invemar.siam.redcam.ServicioEstadistica;
import co.org.invemar.siam.redcam.VariableGeog;
import co.org.invemar.siam.redcam.coneccam.ConexionCam;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Connection;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author usrsig15
 */
public class ExportStatisticAction implements Action {

   

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        response.setContentType("application/vnd.ms-excel; charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache");

        response.setHeader("content-disposition", "attachement; filename=data.xls");
        
        ServletOutputStream out = response.getOutputStream();

        String nuser = "ljarias";

        VariableGeog var = new VariableGeog();
        ServicioEstadistica servicioEstadistica = new ServicioEstadistica();

        ConexionCam con2 = new ConexionCam();
        Connection conn2 = null;
        conn2 = con2.getConn();

        request.setCharacterEncoding("UTF-8");

        String opcion = request.getParameter("opcion");
        String nproy = request.getParameter("nproy");
        String nconsulta = request.getParameter("nconsulta");
        String ntipo = request.getParameter("ntipo");
        String ndepar = request.getParameter("ndepar");
       
       
        ndepar = new String(ndepar.toString().getBytes("ISO-8859-1"),"UTF-8");
        

      

        String nsector = request.getParameter("nsector");
        String ntemporada = request.getParameter("ntemporada");
        String nprofundidad = request.getParameter("nprofundidad");
        String nsustrato = request.getParameter("nsustrato");
        String nregion = request.getParameter("nregion");
        String zona = request.getParameter("zona");
        String tam = request.getParameter("tam");
        String nivel = request.getParameter("nivel");
        String sampletypesubstratum = request.getParameter("tsm");

        String ano1 = "", ano2 = "";
        if (request.getParameter("ano1") != null && !request.getParameter("ano1").equals("")) {
            ano1 = request.getParameter("ano1");
        }
        if (request.getParameter("ano2") != null && !request.getParameter("ano2").equals("")) {
            ano2 = request.getParameter("ano2");
        }

        /*
         * recuperacion de la cadena de variables CADE
         */
        String cadena = request.getParameter("cadena");       
        cadena = new String(cadena.toString().getBytes("ISO-8859-1"),"UTF-8");
        System.out.println("cadena:"+cadena);
        String cad = "";
        Vector cade = new Vector();
        if (cadena != null && !cadena.equals("")) {
            cad = cadena;
            String cadena1 = "";
            for (int h = 0; h < cad.length(); h++) {
                if (cad.charAt(h) == ',') {
                    cade.addElement(cadena1);
                    cadena1 = "";
                } else {
                    cadena1 = cadena1 + cad.charAt(h);
                }
            }
        }
        

        /*
         * recuperacion de la cadena de estaciones SCADE
         */
        String cadest = request.getParameter("cadest");
        String scad = "";
        Vector scade = new Vector();
        if (cadest != null && !cadest.equals("")) {
            scad = cadest;
            String cadest1 = "";
            for (int z = 0; z < scad.length(); z++) {
                if (scad.charAt(z) == ',') {
                    scade.addElement(cadest1);
                    cadest1 = "";
                } else {
                    cadest1 = cadest1 + scad.charAt(z);
                }
            }
        }

        String path= request.getServletContext().getRealPath("/downloaddata/");      
        servicioEstadistica.setPathFile(path);
        servicioEstadistica.generaEstadistica(Integer.parseInt(nivel), nuser, "REDCAM", ntipo, ndepar, nsector, ntemporada, ano1, ano2, cade, "S", "SA", scade, nregion, zona,sampletypesubstratum);
        NivelesEstadisticos nivelestadistico = servicioEstadistica.getNivelestadistico();

        out.write(servicioEstadistica.getFilebytes());
        out.flush();
        out.close();

        return null;
    }
}
