/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam;

import co.org.invemar.siam.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class ServiceProfundidad {
  

    public ArrayList getProfundidad(String tipovarible,String sector, String departamento, String variable) {
        ArrayList list = new ArrayList();
        Connection con = null;
        
       try {
            String sql = "SELECT distinct prof,decode(prof,'S','Superficial','M','Intermedio','Fondo') as profundidad FROM v_redcam, PTIPO_VARIABLE TV, ASECTOR S WHERE codvar IS NOT NULL AND tipo= TV.DESCRIPCION_TP AND S.DESCRIPCION_SC=SECTOR AND TV.CODIGO_TP= '"+tipovarible+"'";           
            
            System.out.println("tipovariable:"+tipovarible);
            System.out.println("sector:"+sector+"**********"+sector.length());
            System.out.println("departamento:"+departamento+"********"+departamento.length());
            System.out.println("variable:"+variable+"************"+variable.length());
            
            if (sector.length()!=0  )  {
               sql=sql+ " and codigo_sc='"+sector+"'";               
            }
            
            if (departamento.length()!=0  )  {
               sql=sql+ " and depto='"+departamento+"'";               
            }
            
            if (variable.length()!=0  )  {
               sql=sql+ " and "+variable+"";               
            }
            
            PreparedStatement pstmt = null;
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");
            pstmt = con.prepareStatement(sql);
            System.out.println("Sql profundidad:"+sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                ProfundidadVo profundidad = new ProfundidadVo();
                profundidad.setCodigo(rs.getString("prof"));
                profundidad.setNombre(rs.getString("profundidad"));
                list.add(profundidad);
            }


        } catch (SQLException ex) {
            System.out.println("Error obteniendo la profundidad para los parametros de la busqueda por estaciones:" + ex.getMessage());
        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceProfundidad.class.getName()).log(Level.SEVERE, null, ex);
            }
            

        }
        return list;
    }
    private void CerrarConexion(Connection con) {
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}