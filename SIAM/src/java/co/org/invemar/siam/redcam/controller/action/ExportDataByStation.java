/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam.controller.action;

import co.org.invemar.library.siam.exportadores.Data;
import co.org.invemar.library.siam.exportadores.ExportXLS;
import co.org.invemar.library.siam.exportadores.Rows;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
@WebServlet(urlPatterns = {"/exportStationToExcel"})
public class ExportDataByStation  extends HttpServlet {
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

       byte file[]=null;
        File f =null;
        try {            
            
            
             String nproy = "REDCAM";            
             String ntemporada = request.getParameter("ntemporada");            
             String ano1 = request.getParameter("ano1");
             String ano2 = request.getParameter("ano2");
             String variable = request.getParameter("variable");             
             String estacion = request.getParameter("estacion");  
             String sampleSubtratum = request.getParameter("samplesubtratum");
             
             
            coneccam.Cconexioncam con2 = new coneccam.Cconexioncam();
            Connection conn2 = null;
            conn2 = con2.getConn();
             
            cvariablegeog.Ccontvariablegeog cvar = new cvariablegeog.Ccontvariablegeog();
            cvar.estadisticas3(conn2, "ljarias", "REDCAM", null,null,ntemporada, ano1, ano2, variable, estacion, null,sampleSubtratum);
             
             
            String path= request.getSession().getServletContext().getRealPath("/downloaddata/");
            String fileName = path+File.separator+"data"+Calendar.getInstance().getTimeInMillis()+ ".xls";         
            
            ArrayList<String> list= new ArrayList<String>();
            list.add("Variable");
            list.add("Muestreo");
            list.add("Valor");
            list.add("Unidad");
          
            
           ArrayList<Rows> Arows= cvar.getArows();
            
            ExportXLS exportxls = new ExportXLS(fileName,list, Arows);
            f =new File(fileName);                    
            file=exportxls.generateExcelFile();                      

        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }finally{
            if(f.exists()){
                f.delete();
            }
        }
        response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-Disposition", "attachment; filename=\"data.xls\"");
	response.setHeader("Pragma", "no-cache");
	ServletOutputStream outs = response.getOutputStream();
	outs.write(file);
	outs.flush();
	outs.close();
        
       

    }
}
