/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam;

import co.org.invemar.siam.ConnectionFactory;
import co.org.invemar.siam.redcam.coneccam.ConexionCam;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class ServiceAno {

    private Connection con = null;
    private ConexionCam conexioncam = null;

    public ServiceAno() {
       
    }
    
    public ArrayList anoPorRegion(String region,String departamento) {
        ArrayList list = new ArrayList();

        String sql=null;
        
        if (region!=null && departamento!=null) {
           
            sql = "SELECT DISTINCT ano "
                + "FROM aestadisticas ae, "
                + "  V_VARIABLETIPO vt, "
                + "  ASECTOR s "
                + " WHERE ae.VARIABLE    = VT.COD_VAR "
                + " AND s.descripcion_sc = ae.sector "
                + " AND region          = '"+region+"'"
                + " AND depto            = '"+departamento+"'"   
                + " AND ano            >= '2001'"        
                + " order by ano ";
        }else if (region!=null && departamento==null ){
           
             sql = "SELECT DISTINCT ano "
                + "FROM aestadisticas ae, "
                + "  V_VARIABLETIPO vt, "
                + "  ASECTOR s "
                + " WHERE ae.VARIABLE    = VT.COD_VAR "
                + " AND s.descripcion_sc = ae.sector "  
                + " AND region          = '"+region+"'"    
                + " AND ano            >= '2001'"        
                + " order by ano ";
        }else if (region==null && departamento!=null){
            
            sql = "SELECT DISTINCT ano "
                + "FROM aestadisticas ae, "
                + "  V_VARIABLETIPO vt, "
                + "  ASECTOR s "
                + " WHERE ae.VARIABLE    = VT.COD_VAR "
                + " AND s.descripcion_sc = ae.sector "               
                + " AND depto            = '"+departamento+"'"   
                + " AND ano            >= '2001'"            
                + " order by ano ";
         }else if (region==null && departamento==null){
           
            sql = "SELECT DISTINCT ano "
                + "FROM aestadisticas ae, "
                + "  V_VARIABLETIPO vt, "
                + "  ASECTOR s "
                + " WHERE ae.VARIABLE    = VT.COD_VAR "
                + " AND s.descripcion_sc = ae.sector "
                + " AND ano            >= '2001'"            
                + " order by ano ";
        }
            
        try {
            //System.out.println("sql ano:"+sql);
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");    
            PreparedStatement pstmt = null;
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Ano ano = new Ano();
                
                if (rs.getString("ano")==null){
                  ano.setAno("No definido");
                 
                }else{
                  ano.setAno(rs.getString("ano"));  
                }                
                list.add(ano);
            }

        } catch (Exception ex) {
            System.out.println("Error obteniendo los a�os monitoreados por regiones:" + ex.getMessage());
        } finally {
            CerrarConexion(con);

        }
        return list;
    }
    
    public ArrayList anoPorNivel(int nivel,String project) {
        ArrayList list = new ArrayList();

        String sql=null;
        
       
            sql = "SELECT distinct ano FROM AESTADISTICAS where prj=? and nivel=? and ano>=2001 order by ano";
        
            
        try {
            //System.out.println("sql ano:"+sql);
             ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");    
            PreparedStatement pstmt = null;
            pstmt = con.prepareStatement(sql);           
            pstmt.setString(1,project);
            pstmt.setInt(2, nivel);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Ano ano = new Ano();
                
                if (rs.getString("ano")==null){
                  ano.setAno("No definido");
                 
                }else{
                  ano.setAno(rs.getString("ano"));  
                }                
                list.add(ano);
            }

        } catch (Exception ex) {
            System.out.println("Error obteniendo los a�os monitoreados por regiones:" + ex.getMessage());
        } finally {
            CerrarConexion(con);

        }
        return list;
    }
    
    private void CerrarConexion(Connection con) {
        try {
            con.close();
            con = null;
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
