package co.org.invemar.siam.redcam.estadisticas.cvariablegeog;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

public class UserProj{
   private String usuario;
   private String proyecto;
   private String nomproyecto;

   private String Error;

   /**
  Metodo constructor de la clase
  */
  public void Cuserproj(){
  }


/** Carga el atributo ano al objeto */
  public void setusuario(String ausuario)
  {
    usuario = ausuario;
  }
  public void setproyecto(String aproyecto)
  {
    proyecto = aproyecto;
  }
  public void setnomproyecto(String anomproyecto)
  {
    nomproyecto = anomproyecto;
  }


  /**  Retorna el atributo a�o del objeto  */
 public String get_usuario()
  {
    return usuario;
  }
  public String get_proyecto()
  {
    return proyecto;
  }
  public String get_nomproyecto()
  {
    return nomproyecto;
  }


 /** Carga una instancia del objeto de la base de datos dependiendo del
 atributo a�o que este cargado en el objeto  */

  public boolean userproj(Connection conn2)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        stmt = conn2.prepareStatement("select up.COD_PROYECTO,p.NOMBRE_PR, up.COD_USUARIO from ausuario_x_proyecto up, pproyecto p where p.CODIGO_PR = up.COD_PROYECTO and up.COD_USUARIO is not null" );

        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
        proyecto  = rs.getString(1);
        nomproyecto  = rs.getString(2);
        usuario   = rs.getString(3);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }
}
