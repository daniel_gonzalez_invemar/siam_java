/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam.cargadatos;

import co.org.invemar.siam.ConnectionFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorksheet;

/**
 *
 * @author usrsig15
 */
public class GeneradorReferentes {

    private Workbook workbookxslm;
    private XSSFSheet sheet;
    private CTWorksheet xml;
    private String fileName;
    private InputStream fileTemp;
    private File archivoCopia;

    /**
     * This method is used to read the data's from an excel file.
     *
     * @param fileName - Name of the excel file.
     */
    public GeneradorReferentes(String fileName) {
        try {

            copyFile(fileName);
            workbookxslm = new XSSFWorkbook(archivoCopia.getPath());
            sheet = (XSSFSheet) workbookxslm.getSheetAt(0);
            xml = sheet.getCTWorksheet();
            this.fileName = fileName;
        } catch (Exception e) {
            System.out.println("Error abriendo archivo excel:" + e.toString());
        }

    }

    private void copyFile(String fileName) {

        try {

            InputStream in = new FileInputStream(fileName);

            archivoCopia = new File("Temp.tmp");
           
            OutputStream out = new FileOutputStream(archivoCopia);

            int read = 0;
            byte[] bytes = new byte[3024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();

            // System.out.println("Archivo copia generado con exito:" + archivoCopia.getPath());


        } catch (IOException e) {
            System.out.println("Error copiando archivo:" + e.toString());
        }

    }

    public boolean guardarCambios() throws Exception, IOException {
        FileOutputStream outPutStream;
        boolean result = false;

        outPutStream = new FileOutputStream(this.fileName);
        this.workbookxslm.write(outPutStream);
        result = true;
        //} catch (IOException ex) {
        //System.out.println("Error guardand el archivo"+ex.toString());
        // }  finally {
        //try {
        outPutStream.close();
        //} catch (IOException ex) {
        //  Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
        //}
        // }
        return result;
    }

    public boolean actualizarEstaciones() {
        boolean result = false;

        ConnectionFactory cf = new ConnectionFactory();
        Connection con = null;

        PreparedStatement pstmt = null;
        ResultSet rs;

        try {
            con = cf.createConnection("redcam");
            String sql = "SELECT CODIGO_ST,NOMBRE_ST,TIPO_AGUA_ST FROM AESTACION where vigencia_st='S' order by codigo_st";
            //System.out.println("sql de consulta de estaciones:"+sql);
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();


            int contador = 3;


            while (rs.next()) {

                CellReference columna1 = new CellReference("G" + String.valueOf(contador));
                CellReference columna2 = new CellReference("H" + String.valueOf(contador));
                CellReference columna3 = new CellReference("I" + String.valueOf(contador));

                String codigo = rs.getString("CODIGO_ST");
                String nombre = rs.getString("NOMBRE_ST");
                String valor3 = rs.getString("TIPO_AGUA_ST");


                Row row = sheet.getRow(columna1.getRow());
                Cell cell = row.getCell(columna1.getCol());
                cell.setCellValue(codigo);


                row = sheet.getRow(columna2.getRow());
                cell = row.getCell(columna2.getCol());
                cell.setCellValue(nombre);

                row = sheet.getRow(columna3.getRow());
                cell = row.getCell(columna3.getCol());
                cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                cell.setCellValue(valor3);

                contador++;


            }
            result = true;
        } catch (Exception e) {
            System.out.println("Error actualizando las estaciones:" + e.toString());
            //e.printStackTrace();

        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    System.out.println("Error:" + ex.toString());
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        return result;
    }

    public boolean actualizarUnidades() {
        boolean result = false;

        ConnectionFactory cf = new ConnectionFactory();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs;

        try {
            con = cf.createConnection("redcam");

            String sql = "SELECT CODIGO_UM, DETALLE_UM FROM AUNIDAD_DE_MEDIDA ORDER BY codigo_um";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();


            int contador = 3;


            while (rs.next()) {

                CellReference columna1 = new CellReference("R" + String.valueOf(contador));
                CellReference columna2 = new CellReference("S" + String.valueOf(contador));

                String valor1 = rs.getString("CODIGO_UM");
                String valor2 = rs.getString("DETALLE_UM");


                Row row = sheet.getRow(columna1.getRow());
                Cell cell = row.getCell(columna1.getCol());
                cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                cell.setCellValue(Integer.parseInt(valor1));


                row = sheet.getRow(columna2.getRow());
                cell = row.getCell(columna2.getCol());
                cell.setCellValue(valor2);

                contador++;


            }
            result = true;
        } catch (Exception e) {
            System.out.println("Error actualizando las unidades:" + e.toString());
            //e.printStackTrace();

        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    //    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        return result;
    }

    public boolean actualizarMetodosAnaliticos() {
        boolean result = false;

        ConnectionFactory cf = new ConnectionFactory();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs;

        try {
            con = cf.createConnection("redcam");
            String sql = "SELECT CODIGO,DESCRIPCION FROM AMETODO_ANALITICO order by codigo";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();


            int contador = 3;


            while (rs.next()) {

                CellReference columna1 = new CellReference("U" + String.valueOf(contador));
                CellReference columna2 = new CellReference("V" + String.valueOf(contador));

                String valor1 = rs.getString("CODIGO");
                String valor2 = rs.getString("DESCRIPCION");


                Row row = sheet.getRow(columna1.getRow());
                Cell cell = row.getCell(columna1.getCol());

                try {
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue(Integer.parseInt(valor1));
                } catch (NumberFormatException ex) {
                    cell.setCellValue(valor1);
                }





                row = sheet.getRow(columna2.getRow());
                cell = row.getCell(columna2.getCol());
                cell.setCellValue(valor2);

                contador++;


            }
            result = true;
        } catch (Exception e) {
            System.out.println("Error actualizando los metodos analíticos:" + e.toString());
            //e.printStackTrace();

        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    //  Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        return result;
    }

    public boolean actualizarSustratos() {
        boolean result = false;

        ConnectionFactory cf = new ConnectionFactory();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs;

        try {
            con = cf.createConnection("redcam");

            String sql = "SELECT CODIGO_SUSTRATO_SS, NOMBRE_SS, CODIGO_TIPO_SUSTRATO_SS FROM ASUSTRATO  order by NOMBRE_SS";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();


            int contador = 3;


            while (rs.next()) {

                CellReference columna1 = new CellReference("N" + String.valueOf(contador));
                CellReference columna2 = new CellReference("O" + String.valueOf(contador));
                CellReference columna3 = new CellReference("P" + String.valueOf(contador));

                String codigoSustratoSs = rs.getString("CODIGO_SUSTRATO_SS");
                String nombreSs = rs.getString("NOMBRE_SS");
                String codigoTipoSustratosSs = rs.getString("CODIGO_TIPO_SUSTRATO_SS");

                Row row = sheet.getRow(columna1.getRow());
                Cell cell = row.getCell(columna1.getCol());
                cell.setCellValue(codigoSustratoSs);

                row = sheet.getRow(columna2.getRow());
                cell = row.getCell(columna2.getCol());
                cell.setCellValue(nombreSs);

                row = sheet.getRow(columna3.getRow());
                cell = row.getCell(columna3.getCol());
                cell.setCellValue(codigoTipoSustratosSs);

                contador++;


            }
            result = true;
        } catch (Exception e) {
            System.out.println("Error actualizando los sustratos:" + e.toString());


        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    //     Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        return result;
    }

    public boolean actualizarInvestigadores() {
        boolean result = false;

        ConnectionFactory cf = new ConnectionFactory();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs;

        try {
            con = cf.createConnection("redcam");
            String sql = "SELECT ID,CODIGO_CO FROM PDIRCOLECTORT where sistema_co like '%CAM%' ORDER BY CODIGO_CO";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();


            int contador = 3;


            while (rs.next()) {

                CellReference columna1 = new CellReference("K" + String.valueOf(contador));
                CellReference columna2 = new CellReference("L" + String.valueOf(contador));

                String id = rs.getString("ID");
                String codigoCo = rs.getString("CODIGO_CO");

                Row row = sheet.getRow(columna1.getRow());
                Cell cell = row.getCell(columna1.getCol());
                cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                cell.setCellValue(Integer.parseInt(id));

                row = sheet.getRow(columna2.getRow());
                cell = row.getCell(columna2.getCol());
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(codigoCo);

                contador++;


            }
            result = true;
        } catch (Exception e) {
            System.out.println("Error actualizando los investigadores:" + e.toString());


        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        return result;
    }

    public boolean actualizarProyectos() {
        boolean result = false;

        ConnectionFactory cf = new ConnectionFactory();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs;

        try {
            con = cf.createConnection("redcam");
            String sql = "SELECT CODIGO_PR, NOMBRE_PR, SISTEMA_PR FROM PPROYECTO where SISTEMA_PR like '%CAM%'";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            int contador = 3;

            while (rs.next()) {

                CellReference columna1 = new CellReference("AE" + String.valueOf(contador));
                CellReference columna2 = new CellReference("AF" + String.valueOf(contador));
                CellReference columna3 = new CellReference("AG" + String.valueOf(contador));

                String valor1 = rs.getString("CODIGO_PR");
                String valor2 = rs.getString("NOMBRE_PR");
                String valor3 = rs.getString("SISTEMA_PR");


                Row row = sheet.getRow(columna1.getRow());
                Cell cell = row.getCell(columna1.getCol());
                cell.setCellValue(valor1);


                row = sheet.getRow(columna2.getRow());
                cell = row.getCell(columna2.getCol());
                cell.setCellValue(valor2);

                row = sheet.getRow(columna3.getRow());
                cell = row.getCell(columna3.getCol());
                cell.setCellValue(valor3);

                contador++;


            }
            result = true;
        } catch (Exception e) {
            System.out.println("Error actualizando los proyectos:" + e.toString());
            //e.printStackTrace();

        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        return result;
    }

    public boolean actualizarProfundidad() {
        boolean result = false;

        ConnectionFactory cf = new ConnectionFactory();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs;

        try {
            con = cf.createConnection("redcam");

            String sql = "SELECT CODIGO_PR,NOMBRE_PR FROM APROFUNDIDAD order by nombre_pr";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            int contador = 3;

            while (rs.next()) {

                CellReference columna1 = new CellReference("AI" + String.valueOf(contador));
                CellReference columna2 = new CellReference("AJ" + String.valueOf(contador));

                String valor1 = rs.getString("CODIGO_PR");
                String valor2 = rs.getString("NOMBRE_PR");



                Row row = sheet.getRow(columna1.getRow());
                Cell cell = row.getCell(columna1.getCol());
                cell.setCellValue(valor1);


                row = sheet.getRow(columna2.getRow());
                cell = row.getCell(columna2.getCol());
                cell.setCellValue(valor2);



                contador++;


            }
            result = true;
        } catch (Exception e) {
            System.out.println("Error actualizando la profundidad:" + e.toString());
            //e.printStackTrace();

        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        return result;
    }

    public boolean actualizarEntidades() {
        boolean result = false;

        ConnectionFactory cf = new ConnectionFactory();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs;

        try {
            con = cf.createConnection("redcam");

            String sql = "SELECT distinct SISTEMA_CO FROM PDIRCOLECTORT order by sistema_co";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            int contador = 3;

            while (rs.next()) {

                CellReference columna1 = new CellReference("AL" + String.valueOf(contador));


                String valor1 = rs.getString("SISTEMA_CO");




                Row row = sheet.getRow(columna1.getRow());
                Cell cell = row.getCell(columna1.getCol());
                cell.setCellValue(valor1);

                contador++;


            }
            result = true;
        } catch (Exception e) {
            System.out.println("Error actualizando las entidades:" + e.toString());
            //e.printStackTrace();

        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GeneradorReferentes.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        return result;
    }

    public static void main(String[] args) {

        try {


            String fileName = "C:" + File.separator + "Referentes.xlsm";
            System.out.println(fileName);
            GeneradorReferentes gf = new GeneradorReferentes(fileName);
            gf.actualizarEstaciones();
            gf.actualizarInvestigadores();
            gf.actualizarSustratos();
            gf.actualizarUnidades();
            gf.actualizarMetodosAnaliticos();
            gf.actualizarProyectos();
            gf.actualizarProfundidad();
            gf.actualizarEntidades();
            gf.guardarCambios();
        } catch (Exception ex) {
            System.out.println("Error generando el archivo de referentes:" + ex.toString());
        }
    }
}
