package co.org.invemar.siam.redcam.estadisticas.cvariablegeog;

import java.sql.*;
import java.lang.*;
import java.util.*;

public class ContVariableGeog {

    private String Error;
    private int tamano;
    private Vector resultados;

    public ContVariableGeog() {
    }

    /**
     * carga todas las categorias de la base de datos en un vector de objetos
     */
    public boolean contenedor(Connection conn2, String ntipo, String ndepar, String nsector, String ntemporada, Calendar fecha1, Calendar fecha2, Vector variables, String nprofundidad, String nsustrato, Vector estaciones) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();
        int mes = 0;
        int ano, mes1, dia;
        select = "select * from v_redcam where codvar is not null ";

        if (ntipo != null && !ntipo.equals("")) {
            select = select + " and tipo = '" + ntipo + "'";
        }
        if (ndepar != null && !ndepar.equals("")) {
            select = select + " and depto = '" + ndepar + "'";
        }
        if (nsector != null && !nsector.equals("")) {
            select = select + " and sector = '" + nsector + "'";
        }
        if (ntemporada != null && !ntemporada.equals("")) {
            select = select + " and tempo = '" + ntemporada + "'";
        }
        if (nprofundidad != null && !nprofundidad.equals("")) {
            select = select + " and prof = '" + nprofundidad + "'";
        }
        if (nsustrato != null && !nsustrato.equals("")) {
            select = select + " and sustrato = '" + nsustrato + "'";
        }


        if (fecha1 != null) {
            mes = fecha1.get(Calendar.MONTH);
            ano = fecha1.get(Calendar.YEAR);
            if (mes == 0) {
                mes = 12;
                ano = ano - 1;
            }
            select = select + " and fecha_real >= to_date('" + fecha1.get(Calendar.DATE) + "/";
            select = select + mes + "/" + ano + "','dd/mm/yyyy')";
        }
        if (fecha2 != null) {
            mes = fecha2.get(Calendar.MONTH);
            ano = fecha2.get(Calendar.YEAR);
            if (mes == 0) {
                mes = 12;
                ano = ano - 1;
            }
            select = select + " and fecha_real <= to_date('" + fecha2.get(Calendar.DATE) + "/";
            select = select + mes + "/" + ano + "','dd/mm/yyyy')";
        }
        if (variables != null && variables.size() > 0) {
            select = select + "and (";
            for (int k = 0; k < variables.size(); k++) {
                if (k == 0) {
                    select = select + " nomvar = '" + (String) variables.elementAt(k) + "'";
                } else {
                    select = select + " or nomvar = '" + (String) variables.elementAt(k) + "'";
                }

            }
            select = select + ")";
        }

        if (estaciones != null && estaciones.size() > 0) {
            select = select + "and (";
            for (int k = 0; k < estaciones.size(); k++) {
                if (k == 0) {
                    select = select + " codest = '" + (String) estaciones.elementAt(k) + "'";
                } else {
                    select = select + " or codest = '" + (String) estaciones.elementAt(k) + "'";
                }

            }
            select = select + ")";
        }

        select = select + " order by tipo, codvar";
       // System.out.println("sql:"+select);
        Error = select;
        try {
            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();

                    var.setdepto(rs.getString(1));
                    var.setsector(rs.getString(2));
                    var.setcodest(rs.getString(3));
                    var.setnomest(rs.getString(4));
                    var.setfecha(rs.getString(6));
                    var.setsustrato(rs.getString(7));
                    var.settemporada(rs.getString(8));
                    var.setprofundidad(rs.getString(9));
                    var.settipo(rs.getString(10));
                    var.setcod(rs.getString(11));
                    var.setnombre(rs.getString(12));
                    var.setvalor(rs.getDouble(13));
                    var.setunidad(rs.getString(14));
                    var.setlatitud(rs.getDouble(18));
                    var.setlongitud(rs.getDouble(19));
                    var.setproyecto(rs.getString(20));
                    var.setano(rs.getString(16));

                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;
        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * el mismo contenedor anterior pero para todos los proyectos
     */
    public boolean contenedorc(Connection conn2, String nuser, String nproj, String ntipo, String ndepar, String nsector, String ntemporada, Calendar fecha1, Calendar fecha2, Vector variables, String nprofundidad, String nsustrato, Vector estaciones) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();
        int mes = 0;
        int ano, mes1, dia;
        select = "select * from vm_variablegeog v, ausuario_x_proyecto u where codvar is not null and v.PROYECTO = u.COD_PROYECTO ";

        if (nuser != null && !nuser.equals("")) {
            select = select + " and u.COD_USUARIO = '" + nuser + "'";
        }
        if (nproj != null && !nproj.equals("")) {
            select = select + " and proyecto = '" + nproj + "'";
        }
        if (ntipo != null && !ntipo.equals("")) {
            select = select + " and tipo = '" + ntipo + "'";
        }
        if (ndepar != null && !ndepar.equals("")) {
            select = select + " and depto = '" + ndepar + "'";
        }
        if (nsector != null && !nsector.equals("")) {
            select = select + " and sector = '" + nsector + "'";
        }
        if (ntemporada != null && !ntemporada.equals("")) {
            select = select + " and tempo = '" + ntemporada + "'";
        }
        if (nprofundidad != null && !nprofundidad.equals("")) {
            select = select + " and prof = '" + nprofundidad + "'";
        }
        if (nsustrato != null && !nsustrato.equals("")) {
            select = select + " and sustrato = '" + nsustrato + "'";
        }


        if (fecha1 != null) {
            mes = fecha1.get(Calendar.MONTH);
            ano = fecha1.get(Calendar.YEAR);
            if (mes == 0) {
                mes = 12;
                ano = ano - 1;
            }
            select = select + " and fecha_real >= to_date('" + fecha1.get(Calendar.DATE) + "/";
            select = select + mes + "/" + ano + "','dd/mm/yyyy')";
        }
        if (fecha2 != null) {
            mes = fecha2.get(Calendar.MONTH);
            ano = fecha2.get(Calendar.YEAR);
            if (mes == 0) {
                mes = 12;
                ano = ano - 1;
            }
            select = select + " and fecha_real <= to_date('" + fecha2.get(Calendar.DATE) + "/";
            select = select + mes + "/" + ano + "','dd/mm/yyyy')";
        }
        if (variables != null && variables.size() > 0) {
            select = select + "and (";
            for (int k = 0; k < variables.size(); k++) {
                if (k == 0) {
                    select = select + " nomvar = '" + (String) variables.elementAt(k) + "'";
                } else {
                    select = select + " or nomvar = '" + (String) variables.elementAt(k) + "'";
                }

            }
            select = select + ")";
        }

        if (estaciones != null && estaciones.size() > 0) {
            select = select + "and (";
            for (int k = 0; k < estaciones.size(); k++) {
                if (k == 0) {
                    select = select + " codest = '" + (String) estaciones.elementAt(k) + "'";
                } else {
                    select = select + " or codest = '" + (String) estaciones.elementAt(k) + "'";
                }

            }
            select = select + ")";
        }

        select = select + " order by tipo, codvar";
      //   System.out.println("sql:"+select);
        Error = select;
        try {
            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();

                    var.setdepto(rs.getString(1));
                    var.setsector(rs.getString(2));
                    var.setcodest(rs.getString(3));
                    var.setnomest(rs.getString(4));
                    var.setfecha(rs.getString(6));
                    var.setsustrato(rs.getString(7));
                    var.settemporada(rs.getString(8));
                    var.setprofundidad(rs.getString(9));
                    var.settipo(rs.getString(10));
                    var.setcod(rs.getString(11));
                    var.setnombre(rs.getString(12));
                    var.setvalor(rs.getDouble(13));
                    var.setunidad(rs.getString(14));
                    var.setlatitud(rs.getDouble(18));
                    var.setlongitud(rs.getDouble(19));
                    var.setproyecto(rs.getString(20));
                    var.setano(rs.getString(16));

                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;
        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

//----------------//
    /**
     * Contenedor para visualizaci�n de las estadisticas precalculadas en el
     * base de datos
     */
    public boolean estadisticas1(Connection conn2) //  , String nuser, String nproj, String ntipo, String ndepar, String nsector, String ntemporada, String ano1, String ano2, Vector variables, String nprofundidad, String nsustrato, Vector estaciones, String nregion)
    {
        boolean resp = false;
        Vector resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        select = "select * from aestadisticas where  ( (nivel >= 1 and nivel <=8) or (nivel >= 29 and nivel <= 33) )  and rownum <=100 ";
       // System.out.println("sql:"+select);
        Error = select;
        try {
            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();

                    var.setano(rs.getString(2));
                    var.settemporada(rs.getString(3));
                    var.setdepto(rs.getString(4));
                    var.setsector(rs.getString(5));
                    var.setnomest(rs.getString(6));
                    var.setnombre(rs.getString(7));
                    var.setprom(rs.getDouble(8));
                    var.setnum(rs.getDouble(9));
                    var.setmax(rs.getDouble(10));
                    var.setmin(rs.getDouble(11));
                    var.setstddv(rs.getDouble(12));
                    var.setregion(rs.getString(14));
                    var.setcodest(rs.getString(15));
                    var.setmoda(rs.getDouble(16));
                    var.setmediana(rs.getDouble(17));
                    var.setvarianza(rs.getDouble(18));
                    var.seterror(rs.getDouble(19));
                    var.setcuart25(rs.getDouble(20));
                    var.setcuart75(rs.getDouble(21));

                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;
        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

//----------------//
//----------------//
    public boolean subcontenedor(Connection conn2, String ntipo, String ndepar, String nsector) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();
        select = "select * from v_redcam where codvar is not null";

        if (ntipo != null && !ntipo.equals("")) {
            select = select + " and tipo = '" + ntipo + "'";
        }
        if (ndepar != null && !ndepar.equals("")) {
            select = select + " and depto = '" + ndepar + "'";
        }
        if (nsector != null && !nsector.equals("")) {
            select = select + " and sector = '" + nsector + "'";
        }

        select = select + "  order by tipo, codvar";
         //System.out.println("sql:"+select);
        Error = select;
        try {
            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();

                    var.setdepto(rs.getString(1));
                    var.setsector(rs.getString(2));
                    var.setcodest(rs.getString(3));
                    var.setnomest(rs.getString(4));
                    var.setfecha(rs.getString(6));
                    var.setsustrato(rs.getString(7));
                    var.settemporada(rs.getString(8));
                    var.setprofundidad(rs.getString(9));
                    var.settipo(rs.getString(10));
                    var.setcod(rs.getString(11));
                    var.setnombre(rs.getString(12));
                    var.setvalor(rs.getDouble(13));
                    var.setunidad(rs.getString(14));
                    var.setlatitud(rs.getDouble(18));
                    var.setlongitud(rs.getDouble(19));

                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;
        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * carga todos los tipos de variables de la base de datos
     */
    public boolean contenedorntipo(Connection conn2) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();
        select = "select distinct tipo from v_redcam";

        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.settipo(rs.getString(1));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * carga todos los tipos de variables de la base de datos para todos los
     * proyectos de un usuaerio determinado
     */
    public boolean contenedorntipoc(Connection conn2, String nproj, String opcion, String nuser) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        if (opcion != null && opcion.equals("1")) {
            select = "select distinct INITCAP(tipo) as tipo from v_redcam";
        }
        if (opcion != null && opcion.equals("2")) {
            select = "select distinct INITCAP(v.tipo) as tipo from vm_variablegeog v, ausuario_x_proyecto u where v.tipo is not null and v.PROYECTO = u.COD_PROYECTO ";
        }

        if (nuser != null && !nuser.equals("")) {
            select = select + " and cod_usuario = '" + nuser + "'";
        }

        if (nproj != null && !nproj.equals("")) {
            select = select + " and proyecto = '" + nproj + "'";
        }
        select = select + " order by tipo";

        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.settipo(rs.getString(1));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * carga todos los departamentos donde hay datos
     */
    public boolean contenedorndepto(Connection conn2) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();
        select = "select distinct depto from v_redcam";

        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.setdepto(rs.getString(1));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * carga todos los departamentos donde hay datos
     */
    public boolean contenedorndeptoc(Connection conn2, String nproj, String nuser, String opcion) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        if (opcion != null && opcion.equals("1")) {
            select = "select distinct depto from v_redcam";
        }

        if (opcion != null && opcion.equals("2")) {
            select = "select distinct v.depto,initcap(depto) as depart  from vm_variablegeog v, ausuario_x_proyecto u where depto is not null and v.proyecto = u.cod_proyecto ";
        }

        if (nuser != null && !nuser.equals("")) {
            select = select + " and cod_usuario = '" + nuser + "'";
        }

        if (nproj != null && !nproj.equals("")) {
            select = select + " and proyecto = '" + nproj + "'";
        }
        select = select + "order by depto";
        //System.out.println("sql depart:"+select);
        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.setdepto(rs.getString(1));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * carga todos los sectores de un departamento
     */
    public boolean contenedornsector(Connection conn2, String ndepar) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();
        select = "select distinct sector from v_redcam where sector is not null";
        if (ndepar != null && !ndepar.equals("")) {
            select = select + " and depto = '" + ndepar + "'";
        }
        Error = select;
       // System.out.println("sql nsector"+select);
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.setsector(rs.getString(1));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * carga todos los sectores de un departamento dado un proyecto, usuario y
     * una opcion (que puede ser 1 o 2)
     */
    public boolean contenedornsectorc(Connection conn2, String ndepar, String nproj, String opcion, String nuser) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        if (opcion != null && opcion.equals("1")) {
            select = "select distinct v.sector from v_redcam where sector is not null";
        }
        if (opcion != null && opcion.equals("2")) {
            select = "select distinct v.sector,s.codigo_sc from vm_variablegeog v, ausuario_x_proyecto u,ASECTOR s where v.tipo is not null and v.PROYECTO = u.COD_PROYECTO and s.descripcion_sc= v.sector ";
        }

        if (nuser != null && !nuser.equals("")) {
            select = select + " and cod_usuario = '" + nuser + "'";
        }

        if (nproj != null && !nproj.equals("")) {
            select = select + " and proyecto = '" + nproj + "'";
        }

        if (ndepar != null && !ndepar.equals("")) {
            select = select + " and depto = '" + ndepar + "'";
        }
        select = select + "order by sector";
       // System.out.println("sql sector:"+select);
        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.setsector(rs.getString(1));
                   
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }


    /*
     * contenedor de variables, recibe la conexion (conn2) y el tipo de variable (vari)
     */
    public boolean contenedorvari(Connection conn2, String vari) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();
        select = "select distinct nomvar from v_redcam where tipo = '" + vari + "'";
       // System.out.println("sql:"+select);
        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.setnombre(rs.getString(1));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }


    /*
     * contenedor de variables, recibe la conexion (conn2) y el tipo de variable (vari)
     */
    public boolean contenedorvaric(Connection conn2, String vari, String nproj, String nuser, String opcion) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        if (opcion != null && opcion.equals("1")) {
            select = "select distinct nomvar from v_redcam where tipo = '" + vari + "'";
        }
        if (opcion != null && opcion.equals("2")) {
          //  select = "select distinct v.nomvar from vm_variablegeog v, ausuario_x_proyecto u  where tipo = '" + vari + "' and v.PROYECTO = u.COD_PROYECTO";// previus version sql.
            select = "SELECT  distinct nomvar  FROM vm_variablegeog v, ausuario_x_proyecto u,ptipo_variable tv, pvariable pv WHERE tv.codigo_tp ='"+vari+"' and pv.codigo_vr= v.codvar  and tv.descripcion_tp=tipo AND v.PROYECTO  = u.COD_PROYECTO ";
        }

        if (nuser != null && !nuser.equals("")) {
            select = select + " and cod_usuario = '" + nuser + "'";
        }

        if (nproj != null && !nproj.equals("")) {
            select = select + " and proyecto = '" + nproj + "'";
        }

        select = select +" and pv.vigencia_vr='S' order by nomvar";
       // System.out.println("sql:"+select);         
        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            while(rs.next()) {             
                
                    var = new VariableGeog();
                    var.setnombre(rs.getString(1));
                    resultados.addElement(var);
                    tamano++;
                
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /*
     * contenedor de variables, recibe la conexion (conn2) y el tipo de variable (vari)
     */
    public boolean contenedortipovaric(Connection conn2, String nproj, String nuser, String opcion) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        if (opcion != null && opcion.equals("1")) {
            select = "select distinct tipo, nomvar, codvar from v_redcam where tipo is not null ";
        }
        if (opcion != null && opcion.equals("2")) {
            select = "select distinct v.tipo, v.nomvar, v.codvar from vm_variablegeog v, ausuario_x_proyecto u  where tipo is not null and v.PROYECTO = u.COD_PROYECTO";
        }

        if (nuser != null && !nuser.equals("")) {
            select = select + " and cod_usuario = '" + nuser + "'";
        }

        if (nproj != null && !nproj.equals("")) {
            select = select + " and proyecto = '" + nproj + "'";
        }

        select = select + " order by tipo, nomvar ";
        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.settipo(rs.getString(1));
                    var.setnombre(rs.getString(2));
                    var.setcod(rs.getString(3));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /*
     * contenedor de variables, recibe la conexion (conn2) y el tipo de variable (vari)
     */
    public boolean contenedorvariable_estacion(Connection conn2, String nproj, String nuser, String opcion, String codestacion) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        if (opcion != null && opcion.equals("1")) {
            select = "select distinct tipo, nomvar, codvar from v_redcam where tipo is not null ";
        }
//siempre se entra por ac�
        if (opcion != null && opcion.equals("2")) {
            select = "select distinct v.tipo, v.nomvar, v.codvar from vm_variablegeog v, ausuario_x_proyecto u  where tipo is not null and v.PROYECTO = u.COD_PROYECTO and v.PROF = 'S' and v.tipo_SUSTRATO='SA' ";
        }

        if (nuser != null && !nuser.equals("")) {
            select = select + " and cod_usuario = '" + nuser + "'";
        }

        if (nproj != null && !nproj.equals("")) {
            select = select + " and proyecto = '" + nproj + "'";
        }
        if (codestacion != null && !codestacion.equals("")) {
            select = select + " and v.CODEST = '" + codestacion + "'";
        }

        select = select + " order by tipo, nomvar ";
        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.settipo(rs.getString(1));
                    var.setnombre(rs.getString(2));
                    var.setcod(rs.getString(3));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /*
     * contenedor de variables, recibe la conexion (conn2) y el tipo de variable (vari)
     */
    public boolean contenedorano_estacion(Connection conn2, String nproj, String nuser, String opcion, String codestacion, String codvar) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        /*
         * if(opcion != null && opcion.equals("1")){ select = "select distinct
         * ano from v_redcam where tipo is not null "; }
         */
        //siempre se entra por ac�
        if (opcion != null && opcion.equals("2")) {
            select = "select distinct v.ano from vm_variablegeog v, ausuario_x_proyecto u  where tipo is not null and v.PROYECTO = u.COD_PROYECTO ";
        }

        if (nuser != null && !nuser.equals("")) {
            select = select + " and cod_usuario = '" + nuser + "'";
        }

        if (nproj != null && !nproj.equals("")) {
            select = select + " and proyecto = '" + nproj + "'";
        }
        if (codestacion != null && !codestacion.equals("")) {
            select = select + " and v.CODEST = '" + codestacion + "'";
        }
        if (codvar != null && !codvar.equals("")) {
            select = select + " and v.codvar = '" + codvar + "'";
        }

        select = select + " order by ano ";
        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.setano(rs.getString(1));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * contenedor de estaciones, recibe la conexion (conn2) y el sector (secto)
     */
    public boolean contenedoresta(Connection conn2, String secto) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();
        select = "select distinct codest, nomest from v_redcam where codest is not null";

        if (secto != null && !secto.equals("")) {
            select = select + " and sector = '" + secto + "'";
        }

        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.setcodest(rs.getString(1));
                    var.setnomest(rs.getString(2));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * contenedor de estaciones, recibe la conexion (conn2) y el sector (secto)
     */
    public boolean contenedorestac(Connection conn2, String secto, String nproj, String nuser, String opcion) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        if (opcion != null && opcion.equals("1")) {
            select = "select distinct codest, nomest from v_redcam where codest is not null";
        }
        if (opcion != null && opcion.equals("2")) {
            select = "select distinct v.codest, v.nomest from vm_variablegeog v, ausuario_x_proyecto u,ASECTOR s where codest is not null and s.descripcion_sc= v.sector and v.PROYECTO = u.COD_PROYECTO ";
        }

        if (nuser != null && !nuser.equals("")) {
            select = select + " and cod_usuario = '" + nuser + "'";
        }

        if (nproj != null && !nproj.equals("")) {
            select = select + " and proyecto = '" + nproj + "'";
        }

        if (secto != null && !secto.equals("")) {
            select = select + " and s.codigo_sc = '" + secto + "'";
        }

        Error = select;
        //System.out.println("sql estaciones:"+select);
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.setcodest(rs.getString(1));
                    var.setnomest(rs.getString(2));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * contenedor de estaciones, recibe la conexion (conn2) y el sector (secto)
     */
    public boolean contenedordeptoestac(Connection conn2, String secto, String nproj, String nuser, String opcion) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        if (opcion != null && opcion.equals("1")) {
            select = "select distinct depto, codest, nomest from v_redcam where codest is not null";
        }
        if (opcion != null && opcion.equals("2")) {
            select = "select distinct v.depto, v.codest, v.nomest from vm_variablegeog v, ausuario_x_proyecto u where codest is not null and v.PROYECTO = u.COD_PROYECTO ";
        }

        if (nuser != null && !nuser.equals("")) {
            select = select + " and cod_usuario = '" + nuser + "'";
        }

        if (nproj != null && !nproj.equals("")) {
            select = select + " and proyecto = '" + nproj + "'";
        }

        if (secto != null && !secto.equals("")) {
            select = select + " and sector = '" + secto + "'";
        }

        select = select + " order by depto, nomest ";

        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();
                    var.setdepto(rs.getString(1));
                    var.setcodest(rs.getString(2));
                    var.setnomest(rs.getString(3));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * * CONTENEDOR DE PRUEBA DE ESTADISTICAS
     */
    public boolean estadisticas2(Connection conn2, String nuser, String nproj, String ntipo, String ndepar, String nsector, String ntemporada, String ano1, String ano2, Vector variables, String nprofundidad, String nsustrato, Vector estaciones, String nregion) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        Vector cod_nivel = new Vector();
        Vector valor_nivel = new Vector();
        String texto = new String();
        int mes = 0;
        int ano, mes1, dia, valor_decimal = 0;

        select = "select * from aestadisticas ae,  V_VARIABLETIPO vt,ASECTOR s where  ae.VARIABLE = VT.COD_VAR and s.descripcion_sc = ae.sector ";

        /*
         * if(nuser != null && !nuser.equals("")){ select = select + " and
         * u.COD_USUARIO = '" + nuser + "'"; }
         */

        /**
         * Llenado del vector que contiene los valores decimales de las
         * combinaciones de parametros en binario
         */
        cod_nivel.addElement("1");
        cod_nivel.addElement("1");
        cod_nivel.addElement("1");
        cod_nivel.addElement("2");
        cod_nivel.addElement("2");
        cod_nivel.addElement("2");
        cod_nivel.addElement("3");
        cod_nivel.addElement("3");
        cod_nivel.addElement("4");
        cod_nivel.addElement("5");
        cod_nivel.addElement("5");
        cod_nivel.addElement("5");
        cod_nivel.addElement("6");
        cod_nivel.addElement("6");
        cod_nivel.addElement("6");
        cod_nivel.addElement("7");
        cod_nivel.addElement("7");
        cod_nivel.addElement("8");
        cod_nivel.addElement("29");
        cod_nivel.addElement("29");
        cod_nivel.addElement("29");
        cod_nivel.addElement("30");
        cod_nivel.addElement("30");
        cod_nivel.addElement("30");
        cod_nivel.addElement("31");
        cod_nivel.addElement("31");
        cod_nivel.addElement("32");
        cod_nivel.addElement("33");

        valor_nivel.addElement("7");
        valor_nivel.addElement("31");
        valor_nivel.addElement("63");
        valor_nivel.addElement("11");
        valor_nivel.addElement("27");
        valor_nivel.addElement("59");
        valor_nivel.addElement("19");
        valor_nivel.addElement("51");
        valor_nivel.addElement("35");
        valor_nivel.addElement("6");
        valor_nivel.addElement("30");
        valor_nivel.addElement("62");
        valor_nivel.addElement("10");
        valor_nivel.addElement("26");
        valor_nivel.addElement("58");
        valor_nivel.addElement("18");
        valor_nivel.addElement("50");
        valor_nivel.addElement("34");
        valor_nivel.addElement("5");
        valor_nivel.addElement("29");
        valor_nivel.addElement("61");
        valor_nivel.addElement("9");
        valor_nivel.addElement("25");
        valor_nivel.addElement("57");
        valor_nivel.addElement("17");
        valor_nivel.addElement("49");
        valor_nivel.addElement("33");
        valor_nivel.addElement("0");


        if (nregion != null && !nregion.equals("")) {
            valor_decimal = valor_decimal + 32;
            select = select + " and region = '" + nregion + "'";
        }
        if (ndepar != null && !ndepar.equals("")) {
            valor_decimal = valor_decimal + 16;
            select = select + " and depto = '" + ndepar + "'";
        }
        if (nsector != null && !nsector.equals("")) {
            valor_decimal = valor_decimal + 8;
            select = select + " and s.codigo_sc  = '" + nsector + "'";
        }
        if (estaciones != null && estaciones.size() > 0) {
            valor_decimal = valor_decimal + 4;
            select = select + " and (";
            for (int k = 0; k < estaciones.size(); k++) {
                if (k == 0) {
                    select = select + " codest = '" + (String) estaciones.elementAt(k) + "'";
                } else {
                    select = select + " or codest = '" + (String) estaciones.elementAt(k) + "'";
                }
            }
            select = select + ")";
        }
        if (ano1 != null && !ano1.equals("") && ano2.equals("")) {
            valor_decimal = valor_decimal + 2;
            select = select + " and ano = '" + ano1 + "' ";
        }
        if (ano1 != null && !ano1.equals("") && ano2 != null && !ano2.equals("")) {
            valor_decimal = valor_decimal + 2;
            select = select + " and ano >= '" + ano1 + "' and ano <= '" + ano2 + "' ";
        }
        if (ntemporada != null && !ntemporada.equals("")) {
            valor_decimal = valor_decimal + 1;
            select = select + " and temporada = '" + ntemporada + "'";
        }

        if (ntipo != null && !ntipo.equals("")) {
            select = select + " and VT.TIPO_VR   = '" + ntipo + "'";
        }
        if (nprofundidad != null && !nprofundidad.equals("")) {
            select = select + " and profundidad = '" + nprofundidad + "'";
        }
        if (nsustrato != null && !nsustrato.equals("")) {
            select = select + " and tipo_sustrato = '" + nsustrato + "'";
        }
        if (nproj != null && !nproj.equals("")) {
            select = select + " and prj = '" + nproj + "'";
        }

        if (variables != null && variables.size() > 0) {
            select = select + " and (";
            for (int k = 0; k < variables.size(); k++) {
                if (k == 0) {
                    select = select + " nom_var = '" + (String) variables.elementAt(k) + "'";
                } else {
                    select = select + " or nom_var = '" + (String) variables.elementAt(k) + "'";
                }
            }
            select = select + ")";
        }

        int bandera = 0;
        for (int i = 0; i < valor_nivel.size(); i++) {
            texto = (String) valor_nivel.elementAt(i);
            if (valor_decimal == Integer.parseInt(texto)) {
                if (bandera == 0) {
                    select = select + " and ( nivel = " + cod_nivel.elementAt(i) + " ";
                    bandera = 1;
                } else {
                    select = select + " or nivel = " + cod_nivel.elementAt(i) + " ";
                }

            }
        }
        if (bandera == 1) {
            select = select + " ) ";
            //bandera = 0;
        }

// Si no se seleccino ningun valor de las combinaciones validas
        if (valor_decimal == 0) {
            select = select + " and nivel = 33 ";
        }

// Si se seleccino una combinacion de parametros no valida
        if (valor_decimal != 0 && bandera == 0) {
            /*
             * select = select + " and rownum = 0 ";
             */
            select = select + " order by ano asc ";
        }


       // System.out.println("sql estadisticas:"+select);
        Error = select;
        try {
            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();

                    var.setano(rs.getString(2));
                    var.settemporada(rs.getString(3));
                    var.setdepto(rs.getString(4));
                    var.setsector(rs.getString(5));
                    var.setnomest(rs.getString(6));
                    var.setnombre(rs.getString(7));
                    var.setprom(rs.getDouble(8));
                    var.setnum(rs.getDouble(9));
                    var.setmax(rs.getDouble(10));
                    var.setmin(rs.getDouble(11));
                    var.setstddv(rs.getDouble(12));
                    var.setregion(rs.getString(14));
                    var.setcodest(rs.getString(15));
                    var.setmoda(rs.getDouble(16));
                    var.setmediana(rs.getDouble(17));
                    var.setvarianza(rs.getDouble(18));
                    var.seterror(rs.getDouble(19));
                    var.setcuart25(rs.getDouble(20));
                    var.setcuart75(rs.getDouble(21));
                    var.setsustrato(rs.getString(24));
                    var.setcuart90(rs.getDouble(26));
                    var.setcuart10(rs.getDouble(25));

                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;
        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * * CONTENEDOR DE ESTADISTICAS NIVEL 1 DE UNA ESTACION
     */
    public boolean estadisticas3(Connection conn2, String nuser, String nproj, String ndepar, String nsector, String ntemporada, String ano1, String ano2, String variable, String estacion, String nregion) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        VariableGeog var = new VariableGeog();
        String select = new String();

        String texto = new String();
        int mes = 0;
        int ano, mes1, dia, valor_decimal = 0;

        select = "select * from DATOS_RDCAM_ESTACION ae where codest is not null ";

        /*
         * if(nregion != null && !nregion.equals("")){ select = select + " and
         * region = '" + nregion + "'"; } if(ndepar != null &&
         * !ndepar.equals("")){ select = select + " and depto = '" + ndepar +
         * "'"; } if(nsector != null && !nsector.equals("")){ select = select +
         * " and sector = '" + nsector + "'"; }
         */
        if (ano1 != null && !ano1.equals("") && ano2.equals("")) {
            select = select + " and ano = '" + ano1 + "' ";
        }
        if (ano1 != null && !ano1.equals("") && ano2 != null && !ano2.equals("")) {
            select = select + " and ano >= '" + ano1 + "' and ano <= '" + ano2 + "' ";
        }
        if (ntemporada != null && !ntemporada.equals("")) {
            select = select + " and temporada = '" + ntemporada + "'";
        }
        /*
         * if(nproj != null && !nproj.equals("")){ select = select + " and prj =
         * '" + nproj + "'"; }
         */
        if (variable != null && !variable.equals("")) {
            select = select + " and variable = '" + variable + "'";
        }
        if (estacion != null && !estacion.equals("")) {
            select = select + " and codest = '" + estacion + "'";
        }


        /*
         * if(ntipo != null && !ntipo.equals("")){ select = select + " and tipo
         * = '" + ntipo + "'"; } if(nprofundidad != null &&
         * !nprofundidad.equals("")){ select = select + " and profundidad = '" +
         * nprofundidad + "'"; } if(nsustrato != null && !nsustrato.equals("")){
         * select = select + " and tipo_sustrato = '" + nsustrato + "'"; }
         */

        Error = select;
        try {
            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new VariableGeog();

                    var.setano(rs.getString(10));
                    var.settemporada(rs.getString(11));
                    var.setdepto(rs.getString(8));
                    var.setsector(rs.getString(9));
                    var.setnomest(rs.getString(4));
                    var.setcod(rs.getString(12));    /*
                     * este es el codvariable
                     */
                    var.setnombre(rs.getString(16)); /*
                     * esta es el nomvariable
                     */
                    var.setprom(rs.getDouble(14));
                    var.setregion(rs.getString(7));
                    var.setcodest(rs.getString(1));
                    var.setanotemp(rs.getString(13));
                    var.setunidad(rs.getString(15));
                    var.setlatitud(rs.getDouble(2));
                    var.setlongitud(rs.getDouble(3));

                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;
        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     *
     * /** retorna una instancia del contenedor de VariableGeog dada su posicion
     * en el vector
     */
    public VariableGeog getvariablegeog(int i) {
        return ((VariableGeog) resultados.elementAt(i));
    }

    /**
     * retorna el tamano del vector contenedor
     */
    public int gettamano() {
        return tamano;
    }

    /**
     * retorna el error generado por la basse de datos
     */
    public String geterror() {
        return Error;
    }
}
