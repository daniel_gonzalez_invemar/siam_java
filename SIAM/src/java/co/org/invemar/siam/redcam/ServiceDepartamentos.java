/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam;

import co.org.invemar.siam.ConnectionFactory;
import co.org.invemar.siam.redcam.coneccam.ConexionCam;
import co.org.invemar.siam.redcam.estadisticas.cvariablegeog.VariableGeog;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class ServiceDepartamentos {

    private Connection con = null;
    private ConexionCam conexioncam = null;

    public ServiceDepartamentos() {
       
    }
    public ArrayList getTodosDepartamentos(){
       ArrayList list = new ArrayList();
        
        String sql = "SELECT DISTINCT depto, "
                + "  initcap(depto) AS depart "
                + "FROM aestadisticas ae, "
                + "  V_VARIABLETIPO vt, "
                + "  ASECTOR s "
                + "WHERE ae.VARIABLE    = VT.COD_VAR "
                + "AND s.descripcion_sc = ae.sector "              
                + "ORDER BY depart";
        try {
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");          
            PreparedStatement pstmt = null;
            pstmt = con.prepareStatement(sql);
           // System.out.println("sql:"+sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                DepartamentosVo departamento = new DepartamentosVo();
                departamento.setId(rs.getString("depto"));
                departamento.setNombre(rs.getString("depart"));
                list.add(departamento);
            }

        } catch (Exception ex) {
            System.out.println("Error obteniendo los departamentos por regiones:" + ex.getMessage());
        } finally {
           try {
               con.close();
               con = null;
           } catch (SQLException ex) {
               Logger.getLogger(ServiceDepartamentos.class.getName()).log(Level.SEVERE, null, ex);
           }

        }
        return list; 
    }
    public ArrayList getDepartamentosPorRegion(String region) {
        ArrayList list = new ArrayList();
        
        String sql = "SELECT DISTINCT depto, "
                + "  initcap(depto) AS depart "
                + "FROM aestadisticas ae, "
                + "  V_VARIABLETIPO vt, "
                + "  ASECTOR s "
                + "WHERE ae.VARIABLE    = VT.COD_VAR "
                + "AND s.descripcion_sc = ae.sector "
                + "AND region           = '"+region+"' "
                + "ORDER BY depart";
        try {
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");          
            PreparedStatement pstmt = null;
            pstmt = con.prepareStatement(sql);
           // System.out.println("sql:"+sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                DepartamentosVo departamento = new DepartamentosVo();
                departamento.setId(rs.getString("depto"));
                departamento.setNombre(rs.getString("depart"));
                list.add(departamento);
            }

        } catch (Exception ex) {
            System.out.println("Error obteniendo los departamentos por regiones:" + ex.getMessage());
        } finally {
             CerrarConexion(con);

        }
        return list;
    }

    public ArrayList getSectoresRedCam(String nproj, String nuser, String opcion) {

        ArrayList list = new ArrayList();
        String sql = null;
        try {

            if (opcion != null && opcion.equals("1")) {
                sql = "select distinct depto from v_redcam";
            }

            if (opcion != null && opcion.equals("2")) {
                sql = "select distinct v.depto,initcap(depto) as depart  from vm_variablegeog v, ausuario_x_proyecto u where depto is not null and v.proyecto = u.cod_proyecto ";
            }

            if (nuser != null && !nuser.equals("")) {
                sql = sql + " and cod_usuario = '" + nuser + "'";
            }

            if (nproj != null && !nproj.equals("")) {
                sql = sql + " and proyecto = '" + nproj + "'";
            }
            sql = sql + "order by depto";

            PreparedStatement pstmt = null;
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                DepartamentosVo departamento = new DepartamentosVo();
                departamento.setId(rs.getString("depto"));
                departamento.setNombre(rs.getString("depart"));
                list.add(departamento);
            }


        } catch (SQLException ex) {
            System.out.println("Error obteniendo los tipos de variables redcam:" + ex.getMessage());
        } finally {
            CerrarConexion(con);
        }
        return list;



    }
    private void CerrarConexion(Connection con) {
        try {
            if(con!=null){
            con.close();
            con = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
