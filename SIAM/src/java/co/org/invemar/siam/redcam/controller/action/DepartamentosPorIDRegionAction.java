/**
 *
 */
package co.org.invemar.siam.redcam.controller.action;

import co.org.invemar.util.actions.Action;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import co.org.invemar.util.actions.ActionRouter;
import java.io.PrintWriter;

/**
 * @author Administrador
 *
 */
public class DepartamentosPorIDRegionAction implements Action{

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        response.setContentType("text/html charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();          
       
       
        String html="<select id='niveldepartamento' name='niveldepartamento' onchange='getComboBoxSectores()'>";
         
        html+="</select>";      
        
       
        pw.write(html);
        //System.out.println("html:"+html);
        return null;//new ActionRouter("/sibm/ficha_catalogo.jsp?" + request.getQueryString());
    }
}
