/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam;

import co.org.invemar.siam.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class ServiceTemporadas {

    public ArrayList geTemporadas(String region, String departamento) {
        ArrayList list = new ArrayList();
        Connection con = null;
        String sql = null;
        
       // System.out.println("Departamento:"+departamento);
        
        if (region != null && departamento != null ) {
            sql = "SELECT DISTINCT NVL(temporada,'')            AS codtemporada, "
                    + "  DECODE(temporada,1,'Seca',2,'LLuviosa','-') AS temporada "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt, "
                    + "  ASECTOR s "
                    + "WHERE ae.VARIABLE    = VT.COD_VAR "
                    + "AND s.descripcion_sc = ae.sector "
                    + "AND region           = '"+region+"' "
                    + "AND depto            = '"+departamento+"' "
                    + "ORDER BY temporada";
            //System.out.println("111111111111111");
        } else if (region != null && departamento == null ) {
            sql = "SELECT DISTINCT NVL(temporada,'')            AS codtemporada, "
                    + "  DECODE(temporada,1,'Seca',2,'LLuviosa','-') AS temporada "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt, "
                    + "  ASECTOR s "
                    + "WHERE ae.VARIABLE    = VT.COD_VAR "
                    + "AND s.descripcion_sc = ae.sector "
                    + "AND region           = '"+region+"' "                  
                    + "ORDER BY temporada";
             //System.out.println("2222222222222222222222");
        } else if (region == null && departamento != null  ) {
            sql = "SELECT DISTINCT NVL(temporada,'')            AS codtemporada, "
                    + "  DECODE(temporada,1,'Seca',2,'LLuviosa','-') AS temporada "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt, "
                    + "  ASECTOR s "
                    + "WHERE ae.VARIABLE    = VT.COD_VAR "
                    + "AND s.descripcion_sc = ae.sector "                 
                    + "AND depto            = '"+departamento+"' "
                    + "ORDER BY temporada";
            //System.out.println("333333333333333333333");
        } else if (region == null && departamento == null ) {
            sql = "SELECT DISTINCT NVL(temporada,'')            AS codtemporada, "
                    + "  DECODE(temporada,1,'Seca',2,'LLuviosa','-') AS temporada "
                    + "FROM aestadisticas ae, "
                    + "  V_VARIABLETIPO vt, "
                    + "  ASECTOR s "
                    + "WHERE ae.VARIABLE    = VT.COD_VAR "
                    + "AND s.descripcion_sc = ae.sector "                    
                    + "ORDER BY temporada";
           // System.out.println("44444444444444444444");
        }

        try {
           // System.out.println("sql temporadas:" + sql);
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");
            PreparedStatement pstmt = null;
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                TemporadaVo temporada = new TemporadaVo();
                temporada.setCodigo(rs.getString("codtemporada"));
                temporada.setNombre(rs.getString("temporada"));
                list.add(temporada);
            }

        } catch (Exception ex) {
            System.out.println("Error obteniendo las temporadas:" + ex.getMessage());
        } finally {
              try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceProfundidad.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return list;
    }
    
    private void CerrarConexion(Connection con) {
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
