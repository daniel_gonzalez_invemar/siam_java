/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam;

import co.org.invemar.siam.ConnectionFactory;
import co.org.invemar.siam.redcam.coneccam.ConexionCam;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class ServiceSectoresRedCam {

    private Connection con = null;
    private ConexionCam conexioncam = null;

    public ServiceSectoresRedCam() {

    }

    public String getSectorOfDepartament(String ndepar, String nproj) {
        JSONArray jsonarray = new JSONArray();
        String sql = null;
        ConnectionFactory cf = new ConnectionFactory();
        try {

            sql = "SELECT DISTINCT v.sector,s.codigo_sc FROM vm_variablegeog v,ASECTOR s WHERE v.tipo IS NOT NULL AND s.descripcion_sc= v.sector";

            if (nproj != null && !nproj.equals("")) {
                sql = sql + " and proyecto = '" + nproj + "'";
            }

            if (ndepar != null && !ndepar.equals("")) {
                sql = sql + " and depto = '" + ndepar + "'";
            }
            sql = sql + " order by sector";
            //System.out.println("sql sectores:"+sql);
            PreparedStatement pstmt = null;
            con = cf.createConnection("redcam");
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                JSONObject jsonobject = new JSONObject();
                jsonobject.put("codigo_sc", rs.getString("codigo_sc"));
                jsonobject.put("sector", rs.getString("sector"));
                jsonarray.put(jsonobject);
            }
        } catch (JSONException ex) {
            Logger.getLogger(ServiceSectoresRedCam.class.getName()).log(Level.SEVERE, "Error getting the json array sector of departament", ex.getMessage());
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, "Error getting the json array sector of departament :", ex.getMessage());
        } finally {
              try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceProfundidad.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        return jsonarray.toString();
    }

    public ArrayList getSectoresRedCam(String ndepar, String nproj, String opcion, String nuser) {

        ArrayList list = new ArrayList();
        String sql = null;
        ConnectionFactory cf = new ConnectionFactory();
        try {

            if (opcion != null && opcion.equals("2")) {
                sql = "SELECT DISTINCT v.sector,s.codigo_sc FROM vm_variablegeog v,ASECTOR s WHERE v.tipo IS NOT NULL AND s.descripcion_sc= v.sector";
            }

           
            if (nproj != null && !nproj.equals("")) {
                sql = sql + " and proyecto = '" + nproj + "'";
            }

            if (ndepar != null && !ndepar.equals("")) {
                sql = sql + " and depto = '" + ndepar + "'";
            }
            sql = sql + " order by sector";
            //System.out.println("sql sectores:"+sql);
            PreparedStatement pstmt = null;
            con = cf.createConnection("redcam");
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                SectoresVo sectores = new SectoresVo();
                sectores.setCodigo(rs.getString("codigo_sc"));
                sectores.setNombre(rs.getString("sector"));
                list.add(sectores);
            }

        } catch (SQLException ex) {
            System.out.println("Error obteniendo los sectores de la redcam:" + ex.getMessage());
        } finally {
              try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(ServiceProfundidad.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return list;

    }

    private void CerrarConexion(Connection con) {
        try {
            if (con != null) {
                con.close();
                con = null;

            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEstaciones.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
}
