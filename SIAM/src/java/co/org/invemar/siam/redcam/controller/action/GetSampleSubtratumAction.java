/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam.controller.action;

import co.org.invemar.siam.redcam.ServiceSampleTypeSubstratum;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usrsig15
 */
public class GetSampleSubtratumAction implements Action{
    
    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        response.setContentType("text/json charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();    
        
        String codVar = request.getParameter("codVariable");
        String codStation = request.getParameter("codStation");
        String year1 = request.getParameter("year1");
        String year2 = request.getParameter("year2");     
        
        ServiceSampleTypeSubstratum sstp = new ServiceSampleTypeSubstratum();
        String json = sstp.getSampleSubtratum(year1,year2,codStation,codVar);     
        pw.write(json);       
        return null;
    }
    
}
