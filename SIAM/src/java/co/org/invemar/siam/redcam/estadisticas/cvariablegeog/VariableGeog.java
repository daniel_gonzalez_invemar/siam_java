package co.org.invemar.siam.redcam.estadisticas.cvariablegeog;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

public class VariableGeog{
   private String depto;
   private String sector;
   private String codest;
   private String nomest;
   private String fecha;
   private String temporada;
   private String profundidad;
   private String sustrato;
   private String region;

   private String tipo;
   private String cod;
   private String nombre;
   private double valor;

   private double lat;
   private double lon;

   private String proyecto;
   private String unidad;
   private String ano;

   private double prom;
   private double num;
   private double max;
   private double min;
   private double stddv;
   private double moda;
   private double mediana;
   private double varianza;
   private double error;
   private double cuart25;
   private double cuart75;
   private double cuart90;
   private double cuart10;

   private String anotemp;

   private String Error;

/** Metodo constructor de la clase */
  public void Cvariablegeog(){
  }


/** Carga el atributo ano al objeto */
  public void setdepto(String adepto)
  {
    depto = adepto;
  }
  public void setsector(String asector)
  {
    sector = asector;
  }
  public void setcodest(String acodest)
  {
    codest = acodest;
  }
  public void setnomest(String anomest)
  {
    nomest = anomest;
  }
  public void setfecha(String afecha)
  {
    fecha = afecha;
  }
  public void settemporada(String atemporada)
  {
    temporada = atemporada;
  }
  public void setprofundidad(String aprofundidad)
  {
    profundidad = aprofundidad;
  }
  public void setsustrato(String asustrato)
  {
    sustrato = asustrato;
  }
  public void setregion(String aregion)
  {
    region = aregion;
  }



  public void settipo(String atipo)
  {
    tipo = atipo;
  }
  public void setcod(String acod)
  {
    cod = acod;
  }
  public void setnombre(String anombre)
  {
    nombre = anombre;
  }
  public void setvalor(double avalor)
  {
    valor = avalor;
  }
  public void setunidad(String aunidad)
  {
    unidad = aunidad;
  }
  public void setano(String aano)
  {
    ano = aano;
  }
  public void setproyecto(String aproyecto)
  {
    proyecto = aproyecto;
  }
  public void setlatitud(double alatitud)
  {
    lat = alatitud;
  }
  public void setlongitud(double alongitud)
  {
    lon = alongitud;
  }


  public void setprom(double aprom)
  {
    prom = aprom;
  }
  public void setnum(double anum)
  {
    num = anum;
  }
  public void setmax(double amax)
  {
    max = amax;
  }
  public void setmin(double amin)
  {
    min = amin;
  }
  public void setstddv(double astddv)
  {
    stddv = astddv;
  }
  public void setmoda(double amoda)
  {
    moda = amoda;
  }
  public void setmediana(double amediana)
  {
    mediana = amediana;
  }
  public void setvarianza(double avarianza)
  {
    varianza = avarianza;
  }
  public void seterror(double aerror)
  {
    error = aerror;
  }
  public void setcuart25(double acuart25)
  {
    cuart25 = acuart25;
  }
  public void setcuart75(double acuart75)
  {
    cuart75 = acuart75;
  }
  public void setcuart90(double acuart90)
  {
    cuart90 = acuart90;
  }
  public void setcuart10(double acuart10)
  {
    cuart10 = acuart10;
  }
  public void setanotemp(String aanotemp)
    {
      anotemp = aanotemp;
    }


  /**  Retorna el atributo a�o del objeto  */
 public String get_depto()
  {
    return depto;
  }
  public String get_sector()
  {
    return sector;
  }
  public String get_codest()
  {
    return codest;
  }
  public String get_nomest()
  {
    return nomest;
  }
  public String get_fecha()
  {
    return fecha;
  }
  public String get_temporada()
  {
    return temporada;
  }
  public String get_profundidad()
  {
    return profundidad;
  }
  public String get_sustrato()
  {
    return sustrato;
  }
  public String get_region()
  {
    return region;
  }


  public String get_tipo()
  {
    return tipo;
  }
  public String get_cod()
  {
    return cod;
  }
  public String get_nombre()
  {
    return nombre;
  }
  public double get_valor()
  {
    return valor;
  }
  public String get_unidad()
  {
    return unidad;
  }
  public String get_ano()
  {
    return ano;
  }
  public String get_proyecto()
  {
    return proyecto;
  }
  public double get_latitud()
  {
    return lat;
  }
  public double get_longitud()
  {
    return lon;
  }


  public double get_prom()
  {
    return prom;
  }
  public double get_num()
  {
    return num;
  }
  public double get_max()
  {
    return max;
  }
  public double get_min()
  {
    return min;
  }
  public double get_stddv()
  {
    return stddv;
  }
  public double get_moda()
  {
    return moda;
  }
  public double get_mediana()
  {
    return mediana;
  }
  public double get_varianza()
  {
    return varianza;
  }
  public double get_cuart25()
  {
    return cuart25;
  }
  public double get_cuart75()
  {
    return cuart75;
  }
  public double get_cuart90()
  {
    return cuart90;
  }
  public double get_cuart10()
  {
    return cuart10;
  }
  public String get_anotemp()
    {
      return anotemp;
    }
  public double get_error()
  {
    return error;
  }


 /** Carga una instancia del objeto de la base de datos dependiendo del
 atributo a�o que este cargado en el objeto  */

  public boolean variablegeog(Connection conn2)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        stmt = conn2.prepareStatement("SELECT * FROM V_REDCAM" );

        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
        depto   = rs.getString(1);
        sector  = rs.getString(2);
        codest  = rs.getString(3);
        nomest  = rs.getString(4);
        fecha   = rs.getString(6);
        sustrato= rs.getString(7);
        temporada   = rs.getString(8);
        profundidad = rs.getString(9);
        tipo    = rs.getString(10);
        cod     = rs.getString(11);
        nombre  = rs.getString(12);
        valor   = rs.getDouble(13);
        unidad  = rs.getString(14);
        lat     = rs.getDouble(18);
        lon     = rs.getDouble(19);
        proyecto     = rs.getString(20);
        ano     = rs.getString(16);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }


 /** Carga una instancia del objeto de la base de datos dependiendo del
 atributo a�o que este cargado en el objeto  */

  public boolean consultas(Connection conn2)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        stmt = conn2.prepareStatement("SELECT * FROM VM_VARIABLEGEOG" );

        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
        depto   = rs.getString(1);
        sector  = rs.getString(2);
        codest  = rs.getString(3);
        nomest  = rs.getString(4);
        fecha   = rs.getString(6);
        sustrato= rs.getString(7);
        temporada   = rs.getString(8);
        profundidad = rs.getString(9);
        tipo    = rs.getString(10);
        cod     = rs.getString(11);
        nombre  = rs.getString(12);
        valor   = rs.getDouble(13);
        unidad  = rs.getString(14);
        lat     = rs.getDouble(18);
        lon     = rs.getDouble(19);
        proyecto     = rs.getString(20);
        ano     = rs.getString(16);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }


}
