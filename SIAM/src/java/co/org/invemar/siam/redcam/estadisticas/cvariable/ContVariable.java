package co.org.invemar.siam.redcam.estadisticas.cvariable;

import co.org.invemar.siam.redcam.coneccam.ConexionCam;
import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

public class ContVariable {

    private String Error;
    private int tamano;
    private Vector resultados;

    public ContVariable() {
    }

    /**
     * carga todas las categorias de la base de datos en un vector de objetos
     */
    public boolean contenedor(Connection conn2, String ntipo) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        Variable var = new Variable();
        String select = new String();
        select = "select * from vvariable where cod is not null";

        if (ntipo != null && !ntipo.equals("")) {
            select = select + " and tipo = '" + ntipo + "'";
        }

        select = select + " order by tipo, var";
        //System.out.println("select:"+select);
        Error = select;
        try {
            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new Variable();
                    var.settipo(rs.getString(1));
                    var.setcod(rs.getString(2));
                    var.setnombre(rs.getString(3));
                    var.setunidad(rs.getString(4));
                    var.setSustrato(rs.getString("sustrato")+"-"+rs.getString("descripcionsustrato"));


                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;
        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * carga todas las areas geograficas de la base de datos
     */
    public boolean contenedorntipo(Connection conn2) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        Variable var = new Variable();
        String select = new String();
        //select = "select DISTINCT INITCAP(tipo) AS Nombretipo, tipo from vvariable order by tipo";
        select = "SELECT DISTINCT INITCAP(tipo) AS Nombretipo, "
                + "  tipo "
                + "FROM "
                + "  (SELECT tv.descripcion_tp AS TIPO, "
                + "    v.codigo_vr             AS COD, "
                + "    v.nombre_vr             AS VAR, "
                + "    um.detalle_um           AS UNIDAD "
                + "  FROM pvariable v, "
                + "    aunidad_variable uv, "
                + "    aunidad_de_medida um, "
                + "    ptipo_variable tv "
                + "  WHERE v.codigo_vr  = uv.codigo_vr_uv "
                + "  AND um.codigo_um   = uv.codigo_um_uv "
                + "  AND tv.codigo_tp   = v.tipo_vr "
                + "  AND v.vigencia_vr  ='S' "
                + "  AND UV.CODIGO_TS_UV='SA' "
                + "  ORDER BY tv.descripcion_tp, "
                + "    v.codigo_vr, "
                + "    v.nombre_vr "
                + "   ) order by tipo" ;
        Error = select;
        try {

            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new Variable();
                    var.settipo(rs.getString("tipo"));
                    var.setnombre(rs.getString("Nombretipo"));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * retorna una instancia del contenedor de Cestacion dada su posicion en el
     * vector
     */
    public Variable getvariable(int i) {
        return ((Variable) resultados.elementAt(i));
    }

    /**
     * retorna el tamano del vector contenedor
     */
    public int gettamano() {
        return tamano;
    }

    /**
     * retorna el error generado por la basse de datos
     */
    public String geterror() {
        return Error;
    }

    public static void main(String[] args) {
        ContVariable contvariable = new ContVariable();
        ConexionCam concam = new ConexionCam();
        contvariable.contenedorntipo(concam.getConn());
        System.out.println(contvariable.getvariable(0).get_tipo());
        System.out.println(contvariable.getvariable(1).get_tipo());
        System.out.println(contvariable.getvariable(2).get_tipo());
        System.out.println(contvariable.getvariable(3).get_tipo());

    }
}
