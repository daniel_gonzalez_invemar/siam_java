/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam;

/**
 *
 * @author usrsig15
 */
public class NivelesEstadisticos {
    private String cod;
    private String descripcion;
    private String atributos_minimos;
    private int posicionVectorParaObtenerNivelEstadistico;

    public int getPosicionVectorParaObtenerNivelEstadistico() {
        return posicionVectorParaObtenerNivelEstadistico;
    }

    public void setPosicionVectorParaObtenerNivelEstadistico(int posicionVectorParaObtenerNivelEstadistico) {
        this.posicionVectorParaObtenerNivelEstadistico = posicionVectorParaObtenerNivelEstadistico;
    }

    public String getAtributos_minimos() {
        return atributos_minimos;
    }

    public void setAtributos_minimos(String atributos_minimos) {
        this.atributos_minimos = atributos_minimos;
    }
    

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
