/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.siam.redcam;

import co.org.invemar.siam.ConnectionFactory;
import co.org.invemar.siam.redcam.coneccam.ConexionCam;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class ServiceZones {
    
      public String getZonesOfSector(String sector){
       Connection con = null;
       ConexionCam conexioncam = null;          
       JSONArray jsonarray = new JSONArray();
       
        
        String sql = "select distinct codigo,nombre from (select codigo, nombre from aestacion  e inner join zonas z on e.id_zona = z.codigo and substr(codigo_st,1,6)=?  order by codigo_st) order by nombre";
        try {
            ConnectionFactory cf = new ConnectionFactory();
            con = cf.createConnection("redcam");          
            PreparedStatement pstmt = null;
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, sector);
            //System.out.println("sql:"+sql);
            ResultSet rs = pstmt.executeQuery();
           
            while (rs.next()) {
                JSONObject jsonobject = new JSONObject();
                jsonobject.put("codigo", rs.getString("codigo"));
                jsonobject.put("nombre", rs.getString("nombre"));
                jsonarray.put(jsonobject);                
            }

        } catch (Exception ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, "Error getting the zones by sector:", ex.getMessage());            
        } finally {
            CerrarConexion(con);

        }
        return jsonarray.toString(); 
    }
      private void CerrarConexion(Connection con) {
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEstaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
