/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.redcam.controller.action;

import co.org.invemar.library.siam.exportadores.ExportXLS;
import co.org.invemar.library.siam.exportadores.Rows;
import co.org.invemar.siam.redcam.ServicioEstadistica;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usrsig15
 */
@WebServlet("/getAllDataRedCamSectorAndStation")
public class getAllDatosRedCamStation extends HttpServlet {
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

       String data="";
       String department = request.getParameter("department");
       String year = request.getParameter("year");
        try {    
              ServicioEstadistica se = new ServicioEstadistica();
              
              data= se.getAllDataRedCamStationView(department,year);            
              

        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }finally{
            
        }
       
        response.setContentType("text/json charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");        
        PrintWriter pw = response.getWriter(); 
        pw.write(data);
	pw.flush();
	pw.close();
        
       

    }
}
