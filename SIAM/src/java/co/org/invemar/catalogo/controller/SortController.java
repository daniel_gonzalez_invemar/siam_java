package co.org.invemar.catalogo.controller;

import co.org.invemar.catalogo.model.Catalogo;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

public class SortController extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    List sortResultados=new ArrayList();
    static int sw=0;
    
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }

    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        HttpSession session=request.getSession();
        sortResultados=(ArrayList)session.getAttribute("LISTOBJECT");
        String sortColumn=request.getParameter("sortColumn");
     
        boolean ascending;
        if(sw==0){
            ascending=true;
            sw=1;
        }else{
            ascending=false;
            sw=0;
        }
        
        sort(sortColumn,ascending);
        
        session.setAttribute("LISTOBJECT",sortResultados);
        
        String pagina=request.getParameter("pagina");
      
        
        String irA="catalogo.jsp?pagina="+pagina;
        
        RequestDispatcher rd = request.getRequestDispatcher(irA);
                        rd.forward(request, response);
        
        
    }

    public void doPost(HttpServletRequest request, 
                       HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        doGet(request,response);
    }
    
   private void sort(final String column, final boolean ascending)
   {
      Comparator comparator = new Comparator()
      {
                                public int compare(Object o1, Object o2)
                                {
                                        Catalogo c1 = (Catalogo) o1;
                                        Catalogo c2 = (Catalogo) o2;
                                        if (column == null)
                                        {
                                                return 0;
                                        }
                                        if (column.equals("ucambio"))
                                        {		
                                                return ascending ?convertStringDate(c1.getUltimoCambio()).compareTo(convertStringDate(c2.getUltimoCambio())) : convertStringDate(c2.getUltimoCambio())
                                                                                .compareTo(convertStringDate(c1.getUltimoCambio()));
                                        }else if (column.equals("coleccion"))
                                        {
                                                return ascending ? c1.getColeccion().compareTo(c2.getColeccion()) : c2.getColeccion()
                                                                                .compareTo(c1.getColeccion());
                                        } 
                                        else if (column.equals("codColeccion"))
                                        {
                                                 return ascending ? c1.getCodColeccion().compareTo(c2.getCodColeccion()) : c2.getCodColeccion()
                                                                                               .compareTo(c1.getCodColeccion());
                                        } 
                                        else if (column.equals("nroCatalogo"))
                                        {
                                                 return ascending ? replaceStringToInteger("-",c1.getNumCatalogo()).compareTo(replaceStringToInteger("-",c2.getNumCatalogo())) : replaceStringToInteger("-",c2.getNumCatalogo())
                                                                                           .compareTo(replaceStringToInteger("-",c1.getNumCatalogo()));
                                                                                        
                                        } 
                                        else if (column.equals("genero"))
                                        {
                                                 return ascending ? c1.getGenero().compareTo(c2.getGenero()) : c2.getGenero()
                                                                                       .compareTo(c1.getGenero());
                                        } 
                                        else if (column.equals("bRegistro"))
                                        {
                                                 return ascending ? c1.getBaseRegistro().compareTo(c2.getBaseRegistro()) : c2.getBaseRegistro()
                                                                                                                .compareTo(c1.getBaseRegistro());
                                       } 
                                        else if (column.equals("reino"))
                                        {
                                                return ascending ? c1.getReino().compareTo(c2.getReino()) : c2.getReino()
                                                                                                              .compareTo(c1.getReino());
                                        }
                                        else if (column.equals("division"))
                                        {
                                                 return ascending ? c1.getDivision().compareTo(c2.getDivision()) : c2.getDivision()
                                                                                                              .compareTo(c1.getDivision());
                                        }
                                        else if (column.equals("clase"))
                                        {
                                                 return ascending ? c1.getClase().compareTo(c2.getClase()) : c2.getClase()
                                                                                                          .compareTo(c1.getClase());
                                        }
                                        else if (column.equals("orden"))
                                        {
                                                 return ascending ? c1.getOrden().compareTo(c2.getOrden()) : c2.getOrden()
                                                                                                           .compareTo(c1.getOrden());
                                        }
                                        else if (column.equals("familia"))
                                        {
                                                 return ascending ? c1.getFamilia().compareTo(c2.getFamilia()) : c2.getFamilia()
                                                                                                           .compareTo(c1.getFamilia());
                                        }
                                        else if (column.equals("especie"))
                                        {
                                                return ascending ? c1.getEspecie().compareToIgnoreCase(c2.getEspecie()) : c2.getEspecie()
                                                                                                     .compareToIgnoreCase(c1.getEspecie());
                                        }
                                        else if (column.equals("epiteto"))
                                        {
                                                return ascending ? c1.getEpiteto().compareTo(c2.getEpiteto()) : c2.getEpiteto()
                                                                                .compareTo(c1.getEpiteto());
                                        }
                                        else if (column.equals("autorSpecie"))
                                        {
                                                return ascending ? c1.getAutorEspecie().compareTo(c2.getAutorEspecie()) : c2.getAutorEspecie()
                                                                                                .compareTo(c1.getAutorEspecie());
                                        }
                                        else if (column.equals("identPor"))
                                        {
                                                return ascending ? c1.getIdentificadoPor().compareTo(c2.getIdentificadoPor()) : c2.getIdentificadoPor()
                                                                                                      .compareTo(c1.getIdentificadoPor());
                                        }
                                        else if (column.equals("anoIdent"))
                                        {
                                               return ascending ? c1.getAnoIdentificacion().compareTo(c2.getAnoIdentificacion()) : c2.getAnoIdentificacion()
                                                                                                        .compareTo(c1.getAnoIdentificacion());
                                        }
                                        else if (column.equals("tipo"))
                                        {
                                                return ascending ? c1.getTipo().compareTo(c2.getTipo()) : c2.getTipo()
                                                                                                    .compareTo(c1.getTipo());
                                        }
                                        else if (column.equals("colectadoPor"))
                                        {
                                                return ascending ? c1.getColectadoPor().compareTo(c2.getColectadoPor()) : c2.getColectadoPor()
                                                                                                .compareTo(c1.getColectadoPor());
                                        }
                                        else if (column.equals("anoCaptura"))
                                        {
                                                return ascending ? c1.getAnoCaptura().compareTo(c2.getAnoCaptura()) : c2.getAnoCaptura()
                                                                                                .compareTo(c1.getAnoCaptura());
                                        }
                                        else if (column.equals("cAgua"))
                                        {
                                                return ascending ? c1.getContinentOcean().compareTo(c2.getContinentOcean()) : c2.getContinentOcean()
                                                                                                .compareTo(c1.getContinentOcean());
                                        }
                                        else if (column.equals("pais"))
                                        {
                                                return ascending ? c1.getPais().compareTo(c2.getPais()) : c2.getPais()
                                                                                                .compareTo(c1.getPais());
                                        }
                                        else if (column.equals("dpto"))
                                        {
                                                return ascending ? c1.getDepartamento().compareTo(c2.getDepartamento()) : c2.getDepartamento()
                                                                                                .compareTo(c1.getDepartamento());
                                        }
                                        else if (column.equals("localidad"))
                                        {
                                                return ascending ? c1.getLocalidad().compareTo(c2.getLocalidad()) : c2.getLocalidad()
                                                                                                .compareTo(c1.getLocalidad());
                                        }
                                        else if (column.equals("longitud"))
                                        {
                                                return ascending ? c1.getLongitud().compareTo(c2.getLongitud()) : c2.getLongitud()
                                                                                                .compareTo(c1.getLongitud());
                                        }
                                        else if (column.equals("latitud"))
                                        {
                                                return ascending ? c1.getLatitud().compareTo(c2.getLatitud()) : c2.getLatitud()
                                                                                                .compareTo(c1.getLatitud());
                                        }
                                        else if (column.equals("profCaptura"))
                                        {
                                                return ascending ? c1.getProfCaptura().compareTo(c2.getProfCaptura()) : c2.getProfCaptura()
                                                                                .compareTo(c1.getProfCaptura());
                                        }
                                        else if (column.equals("ejemplares"))
                                        {
                                                return ascending ? c1.getEjemplares().compareTo(c2.getEjemplares()) : c2.getEjemplares()
                                                                                .compareTo(c1.getEjemplares());
                                        }
                                        else 
                                                return 0;
                                               
                                }
                        };
      Collections.sort(sortResultados, comparator);
   }
    
    private GregorianCalendar convertStringDate(String date){
       
        if(null==date || date.equals("")||date.equals("-")){
       
            date="0000-00-00";
        }
                
        int inicio=date.indexOf("-");
        int year = Integer.parseInt(date.substring(0, inicio));
       
        
        String  mesyano=date.substring(inicio+1,date.length());
        int seg=mesyano.indexOf("-");
        
        int month = Integer.parseInt(date.substring(inicio+1, seg+inicio+1));
       
        int day = Integer.parseInt(date.substring(seg+inicio+2, date.length()));
       
          
           
       GregorianCalendar dateRet = new GregorianCalendar();
       dateRet.set(Calendar.DATE, day);
       dateRet.set(Calendar.MONTH, month);
       dateRet.set(Calendar.YEAR, year);
       
       return dateRet;
       
       
    }
    
    private Integer replaceStringToInteger(String oldChar, String word){
        
        if(word.equals("")){
            return new Integer(0);
        
        }
        String newWord=word.substring(word.indexOf(oldChar)+1);
        
        Integer num=new Integer(0);
        try{
            num=new Integer(newWord);
        
        }catch(Exception e){
            num=new Integer(0);
        }
        return num;
    
    }
    
}
