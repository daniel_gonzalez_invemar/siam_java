package co.org.invemar.catalogo.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginService extends HttpServlet {

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		
		String nextPage="/sibm/estadisticas/index.jsp";
		HttpSession session=request.getSession();
		
		if(username!=null && password!=null){
			if(username.equalsIgnoreCase("admin") && password.equalsIgnoreCase("naufragos")){
				nextPage="/sibm/estadisticas/index2.jsp";
				session.setAttribute("USER", "ADMIN");
			}else {
				nextPage="/sibm/estadisticas/index.jsp?login=NOTUser";
			}
		}else {
			nextPage="/sibm/estadisticas/index.jsp?login=NOTUser";
		}
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(nextPage);
		rd.forward(request, response);
	}

}
