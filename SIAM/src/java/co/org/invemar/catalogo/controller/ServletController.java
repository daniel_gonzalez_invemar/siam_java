package co.org.invemar.catalogo.controller;

import co.org.invemar.catalogo.model.DAOFactory;

import co.org.invemar.catalogo.model.SearchCatalogoDAO;

import java.io.IOException;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

public class ServletController extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=utf8";
   

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        request.setAttribute("INICIO","busqueda");
        String criteriaOfSearch=null;
        String valueOfSearch=null;
        
        DAOFactory df;
        Connection  conn;
        SearchCatalogoDAO search;
        
        criteriaOfSearch=request.getParameter("criterioBusqueda");
        valueOfSearch=request.getParameter("valorBusqueda");
        df = new DAOFactory();
        String irA="error.jsp";
        List resultados=null;
        //System.out.println("CRITERIO DE BUSQUEDA::... "+criteriaOfSearch);
        
        HttpSession session=request.getSession();
        session.removeAttribute("LISTOBJECT");
        session.invalidate();
        
        session=request.getSession();
        String inicio=request.getParameter("inicio");
        
        if(inicio!=null){
             session.removeAttribute("LISTOBJECT");
             session.invalidate();
             RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
                    rd.forward(request, response);        
                }

        try {
            conn = df.createConnection();
            //System.out.println("conexion");
            if((criteriaOfSearch!=null && criteriaOfSearch.equals("orden")) && null!=valueOfSearch )
            {
                search=df.getSearchCatalogoDAO();
                resultados=search.findByOrden(conn,valueOfSearch);
                
                irA="catalogo.jsp";
            }else if (criteriaOfSearch != null && criteriaOfSearch.equals("familia") && null!=valueOfSearch) {
                        search=df.getSearchCatalogoDAO();
                        resultados=search.findByFamilia(conn,valueOfSearch);
                        irA="catalogo.jsp";
                
            }else if (criteriaOfSearch!=null && criteriaOfSearch.equals("genero") && null!=valueOfSearch){
                        search=df.getSearchCatalogoDAO();
                        resultados= search.findByGenus(conn,valueOfSearch); 
                        session.setAttribute("CRITERIO",valueOfSearch);
                        irA="catalogo.jsp";
            }else if (criteriaOfSearch!=null && criteriaOfSearch.equals("especie") && null!=valueOfSearch){
                        search=df.getSearchCatalogoDAO();
                        resultados=search.findBySpecies(conn,valueOfSearch);            
                
                        irA="catalogo.jsp";
            }else {
                System.out.println("Ultimo else");
                irA="error.jsp";
            }
            
            session.setAttribute("LISTOBJECT",resultados);
            
        } catch (SQLException e) {
            System.out.println("OBtenida la conexion parece que no funciona "+e);
        }
        
        RequestDispatcher rd = request.getRequestDispatcher(irA);
                        rd.forward(request, response);

        
    }

    public void doPost(HttpServletRequest request, 
                       HttpServletResponse response) throws ServletException, IOException {
                       
        doGet(request,response);
    }
}
