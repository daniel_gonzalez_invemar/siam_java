package co.org.invemar.catalogo.controller;

import co.org.invemar.catalogo.model.Archivo;
import co.org.invemar.catalogo.model.Catalogo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class DownloadFileServlet extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private javax.servlet.ServletContext servletContext; 
    public void init(ServletConfig config) throws ServletException {
         servletContext=config.getServletContext();
        super.init(config);
    }

    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        //obtenemos el objeto que se encuentra en la session con os datos
        HttpSession session=request.getSession();
        List<Catalogo> datos=(List)session.getAttribute("LISTOBJECT");
        
        String ext=request.getParameter("ext");
        
        String context = request.getContextPath();
        //obtenemos la ruta real del directorio temp dentro de la aplicacion
        String path =  servletContext.getRealPath("temp");
        //System.out.println("PATH COMPLETO::.. "+path);
        //System.out.println("CONETXT: "+context);
        //Instanciamos un objeto de la clase Archivo
      /*  Archivo archivo=new Archivo();
        //Creamos un archivo con el contenido seleccionado y retorna el nombre del archivo creado
        String nombreArchivo=archivo.archivar(path+File.separator,ext,datos);
        System.out.println("Nombre de Archivo: "+nombreArchivo);
        
        String pathdelarchivo=path+File.separator+nombreArchivo;
       // System.out.println("PATH::.. "+pathdelarchivo);
        
        //Creamos un stream del archivo
        FileInputStream ar = new FileInputStream(pathdelarchivo);
        
        //comvertimos el archivo en un array de bytes para enviarlo en el response
        int longitud = ar.available();
        byte data[];
        data= new byte[longitud];
        ar.read(data);
        ar.close();
        
        //header necesarios para la descarga del archivo
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition","attachment; filename=\""+nombreArchivo+"\"");
        response.setHeader("Pragma","no-cache");
        
        //eliminar el archivo despues de enviar 
        File f=new File(path+File.separator+nombreArchivo);
        if (f.delete()){
           System.out.println("Archivo borrado");
        }else{
            System.out.println("No se pudo eliminar el archivos");
        }
        
        //escribimos el archivo en un flujo de bytes en el response
        ServletOutputStream outs=response.getOutputStream();
        outs.write(data);
        outs.flush();
        outs.close();
        
        */
        
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        
        ResourceBundle bundle=ResourceBundle.getBundle("catalogoexcel");
        String[] columns=bundle.getString("columnas").split(",");
        
    	HSSFWorkbook wb = new HSSFWorkbook();
    	HSSFSheet sheet =null;
    	HSSFRow row = null;
    	sheet = wb.createSheet(bundle.getString("sheet1"));
    	row = sheet.createRow((short) 0);
    	createCell(bundle.getString("citese.linea1"), wb, row, (short) 0);
    	row = sheet.createRow((short) 1);
    	createCell(bundle.getString("citese.linea2").replace("@@@",formateador.format(ahora) ), wb, row, (short) 0);
    	row = sheet.createRow((short) 2);
    	createCell(bundle.getString("citese.linea3"), wb, row, (short) 0);
    	row = sheet.createRow((short) 3);
    	createCell(bundle.getString("citese.linea4"), wb, row, (short) 0);
    	row = sheet.createRow((short) 4);
    	createCell(bundle.getString("citese.linea5"), wb, row, (short) 0);
    	row = sheet.createRow((short) 5);
    	createCell(bundle.getString("citese.linea6"), wb, row, (short) 0);
    	
    	
    	sheet = wb.createSheet(bundle.getString("sheet2"));
		
		int i = 0;
		int j = 0;
		
		
		row = sheet.createRow((short) i++);
                //System.out.println("Columns length:"+columns.length);
		for(j=0;j<columns.length;j++){
			createCell(columns[j], wb, row, (short) j);
			                // System.out.println("columna j:"+columns[j]);
			
		}
		
		//System.out.println("catalogo: "+datos.size());		
		for (Catalogo catalogo:datos){
			row = sheet.createRow((short) i++);
			j = 0;
			/*createCell(catalogo.getBaseRegistro(), wb, row, (short) j++);
			createCell(catalogo.getReino(), wb, row, (short) j++);
			createCell(catalogo.getDivision(), wb, row, (short) j++);
			createCell(catalogo.getClase(), wb, row, (short) j++);
			createCell(catalogo.getOrden(), wb, row, (short) j++);
			createCell(catalogo.getFamilia(), wb, row, (short) j++);
			createCell(catalogo.getGenero(), wb, row, (short) j++);
			createCell(catalogo.getEpiteto(), wb, row, (short) j++);
			createCell(catalogo.getEspecie(), wb, row, (short) j++);
			createCell(catalogo.getAutorEspecie(), wb, row, (short) j++);
			createCell(catalogo.getVariedad(), wb, row, (short) j++);
			createCell(catalogo.getAutorVariedad(), wb, row, (short) j++);
			createCell(catalogo.getNombreComun(), wb, row, (short) j++);
			createCell(catalogo.getColeccion(), wb, row, (short) j++);
			createCell(catalogo.getCodColeccion(), wb, row, (short) j++);
			createCell(catalogo.getNumCatalogo(), wb, row, (short) j++);
			createCell(catalogo.getConsecutivoMuestra(), wb, row, (short) j++);
			createCell(catalogo.getColectadoPor(), wb, row, (short) j++);
			createCell(catalogo.getBarco(), wb, row, (short) j++);
			createCell(catalogo.getProyecto(), wb, row, (short) j++);
			createCell(catalogo.getFechaCaptura(), wb, row, (short) j++);
			createCell(catalogo.getEjemplares(), wb, row, (short) j++);
			createCell(catalogo.getMetodoCaptura(), wb, row, (short) j++);
			createCell(catalogo.getIdentificadoPor(), wb, row, (short) j++);
			createCell(catalogo.getFechaIdentificacion(), wb, row, (short) j++);
			createCell(catalogo.getPais(), wb, row, (short) j++);
			createCell(catalogo.getContinentOcean(), wb, row, (short) j++);
			createCell(catalogo.getDepartamento(), wb, row, (short) j++);
			createCell(catalogo.getMunicipio(), wb, row, (short) j++);
			createCell(catalogo.getLocalidad(), wb, row, (short) j++);
			createCell(catalogo.getLongitud(), wb, row, (short) j++);
			createCell(catalogo.getLatitud(), wb, row, (short) j++);
			createCell(catalogo.getLatitudInicial(), wb, row, (short) j++);
			createCell(catalogo.getLatitudFinal(), wb, row, (short) j++);
			createCell(catalogo.getLongitudInicial(), wb, row, (short) j++);
			createCell(catalogo.getLongitudFinal(), wb, row, (short) j++);
			createCell(catalogo.getProfCaptura(), wb, row, (short) j++);
			createCell(catalogo.getProfAgua(), wb, row, (short) j++);
			createCell(catalogo.getProfundidadMin(), wb, row, (short) j++);
			createCell(catalogo.getProfundidadMax(), wb, row, (short) j++);
			createCell(catalogo.getProfundidadExacta(), wb, row, (short) j++);
			createCell(catalogo.getZona(), wb, row, (short) j++);
			createCell(catalogo.getEcorregion(), wb, row, (short) j++);
			createCell(catalogo.getAmbiente(), wb, row, (short) j++);
			createCell(catalogo.getTemperaturaAire(), wb, row, (short) j++);
			createCell(catalogo.getTemperaturaAgua(), wb, row, (short) j++);
			createCell(catalogo.getSalinidad(), wb, row, (short) j++);
			createCell(catalogo.getHoraCon(), wb, row, (short) j++);
			createCell(catalogo.getCuerpoAgua(), wb, row, (short) j++);
			createCell(catalogo.getTipoCostaCon(), wb, row, (short) j++);
			createCell(catalogo.getMareasCon(), wb, row, (short) j++);
			createCell(catalogo.getCorrienteCon(), wb, row, (short) j++);
			createCell(catalogo.getTranspCon(), wb, row, (short) j++);
			createCell(catalogo.getTiempo(), wb, row, (short) j++);
			createCell(catalogo.getUicnGlobal(), wb, row, (short) j++);
			createCell(catalogo.getUicnNacional(), wb, row, (short) j++);
			createCell(catalogo.getCites(), wb, row, (short) j++);
			createCell(catalogo.getNotasMuestra(), wb, row, (short) j++);
			createCell(catalogo.getNotasCaptura(), wb, row, (short) j++);
			createCell(catalogo.getNotasEstacion(), wb, row, (short) j++);
			createCell(catalogo.getNotasCrucero(), wb, row, (short) j++);
			createCell(catalogo.getNotasCon(), wb, row, (short) j++);
			createCell(catalogo.getFechaRecibido(), wb, row, (short) j++);
			createCell(catalogo.getFechaInicial(), wb, row, (short) j++);
			createCell(catalogo.getUbicacionGeo(), wb, row, (short) j++);
			createCell(catalogo.getTipo(), wb, row, (short) j++);
			createCell(catalogo.getTipoEspecimen(), wb, row, (short) j++);
			createCell(catalogo.getPreservativo(), wb, row, (short) j++);
			createCell(catalogo.getAdquisicion(), wb, row, (short) j++);
			createCell(catalogo.getProcedencia(), wb, row, (short) j++);
			createCell(catalogo.getTevPreservado(), wb, row, (short) j++);
			createCell(catalogo.getTevEjemplares(), wb, row, (short) j++);
			createCell(catalogo.getClave(), wb, row, (short) j++);
			createCell(catalogo.getEcorregionCDG(), wb, row, (short) j++);
			createCell(catalogo.getPublicado(), wb, row, (short) j++);
			createCell(catalogo.getUltimoCambio(), wb, row, (short) j++);
			createCell(catalogo.getColectadoPorCDG(), wb, row, (short) j++);
			createCell(catalogo.getRegion(), wb, row, (short) j++);
			*/
			//createCell(catalogo.getMetadato()/*!=null && catalogo.getMetadato()!=""?bundle.getString("url.metadato").replace("@@@",catalogo.getMetadato()):catalogo.getMetadato()*/, wb, row, (short) j++);
			/*createCell(catalogo.getNivel(), wb, row, (short) j++);
			*/
			//////
			createCell(catalogo.getNroInvemar(), wb, row, (short) j++);
                        createCell(catalogo.getClave(), wb, row, (short) j++);
                        createCell(catalogo.getObjetos(), wb, row, (short) j++);
                        createCell(catalogo.getOlas(), wb, row, (short) j++);                        
                        createCell(catalogo.getCAMP(), wb, row, (short) j++);   
                        createCell(catalogo.getQualityflag(), wb, row, (short) j++);                             
                        createCell(catalogo.getTecnica_ident(),wb, row, (short) j++);                              
			createCell(catalogo.getUltimoCambio(), wb, row, (short) j++);
			createCell(catalogo.getColeccion(), wb, row, (short) j++);
			createCell(catalogo.getCodColeccion(), wb, row, (short) j++);
			createCell(catalogo.getNumCatalogo(), wb, row, (short) j++);
			createCell(catalogo.getProyecto(), wb, row, (short) j++);
			createCell(catalogo.getEstacion(), wb, row, (short) j++);
			createCell(catalogo.getEspecie(), wb, row, (short) j++);
			createCell(catalogo.getBaseRegistro(), wb, row, (short) j++);
			createCell(catalogo.getReino(), wb, row, (short) j++);
			createCell(catalogo.getDivision(), wb, row, (short) j++);
			createCell(catalogo.getClase(), wb, row, (short) j++);
			createCell(catalogo.getOrden(), wb, row, (short) j++);
			createCell(catalogo.getFamilia(), wb, row, (short) j++);
			createCell(catalogo.getGenero(), wb, row, (short) j++);
			createCell(catalogo.getEpiteto(), wb, row, (short) j++);
			createCell(catalogo.getAutorEspecie(), wb, row, (short) j++);
			createCell(catalogo.getIdentificadoPor(), wb, row, (short) j++);
			createCell(catalogo.getAnoIdentificacion(), wb, row, (short) j++);
			createCell(catalogo.getTipo(), wb, row, (short) j++);
			createCell(catalogo.getColectadoPor(), wb, row, (short) j++);
			createCell(catalogo.getAnoCaptura(), wb, row, (short) j++);
			createCell(catalogo.getContinentOcean(), wb, row, (short) j++);
			createCell(catalogo.getPais(), wb, row, (short) j++);
			createCell(catalogo.getDepartamento(), wb, row, (short) j++);
			
                        createCell(catalogo.getLocalidad(), wb, row, (short) j++);                            
			createCell(catalogo.getLatitud(), wb, row, (short) j++);                       
			createCell(catalogo.getLongitud(), wb, row, (short) j++);
                        createCell(catalogo.getLatfindecimal(), wb, row, (short) j++);
                        createCell(catalogo.getLongfindecimal(), wb, row, (short) j++);
                        
                        createCell(catalogo.getLatitudInicial(), wb, row, (short) j++);
			createCell(catalogo.getLatitudFinal(), wb, row, (short) j++);
			createCell(catalogo.getLongitudInicial(), wb, row, (short) j++);
			createCell(catalogo.getLongitudFinal(), wb, row, (short) j++);                       
                       
			createCell(catalogo.getProfCaptura(), wb, row, (short) j++);
                        createCell(catalogo.getProfAgua(), wb, row, (short) j++);
                        
			createCell(catalogo.getEjemplares(), wb, row, (short) j++);
			createCell(catalogo.getProyecto(), wb, row, (short) j++);
			createCell(catalogo.getMetadato(), wb, row, (short) j++);
			createCell(catalogo.getUbicacionGeo(), wb, row, (short) j++);
			createCell(catalogo.getAutor(), wb, row, (short) j++);
			createCell(catalogo.getNivel(), wb, row, (short) j++);
			createCell(catalogo.getNombreComun(), wb, row, (short) j++);
			createCell(catalogo.getVariedad(), wb, row, (short) j++);
			createCell(catalogo.getVariedad(), wb, row, (short) j++);
			createCell(catalogo.getFechaRecibido(), wb, row, (short) j++);
			createCell(catalogo.getTipoEspecimen(), wb, row, (short) j++);
			createCell(catalogo.getPreservativo(), wb, row, (short) j++);
			createCell(catalogo.getAdquisicion(), wb, row, (short) j++);
			createCell(catalogo.getProcedencia(), wb, row, (short) j++);
			createCell(catalogo.getColectadoPorCDG(), wb, row, (short) j++);
			createCell(catalogo.getFechaCaptura(), wb, row, (short) j++);
                        createCell(catalogo.getFechaRecibido(), wb, row, (short) j++);
			createCell(catalogo.getMetodoCaptura(), wb, row, (short) j++);
			createCell(catalogo.getMunicipio(), wb, row, (short) j++);
			createCell(catalogo.getCuerpoAgua(), wb, row, (short) j++);			
			createCell(catalogo.getBarco(), wb, row, (short) j++);
			
			createCell(catalogo.getAmbiente(), wb, row, (short) j++);
			createCell(catalogo.getTemperaturaAire(), wb, row, (short) j++);
			createCell(catalogo.getTemperaturaAgua(), wb, row, (short) j++);
			createCell(catalogo.getSalinidad(), wb, row, (short) j++);
			createCell(catalogo.getPublicado(), wb, row, (short) j++);
			createCell(catalogo.getFechaIdentificacion(), wb, row, (short) j++);
			createCell(catalogo.getFechaActualizo(), wb, row, (short) j++);
			createCell(catalogo.getFechaInicial(), wb, row, (short) j++);
			createCell(catalogo.getProfundidadMin(), wb, row, (short) j++);
			createCell(catalogo.getProfundidadMax(), wb, row, (short) j++);
			createCell(catalogo.getProfundidadExacta(), wb, row, (short) j++);
			createCell(catalogo.getEcorregion(), wb, row, (short) j++);
			createCell(catalogo.getEcorregionCDG(), wb, row, (short) j++);
			createCell(catalogo.getRegion(), wb, row, (short) j++);
			createCell(catalogo.getTevPreservado(), wb, row, (short) j++);
			createCell(catalogo.getTevEjemplares(), wb, row, (short) j++);
			createCell(catalogo.getZona(), wb, row, (short) j++);
			createCell(catalogo.getUicnGlobal(), wb, row, (short) j++);
			createCell(catalogo.getUicnNacional(), wb, row, (short) j++);
			createCell(catalogo.getCites(), wb, row, (short) j++);
			createCell(catalogo.getNotasMuestra(), wb, row, (short) j++);
			createCell(catalogo.getNotasCaptura(), wb, row, (short) j++);
			createCell(catalogo.getNotasIdentificacion(), wb, row, (short) j++);
			createCell(catalogo.getNotasEstacion(), wb, row, (short) j++);
			createCell(catalogo.getNotasCrucero(), wb, row, (short) j++);
			createCell(catalogo.getConsecutivoMuestra(), wb, row, (short) j++);
			createCell(catalogo.getTipoCostaCon(), wb, row, (short) j++);
			createCell(catalogo.getOlas(), wb, row, (short) j++);
			createCell(catalogo.getMareasCon(), wb, row, (short) j++);
			createCell(catalogo.getCorrienteCon(), wb, row, (short) j++);
			createCell(catalogo.getTranspCon(), wb, row, (short) j++);
			createCell(catalogo.getTiempo(), wb, row, (short) j++);
			createCell(catalogo.getNotasCon(), wb, row, (short) j++);
			createCell(catalogo.getHoraCon(), wb, row, (short) j++);
                        createCell(catalogo.getUbicacion(), wb, row, (short) j++);
                      
			
			
			
			
		}
		String fileName="Catalogo";
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\""
				+ fileName +".xls"+ "\"");
		response.setHeader("Pragma", "no-cache");

		ServletOutputStream outs = response.getOutputStream();
		wb.write(outs);
		outs.flush();
		outs.close();
		
		
		
        
    }

    public void doPost(HttpServletRequest request, 
                       HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        doGet(request,response);
    }
    
    /**
	 * Creates a cell and aligns it a certain way.
	 * 
	 * @param wb
	 *            the workbook
	 * @param row
	 *            the row to create the cell in
	 * @param column
	 *            the column number to create the cell in
	 * @param align
	 *            the alignment for the cell.
	 */
	private static void createCell(String value, HSSFWorkbook wb, HSSFRow row,
			short column) {
		HSSFCell cell = row.createCell(column);
		cell.setCellValue(new HSSFRichTextString(value));
		
		/*
		 * HSSFCellStyle cellStyle = wb.createCellStyle();
		 * cellStyle.setAlignment(align); cell.setCellStyle(cellStyle);
		 */
	}

	/**
	 * Creates a cell and aligns it a certain way.
	 * 
	 * @param wb
	 *            the workbook
	 * @param row
	 *            the row to create the cell in
	 * @param column
	 *            the column number to create the cell in
	 * @param align
	 *            the alignment for the cell.
	 */
	private static void createCell(int value, HSSFWorkbook wb, HSSFRow row,
			short column/* , short align */) {
		HSSFCell cell = row.createCell(column);
		cell.setCellValue(value);
		/*
		 * HSSFCellStyle cellStyle = wb.createCellStyle();
		 * cellStyle.setAlignment(align); cell.setCellStyle(cellStyle);
		 */
	}

	private String isNull(String str) {
		String cadena = "";
		if (str != null) {
			cadena = str;
		}
		return cadena;
	}

	public boolean isEmpty(String str) {
		return str == null || str.length() == 0 || str.equals("null");
	}
}
