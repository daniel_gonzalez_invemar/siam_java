package co.org.invemar.catalogo.controller;
import co.org.invemar.catalogo.model.Catalogo;
import co.org.invemar.catalogo.model.DAOFactory;
import co.org.invemar.catalogo.model.SearchCatalogoDAO;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

public class FilterController extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        
        
        
        HttpSession session=request.getSession();
        
       
        String ultimoCambio=request.getParameter("ucambio");
        String coleccion=request.getParameter("coleccion");
        String codigoColeccion=request.getParameter("ccoleccion");
        String nroCatalogo=request.getParameter("ncatalogo");
        String baseRegistro=request.getParameter("bregistro");
        String reino=request.getParameter("reino");
        String division=request.getParameter("division");
        String clase=request.getParameter("clase");
        String epiteto=request.getParameter("epiteto");
        String autorSpecie=request.getParameter("aespecie");
        String identificadoPor=request.getParameter("identPor");
        String anoIdentificacion=request.getParameter("anoIdent");
        String tipo=request.getParameter("tipo");
        String colectadoPor=request.getParameter("colectadoPor");
        String anoCaptura=request.getParameter("anocaptura");
        String cuerpoAgua=request.getParameter("nombreagua");
        String pais=request.getParameter("pais");
        String depto=request.getParameter("depto");
        String localidad=request.getParameter("localidad");
        String logitud=request.getParameter("longitud");
        String latitud=request.getParameter("latitud");
        String profCaptura=request.getParameter("profcaptura");
        String ejemplares=request.getParameter("ejemplares");
        
        session.setAttribute("OCAMBIO","TRUE");
        if(ultimoCambio==null){
           
            session.setAttribute("OCAMBIO","FALSE");
          
        }
        session.setAttribute("OCOLECCION","TRUE");
        if(coleccion==null ){
            session.setAttribute("OCOLECCION","FALSE");
            
        }
        
        session.setAttribute("OCCOLECCION","TRUE");
        if(codigoColeccion==null ){
            session.setAttribute("OCCOLECCION","FALSE");
         
        }
        
         session.setAttribute("ONROCATALOGO","TRUE");
        if(nroCatalogo==null){
            session.setAttribute("ONROCATALOGO","FALSE");
         
        }
        
        session.setAttribute("OBASEREGISTRO","TRUE");
        if(baseRegistro==null){
           session.setAttribute("OBASEREGISTRO","FALSE");
        
        }
   
        session.setAttribute("OREINO","TRUE");
        if(reino==null){
           session.setAttribute("OREINO","FALSE");
        
        }
        
        
         session.setAttribute("ODIVISION","TRUE");
         if(division==null){
            session.setAttribute("ODIVISION","FALSE");
         
         }
         
        session.setAttribute("OCLASE","TRUE");
        if(clase==null){
           session.setAttribute("OCLASE","FALSE");
        
        }
        
        session.setAttribute("OEPITETO","TRUE");
        if(epiteto==null){
           session.setAttribute("OEPITETO","FALSE");
        
        }
        
        session.setAttribute("OAESPECIE","TRUE");
        if(autorSpecie==null){
           session.setAttribute("OAESPECIE","FALSE");
        
        }
        
        session.setAttribute("OIDENPOR","TRUE");
        if(identificadoPor==null){
           session.setAttribute("OIDENPOR","FALSE");
        
        }
        
        
        session.setAttribute("OAIDENT","TRUE");
        if(anoIdentificacion==null){
           session.setAttribute("OAIDENT","FALSE");
        
        }
        
        
        session.setAttribute("OTIPO","TRUE");
        if(tipo==null){
           session.setAttribute("OTIPO","FALSE");
        
        }
        
        session.setAttribute("OCOLPOR","TRUE");
        if(colectadoPor==null){
           session.setAttribute("OCOLPOR","FALSE");
        
        }
        
        
        session.setAttribute("OACAPTURA","TRUE");
        if(anoCaptura==null){
           session.setAttribute("OACAPTURA","FALSE");
        
        }
        
        
        session.setAttribute("OCAGUA","TRUE");
        if(cuerpoAgua==null){
           session.setAttribute("OCAGUA","FALSE");
        
        }
        
        
        session.setAttribute("OPAIS","TRUE");
        if(pais==null){
           session.setAttribute("OPAIS","FALSE");
        
        }
        
        
        session.setAttribute("ODEPTO","TRUE");
        if(depto==null){
           session.setAttribute("ODEPTO","FALSE");
        
        }
        
        
        session.setAttribute("OLOCALIDAD","TRUE");
        if(localidad==null){
           session.setAttribute("OLOCALIDAD","FALSE");
        
        }
        
        session.setAttribute("OLONGITUD","TRUE");
        if(logitud==null){
           session.setAttribute("OLONGITUD","FALSE");
        
        }
        
        
        session.setAttribute("OLATITUD","TRUE");
        if(latitud==null){
           session.setAttribute("OLATITUD","FALSE");
        
        }
        
        //////////////
        session.setAttribute("OPCAPTURA","TRUE");
        if(profCaptura==null){
           session.setAttribute("OPCAPTURA","FALSE");
        
        }
        
        
        session.setAttribute("OEJEMPLARES","TRUE");
        if(ejemplares==null){
           session.setAttribute("OEJEMPLARES","FALSE");
        
        }
        
        
        
        RequestDispatcher rd = request.getRequestDispatcher("catalogo.jsp");
                        rd.forward(request, response);

        
        
        
        
    }

    public void doPost(HttpServletRequest request, 
                       HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        doGet(request,response);
    }
    
}