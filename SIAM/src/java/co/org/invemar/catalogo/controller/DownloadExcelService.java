package co.org.invemar.catalogo.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import co.org.invemar.catalogo.model.DAOFactory;
import co.org.invemar.catalogo.model.EstadisticasExcelDAO;
import co.org.invemar.catalogo.model.EstadisticasXMLDAO;
import co.org.invemar.catalogo.model.SearchDAO;
import co.org.invemar.util.ConnectionFactory;

public class DownloadExcelService extends HttpServlet {
	private HSSFWorkbook wb = null;
	private DAOFactory  df;
	private Connection  conn;
	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		df = new DAOFactory();
       
		String action=request.getParameter("action");
		String fileName="";
        EstadisticasExcelDAO excelDao;
        
        String codigo=request.getParameter("valor");
        String coleccion =request.getParameter("coleccion");
        
        
       
        
        coleccion=coleccion.equals("")?null:coleccion;
        
        if (action.equalsIgnoreCase("doEstadNivelPhylumBasic")){
	        
        	String phylum=request.getParameter("phylum");
        	String proyecto=request.getParameter("proyecto");
            String nivel54=request.getParameter("nivel54");
            fileName=phylum;
        	proyecto=proyecto.equals("")?null:proyecto;
	        nivel54=nivel54.equals("false")?null:nivel54;
	        
			 try {
	             conn = df.createConnection();
	             excelDao=df.getEstadisticasExcelDAO();
	             
	             wb=excelDao.findLotesByGruposTaxEXCEL(conn, codigo, coleccion, proyecto,nivel54);
	             
	         } catch (SQLException e) {
	             System.out.println("Fallida la conexion no funciona "+e);
	         }finally {
	        	 if(conn!=null){
					ConnectionFactory.closeConnection(conn);
	        	 }
					conn=null;
			}
         
        }
        
        
        if (action.equalsIgnoreCase("doEstadInvestigadores")){
	        
        	
        	 	        
			 try {
	             conn = df.createConnection();
	             excelDao=df.getEstadisticasExcelDAO();
	             
	             wb=excelDao.findLotesInvestigadorBySeccionEXCEL(conn, codigo, coleccion);
	             
	         } catch (SQLException e) {
	             System.out.println("Fallida la conexion no funciona "+e);
	         }finally {
	        	 if(conn!=null){
					ConnectionFactory.closeConnection(conn);
	        	 }
					
			}
	         
	         codigo=StringUtils.contains(codigo,"/")? StringUtils.replace(codigo,"/",""):codigo;
	         fileName=StringUtils.deleteWhitespace(codigo);
         
        }
        
        if (action.equalsIgnoreCase("doEstadInvest")){
	        
        	
			 try {
	             conn = df.createConnection();
	             excelDao=df.getEstadisticasExcelDAO();
	             
	             wb=excelDao.findLotesByInvestigadorEXCEL(conn, codigo, coleccion);
	             
	         } catch (SQLException e) {
	             System.out.println("Fallida la conexion no funciona "+e);
	         }finally {
	        	 if(conn!=null){
					ConnectionFactory.closeConnection(conn);
	        	 }
					conn=null;
			}
	         codigo=StringUtils.contains(codigo,"/")? StringUtils.replace(codigo,"/",""):codigo;
	        	
	        fileName=StringUtils.deleteWhitespace(codigo);
        }
        
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\""
				+ "datos_estadisticas_"+fileName +".xls"+ "\"");
		response.setHeader("Pragma", "no-cache");

		ServletOutputStream outs = response.getOutputStream();
		wb.write(outs);
		outs.flush();
		outs.close();
		
	}

}
