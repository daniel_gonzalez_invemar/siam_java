package co.org.invemar.catalogo.controller;

import co.org.invemar.catalogo.model.DAOFactory;
import co.org.invemar.catalogo.model.EstadisticasXMLDAO;
import co.org.invemar.catalogo.model.SearchCatalogoDAO;
import co.org.invemar.catalogo.model.SearchDAO;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.*;
import javax.servlet.http.*;

import org.json.JSONArray;
import org.json.JSONException;

public class ServletAjaxProcess extends HttpServlet {
    private static final String CONTENT_TYPE = "text/xml; charset=windows-1252";
    private static final String DOC_TYPE = null;
    private DAOFactory  df;
    private Connection  conn;
    SearchCatalogoDAO searchDao;
    private List generos;
    private List species;
    private List familias;
    private List orden;
    //

    public void init(ServletConfig config) throws ServletException {
        
        
       df = new DAOFactory(); 
       
       searchDao=df.getSearchCatalogoDAO();
       try{
       
        conn=df.createConnection();
        generos=searchDao.findAll(conn,SearchCatalogoDAO.SQL_ALLGENEROS);
        conn=df.createConnection();
        species=searchDao.findAll(conn,SearchCatalogoDAO.SQL_ALLSPECIES);
        conn=df.createConnection();
        familias=searchDao.findAll(conn,SearchCatalogoDAO.SQL_ALLFAMILIAS);
        conn=df.createConnection();
        orden=searchDao.findAll(conn,SearchCatalogoDAO.SQL_ALLORDEN);
       }catch(SQLException e){
        System.out.println("Error en el init del servletAjaxProcess "+e);
       }
       
        
    }

    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
      
       df = new DAOFactory();
        SearchDAO search;
        EstadisticasXMLDAO xmlDao;
       NameService service=null;
       
        
        
        String str=request.getParameter("action");
        System.out.println("ACTION:.. "+str);
        
        if(str.equals("doCompletion")){
            String prefix=request.getParameter("valorBusqueda");
            String types=request.getParameter("types");
            
            if(types.equals("genero")){
              service = NameService.getInstance(generos);
            }else if(types.equals("especie")){
              service = NameService.getInstance(species);
            }if(types.equals("familia")){
              service = NameService.getInstance(familias);
            }if(types.equals("orden")){
              service = NameService.getInstance(orden);
            }
            
            
            List matching = service.findNames(prefix);
            System.out.println("MATCHING :.. "+matching);
            
            if (matching.size() > 0) {
                  PrintWriter out = response.getWriter();
                  response.setContentType("text/xml");
                  response.setHeader("Cache-Control", "no-cache");
                  out.println("<response>");
                  Iterator iter = matching.iterator();
                  while(iter.hasNext()) {
                      String name = (String) iter.next();
                      out.println("<name>" + name + "</name>");
                  }
                  out.println("</response>");
                  matching = null;
                  service = null;
                  out.close();
          } else {
              response.setStatus(HttpServletResponse.SC_NO_CONTENT);
          }
        }
        
        if(str.equals("doFillColeccion")){
            
            
            String respon="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respon=xmlDao.loadColeccionesXML(conn);
                if(respon!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respon);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion no funciona "+e);
            }
            
            
           
        }
        
if(str.equals("doFillListProyectos")){
            
            
            String respon="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respon=xmlDao.loadProyectosXML(conn);
                if(respon!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respon);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion no funciona "+e);
            }
            
            
           
        }
        
      if(str.equals("doFillListPhylums")){
            
            
            String respon="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respon=xmlDao.loadPhylumsXML(conn);
                if(respon!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respon);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion no funciona "+e);
            }
           
        }
        
        String coleccion=request.getParameter("coleccion");
        if(str.equals("doFillFechaRecibido")){
        
        	
            String respon="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respon=xmlDao.loadSeccionesXML(conn, coleccion);
                if(respon!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respon);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion NO funciona "+e);
            }
           
        }
        
        if(str.equals("doEstadisticaFechaRecibido")){
            String value=request.getParameter("valor");
            System.out.println("VALOR:... "+value );
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findBySeccionFechaRecibido(conn,value, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
            
         
        }
        
        if(str.equals("doFillMuestraFI")){
        
            String respon="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respon=xmlDao.loadSeccionesFechaIdentXML(conn, coleccion);
                if(respon!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respon);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
           
        }
       
       
        if(str.equals("doFillFechaIdent")){
            String value=request.getParameter("valor");
            
            String respuesta="";
            
           
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findFechaIdentBySeccion(conn,value, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
                   
        }
        
        if(str.equals("doEstadFechaIdent")){
            String value=request.getParameter("valorSeccion");
            String valueAno=request.getParameter("valorAno");
            System.out.println("VALOR!!!!!!!!!!!!:... "+value );
            System.out.println("VALOR Ano!!!!!!!!!!!!:... "+valueAno );
            
            String respuesta="";
            
            
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesBySeccionFechaIdent(conn,value,valueAno, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
            
          
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////
      if(str.equals("doFillMuestraFC")){
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.loadSeccionesFechaCapturaXML(conn, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
        
        }
        
        
        if(str.equals("doFillFechaCaptura")){
            String value=request.getParameter("valor");
            String respuesta="";
            
            
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findFechaCaptBySeccion(conn,value, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
           
        }
        
        if(str.equals("doEstadFechaCaptura")){
            String value=request.getParameter("valorSeccion");
            String valueAno=request.getParameter("valorAno");
            System.out.println("VALOR!!!!!!!!!!!!:... "+value );
            System.out.println("VALOR Ano!!!!!!!!!!!!:... "+valueAno );
            
            String respuesta="";
            
            
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesBySeccionFechaCapt(conn,value,valueAno, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
          
        }

        if(str.equals("doSeccionImages")){
        
                String respon="";
                
                try {
                    conn = df.createConnection();
                    xmlDao=df.getEstadisticasXMLDAO();
                    respon=xmlDao.findSeccionImages(conn, coleccion);
                    if(respon!=null){
                        response.setContentType("text/xml");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().write(respon);            
                    }else{
                         //nothing to show
                         response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                    }
                    
                    
                } catch (SQLException e) {
                    System.out.println("Fallida la conexion parece que no funciona "+e);
                }
            
        }
        
        if(str.equals("doFillMuestraPROY")){
            
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.loadSeccionesProyectosXML(conn);
                if(respuesta!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
            
        }
        
        if(str.equals("doEstadSeccProyectos")){
            String value=request.getParameter("valor");
            System.out.println("VALOR PROYECTOSS:  "+value);
            String respuesta=null;
            
            
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesProyectosBySeccion(conn,value);
                if(respuesta!=null){
                    response.setContentType("text/xml; charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
           
        }
        if(str.equals("doFillInvestigadores")){
            
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.loadInvestigadoresXML(conn, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml; charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
            
        }
        
        if(str.equals("doEstadInvest")){
            String value=request.getParameter("valor");
            System.out.println("VALOR PROYECTOSS:  "+value);
            String respuesta=null;
            
            
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesByInvestigador(conn,value, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml; charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
           
        }
        
        
        if(str.equals("viewAllSFI")){
            String value=request.getParameter("valor");
            System.out.println("VALOR:... "+value );
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesFechaIdentBySeccion(conn,value, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
            
         
        }
        
        
        if(str.equals("viewAllSFC")){
            String value=request.getParameter("valor");
            System.out.println("VALOR:... "+value );
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesFechaCaptBySeccion(conn,value, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
         }
         
         
        if(str.equals("doFillMuestraPRES")){
        
        
            String respon="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respon=xmlDao.loadSeccionesPreservativoXML(conn, coleccion);
                if(respon!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respon);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
           
        }
        
        if(str.equals("doEstadPreservativos")){
            String value=request.getParameter("valor");
            System.out.println("VALOR:... "+value );
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesPreservativoBySeccion(conn,value, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("doFillMuestraObjRelac")){
        
        
            String respon="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respon=xmlDao.loadSeccionesObjetosXML(conn, coleccion);
                if(respon!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respon);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
           
        }
        
        if(str.equals("doEstadObjRelacionado")){
            String value=request.getParameter("valor");
            System.out.println("VALOR:... "+value );
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesObjetosBySeccion(conn,value, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("doFillSeccionINV")){
        
        
            String respon="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respon=xmlDao.loadSeccionesInvestigadorXML(conn, coleccion);
                if(respon!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respon);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
           
        }
        
        
        if(str.equals("doEstadInvestigadores")){
            String value=request.getParameter("valor");
            System.out.println("VALOR:... "+value );
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesInvestigadorBySeccion(conn,value, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("doMuestrasByPhylum")){
        
        
            String respon="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respon=xmlDao.findLotesByPhylums(conn, coleccion);
                if(respon!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respon);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
           
        }
        
        
        if(str.equals("doFillPhylum")){
        
        
            String respon="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respon=xmlDao.findPhilums(conn);
                if(respon!=null){
                    response.setContentType("text/xml");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respon);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
            }
           
        }
        
        if(str.equals("doEstadNivelPhylum")){
            String value=request.getParameter("valor");
            System.out.println("VALOR:... "+value );
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                
                respuesta=xmlDao.findLotesNivelByPhylums(conn,value);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("doEstadNivelPhylumBasic")){
            String value=request.getParameter("valor");
            
            String proyecto=request.getParameter("proyecto");
            String nivel54=request.getParameter("nivel54");
            String respuesta="";
            
            
                      
            nivel54=nivel54.equals("false")?null:nivel54;
           
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                
                respuesta=xmlDao.findLotesByGruposTax(conn, value, coleccion, proyecto,nivel54);//xmlDao.findLotesNivelByPhylums(conn,value);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("dosearchRMuseoByNivel")){
            String value=request.getParameter("valor");
            
            String proyecto=request.getParameter("proyecto");
            
            String respuesta="";
            
            
            
            coleccion=coleccion.equals("")?null:coleccion;
            proyecto=proyecto.equals("")?null:proyecto;
            
                     
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                
                respuesta=xmlDao.findRLotesByNivel(conn, value, coleccion, proyecto);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("doEstadNivelTax")){
            String value=request.getParameter("valor");
            System.out.println("VALOR:... "+value );
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesByNivelTax(conn,value);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("doEstadEspeciesNivelTax")){
            String value=request.getParameter("valor");
            System.out.println("VALOR:... "+value );
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findEspecimenesByNivelTax(conn,value);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("doEstadElementosNivelTax")){
            String value=request.getParameter("valor");
            System.out.println("VALOR:... "+value );
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findGruposTaxByPhylum(conn,value);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("doEstadElementDiccNivelTax")){
            String value=request.getParameter("valor");
            System.out.println("VALOR:... "+value );
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findListadoGruposenDicc(conn,value);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        
        if(str.equals("doFichaEspecByPhylum")){
            
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findEspecieConDescrip(conn);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("doEspecieByPhylum")){
            
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findNombresViegentes(conn);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("doEstadLotesByAreaGeograf")){
            
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesByAreasGeograficas(conn);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        
        if(str.equals("doEstadMantenimiento")){
            
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findDatosMantenimiento(conn,coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        
        if(str.equals("doEstadMantenimtoConFaltantes")){
            
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findDatosMantenimientoFaltantes(conn, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        
        if(str.equals("doEstadNomComuneSinonimias")){
            
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findNumSinonimiasAndVulgares(conn, coleccion);
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        
        if(str.equals("doLotesOtrasEntidades")){
            
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesOtrasEntidades(conn,coleccion);
                
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("doLotesByTipos")){
            
            String respuesta="";
            
            try {
                conn = df.createConnection();
                xmlDao=df.getEstadisticasXMLDAO();
                respuesta=xmlDao.findLotesByTipos(conn,coleccion);
                
                if(respuesta!=null){
                    response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta);            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        
        if(str.equals("fillEstaciones")){
            
            JSONArray respuesta;
            String proyecto=request.getParameter("proyecto");
            try {
                conn = df.createConnection();
                search=df.getSearchDAO();
                respuesta=search.findEstacionesByProyectos(conn, proyecto);
                
                if(respuesta!=null){
                    //response.setContentType("text/xml;charset=ISO-8859-1");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().write(respuesta.toString());            
                }else{
                     //nothing to show
                     response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                
                
            } catch (SQLException e) {
                System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            } catch (JSONException e) {
            	System.out.println("Fallida la conexion parece que no funciona "+e);
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}
        }
        
    }

    public void doPost(HttpServletRequest request, 
                       HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        doGet(request,response);
    }
}
