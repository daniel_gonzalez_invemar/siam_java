package co.org.invemar.catalogo.model;

public class Sinonimias {
    private String sinonimia;
    private String autor;
    private String tipo;


    public void setSinonimia(String sinonimia) {
        this.sinonimia = sinonimia;
    }

    public String getSinonimia() {
        return sinonimia;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getAutor() {
        return autor;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }
}