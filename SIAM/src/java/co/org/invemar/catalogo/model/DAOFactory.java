package co.org.invemar.catalogo.model;


import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;

public class DAOFactory {
    // Initialize the JNDI InitialContext somewhere static in your application...
    static InitialContext initialContext;
    
    
    static
    {
      try
      {
        initialContext = new InitialContext();

      }
      catch ( NamingException ne )
      {
        
        System.out.println("Error!! No se ha podido crear el Objeto InitialContext "+ne);
      }
    }
    /**DataSource para OC4J*/
    public synchronized DataSource getDataSource() {
            DataSource data;
            Context envContext;       
            try {
                   envContext = (Context) initialContext.lookup ("java:comp/env");
                    data = (DataSource) envContext.lookup ("jdbc/sibm");
                               
                    
                    System.out.println("DataSource obtenido exitosamente");
                    return data;
            } catch (Exception ex) {
                    System.out.println("ERROR: en getDataSource::::::::::::::::::. " + ex);
                    return null;
            }
    }

	 /*DataSource para JBoss
	public DataSource getDataSource() {
		DataSource data;
		InitialContext ic;
		Properties props = new Properties();

		props.put(Context.INITIAL_CONTEXT_FACTORY,
				"org.jnp.interfaces.NamingContextFactory");
		props.put(Context.PROVIDER_URL, "localhost:5432");

		try {
			ic = new InitialContext(props);
			data = (DataSource) ic.lookup("java:mysqlDS");
			System.out.println("DataSource obtenido exitosamente");
			return data;
		} catch (Exception ex) {
			System.out.println("ERROR: en getDataSource: " + ex);
			return null;
		}
	}*/
	//datasource para apache tomcat
        /*
	public synchronized DataSource getDataSource() {
		DataSource data;
		InitialContext ic;
		Context envContext;
		try {
			ic = new InitialContext();
			 envContext = (Context) ic.lookup ("java:comp/env");
	        data = (DataSource) envContext.lookup ("jdbc/biblioteca");
	        	           
			
			System.out.println("DataSource obtenido exitosamente");
			return data;
		} catch (Exception ex) {
			System.out.println("ERROR: en getDataSource: " + ex);
			return null;
		}
	}
	*/
        
        
        
        
	public synchronized Connection createConnection() throws SQLException {
		Connection con = getDataSource().getConnection();
		return con;
	}
	
	public SearchCatalogoDAO getSearchCatalogoDAO(){
            return (new SearchCatalogoDAO());
        
        }
        
    public EstadisticasXMLDAO getEstadisticasXMLDAO(){
        return new EstadisticasXMLDAO();
    }
    public EstadisticasExcelDAO getEstadisticasExcelDAO(){
        return new EstadisticasExcelDAO();
    }
    
    public EstadisticaDAO getStatDAO(){
        return new EstadisticaDAO();
    }
    
    public SearchDAO getSearchDAO(){
    	return new SearchDAO();
    }
	
}