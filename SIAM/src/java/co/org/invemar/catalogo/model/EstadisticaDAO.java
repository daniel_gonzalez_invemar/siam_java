package co.org.invemar.catalogo.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EstadisticaDAO {
    public EstadisticaDAO() {
    }
    
    
    private static final String SQL_INSERT="insert into publico.estadisticas (fecha,pagina,ip,subcategoria) values (sysdate,?,?,?)";
    
    public void insertStat(Connection connection,int pagina,String ip,int subcategoria) throws SQLException {
    
        PreparedStatement preparedStatement = null;
        try {

            /* Create "preparedStatement". */

            preparedStatement = connection.prepareStatement(SQL_INSERT);

            /* Fill "preparedStatement". */
            int i = 1;
            
            preparedStatement.setInt(i++,pagina);
            preparedStatement.setString(i++,ip);
            preparedStatement.setInt(i++,subcategoria);
            
            /* Execute query. */
            int insertedRows = preparedStatement.executeUpdate();

            if (insertedRows == 0) {
                    throw new SQLException("Can not add row to table" + " ' ???? '");
            }

            if (insertedRows > 1) {
                    throw new SQLException("Duplicate row in table ' ???? '");
            }
            
        
        } finally {
            if (preparedStatement != null)
                preparedStatement.close();
            if (connection!=null){
                connection.close();
            }           
        }
    }
    
    
}
