package co.org.invemar.catalogo.model;

import java.sql.Connection;

import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

public class SearchCatalogoDAO {
    
    static final String SQL_GENUS= "select f.CLAVE, f.ULTIMOCAMBIO, f.COLECCION, f.CODIGOCOLECCION, f.NUMEROCATALOGO,f.ESPECIE, f.BASEDELREGISTRO,\n" + 
    "    f.REINO, f.DIVISION, f.CLASE, f.ORDEN, f.FAMILIA, f.GENERO, f.EPITETO, f.AUTORESPECIE, f.IDENTIFICADO_POR,\n" + 
    "    f.ANOIDENTIFICACION, f.TIPO, f.COLECTADO_POR, f.ANOCAPTURA, f.NOMBRECAGUA, f.PAIS, f.DEPTO, f.LOCALIDAD, \n" + 
    "    f.LONGITUD, f.LATITUD, f.PROF_CAPTURA, f.EJEMPLARES, f.NOINV,  i.Archivo_im, i.notas_im, \n" + 
    "    i.nroimagen_im, i.nroinvemar_im , i.clave_im\n" + 
    "from CURADOR.VM_FICHAC f , cimagenes i\n" + 
    "where  f.NOINV=i.NROINVEMAR_IM(+) and (clave in (select clave_sn\n" + 
    "                                                 from csinonimias\n" + 
    "                                                 where tipo_sn>0 and descripcion_sn like ?) " +
    "                                             or clave in (select clave \n" + 
    "						               from vm_fichac \n" + 
    "							       where genero like ?)) order by genero";

    static final String SQL_SPECIES="select f.CLAVE,f.ULTIMOCAMBIO, f.COLECCION, f.CODIGOCOLECCION, f.NUMEROCATALOGO,f.ESPECIE, f.BASEDELREGISTRO,\n" + 
    "    f.REINO, f.DIVISION, f.CLASE, f.ORDEN, f.FAMILIA, f.GENERO, f.EPITETO, f.AUTORESPECIE, f.IDENTIFICADO_POR,\n" + 
    "    f.ANOIDENTIFICACION, f.TIPO, f.COLECTADO_POR, f.ANOCAPTURA, f.NOMBRECAGUA, f.PAIS, f.DEPTO, f.LOCALIDAD, \n" + 
    "    f.LONGITUD, f.LATITUD, f.PROF_CAPTURA, f.EJEMPLARES,f.NOINV,  i.Archivo_im, i.notas_im, \n" + 
    "    i.nroimagen_im, i.nroinvemar_im , i.clave_im\n" + 
    "from CURADOR.VM_FICHAC f , curador.cimagenes i\n" + 
    "where  f.NOINV=i.NROINVEMAR_IM(+) and (clave in (select clave_sn\n" + 
    "                                                 from curador.csinonimias\n" + 
    "                                                 where tipo_sn>0 and descripcion_sn like ? ) " +
    "                                              or clave in (select clave \n" + 
    "						                from curador.vm_fichac \n" + 
    "								where especie like ?)) order by especie";
    
    
    static final String SQL_ORDEN="select f.CLAVE,f.ULTIMOCAMBIO, f.COLECCION, f.CODIGOCOLECCION, f.NUMEROCATALOGO,f.ESPECIE, f.BASEDELREGISTRO,\n" + 
    "    f.REINO, f.DIVISION, f.CLASE, f.ORDEN, f.FAMILIA, f.GENERO, f.EPITETO, f.AUTORESPECIE, f.IDENTIFICADO_POR,\n" + 
    "    f.ANOIDENTIFICACION, f.TIPO, f.COLECTADO_POR, f.ANOCAPTURA, f.NOMBRECAGUA, f.PAIS, f.DEPTO, f.LOCALIDAD, \n" + 
    "    f.LONGITUD, f.LATITUD, f.PROF_CAPTURA, f.EJEMPLARES,f.NOINV,  i.Archivo_im, i.notas_im, \n" + 
    "    i.nroimagen_im, i.nroinvemar_im , i.clave_im\n" + 
    "from CURADOR.VM_FICHAC f , curador.cimagenes i\n" + 
    "where  f.NOINV=i.NROINVEMAR_IM(+) and (clave in (select clave_sn\n" + 
    "                                                 from curador.csinonimias\n" + 
    "                                                 where tipo_sn>0 and descripcion_sn like ? ) " +
    "                                              or clave in (select clave \n" + 
    "                                                           from curador.vm_fichac \n" + 
    "                                                           where orden like ?)) order by orden";
    
    static final String SQL_FAMILIA="select f.CLAVE,f.ULTIMOCAMBIO, f.COLECCION, f.CODIGOCOLECCION, f.NUMEROCATALOGO,f.ESPECIE, f.BASEDELREGISTRO,\n" + 
    "    f.REINO, f.DIVISION, f.CLASE, f.ORDEN, f.FAMILIA, f.GENERO, f.EPITETO, f.AUTORESPECIE, f.IDENTIFICADO_POR,\n" + 
    "    f.ANOIDENTIFICACION, f.TIPO, f.COLECTADO_POR, f.ANOCAPTURA, f.NOMBRECAGUA, f.PAIS, f.DEPTO, f.LOCALIDAD, \n" + 
    "    f.LONGITUD, f.LATITUD, f.PROF_CAPTURA, f.EJEMPLARES,f.NOINV,  i.Archivo_im, i.notas_im, \n" + 
    "    i.nroimagen_im, i.nroinvemar_im , i.clave_im\n" + 
    "from CURADOR.VM_FICHAC f , curador.cimagenes i\n" + 
    "where  f.NOINV=i.NROINVEMAR_IM(+) and (clave in (select clave_sn\n" + 
    "                                                 from curador.csinonimias\n" + 
    "                                                 where tipo_sn>0 and descripcion_sn like ? ) " +
    "                                              or clave in (select clave \n" + 
    "                                                           from curador.vm_fichac \n" + 
    "                                                           where familia like ?)) order by familia";
    
            
            
    
    static final String SQL_SINONIMIAS="select s.descripcion_sn, s.autores_sn, c.descripcion_lov \n" + 
    "from curador.csinonimias s,codigoslov c \n" + 
    "where c.tabla_lov=11 and s.tipo_sn=c.codigo_lov and s.clave_sn=?";
    
    
    public static String SQL_AUTORESFICHA="SELECT   Apellidos_co||' '||nombre_co, sec.descripcion_lov descripicon, to_char(fecha_re,'dd/mm/yyyy'), lov.descripcion_lov\n" + 
    "    FROM cdircolector dir, curador.cmresponsables cmr, codigoslov lov, codigoslov sec\n" + 
    "   WHERE cmr.clave_re = ?\n" + 
    "     AND cmr.codigo_co = dir.codigo_co\n" + 
    "     AND (cmr.tarea_re = lov.codigo_lov)\n" + 
    "     AND (cmr.seccion_re = sec.codigo_lov)\n" + 
    "     AND (lov.tabla_lov = 15)\n" + 
    "ORDER BY 1, 2 ";
    
    
    public static String SQL_AUTORESFICHAMUSEO="SELECT   Apellidos_co||' '||nombre_co Nombre, sec.descripcion_lov seccion, to_char(fecha_re,'dd/mm/yyyy') fecha, lov.descripcion_lov\n" + 
    "   FROM cdircolector dir, curador.cmresponsables cmr, codigoslov lov, codigoslov sec\n" + 
    "  WHERE cmr.nroinvemar_re = ? \n" + 
    "    AND cmr.codigo_co   = dir.codigo_co\n" + 
    "    AND (cmr.tarea_re   = lov.codigo_lov)\n" + 
    "    AND (lov.tabla_lov = 15 or lov.tabla_lov = 12)\n" + 
    "    AND (cmr.seccion_re = sec.codigo_lov)\n" + 
    "    and sec.tabla_lov=29\n" + 
    "ORDER BY 1, 2";
    
    public static final String SQL_ALLGENEROS="select distinct lower(genero) from vm_fichac order by 1";
    public static final String SQL_ALLSPECIES="select distinct especie from vm_fichac order by 1";
    public static final String SQL_ALLFAMILIAS="select distinct lower(familia) from vm_fichac order by 1";
    public static final String SQL_ALLORDEN="select distinct lower(orden) from vm_fichac order by 1";
    
    
    
      
    public List findByGenus(Connection conn, 
                            String genus) throws SQLException {
         
        genus=stringConverter(genus);
        PreparedStatement preparedStatement = null;
        preparedStatement = conn.prepareStatement(SearchCatalogoDAO.SQL_GENUS);
        preparedStatement.setString(1, genus+"%");
        preparedStatement.setString(2, genus+"%");
        

        List searchResults;
        searchResults = listQuery(preparedStatement);
        
        if (conn!=null){
            conn.close();
        }
        
        return searchResults;
    }

    public List findBySpecies(Connection conn, 
                              String species) throws SQLException {
        
        //species=stringConverter(species);
        PreparedStatement preparedStatement = null;
        preparedStatement = conn.prepareStatement(SearchCatalogoDAO.SQL_SPECIES);
        preparedStatement.setString(1, species+"%");
        preparedStatement.setString(2, species+"%");

        List searchResults;
        searchResults = listQuery(preparedStatement);
        
        if (conn!=null){
            conn.close();
        }
    
        return searchResults;
    }

    public List findByFamilia(Connection conn, 
                              String familia) throws SQLException {
        
        //species=stringConverter(species);
        PreparedStatement preparedStatement = null;
        preparedStatement = conn.prepareStatement(SearchCatalogoDAO.SQL_FAMILIA);
        preparedStatement.setString(1, familia.toUpperCase()+"%");
        preparedStatement.setString(2, familia.toUpperCase()+"%");

        List searchResults;
        searchResults = listQuery(preparedStatement);
        
        if (conn!=null){
            conn.close();
        }
    
        return searchResults;
    }
    
    public List findByOrden(Connection conn, 
                              String orden) throws SQLException {
        
        //species=stringConverter(species);
        PreparedStatement preparedStatement = null;
        preparedStatement = conn.prepareStatement(SearchCatalogoDAO.SQL_ORDEN);
        preparedStatement.setString(1, orden.toUpperCase()+"%");
        preparedStatement.setString(2, orden.toUpperCase()+"%");

        List searchResults;
        searchResults = listQuery(preparedStatement);
        
        if (conn!=null){
            conn.close();
        }
    
        return searchResults;
    }
    

    public List findSinonimiasByClave(String clave, Connection conn) throws SQLException {
        PreparedStatement preparedStatement = null;
        preparedStatement = conn.prepareStatement(SearchCatalogoDAO.SQL_SINONIMIAS);
        preparedStatement.setString(1, clave);
       
       
        
        List searchResults = new ArrayList();
        ResultSet result = null;
        
        try {
            
            result = preparedStatement.executeQuery();
            while (result.next()) {
            
                Sinonimias sinonimia=new Sinonimias();
                sinonimia.setSinonimia(deleteNullOfRetrive(result.getString("descripcion_sn")));
                sinonimia.setAutor(deleteNullOfRetrive(result.getString("autores_sn")));
                sinonimia.setTipo(deleteNullOfRetrive(result.getString("descripcion_lov")));
                searchResults.add(sinonimia);
            }
         
                
        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn!=null){
                conn.close();
            }
        }
      
        
        return searchResults;
    }

    protected List listQuery(PreparedStatement stmt) throws SQLException {

        ArrayList searchResults = new ArrayList();
        ResultSet result = null;

        try {
            result = stmt.executeQuery();
            
            
            while (result.next()) {
                Catalogo temp;
                temp = new Catalogo();
                Imagenes img=new Imagenes();
                temp.setClave(deleteNullOfRetrive(result.getString("CLAVE")));
                temp.setUltimoCambio(deleteNullOfRetrive(result.getString("ULTIMOCAMBIO")));
                temp.setColeccion(deleteNullOfRetrive(result.getString("COLECCION")));
                temp.setCodColeccion(deleteNullOfRetrive(result.getString("CODIGOCOLECCION")));
                temp.setNumCatalogo(deleteNullOfRetrive(result.getString("NUMEROCATALOGO")));
                temp.setEspecie(deleteNullOfRetrive(result.getString("ESPECIE")));
                temp.setBaseRegistro(deleteNullOfRetrive(result.getString("BASEDELREGISTRO")));
                temp.setReino(deleteNullOfRetrive(result.getString("REINO")));
                temp.setDivision(deleteNullOfRetrive(result.getString("DIVISION")));
                temp.setClase(deleteNullOfRetrive(result.getString("CLASE")));
                temp.setOrden(deleteNullOfRetrive(result.getString("ORDEN")));
                temp.setFamilia(deleteNullOfRetrive(result.getString("FAMILIA")));
                temp.setGenero(deleteNullOfRetrive(result.getString("GENERO")));
                temp.setEpiteto(deleteNullOfRetrive(result.getString("EPITETO")));
                temp.setAutorEspecie(deleteNullOfRetrive(result.getString("AUTORESPECIE")));
                temp.setIdentificadoPor(deleteNullOfRetrive(result.getString("IDENTIFICADO_POR")));
                temp.setAnoIdentificacion(deleteNullOfRetrive(result.getString("ANOIDENTIFICACION")));
                temp.setTipo(deleteNullOfRetrive(result.getString("TIPO")));
                temp.setColectadoPor(deleteNullOfRetrive(result.getString("COLECTADO_POR")));
                temp.setAnoCaptura(deleteNullOfRetrive(result.getString("ANOCAPTURA")));
                temp.setContinentOcean(deleteNullOfRetrive(result.getString("NOMBRECAGUA")));
                temp.setPais(deleteNullOfRetrive(result.getString("PAIS")));
                temp.setDepartamento(deleteNullOfRetrive(result.getString("DEPTO")));
                temp.setLocalidad(deleteNullOfRetrive(result.getString("LOCALIDAD")));
                temp.setLongitud(deleteNullOfRetrive(result.getString("LONGITUD")));
                temp.setLatitud(deleteNullOfRetrive(result.getString("LATITUD")));
                temp.setProfCaptura(deleteNullOfRetrive(result.getString("PROF_CAPTURA")));
                temp.setEjemplares(deleteNullOfRetrive(result.getString("EJEMPLARES")));
                temp.setNroInvemar((result.getString("NOINV")!=null?result.getString("NOINV"):new String("0")));
                /************************************************/            
                img.setArchivoIm(deleteNullOfRetrive(result.getString("Archivo_im")));
                img.setNotasIm(deleteNullOfRetrive(result.getString("notas_im")));
                img.setNroImagenIm(result.getString("nroimagen_im"));
                img.setNroInvemarIm(result.getString("nroinvemar_im"));
                img.setClaveIm(result.getString("clave_im"));
                /************************************************/
                temp.setImg(img);

                searchResults.add(temp);
            }

        } finally {
            if (result != null)
                result.close();
            if (stmt != null)
                stmt.close();
        }

        return searchResults;
    }

    private String deleteNullOfRetrive(String datos){
        if (datos==null){
            datos="";
           return datos;
        }else{
            return datos;
        }
    
    }
    public int countAll(Connection conn) throws SQLException {

        String sql;
        sql = "SELECT count(*) FROM CURADOR.VM_FICHAC";
        PreparedStatement stmt = null;
        ResultSet result = null;
        int allRows = 0;

        try {
            stmt = conn.prepareStatement(sql);
            result = stmt.executeQuery();

            if (result.next())
                allRows = result.getInt(1);
        } finally {
            if (result != null)
                result.close();
            if (stmt != null)
                stmt.close();
        }
        return allRows;
    }
    
    public String stringConverter(String name){
        String newName=null;
        name=name.toLowerCase();
        if(name.length()>0){
            char letter=name.charAt(0);
            letter=Character.toUpperCase(letter);
            String nameSubstring=name.substring(1);
            newName=letter+""+nameSubstring;
            
        }
        return newName;
    }
    

   public List findAll(Connection conn, String sql) throws SQLException {
        PreparedStatement preparedStatement = null;
        preparedStatement = conn.prepareStatement(sql);
              
        
        List searchResults = new ArrayList();
        ResultSet result = null;
        
        try {
            
            result = preparedStatement.executeQuery();
            while (result.next()) {
                 String valor=result.getString(1)==null? new String("") :result.getString(1);
                 searchResults.add(valor);
            }
         
                
        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn!=null){
                conn.close();
            }
        }
      
        
        return searchResults;
    }
    
    public List findAutoresFicha(String valor, Connection conn, String SQL) throws SQLException {
        PreparedStatement preparedStatement = null;
        preparedStatement = conn.prepareStatement(SQL);
        preparedStatement.setString(1, valor);
       
       
        
        List searchResults = new ArrayList();
        ResultSet result = null;
        
        try {
            
            result = preparedStatement.executeQuery();
            while (result.next()) {
            
                AutorFicha autor=new AutorFicha();
                autor.setNombre(deleteNullOfRetrive(result.getString(1)));
                autor.setDescripcion(deleteNullOfRetrive(result.getString(2)));
                autor.setFecha(deleteNullOfRetrive(result.getString(3)));
                autor.setTarea(deleteNullOfRetrive(result.getString(4)));
                searchResults.add(autor);
            }
         
                
        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn!=null){
                conn.close();
            }
        }
      
        
        return searchResults;
    }
    
 

}
