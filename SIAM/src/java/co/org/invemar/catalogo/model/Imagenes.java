package co.org.invemar.catalogo.model;

public class Imagenes {
    
    private String archivoIm;
    private String notasIm;
    private String nroImagenIm;
    private String nroInvemarIm;
    private String claveIm;


    public void setArchivoIm(String archivoIm) {
        this.archivoIm = archivoIm;
    }

    public String getArchivoIm() {
        return archivoIm;
    }

    public void setNotasIm(String notasIm) {
        this.notasIm = notasIm;
    }

    public String getNotasIm() {
        return notasIm;
    }

    public void setNroImagenIm(String nroImagenIm) {
        this.nroImagenIm = nroImagenIm;
    }

    public String getNroImagenIm() {
        return nroImagenIm;
    }

    public void setNroInvemarIm(String nroInvemarIm) {
        this.nroInvemarIm = nroInvemarIm;
    }

    public String getNroInvemarIm() {
        return nroInvemarIm;
    }

   public void setClaveIm(String claveIm) {
        this.claveIm = claveIm;
    }

    public String getClaveIm() {
        return claveIm;
    }
}
