package co.org.invemar.catalogo.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SearchDAO {
	private static final String SQL_FINDEST="SELECT nombre_corto AS proyecto, estacion FROM clst_proyectos_estaciones WHERE nombre_corto = ?";
	
	 public JSONArray findEstacionesByProyectos(Connection conn, String proyecto) throws SQLException, JSONException {
	        PreparedStatement preparedStatement = null;
	        preparedStatement = conn.prepareStatement(SQL_FINDEST);
	           
	        ResultSet result = null;
	        JSONObject resp = null;
			JSONArray list = new JSONArray();
			
	        try {
	            
	            result = preparedStatement.executeQuery();
	            while (result.next()) {
	            	resp = new JSONObject();
	            	String value=result.getString(1);
	            	resp.put("value", value);
					resp.put("name", value);
					list.put(resp);
	               
	            }
	         
	                
	        } finally {
	            if (result != null)
	                result.close();
	            if (preparedStatement != null)
	                preparedStatement.close();
	            if (conn!=null){
	                conn.close();
	            }
	        }
	      
	        
	        return list;
	    }

}
