/**
 * 
 */
package co.org.invemar.catalogo.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.HSSFCell;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * @author Administrador
 * 
 */
public class EstadisticasExcelDAO {

	public HSSFWorkbook findLotesByGruposTaxEXCEL(Connection conn,
			String codigo, String coleccion, String proyecto, String nivel)
			throws SQLException {

		PreparedStatement preparedStatement = null;

		StringBuffer selectAndFom = new StringBuffer(
				"SELECT   vm.phylum, NVL (vm.clase, 'No Clasificado') clase, "
						+ " NVL (vm.orden, 'No Clasificado') orden, "
						+ " NVL (vm.familia, 'No Clasificado') familia, "
						+ " NVL (vm.genero, 'No Clasificado') genero, "
						+ " NVL (vm.especie, 'No Clasificado') especie, SUM (cl.individuos) Lotes, sum(ejemplares) ejemplares, "
						+ " GROUPING (NVL (vm.clase, 'No Clasificado')) "
						+ " || GROUPING (NVL (vm.orden, 'No Clasificado')) "
						+ " || GROUPING (NVL (vm.familia, 'No Clasificado')) "
						+ " || GROUPING (NVL (vm.genero, 'No Clasificado')) "
						+ " || GROUPING (NVL (vm.especie, 'No Clasificado')) grupo "
						+ " FROM vm_planilla vm, "
						+ " 	(SELECT   clave, COUNT (clave) individuos, sum(nvl(ejemplares,0)) ejemplares "
						+ "		 	FROM vm_fichac " + " 		WHERE phylum = ? ");
		StringBuffer subWhere = null;
		String endSelect = "  GROUP BY clave) cl "
				+ " WHERE cl.clave = vm.clave " + " GROUP BY vm.phylum, "
				+ " ROLLUP (NVL (vm.clase, 'No Clasificado'), "
				+ " NVL (vm.orden, 'No Clasificado'), "
				+ " NVL (vm.familia, 'No Clasificado'), "
				+ " NVL (vm.genero, 'No Clasificado'), "
				+ " NVL (vm.especie, 'No Clasificado')) " + " ORDER BY 1, 2";
		int sw = 0;
		if (!isEmpty(coleccion) && !isEmpty(proyecto)) {
			subWhere = new StringBuffer();
			subWhere.append(" AND ubi_geo = ? AND proyecto = ? ");
			if (nivel != null && sw == 0) {
				subWhere.append("  and nivel>54");
				sw = 1;
			}
			selectAndFom.append(subWhere.toString());
			selectAndFom.append(endSelect);

			preparedStatement = conn.prepareStatement(selectAndFom.toString());
			preparedStatement.setString(1, codigo);
			preparedStatement.setString(2, coleccion);
			preparedStatement.setString(3, proyecto);

		} else if (!isEmpty(coleccion) && isEmpty(proyecto)) {
			subWhere = new StringBuffer();
			subWhere.append(" AND ubi_geo = ? ");
			if (nivel != null && sw == 0) {
				subWhere.append("  and nivel>54");
				sw = 1;
			}
			selectAndFom.append(subWhere.toString());
			selectAndFom.append(endSelect);

			preparedStatement = conn.prepareStatement(selectAndFom.toString());
			preparedStatement.setString(1, codigo);
			preparedStatement.setString(2, coleccion);

		} else if (!isEmpty(proyecto) && isEmpty(coleccion)) {
			subWhere = new StringBuffer();
			subWhere.append(" AND proyecto = ? ");
			if (nivel != null && sw == 0) {
				subWhere.append("  and nivel>54");
				sw = 1;
			}
			selectAndFom.append(subWhere.toString());
			selectAndFom.append(endSelect);

			preparedStatement = conn.prepareStatement(selectAndFom.toString());
			preparedStatement.setString(1, codigo);
			preparedStatement.setString(2, proyecto);

		} else {

			selectAndFom.append(endSelect);
			preparedStatement = conn.prepareStatement(selectAndFom.toString());
			preparedStatement.setString(1, codigo);

		}

		//System.out.println("Query: findLotesByGruposTaxEXCEL ... "+ selectAndFom.toString());

		ResultSet result = null;
		StringBuffer html = new StringBuffer();
		String colorSchema = null;

		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("Hoja de datos");
		HSSFRow row = null;
		int i = 1;
		int j = 0;
		try {

			result = preparedStatement.executeQuery();
			row = sheet.createRow((short) 0);

			createCell("PHYLUM", wb, row, (short) j++);
			createCell("CLASE", wb, row, (short) j++);
			createCell("ORDEN", wb, row, (short) j++);
			createCell("FAMILIA", wb, row, (short) j++);
			createCell("GENERO", wb, row, (short) j++);
			createCell("ESPECIE", wb, row, (short) j++);
			createCell("LOTES", wb, row, (short) j++);
			createCell("EJEMPLARES", wb, row, (short) j++);
			while (result.next()) {
				row = sheet.createRow((short) i);
				j = 0;
				createCell(isNull(result.getString("phylum")), wb, row,
						(short) j++);
				createCell(isNull(result.getString("clase")), wb, row,
						(short) j++);
				createCell(isNull(result.getString("orden")), wb, row,
						(short) j++);
				createCell(isNull(result.getString("familia")), wb, row,
						(short) j++);
				createCell(isNull(result.getString("genero")), wb, row,
						(short) j++);
				createCell(isNull(result.getString("especie")), wb, row,
						(short) j++);
				createCell(result.getInt("Lotes"), wb, row, (short) j++);
				createCell(result.getInt("ejemplares"), wb, row, (short) j++);

				i++;
			}

		} finally {
			if (result != null)
				result.close();
			if (preparedStatement != null)
				preparedStatement.close();

		}

		return wb;

	}

	public HSSFWorkbook  findLotesInvestigadorBySeccionEXCEL(Connection conn,
			String seccion, String coleccion) throws SQLException {
		
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("Hoja de datos");
		HSSFRow row = null;
		int i = 1;
		int j = 0;
		
		PreparedStatement preparedStatement = null;
		preparedStatement = conn
				.prepareStatement(EstadisticasXMLDAO.SQL_LOTESINVESTIGADOR);
		preparedStatement.setString(1, seccion.toUpperCase());
		preparedStatement.setString(2, coleccion);
		
		ResultSet result = null;
		
		try {

			result = preparedStatement.executeQuery();
			row = sheet.createRow((short) 0);

			createCell("INVESTIGADOR", wb, row, (short) j++);
			createCell("TAREA", wb, row, (short) j++);
			createCell("A�O", wb, row, (short) j++);
			createCell("LOTES", wb, row, (short) j++);
			
			while (result.next()) {
				row = sheet.createRow((short) i);
				j = 0;
				
				createCell(result.getString(1), wb, row,
						(short) j++);
				createCell(result.getString(2), wb, row,
						(short) j++);
				createCell((result.getString(3) == null ? "Sin Fecha" : result
						.getString(3)), wb, row,
						(short) j++);
				createCell(result.getString(4), wb, row,
						(short) j++);
				
				i++;

			}
			

		} finally {
			if (result != null)
				result.close();
			if (preparedStatement != null)
				preparedStatement.close();
			
		}

		return wb;

	}

	public HSSFWorkbook findLotesByInvestigadorEXCEL(Connection conn, String investigador,
			String coleccion) throws SQLException {

		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("Hoja de datos");
		HSSFRow row = null;
		int i = 1;
		int j = 0;
		
		PreparedStatement preparedStatement = null;
		preparedStatement = conn
				.prepareStatement(EstadisticasXMLDAO.SQL_LOTESBYINVEST);
		preparedStatement.setString(2, investigador);
		preparedStatement.setString(1, coleccion);
		
		ResultSet result = null;
		
		try {

			result = preparedStatement.executeQuery();
			row = sheet.createRow((short) 0);
			createCell("A�O", wb, row, (short) j++);
			createCell("SECCION", wb, row, (short) j++);
			createCell("TAREA", wb, row, (short) j++);
			
			createCell("LOTES", wb, row, (short) j++);
			
			while (result.next()) {
				
				row = sheet.createRow((short) i);
				j = 0;
				createCell((result.getString(4) == null ? "Sin Fecha" : result
						.getString(4)), wb, row,
						(short) j++);
				createCell(result.getString(1), wb, row,
						(short) j++);
				createCell(result.getString(3), wb, row,
						(short) j++);
				
				createCell(result.getString(5), wb, row,
						(short) j++);
				
				i++;
				
				

			}
			
		} finally {
			if (result != null)
				result.close();
			if (preparedStatement != null)
				preparedStatement.close();
			
		}

		return wb;

	}

	/**
	 * Creates a cell and aligns it a certain way.
	 * 
	 * @param wb
	 *            the workbook
	 * @param row
	 *            the row to create the cell in
	 * @param column
	 *            the column number to create the cell in
	 * @param align
	 *            the alignment for the cell.
	 */
	private static void createCell(String value, HSSFWorkbook wb, HSSFRow row,
			short column) {
		HSSFCell cell = row.createCell(column);
		cell.setCellValue(new HSSFRichTextString(value));
		/*
		 * HSSFCellStyle cellStyle = wb.createCellStyle();
		 * cellStyle.setAlignment(align); cell.setCellStyle(cellStyle);
		 */
	}

	/**
	 * Creates a cell and aligns it a certain way.
	 * 
	 * @param wb
	 *            the workbook
	 * @param row
	 *            the row to create the cell in
	 * @param column
	 *            the column number to create the cell in
	 * @param align
	 *            the alignment for the cell.
	 */
	private static void createCell(int value, HSSFWorkbook wb, HSSFRow row,
			short column/* , short align */) {
		HSSFCell cell = row.createCell(column);
		cell.setCellValue(value);
		/*
		 * HSSFCellStyle cellStyle = wb.createCellStyle();
		 * cellStyle.setAlignment(align); cell.setCellStyle(cellStyle);
		 */
	}

	private String isNull(String str) {
		String cadena = "";
		if (str != null) {
			cadena = str;
		}
		return cadena;
	}

	public boolean isEmpty(String str) {
		return str == null || str.length() == 0 || str.equals("null");
	}
}
