package co.org.invemar.catalogo.model;

public class Catalogo {

    private String clave;
    private String nroInvemar;
    private String ultimoCambio;
    private String coleccion;
    private String codColeccion;
    private String numCatalogo;
    private String especie;
    private String baseRegistro;
    private String reino;
    private String division;
    private String clase;
    private String orden;
    private String familia;
    private String genero;
    private String epiteto;
    private String autorEspecie;
    private String identificadoPor;
    private String anoIdentificacion;
    private String tipo;
    private String colectadoPor;
    private String anoCaptura;
    private String ContinentOcean;
    private String pais;
    private String departamento;
    private String localidad;
    private String longitud;
    private String latitud;
    private String profCaptura;
    private String ejemplares;
    private String proyecto;
    private String metadato;
    private Imagenes img;
    private String estacion;
    //nuevos campos agregados
    private String ubicacionGeo;
    private String autor;
    private String nivel;
    private String nombreComun;
    private String variedad;
    private String autorVariedad;
    private String fechaRecibido;
    private String tipoEspecimen;
    private String preservativo;
    //private String objeto;
    private String adquisicion;
    private String procedencia;
    private String colectadoPorCDG;
    private String fechaCaptura;
    private String metodoCaptura;
    private String municipio;
    private String cuerpoAgua;
    private String latitudInicial;
    private String latitudFinal;
    private String longitudInicial;
    private String longitudFinal;
    private String barco;
    private String profAgua;
    private String ambiente;
    private String temperaturaAire;
    private String temperaturaAgua;
    private String salinidad;
    private String publicado;
    private String fechaIdentificacion;
    private String fechaActualizo;
    private String fechaInicial;
    private String profundidadMin;
    private String profundidadMax;
    private String profundidadExacta;
    private String ecorregion;
    private String ecorregionCDG;
    private String region;
    private String tevPreservado;
    private String tevEjemplares;
    private String zona;
    private String uicnGlobal;
    private String uicnNacional;
    private String cites;
    private String notasMuestra;
    private String notasIdentificacion;
    private String notasCaptura;
    private String notasEstacion;
    private String notasCrucero;
    private String consecutivoMuestra;
    private String tipoCostaCon;
    private String olas;
    private String mareasCon;
    private String corrienteCon;
    private String transpCon;
    private String tiempo;
    private String notasCon;
    private String horaCon;
    private String ubicacion;
    private String CAMP;
    private String qualityflag;
    private String objetos;
    private String tecnica_ident;
    private String longfindecimal;
    private String latfindecimal;
   
    

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Catalogo() {
    }

    public void setAll(String clave, String ultimoCambioIn, String coleccionIn,
            String codColeccionIn, String numCatalogoIn,
            String especieIn, String baseRegistroIn, String reinoIn,
            String divisionIn, String claseIn, String ordenIn,
            String familiaIn, String generoIn, String epitetoIn,
            String autorEspecieIn, String identificadoPorIn,
            String anoIdentificacionIn, String tipoIn,
            String colectadoPorIn, String anoCapturaIn,
            String ContinentOceanIn, String paisIn,
            String departamentoIn, String localidadIn,
            String longitudIn, String latitudIn,
            String profCapturaIn,
            String ejemplaresIn,
            Imagenes img,
            String proyecto,
            String metadato,
            String ubicacionGeo,
            String autor,
            String nivel,
            String nombreComun,
            String variedad,
            String autorVariedad,
            String fechaRecibido,
            String tipoEspecimen,
            String preservativo,
            // String objeto,
            String adquisicion,
            String procedencia,
            String colectadoPorCDG,
            String fechaCaptura,
            String metodoCaptura,
            String municipio,
            String cuerpoAgua,
            String latitudInicial,
            String latitudFinal,
            String longitudInicial,
            String longitudFinal,
            String barco,
            String profAgua,
            String ambiente,
            String temperaturaAire,
            String temperaturaAgua,
            String salinidad,
            String publicado,
            String fechaIdentificacion,
            String fechaActualizo,
            String fechaInicial,
            String profundidadMin,
            String profundidadMax,
            String profundidadExacta,
            String ecorregion,
            String ecorregionCDG,
            String region,
            String tevPreservado,
            String tevEjemplares,
            String zona,
            String uicnGlobal,
            String uicnNacional,
            String cites,
            String notasMuestra,
            String notasIdentificacion,
            String notasCaptura,
            String notasEstacion,
            String notasCrucero,
            String consecutivoMuestra,
            String tipoCostaCon,
            String olas,
            String mareasCon,
            String corrienteCon,
            String transpCon,
            String tiempo,
            String notasCon,
            String horaCon,
            String CAMP,
            String nroInvemar,
            String qualityflag,
            String objetos,
            String tecnica_ident,
            String longfindecimal,
            String latfindecimal        
    ) {
        this.clave = clave;
        this.ultimoCambio = ultimoCambioIn;
        this.coleccion = coleccionIn;
        this.codColeccion = codColeccionIn;
        this.numCatalogo = numCatalogoIn;
        this.especie = especieIn;
        this.baseRegistro = baseRegistroIn;
        this.reino = reinoIn;
        this.division = divisionIn;
        this.clase = claseIn;
        this.orden = ordenIn;
        this.familia = familiaIn;
        this.genero = generoIn;
        this.epiteto = epitetoIn;
        this.autorEspecie = autorEspecieIn;
        this.identificadoPor = identificadoPorIn;
        this.anoIdentificacion = anoIdentificacionIn;
        this.tipo = tipoIn;
        this.colectadoPor = colectadoPorIn;
        this.anoCaptura = anoCapturaIn;
        this.ContinentOcean = ContinentOceanIn;
        this.pais = paisIn;
        this.departamento = departamentoIn;
        this.localidad = localidadIn;
        this.longitud = longitudIn;
        this.latitud = latitudIn;
        this.profCaptura = profCapturaIn;
        this.ejemplares = ejemplaresIn;
        this.img = img;
        this.setProyecto(proyecto);
        this.setMetadato(metadato);

        this.ubicacionGeo = ubicacionGeo;
        this.autor = autor;
        this.nivel = nivel;
        this.nombreComun = nombreComun;
        this.variedad = variedad;
        this.autorVariedad = autorVariedad;
        this.fechaRecibido = fechaRecibido;
        this.tipoEspecimen = tipoEspecimen;
        this.preservativo = preservativo;
        // this.objeto=
        this.adquisicion = adquisicion;
        this.procedencia = procedencia;
        this.colectadoPorCDG = colectadoPorCDG;
        this.fechaCaptura = fechaCaptura;
        this.metodoCaptura = metodoCaptura;
        this.municipio = municipio;
        this.cuerpoAgua = cuerpoAgua;
        this.latitudInicial = latitudInicial;
        this.latitudFinal = latitudFinal;
        this.longitudInicial = longitudInicial;
        this.longitudFinal = longitudFinal;
        this.barco = barco;
        this.profAgua = profAgua;
        this.ambiente = ambiente;
        this.temperaturaAire = temperaturaAire;
        this.temperaturaAgua = temperaturaAgua;
        this.salinidad = salinidad;
        this.publicado = publicado;
        this.fechaIdentificacion = fechaIdentificacion;
        this.fechaActualizo = fechaActualizo;
        this.fechaInicial = fechaInicial;
        this.profundidadMin = profundidadMin;
        this.profundidadMax = profundidadMax;
        this.profundidadExacta = profundidadExacta;
        this.ecorregion = ecorregion;
        this.ecorregionCDG = ecorregionCDG;
        this.region = region;
        this.tevPreservado = tevPreservado;
        this.tevEjemplares = tevEjemplares;
        this.zona = zona;
        this.uicnGlobal = uicnGlobal;
        this.uicnNacional = uicnNacional;
        this.cites = cites;
        this.notasMuestra = notasMuestra;
        this.notasIdentificacion = notasIdentificacion;
        this.notasCaptura = notasCaptura;
        this.notasEstacion = notasEstacion;
        this.notasCrucero = notasCrucero;
        this.consecutivoMuestra = consecutivoMuestra;
        this.tipoCostaCon = tipoCostaCon;
        this.olas = olas;
        this.mareasCon = mareasCon;
        this.corrienteCon = corrienteCon;
        this.transpCon = transpCon;
        this.tiempo = tiempo;
        this.notasCon = notasCon;
        this.horaCon = horaCon;
        this.CAMP =CAMP;
        this.nroInvemar=nroInvemar;
        this.qualityflag=qualityflag;
        this.objetos=objetos;
        this.tecnica_ident=tecnica_ident;
        this.longfindecimal=longfindecimal;
        this.latfindecimal = latfindecimal;
        

    }

    public void setUltimoCambio(String ultimoCambio) {
        this.ultimoCambio = ultimoCambio;
    }

    public String getUltimoCambio() {
        return ultimoCambio;
    }

    public void setColeccion(String coleccion) {
        this.coleccion = coleccion;
    }

    public String getColeccion() {
        return coleccion;
    }

    public void setCodColeccion(String codColeccion) {
        this.codColeccion = codColeccion;
    }

    public String getCodColeccion() {
        return codColeccion;
    }

    public void setNumCatalogo(String numCatalogo) {
        this.numCatalogo = numCatalogo;
    }

    public String getNumCatalogo() {
        return numCatalogo;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getEspecie() {
        return especie;
    }

    public void setBaseRegistro(String baseRegistro) {
        this.baseRegistro = baseRegistro;
    }

    public String getBaseRegistro() {
        return baseRegistro;
    }

    public void setReino(String reino) {
        this.reino = reino;
    }

    public String getReino() {
        return reino;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getDivision() {
        return division;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getClase() {
        return clase;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public String getOrden() {
        return orden;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getFamilia() {
        return familia;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getGenero() {
        return genero;
    }

    public void setEpiteto(String epiteto) {
        this.epiteto = epiteto;
    }

    public String getEpiteto() {
        return epiteto;
    }

    public void setAutorEspecie(String autorEspecie) {
        this.autorEspecie = autorEspecie;
    }

    public String getAutorEspecie() {
        return autorEspecie;
    }

    public void setIdPor(String idPor) {
        this.identificadoPor = idPor;
    }

    public String getIdPor() {
        return identificadoPor;
    }

    public void setAnoIdentificacion(String anoIdentificacion) {
        this.anoIdentificacion = anoIdentificacion;
    }

    public String getAnoIdentificacion() {
        return anoIdentificacion;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setColectadoPor(String colectadoPor) {
        this.colectadoPor = colectadoPor;
    }

    public String getColectadoPor() {
        return colectadoPor;
    }

    public void setAnoCaptura(String anoCaptura) {
        this.anoCaptura = anoCaptura;
    }

    public String getAnoCaptura() {
        return anoCaptura;
    }

    public void setContinentOcean(String continentOcean) {
        this.ContinentOcean = continentOcean;
    }

    public String getContinentOcean() {
        return ContinentOcean;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getPais() {
        return pais;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setProfCaptura(String profCaptura) {
        this.profCaptura = profCaptura;
    }

    public String getProfCaptura() {
        return profCaptura;
    }

    public void setIdentificadoPor(String identificadoPor) {
        this.identificadoPor = identificadoPor;
    }

    public String getIdentificadoPor() {
        return identificadoPor;
    }

    public void setEjemplares(String ejemplares) {
        this.ejemplares = ejemplares;
    }

    public String getEjemplares() {
        return ejemplares;
    }

    public String toString() {
        StringBuffer out;
        out = new StringBuffer("");

        out.append(this.ultimoCambio + "\t");
        out.append(this.coleccion + "\t");
        out.append(this.codColeccion + "\t");
        out.append(this.numCatalogo + "\t");
        out.append(this.proyecto + "\t");
        out.append(this.estacion + "\t");
        out.append(this.especie + "\t");
        out.append(this.baseRegistro + "\t");
        out.append(this.reino + "\t");
        out.append(this.division + "\t");
        out.append(this.clase + "\t");
        out.append(this.orden + "\t");
        out.append(this.familia + "\t");
        out.append(this.genero + "\t");
        out.append(this.epiteto + "\t");
        out.append(this.autorEspecie + "\t");
        out.append(this.identificadoPor + "\t");
        out.append(this.anoIdentificacion + "\t");
        out.append(this.tipo + "\t");
        out.append(this.colectadoPor + "\t");
        out.append(this.anoCaptura + "\t");
        out.append(this.ContinentOcean + "\t");
        out.append(this.pais + "\t");
        out.append(this.departamento + "\t");
        out.append(this.localidad + "\t");
        out.append(this.longitud + "\t");
        out.append(this.latitud + "\t");
        out.append(this.profCaptura + "\t");
        out.append(this.ejemplares + "\t");
        //out.append(this.img.getArchivoIm() + "\n");
        return out.toString();
    }

    /* public String toString() {
     StringBuffer out;
     out = new StringBuffer("");
     out.append("Persistent attributes: \n");
     out.append("ultimoCambio = " + this.ultimoCambio + "\n");
     out.append("coleccion = " + this.coleccion + "\n");
     out.append("codColeccion = " + this.codColeccion + "\n");
     out.append("numCatalogo = " + this.numCatalogo + "\n");
     out.append("especie = " + this.especie + "\n");
     out.append("baseRegistro = " + this.baseRegistro + "\n");
     out.append("reino = " + this.reino + "\n");
     out.append("division = " + this.division + "\n");
     out.append("clase = " + this.clase + "\n");
     out.append("orden = " + this.orden + "\n");
     out.append("familia = " + this.familia + "\n");
     out.append("genero = " + this.genero + "\n");
     out.append("epiteto = " + this.epiteto + "\n");
     out.append("autorEspecie = " + this.autorEspecie + "\n");
     out.append("identificadoPor = " + this.identificadoPor + "\n");
     out.append("anoIdentificacion = " + this.anoIdentificacion + "\n");
     out.append("tipo = " + this.tipo + "\n");
     out.append("colectadoPor = " + this.colectadoPor + "\n");
     out.append("anoCaptura = " + this.anoCaptura + "\n");
     out.append("ContinentOcean = " + this.ContinentOcean + "\n");
     out.append("pais = " + this.pais + "\n");
     out.append("departamento = " + this.departamento + "\n");
     out.append("localidad = " + this.localidad + "\n");
     out.append("longitud = " + this.longitud + "\n");
     out.append("latitud = " + this.latitud + "\n");
     out.append("profCaptura = " + this.profCaptura + "\n");
     out.append("ejemplares = " + this.ejemplares + "\n");
     out.append("IMAGENES = " + this.img.getArchivoIm() + "\n");
     return out.toString();
     }    
     */
    public void setImg(Imagenes img) {
        this.img = img;
    }

    public Imagenes getImg() {
        return img;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }

    public void setNroInvemar(String nroInvemar) {
        this.nroInvemar = nroInvemar;
    }

    public String getNroInvemar() {
        return nroInvemar;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public String getMetadato() {
        return metadato;
    }

    public void setMetadato(String metadato) {
        this.metadato = metadato;
    }

    /**
     * @return the estacion
     */
    public String getEstacion() {
        return estacion;
    }

    /**
     * @param estacion the estacion to set
     */
    public void setEstacion(String estacion) {
        this.estacion = estacion;
    }

    public String getUbicacionGeo() {
        return ubicacionGeo;
    }

    public void setUbicacionGeo(String ubicacionGeo) {
        this.ubicacionGeo = ubicacionGeo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getVariedad() {
        return variedad;
    }

    public void setVariedad(String variedad) {
        this.variedad = variedad;
    }

    public String getAutorVariedad() {
        return autorVariedad;
    }

    public void setAutorVariedad(String autorVariedad) {
        this.autorVariedad = autorVariedad;
    }

    public String getFechaRecibido() {
        return fechaRecibido;
    }

    public void setFechaRecibido(String fechaRecibido) {
        this.fechaRecibido = fechaRecibido;
    }

    public String getTipoEspecimen() {
        return tipoEspecimen;
    }

    public void setTipoEspecimen(String tipoEspecimen) {
        this.tipoEspecimen = tipoEspecimen;
    }

    public String getPreservativo() {
        return preservativo;
    }

    public void setPreservativo(String preservativo) {
        this.preservativo = preservativo;
    }

    public String getAdquisicion() {
        return adquisicion;
    }

    public void setAdquisicion(String adquisicion) {
        this.adquisicion = adquisicion;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getColectadoPorCDG() {
        return colectadoPorCDG;
    }

    public void setColectadoPorCDG(String colectadoPorCDG) {
        this.colectadoPorCDG = colectadoPorCDG;
    }

    public String getFechaCaptura() {
        return fechaCaptura;
    }

    public void setFechaCaptura(String fechaCaptura) {
        this.fechaCaptura = fechaCaptura;
    }

    public String getMetodoCaptura() {
        return metodoCaptura;
    }

    public void setMetodoCaptura(String metodoCaptura) {
        this.metodoCaptura = metodoCaptura;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getCuerpoAgua() {
        return cuerpoAgua;
    }

    public void setCuerpoAgua(String cuerpoAgua) {
        this.cuerpoAgua = cuerpoAgua;
    }

    public String getLatitudInicial() {
        return latitudInicial;
    }

    public void setLatitudInicial(String latitudInicial) {
        this.latitudInicial = latitudInicial;
    }

    public String getLatitudFinal() {
        return latitudFinal;
    }

    public void setLatitudFinal(String latitudFinal) {
        this.latitudFinal = latitudFinal;
    }

    public String getLongitudInicial() {
        return longitudInicial;
    }

    public void setLongitudInicial(String longitudInicial) {
        this.longitudInicial = longitudInicial;
    }

    public String getLongitudFinal() {
        return longitudFinal;
    }

    public void setLongitudFinal(String longitudFinal) {
        this.longitudFinal = longitudFinal;
    }

    public String getBarco() {
        return barco;
    }

    public void setBarco(String barco) {
        this.barco = barco;
    }

    public String getProfAgua() {
        return profAgua;
    }

    public void setProfAgua(String profAgua) {
        this.profAgua = profAgua;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String getTemperaturaAire() {
        return temperaturaAire;
    }

    public void setTemperaturaAire(String temperaturaAire) {
        this.temperaturaAire = temperaturaAire;
    }

    public String getTemperaturaAgua() {
        return temperaturaAgua;
    }

    public void setTemperaturaAgua(String temperaturaAgua) {
        this.temperaturaAgua = temperaturaAgua;
    }

    public String getSalinidad() {
        return salinidad;
    }

    public void setSalinidad(String salinidad) {
        this.salinidad = salinidad;
    }

    public String getPublicado() {
        return publicado;
    }

    public void setPublicado(String publicado) {
        this.publicado = publicado;
    }

    public String getFechaIdentificacion() {
        return fechaIdentificacion;
    }

    public void setFechaIdentificacion(String fechaIdentificacion) {
        this.fechaIdentificacion = fechaIdentificacion;
    }

    public String getFechaActualizo() {
        return fechaActualizo;
    }

    public void setFechaActualizo(String fechaActualizo) {
        this.fechaActualizo = fechaActualizo;
    }

    public String getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public String getProfundidadMin() {
        return profundidadMin;
    }

    public void setProfundidadMin(String profundidadMin) {
        this.profundidadMin = profundidadMin;
    }

    public String getProfundidadMax() {
        return profundidadMax;
    }

    public void setProfundidadMax(String profundidadMax) {
        this.profundidadMax = profundidadMax;
    }

    public String getProfundidadExacta() {
        return profundidadExacta;
    }

    public void setProfundidadExacta(String profundidadExacta) {
        this.profundidadExacta = profundidadExacta;
    }

    public String getEcorregion() {
        return ecorregion;
    }

    public void setEcorregion(String ecorregion) {
        this.ecorregion = ecorregion;
    }

    public String getEcorregionCDG() {
        return ecorregionCDG;
    }

    public void setEcorregionCDG(String ecorregionCDG) {
        this.ecorregionCDG = ecorregionCDG;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getTevPreservado() {
        return tevPreservado;
    }

    public void setTevPreservado(String tevPreservado) {
        this.tevPreservado = tevPreservado;
    }

    public String getTevEjemplares() {
        return tevEjemplares;
    }

    public void setTevEjemplares(String tevEjemplares) {
        this.tevEjemplares = tevEjemplares;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getUicnGlobal() {
        return uicnGlobal;
    }

    public void setUicnGlobal(String uicnGlobal) {
        this.uicnGlobal = uicnGlobal;
    }

    public String getUicnNacional() {
        return uicnNacional;
    }

    public void setUicnNacional(String uicnNacional) {
        this.uicnNacional = uicnNacional;
    }

    public String getCites() {
        return cites;
    }

    public void setCites(String cites) {
        this.cites = cites;
    }

    public String getNotasMuestra() {
        return notasMuestra;
    }

    public void setNotasMuestra(String notasMuestra) {
        this.notasMuestra = notasMuestra;
    }

    public String getNotasCaptura() {
        return notasCaptura;
    }

    public void setNotasCaptura(String notasCaptura) {
        this.notasCaptura = notasCaptura;
    }

    public String getNotasEstacion() {
        return notasEstacion;
    }

    public void setNotasEstacion(String notasEstacion) {
        this.notasEstacion = notasEstacion;
    }

    public String getNotasCrucero() {
        return notasCrucero;
    }

    public void setNotasCrucero(String notasCrucero) {
        this.notasCrucero = notasCrucero;
    }

    public String getConsecutivoMuestra() {
        return consecutivoMuestra;
    }

    public void setConsecutivoMuestra(String consecutivoMuestra) {
        this.consecutivoMuestra = consecutivoMuestra;
    }

    public String getTipoCostaCon() {
        return tipoCostaCon;
    }

    public void setTipoCostaCon(String tipoCostaCon) {
        this.tipoCostaCon = tipoCostaCon;
    }

    public String getOlas() {
        return olas;
    }

    public void setOlas(String olas) {
        this.olas = olas;
    }

    public String getMareasCon() {
        return mareasCon;
    }

    public void setMareasCon(String mareasCon) {
        this.mareasCon = mareasCon;
    }

    public String getCorrienteCon() {
        return corrienteCon;
    }

    public void setCorrienteCon(String corrienteCon) {
        this.corrienteCon = corrienteCon;
    }

    public String getTranspCon() {
        return transpCon;
    }

    public void setTranspCon(String transpCon) {
        this.transpCon = transpCon;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getNotasCon() {
        return notasCon;
    }

    public void setNotasCon(String notasCon) {
        this.notasCon = notasCon;
    }

    public String getHoraCon() {
        return horaCon;
    }

    public void setHoraCon(String horaCon) {
        this.horaCon = horaCon;
    }

    public String getNotasIdentificacion() {
        return notasIdentificacion;
    }

    public void setNotasIdentificacion(String notasIdentificacion) {
        this.notasIdentificacion = notasIdentificacion;
    }

    public String getCAMP() {
        return CAMP;
    }

    public void setCAMP(String CAMP) {
        this.CAMP = CAMP;
    }

    public String getQualityflag() {
        return qualityflag;
    }

    public void setQualityflag(String qualityflag) {
        this.qualityflag = qualityflag;
    }

    public String getObjetos() {
        return objetos;
    }

    public void setObjetos(String objetos) {
        this.objetos = objetos;
    }

    public String getTecnica_ident() {
        return tecnica_ident;
    }

    public void setTecnica_ident(String tecnica_ident) {
        this.tecnica_ident = tecnica_ident;
    }

    public String getLongfindecimal() {
        return longfindecimal;
    }

    public void setLongfindecimal(String longfindecimal) {
        this.longfindecimal = longfindecimal;
    }

    public String getLatfindecimal() {
        return latfindecimal;
    }

    public void setLatfindecimal(String latfindecimal) {
        this.latfindecimal = latfindecimal;
    }
}
