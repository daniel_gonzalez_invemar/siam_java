package co.org.invemar.catalogo.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

public class EstadisticasXMLDAO {

	/************************* FILL COLECCIONES ****************************************/
	static final String SQL_FILLCOLECCIONES="select codigo_in from cdirinstituto where naturaleza_in = 317 ";
	
	/**********************************************************************************/
	/******************************* FILL LIST PHYLUMS *******************************/
	static final String SQL_FILLPHYLUMS="SELECT DISTINCT descripcion_ph "+
										" FROM cphylum "+
										" WHERE usuario_ph IS NOT NULL OR grupodescripcion_ph IS NOT NULL ";
	
    /******************************** SECCION-FECHA RECIBIDO ****************************************/
   //Numero de lotes por secci�n - fecha recibido   
    static final String SQL_SECCIONESMU =
        "select distinct seccion_mu from curador.cmuestra where instituto_mu = ? order by 1 asc";
    
    static final String SQL_FECHARECIBIDO =
        "select to_char(fecha_mu, 'YYYY') ANO, count(1) Lotes\n" +
        "from curador.cmuestra\n" + "where seccion_mu=? and instituto_mu =? \n" +
        "group by seccion_mu, to_char(fecha_mu, 'YYYY')\n" +
        "order by ano desc\n";
    /***********************************************************************************************/

    /**************************** SECCION-FECHA INDENTIFICACION *****************************/
   //Numero de lotes por secci�n - fecha identificacion   
    static final String SQL_SECCIONMUFECHAIDEN =
        "select distinct m.seccion_mu\n" +
        "from  cmuestra m, cmresponsables_iden r\n" +
        "where m.NROINVEMAR_MU=r.NROINVEMAR_RE(+)\n" +
        "and   nvl(orden_re,1) = 1 and instituto_mu = ? order by 1 asc ";

    static final String SQL_FECHAINDENT =
        "select (to_char(r.fecha_re, 'yyyy')) ano, count(*) Lotes\n" +
        "from  cmuestra m, cmresponsables_iden r\n" +
        "where m.NROINVEMAR_MU=r.NROINVEMAR_RE(+)\n" +
        "and   nvl(orden_re,1) = 1 and m.seccion_mu=? and instituto_mu =? \n" +
        "group by m.seccion_mu, to_char(r.fecha_re, 'yyyy') order by 1 desc";

    static final String SQL_LOTESFECHAIDENT =
        "select count(*) Lotes\n" + "from  cmuestra m, cmresponsables_iden r\n" +
        "where m.NROINVEMAR_MU=r.NROINVEMAR_RE(+)\n" +
        "and   nvl(orden_re,1) = 1 and to_char(r.fecha_re, 'yyyy')=? and m.seccion_mu=? and instituto_mu =? \n" +
        "group by m.seccion_mu, to_char(r.fecha_re, 'yyyy')";

    static final String SQL_LOTESFECHAIDENTNULL =
        "select count(*) Lotes\n" + "from  cmuestra m, cmresponsables_iden r\n" +
        "where m.NROINVEMAR_MU=r.NROINVEMAR_RE(+)\n" +
        "and   nvl(orden_re,1) = 1 and to_char(r.fecha_re, 'yyyy') is null and m.seccion_mu=? and instituto_mu =? \n" +
        "group by m.seccion_mu, to_char(r.fecha_re, 'yyyy')";

    static final String SQL_LOTESIDENTABYSECCION =
        "select (to_char(r.fecha_re, 'yyyy')) ano, count(*) Lotes\n" +
        "from  cmuestra m, cmresponsables_iden r\n" +
        "where m.NROINVEMAR_MU=r.NROINVEMAR_RE(+)\n" +
        "and   nvl(orden_re,1) = 1 and m.seccion_mu=? and instituto_mu = ? \n" +
        "group by m.seccion_mu, to_char(r.fecha_re, 'yyyy') order by 1 desc";

    /***********************************************************************************************/

    /**************************************** SECCCION FECHA CAPTURA ***********************************/
   //Numero de lotes por secci�n - fecha de captura   
    static final String SQL_SECCIONFECHACAP =
        "select distinct m.seccion_mu\n" + "from  cmuestra m, cmresponsables_capt r\n" +
        "where m.NROINVEMAR_MU=r.NROINVEMAR_RE(+)\n" +
        "and   nvl(orden_re,1) = 1 and instituto_mu =? " +
        "order by 1 asc";

    static final String SQL_FECHACAPTURA =
        "select to_char(r.fecha_re, 'yyyy') ano\n" +
        "from  cmuestra m, cmresponsables_capt r\n" +
        "where m.NROINVEMAR_MU=r.NROINVEMAR_RE(+)\n" +
        "and   nvl(orden_re,1) = 1 and m.SECCION_MU=? and instituto_mu =? \n" +
        "group by m.seccion_mu, to_char(r.fecha_re, 'yyyy') " +
        "order by 1 desc";

    static final String SQL_LOTESFECHACAPTURA =
        "select count(*) Lotes\n" + "from  cmuestra m, cmresponsables_capt r\n" +
        "where m.NROINVEMAR_MU=r.NROINVEMAR_RE(+)\n" +
        "and   nvl(orden_re,1) = 1 and m.seccion_mu=?  and instituto_mu =?  and to_char(r.fecha_re, 'yyyy')=? \n" +
        "group by m.seccion_mu, to_char(r.fecha_re, 'yyyy') ";

    static final String SQL_LOTESFECHACAPTURAISNULL =
        "select count(*) Lotes\n" +
        "from  cmuestra m, cmresponsables_capt r\n" +
        "where m.NROINVEMAR_MU=r.NROINVEMAR_RE(+)\n" +
        "and   nvl(orden_re,1) = 1 and m.seccion_mu=? and instituto_mu =? and to_char(r.fecha_re, 'yyyy')is null \n" +
        "group by m.seccion_mu, to_char(r.fecha_re, 'yyyy')";

    static final String SQL_LOTESCAPTURABYSECCION =
        "select to_char(r.fecha_re, 'yyyy') ano, count(*) Lotes\n" +
        "from  cmuestra m, cmresponsables_capt r\n" +
        "where m.NROINVEMAR_MU=r.NROINVEMAR_RE(+) and m.seccion_mu=? and instituto_mu =? \n" +
        "and   nvl(orden_re,1) = 1\n" +
        "group by m.seccion_mu, to_char(r.fecha_re, 'yyyy') " +
        "order by 1 desc";
    /***************************************************************************************************/

    /****************************** SECCION-IMAGNES******************************************************/
     //Numero de lotes por secci�n - con imagen y sin imagen  
    static final String SQL_SECCIONIMAGES =
        "select seccion_mu, count(7) \"sin Imagenes\", count(nroinvemar_im) \"Con Imagenes\" " +
        "from curador.cmuestra, curador.cimagenes\n" +
        "where nroinvemar_mu = nroinvemar_im(+)\n" +
        "and   nvl(nroimagen_im,1) = 1 and instituto_mu =? \n" + 
        "group by seccion_mu " +
        "order by 1 asc";

    /***************************************************************************************************/

    /************************************* SECCION PORYECTO ***********************************************/
    static final String SQL_SECCIONPROYECTOS =
        "select distinct seccion\n" + "from vm_fichac\n" + "order by 1\n";

    static final String SQL_LOTESPROYECTOS =
        "select proyecto, count(1)\n" + "from vm_fichac\n" +
        "where seccion=?\n" + "group by seccion, proyecto\n" + "order by 1";


    /********************************************************************************************************/

    /********************************** SECCION-PRESERVATIVO ************************************************/
    static final String SQL_SECCIONPRES =
        "select distinct m.seccion_mu\n" + "from cmuestra m, codigoslov c\n" +
        "where m.preservativo_mu =c.codigo_lov and tabla_lov=6 and instituto_mu =? \n" +
        "group by m.seccion_mu, c.descripcion_lov";


    static final String SQL_LOTESPRESERVATIVO =
        "select  c.descripcion_lov Preservativo, count(1) Lotes \n" +
        "from cmuestra m, codigoslov c\n" +
        "where m.preservativo_mu =c.codigo_lov and tabla_lov=6 and m.seccion_mu=? and instituto_mu =? \n" +
        "group by m.seccion_mu, c.descripcion_lov";

    /********************************************************************************************************/

    /************************************ SECCION- OBJESTOS*************************************************/
    static final String SQL_SECCIONOBJETOS =
        "select distinct m.seccion_mu \n" + "from cmuestra m, cmobjetos o, codigoslov c\n" +
        "where m.nroinvemar_mu=o.nroinvemar_ob \n" +
        "and o.objeto_ob=c.codigo_lov and c.tabla_lov=5 and instituto_mu =?";

    static final String SQL_LOTESOBJESTOS =
        "select c.descripcion_lov \"Objeto\", count(1) \"Lotes\"\n" +
        "from cmuestra m, cmobjetos o, codigoslov c\n" +
        "where m.nroinvemar_mu=o.nroinvemar_ob \n" +
        "and o.objeto_ob=c.codigo_lov and c.tabla_lov=5 and m.seccion_mu= ? and instituto_mu =?\n" +
        "group by m.seccion_mu, c.descripcion_lov";

    /*******************************************************************************************************/

    /************************************* SECCION - INVESTIGADOR********************************************/
     //Numero de lotes por secci�n, investigador y tarea   
    static final String SQL_SECCIONINVESTIGADOR =
        "select distinct m.seccion_mu\n" + "from cmuestra m, cmresponsables r, cdircolector d, codigoslov c\n" +
        "where m.nroinvemar_mu=r.NROINVEMAR_RE \n" +
        "and r.codigo_co=d.codigo_co\n" + "and r.tarea_re=c.CODIGO_LOV\n" +
        "and tabla_lov=12 and instituto_mu =? \n" + "order by 1";

    static final String SQL_LOTESINVESTIGADOR ="select nvl2(d.nombre_co,d.nombre_co || ' ' || d.apellidos_co,d.apellidos_co) Investigador, c.descripcion_lov Tarea,to_char(r.fecha_re, 'yyyy') ano, count(1) Lotes\n" + 
    "from cmuestra m, cmresponsables r, cdircolector d, codigoslov c\n" + 
    "where m.nroinvemar_mu=r.NROINVEMAR_RE \n" + 
    "and r.codigo_co=d.codigo_co and r.tarea_re=c.CODIGO_LOV\n" + 
    "and tabla_lov=12 and seccion_mu=? and instituto_mu =? \n" + 
    "group by m.seccion_mu, nvl2(d.nombre_co,d.nombre_co || ' ' || d.apellidos_co,d.apellidos_co) ,\n" + 
    "c.descripcion_lov, to_char(r.fecha_re, 'yyyy')\n" + 
    "order by 1";
    
    //Numero de lotes por investigador - a�o  
    static final String SQL_INVESTIGADORES="select distinct d.nombre_co || ' ' || d.apellidos_co\n" + 
    "from cmuestra m, cmresponsables r, cdircolector d, codigoslov c\n" + 
    "where m.nroinvemar_mu=r.NROINVEMAR_RE \n" + 
    "and r.codigo_co=d.codigo_co\n" + 
    "and r.tarea_re=c.CODIGO_LOV\n" + 
    "and tabla_lov=12 and instituto_mu =? \n" + 
    "order by 1";
    
    static final String SQL_LOTESBYINVEST="select m.seccion_mu, d.nombre_co || ' ' || d.apellidos_co \"Investigador\", c.descripcion_lov \"Tarea\",to_char(r.fecha_re, 'yyyy') ano, count(1) \"Lotes\"\n" + 
    "from cmuestra m, cmresponsables r, cdircolector d, codigoslov c\n" + 
    "where m.nroinvemar_mu=r.NROINVEMAR_RE \n" + 
    "and instituto_mu =? and r.codigo_co=d.codigo_co\n" + 
    "and r.tarea_re=c.CODIGO_LOV\n" + 
    "and tabla_lov=12 \n" + 
    "and (d.nombre_co || ' ' || d.apellidos_co)=? \n" + 
    "group by m.seccion_mu, d.nombre_co || ' ' || d.apellidos_co , c.descripcion_lov, to_char(r.fecha_re, 'yyyy')\n" + 
    "order by 1";

    /************************************************************************************************************/
    static final String SQL_PHYLUMS =
        "select  ph.CODIGO_PH, ph.DESCRIPCION_PH from cphylum ph WHERE usuario_ph IS NOT NULL OR grupodescripcion_ph IS NOT NULL  order by ph.DESCRIPCION_PH";
    //Lotes en el MHNMC por phylum  
    static final String SQL_MUESTRASBYPHYLUM =
        "SELECT Phylum, Nro_registros Muestras, round(RATIO_TO_REPORT(Nro_registros) OVER ()* 100,0) as Porcentaje " +
        "FROM  (select p.descripcion_ph as Phylum, phylum_mu, count(*) as Nro_Registros " +
        "from cmuestra, cphylum p where codigo_ph = phylum_mu and instituto_mu =? group by p.descripcion_ph, phylum_mu) " +
        "ORDER  BY Phylum asc\n";

 
    //Lotes en museo identificados hasta el nivel de especie o inferior por nivel por phylum   
    static final String SQL_LOTESINDENTHASTANIVEL =
        "select N1.NIVEL_TX COD_NIVEL, NT.NIVEL NIVEL," +
        "DECODE(NT.NIV,'46',INITCAP(N1.DESCRIPCION_TX)||' Sp.','49',initcap(NT.GENERO)||' '||N1.DESCRIPCION_TX||' Sp.','55'," +
        "decode(NT.NIVUP,'46',INITCAP(NT.GENERO)||' '||N1.DESCRIPCION_TX,'49',INITCAP(NT.GENERO)||' '||LOWER(NT.SUBGENERO)||' '||N1.DESCRIPCION_TX,null,null),'58'," +
        "INITCAP(NT.GENERO)||' '||NT.SUBGENERO||' '||NT.ESPECIE||' '||N1.DESCRIPCION_TX, N1.DESCRIPCION_TX) nombre, " +
        "sum(N1.MUESTRAS) LOTES " +
        "from (select N2.clave_txg, N2.nivel_tx, N2.descripcion_tx, ( select sum(individuos) \n" +
        "from ( select clave_tx clavetx, padre, hijo, clave_mu, individuos " +
        "from CTAXONESS_AND_PADRES,(select clave_mu, count(clave_mu) as individuos " +
        "from cmuestra where phylum_mu = ? group by clave_mu) " +
        "where clave_tx = clave_mu(+)) connect by prior hijo=padre start with clavetx = N2.clave_txg ) MUESTRAS " +
        "from(select clave_tx clave_txg, padre, hijo, clave_mu, nivel_tx, descripcion_tx, individuos " +
        "from CTAXONESS_AND_PADRES,(select clave_mu, count(clave_mu) as individuos " +
        "from cmuestra where phylum_mu = ? group by clave_mu) " +
        "where clave_tx = clave_mu(+) ) N2 " +
        "connect by prior hijo=padre start with clave_txg = ?) N1, vm_planilla NT \n" +
        "where N1.muestras is not null and NT.NIV = N1.Nivel_tx and NT.CLAVE = N1.CLAVE_TXG " +
        "group by N1.NIVEL_TX, NT.NIVEL," +
        "DECODE(NT.NIV,'46',INITCAP(N1.DESCRIPCION_TX)||' Sp.','49',initcap(NT.GENERO)||' '||N1.DESCRIPCION_TX||' Sp.','55'," +
        "decode(NT.NIVUP,'46',INITCAP(NT.GENERO)||' '||N1.DESCRIPCION_TX,'49',INITCAP(NT.GENERO)||' '||LOWER(NT.SUBGENERO)||' '||N1.DESCRIPCION_TX,null,null),'58'," +
        "INITCAP(NT.GENERO)||' '||NT.SUBGENERO||' '||NT.ESPECIE||' '||N1.DESCRIPCION_TX, N1.DESCRIPCION_TX) order by 1";


    //Lotes en museo por nivel por phylum  
    static final String SQL_LOTESMUSEOBYNIVELPHY =
        "select N1.NIVEL_TX COD_NIVEL, NT.descripcion_ta NIVEL, sum(N1.MUESTRAS) LOTES, count(nivel_tx) TOTAL_REPRESENTADO " +
        "from (select N2.clave_txg, N2.nivel_tx, N2.descripcion_tx, (select sum(individuos) " +
        "from ( select clave_tx clavetx, padre, hijo, clave_mu, individuos " +
        "from CTAXONESS_AND_PADRES,(select clave_mu, count(clave_mu) as individuos " +
        "from cmuestra where phylum_mu = ? group by clave_mu) " +
        "where clave_tx = clave_mu(+)) connect by prior hijo=padre start with clavetx = N2.clave_txg ) MUESTRAS " +
        "from(	select clave_tx clave_txg, padre, hijo, clave_mu, nivel_tx, descripcion_tx, individuos " +
        "from CTAXONESS_AND_PADRES,(select clave_mu, count(clave_mu) as individuos " +
        "from cmuestra where phylum_mu = ? group by clave_mu) " +
        "where clave_tx = clave_mu(+) ) N2 connect by prior hijo=padre start with clave_txg = ?) N1, cnivelestax NT " +
        "where N1.muestras is not null and NT.nivel_ta = N1.Nivel_tx group by N1.NIVEL_TX, NT.descripcion_ta " +
        "order by 1";

    
    //Especimenes en museo con nombre por nivel por phylum  
    static final String SQL_ESPECIMENESENMUSEO =
        "select N1.NIVEL_TX COD_NIVEL, NT.NIVEL NIVEL," +
        "DECODE(NT.NIV,'46',INITCAP(N1.DESCRIPCION_TX)||' Sp.','49',initcap(NT.GENERO)||' '||N1.DESCRIPCION_TX||' Sp.','55'," +
        "decode(NT.NIVUP,'46',INITCAP(NT.GENERO)||' '||N1.DESCRIPCION_TX,'49',INITCAP(NT.GENERO)||' '||LOWER(NT.SUBGENERO)||' '||N1.DESCRIPCION_TX,null,null),'58',INITCAP(NT.GENERO)||' '||NT.SUBGENERO||' '||NT.ESPECIE||' '||N1.DESCRIPCION_TX, N1.DESCRIPCION_TX) nombre, sum(N1.MUESTRAS) LOTES from (select N2.clave_txg, N2.nivel_tx, N2.descripcion_tx, ( select sum(individuos) \n" +
        "from ( select clave_tx clavetx, padre, hijo, clave_mu, individuos " +
        "from CTAXONESS_AND_PADRES,(select clave_mu, sum(nroespecimenes_mu) as individuos " +
        "from cmuestra where phylum_mu = ? group by clave_mu) " +
        "where clave_tx = clave_mu(+)) connect by prior hijo=padre start with clavetx = N2.clave_txg ) MUESTRAS " +
        "from(	select clave_tx clave_txg, padre, hijo, clave_mu, nivel_tx, descripcion_tx, individuos " +
        "from CTAXONESS_AND_PADRES,(select clave_mu, sum(nroespecimenes_mu) as individuos " +
        "from cmuestra where phylum_mu = ? group by clave_mu) " +
        "where clave_tx = clave_mu(+) ) N2 connect by prior hijo=padre start with clave_txg = ?) N1, " +
        "vm_planilla NT \n" +
        "where N1.muestras is not null and NT.NIV = N1.Nivel_tx " +
        "and NT.CLAVE = N1.CLAVE_TXG " + "group by N1.NIVEL_TX, NT.NIVEL, " +
        "DECODE(NT.NIV,'46',INITCAP(N1.DESCRIPCION_TX)||' Sp.','49',initcap(NT.GENERO)||' '||N1.DESCRIPCION_TX||' Sp.','55'," +
        "decode(NT.NIVUP,'46',INITCAP(NT.GENERO)||' '||N1.DESCRIPCION_TX,'49',INITCAP(NT.GENERO)||' '||LOWER(NT.SUBGENERO)||' '||N1.DESCRIPCION_TX,null,null),'58',INITCAP(NT.GENERO)||' '||NT.SUBGENERO||' '||NT.ESPECIE||' '||N1.DESCRIPCION_TX, N1.DESCRIPCION_TX) " +
        "order by 1";

    //Grupos taxom�nicos con nombres vigentes incluidos en el diccionario por phylum   
    //1->codigo
    static final String SQL_GRUPOSTAXCONNOMBRE =
        "select ct.phylum_tx, p.descripcion_ph as Phylum, niv.NIVEL_TA, niv.DESCRIPCION_TA nivel, count(*) as Nro_Registros " +
        "from ctaxoness ct, cphylum p, cnivelestax niv " +
        "where p.codigo_ph = ct.phylum_tx and ct.PHYLUM_TX = ? " +
        "and ct.NIVEL_TX = niv.NIVEL_TA group by ct.phylum_tx, p.descripcion_ph, niv.NIVEL_TA, niv.DESCRIPCION_TA " +
        "order by ct.phylum_tx, p.descripcion_ph, niv.NIVEL_TA, niv.DESCRIPCION_TA";

    //Listado completo de los grupos taxon�micos incluidos en el diccionario por phylum (Nombres vigentes)   
    //1->codigo
    static final String SQL_LISTADOCOMPLETOGRUPOS =
        "select ct.phylum_tx, p.descripcion_ph as Phylum, niv.NIVEL_TA, niv.DESCRIPCION_TA nivel, " +
        "DECODE(NT.NIV,'46',INITCAP(ct.DESCRIPCION_TX)||' Sp.','49',initcap(NT.GENERO)||' '||ct.DESCRIPCION_TX||' Sp.','55'," +
        "decode(NT.NIVUP,'46',INITCAP(NT.GENERO)||' '||ct.DESCRIPCION_TX,'49'," +
        "INITCAP(NT.GENERO)||' '||LOWER(NT.SUBGENERO)||' '||ct.DESCRIPCION_TX,null,null),'58',INITCAP(NT.GENERO)||' '||NT.SUBGENERO||' '||NT.ESPECIE||' '||ct.DESCRIPCION_TX, ct.DESCRIPCION_TX) nombre " +
        "from ctaxoness ct, cphylum p, cnivelestax niv, vm_planilla NT " +
        "where p.codigo_ph = ct.phylum_tx and ct.phylum_tx = ? " +
        "and ct.NIVEL_TX = niv.NIVEL_TA and NT.CLAVE = ct.CLAVE_TX " +
        "group by ct.phylum_tx, p.descripcion_ph, niv.NIVEL_TA, niv.DESCRIPCION_TA, " +
        "DECODE(NT.NIV,'46',INITCAP(ct.DESCRIPCION_TX)||' Sp.','49',initcap(NT.GENERO)||' '||ct.DESCRIPCION_TX||' Sp.','55'," +
        "decode(NT.NIVUP,'46',INITCAP(NT.GENERO)||' '||ct.DESCRIPCION_TX,'49',INITCAP(NT.GENERO)||' '||LOWER(NT.SUBGENERO)||' '||ct.DESCRIPCION_TX,null,null),'58',INITCAP(NT.GENERO)||' '||NT.SUBGENERO||' '||NT.ESPECIE||' '||ct.DESCRIPCION_TX, ct.DESCRIPCION_TX) \n";

    //Especies con descripci�n de su historia natural por phylum  
    //
    static final String SQL_ESPECIMENESHISTORIANATURAL =
        "SELECT Phylum, Nro_registros Muestras, round(RATIO_TO_REPORT(Nro_registros) OVER ()* 100,0) as Porcentaje " +
        "FROM  (select p.descripcion_ph as Phylum, phylum_es, count(*) as Nro_Registros " +
        "from cespecimen, cphylum p where codigo_ph = phylum_es and cespecimen.NIVEL_ES >= 55 group by p.descripcion_ph, phylum_es) " +
        "ORDER  BY 1, Phylum_es\n";

    //Nombres vigentes para especies en el diccionario por phylum  
    static final String SQL_NOMBREVIGENETESENDICC =
        "SELECT Phylum, Nro_registros ESPECIES, round(RATIO_TO_REPORT(Nro_registros) OVER ()* 100,0) as Porcentaje " +
        "FROM  (select p.descripcion_ph as Phylum, phylum_tx, count(*) as Nro_Registros " +
        "from ctaxoness, cphylum p where codigo_ph = phylum_tx and ctaxoness.NIVEL_tx >= 55 " +
        "group by p.descripcion_ph, phylum_tx) " +
        "ORDER  BY 1, nro_registros ";
        
    //Distribucion de lotes por area geogr�fica (55 km)  .
    static final String SQL_AREASGEOGRAFICAS="select latitud, longitud, count(8)\n" + 
    "from  cmuestra m, (select ((trunc(mod(round(latitudinicio_loc,1)*10,10)/5,0)*5)/10 + round(latitudinicio_loc,0)) Latitud,\n" + 
    "((trunc(mod(round(longitudinicio_loc,1)*10,10)/5,0)*5)/10 + round(longitudinicio_loc,0)) Longitud,\n" + 
    "secuencia_loc from clocalidad)\n" + 
    "where  m.secuencia_es = secuencia_loc\n" + 
    "group by latitud, longitud\n" + 
    "order by 1,2,3 ";
    
    //Estadisticas mantenimiento de los lotes por secci�n por a�o  
    static final String SQL_DATOSMANTENIMIENTO="SELECT   cmuestra.seccion_mu seccion, TO_CHAR (fecha_re, 'yyyy') ano,\n" + 
    "         descripcion_lov tarea, COUNT (9) lotes\n" + 
    "    FROM cmuestra, curador.cmresponsables, codigoslov\n" + 
    "   WHERE curador.cmresponsables.seccion_re = 216\n" + 
    "     AND (curador.cmresponsables.tarea_re = codigoslov.codigo_lov)\n" + 
    "     AND codigoslov.tabla_lov = 16\n" + 
    "     AND nroinvemar_re = nroinvemar_mu and instituto_mu =? \n" + 
    "GROUP BY seccion_mu, TO_CHAR (fecha_re, 'yyyy'), codigoslov.descripcion_lov\n" + 
    "ORDER BY 1, 2";
    
    //Estadisticas mantenimiento de los lotes por secci�n por a�o
    static final String SQL_DATOSMANTENIMIENTOFALTANTES="SELECT   cms.seccion_mu seccion, descripcion_lov tarea,  COUNT (9) lotes,\n" + 
    "         ((select count(9) from cmuestra cmt where cmt.seccion_mu = cms.seccion_mu) -\n" + 
    "          (  count(8)  )) faltantes\n" + 
    "    FROM cmuestra cms, curador.cmresponsables cmr, codigoslov lov\n" + 
    "   WHERE cmr.seccion_re = 216\n" + 
    "     AND (cmr.tarea_re = lov.codigo_lov)\n" + 
    "     AND lov.tabla_lov = 16\n" + 
    "     AND nroinvemar_re = nroinvemar_mu and instituto_mu =? \n" + 
    "GROUP BY cms.seccion_mu,\n" + 
    "         lov.descripcion_lov\n" + 
    "ORDER BY 1, 2";
    
    //Numero de sinonimias y nombres comunes indexados por phylum
    static final String SQL_VULGARESSINONIMIAS="SELECT p.descripcion_ph, SUM (c1.vulgar) vulgares, SUM (c1.sinonimia) sinonimias\n" + 
    "FROM cphylum p, (SELECT ct.phylum_tx phylum, ct.clave_tx clave,\n" + 
    "                    COUNT (DISTINCT vu.nombre) vulgar,\n" + 
    "                    COUNT (DISTINCT si.descripcion_sn) sinonimia\n" + 
    "                 FROM ctaxoness ct, cvulgares vu, (select s.PHYLUM_SN, s.CLAVE_SN, s.descripcion_sn\n" + 
    "                                                   from csinonimias s where s.TIPO_SN>0 and s.TIPO_SN<20) si\n" + 
    "                 WHERE ct.clave_tx = vu.clave_vu(+) AND ct.clave_tx = si.clave_sn(+)\n" + 
    "                 GROUP BY ct.phylum_tx, ct.clave_tx\n" + 
    "                 ORDER BY 2 DESC) c1\n" + 
    "WHERE c1.phylum = p.codigo_ph\n" + 
    "GROUP BY p.descripcion_ph\n" + 
    "HAVING (SUM (c1.vulgar) > 0 OR SUM (c1.sinonimia) > 0)\n" + 
    "ORDER BY p.descripcion_ph";
    
    //Lotes del Museo recibidos de otras entidades por seccion 
     static final String SQL_LOTESOTRASENTIDADES="SELECT   cmuestra.seccion_mu seccion, cmuestra.origen_mu institucion,\n" + 
     "         codigoslov.descripcion_lov modalidad, COUNT (1) lotes\n" + 
     "    FROM cmuestra, codigoslov\n" + 
     "   WHERE cmuestra.modalidad_mu = codigoslov.codigo_lov\n" + 
     "     AND tabla_lov = 24\n" + 
     "     AND cmuestra.origen_mu <> ? \n" + 
     "GROUP BY cmuestra.seccion_mu, cmuestra.origen_mu, codigoslov.descripcion_lov\n" + 
     "ORDER BY 1, 2, 3";
     
     //Lotes del MHNMC por tipos 
      static final String SQL_LOTESBYTIPOS="SELECT cmuestra.seccion_mu Seccion, codigoslov.descripcion_lov Tipo,\n" + 
      "COUNT (1)Lotes\n" + 
      "    FROM cmuestra, codigoslov\n" + 
      "   WHERE codigoslov.codigo_lov = cmuestra.materialtipo_mu and instituto_mu =? \n" + 
      "     AND codigoslov.tabla_lov = 3\n" + 
      "GROUP BY cmuestra.seccion_mu, codigoslov.descripcion_lov\n" + 
      "ORDER BY 1, 2";

      
      /******************** agregado para las consultas informacion por grupos registros biologicos *************/
      static final String SQL_PROYECTOS="select nombre_alterno from clst_proyectos order by 1 asc";
      	
      
      /*********************************************************************************************************/
      
	public String loadProyectosXML(Connection conn) throws SQLException {
	    	  
	    	  PreparedStatement preparedStatement = null;
	          preparedStatement =
	                  conn.prepareStatement(EstadisticasXMLDAO.SQL_PROYECTOS);
	          String searchResults = "";
	          ResultSet result = null;
	          StringBuffer sb = new StringBuffer();
	          try {
	
	              result = preparedStatement.executeQuery();
	
	              while (result.next()) {
	                  String valor = result.getString(1);
	                  sb.append("<proyecto>");
	                  sb.append("<value>" + valor + "</value>");
	                  sb.append("<name>" + valor + "</name>");
	                  sb.append("</proyecto>");
	
	              }
	              searchResults = "<proyectos>" + sb.toString() + "</proyectos>";
	
	              System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);
	
	          } finally {
	              if (result != null)
	                  result.close();
	              if (preparedStatement != null)
	                  preparedStatement.close();
	              if (conn != null) {
	                  conn.close();
	              }
	          }
	
	          return searchResults;
	      }
	      
      public String loadColeccionesXML(Connection conn) throws SQLException {
    	  
    	  PreparedStatement preparedStatement = null;
          preparedStatement =
                  conn.prepareStatement(EstadisticasXMLDAO.SQL_FILLCOLECCIONES);
          String searchResults = "";
          ResultSet result = null;
          StringBuffer sb = new StringBuffer();
          try {

              result = preparedStatement.executeQuery();

              while (result.next()) {
                  String valor = result.getString(1);
                  sb.append("<coleccion>");
                  sb.append("<value>" + valor + "</value>");
                  sb.append("<name>" + valor + "</name>");
                  sb.append("</coleccion>");

              }
              searchResults = "<colecciones>" + sb.toString() + "</colecciones>";

              System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

          } finally {
              if (result != null)
                  result.close();
              if (preparedStatement != null)
                  preparedStatement.close();
              if (conn != null) {
                  conn.close();
              }
          }

          return searchResults;
      }
      
    public String loadPhylumsXML(Connection conn) throws SQLException {
    	  
    	  PreparedStatement preparedStatement = null;
          preparedStatement =
                  conn.prepareStatement(EstadisticasXMLDAO.SQL_FILLPHYLUMS);
          String searchResults = "";
          ResultSet result = null;
          StringBuffer sb = new StringBuffer();
          try {

              result = preparedStatement.executeQuery();

              while (result.next()) {
                  String valor = result.getString(1);
                  sb.append("<phylum>");
                  sb.append("<value>" + valor + "</value>");
                  sb.append("<name>" + valor + "</name>");
                  sb.append("</phylum>");

              }
              searchResults = "<phylums>" + sb.toString() + "</phylums>";

              System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

          } finally {
              if (result != null)
                  result.close();
              if (preparedStatement != null)
                  preparedStatement.close();
              if (conn != null) {
                  conn.close();
              }
          }

          return searchResults;
      }
      
      
      public String loadSeccionesXML(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_SECCIONESMU);
        preparedStatement.setString(1, coleccion);
        
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {
                String valor = result.getString(1);
                sb.append("<seccion>");
                sb.append("<value>" + valor + "</value>");
                sb.append("<name>" + valor + "</name>");
                sb.append("</seccion>");

            }
            searchResults = "<secciones>" + sb.toString() + "</secciones>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    public String findBySeccionFechaRecibido(Connection conn,
                                             String seccion, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_FECHARECIBIDO);
        preparedStatement.setString(1, seccion.toUpperCase());
        preparedStatement.setString(2, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<cantidad>");
                sb.append("<ano>" +
                          (result.getString(1) == null ? "Sin Fecha" :
                           result.getString(1)) + "</ano>");
                sb.append("<lotes>" + result.getString(2) + "</lotes>");
                sb.append("</cantidad>");

            }
            searchResults =
                    "<cantidad-muestras>" + sb.toString() + "</cantidad-muestras>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    public String loadSeccionesFechaIdentXML(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_SECCIONMUFECHAIDEN);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {
                String valor = result.getString(1);
                sb.append("<seccion>");
                sb.append("<value>" + valor + "</value>");
                sb.append("<name>" + valor + "</name>");
                sb.append("</seccion>");

            }
            searchResults = "<secciones>" + sb.toString() + "</secciones>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;


    }


    public String findFechaIdentBySeccion(Connection conn,
                                          String seccion, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_FECHAINDENT);
        preparedStatement.setString(1, seccion.toUpperCase());
        preparedStatement.setString(2, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {
                String data =
                    (result.getString(1) == null ? "Sin Fecha" : result.getString(1));
                sb.append("<fecha>");
                sb.append("<value>" + data + "</value>");
                sb.append("<name>" + data + "</name>");
                sb.append("</fecha>");

            }
            searchResults =
                    "<fechasIndent>" + sb.toString() + "</fechasIndent>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    public String findLotesBySeccionFechaIdent(Connection conn, String seccion,
                                               String fecha, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESFECHAIDENT);
        if (fecha.equals("Sin Fecha")) {
            preparedStatement =
                    conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESFECHAIDENTNULL);
            preparedStatement.setString(1, seccion.toUpperCase());
            preparedStatement.setString(2, coleccion);
        } else {

            preparedStatement.setString(1, fecha);
            preparedStatement.setString(2, seccion.toUpperCase());
            preparedStatement.setString(3, coleccion);
        }


        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {


                sb.append("<valor>" + result.getString(1) + "</valor>");


            }
            searchResults = "<valor-fecha>" + sb.toString() + "</valor-fecha>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    public String findLotesFechaIdentBySeccion(Connection conn,
                                               String seccion, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESIDENTABYSECCION);
        preparedStatement.setString(1, seccion.toUpperCase());
        preparedStatement.setString(2, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<cantidad>");
                sb.append("<ano>" +
                          (result.getString(1) == null ? "Sin Fecha" :
                           result.getString(1)) + "</ano>");
                sb.append("<lotes>" + result.getString(2) + "</lotes>");
                sb.append("</cantidad>");

            }
            searchResults =
                    "<cantidad-muestras>" + sb.toString() + "</cantidad-muestras>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }


    public String loadSeccionesFechaCapturaXML(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_SECCIONFECHACAP);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {
                String valor = result.getString(1);
                sb.append("<seccion>");
                sb.append("<value>" + valor + "</value>");
                sb.append("<name>" + valor + "</name>");
                sb.append("</seccion>");

            }
            searchResults = "<secciones>" + sb.toString() + "</secciones>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;


    }


    public String findFechaCaptBySeccion(Connection conn,
                                         String seccion, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_FECHACAPTURA);
        preparedStatement.setString(1, seccion.toUpperCase());
        preparedStatement.setString(2, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {
                String data =
                    (result.getString(1) == null ? "Sin Fecha" : result.getString(1));
                sb.append("<fecha>");
                sb.append("<value>" + data + "</value>");
                sb.append("<name>" + data + "</name>");
                sb.append("</fecha>");

            }
            searchResults =
                    "<fechasCapturas>" + sb.toString() + "</fechasCapturas>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    public String findLotesBySeccionFechaCapt(Connection conn, String seccion,
                                              String fecha, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESFECHACAPTURA);
        if (fecha.equals("Sin Fecha")) {
        	preparedStatement = null;
            preparedStatement =
                    conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESFECHACAPTURAISNULL);
            preparedStatement.setString(1, seccion.toUpperCase());
            preparedStatement.setString(2, coleccion);
        } else {
            preparedStatement.setString(1, seccion.toUpperCase());
            preparedStatement.setString(3, fecha);
            preparedStatement.setString(2, coleccion);

        }


        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {


                sb.append("<valor>" + result.getString(1) + "</valor>");


            }
            searchResults = "<valor-fecha>" + sb.toString() + "</valor-fecha>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    public String findLotesFechaCaptBySeccion(Connection conn,
                                              String seccion, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESCAPTURABYSECCION);
        preparedStatement.setString(1, seccion.toUpperCase());
        preparedStatement.setString(2, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<cantidad>");
                sb.append("<ano>" +
                          (result.getString(1) == null ? "Sin Fecha" :
                           result.getString(1)) + "</ano>");
                sb.append("<lotes>" + result.getString(2) + "</lotes>");
                sb.append("</cantidad>");

            }
            searchResults =
                    "<cantidad-muestras>" + sb.toString() + "</cantidad-muestras>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    public String findSeccionImages(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_SECCIONIMAGES);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<lote>");
                sb.append("<seccion>" + result.getString(1) + "</seccion>");
                sb.append("<conimages>" + result.getString(3) +
                          "</conimages>");
                sb.append("<sinimages>" + result.getString(2) +
                          "</sinimages>");
                sb.append("</lote>");

            }
            searchResults = "<lotes>" + sb.toString() + "</lotes>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    /*ojo revisar la cosulta primero */
    public String loadSeccionesProyectosXML(Connection conn) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_SECCIONPROYECTOS);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {
                String valor = result.getString(1);
                sb.append("<seccion>");
                sb.append("<value>" + valor + "</value>");
                sb.append("<name>" + valor + "</name>");
                sb.append("</seccion>");

            }
            searchResults = "<secciones>" + sb.toString() + "</secciones>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    /*ojo revsar consulta*/
    public String findLotesProyectosBySeccion(Connection conn,
                                              String seccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESPROYECTOS);
        preparedStatement.setString(1, seccion.toUpperCase());
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {
                String titulo =
                    (result.getString(1) == null ? "Sin Proyectos" :
                     result.getString(1));
                sb.append("<proyecto>");
                sb.append("<titulo>" + titulo + "</titulo>");
                sb.append("<lotes>" + result.getString(2) + "</lotes>");
                sb.append("</proyecto>");

            }
            searchResults = "<proyectos>" + sb.toString() + "</proyectos>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    public String loadSeccionesPreservativoXML(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_SECCIONPRES);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {
                String valor = result.getString(1);
                sb.append("<seccion>");
                sb.append("<value>" + valor + "</value>");
                sb.append("<name>" + valor + "</name>");
                sb.append("</seccion>");

            }
            searchResults = "<secciones>" + sb.toString() + "</secciones>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    public String findLotesPreservativoBySeccion(Connection conn,
                                                 String seccion, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESPRESERVATIVO);
        preparedStatement.setString(1, seccion.toUpperCase());
        preparedStatement.setString(2, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<cantidad>");
                sb.append("<preservativo>" + result.getString(1) +
                          "</preservativo>");
                sb.append("<lotes>" + result.getString(2) + "</lotes>");
                sb.append("</cantidad>");

            }
            searchResults =
                    "<cantidad-muestras>" + sb.toString() + "</cantidad-muestras>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    /////////////////////////////////////

    public String loadSeccionesObjetosXML(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_SECCIONOBJETOS);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {
                String valor = result.getString(1);
                sb.append("<seccion>");
                sb.append("<value>" + valor + "</value>");
                sb.append("<name>" + valor + "</name>");
                sb.append("</seccion>");

            }
            searchResults = "<secciones>" + sb.toString() + "</secciones>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    public String findLotesObjetosBySeccion(Connection conn,
                                            String seccion, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESOBJESTOS);
        preparedStatement.setString(1, seccion.toUpperCase());
        preparedStatement.setString(2, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<cantidad>");
                sb.append("<objeto>" + result.getString(1) + "</objeto>");
                sb.append("<lotes>" + result.getString(2) + "</lotes>");
                sb.append("</cantidad>");

            }
            searchResults =
                    "<cantidad-muestras>" + sb.toString() + "</cantidad-muestras>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    ///////////////////////////////////////////////////////////////////////////

    public String loadSeccionesInvestigadorXML(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_SECCIONINVESTIGADOR);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {
                String valor = result.getString(1);
                sb.append("<seccion>");
                sb.append("<value>" + valor + "</value>");
                sb.append("<name>" + valor + "</name>");
                sb.append("</seccion>");

            }
            searchResults = "<secciones>" + sb.toString() + "</secciones>";

            //System.out.println("AQUI ESTA LA CONSULTA::::... \n"+searchResults);

        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    public String findLotesInvestigadorBySeccion(Connection conn,
                                                 String seccion, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESINVESTIGADOR);
        preparedStatement.setString(1, seccion.toUpperCase());
        preparedStatement.setString(2, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<investigador>");
                sb.append("<nombre>" + result.getString(1) + "</nombre>");
                sb.append("<tarea>" + result.getString(2) + "</tarea>");
                sb.append("<ano>" +
                          (result.getString(3) == null ? "Sin Fecha" :
                           result.getString(3)) + "</ano>");
                sb.append("<lotes>" + result.getString(4) + "</lotes>");
                sb.append("</investigador>");

            }
            searchResults =
                    "<investigadores>" + sb.toString() + "</investigadores>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    
    public String loadInvestigadoresXML(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_INVESTIGADORES);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {
                String valor = result.getString(1);
                sb.append("<investigador>");
                sb.append("<value>" + valor + "</value>");
                sb.append("<name>" + valor + "</name>");
                sb.append("</investigador>");

            }
            searchResults = "<investigadores>" + sb.toString() + "</investigadores>";

            
        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    
    public String findLotesByInvestigador(Connection conn,
                                                 String investigador, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESBYINVEST);
        preparedStatement.setString(2, investigador);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<datos>");
                sb.append("<seccion>" + result.getString(1) + "</seccion>");
                sb.append("<tarea>" + result.getString(3) + "</tarea>");
                sb.append("<ano>" +
                          (result.getString(4) == null ? "Sin Fecha" :
                           result.getString(4)) + "</ano>");
                sb.append("<lotes>" + result.getString(5) + "</lotes>");
                sb.append("</datos>");

            }
            searchResults ="<investigador>" + sb.toString() + "</investigador>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////

    public String findPhilums(Connection conn) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_PHYLUMS);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<phylum>");
                sb.append("<value>" + result.getString(1) + "</value>");
                sb.append("<name>" + result.getString(2) + "</name>");
                sb.append("</phylum>");

            }
            searchResults = "<phylums>" + sb.toString() + "</phylums>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    //10.

    public String findLotesByPhylums(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_MUESTRASBYPHYLUM);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<muestra>");
                sb.append("<phylum>" + result.getString(1) + "</phylum>");
                sb.append("<cantidad>" + result.getString(2) + "</cantidad>");
                sb.append("<porcentages>" + result.getString(3) +
                          "</porcentages>");

                sb.append("</muestra>");

            }
            searchResults = "<muestras>" + sb.toString() + "</muestras>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    //11.

    public String findLotesNivelByPhylums(Connection conn,String codigo) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =
                conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESINDENTHASTANIVEL);
        preparedStatement.setString(1, codigo);
        preparedStatement.setString(2, codigo);
        preparedStatement.setString(3, codigo + ".5." + codigo);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<taxonomia>");
                sb.append("<codigo>" + result.getString(1) + "</codigo>");
                sb.append("<nombre>" + result.getString(3) + "</nombre>");
                sb.append("<phylum>"+ result.getString(2) +"</phylum>");
                sb.append("<lotes>"+ result.getString(4) +"</lotes>");
                sb.append("</taxonomia>");

            }
            searchResults = "<taxonomias>" + sb.toString() + "</taxonomias>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    
    //12.

    public String findLotesByNivelTax(Connection conn,String codigo) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESMUSEOBYNIVELPHY);
        preparedStatement.setString(1, codigo);
        preparedStatement.setString(2, codigo);
        preparedStatement.setString(3, codigo + ".5." + codigo);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<taxonomia>");
                sb.append("<codigo>" + result.getString(1) + "</codigo>");
                sb.append("<phylum>"+ result.getString(2) +"</phylum>");
                sb.append("<lotes>" + result.getString(3) + "</lotes>");
                sb.append("<elementos>" + result.getString(4) + "</elementos>");

                sb.append("</taxonomia>");

            }
            searchResults = "<taxonomias>" + sb.toString() + "</taxonomias>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    
    //13.

    public String findEspecimenesByNivelTax(Connection conn,String codigo) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =conn.prepareStatement(EstadisticasXMLDAO.SQL_ESPECIMENESENMUSEO);
        preparedStatement.setString(1, codigo);
        preparedStatement.setString(2, codigo);
        preparedStatement.setString(3, codigo + ".5." + codigo);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<taxonomia>");
                sb.append("<codigo>" + result.getString(1) + "</codigo>");
                sb.append("<phylum>"+ result.getString(2) +"</phylum>");
                sb.append("<nombre>" + result.getString(3) + "</nombre>");
                sb.append("<especimenes>"+ result.getString(4) +"</especimenes>");
                

                sb.append("</taxonomia>");

            }
            searchResults = "<taxonomias>" + sb.toString() + "</taxonomias>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    
    //14    
    public String findGruposTaxByPhylum(Connection conn,String codigo) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =conn.prepareStatement(EstadisticasXMLDAO.SQL_GRUPOSTAXCONNOMBRE);
        preparedStatement.setString(1, codigo);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<taxonomia>");
                sb.append("<codigo>" + result.getString(1) + "</codigo>");
                sb.append("<phylum>"+ result.getString(2) +"</phylum>");
                sb.append("<nivel>" + result.getString(4) + "</nivel>");
                sb.append("<elementos>"+ result.getString(5) +"</elementos>");
                

                sb.append("</taxonomia>");

            }
            searchResults = "<taxonomias>" + sb.toString() + "</taxonomias>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    //15    
    public String findListadoGruposenDicc(Connection conn,String codigo) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =conn.prepareStatement(EstadisticasXMLDAO.SQL_LISTADOCOMPLETOGRUPOS);
        preparedStatement.setString(1, codigo);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<taxonomia>");
                sb.append("<codigo>" + result.getString(1) + "</codigo>");
                sb.append("<phylum>"+ result.getString(2) +"</phylum>");
                sb.append("<nivel>" + result.getString(4) + "</nivel>");
                sb.append("<nombre>"+ result.getString(5) +"</nombre>");
                

                sb.append("</taxonomia>");

            }
            searchResults = "<taxonomias>" + sb.toString() + "</taxonomias>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    //16    
    public String findEspecieConDescrip(Connection conn) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =conn.prepareStatement(EstadisticasXMLDAO.SQL_ESPECIMENESHISTORIANATURAL);
       
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<ficha>");
                sb.append("<phylum>" + result.getString(1) + "</phylum>");
                sb.append("<cantidad>"+ result.getString(2) +"</cantidad>");
                sb.append("<porcentages>" + result.getString(3) + "</porcentages>");
                
                

                sb.append("</ficha>");

            }
            searchResults = "<fichas>" + sb.toString() + "</fichas>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    
    //17    
    public String findNombresViegentes(Connection conn) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =conn.prepareStatement(EstadisticasXMLDAO.SQL_NOMBREVIGENETESENDICC);
       
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<especie>");
                sb.append("<phylum>" + result.getString(1) + "</phylum>");
                sb.append("<cantidad>"+ result.getString(2) +"</cantidad>");
                sb.append("<porcentages>" + result.getString(3) + "</porcentages>");
                
                

                sb.append("</especie>");

            }
            searchResults = "<especies>" + sb.toString() + "</especies>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }

    
    //18    
    public String findLotesByAreasGeograficas(Connection conn) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =conn.prepareStatement(EstadisticasXMLDAO.SQL_AREASGEOGRAFICAS);
       
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<area>");
                sb.append("<latitud>" + (result.getString(1)!=null?result.getString(1):"NO Espesificado") + "</latitud>");
                sb.append("<longitud>"+ (result.getString(2)!=null?result.getString(2):"NO Espesificado") +"</longitud>");
                sb.append("<lotes>" + result.getString(3) + "</lotes>");
                sb.append("</area>");
               
            }
            searchResults = "<areas>" + sb.toString() + "</areas>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    
    
    
      
    public String findDatosMantenimiento(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =conn.prepareStatement(EstadisticasXMLDAO.SQL_DATOSMANTENIMIENTO);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<dato>");
                sb.append("<seccion>" + result.getString(1) + "</seccion>");
                sb.append("<ano>"+ result.getString(2) +"</ano>");
                sb.append("<tarea>" + result.getString(3) + "</tarea>");
                sb.append("<lotes>" + result.getString(4) + "</lotes>");
                sb.append("</dato>");
               
            }
            searchResults = "<datos>" + sb.toString() + "</datos>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    
    public String findDatosMantenimientoFaltantes(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =conn.prepareStatement(EstadisticasXMLDAO.SQL_DATOSMANTENIMIENTOFALTANTES);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<dato>");
                sb.append("<seccion>" + result.getString(1) + "</seccion>");
                sb.append("<tarea>" + result.getString(2) + "</tarea>");
                sb.append("<lotes>" + result.getString(3) + "</lotes>");
                sb.append("<faltantes>"+ result.getString(4) +"</faltantes>");
                sb.append("</dato>");
               
            }
            searchResults = "<datos>" + sb.toString() + "</datos>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        return searchResults;

    }
    
    public String findNumSinonimiasAndVulgares(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =conn.prepareStatement(EstadisticasXMLDAO.SQL_VULGARESSINONIMIAS);
       
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<dato>");
                sb.append("<phylum>" + result.getString(1) + "</phylum>");
                sb.append("<vulgares>" + result.getString(2) + "</vulgares>");
                sb.append("<sinonimias>" + result.getString(3) + "</sinonimias>");
                sb.append("</dato>");
               
            }
            searchResults = "<datos>" + sb.toString() + "</datos>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }
         return searchResults;

    }
    
    public String findLotesOtrasEntidades(Connection conn, String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESOTRASENTIDADES);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<dato>");
                sb.append("<seccion>" + result.getString(1) + "</seccion>");
                sb.append("<institucion>" + result.getString(2) + "</institucion>");
                sb.append("<modalidad>" + result.getString(3) + "</modalidad>");
                sb.append("<lotes>" + result.getString(4) + "</lotes>");
                sb.append("</dato>");
               
            }
            searchResults = "<datos>" + sb.toString() + "</datos>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }
         return searchResults;

    }   
    
    public String findLotesByTipos(Connection conn,String coleccion) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement =conn.prepareStatement(EstadisticasXMLDAO.SQL_LOTESBYTIPOS);
        preparedStatement.setString(1, coleccion);
        String searchResults = "";
        ResultSet result = null;
        StringBuffer sb = new StringBuffer();
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {

                sb.append("<dato>");
                sb.append("<seccion>" + result.getString(1) + "</seccion>");
                sb.append("<tipo>" + result.getString(2) + "</tipo>");
                sb.append("<lotes>" + result.getString(3) + "</lotes>");
                sb.append("</dato>");
               
            }
            searchResults = "<datos>" + sb.toString() + "</datos>";


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }
         return searchResults;

    }
    
    
    public String findLotesByGruposTax(Connection conn,String codigo,String coleccion,String proyecto,String nivel) throws SQLException {
    	
    	
        System.out.println("codigo en Method => "+codigo);
    	System.out.println("coleccion en Method => "+coleccion);
        System.out.println("proyecto en Method => "+proyecto);
        
        if(coleccion==null){
        	System.out.println("Colecciion is null");
        }
    	
        PreparedStatement preparedStatement = null;
        
        StringBuffer selectAndFom=new StringBuffer("SELECT   vm.phylum, NVL (vm.clase, 'No Clasificado') clase,  "+ 
        			  " NVL (vm.orden, 'No Clasificado') orden, "+
        			  " NVL (vm.familia, 'No Clasificado') familia, "+
        			  " NVL (vm.genero, 'No Clasificado') genero, "+
        			  " NVL (vm.especie, 'No Clasificado') especie, SUM (cl.individuos) Lotes, sum(ejemplares) ejemplares, "+
        			  " GROUPING (NVL (vm.clase, 'No Clasificado')) "+
        			  " || GROUPING (NVL (vm.orden, 'No Clasificado')) "+
        			  " || GROUPING (NVL (vm.familia, 'No Clasificado')) "+
        			  " || GROUPING (NVL (vm.genero, 'No Clasificado')) "+
        			  " || GROUPING (NVL (vm.especie, 'No Clasificado')) grupo "+
        			  " FROM vm_planilla vm, "+
        			  " 	(SELECT   clave, COUNT (clave) individuos, sum(nvl(ejemplares,0)) ejemplares "+
        			  "		 	FROM vm_fichac "+
        			  " 		WHERE phylum = ? ");	
        StringBuffer subWhere=null;
        String endSelect="  GROUP BY clave) cl "+
        			" WHERE cl.clave = vm.clave "+
        			" GROUP BY vm.phylum, "+
        			" ROLLUP (NVL (vm.clase, 'No Clasificado'), "+
        			" NVL (vm.orden, 'No Clasificado'), "+
        			" NVL (vm.familia, 'No Clasificado'), "+
        			" NVL (vm.genero, 'No Clasificado'), "+
        			" NVL (vm.especie, 'No Clasificado')) "+
        			" ORDER BY 1, 2";
        
        int sw=0;
        
        if(!isEmpty(coleccion) && !isEmpty(proyecto) ){
        	subWhere=new StringBuffer();
        	System.out.println("ambos son not null");
        	subWhere.append(" AND ubi_geo = ? AND proyecto = ? ");
        	
        	System.out.println("nivel: "+nivel);
        	System.out.println("sw: "+sw);
        	if(nivel!=null && sw==0){
        		subWhere.append("  and nivel>54");
        		sw=1;
        	}
        	selectAndFom.append(subWhere.toString());
        	selectAndFom.append(endSelect);
        	
        	
        	
        	preparedStatement =conn.prepareStatement(selectAndFom.toString());
            preparedStatement.setString(1, codigo);
            preparedStatement.setString(2, coleccion);
            preparedStatement.setString(3, proyecto);
            
        }else if(!isEmpty(coleccion)  && isEmpty(proyecto) ){
        	subWhere=new StringBuffer();
        	subWhere.append(" AND ubi_geo = ? ");
        	if(nivel!=null && sw==0){
        		subWhere.append("  and nivel>54");
        		sw=1;
        	}
        	selectAndFom.append(subWhere.toString());
        	selectAndFom.append(endSelect);
        	
        	preparedStatement =conn.prepareStatement(selectAndFom.toString());
            preparedStatement.setString(1, codigo);
            preparedStatement.setString(2, coleccion);
            
        }else if(!isEmpty(proyecto) && isEmpty(coleccion)  ){
        	subWhere=new StringBuffer();
        	subWhere.append(" AND proyecto = ? ");
        	if(nivel!=null && sw==0){
        		subWhere.append("  and nivel>54");
        		sw=1;
        	}
        	selectAndFom.append(subWhere.toString());
        	selectAndFom.append(endSelect);
        	
        	preparedStatement =conn.prepareStatement(selectAndFom.toString());
            preparedStatement.setString(1, codigo);
            preparedStatement.setString(2, proyecto);
            
        }else {
        
        	selectAndFom.append(endSelect);
           	preparedStatement =conn.prepareStatement(selectAndFom.toString());
            preparedStatement.setString(1, codigo);
        
        }
        
        System.out.println("Query: findLotesByGruposTax ... "+selectAndFom.toString());
        
        ResultSet result = null;
        StringBuffer html = new StringBuffer();
        StringBuffer htmlTABLE = new StringBuffer();
        htmlTABLE.append("<table style=\"margin-left: 10px; border: 1px solid #d7edfb;\">"+
                        "<thead> "+
                        	"<tr> "+
                        		"<th style=\"margin: 6px 10px; padding:10px 15px;\" >Phylum</th> "+
			                    "<th style=\"margin: 6px 10px; padding:10px 15px;\">Clase</th> "+
			                    "<th style=\"margin: 6px 10px; padding:10px 15px; \">Orden</th> "+
			                    "<th style=\"margin: 6px 10px; padding:10px 15px;\">Familia</th> "+
			                    "<th style=\"margin: 6px 10px; padding:10px 15px;\">Genero</th> "+
			                    "<th style=\"margin: 6px 10px; padding:10px 15px;\">Especie</th> "+
			                    "<th style=\"margin: 6px 10px; padding:10px 2px;\">Lotes</th> "+
			                    "<th style=\"margin: 6px 10px; padding:10px 2px;\">Ejemplares</th> "+
			                "</tr> "+
			            "</thead> "+
			            
			            " <tbody id=\"tbodyEstad11_1\"> ");
        String colorSchema=null;
        try {

            result = preparedStatement.executeQuery();

            while (result.next()) {
            	colorSchema=getColorSchema(result.getString("grupo"));
            	
            	html.append("<tr style=\"background-color:"+colorSchema+"\">");
            	html.append("<td>");
            	html.append(isNull(result.getString("phylum")));
            	html.append("</td>");
            	html.append("<td>");
            	html.append(isNull(result.getString("clase")));
            	html.append("</td>");
            	html.append("<td>");
            	html.append(isNull(result.getString("orden")));
            	html.append("</td>");
            	html.append("<td>");
            	html.append(isNull(result.getString("familia")));
            	html.append("</td>");
            	html.append("<td>");
            	html.append(isNull(result.getString("genero")));
            	html.append("</td>");
            	html.append("<td>");
            	html.append(isNull(result.getString("especie")));
            	html.append("</td>");
            	html.append("<td>");
            	html.append(isNull(result.getString("Lotes")));
            	html.append("</td>");
            	html.append("<td>");
            	html.append(isNull(result.getString("ejemplares")));
            	html.append("</td>");
            	html.append("</tr>");
                

            }
           
            


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }


        htmlTABLE.append(html.toString());
        htmlTABLE.append("</tbody");
        htmlTABLE.append("</table>");
        
        return htmlTABLE.toString();

    }
    
    
    public String findRLotesByNivel(Connection conn,String codigo,String coleccion,String proyecto) throws SQLException {

        PreparedStatement preparedStatement = null;
        
        StringBuffer selectAndFom=new StringBuffer("SELECT v.nivel, n.descripcion_ta, (cantidad) lotes, "+
        											" SUM (cantidad) OVER (ORDER BY v.nivel DESC) acumulados "+
        											" FROM cnivelestax n, "+
        											" (SELECT   nivel, COUNT (nivel) cantidad "+
        											" FROM vm_fichac "+
        											" WHERE phylum = ? ");	
        StringBuffer subWhere=null;
        String endSelect="   GROUP BY nivel "+
        				" ORDER BY 1 DESC) v "+
        				" WHERE v.nivel = n.nivel_ta";
        
                
        if(!isEmpty(coleccion) && !isEmpty(proyecto) ){
        	subWhere=new StringBuffer();
        	subWhere.append(" AND ubi_geo = ? AND proyecto = ? ");
        	selectAndFom.append(subWhere.toString());
        	selectAndFom.append(endSelect);
        	
        	preparedStatement =conn.prepareStatement(selectAndFom.toString());
            preparedStatement.setString(1, codigo);
            preparedStatement.setString(2, coleccion);
            preparedStatement.setString(3, proyecto);
            
        }else if(!isEmpty(coleccion) && isEmpty(proyecto) ){
        	subWhere=new StringBuffer();
        	subWhere.append(" AND ubi_geo = ? ");
        	
        	selectAndFom.append(subWhere.toString());
        	selectAndFom.append(endSelect);
        	
        	preparedStatement =conn.prepareStatement(selectAndFom.toString());
            preparedStatement.setString(1, codigo);
            preparedStatement.setString(2, coleccion);
            
        }else if(!isEmpty(proyecto) && isEmpty(coleccion)  ){
        	subWhere=new StringBuffer();
        	subWhere.append(" AND proyecto = ? ");
        	
        	selectAndFom.append(subWhere.toString());
        	selectAndFom.append(endSelect);
        	
        	preparedStatement =conn.prepareStatement(selectAndFom.toString());
            preparedStatement.setString(1, codigo);
            preparedStatement.setString(2, proyecto);
            
        }else {
        
        	selectAndFom.append(endSelect);
           	preparedStatement =conn.prepareStatement(selectAndFom.toString());
            preparedStatement.setString(1, codigo);
        
        }
        
        System.out.println("Query: findLotesByGruposTax ... "+selectAndFom.toString());
        
        ResultSet result = null;
        StringBuffer html = new StringBuffer();
        StringBuffer htmlTABLE = new StringBuffer();
        htmlTABLE.append("<table style=\"margin-left: 50px; border: 1px solid #d7edfb;\">"+
                        "<thead> "+
                        	"<tr> "+
                        		"<th style=\"margin: 6px 10px; padding:10px 15px;\" >Nivel</th> "+
			                    "<th style=\"margin: 6px 10px; padding:10px 15px;\">Lotes</th> "+
			                    "<th style=\"margin: 6px 10px; padding:10px 15px; \">Acumulados</th> "+
			                    
			                "</tr> "+
			            "</thead> "+
			            
			            " <tbody id=\"tbodyEstad12_1\"></tbody>  ");
        try {

            result = preparedStatement.executeQuery();
            
            while (result.next()) {
            	
            	
            	html.append("<tr style=\"background-color:#ffffff\">");
            	html.append("<td>");
            	html.append(result.getString("descripcion_ta"));
            	html.append("</td>");
            	html.append("<td>");
            	html.append(result.getString("lotes"));
            	html.append("</td>");
            	html.append("<td>");
            	html.append(result.getString("acumulados"));
            	html.append("</td>");
            	html.append("</tr>");
                

            }
           
            


        } finally {
            if (result != null)
                result.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (conn != null) {
                conn.close();
            }
        }

        htmlTABLE.append(html.toString());
        htmlTABLE.append("</tbody");
        htmlTABLE.append("</table>");
        return htmlTABLE.toString();

    }
    
    private String getColorSchema(String codigo){
    	String color=null;
    	if(codigo.equalsIgnoreCase("00000")){
    		color="#ffffff";
    	}else if(codigo.equalsIgnoreCase("00001")){
    		color="#eff7ff";
    	}else if(codigo.equalsIgnoreCase("00011")){
    		color="#cfe7ff";
    	}else if(codigo.equalsIgnoreCase("00111")){
    		color="#afd7ff";
    	}else if(codigo.equalsIgnoreCase("01111")){
    		color="#8fc7ff";
    	}else if(codigo.equalsIgnoreCase("11111")){
    		color="#6fb8ff";
    	}
    	
    	return color;
    }
    
    private String isNull(String str){
    	String cadena="&nbsp;";
    	if(str!=null){
    		cadena=str;
    	}
    	return cadena;
    }

    public boolean isEmpty(String str) {
        return str == null || str.length() == 0 ||str.equals("null");
    }
    
}
