package co.org.invemar.catalogo.model;

import java.io.File;
import java.io.FileNotFoundException;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

public class Archivo {
    ArrayList body=new ArrayList();
   final String header="Ultimo Cambio\t Coleccion\t Codigo Coleccion\t Numero Catalogo\t Proyecto\t Estacion\t Especie\t Base del Resgistro\t Reino\t División\t "+
    "Clase\t Orden\t Familia\t Genero\t Epiteto\t Autor Especie\t Identificado por\t Año Identificacion\t "+
    "Tipo\t Colectado Por\t Año Captura\t Cuerpo de Agua\t Pais\t Departamento\t Localidad\t Longitud\t "+
    "Latitud\t Prof de Captura\t Ejemplares\t";

    public String archivar(String path,String ext,List datos){
       
        /*generar el nombre del archivo en formato dia:mes:anio_hora:min:sec_milllisec*/
        Calendar c = new GregorianCalendar();    
        String dia = Integer.toString(c.get(Calendar.DATE));
        String mes = Integer.toString(c.get(Calendar.MONTH));
        String annio = Integer.toString(c.get(Calendar.YEAR));
        String hour=Integer.toString(c.get(Calendar.HOUR));
        String minute=Integer.toString(c.get(Calendar.MINUTE));
        String second=Integer.toString(c.get(Calendar.SECOND));
        String millisecond=Integer.toString(c.get(Calendar.MILLISECOND));
        String nombreArchivo="DatosMHNMC_"+dia+"_"+mes+"_"+annio+"_"+hour+minute+second+"_"+millisecond+"."+ext;
        
        /*Creamos el archivo*/
        java.io.File file = new java.io.File(path+nombreArchivo);
        java.io.PrintStream fileWriter;

        try {
            fileWriter = new java.io.PrintStream(file,"ISO-8859-1");
            /*agregamos los encabezados de las columnas*/
            fileWriter.println(header);
            
            Iterator it=datos.iterator();
            
            while(it.hasNext()){
                Catalogo catalogo=(Catalogo)it.next();
                StringBuffer sb=new StringBuffer();
                sb.append(catalogo.toString());
                fileWriter.println(sb.toString());            
            
            }
            
            fileWriter.close();
            
            
            
        } catch (FileNotFoundException e) {
            System.out.println("Error generado por el Archivo "+e);
        } catch (UnsupportedEncodingException e) {
             System.out.println("No soprta el charset=ISO-8859-1"+e);
        }

        return nombreArchivo;
    }

    /*
    public static void main(String[] args) {
        Archivo archivo = new Archivo();
        System.out.println("Copiando....");
        
        String nombreArchivo=archivo.archivar();
        System.out.println("Complete...");
        
    }
    */
}
