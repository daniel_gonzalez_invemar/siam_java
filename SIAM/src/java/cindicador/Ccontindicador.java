package cindicador;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;


public class Ccontindicador{

  private String Error;
  private int tamano;
  private Vector resultados;
  public Ccontindicador(){}

  public int gettamanoresultados(){
      return resultados.size();
      }

/*** CONTENEDOR DE ESTADISTICAS*/
  public boolean estadisticas3(Connection conn2, String nindicador, String nparamnombre, String nuer, String ano1, String ano2, String orden)
      {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        Cindicador var = new Cindicador();
        String select = new String();
        String texto = new String();
        int porden = 11;

        select = "SELECT * FROM v_indicadores where param_nombre is not null ";


        if(ano1 != null && !ano1.equals("") && ano2.equals("")){
          select = select + " and param_year = '" + ano1 +"' " ;
        }
        if(ano1 != null && !ano1.equals("") && ano2 != null && !ano2.equals("") ){
          select = select + " and param_year >= '" + ano1 +"' and param_year <= '" + ano2 +"' " ;
        }
        
        if(nindicador != null && !nindicador.equals("")){
          select = select + " and param_indicador = '" + nindicador + "'";
        }
        if(nparamnombre != null && !nparamnombre.equals("")){
          select = select + " and param_nombre = '" + nparamnombre + "'";
        }
        if(nuer != null && !nuer.equals("")){
          select = select + " and param_coduer = '" + nuer + "'";
        }
        if(orden != null && !orden.equals("")){
          porden = Integer.parseInt(orden);
        }


	select = select + " order by " + porden;
		
        Error = select;
        try
          {
            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()){
              resp = false;
            }
            else
            {
              do{
                    var = new Cindicador();

                    var.setparam_nombre(rs.getString(1));
                    var.setvalor(rs.getString(2));
                    var.setparam_coduer(rs.getString(5));
                    var.setparam_indicador(rs.getString(5));
                    var.setindicador(rs.getString(8));
                    var.setparam_unidad(rs.getString(9));    /*este es el codvariable*/
                    var.setunidad(rs.getString(10)); /*esta es el nomvariable*/
                    var.setvalorn(rs.getDouble(3));
                    var.setparam_year(rs.getString(4));
                    var.setlabel_year(rs.getString(11));
                    
                  resultados.addElement(var);
                  tamano++;
              }while(rs.next());
            }
            if (rs!= null) rs.close();
            if (stmt!= null) stmt.close();
            resp = true;
         }
          catch(SQLException e)
         {
          Error = e.getMessage() + "," + select;
          resp = false;
         }
         return resp;
      }


/**
  
  /** retorna una instancia del contenedor de Cindicador dada su posicion en el
vector */
  public Cindicador getindicador(int i) {
    return ((Cindicador)resultados.elementAt(i));
  }

  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }
}
