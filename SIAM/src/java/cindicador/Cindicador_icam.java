package cindicador;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

public class Cindicador_icam{
   private int ano;
   private int mes;
   private int temporada;
   private int muestreo;
   private String departamento;
   private String sector;
   private String coduer;
   private String estacion;
   private double ca_ph;
   private double ca_po4;
   private double ca_sst;
   private double ca_no3;
   private double ca_od;
   private double ca_hdd;
   private double ca_cte;
   private double ca_dbo;
   private double ca_cla;
   private double icampff;
   private String icampff_calificacion;
   private double peso;
   private int count_pesos;
   private String link;
   private double lat;
   private double lon;

    private String ph_calificacion;
    private String po4_calificacion;
    private String sst_calificacion;
    private String no3_calificacion;
    private String od_calificacion;
    private String hdd_calificacion;
    private String cte_calificacion;
    private String dbo_calificacion;
    private String cla_calificacion;
   
   private String Error;

/** Metodo constructor de la clase */
  public void Cindicador_icam(){}

	public void setparam_ano(int aano){    ano=aano;}
	public void setparam_mes(int ames){    mes=ames;}
	public void setparam_temporada(int atemporada){    temporada=atemporada;}
	public void setparam_muestreo(int amuestreo){    muestreo=amuestreo;}
	public void setparam_departamento(String adepartamento){    departamento=adepartamento;}
	public void setparam_sector(String asector){    sector=asector;}
	public void setparam_coduer(String acoduer){    coduer=acoduer;}
	public void setparam_estacion(String aestacion){    estacion=aestacion;}
	public void setparam_ca_ph(double aca_ph){    ca_ph=aca_ph;}
	public void setparam_ca_po4(double aca_po4){    ca_po4=aca_po4;}
	public void setparam_ca_sst(double aca_sst){    ca_sst=aca_sst;}
	public void setparam_ca_no3(double aca_no3){    ca_no3=aca_no3;}
	public void setparam_ca_od(double aca_od){    ca_od=aca_od;}
	public void setparam_ca_hdd(double aca_hdd){    ca_hdd=aca_hdd;}
	public void setparam_ca_cte(double aca_cte){    ca_cte=aca_cte;}
	public void setparam_ca_dbo(double aca_dbo){    ca_dbo=aca_dbo;}
	public void setparam_ca_cla(double aca_cla){    ca_cla=aca_cla;}
	public void setparam_icampff(double aicampff){    icampff=aicampff;}
	public void setparam_icampff_calificacion(String aicampff_calificacion){    icampff_calificacion=aicampff_calificacion;}
	public void setparam_peso(double apeso){    peso=apeso;}
	public void setparam_count_pesos(int acount_pesos){    count_pesos=acount_pesos;}
	public void setparam_link(String alink){    link=alink;}
	public void setparam_lat(double alat){    lat=alat;}
	public void setparam_lon(double alon){    lon=alon;}

        public void setparam_ph_calificacion(String vph_calificacion)  {    ph_calificacion=vph_calificacion;}
        public void setparam_po4_calificacion(String vpo4_calificacion){    po4_calificacion=vpo4_calificacion;}
        public void setparam_sst_calificacion(String vsst_calificacion){    sst_calificacion=vsst_calificacion;}
        public void setparam_no3_calificacion(String vno3_calificacion){    no3_calificacion=vno3_calificacion;}
        public void setparam_od_calificacion(String vod_calificacion)  {    od_calificacion=vod_calificacion;}
        public void setparam_hdd_calificacion(String vhdd_calificacion){    hdd_calificacion=vhdd_calificacion;}
        public void setparam_cte_calificacion(String vcte_calificacion){    cte_calificacion=vcte_calificacion;}
        public void setparam_dbo_calificacion(String vdbo_calificacion){    dbo_calificacion=vdbo_calificacion;}
        public void setparam_cla_calificacion(String vcla_calificacion){    cla_calificacion=vcla_calificacion;}
  
  public void seterror(String aerror)  {    Error = aerror;  }
  
  
  
  /**  Retorna los atributos del objeto  */

	public int get_ano() { return ano; }
	public int get_mes() { return mes; }
	public int get_temporada() { return temporada; }
	public int get_muestreo() { return muestreo; }
	public String get_departamento() { return departamento; }
	public String get_sector() { return sector; }
	public String get_coduer() { return coduer; }
	public String get_estacion() { return estacion; }
	public double get_ca_ph() { return ca_ph; }
	public double get_ca_po4() { return ca_po4; }
	public double get_ca_sst() { return ca_sst; }
	public double get_ca_no3() { return ca_no3; }
	public double get_ca_od() { return ca_od; }
	public double get_ca_hdd() { return ca_hdd; }
	public double get_ca_cte() { return ca_cte; }
	public double get_ca_dbo() { return ca_dbo; }
	public double get_ca_cla() { return ca_cla; }
	public double get_icampff() { return icampff; }
	public String get_icampff_calificacion() { return icampff_calificacion; }
	public double get_peso() { return peso; }
	public int get_count_pesos() { return count_pesos; }
	public String get_link() { return link; }
	public double get_lat() { return lat; }
	public double get_lon() { return lon; }

        public String get_ph_calificacion()  { return ph_calificacion; }
        public String get_po4_calificacion() { return po4_calificacion; }
        public String get_sst_calificacion() { return sst_calificacion; }
        public String get_no3_calificacion() { return no3_calificacion; }
        public String get_od_calificacion()  { return od_calificacion; }
        public String get_hdd_calificacion() { return hdd_calificacion; }
        public String get_cte_calificacion() { return cte_calificacion; }
        public String get_dbo_calificacion() { return dbo_calificacion; }
        public String get_cla_calificacion() { return cla_calificacion; }

  public String get_error()  {    return Error;  }


 /** Carga una instancia del objeto de la base de datos dependiendo del
 atributo a�o que este cargado en el objeto  */

  public boolean indicador_icam(Connection conn2)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        stmt = conn2.prepareStatement("select * from CAM_ADMON.VSPINCAM_ICAM2011_4_FINAL" );

        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
			ano  = rs.getInt(1);
			mes  = rs.getInt(2);
			temporada  = rs.getInt(3);
			muestreo  = rs.getInt(4);
			departamento  = rs.getString(5);
			sector  = rs.getString(6);
			coduer  = rs.getString(7);
			estacion  = rs.getString(9);
			ca_ph  = rs.getDouble(13);
			ca_po4  = rs.getDouble(14);
			ca_sst  = rs.getDouble(15);
			ca_no3  = rs.getDouble(16);
			ca_od  = rs.getDouble(17);
			ca_hdd  = rs.getDouble(18);
			ca_dbo  = rs.getDouble(19);
			ca_cte  = rs.getDouble(20);

			ca_cla  = rs.getDouble(21);

			icampff  = rs.getDouble(22);
			icampff_calificacion  = rs.getString(23);
			peso  = rs.getDouble(24);
			count_pesos  = rs.getInt(25);
			link  = rs.getString(26);
			lat  = rs.getDouble(10);
			lon  = rs.getDouble(11);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }



}
