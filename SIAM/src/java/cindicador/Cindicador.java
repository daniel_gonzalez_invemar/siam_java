package cindicador;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

public class Cindicador{
   private String param_nombre;
   private String valor;
   private String param_coduer;
   private String param_indicador;
   private String indicador;
   private String param_unidad;
   private String unidad;
   private double valorn;
   private String param_year;
   private String label_year;

   private String Error;

/** Metodo constructor de la clase */
  public void Cindicador(){
  }


/** Carga el atributo ano al objeto */
  public void setparam_nombre(String aparam_nombre)
  {
    param_nombre = aparam_nombre;
  }
  public void setvalor(String avalor)
  {
    valor = avalor;
  }
  public void setparam_coduer(String aparam_coduer)
  {
    param_coduer = aparam_coduer;
  }
  public void setparam_indicador(String aparam_indicador)
  {
    param_indicador = aparam_indicador;
  }
  public void setindicador(String aindicador)
  {
    indicador = aindicador;
  }
  public void setparam_unidad(String aparam_unidad)
  {
    param_unidad = aparam_unidad;
  }
  public void setunidad(String aunidad)
  {
    unidad = aunidad;
  }
  public void setvalorn(double avalorn)
  {
    valorn = avalorn;
  }
  public void setparam_year(String aparam_year)
  {
    param_year = aparam_year;
  }
  public void setlabel_year(String alabel_year)
  {
    label_year = alabel_year;
  }
  public void seterror(String aerror)
  {
    Error = aerror;
  }
  
  
  
  /**  Retorna el atributo a�o del objeto  */
 public String get_param_nombre()
  {
    return param_nombre;
  }
  public String get_valor()
  {
    return valor;
  }
  public String get_param_coduer()
  {
    return param_coduer;
  }
  public String get_param_indicador()
  {
    return param_indicador;
  }
  public String get_indicador()
  {
    return indicador;
  }
  public String get_param_unidad()
  {
    return param_unidad;
  }
  public String get_unidad()
  {
    return unidad;
  }
  public double get_valorn()
  {
    return valorn;
  }
  public String get_param_year()
  {
    return param_year;
  }
  public String get_label_year()
  {
    return label_year;
  }
  public String get_error()
  {
    return Error;
  }


 /** Carga una instancia del objeto de la base de datos dependiendo del
 atributo a�o que este cargado en el objeto  */

  public boolean indicador(Connection conn2)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        stmt = conn2.prepareStatement("SELECT * FROM v_indicadores" );

        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
            param_nombre   = rs.getString(1);
            valor  = rs.getString(2);
            param_coduer  = rs.getString(5);
            param_indicador  = rs.getString(7);
            indicador   = rs.getString(8);
            param_unidad= rs.getString(9);
            unidad   = rs.getString(10);
            valorn   = rs.getDouble(3);
            param_year   = rs.getString(4);
            label_year   = rs.getString(11);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }



}
