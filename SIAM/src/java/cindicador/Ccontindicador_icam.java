package cindicador;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

public class Ccontindicador_icam {

    private String Error;
    private int tamano;
    private Vector resultados;

    public Ccontindicador_icam() {
    }

    public int gettamanoresultados() {
        return resultados.size();
    }

    /**
     * * CONTENEDOR DE ESTADISTICAS
     */
    public boolean estadisticas_icam(Connection conn2, String nindicador, String ncoduer, String nestacion, String ano1, String ano2, String orden) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        Cindicador_icam var = new Cindicador_icam();
        String select = new String();
        String texto = new String();
        int porden = 11;

        select = "select * from CAM_ADMON.VSPINCAM_ICAM2011_4_FINAL where muestreo is not null ";

        if (ano1 != null && !ano1.equals("") && ano2.equals("")) {
            select = select + " and param_year = '" + ano1 + "' ";
        }
        if (ano1 != null && !ano1.equals("") && ano2 != null && !ano2.equals("")) {
            select = select + " and param_year >= '" + ano1 + "' and param_year <= '" + ano2 + "' ";
        }
        if (ncoduer != null && !ncoduer.equals("")) {
            select = select + " and coduer = '" + ncoduer + "'";
        }
        if (nestacion != null && !nestacion.equals("")) {
            select = select + " and estacion = '" + nestacion + "'";
        }

        if (nindicador.equals("SPINCAM")) {
            select = select + " and (departamento = 'CHOCO' or departamento = 'VALLE DEL CAUCA' or departamento ='CAUCA' or departamento='NARI�O') ";
        }
        if (orden != null && !orden.equals("")) {
            porden = Integer.parseInt(orden);
        }

        select = select + " order by " + porden;

        Error = select;
        try {
            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new Cindicador_icam();

                    var.setparam_ano(rs.getInt(1));
                    var.setparam_mes(rs.getInt(2));
                    var.setparam_temporada(rs.getInt(3));
                    var.setparam_muestreo(rs.getInt(4));
                    var.setparam_departamento(rs.getString(5));
                    var.setparam_sector(rs.getString(6));
                    var.setparam_coduer(rs.getString(7));
                    var.setparam_estacion(rs.getString(9));
                    var.setparam_ca_ph(rs.getDouble(13));
                    var.setparam_ca_po4(rs.getDouble(14));
                    var.setparam_ca_sst(rs.getDouble(15));
                    var.setparam_ca_no3(rs.getDouble(16));
                    var.setparam_ca_od(rs.getDouble(17));
                    var.setparam_ca_hdd(rs.getDouble(18));
                    var.setparam_ca_cte(rs.getDouble(20));
                    var.setparam_ca_cla(rs.getDouble(21));

                    var.setparam_ca_dbo(rs.getDouble(19));
                    var.setparam_icampff(rs.getDouble(22));
                    var.setparam_icampff_calificacion(rs.getString(23));
                    var.setparam_peso(rs.getDouble(24));
                    var.setparam_count_pesos(rs.getInt(25));
                    var.setparam_link(rs.getString(26));
                    var.setparam_lat(rs.getDouble(10));
                    var.setparam_lon(rs.getDouble(11));

                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;
        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     *
     *
     * /*** CONTENEDOR DE ESTADISTICAS
     */
    public boolean estadisticas_icam_valores(Connection conn2, String vsustrato, String svod, String svno3, String svpo4, String svsst, String svhdd, String svcte, String svph, String svdbo, String svcla) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        Cindicador_icam var = new Cindicador_icam();
        String select = new String();
        String texto = new String();
        int porden = 11;
        float vod, vno3, vsst, vcte, vph, vhdd, vdbo, vpo4, vcla;

        /*el valor se incializa en -1 por .....*/
        vod = -1;
        vno3 = -1;
        vsst = -1;
        vcte = -1;
        vph = -1;
        vhdd = -1;
        vdbo = -1;
        vpo4 = -1;
        vcla = -1;

        int count = 0;
        try{
            
        if (svod != null && !svod.isEmpty() ) {         
            vod = Float.parseFloat(svod);
            count++;
           // System.out.println("Count 1:"+count);
        }
        
        if (svno3 != null && !svno3.isEmpty() ) {          
            vno3 = Float.parseFloat(svno3);
            count++;
            // System.out.println("Count 2:"+count);
        }

        if (svsst != null && !svsst.isEmpty() ) {
            vsst = Float.parseFloat(svsst);
            count++;
           // System.out.println("Count 3:"+count);
        }
        
        if (svcte != null  && !svcte.isEmpty() ) {
            vcte = Float.parseFloat(svcte);
            
             count++;
           //   System.out.println("Count 4:"+count);
        }
        
         if (svph != null && !svph.isEmpty() ) {
            vph = Float.parseFloat(svph);           
            count++;
             // System.out.println("Count 5:"+count);
        }       
        
          
        if (svhdd != null  && !svhdd.isEmpty() ) {
            vhdd = Float.parseFloat(svhdd);            
            count++;
            //System.out.println("Count 7:"+count);
        } 
        
        if (svdbo != null  && !svdbo.isEmpty() ) {
            vdbo = Float.parseFloat(svdbo);           
            count++;
            //  System.out.println("Count 8:"+count);
        }  
       
         if (svpo4 != null && !svpo4.isEmpty() ) {
            vpo4 = Float.parseFloat(svpo4);          
            count++;
          //  System.out.println("Count 9:"+count);
        } 
         
           if (svcla != null && !svcla.isEmpty() ) {
            vcla = Float.parseFloat(svcla);
           // System.out.println("Count 10:"+count);
            count++;
          }
        }catch(Exception e){
          System.out.println("Error:"+e.getMessage());
       }
        
         
        //System.out.println("variable acount:"+count);
        

//        if (svod != null && !svod.equals("")) {
//            vod = Float.parseFloat(svod);
//        }
//        if (svno3 != null && !svno3.equals("")) {
//            vno3 = Float.parseFloat(svno3);
//        }
//        if (svsst != null && !svsst.equals("")) {
//            vsst = Float.parseFloat(svsst);
//        }
//        if (svcte != null && !svcte.equals("")) {
//            vcte = Float.parseFloat(svcte);
//        }
//        if (svph != null && !svph.equals("")) {
//            vph = Float.parseFloat(svph);
//        }
//        if (svhdd != null && !svhdd.equals("")) {
//            vhdd = Float.parseFloat(svhdd);
//        }
//        if (svdbo != null && !svdbo.equals("")) {
//            vdbo = Float.parseFloat(svdbo);
//        }
//        if (svpo4 != null && !svpo4.equals("")) {
//            vpo4 = Float.parseFloat(svpo4);
//        }
//
//        if (svcla != null && !svcla.equals("")) {
//            vcla = Float.parseFloat(svcla);
//        }

        select = "select ROUND (icam2011_api.f2011_icam('INCOL', '" + vsustrato + "', " + vph + ", " + vpo4 + ", " + vsst + ", " + vno3 + ", " + vod + ", " + vhdd + ", " + vdbo + ", " + vcte + ", " + vcla + "), 2) icam, ";
        select = select + "icam2011_api. F2011_CALIFICACION( icam2011_api.f2011_icam('INCOL', '" + vsustrato + "', " + vph + ", " + vpo4 + ", " + vsst + ", " + vno3 + ", " + vod + ", " + vhdd + ", " + vdbo + ", " + vcte + ", " + vcla + ") ) calificacion, ";

        select = select + "ROUND (icam2011_api.F2011CURVA_OD('" + vsustrato + "', " + vod + ", null),2) ca_od, ";
        select = select + "ROUND (icam2011_api.F2011CURVA_NO3('" + vsustrato + "', " + vno3 + ", null),2) ca_no3, ";
        select = select + "ROUND (icam2011_api.F2011CURVA_PO4('" + vsustrato + "', " + vpo4 + ", null),2) ca_po4, ";
        select = select + "ROUND (icam2011_api.F2011CURVA_SST('" + vsustrato + "', " + vsst + ", null),2) ca_sst, ";
        select = select + "ROUND (icam2011_api.F2011CURVA_HDD('" + vsustrato + "', " + vhdd + ", null),2) ca_hdd, ";
        select = select + "ROUND (icam2011_api.F2011CURVA_CTE('" + vsustrato + "', " + vcte + ", null),2) ca_cte, ";
        select = select + "ROUND (icam2011_api.F2011CURVA_PH('" + vsustrato + "', " + vph + ", null),2) ca_ph, ";
        select = select + "ROUND (icam2011_api.F2011CURVA_DBO('" + vsustrato + "', " + vdbo + ", null),2) ca_dbo, ";
        select = select + "ROUND (icam2011_api.F2011CURVA_CLA('" + vsustrato + "', " + vcla + ", null),2) ca_cla, ";

        select = select + "ROUND (icam2011_api.FR_sumpesos('INCOL', '" + vsustrato + "', " + vph + ", " + vpo4 + ", " + vsst + ", " + vno3 + ", " + vod + ", " + vhdd + ", " + vdbo + ", " + vcte + ", " + vcla + "), 2) * 100 confianza, ";
       // select = select + "ROUND (icam2011_api.FR_countpesos('INCOL', '" + vsustrato + "', " + vph + ", " + vpo4 + ", " + vsst + ", " + vno3 + ", " + vod + ", " + vhdd + ", " + vdbo + ", " + vcte + ", " + vcla + "), 2) parametros, ";
        select = select + "ROUND ("+count+",1) parametros, ";

        select = select + "icam2011_api. F2011_CALIFICACION( ROUND (icam2011_api.F2011CURVA_OD ('" + vsustrato + "', " + vod + ", null),2) ) od_calificacion, ";
        select = select + "icam2011_api. F2011_CALIFICACION( ROUND (icam2011_api.F2011CURVA_NO3('" + vsustrato + "', " + vno3 + ", null),2) ) no3_calificacion, ";
        select = select + "icam2011_api. F2011_CALIFICACION( ROUND (icam2011_api.F2011CURVA_PO4('" + vsustrato + "', " + vpo4 + ", null),2) ) po4_calificacion, ";
        select = select + "icam2011_api. F2011_CALIFICACION( ROUND (icam2011_api.F2011CURVA_SST('" + vsustrato + "', " + vsst + ", null),2) ) sst_calificacion, ";
        select = select + "icam2011_api. F2011_CALIFICACION( ROUND (icam2011_api.F2011CURVA_HDD('" + vsustrato + "', " + vhdd + ", null),2) ) hdd_calificacion, ";
        select = select + "icam2011_api. F2011_CALIFICACION( ROUND (icam2011_api.F2011CURVA_CTE('" + vsustrato + "', " + vcte + ", null),2) ) cte_calificacion, ";
        select = select + "icam2011_api. F2011_CALIFICACION( ROUND (icam2011_api.F2011CURVA_PH ('" + vsustrato + "', " + vph + ", null),2) ) ph_calificacion, ";
        select = select + "icam2011_api. F2011_CALIFICACION( ROUND (icam2011_api.F2011CURVA_DBO('" + vsustrato + "', " + vdbo + ", null),2) ) dbo_calificacion, ";
        select = select + "icam2011_api. F2011_CALIFICACION( ROUND (icam2011_api.F2011CURVA_CLA('" + vsustrato + "', " + vcla + ", null),2) ) cla_calificacion ";

        select = select + "from dual";

        Error = select;
        try {
            stmt = conn2.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    var = new Cindicador_icam();

//                   var.setparam_sector(rs.getString(6));
                    var.setparam_icampff(rs.getDouble(1));
                    var.setparam_icampff_calificacion(rs.getString(2));
                    var.setparam_ca_od(rs.getDouble(3));
                    var.setparam_ca_no3(rs.getDouble(4));
                    var.setparam_ca_po4(rs.getDouble(5));
                    var.setparam_ca_sst(rs.getDouble(6));
                    var.setparam_ca_hdd(rs.getDouble(7));
                    var.setparam_ca_cte(rs.getDouble(8));
                    var.setparam_ca_ph(rs.getDouble(9));
                    var.setparam_ca_dbo(rs.getDouble(10));

                    var.setparam_ca_cla(rs.getDouble("ca_cla"));

                    var.setparam_peso(rs.getDouble(12));
                    var.setparam_count_pesos(rs.getInt(13));
                    var.setparam_od_calificacion(rs.getString(14));
                    var.setparam_no3_calificacion(rs.getString(15));
                    var.setparam_po4_calificacion(rs.getString(16));
                    var.setparam_sst_calificacion(rs.getString(17));
                    var.setparam_hdd_calificacion(rs.getString(18));
                    var.setparam_cte_calificacion(rs.getString(19));
                    var.setparam_ph_calificacion(rs.getString(20));
                    var.setparam_dbo_calificacion(rs.getString(21));
                    var.setparam_cla_calificacion(rs.getString("cla_calificacion"));
                    resultados.addElement(var);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;
        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            System.out.println("====================== " + e.getMessage() + "," + select);
            resp = false;
        }

        return resp;
    }

    /**
     * /** retorna una instancia del contenedor de Cindicador dada su posicion
     * en el vector
     */
    public Cindicador_icam getindicador(int i) {
        return ((Cindicador_icam) resultados.elementAt(i));
    }

    /**
     * retorna el tamano del vector contenedor
     */
    public int gettamano() {
        return tamano;
    }

    /**
     * retorna el error generado por la basse de datos
     */
    public String geterror() {
        return Error;
    }
}
