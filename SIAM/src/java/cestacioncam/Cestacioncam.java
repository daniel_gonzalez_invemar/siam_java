package cestacioncam;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

public class Cestacioncam{
   private String depto;
   private String nomdepto;
   private String sector;
   private String nomsector;
   private String cod_est;
   private String nomest;
   private double lat;
   private double lon;
   private String vigente;
   private String Error;

   /**
  Metodo constructor de la clase
  */
  public void Cestacioncam(){
  }


/** Carga el atributo ano al objeto */
  public void setdepto(String depar)
  {
    depto = depar;
  }
  public void setnomdepto(String nomdepar)
  {
    nomdepto = nomdepar;
  }
  public void setsector(String sec)
  {
    sector = sec;
  }
  public void setnomsector(String nomsec)
  {
    nomsector = nomsec;
  }
  public void setestacion(String est)
  {
    cod_est = est;
  }
  public void setnomestacion(String nomesta)
  {
    nomest = nomesta;
  }
  public void setlatitud(double latitud)
  {
    lat = latitud;
  }
  public void setlongitud(double longitud)
  {
    lon = longitud;
  }
  public void setvigente(String vig)
  {
    vigente = vig;
  }



  /**  Retorna el atributo a�o del objeto  */

  public String get_depto()
  {
    return depto;
  }
  public String get_nomdepto()
  {
    return nomdepto;
  }
  public String get_sector()
  {
    return sector;
  }
  public String get_nomsector()
  {
    return nomsector;
  }
  public String get_estacion()
  {
    return cod_est;
  }
  public String get_nomestacion()
  {
    return nomest;
  }
  public double get_latitud()
  {
    return lat;
  }
  public double get_longitud()
  {
    return lon;
  }
  public String get_vigente()
  {
    return vigente;
  }


 /** Carga una instancia del objeto de la base de datos dependiendo del
 atributo a�o que este cargado en el objeto  */

  public boolean estacion(Connection conn1)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        stmt = conn1.prepareStatement("SELECT * FROM VESTACION" );

        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {

        depto     = rs.getString(1);
        nomdepto  = rs.getString(2);
        sector    = rs.getString(3);
        nomsector = rs.getString(4);
        cod_est   = rs.getString(5);
        nomest    = rs.getString(6);
        lat       = rs.getDouble(7);
        lon       = rs.getDouble(8);
        vigente   = rs.getString(9);

        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }

}
