package cestacioncam;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;


public class Ccontestacioncam {

  private String Error;
  private int tamano;
  private Vector resultados;
  public Ccontestacioncam(){}

  /**
  * carga todas las categorias de la base de datos en un vector de objetos
  */
  public boolean contenedor(Connection conn1, String ndepar, String nsector)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Cestacioncam est = new Cestacioncam();
    String select = new String();
    select = "select * from vestacion where depto is not null";

    if(ndepar != null && !ndepar.equals("")){
      select = select + " and nomdepto = '" + ndepar + "'";
    }
    if(nsector != null && !nsector.equals("")){
      select = select + " and nomsector = '" + nsector + "'";
    }
      select = select + " order by depto, sector, codigo";
    Error = select;
    try
      {
        stmt = conn1.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
                est = new Cestacioncam();
                est.setdepto(rs.getString(1));
                est.setnomdepto(rs.getString(2));
                est.setsector(rs.getString(3));
                est.setnomsector(rs.getString(4));
                est.setestacion(rs.getString(5));
                est.setnomestacion(rs.getString(6));
                est.setlatitud(rs.getDouble(7));
                est.setlongitud(rs.getDouble(8));
                est.setvigente(rs.getString(9));


              resultados.addElement(est);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }

  /**
  * carga todas las areas geograficas de la base de datos
  */
  public boolean contenedorndepto(Connection conn1)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Cestacioncam est = new Cestacioncam();
    String select = new String();
    select = "select distinct nomdepto from vestacion" ;

    Error = select;
    try
      {

        stmt = conn1.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              est = new Cestacioncam();
              est.setnomdepto(rs.getString(1));
              resultados.addElement(est);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }
/*contenedor de sectores*/
  public boolean contenedornsector(Connection conn1, String ndepar)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Cestacioncam est = new Cestacioncam();
    String select = new String();
    select = "select distinct nomsector from vestacion where nomsector is not null" ;
    if(ndepar != null && !ndepar.equals("")){
      select = select + " and nomdepto = '" + ndepar + "'";
    }
    Error = select;
    try
      {

        stmt = conn1.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              est = new Cestacioncam();
              est.setnomsector(rs.getString(1));
              resultados.addElement(est);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }


  /** retorna una instancia del contenedor de Cestacion dada su posicion en el
vector */
  public Cestacioncam getestacioncam(int i) {
    return ((Cestacioncam)resultados.elementAt(i));
  }

  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }
}
