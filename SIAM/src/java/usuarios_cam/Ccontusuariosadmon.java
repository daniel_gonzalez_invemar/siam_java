package usuarios_cam;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja el vector usuariosadmon autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br> Fecha: 03-04-2002
 */
public class Ccontusuariosadmon {

    private String Error;
    private int tamano;
    private Vector resultados;

    public Ccontusuariosadmon() {
    }

    /**
     * carga todas los usuarios de admon de la base de datos en un vector de
     * objetos contusuariosadmon
     */
    public boolean contenedor(Connection conn1) {
        boolean resp = false;
        resultados = new Vector();
        Statement stmt = null;
        ResultSet rs = null;
        Cusuariosadmon usuariosadmon = new Cusuariosadmon();
        String select = new String();
        select = "select * from pdircolectort order by nombre_co";
        Error = select;
        try {

            stmt = conn1.createStatement();
            rs = stmt.executeQuery(select);
            if (!rs.next()) {
                resp = false;
            } else {
                do {
                    usuariosadmon = new Cusuariosadmon();

                    usuariosadmon.setcod(rs.getString(2));
                    usuariosadmon.setnombre(rs.getString(3));
                    usuariosadmon.setapellido(rs.getString(4));
                    usuariosadmon.setemail(rs.getString(12));
                    usuariosadmon.setclave(rs.getString(17));

                    resultados.addElement(usuariosadmon);
                    tamano++;
                } while (rs.next());
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            resp = true;

        } catch (SQLException e) {
            Error = e.getMessage() + "," + select;
            resp = false;
        }
        return resp;
    }

    /**
     * retorna una instancia del contenedor de Cusuariosadmon dada su posicion
     * en el vector
     */
    public Cusuariosadmon getusuariosadmon(int i) {
        return ((Cusuariosadmon) resultados.elementAt(i));
    }

    /**
     * retorna el tamano del vector contenedor
     */
    public int gettamano() {
        return tamano;
    }

    /**
     * retorna el error generado por la basse de datos
     */
    public String geterror() {
        return Error;
    }
}