package usuarios_cam;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja los usuarios que administran los portales
 * autor: Rafael E. Lastra C.<br>
 * modificado por: Leonardo J. Arias<br>
 * Version: 2.0 <br>
 * Fecha: 26-08-2002
*/

public class Cusuariosadmon{

  private String codigo_co;
  private String nombre_co;
  private String apellidos_co;
  private String correo_co;
  private String clave;
  private String Error;

  /**
  Metodo constructor de la clase
  */
  public void Cusuariosadmon(){
  }

  /**
  Carga atributos al objeto
  */
  public void setcod(String cod)
  {
    codigo_co = cod;
  }
  public void setnombre(String nombre)
  {
    nombre_co = nombre;
  }
  public void setapellido(String ape)
  {
    apellidos_co = ape;
  }
  public void setemail(String email)
  {
    correo_co = email;
  }
  public void setclave(String psw)
  {
    clave = psw;
  }

  //metodos get
  /**
  Retorna el atributo a_id del objeto
  */
  public String get_cod(){
    return codigo_co;
  }
  public String get_nombre(){
    return nombre_co;
  }
  public String get_apellido(){
    return apellidos_co;
  }
  public String get_email(){
    return correo_co;
  }
  public String get_clave(){
    return clave;
  }
  public String get_error(){
    return Error;
  }

  /**
  * Carga una instancia del objeto usuariosadmon de la base de datos <br>
  * dependiendo del atributo usuario y clave que este cargado en el objeto
  */
  public int cargausuariosadmon1(Connection conn1)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        String sql = "SELECT * FROM PDIRCOLECTORT where activo_co='S' and codigo_co =? and CLAVE =?";
        stmt = conn1.prepareStatement(sql);
        stmt.setString(1, codigo_co);
        stmt.setString(2, clave);
         // System.out.println("sql:"+sql);
        rs = stmt.executeQuery ();
        if (!rs.next()){
          resp = false;
          i = 0;
        }
        else
        {
          codigo_co     = rs.getString(2);
          nombre_co     = rs.getString(3);
          apellidos_co  = rs.getString(4);
          correo_co     = rs.getString(12);
          clave         = rs.getString(17);
          i = 1;
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return i;
   }
}