package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

public class Ccontficha_cat  {

  private String Error;
  private int tamano;
  private Vector resultados;
  public Ccontficha_cat() {}

  /**
  * carga todas las categorias de la base de datos en un vector de objetos
  */
  public boolean contenedor(Connection conn1, String clave, String numero )
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Cficha_cat ficha = new Cficha_cat();
    String select = new String();
    select = "SELECT * FROM vm_ficha where seccion is not null ";

    if(clave != null && !clave.equals("")){
      select = select + " AND  clave  = '" + clave + "'";
    }
    if(numero != null && !numero.equals("")){
      select = select + " AND noinv = " + numero;
    }

    Error = select;
    try
      {

        stmt = conn1.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              ficha = new Cficha_cat();

				ficha.setseccion 	(rs.getString(1));
				ficha.setnocat 		(rs.getInt(2));
				ficha.setnoinv 		(rs.getInt(4));
				ficha.setnoorig_dest 	(rs.getString(5));
				ficha.setejemplares 	(rs.getInt(6));
				ficha.settipo 		(rs.getString(7));
				ficha.setubicacion 	(rs.getString(8));
				ficha.setubi_geo 	(rs.getString(9));
				ficha.setclave 		(rs.getString(11));
				ficha.setespecie 	(rs.getString(12));
				ficha.setautor 		(rs.getString(13));
				ficha.setnombre_comun 	(rs.getString(15));
				ficha.setfamilia 	(rs.getString(21));
				ficha.setorden 		(rs.getString(22));
				ficha.setclase 		(rs.getString(23));
				ficha.setfecha_recibido 	(rs.getString(27));
				ficha.settipo_especimen 	(rs.getString(28));
				ficha.setpreservativo 	(rs.getString(29));
				ficha.setadquisicion 	(rs.getString(31));
				ficha.setproyecto 	(rs.getString(32));
				ficha.setprocedencia 	(rs.getString(33));
				ficha.setcolectado_por 	(rs.getString(34));
				ficha.setfechacap 	(rs.getString(36));
				ficha.setmetodo_captura 	(rs.getString(39));
				ficha.setidentificado_por (rs.getString(40));
				ficha.setfechaide 	(rs.getString(41));
				ficha.settecnica_ident 	(rs.getString(42));
				ficha.setpais 		(rs.getString(43));
				ficha.setdepto 		(rs.getString(44));
				ficha.setmcpio 		(rs.getString(45));
				ficha.setlocalidad 	(rs.getString(46));
				ficha.setcuer_agua 	(rs.getString(47));
				ficha.setnombrecagua 	(rs.getString(48));
				ficha.setlatini 		(rs.getString(49));
				ficha.setlatfin 		(rs.getString(50));
				ficha.setlonini 		(rs.getString(51));
				ficha.setlonfin 		(rs.getString(52));
				ficha.setbarco 		(rs.getString(55));
				ficha.setcamp 		(rs.getString(56));
				ficha.setestacion 	(rs.getString(57));
				ficha.setprof_agua 	(rs.getString(58));
				ficha.setprof_captura 	(rs.getString(59));
				ficha.setambiente 	(rs.getString(60));
				ficha.settemp_aire 	(rs.getString(61));
				ficha.settemp_agua 	(rs.getString(62));
				ficha.setsalinidad 	(rs.getFloat(63));
				ficha.setphylum 	(rs.getInt(26));
                                ficha.setobjetos        (rs.getString(30));

//				ficha.setzona 		(rs.getString(53));
//				ficha.setprmin 		(rs.getFloat(54));
//				ficha.setprmax 		(rs.getFloat(55));
              resultados.addElement(ficha);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }


  /** retorna una instancia del contenedor de Ccategoria dada su posicion en el vector */
  public Cficha_cat getficha_cat(int i) {
    return ((Cficha_cat)resultados.elementAt(i));
  }

  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }
}