package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Cinfofilogenia (la filogenia de las especies)
 * autor: Leonardo J. Arias Aleman.<br>
 * Version: 1.0 <br>
 * Fecha: 06-05-2004
*/

public class Cinfofilogenia  {
  private String a_filogenia;

  /**
  * Metodo constructor
  */
  public void Cinfofilogenia(){}

  /**
  Carga el atributo a_filogenia al objeto
  */
  public void setafilogenia(String filogenia)
  {
    a_filogenia = filogenia;
  }



  //metodos get
  /**
  Retorna el atributo a_filogenia del objeto
  */
  public String get_afilogenia(){
    return a_filogenia;
  }


}