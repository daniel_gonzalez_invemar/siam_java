

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja el vector niveltax
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 21-01-2002
*/

public class Ccontniveltax  {
  private String Error;
  private int tamano;
  private Vector resultados;
  /**
  * Metodo constructor
  */
  public void Ccontniveltax(){}
  /**
  * carga todas las niveltax de la base de datos en un vector de objetos
  * niveltax
  */
  public boolean contenedor(Connection conn1)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Cniveltax niveltax = new Cniveltax();
    String select = "select * from cnivelestax where nivel_ta > 36 and nivel_ta <= 55";
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              niveltax = new Cniveltax();
              niveltax.a_nivel = rs.getString("nivel_ta");
              niveltax.a_descripcion = rs.getString("descripcion_ta");
              resultados.addElement(niveltax);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }
  /**
  * carga todas las niveltax de la base de datos en un vector de objetos
  * niveltax dependiendo del primer nivel
  */
  public boolean contenedor1(Connection conn1, String nivel)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Cniveltax niveltax = new Cniveltax();
    String select = "select * from cnivelestax where nivel_ta > " + nivel;
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              niveltax = new Cniveltax();
              niveltax.a_nivel = rs.getString("nivel_ta");
              niveltax.a_descripcion = rs.getString("descripcion_ta");
              resultados.addElement(niveltax);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + ", " + select;
      resp = false;
     }
     return resp;
  }
  /**
  * retorna una instancia del contenedor de niveltax dada su posicion en el vector
  */
  public Cniveltax getniveltax(int i) {
    return ((Cniveltax)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }
}
