

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Cinforeferencias (la lista de referencias bibliograficas)
 * autor: Leonardo J. Arias Aleman.<br>
 * Version: 1.0 <br>
 * Fecha: 06-05-2004
*/

public class Ccontinfosinonimia  {

  private String Error;
  private int tamano;
  private Vector resultados;
  /**
  * Metodo constructor
  */
  public void Ccontinfosinonimia(){}

  /**
  * carga todas las imagenes de la base de datos en un vector de objetos
  * inforeferencias dependiendo de la clave
  */
  public boolean contenedor(Connection conn1, String clave)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Cinfosinonimia infosinonimia = new Cinfosinonimia();

    String select = "select s.DESCRIPCION_SN nombre, s.AUTORES_SN autor, s.PHYLUM_SN phylum, s.CLAVE_SN clave, s.SECUENCIABBG_SN bibliografia, s.NOTAS_SN notas from csinonimias s where s.CLAVE_SN = '" + clave + "' and ( s.TIPO_SN >0 and s.TIPO_SN <20) order by nroregistro_sn desc";
    
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              infosinonimia = new Cinfosinonimia();
              infosinonimia.setanombre(rs.getString(1));
              infosinonimia.setaautor(rs.getString(2));
              infosinonimia.setaphylum(rs.getString(3));
              infosinonimia.setaclave(rs.getString(4));
              infosinonimia.setabibliografia(rs.getString(5));
              infosinonimia.setanotas(rs.getString(6));

              resultados.addElement(infosinonimia);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }
  /**
  * retorna una instancia del contenedor de inforeferencias dada su posicion en el vector
  */
  public Cinfosinonimia getinfosinonimia(int i) {
    return ((Cinfosinonimia)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }
}