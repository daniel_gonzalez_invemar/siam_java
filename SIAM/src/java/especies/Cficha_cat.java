package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

public class Cficha_cat{
   private String 	seccion;
   private int 		nocat;
   private int 		noinv;
   private String 	noorig_dest;
   private int 		ejemplares;
   private String 	tipo;
   private String 	ubicacion;
   private String 	ubi_geo;
   private String 	clave;
   private String 	especie;
   private String 	autor;
   private String 	nombre_comun;
   private String 	familia;
   private String 	orden;
   private String 	clase;
   private String 	fecha_recibido;
   private String 	tipo_especimen;
   private String 	preservativo;
   private int 	 	diapositivas;
   private int 	 	fotos;
   private int 	 	dibujos;
   private int 	 	rx;
   private int 	 	montajes;
   private int 	 	videos;
   private String 	adquisicion;
   private String 	proyecto;
   private String 	procedencia;
   private String 	colectado_por;
   private String 	fechacap;
   private String 	metodo_captura;
   private String 	identificado_por;
   private String 	fechaide;
   private String 	tecnica_ident;
   private String 	pais;
   private String 	depto;
   private String 	mcpio;
   private String 	localidad;
   private String 	cuer_agua;
   private String 	nombrecagua;
   private String 	latini;
   private String 	latfin;
   private String 	lonini;
   private String 	lonfin;
   private String 	barco;
   private String 	camp;
   private String 	estacion;
   private String 	prof_agua;
   private String 	prof_captura;
   private String 	ambiente;
   private String 	temp_aire;
   private String 	temp_agua;
   private float 	salinidad;
   private int 	 	phylum;
   private String       objetos;
   private String  	Error;


  /**
  Metodo constructor de la clase
  */
  public void Cficha_cat(){
  }


/** Carga el atributo al objeto */
  public void setseccion(String aseccion)
  {
    seccion = aseccion;
  }
  public void setnocat(int anocat)
  {
    nocat = anocat;
  }
  public void setnoinv(int anoinv)
  {
    noinv = anoinv;
  }
  public void setnoorig_dest(String anoorig_dest)
  {
    noorig_dest = anoorig_dest;
  }
  public void setejemplares(int aejemplares)
  {
    ejemplares = aejemplares;
  }
  public void settipo(String atipo)
  {
    tipo = atipo;
  }
  public void setubicacion(String aubicacion)
  {
    ubicacion = aubicacion;
  }
  public void setubi_geo(String aubi_geo)
  {
    ubi_geo = aubi_geo;
  }
  public void setclave(String aclave)
  {
    clave = aclave;
  }
  public void setespecie(String aespecie)
  {
    especie = aespecie;
  }
  public void setautor(String aautor)
  {
    autor = aautor;
  }
  public void setnombre_comun(String anombre_comun)
  {
    nombre_comun = anombre_comun;
  }
  public void setfamilia(String afamilia)
  {
    familia = afamilia;
  }
  public void setorden(String aorden)
  {
    orden = aorden;
  }
  public void setclase(String aclase)
  {
    clase = aclase;
  }
  public void setfecha_recibido(String afecha_recibido)
  {
    fecha_recibido = afecha_recibido;
  }
  public void settipo_especimen(String atipo_especimen)
  {
    tipo_especimen = atipo_especimen;
  }
  public void setpreservativo(String apreservativo)
  {
    preservativo = apreservativo;
  }
  public void setdiapositivas(int adiapositivas)
  {
    diapositivas = adiapositivas;
  }
  public void setfotos(int afotos)
  {
    fotos = afotos;
  }
  public void setdibujos(int adibujos)
  {
    dibujos = adibujos;
  }
  public void setrx(int arx)
  {
    rx = arx;
  }
  public void setmontajes(int amontajes)
  {
    montajes = amontajes;
  }
  public void setvideos(int avideos)
  {
    videos = avideos;
  }
  public void setadquisicion(String aadquisicion)
  {
    adquisicion = aadquisicion;
  }
  public void setproyecto(String aproyecto)
  {
    proyecto = aproyecto;
  }
  public void setprocedencia(String aprocedencia)
  {
    procedencia = aprocedencia;
  }
  public void setcolectado_por(String acolectado_por)
  {
    colectado_por = acolectado_por;
  }
  public void setfechacap(String afechacap)
  {
    fechacap = afechacap;
  }
  public void setmetodo_captura(String ametodo_captura)
  {
    metodo_captura = ametodo_captura;
  }
  public void setidentificado_por(String aidentificado_por)
  {
    identificado_por = aidentificado_por;
  }
  public void setfechaide(String afechaide)
  {
    fechaide = afechaide;
  }
  public void settecnica_ident(String atecnica_ident)
  {
    tecnica_ident = atecnica_ident;
  }
  public void setpais(String apais)
  {
    pais = apais;
  }
  public void setdepto(String adepto)
  {
    depto = adepto;
  }
  public void setmcpio(String amcpio)
  {
    mcpio = amcpio;
  }
  public void setlocalidad(String alocalidad)
  {
    localidad = alocalidad;
  }
  public void setcuer_agua(String acuer_agua)
  {
    cuer_agua = acuer_agua;
  }
  public void setnombrecagua(String anombrecagua)
  {
    nombrecagua = anombrecagua;
  }
  public void setlatini(String alatini)
  {
    latini = alatini;
  }
  public void setlatfin(String alatfin)
  {
    latfin = alatfin;
  }
  public void setlonini(String alonini)
  {
    lonini = alonini;
  }
  public void setlonfin(String alonfin)
  {
    lonfin = alonfin;
  }
  public void setbarco(String abarco)
  {
    barco = abarco;
  }
  public void setcamp(String acamp)
  {
    camp = acamp;
  }
  public void setestacion(String aestacion)
  {
    estacion = aestacion;
  }
  public void setprof_agua(String aprof_agua)
  {
    prof_agua = aprof_agua;
  }
  public void setprof_captura(String aprof_captura)
  {
    prof_captura = aprof_captura;
  }
  public void setambiente(String aambiente)
  {
    ambiente = aambiente;
  }
  public void settemp_aire(String atemp_aire)
  {
    temp_aire = atemp_aire;
  }
  public void settemp_agua(String atemp_agua)
  {
    temp_agua = atemp_agua;
  }
  public void setsalinidad(float asalinidad)
  {
    salinidad = asalinidad;
  }
  public void setphylum(int aphylum)
  {
    phylum = aphylum;
  }
  public void setobjetos(String aobjetos)
    {
      objetos = aobjetos;
    }




  /**  Retorna el atributo del objeto  */
  public String get_seccion()
  {
    return     seccion ;
  }
  public int get_nocat()
  {
    return     nocat ;
  }
  public int get_noinv()
  {
    return     noinv ;
  }
  public String get_noorig_dest()
  {
    return     noorig_dest ;
  }
  public int get_ejemplares()
  {
    return     ejemplares ;
  }
  public String get_tipo()
  {
    return     tipo ;
  }
  public String get_ubicacion()
  {
    return     ubicacion ;
  }
  public String get_ubi_geo()
  {
    return     ubi_geo ;
  }
  public String get_clave()
  {
    return     clave ;
  }
  public String get_especie()
  {
    return     especie ;
  }
  public String get_autor()
  {
    return     autor ;
  }
  public String get_nombre_comun()
  {
    return     nombre_comun ;
  }
  public String get_familia()
  {
    return     familia ;
  }
  public String get_orden()
  {
    return     orden ;
  }
  public String get_clase()
  {
    return     clase ;
  }
  public String get_fecha_recibido()
  {
    return     fecha_recibido ;
  }
  public String get_tipo_especimen()
  {
    return     tipo_especimen ;
  }
  public String get_preservativo()
  {
    return     preservativo ;
  }
  public int get_diapositivas()
  {
    return     diapositivas ;
  }
  public int get_fotos()
  {
    return     fotos ;
  }
  public int get_dibujos()
  {
    return     dibujos ;
  }
  public int get_rx()
  {
    return     rx ;
  }
  public int get_montajes()
  {
    return     montajes ;
  }
  public int get_videos()
  {
    return     videos ;
  }
  public String get_adquisicion()
  {
    return     adquisicion ;
  }
  public String get_proyecto()
  {
    return     proyecto ;
  }
  public String get_procedencia()
  {
    return     procedencia ;
  }
  public String get_colectado_por()
  {
    return     colectado_por ;
  }
  public String get_fechacap()
  {
    return     fechacap ;
  }
  public String get_metodo_captura()
  {
    return     metodo_captura ;
  }
  public String get_identificado_por()
  {
    return     identificado_por ;
  }
  public String get_fechaide()
  {
    return     fechaide ;
  }
  public String get_tecnica_ident()
  {
    return     tecnica_ident ;
  }
  public String get_pais()
  {
    return     pais ;
  }
  public String get_depto()
  {
    return     depto ;
  }
  public String get_mcpio()
  {
    return     mcpio ;
  }
  public String get_localidad()
  {
    return     localidad ;
  }
  public String get_cuer_agua()
  {
    return     cuer_agua ;
  }
  public String get_nombrecagua()
  {
    return     nombrecagua ;
  }
  public String get_latini()
  {
    return     latini ;
  }
  public String get_latfin()
  {
    return     latfin ;
  }
  public String get_lonini()
  {
    return     lonini ;
  }
  public String get_lonfin()
  {
    return     lonfin ;
  }
  public String get_barco()
  {
    return     barco ;
  }
  public String get_camp()
  {
    return     camp ;
  }
  public String get_estacion()
  {
    return     estacion ;
  }
  public String get_prof_agua()
  {
    return     prof_agua ;
  }
  public String get_prof_captura()
  {
    return     prof_captura ;
  }
  public String get_ambiente()
  {
    return     ambiente ;
  }
  public String get_temp_aire()
  {
    return     temp_aire ;
  }
  public String get_temp_agua()
  {
    return     temp_agua ;
  }
  public float get_salinidad()
  {
    return     salinidad ;
  }
  public int get_phylum()
  {
    return     phylum ;
  }
  public String get_objetos()
    {
      return     objetos ;
   }



 /** Carga una instancia del objeto de la base de datos dependiendo del
 atributo que este cargado en el objeto  */

  public boolean ficha_cat(Connection conn1)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        stmt = conn1.prepareStatement("select * FROM vm_ficha where " );
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {

            seccion        = rs.getString(1);
            nocat          = rs.getInt(2);
            noinv          = rs.getInt(4);
            noorig_dest    = rs.getString(5);
            ejemplares     = rs.getInt(6);
            tipo           = rs.getString(7);
            ubicacion      = rs.getString(8);
            ubi_geo        = rs.getString(9);
            clave          = rs.getString(11);
            especie        = rs.getString(12);
            autor          = rs.getString(13);
            nombre_comun   = rs.getString(14);
            familia        = rs.getString(20);
            orden          = rs.getString(21);
            clase          = rs.getString(22);
            fecha_recibido = rs.getString(26);
            tipo_especimen = rs.getString(27);
            preservativo   = rs.getString(28);
            adquisicion    = rs.getString(30);
            proyecto       = rs.getString(31);
            procedencia    = rs.getString(32);
            colectado_por  = rs.getString(33);
            fechacap       = rs.getString(35);
            metodo_captura = rs.getString(38);
            identificado_por = rs.getString(39);
            fechaide       = rs.getString(40);
            tecnica_ident  = rs.getString(41);
            pais           = rs.getString(42);
            depto          = rs.getString(43);
            mcpio          = rs.getString(44);
            localidad      = rs.getString(45);
            cuer_agua      = rs.getString(46);
            nombrecagua    = rs.getString(47);
            latini         = rs.getString(48);
            latfin         = rs.getString(49);
            lonini         = rs.getString(50);
            lonfin         = rs.getString(51);
            barco          = rs.getString(54);
            camp           = rs.getString(55);
            estacion       = rs.getString(56);
            prof_agua      = rs.getString(57);
            prof_captura   = rs.getString(58);
            ambiente       = rs.getString(59);
            temp_aire      = rs.getString(60);
            temp_agua      = rs.getString(61);
            salinidad      = rs.getFloat(62);
            phylum         = rs.getInt(25);
            objetos        = rs.getString(29);

//				zona 		= rs.getString(53);
//				prmin 		= rs.getFloat(54);
//				prmax 		= rs.getFloat(55);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }

}