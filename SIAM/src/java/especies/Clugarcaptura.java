

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Clugarcaptura(sitio de captura del especimen )
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 12-06-2002
*/

public class Clugarcaptura  {

  private String a_clave;
  private String a_pais;
  private String a_municipio;
  private String a_lugar;
  private String a_descripcion;
  private String a_notas;
  public String Error;

  /**
  * Metodo constructor
  */
  public void Clugarcaptura(){}
  /**
  * carga el atributo a_clave
  */
  public void setaclave(String clave){
    a_clave = clave;
  }
  /**
  * carga el atributo a_pais
  */
  public void setapais(String pais){
    a_pais = pais;
  }
  /**
  * carga el atributo a_municipio
  */
  public void setamunicipio(String municipio){
    a_municipio = municipio;
  }
  /**
  * carga el atributo a_lugar
  */
  public void setalugar(String lugar){
    a_lugar = lugar;
  }
  /**
  * carga el atributo a_descripcion
  */
  public void setadescripcion(String descripcion){
    a_descripcion = descripcion;
  }
  /**
  * carga el atributo a_notas
  */
  public void setanotas(String notas){
    a_notas = notas;
  }
  //metodos get
  /**
  Retorna el atributo a_clave del objeto
  */
  public String get_aclave(){
    return a_clave;
  }
  /**
  Retorna el atributo a_pais del objeto
  */
  public String get_apais(){
    return a_pais;
  }
  /**
  Retorna el atributo a_municipio del objeto
  */
  public String get_amunicipio(){
    return a_municipio;
  }
  /**
  Retorna el atributo a_lugar del objeto
  */
  public String get_alugar(){
    return a_lugar;
  }
  /**
  Retorna el atributo a_descripcion del objeto
  */
  public String get_adescripcion(){
    return a_descripcion;
  }
  /**
  Retorna el atributo a_notas del objeto
  */
  public String get_anotas(){
    return a_notas;
  }
  /**
  * Carga una instancia del objeto lugarcaptura de la base de datos <br>
  * dependiendo del atributo a_clave que este cargado en el objeto
  */
  public boolean cargalugarcaptura(Connection conn1)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        stmt = conn1.prepareStatement("select cm.nroinvemar_mu noinv, cpaisest.nombre_pa pais, clst_depto.nombre||' / '||clst_mcpio.nombre mcpio, cl.lugar localidad, ZON.DESCRIPCION zona, cm.NOTAS_CAPT notas from cmuestra cm, clocalidad cl, clst_toponimiat cls, cpaisest, clst_depto, clst_mcpio, condiciones con, CMLST_ZONAS_PROTEGIDAS ZON where ((cm.secuencia_es = cl.secuencia_loc(+)) and (cl.TOPONIMIA_LOC = cls.toponimo_tp(+)) and (cpaisest.codigo_pa(+) = cls.pais_mn) and (clst_depto.cod(+) = trunc(cls.toponimo_tp/10000000)) and (clst_mcpio.cod(+) = trunc(cls.toponimo_tp/10000)) and (con.secuencia_con(+) = cm.secuencia_cr) AND (ZON.CODIGO(+) = cl.ZONA_PROTEGIDA) and cm.NROINVEMAR_MU = ? )" );

        //SELECT  Pais_Loc , Clst_Toponimia.Toponimo_texto , Lugar, CMLST_Zonas_Protegidas.descripcion , Notas_ES  FROM  CMLugar_Capturat10jun, CLOCALIDAD, CMLST_Zonas_Protegidas,CLst_Toponimia WHERE CMLugar_Capturat10jun.NroInvemar_es = ? AND CMLugar_Capturat10jun.Secuencia_Es  = Clocalidad.Secuencia_Loc AND Clocalidad.Toponimia_Loc  = Clst_Toponimia.Toponimo_Tp AND CLocalidad.Zona_Protegida = CMLST_Zonas_Protegidas.Codigo(+)

        stmt.setString(1,a_clave);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          a_pais = rs.getString(2);
          a_municipio = rs.getString(3);
          a_lugar = rs.getString(4);
          a_descripcion = rs.getString(5);
          a_notas = rs.getString(6);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }
}