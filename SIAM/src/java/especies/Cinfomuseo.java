

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Cinfomuseo(informacion de la muestra )
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 27-05-2002
*/

public class Cinfomuseo  {

  private String a_clave;
  private String a_numero;
  private String a_seccion;
  private String a_ubicacion;
  private String a_fecha;
  private String a_especimenes;
  private String a_preservadoen;
  private String a_concentracion;
  private String a_tipoespecimen;
  private String a_partepreservada;
  private String a_dimensiones;
  private String Error;

  /**
  * Metodo constructor
  */
  public void Cinfomuseo(){}
  /**
  * carga el atributo a_clave
  */
  public void setaclave(String clave){
    a_clave = clave;
  }
  /**
  * carga el atributo a_numero
  */
  public void setanumero(String numero){
    a_numero = numero;
  }
  /**
  * carga el atributo a_seccion
  */
  public void setaseccion(String seccion){
    a_seccion = seccion;
  }
  /**
  * carga el atributo a_ubicacion
  */
  public void setaubicacion(String ubicacion){
    a_ubicacion = ubicacion;
  }
  /**
  * carga el atributo a_fecha
  */
  public void setafecha(String fecha){
    a_fecha = fecha;
  }
  /**
  * carga el atributo a_especimenes
  */
  public void setaespecimenes(String especimenes){
    a_especimenes = especimenes;
  }
  /**
  * carga el atributo a_preservadoen
  */
  public void setapreservadoen(String preservadoen){
    a_preservadoen = preservadoen;
  }
  /**
  * carga el atributo a_concentracion
  */
  public void setaconcentracion(String concentracion){
    a_concentracion = concentracion;
  }
  /**
  * carga el atributo a_tipoespecimen
  */
  public void setatipoespecimen(String tipoespecimen){
    a_tipoespecimen = tipoespecimen;
  }
  /**
  * carga el atributo a_partepreservada
  */
  public void setapartepreservada(String partepreservada){
    a_partepreservada = partepreservada;
  }
  /**
  * carga el atributo a_dimensiones
  */
  public void setadimensiones(String dimensiones){
    a_dimensiones = dimensiones;
  }
  //metodos get
  /**
  Retorna el atributo a_clave del objeto
  */
  public String get_aclave(){
    return a_clave;
  }
  /**
  Retorna el atributo a_numero del objeto
  */
  public String get_anumero(){
    return a_numero;
  }
  /**
  Retorna el atributo a_seccion del objeto
  */
  public String get_aseccion(){
    return a_seccion;
  }
  /**
  Retorna el atributo a_ubicacion del objeto
  */
  public String get_aubicacion(){
    return a_ubicacion;
  }
  /**
  Retorna el atributo a_fecha del objeto
  */
  public String get_afecha(){
    return a_fecha;
  }
  /**
  Retorna el atributo a_especimenes del objeto
  */
  public String get_aespecimenes(){
    return a_especimenes;
  }
  /**
  Retorna el atributo a_preservadoen del objeto
  */
  public String get_apreservadoen(){
    return a_preservadoen;
  }
  /**
  Retorna el atributo a_concentracion del objeto
  */
  public String get_aconcentracion(){
    return a_concentracion;
  }
  /**
  Retorna el atributo a_tipoespecimen del objeto
  */
  public String get_atipoespecimen(){
    return a_tipoespecimen;
  }
  /**
  Retorna el atributo a_partepreservada del objeto
  */
  public String get_apartepreservada(){
    return a_partepreservada;
  }
  /**
  Retorna el atributo a_dimensiones del objeto
  */
  public String get_adimensiones(){
    return a_dimensiones;
  }
  /**
  * Carga una instancia del objeto infomuseo de la base de datos <br>
  * dependiendo del atributo clave y numero que este cargado en el objeto
  */
   public boolean cargainfomuseo(Connection conn1)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      { 
        stmt = conn1.prepareStatement("SELECT f.SECCION, f.NOCAT, f.UBICACION, f.FECHA_RECIBIDO, f.EJEMPLARES, f.PRESERVATIVO, f.TIPO_ESPECIMEN parte_conservada, f.TIPO FROM  vm_ficha f WHERE f.CLAVE = ? and f.NOINV = ? " );
        stmt.setString(1, a_clave);
        stmt.setString(2, a_numero);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          a_seccion = rs.getString(1);
          a_numero = rs.getString(2);
          a_ubicacion = rs.getString(3);
          a_fecha = rs.getString(4);
          a_especimenes = rs.getString(5);
          a_preservadoen = rs.getString(6);
          a_partepreservada = rs.getString(7);
          a_tipoespecimen = rs.getString(8);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }

}