

package especies;


import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Categorias CITES
 * autor: Leonardo J. Arias A.<br>
 * Version: 1.0 <br>
 * Fecha: 20-11-2002
*/
public class Ccategoriacites{

public String a_codigo;
public String a_descripcion;

/**
  Metodo constructor
*/
  public void Ccategoriacites(){}
/**
  Retorna el atributo a_codigo del objeto
*/
  public String get_acodigo(){
    return a_codigo;
  }
/**
  Retorna el atributo a_descripcion del objeto
*/
  public String get_adescripcion(){
    return a_descripcion;
  }
}
