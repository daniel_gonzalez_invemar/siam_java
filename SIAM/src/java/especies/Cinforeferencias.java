package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Cinforeferencias (la lista de referencias bibliograficas)
 * autor: Leonardo J. Arias Aleman.<br>
 * Version: 1.0 <br>
 * Fecha: 06-05-2004
*/

public class Cinforeferencias  {
  private String a_referencia;
  private String a_codigo;

  /**
  * Metodo constructor
  */
  public void Cinforeferencias(){}

  /**
  Carga el atributo a_referencia al objeto
  */
  public void setareferencia(String referencia)
  {
    a_referencia = referencia;
  }
  /**
  Carga el atributo a_codigo al objeto
  */
  public void setacodigo(String codigo)
  {
    a_codigo = codigo;
  }



  //metodos get
  /**
  Retorna el atributo a_referencia del objeto
  */
  public String get_areferencia(){
    return a_referencia;
  }
  /**
  Retorna el atributo a_codigo del objeto
  */
  public String get_acodigo(){
    return a_codigo;
  }


}