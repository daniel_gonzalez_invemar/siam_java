package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja el vector ficha
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 23-05-2002
*/
public class Ccontficha  {

  private String Error;
  private int tamano;
  private Vector resultados;
  /**
  * Metodo constructor
  */
  public void Ccontficha(){}

  /**
  * ESTE ES EL CONTENEDOR DEL BOTON DE FICHA INFORMACION DE LA ESPECIE
  * carga todas las taxones de la base de datos en un vector de objetos
  * ficha dependiendo de los parámettros seleccionados
  */
  public boolean contenedor_sibficha1(Connection conn1, String phylum, String conse, String conse1, String ncites, String ncatlibrorojo, String nlibrorojo, String nprof1, String nprof2, String nvulgar, String nambiente, String nzona, String neco, String nreg, String clase, String orden, String genero, String reino, String especie, String subclase, String superfamilia, String infraorden, String nphylum, String ngenero, String nespecie)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Cficha ficha = new Cficha();
    String select =  "select distinct clave, nombre, autores, nvulgar, familia, phylum, codphylum from curador.lista_fespecie where clave is not null ";

    if(phylum != null && !phylum.equals("") && !phylum.equals("null")){
      select = select + " and  codphylum = " + phylum + " ";
    }
    if(conse != null && !conse.equals("") && !conse.equals("null") && conse1 != null && !conse1.equals("") && !conse1.equals("")){
      select = select + " AND  consec >= " + conse + " AND   consec < " + conse1 + " ";
    }
    if(ncites != null && !ncites.equals("") && !ncites.equals("null")){
      select = select + " and CITES is not null";
    }
    if(ncatlibrorojo != null && !ncatlibrorojo.equals("") && !ncatlibrorojo.equals("null")){
      select = select + " and s_catlibrorojo = '" + ncatlibrorojo + "' ";
    }
    if(nlibrorojo != null && !nlibrorojo.equals("") && !nlibrorojo.equals("null")){
      select = select + " and s_catlibrorojo is not null ";
    }
    if(nprof1 != null && !nprof1.equals("") && !nprof1.equals("null")){
      select = select + " and PRMIN >= '" + nprof1 + "' ";
    }
    if(nprof2 != null && !nprof2.equals("") && !nprof2.equals("null")){
      select = select + " and PRMAX <= '" + nprof2 + "' ";
    }
    if(nvulgar != null && !nvulgar.equals("") && !nvulgar.equals("null")){
      select = select + " and CLAVE = '" + nvulgar + "' ";
    }
    if(nambiente != null && !nambiente.equals("") && !nambiente.equals("null")){
      select = select + " and cod_amb = '" + nambiente + "' ";
    }
    if(nzona != null && !nzona.equals("") && !nzona.equals("null")){
      select = select + " and cod_zon = '" + nzona + "' ";
    }
    if(neco != null && !neco.equals("") && !neco.equals("null")){
      select = select + " and ecorregion LIKE '" + neco + "%' ";
    }
    if(nreg != null && !nreg.equals("") && !nreg.equals("null")){
      select = select + " and ecorregion LIKE '" + nreg + "%' ";
    }
    if(clase != null && !clase.equals("") && !clase.equals("null")){
      select = select + " and clase = '" + clase + "' ";
    }
    if(orden != null && !orden.equals("") && !orden.equals("null")){
      select = select + " and orden = '" + orden + "' ";
    }
    if(genero != null && !genero.equals("") && !genero.equals("null")){
      select = select + " and genero = '" + genero + "' ";
    }
    if(reino != null && !reino.equals("") && !reino.equals("null")){
      select = select + " and reino = '" + reino + "' ";
    }
    if(especie != null && !especie.equals("") && !especie.equals("null")){
      select = select + " and  especie = '" + especie + "' ";
    }
    if(nphylum != null && !nphylum.equals("") && !nphylum.equals("null") ){
      select = select + " and  phylum = '" + nphylum + "' ";
    }
    if(ngenero != null && !ngenero.equals("") && !ngenero.equals("null")){
      select = select + " and  genero LIKE UPPER('%" + ngenero + "%') ";
    }
    if(nespecie != null && !nespecie.equals("") && !nespecie.equals("null")){
      select = select + " and  especie LIKE UPPER('%" + nespecie + "%') ";
    }
    if(subclase != null && !subclase.equals("") && !subclase.equals("null")){
      select = select + " and subclase = '" + subclase + "' ";
    }
    if(superfamilia != null && !superfamilia.equals("") && !superfamilia.equals("null")){
      select = select + " and superfamilia = '" + superfamilia + "' ";
    }
    if(infraorden != null && !infraorden.equals("") && !infraorden.equals("null")){
      select = select + " and infraorden = '" + infraorden + "' ";
    }


    select = select + " order by nombre";

    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              ficha = new Cficha();
              ficha.setaclave(rs.getString(1));
              ficha.setadescripcion(rs.getString(2));
              ficha.setaautor(rs.getString(3));
              ficha.setanvulgar(rs.getString(4));
              ficha.setafamilia(rs.getString(5));
              ficha.setaphylum(rs.getString(6));
              ficha.setacodphylum(rs.getString(7));

              resultados.addElement(ficha);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + " " + select;

      resp = false;
     }
     return resp;
  }


  /**
  * ESTE ES EL CONTENEDOR DEL BOTON DE MUSEO
  * carga todas las taxones de la base de datos en un vector de objetos
  * ficha dependiendo de los parámettros seleccionados
  */
  public boolean contenedor_sibmuseo1(Connection conn1, String phylum, String conse, String conse1, String ncites, String ncatlibrorojo, String nlibrorojo, String nprof1, String nprof2, String nvulgar, String nambiente, String nzona, String neco, String nreg, String ngenero, String nespecie)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Cficha ficha = new Cficha();
    String select = "SELECT ne.Clave CLAVE, ne.nombre, ne.Autor AutorES, cm. NROINVEMAR_MU NROINVEMAR, FVULGAR(ne.Clave) NVULGAR, UPPER(FFAMILIA(ne.Clave)) FAMILIA, FPHYLUM(ne.Clave) PHYLUM, ne.PHYLUM COD_PHYLUM, CON.CITES, SUBSTR(CON.CATEGORIAS_C,0,2) S_CATLIBROROJO, SUBSTR(CON.CATEGORIA_UICN_GLOBAL,0,2) S_CATUICN, LOC.AMBIENTE_LOC CODAMB, LOC.ZONA_PROTEGIDA CODZON, CM.PROF_CAPT_MIN PRMIN, CM.PROF_CAPT_MAX PRMAX, cm.SECCION_MU||'-'||cm.NROCATALOGO catalogo, ce.CLAVE_ES claveesp, loc.ECORREGION_LOC eco, VP.GENERO NGENERO, VP.ESPECIE NESPECIE";
    select = select + " FROM vm_nespecies ne, Cmuestra cm, Conservacion CON,CLOCALIDAD LOC,CTIPO_AMBIENTE TIPOA,CMLST_ZONAS_PROTEGIDAS ZON,cespecimen ce, VM_PLANILLA VP WHERE  ne.Clave = cm.Clave_mu AND CON.CLAVE_C(+) = cm.Clave_mu AND CM.SECUENCIA_ES = LOC.SECUENCIA_LOC(+) AND LOC.ZONA_PROTEGIDA = ZON.CODIGO(+) AND LOC.AMBIENTE_LOC = TIPOA.CODIGO_LOV(+) and ce.CLAVE_ES(+) = cm.CLAVE_MU and cm.PUBLICADO_MU = 'S' AND ne.CLAVE = VP.CLAVE";

    if(phylum != null && !phylum.equals("") && !phylum.equals("null")){
      select = select + " AND  ne.Phylum  = " + phylum + " ";
    }

    if(conse != null && !conse.equals("") && !conse.equals("null") && conse1 != null && !conse1.equals("") && !conse1.equals("null") ){
      select = select + " AND  ne.Consecutivo_tx >= " + conse + " AND   ne.Consecutivo_tx < " + conse1 + " ";
    }

    if(ncites != null && !ncites.equals("") && !ncites.equals("null")){
      select = select + " and con.CITES IS NOT NULL ";
    }
    if(ncatlibrorojo != null && !ncatlibrorojo.equals("") && !ncatlibrorojo.equals("null")){
      select = select + " and SUBSTR(CON.CATEGORIAS_C,0,2) = '" + ncatlibrorojo + "' ";
    }
    if(nlibrorojo != null && !nlibrorojo.equals("") && !nlibrorojo.equals("null")){
      select = select + " and CON.CATEGORIAS_C IS NOT NULL ";
//      select = select + " and SUBSTR(CON.CATEGORIA_UICN_GLOBAL,0,2) = '" + ncatuicn + "' ";
    }
    if(nprof1 != null && !nprof1.equals("") && !nprof1.equals("null")){
      select = select + " and CM.PROF_CAPT_MIN >= '" + nprof1 + "' ";
    }
    if(nprof2 != null && !nprof2.equals("") && !nprof2.equals("null")){
      select = select + " and CM.PROF_CAPT_MAX <= '" + nprof2 + "' ";
    }
    if(nvulgar != null && !nvulgar.equals("") && !nvulgar.equals("null")){
      select = select + " and ne.Clave = '" + nvulgar + "' ";
    }
    if(nambiente != null && !nambiente.equals("") && !nambiente.equals("null")){
      select = select + " and LOC.AMBIENTE_LOC = '" + nambiente + "' ";
    }
    if(nzona != null && !nzona.equals("") && !nzona.equals("null")){
      select = select + " and LOC.ZONA_PROTEGIDA = '" + nzona + "' ";
    }
    if(neco != null && !neco.equals("") && !neco.equals("null")){
      select = select + " and LOC.ECORREGION_LOC LIKE '" + neco + "%' ";
    }
    if(nreg != null && !nreg.equals("") && !nreg.equals("null")){
      select = select + " and LOC.ECORREGION_LOC LIKE '" + nreg + "%' ";
    }
    if(ngenero != null && !ngenero.equals("") && !ngenero.equals("null")){
      select = select + " and  VP.GENERO LIKE UPPER('%" + ngenero + "%') ";
    }
    if(nespecie != null && !nespecie.equals("") && !nespecie.equals("null")){
      select = select + " and  VP.ESPECIE LIKE UPPER('%" + nespecie + "%') ";
    }


select = select + "union SELECT ne.Clave CLAVE, ne.nombre, ne.Autor AutorES, null NROINVEMAR, FVULGAR(ne.Clave) NVULGAR, UPPER(FFAMILIA(ne.Clave)) FAMILIA, FPHYLUM(ne.Clave) PHYLUM, ne.PHYLUM COD_PHYLUM, CON.CITES, SUBSTR(CON.CATEGORIAS_C,0,2) S_CATLIBROROJO, SUBSTR(CON.CATEGORIA_UICN_GLOBAL,0,2) S_CATUICN, null CODAMB, null CODZON, null PRMIN, null PRMAX, null catalogo, ce.CLAVE_ES claveesp, null eco, VP.GENERO NGENERO, VP.ESPECIE NESPECIE ";
select = select + "FROM vm_nespecies ne, Conservacion CON,cespecimen ce, VM_PLANILLA VP, cmuestra cm WHERE ce.CLAVE_ES = cm.CLAVE_MU(+) and cm.CLAVE_MU is null and ne.Clave = ce.Clave_es AND CON.CLAVE_C(+) = ce.Clave_es AND ne.CLAVE = VP.CLAVE and (ce.diagnosis_es is not null or ce.descripcion_es is not null)";

    if(phylum != null && !phylum.equals("") && !phylum.equals("null")){
      select = select + " AND  ne.Phylum  = " + phylum + " ";
    }

    if(conse != null && !conse.equals("") && !conse.equals("null") && conse1 != null && !conse1.equals("") && !conse1.equals("null") ){
      select = select + " AND  ne.Consecutivo_tx >= " + conse + " AND   ne.Consecutivo_tx < " + conse1 + " ";
    }

    if(ncites != null && !ncites.equals("") && !ncites.equals("null")){
      select = select + " and con.CITES IS NOT NULL ";
    }
    if(ncatlibrorojo != null && !ncatlibrorojo.equals("") && !ncatlibrorojo.equals("null")){
      select = select + " and SUBSTR(CON.CATEGORIAS_C,0,2) = '" + ncatlibrorojo + "' ";
    }
    if(nlibrorojo != null && !nlibrorojo.equals("") && !nlibrorojo.equals("null")){
      select = select + " and CON.CATEGORIAS_C IS NOT NULL ";
//      select = select + " and SUBSTR(CON.CATEGORIA_UICN_GLOBAL,0,2) = '" + ncatuicn + "' ";
    }
    if(nvulgar != null && !nvulgar.equals("") && !nvulgar.equals("null")){
      select = select + " and ne.Clave = '" + nvulgar + "' ";
    }
    if(ngenero != null && !ngenero.equals("") && !ngenero.equals("null")){
      select = select + " and  VP.GENERO LIKE UPPER('%" + ngenero + "%') ";
    }
    if(nespecie != null && !nespecie.equals("") && !nespecie.equals("null")){
      select = select + " and  VP.ESPECIE LIKE UPPER('%" + nespecie + "%') ";
    }

    select = select + " order by 2,4";

    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              ficha = new Cficha();
              ficha.setaclave(rs.getString(1));
              ficha.setadescripcion(rs.getString(2));
              ficha.setaautor(rs.getString(3));
              ficha.setanumero(rs.getString(4));
              ficha.setanvulgar(rs.getString(5));
              ficha.setafamilia(rs.getString(6));
              ficha.setaphylum(rs.getString(7));
              ficha.setacodphylum(rs.getString(8));
              ficha.setacites(rs.getString(9));
              ficha.setascatlibrorojo(rs.getString(10));
              ficha.setascatuicn(rs.getString(11));
              ficha.setacodamb(rs.getString(12));
              ficha.setacodzon(rs.getString(13));
              ficha.setaprmin(rs.getString(14));
              ficha.setaprmax(rs.getString(15));
              ficha.setacatalogo(rs.getString(16));
              ficha.setaclaveesp(rs.getString(17));
              ficha.setacodeco(rs.getString(18));
              resultados.addElement(ficha);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }


  /**
  * retorna una instancia del contenedor de ficha dada su posicion en el vector
  */
  public Cficha getficha(int i) {
    return ((Cficha)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }

}