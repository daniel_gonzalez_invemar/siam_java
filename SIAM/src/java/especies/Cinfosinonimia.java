package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Cinforeferencias (la lista de referencias bibliograficas)
 * autor: Leonardo J. Arias Aleman.<br>
 * Version: 1.0 <br>
 * Fecha: 06-05-2004
*/

public class Cinfosinonimia  {
  private String s_nombre;
  private String s_autor;
  private String s_clave;
  private String s_phylum;
  private String s_notas;
  private String s_bibliografia;

  /**
  * Metodo constructor
  */
  public void Cinforeferencias(){}

  /**
  Carga el atributo nombre al objeto
  */
  public void setanombre(String nombre)
  {
    s_nombre = nombre;
  }
  /**
  Carga el atributo a_autor al objeto
  */
  public void setaautor(String autor)
  {
    s_autor = autor;
  }
    /**
    Carga el atributo a_clave al objeto
    */
    public void setaclave(String clave)
    {
      s_clave = clave;
    }
    /**
    Carga el atributo a_notas al objeto
    */
    public void setanotas(String notas)
    {
      s_notas = notas;
    }
    /**
    Carga el atributo a_phylum al objeto
    */
    public void setaphylum(String phylum)
    {
      s_phylum = phylum;
    }
    /**
    Carga el atributo a_bibliografia al objeto
    */
    public void setabibliografia(String bibliografia)
    {
      s_bibliografia = bibliografia;
    }



  //metodos get
  /**
  Retorna el atributo a_nombre del objeto
  */
  public String get_anombre(){
    return s_nombre;
  }
  /**
  Retorna el atributo a_autor del objeto
  */
  public String get_aautor(){
    return s_autor;
  }
    /**
    Retorna el atributo a_clave del objeto
    */
    public String get_aclave(){
      return s_clave;
    }
    /**
    Retorna el atributo a_notas del objeto
    */
    public String get_anotas(){
      return s_notas;
    }
    /**
    Retorna el atributo a_phylum del objeto
    */
    public String get_aphylum(){
      return s_phylum;
    }
    /**
    Retorna el atributo a_bibliografía del objeto
    */
    public String get_abibliografia(){
      return s_bibliografia;
    }


}