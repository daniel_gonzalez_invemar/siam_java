

package especies;


import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja phylum
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 21-01-2002
*/
public class Cphylum{

public String a_codigo;
public String a_descripcion;
public String a_reino;
/**
  Metodo constructor
*/
  public void Cphylum(){}
/**
  Retorna el atributo a_codigo del objeto
*/
  public String get_acodigo(){
    return a_codigo;
  }
/**
  Retorna el atributo a_descripcion del objeto
*/
  public String get_adescripcion(){
    return a_descripcion;
  }
/**
  Retorna el atributo a_reino del objeto
*/
  public String get_areino(){
    return a_reino;
  }
}
