

package especies;


import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja nombre vulgares
 * autor: Leonardo J. Arias A.<br>
 * Version: 1.0 <br>
 * Fecha: 20-11-2002
*/
public class Cvulgares{

public String a_clave;
public String a_nombre;

/**
  Metodo constructor
*/
  public void Cvulgares(){}

/**
  Retorna el atributo a_clave del objeto
*/
  public String get_aclave(){
    return a_clave;
  }
/**
  Retorna el atributo a_nombre del objeto
*/
  public String get_anombre(){
    return a_nombre;
  }
}
