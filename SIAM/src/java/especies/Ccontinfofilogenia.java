

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Cinfofilogenia (la filogenia de las especies)
 * autor: Leonardo J. Arias Aleman.<br>
 * Version: 1.0 <br>
 * Fecha: 06-05-2004
*/

public class Ccontinfofilogenia  {

  private String Error;
  private int tamano;
  private Vector resultados;
  /**
  * Metodo constructor
  */
  public void Ccontinfofilogenia(){}

  /**
  * carga todas las imagenes de la base de datos en un vector de objetos
  * infofilogenia dependiendo de la clave
  */
  public boolean contenedor(Connection conn1, String clave)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Cinfofilogenia infofilogenia = new Cinfofilogenia();
//    String select = "select distinct taxon_api.ffilogenia(clave) from vm_planilla where clave = '" + clave + "' ";
    String select = "select curador.taxon_api.ffilogenia('" + clave + "') from sys.dual";
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              infofilogenia = new Cinfofilogenia();
              infofilogenia.setafilogenia(rs.getString(1));
              resultados.addElement(infofilogenia);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }
  /**
  * retorna una instancia del contenedor de infofilogenia dada su posicion en el vector
  */
  public Cinfofilogenia getinfofilogenia(int i) {
    return ((Cinfofilogenia)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }
}