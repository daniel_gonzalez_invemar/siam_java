

package especies;
import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja el vector taxones
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 21-01-2002
*/


public class Cconttaxones  {
  private String Error;
  private int tamano;
  private Vector resultados;
  /**
  * Metodo constructor
  */
  public void Cconttaxones(){}
  /**
  * carga todas las taxones de la base de datos en un vector de objetos
  * niveltax
  */
  public boolean contenedor(Connection conn1)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Ctaxones taxones = new Ctaxones();
    String select = "select * from curador.ctaxoness order by descripcion_tx";
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              taxones = new Ctaxones();
              taxones.a_clave = rs.getString("consecutivo_tx");
              taxones.a_nivelup = rs.getString("nivelup_tx");
              taxones.a_phylum = rs.getString("phylum_tx");
              taxones.a_descripcion = rs.getString("descripcion_tx");
              taxones.a_niveltx = rs.getString("nivel_tx");
              taxones.a_idup = rs.getString("idup_tx");
              taxones.a_id = rs.getString("id_tx");
              //taxones.a_consecutivo = rs.getString("");
              resultados.addElement(taxones);
              tamano++; 
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }
  /**
  * carga todas las taxones de la base de datos en un vector de objetos
  * niveltax dependiendo de phylum y nivel
  */
  public boolean contenedor1(Connection conn1, String phylum, String nivel)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Ctaxones taxones = new Ctaxones();
    String select = "select ct.CLAVE_TX, ct.NIVELUP_TX, ct.PHYLUM_TX, nt.NOMBRE, ct.NIVEL_TX, ct.IDUP_TX, ct.ID_TX, ct.CONSECUTIVO_TX from curador.ctaxoness ct, curador.LISTA_NOMBRES_CTAXONESS nt where ct.CLAVE_TX = nt.CLAVE and  ct.phylum_tx = " + phylum +  " and ct.nivel_tx = " + nivel +  " order by nt.nombre";
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              taxones = new Ctaxones();
              taxones.a_clave = rs.getString("clave_tx");
              taxones.a_nivelup = rs.getString("nivelup_tx");
              taxones.a_phylum = rs.getString("phylum_tx");
              taxones.a_descripcion = rs.getString("nombre");
              taxones.a_niveltx = rs.getString("nivel_tx");
              taxones.a_idup = rs.getString("idup_tx");
              taxones.a_id = rs.getString("id_tx");
              taxones.a_consecutivo = rs.getString("consecutivo_tx");
              resultados.addElement(taxones);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }
  /**
  * carga todas las taxones de la base de datos en un vector de objetos
  * niveltax dependiendo de phylum , nivel y nivel1
  */
  public boolean contenedor2(Connection conn1, String phylum, String nivel, String nivel1)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Ctaxones taxones = new Ctaxones();
    String select = "select * from ctaxoness where phylum_tx = " + phylum +  " and nivel_tx = " + nivel1 +  " and nivelup_tx = " + nivel + " order by descripcion_tx";
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              taxones = new Ctaxones();
              taxones.a_clave = rs.getString("clave_tx");
              taxones.a_nivelup = rs.getString("nivelup_tx");
              taxones.a_phylum = rs.getString("phylum_tx");
              taxones.a_descripcion = rs.getString("descripcion_tx");
              taxones.a_niveltx = rs.getString("nivel_tx");
              taxones.a_idup = rs.getString("idup_tx");
              taxones.a_id = rs.getString("id_tx");
              resultados.addElement(taxones);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage()+ "," + select;
      resp = false;
     }
     return resp;
  }
  
  /**
  * retorna una instancia del contenedor de taxones dada su posicion en el vector
  */
  public Ctaxones gettaxones(int i) {
    return ((Ctaxones)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }
}
