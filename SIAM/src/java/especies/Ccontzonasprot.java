

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja el vector Zonas protegidas
 * autor: Leonardo J. Arias A.<br>
 * Version: 1.0 <br>
 * Fecha: 20-11-2002
*/


public class Ccontzonasprot {
  private String Error;
  private int tamano;
  private Vector resultados;

  /**
  * Metodo constructor
  */
  public void Ccontzonasprot(){}
   /**
  * carga todas las zonas protegidas de la base de datos en un vector de objetos
  * czonasprot
  */
  public boolean contenedor(Connection conn1)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Czonasprot zonasprot = new Czonasprot();
    String select = "select ZON.CODIGO, ZON.DESCRIPCION from curador.CMLST_ZONAS_PROTEGIDAS ZON";
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              zonasprot = new Czonasprot();
              zonasprot.a_codigo = rs.getString("codigo");
              zonasprot.a_descripcion = rs.getString("descripcion");
              resultados.addElement(zonasprot);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }

  /**
  * retorna una instancia del contenedor de phylum dada su posicion en el vector
  */
  public Czonasprot getzonasprot(int i) {
    return ((Czonasprot)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }

}
