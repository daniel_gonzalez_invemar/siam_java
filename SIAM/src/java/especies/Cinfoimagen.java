package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Cficha(la lista de la ficha de la muestra y el museo)
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 28-05-2002
*/

public class Cinfoimagen  {
  private String a_archivo;
  private String a_nota;
  private String a_consecutivo;

  /**
  * Metodo constructor
  */
  public void Cficha(){}

  /**
  Carga el atributo a_archivo al objeto
  */
  public void setaarchivo(String archivo)
  {
    a_archivo = archivo;
  }
  /**
  Carga el atributo a_nota al objeto
  */
  public void setanota(String nota)
  {
    a_nota = nota;
  }

  /**
  Carga el atributo a_nota al objeto
  */
  public void setaconsecutivo(String consecutivo)
  {
    a_consecutivo = consecutivo;
  }



  //metodos get
  /**
  Retorna el atributo a_archivo del objeto
  */
  public String get_aarchivo(){
    return a_archivo;
  }
  /**
  Retorna el atributo a_nota del objeto
  */
  public String get_anota(){
    return a_nota;
  }
  /**
  Retorna el atributo a_consecutivo del objeto
  */
  public String get_aconsecutivo(){
    return a_consecutivo;
  }


}