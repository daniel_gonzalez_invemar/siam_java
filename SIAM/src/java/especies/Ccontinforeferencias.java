

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Cinforeferencias (la lista de referencias bibliograficas)
 * autor: Leonardo J. Arias Aleman.<br>
 * Version: 1.0 <br>
 * Fecha: 06-05-2004
*/

public class Ccontinforeferencias  {

  private String Error;
  private int tamano;
  private Vector resultados;
  /**
  * Metodo constructor
  */
  public void Ccontinforeferencias(){}

  /**
  * carga todas las imagenes de la base de datos en un vector de objetos
  * inforeferencias dependiendo de la clave
  */
  public boolean contenedor(Connection conn1, String clave, String fichamuseo, String referencia, String phylum)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Cinforeferencias inforeferencias = new Cinforeferencias();
    String select = "select distinct b.secuenciabbg_bib||'-'||b.phylum_bib codigo, '<b>'||autor_bib||', '||ano_bib||'</b> '||titulo_bib||'. '||paginas_bib||' '||titulo_paralelo||'. '||texto_bib referencia from cautores a, cbibliografia b where a.secuenciabbg = b.secuenciabbg_bib and a.phylum_au = b.phylum_bib ";
    
//    and a.clave_au = '" + clave + "' ";

    if(clave != null && !clave.equals("") && !clave.equals("null")){
      select = select + " and a.clave_au = '" + clave + "' ";
    }
      if(fichamuseo.equals("true")){
        select = select + " and a.ITEM_AU >=50 and a.ITEM_AU <80 ";
      }
      if(referencia != null && !referencia.equals("") && !referencia.equals("null")){
        select = select + " and b.secuenciabbg_bib = "+ referencia;
      }
      if(phylum != null && !phylum.equals("") && !phylum.equals("null")){
        select = select + " and b.phylum_bib = "+ phylum;
      }
    
    select = select + " order by referencia";
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              inforeferencias = new Cinforeferencias();
              inforeferencias.setacodigo(rs.getString(1));
              inforeferencias.setareferencia(rs.getString(2));
              resultados.addElement(inforeferencias);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }
  /**
  * retorna una instancia del contenedor de inforeferencias dada su posicion en el vector
  */
  public Cinforeferencias getinforeferencias(int i) {
    return ((Cinforeferencias)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }
}