

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja el vector nombres vulgares
 * autor: Leonardo J. Arias A.<br>
 * Version: 1.0 <br>
 * Fecha: 20-11-2002
*/


public class Ccontvulgares {
  private String Error;
  private int tamano;
  private Vector resultados;

  /**
  * Metodo constructor
  */
  public void Ccontvulgares(){}

   /**
  * carga todas los nombres vulgares de las fichas de especie un vector de objetos
  * cvulgares
  */
  public boolean contenedorfespecies(Connection conn1)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Cvulgares vulgares = new Cvulgares();
    String select = "select distinct cv.clave_vu, cv.nombre from curador.cvulgares cv, curador.cespecimen ce where ce.CLAVE_ES = cv.CLAVE_VU order by nombre";
    try
      {
        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              vulgares = new Cvulgares();
              vulgares.a_clave = rs.getString("clave_vu");
              vulgares.a_nombre = rs.getString("nombre");
              resultados.addElement(vulgares);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }


   /**
  * carga todas los nombres vulgares de la base de datos en un vector de objetos
  * cvulgares
  */
  public boolean contenedor(Connection conn1)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Cvulgares vulgares = new Cvulgares();
    String select = "select distinct clave_vu, nombre from cvulgares order by nombre";
    try
      {
        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              vulgares = new Cvulgares();
              vulgares.a_clave = rs.getString("clave_vu");
              vulgares.a_nombre = rs.getString("nombre");
              resultados.addElement(vulgares);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }

  /**
  * retorna una instancia del contenedor de vulgares dada su posicion en el vector
  */
  public Cvulgares getvulgares(int i) {
    return ((Cvulgares)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la base de datos
  */
  public String geterror(){
    return Error;
  }

}

