

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja el vector Categorias CITES
 * autor: Leonardo J. Arias A.<br>
 * Version: 1.0 <br>
 * Fecha: 20-11-2002
*/


public class Ccontcategoriacites {
  private String Error;
  private int tamano;
  private Vector resultados;

  /**
  * Metodo constructor
  */
  public void Ccontcategoriacites(){}
   /**
  * carga todas las categorias de la base de datos en un vector de objetos
  * ccategoriacites
  */
  public boolean contenedor(Connection conn1)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Ccategoriacites categoriacites = new Ccategoriacites();
    String select = "SELECT distinct grupodescripcion_ph , seccion FROM curador.vm_fichac, curador.cphylum WHERE seccion = grupo_ph ORDER BY grupodescripcion_ph"
;
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              categoriacites = new Ccategoriacites();
              categoriacites.a_codigo = rs.getString("seccion");
              categoriacites.a_descripcion = rs.getString("grupodescripcion_ph");
              resultados.addElement(categoriacites);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }

  /**
  * retorna una instancia del contenedor de phylum dada su posicion en el vector
  */
  public Ccategoriacites getcategoriacites(int i) {
    return ((Ccategoriacites)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }

}