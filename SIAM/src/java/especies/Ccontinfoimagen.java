

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja el vector infoimagen
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 28-05-2002
*/

public class Ccontinfoimagen  {

  private String Error;
  private int tamano;
  private Vector resultados;
  /**
  * Metodo constructor
  */
  public void Ccontinfoimagen(){}

  /**
  * carga todas las imagenes de la base de datos en un vector de objetos
  * infoimagen dependiendo de la clave
  */
  public boolean contenedor1(Connection conn1, String clave, String numero)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Cinfoimagen infoimagen = new Cinfoimagen();
    String select = "SELECT   Archivo_Im, notas_im, nroimagen_im  FROM curador.CIMAGENES WHERE  CLAVE_IM = '" + clave + "' ";

    if(numero != null && !numero.equals("") && !numero.equals("null"))
      select = select + " and nroinvemar_im = " + numero + " ";
    else 
      select = select + " and nroinvemar_im is null ";

      select = select + " order by consecutivo_im ";
      
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              infoimagen = new Cinfoimagen();
              infoimagen.setaarchivo(rs.getString(1));
              infoimagen.setanota(rs.getString(2));
              infoimagen.setaconsecutivo(rs.getString(3));
              resultados.addElement(infoimagen);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }
  /**
  * retorna una instancia del contenedor de infoimagen dada su posicion en el vector
  */
  public Cinfoimagen getinfoimagen(int i) {
    return ((Cinfoimagen)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }
}