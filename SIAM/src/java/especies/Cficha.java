

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Cficha(la lista de la ficha de la muestra y el museo)
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 23-05-2002
*/

public class Cficha  {

  private String a_clave;
  private String a_claveesp;
  private String a_descripcion;
  private String a_autor;
  private String a_numero;
  private String a_nvulgar;
  private String a_familia;
  private String a_phylum;
  private String a_prmin;
  private String a_prmax;
  private String a_cites;
  private String a_catlibrorojo;
  private String a_catuicn;
  private String a_scatlibrorojo;
  private String a_scatuicn;
  private String a_codamb;
  private String a_codzon;
  private String a_catalogo;
  private String a_codphylum;
  private String a_codeco;
  private String a_codreg;


  /**
  * Metodo constructor
  */
  public void Cficha(){}

  /**
  Carga el atributo a_clave al objeto
  */
  public void setaclave(String clave)
  {
    a_clave = clave;
  }
  /**
  Carga el atributo a_claveesp al objeto
  */
  public void setaclaveesp(String claveesp)
  {
    a_claveesp = claveesp;
  }

  /**
  Carga el atributo a_descripcion al objeto
  */
  public void setadescripcion(String descripcion)
  {
    a_descripcion = descripcion;
  }
  /**
  Carga el atributo a_autor al objeto
  */
  public void setaautor(String autor)
  {
    a_autor = autor;
  }
  /**
  Carga el atributo a_numero al objeto
  */
  public void setanumero(String numero)
  {
    a_numero = numero;
  }

  /**
  Carga el atributo a_nvulgar al objeto
  */
  public void setanvulgar(String nvulgar)
  {
    a_nvulgar = nvulgar;
  }
  /**
  Carga el atributo a_familia al objeto
  */
  public void setafamilia(String familia)
  {
    a_familia = familia;
  }
  /**
  Carga el atributo a_phylum al objeto
  */
  public void setaphylum(String phylum)
  {
    a_phylum = phylum;
  }

  public void setacodphylum(String codphylum)
  {
    a_codphylum = codphylum;
  }

  public void setaprmin(String prmin)
  {
    a_prmin = prmin;
  }
  public void setaprmax(String prmax)
  {
    a_prmax = prmax;
  }
  public void setacites(String cites)
  {
    a_cites = cites;
  }
  public void setacatlibrorojo(String catlibrorojo)
  {
    a_catlibrorojo = catlibrorojo;
  }
  public void setacatuicn(String catuicn)
  {
    a_catuicn = catuicn;
  }
  public void setascatlibrorojo(String scatlibrorojo)
  {
    a_scatlibrorojo = scatlibrorojo;
  }
  public void setascatuicn(String scatuicn)
  {
    a_scatuicn = scatuicn;
  }
  public void setacodamb(String codamb)
  {
    a_codamb = codamb;
  }
  public void setacodzon(String codzon)
  {
    a_codzon = codzon;
  }
  public void setacatalogo(String catalogo)
  {
    a_catalogo = catalogo;
  }
  public void setacodeco(String codeco)
  {
    a_codeco = codeco;
  }
  public void setacodreg(String codreg)
  {
    a_codreg = codreg;
  }


  //metodos get
  /**
  Retorna el atributo a_clave del objeto
  */
  public String get_aclave(){
    return a_clave;
  }
  /**
  Retorna el atributo a_claveesp del objeto
  */
  public String get_aclaveesp(){
    return a_claveesp;
  }

  /**
  Retorna el atributo a_descripcion del objeto
  */
  public String get_adescripcion(){
    return a_descripcion;
  }
  /**
  Retorna el atributo a_autor del objeto
  */
  public String get_aautor(){
    return a_autor;
  }
  /**
  Retorna el atributo a_numero del objeto
  */
  public String get_anumero(){
    return a_numero;
  }

  /**
  Retorna el atributo a_nvulgar del objeto
  */
  public String get_anvulgar(){
    return a_nvulgar;
  }
  /**
  Retorna el atributo a_familia del objeto
  */
  public String get_afamilia(){
    return a_familia;
  }
  /**
  Retorna el atributo a_phylum del objeto
  */
  public String get_aphylum(){
    return a_phylum;
  }
  public String get_acodphylum(){
    return a_codphylum;
  }
  public String get_aprmin(){
    return a_prmin;
  }
  public String get_aprmax(){
    return a_prmax;
  }
  public String get_acites(){
    return a_phylum;
  }
  public String get_acatlibrorojo(){
    return a_catlibrorojo;
  }
  public String get_acatuicn(){
    return a_catuicn;
  }
  public String get_ascatlibrorojo(){
    return a_scatlibrorojo;
  }
  public String get_ascatuicn(){
    return a_scatuicn;
  }
  public String get_acodamb(){
    return a_codamb;
  }
  public String get_acodzon(){
    return a_codzon;
  }
  public String get_acatalogo(){
    return a_catalogo;
  }
  public String get_acodeco(){
    return a_codeco;
  }
  public String get_acodreg(){
    return a_codreg;
  }

}