package especies;
import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Cnivelestax
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 21-01-2002
*/

public class Cniveltax {

public String a_nivel;
public String a_descripcion;
/**
  Metodo constructor
*/
  public void Cniveltax(){}

/**
  Metodo que retorna el codigo del nivel
*/
  public String get_anivel(){
    return a_nivel;
  }

/**
  Metodo que retorna el nombre del nivel
*/
  public String get_adescripcion(){
    return a_descripcion;
  }


}
