

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja el vector phylum
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 21-01-2002
*/


public class Ccontphylum {
  private String Error;
  private int tamano;
  private Vector resultados;
  /**
  * Metodo constructor
  */
  public void Ccontphylum(){}

   /**
  * carga todas las phylum de la base de datos en un vector de objetos
  * cphylum
  */
  public boolean contenedor(Connection conn1)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Cphylum phylum = new Cphylum();
    String select = "SELECT DISTINCT  Descripcion_Ph, codigo_ph  FROM  curador.Ctaxoness, curador.Cphylum WHERE Nivelup_tx = 0    AND Codigo_ph  = Phylum_tx ORDER BY  descripcion_ph";
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              phylum = new Cphylum();
              phylum.a_codigo = rs.getString("codigo_ph");
              phylum.a_descripcion = rs.getString("Descripcion_Ph");
              resultados.addElement(phylum);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }


   /**
  * carga todas las phylum de las fichas de especies en un vector de objetos
  * cphylum
  */
  public boolean contenedorfespecies(Connection conn1)
  {
    boolean resp = false;
    resultados = new Vector();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Cphylum phylum = new Cphylum();
    String select = "SELECT DISTINCT  grupodescripcion_Ph descripcion_Ph, codigo_ph FROM  Cespecimen ce, Cphylum cp WHERE ce.PHYLUM_ES  = cp.CODIGO_PH ORDER BY descripcion_ph";
    try
      {

        stmt = conn1.prepareStatement(select);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          do{
              phylum = new Cphylum();
              phylum.a_codigo = rs.getString("codigo_ph");
              phylum.a_descripcion = rs.getString("descripcion_Ph");
              resultados.addElement(phylum);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
  }



  /**
  * retorna una instancia del contenedor de phylum dada su posicion en el vector
  */
  public Cphylum getphylum(int i) {
    return ((Cphylum)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }

}