

package especies;
import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Ctaxones
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 21-01-2002
*/


public class Ctaxones  {

public String a_clave;
public String a_phylum;
public String a_niveltx;
public String a_nivelup;
public String a_idup;
public String a_id;
public String a_descripcion;
public String a_consecutivo ;
public String a_consecutivo1 ;
public String Error;
/**
  Metodo constructor
*/
  public void Ctaxones(){}
/**
  * Carga una instancia del objeto taxones de la base de datos <br>
  * dependiendo del atributo a_clave que este cargado en el objeto
  */
  public boolean cargataxon(Connection conn1)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    try
      {
        stmt = conn1.prepareStatement("SELECT * FROM curador.ctaxoness where clave_tx = ?" );
        stmt.setString(1, a_clave);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          a_clave =  rs.getString("clave_tx");
          a_descripcion = rs.getString("descripcion_tx");
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
    }
  /**
  * Carga una instancia del objeto taxones de la base de datos <br>
  * dependiendo del atributo a_clave que este cargado en el objeto
  */
  public boolean cargataxon1(Connection conn1)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    try
      {
        stmt = conn1.prepareStatement("SELECT  Min(Consecutivo_tx)  FROM  Ctaxoness WHERE  Phylum_tx = ? AND  Nivel_tx <= ? AND  Consecutivo_tx > ?" );
        stmt.setString(1, a_phylum);
        stmt.setString(2, a_niveltx);
        stmt.setString(3, a_consecutivo);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          a_consecutivo1 = rs.getString(1);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
    }
}
