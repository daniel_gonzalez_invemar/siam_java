

package especies;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja Cinfoficha(informacion de la muestra )
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 27-05-2002
*/
public class Cinfoficha {

  private String a_clave;
  private String a_coloracion;
  private String a_descripcion;
  private String a_dimensiones;
  private String a_diagnosis;
  private String a_notas;
  private String a_habitat;
  private String a_sustrato;
  private String a_profundidadmin;
  private String a_profundidadmax;
  private String a_distglobal;
  private String a_distlocal;
  private String a_generales;
  private String a_taxonomicos;
  private String a_amenazas;
  private String a_conserv_actual;
  private String a_conserv_propuesta;
  private String a_cites;
  private String a_catlibrorojo;
  private String a_catuicn;

  private String Error;
  /**
  * Metodo constructor
  */
  public void Cinfoficha(){}
  /**
  * carga el atributo a_clave
  */
  public void setaclave(String clave){
    a_clave = clave;
  }
  /**
  * carga el atributo a_coloracion
  */
  public void setacoloracion(String coloracion){
    a_coloracion = coloracion;
  }
  /**
  * carga el atributo a_descripcion
  */
  public void setadescripcion(String descripcion){
    a_descripcion = descripcion;
  }
  /**
  * carga el atributo a_dimensiones
  */
  public void setadimensiones(String dimensiones){
    a_dimensiones = dimensiones;
  }
  /**
  * carga el atributo a_diagnosis
  */
  public void setadiagnosis(String diagnosis){
    a_diagnosis = diagnosis;
  }
  /**
  * carga el atributo a_notas
  */
  public void setanotas(String notas){
    a_notas = notas;
  }
  /**
  * carga el atributo a_habitat
  */
  public void setahabitat(String habitat){
    a_habitat = habitat;
  }
  /**
  * carga el atributo a_sustrato
  */
  public void setasustrato(String sustrato){
    a_sustrato = sustrato;
  }
  /**
  * carga el atributo a_profundidadmin
  */
  public void setaprofundidadmin(String profundidadmin){
    a_profundidadmin = profundidadmin;
  }
  /**
  * carga el atributo a_profundidadmax
  */
  public void setaprofundidadmax(String profundidadmax){
    a_profundidadmax = profundidadmax;
  }
  /**
  * carga el atributo a_distglobal
  */
  public void setadistglobal(String distglobal){
    a_distglobal = distglobal;
  }
  /**
  * carga el atributo a_distlocal
  */
  public void setadistlocal(String distlocal){
    a_distlocal = distlocal;
  }
  /**
  * carga el atributo a_generales
  */
  public void setagenerales(String generales){
    a_generales = generales;
  }
  /**
  * carga el atributo a_taxonomicos
  */
  public void setataxonomicos(String taxonomicos){
    a_taxonomicos = taxonomicos;
  }

  public void setaamenazas(String amenazas){
    a_amenazas = amenazas;
  }
  public void setaconserv_actual(String conserv_actual){
    a_conserv_actual = conserv_actual;
  }
  public void setaconserv_propuesta(String conserv_propuesta){
    a_conserv_propuesta = conserv_propuesta;
  }
  public void setacites(String cites){
    a_cites = cites;
  }
  public void setacatlibrorojo(String catlibrorojo){
    a_catlibrorojo = catlibrorojo;
  }
  public void setacatuicn(String catuicn){
    a_catuicn = catuicn;
  }



  //metodos get
  /**
  Retorna el atributo a_clave del objeto
  */
  public String get_aclave(){
    return a_clave;
  }
  /**
  Retorna el atributo a_coloracion del objeto
  */
  public String get_acoloracion(){
    return a_coloracion;
  }
  /**
  Retorna el atributo a_descripcion del objeto
  */
  public String get_adescripcion(){
    return a_descripcion;
  }
  /**
  Retorna el atributo a_dimensiones del objeto
  */
  public String get_adimensiones(){
    return a_dimensiones;
  }
  /**
  Retorna el atributo a_diagnosis del objeto
  */
  public String get_adiagnosis(){
    return a_diagnosis;
  }
  /**
  Retorna el atributo a_notas del objeto
  */
  public String get_anotas(){
    return a_notas;
  }
  /**
  Retorna el atributo a_habitat del objeto
  */
  public String get_ahabitat(){
    return a_habitat;
  }
  /**
  Retorna el atributo a_sustrato del objeto
  */
  public String get_asustrato(){
    return a_sustrato;
  }
  /**
  Retorna el atributo a_profundidadmin del objeto
  */
  public String get_aprofundidadmin(){
    return a_profundidadmin;
  }
  /**
  Retorna el atributo a_profundidadmax del objeto
  */
  public String get_aprofundidadmax(){
    return a_profundidadmax;
  }
  /**
  Retorna el atributo a_distglobal del objeto
  */
  public String get_adistglobal(){
    return a_distglobal;
  }
  /**
  Retorna el atributo a_distlocal del objeto
  */
  public String get_adistlocal(){
    return a_distlocal;
  }
  /**
  Retorna el atributo a_generales del objeto
  */
  public String get_agenerales(){
    return a_generales;
  }
  /**
  Retorna el atributo a_taxonomicos del objeto
  */
  public String get_ataxonomicos(){
    return a_taxonomicos;
  }


  public String get_aamenazas(){
    return a_amenazas;
  }
  public String get_aconserv_actual(){
    return a_conserv_actual;
  }
  public String get_aconserv_propuesta(){
    return a_conserv_propuesta ;
  }
  public String get_acites(){
    return a_cites;
  }
  public String get_acatlibrorojo(){
    return a_catlibrorojo;
  }
  public String get_acatuicn(){
    return a_catuicn ;
  }

  public String geterror(){
    return Error;
  }


  /**
  * Carga una instancia del objeto infoficha de la base de datos <br>
  * dependiendo del atributo a_clave que este cargado en el objeto
  */
  public boolean cargainfoficha(Connection conn1)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        stmt = conn1.prepareStatement("SELECT  ce.Coloracion_es , ce.Descripcion_Es , ce.Dimensiones_Es , ce.Diagnosis_Es , ch.Ecologicas_H, ch.Habitat_H, ch.Sustrato_H, ch.Prof_Min_H, ch.Prof_Max_H, ch.Global_H, ch.Local_H, ce.generales_Es, ce.Comenta_es , co.AMENAZAS , co.CONSERVACION_ACTUAL , co.CONSERVACION_FUTURO conserv_propuesta , co.CITES , co.CATEGORIAS_C librorojo_colombia , co.CATEGORIA_UICN_GLOBAL uicn_global FROM    curador.CEspecimen ce, curador.CHistoria_Natural ch, curador.conservacion co WHERE ce.Clave_Es = ? AND ch.Clave_H(+) = ce.Clave_Es and co.clave_c(+) = ce.clave_es" );
        stmt.setString(1,a_clave);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          a_coloracion = rs.getString(1);
          a_descripcion = rs.getString(2);
          a_dimensiones = rs.getString(3);
          a_diagnosis = rs.getString(4);
          a_notas = rs.getString(5);
          a_habitat = rs.getString(6);
          a_sustrato = rs.getString(7);
          a_profundidadmin = rs.getString(8);
          a_profundidadmax = rs.getString(9);
          a_distglobal = rs.getString(10);
          a_distlocal = rs.getString(11);
          a_generales = rs.getString(12);
          a_taxonomicos = rs.getString(13);
          a_amenazas = rs.getString(14);
          a_conserv_actual = rs.getString(15);
          a_conserv_propuesta = rs.getString(16);
          a_cites = rs.getString(17);
          a_catlibrorojo = rs.getString(18);
          a_catuicn = rs.getString(19);

        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }
}