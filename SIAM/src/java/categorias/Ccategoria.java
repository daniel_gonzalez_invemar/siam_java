

package categorias;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja las categorias de los portales
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 22-03-2002
*/
public class Ccategoria  {
   private int a_id;
   private int a_portales;
   private String a_nombre;
   private String a_descripcion;
   private String a_foto;
   private String Error;

   /**
  Metodo constructor de la clase
  */
  public void Ccategoria(){
  }
  /**
  Carga el atributo a_id al objeto
  */
  public void setaid(int id)
  {
    a_id = id;
  }
  /**
  Carga el atributo a_portales al objeto
  */
  public void setaportales(int portales)
  {
    a_portales = portales;
  }
  /**
  Carga el atributo a_nombre al objeto
  */
  public void setanombre(String nombre)
  {
    a_nombre = nombre;
  }
  /**
  Carga el atributo a_foto al objeto
  */
  public void setafoto(String foto)
  {
    a_foto = foto;
  }
  /**
  Carga el atributo a_descripcion al objeto
  */
  public void setadescripcion(String descripcion)
  {
    a_descripcion = descripcion;
  }
  //metodos get
  /**
  Retorna el atributo a_id del objeto
  */
  public int get_aid(){
    return a_id;
  }
  /**
  Retorna el atributo a_portales del objeto
  */
  public int get_aportales(){
    return a_portales;
  }
  /**
  Retorna el atributo a_nombre del objeto
  */
  public String get_anombre(){
    return a_nombre;
  }
  /**
  Retorna el atributo a_foto del objeto
  */
  public String get_afoto(){
    return a_foto;
  }
  /**
  Retorna el atributo a_descripcion del objeto
  */
  public String get_adescripcion(){
    return a_descripcion;
  }
  /**
  Retorna el atributo Error del objeto
  */
  public String get_error(){
    return Error;
  }
  /**
  * inserta una instancia del objeto categoria en la base de datos
  */
  public boolean insertar(Connection conn1){
    boolean resp= false;
    PreparedStatement stmt = null;
    try
      {
        conn1.setAutoCommit(false);
        stmt = conn1.prepareStatement("insert into categorias(nombre, id_portales, descripcion, foto) values(?,?,?,?)" );
        stmt.setString(1,a_nombre);
        stmt.setInt(2,a_portales);
        stmt.setString(3,a_descripcion);
        stmt.setString(4, a_foto);
        stmt.execute();
        conn1.commit();
        conn1.setAutoCommit(true);
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() ;
      resp = false;
     }
    return resp;
  }
  /**
  * Carga una instancia del objeto categoria de la base de datos <br>
  * dependiendo del atributo id_cat que este cargado en el objeto
  */
  public boolean cargacategoria(Connection conn1)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    try
      {
        stmt = conn1.prepareStatement("SELECT * FROM categorias where id_cat = ?" );
        stmt.setInt(1,a_id);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          a_id = rs.getInt(1);
          a_portales = rs.getInt(2);
          a_nombre = rs.getString(3);
          a_descripcion = rs.getString(4);
          a_foto = rs.getString(5);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }
  /**
  * actualiza una instancia del objeto categoria en la base de datos
  */
  public boolean actualizar(Connection conn1){
    boolean resp= false;
    PreparedStatement stmt = null;
    try
      {
        conn1.setAutoCommit(false);
        stmt = conn1.prepareStatement("update categorias set nombre = ?, id_portales = ?, descripcion = ?, foto = ? where id_cat = ?" );
        stmt.setString(1,a_nombre);
        stmt.setInt(2,a_portales);
        stmt.setString(3,a_descripcion);
        stmt.setString(4, a_foto);
        stmt.setInt(5,a_id);
        stmt.execute();
        conn1.commit();
        conn1.setAutoCommit(true);
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() ;
      resp = false;
     }
    return resp;
  }
  /**
  * elimina una instancia del objeto categoria en la base de datos dado su id
  */
  public boolean eliminar(Connection conn1){
    boolean resp= false;
    PreparedStatement stmt = null;
    try
      {
        conn1.setAutoCommit(false);
        stmt = conn1.prepareStatement("delete from categorias where id_cat = ?" );
        stmt.setInt(1,a_id);
        stmt.execute();
        conn1.commit();
        conn1.setAutoCommit(true);
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() ;
      resp = false;
     }
    return resp;
  }
}