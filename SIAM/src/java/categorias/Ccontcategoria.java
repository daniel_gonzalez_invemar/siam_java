

package categorias;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;


/**
 * Clase que maneja el vector categorias
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 01-04-2002
*/


public class Ccontcategoria  {

  private String Error;
  private int tamano;
  private Vector resultados;
  public Ccontcategoria() {}

  /**
  * carga todas las categorias de la base de datos en un vector de objetos
  * contcategoria dependiendo de portal, nombre y descripcion
  */
  public boolean contenedor1(Connection conn1, String portal, String nombre, String desc)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Ccategoria categoria = new Ccategoria();
    String select = new String();
    select = "select * from categorias where id_cat > 0" ;
    if(nombre != null && !nombre.equals("")){
      select = select + " and nombre = '" + nombre + "'";
    }
    if(portal != null && !portal.equals("")){
      select = select + " and id_portales = " + portal ;
    }
    if(desc != null && !desc.equals("")){
      select = select + " and descripcion like '%" + desc + "%'" ;
    }
    Error = select;
    try
      {

        stmt = conn1.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              categoria = new Ccategoria();
              categoria.setaid(rs.getInt(1));
              categoria.setaportales(rs.getInt(2));
              categoria.setanombre(rs.getString(3));
              categoria.setadescripcion(rs.getString(4));
              resultados.addElement(categoria);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }
  /**
  * carga todas las categorias de la base de datos en un vector de objetos
  * contcategoria dependiendo del portal
  */
  public boolean contenedor(Connection conn1, String portal)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Ccategoria categoria = new Ccategoria();
    String select = new String();
    select = "select * from categorias where id_portales = " + portal + " order by nombre" ;
    Error = select;
    try
      {

        stmt = conn1.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              categoria = new Ccategoria();
              categoria.setaid(rs.getInt(1));
              categoria.setaportales(rs.getInt(2));
              categoria.setanombre(rs.getString(3));
              categoria.setadescripcion(rs.getString(4));
              resultados.addElement(categoria);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }
  /**
  * carga todas las categorias de la base de datos en un vector de objetos
  * contcategoria dependiendo del portal(menos las categorias principales <b>)
  * del portal del invemar
  */
  public boolean contenedor2(Connection conn1, String portal)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Ccategoria categoria = new Ccategoria();
    String select = new String();
    select = "select * from categorias where id_portales = " + portal + " and id_cat not in(12,13,14,15) order by id_cat" ;
    Error = select;
    try
      {

        stmt = conn1.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              categoria = new Ccategoria();
              categoria.setaid(rs.getInt(1));
              categoria.setaportales(rs.getInt(2));
              categoria.setanombre(rs.getString(3));
              categoria.setadescripcion(rs.getString(4));
              resultados.addElement(categoria);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }
  /**
  * retorna una instancia del contenedor de Ccategoria dada su posicion en el vector
  */
  public Ccategoria getcategoria(int i) {
    return ((Ccategoria)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos 
  */
  public String geterror(){
    return Error;
  }
}