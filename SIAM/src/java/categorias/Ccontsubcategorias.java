

package categorias;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;


/**
 * Clase que maneja el vector subcategorias
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 16-08-2002
*/

public class Ccontsubcategorias {

  private String Error;
  private int tamano;
  private Vector resultados;
  public Ccontsubcategorias() {}

  /**
  * carga todas las categorias de la base de datos en un vector de objetos
  * contcategoria dependiendo de portal, nombre y descripcion
  */
  public boolean contenedor1(Connection conn1, String portal, String categ)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Csubcategorias subcategorias = new Csubcategorias();
    String select = new String();
    select = "select subcategorias.* from categorias, subcategorias where categorias.ID_CAT = subcategorias.ID_CAT" ;
    if(categ != null && !categ.equals("")){
      select = select + " and subcategorias.id_cat = '" + categ + "'";
    }
    if(portal != null && !portal.equals("")){
      select = select + " and categorias.ID_PORTALES = " + portal ;
    }
    select = select + " order by subcategorias.fecha asc";
    Error = select;
    try
      {

        stmt = conn1.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              subcategorias = new Csubcategorias();
              subcategorias.setaid(rs.getInt(1));
              subcategorias.setacategoria(rs.getInt(2));
              subcategorias.setanombre(rs.getString(3));
              subcategorias.setadescripcion(rs.getString(4));
              subcategorias.setaenlace(rs.getString(6));
              resultados.addElement(subcategorias);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }
  /**
  * retorna una instancia del contenedor de Csubcategorias dada su posicion en el vector
  */
  public Csubcategorias getsubcategorias(int i) {
    return ((Csubcategorias)resultados.elementAt(i));
  }
  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos 
  */
  public String geterror(){
    return Error;
  }
}