

package categorias;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase que maneja las subcategorias de los portales
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 16-08-2002
*/

public class Csubcategorias {

  private int a_id;
  private int a_categoria;
  private String a_nombre;
  private String a_descripcion;
  private String a_enlace;
  private Calendar a_fecha;
  private String Error;

  /**
  Metodo constructor de la clase
  */
  public void Csubcategorias(){
  }
  /**
  Carga el atributo a_id al objeto
  */
  public void setaid(int id)
  {
    a_id = id;
  }
  /**
  Carga el atributo a_categoria al objeto
  */
  public void setacategoria(int categoria)
  {
    a_categoria = categoria;
  }
  /**
  Carga el atributo a_nombre al objeto
  */
  public void setanombre(String nombre)
  {
    a_nombre = nombre;
  }
  /**
  Carga el atributo a_descripcion al objeto
  */
  public void setadescripcion(String descripcion)
  {
    a_descripcion = descripcion;
  }
  /**
  Carga el atributo a_enlace al objeto
  */
  public void setaenlace(String enlace)
  {
    a_enlace = enlace;
  }
  /**
   carga el atributo a_fecha
  */
  public void setafecha(int ano, int mes, int dia)
  {
    a_fecha = new GregorianCalendar (ano, mes, dia);
  }
  //metodos get
  /**
  Retorna el atributo a_id del objeto
  */
  public int get_aid(){
    return a_id;
  }
  /**
  Retorna el atributo a_categoria del objeto
  */
  public int get_acategoria(){
    return a_categoria;
  }
  /**
  Retorna el atributo a_nombre del objeto
  */
  public String get_anombre(){
    return a_nombre;
  }
  /**
  Retorna el atributo a_descripcion del objeto
  */
  public String get_adescripcion(){
    return a_descripcion;
  }
  /**
  Retorna el atributo a_enlace del objeto
  */
  public String get_aenlace(){
    return a_enlace;
  }
  /**
   Retorna el atributo a_fecha
  */
  public Calendar get_afecha()
  {
    return a_fecha;
  }
  /**
  Retorna el atributo Error del objeto
  */
  public String get_error(){
    return Error;
  }
  /**
  * inserta una instancia del objeto subcategoria en la base de datos
  */
  public boolean insertar(Connection conn1){
    boolean resp= false;
    String insert ="";
    PreparedStatement stmt3 = null;
    Statement stmt = null;
    ResultSet rs = null;
    Statement stmt1 = null;
    Statement stmt2 = null;
    int mes = a_fecha.get(Calendar.MONTH);
    int ano = a_fecha.get(Calendar.YEAR);
    if( mes == 0){
      mes = 12;
      ano = ano -1;
    }

    try
      {
        conn1.setAutoCommit(false);
        //bloque la tabla de subcategorias
        stmt1 = conn1.createStatement();
        stmt1.execute("LOCK TABLE subcategorias IN EXCLUSIVE MODE NOWAIT");
        stmt = conn1.createStatement();
        insert = "insert into subcategorias (fecha) values(to_date('" + a_fecha.get(Calendar.DATE) + "/" + mes +  "/" + ano +  "','dd/mm/yyyy'))";
        stmt.execute(insert);
        stmt2 = conn1.createStatement();
        rs = stmt2.executeQuery("select max(subcat_id) from subcategorias");
        if (!rs.next())
          resp = false;
        else
        {
          do{
              a_id = rs.getInt(1);
          }while(rs.next());
        }
        stmt3 = conn1.prepareStatement("update subcategorias set id_cat = ?, nombre = ?, descripcion = ?, enlace = ? where subcat_id = ?" );
        stmt3.setInt(1, a_categoria);
        stmt3.setString(2, a_nombre);
        stmt3.setString(3, a_descripcion);
        stmt3.setString(4, a_enlace);
        stmt3.setInt(5, a_id);
        stmt3.execute();
        conn1.commit();
        conn1.setAutoCommit(true);
        if (stmt3!= null) stmt3.close();
        if (stmt2!= null) stmt2.close();
        if (stmt1!= null) stmt1.close();
        if (stmt!= null) stmt.close();
        if (rs!= null) rs.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() ;
      resp = false;
     }
    return resp;
  }
  /**
  * Carga una instancia del objeto subcategorias de la base de datos <br>
  * dependiendo del atributo id que este cargado en el objeto
  */
  public boolean cargasubcategorias(Connection conn1)
  {
    boolean resp = false;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int i = 0;
    int ano, mes, dia;
    try
      {
        stmt = conn1.prepareStatement("SELECT subcategorias.*, to_char(fecha,'yyyy'), to_char(fecha,'mm'), to_char(fecha,'dd') FROM subcategorias where subcat_id = ?" );
        stmt.setInt(1,a_id);
        rs = stmt.executeQuery ();
        if (!rs.next())
          resp = false;
        else
        {
          a_id = rs.getInt(1);
          a_categoria = rs.getInt(2);
          a_nombre = rs.getString(3);
          a_descripcion = rs.getString(4);
          a_enlace = rs.getString(6);
          ano = rs.getInt(7);
          mes = rs.getInt(8);
          dia = rs.getInt(9);
          this.setafecha(ano,mes,dia);
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage();
      resp = false;
     }
     return resp;
   }
   /**
  * actualiza una instancia del objeto subcategoria en la base de datos dado su id
  */
  public boolean actualizar(Connection conn1){
    boolean resp= false;
    PreparedStatement stmt3 = null;
    Statement stmt = null;
    String update = "";
    int mes = a_fecha.get(Calendar.MONTH);
    int ano = a_fecha.get(Calendar.YEAR);
    if( mes == 0){
      mes = 12;
      ano = ano -1;
    }
    try
      {
        conn1.setAutoCommit(false);
        stmt = conn1.createStatement();
        update = "update subcategorias set fecha = to_date('" + a_fecha.get(Calendar.DATE) + "/" + mes +  "/" + ano +  "','dd/mm/yyyy') where subcat_id = " + a_id;
        stmt.execute(update);
        stmt3 = conn1.prepareStatement("update subcategorias set id_cat = ?, nombre = ?, descripcion = ?, enlace = ? where subcat_id = ?" );
        stmt3.setInt(1, a_categoria);
        stmt3.setString(2, a_nombre);
        stmt3.setString(3, a_descripcion);
        stmt3.setString(4, a_enlace);
        stmt3.setInt(5, a_id);
        stmt3.execute();
        conn1.commit();
        conn1.setAutoCommit(true);
        if (stmt!= null) stmt.close();
        if (stmt3!= null) stmt3.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage() ;
      resp = false;
     }
    return resp;
  }
  /**
  * elimina una instancia del objeto tema en la base de datos dado su id
  */
  public boolean eliminar(Connection conn1){
    boolean resp= false;
    PreparedStatement stmt = null;
    try
      {
        conn1.setAutoCommit(false);
        stmt = conn1.prepareStatement("delete from subcategorias where subcat_id = ?" );
        stmt.setInt(1,a_id);
        stmt.execute();
        conn1.commit();
        conn1.setAutoCommit(true);
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() ;
      resp = false;
     }
    return resp;
  }
}