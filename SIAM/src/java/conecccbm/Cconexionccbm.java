package conecccbm;

import java.sql.*;
import java.lang.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Clase de conexion a la base dedatos autor: Rafael E. Lastra C.<br> Version:
 * 1.0 <br> Fecha: 21-03-2001.
 */
public class Cconexionccbm {

    private Connection aCon;
    public String Error;
    static InitialContext initialContext;

    static {
        try {
            initialContext = new InitialContext();

        } catch (NamingException ne) {

            System.out.println("Error!! No se ha podido crear el Objeto InitialContext " + ne);
        }
    }

    public synchronized DataSource getDataSource(String datasource) {
        DataSource data;
        Context envContext;
        try {
            envContext = (Context) initialContext.lookup("java:comp/env");
            data = (DataSource) envContext.lookup("jdbc/" + datasource);

            System.out.println("DataSource obtenido exitosamente");
            return data;
        } catch (Exception ex) {
            System.out.println("ERROR: en getDataSource::::::::::::::::::. "
                    + ex);
            return null;
        }
    }

    public synchronized Connection createConnection(String datasource) throws SQLException {
        Connection con = getDataSource(datasource).getConnection();
        return con;
    }

    public static void closeConnection(Connection connection) {

        if (connection != null) {
            try {
                connection.close();

            } catch (SQLException e) {
                System.out.println("Error closeConnection " + e);
            }

        }

    }

    /*try{
     Class.forName("oracle.jdbc.driver.OracleDriver");
     String url = "jdbc:oracle:thin:@192.168.3.90:1522:sci";
     aCon = DriverManager.getConnection(url, "microbiologo", "mic2005");
 
     }
     catch (Exception e){
     Error = e.getMessage();
     }
     }*/
    public Connection getConn() {
        try {
            aCon = createConnection("cepario");

        } catch (SQLException ex) {
            Logger.getLogger(Cconexionccbm.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aCon;

    }

    public void close() {
        try {
            aCon.close();
        } catch (Exception e) {
            Error = e.getMessage();
        }
    }
}
