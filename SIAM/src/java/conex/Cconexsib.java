

package conex;

import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase de conexion a la base de datos del SIB
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 21-05-2002
*/
public class Cconexsib  {

private Connection aCon;
public String Error;
public Cconexsib(){
  try{
    Class.forName("oracle.jdbc.driver.OracleDriver");
    String url = "jdbc:oracle:thin:@192.168.3.2:1521:sci";
    aCon = DriverManager.getConnection(url, "curador", "paque");

  }
  catch (Exception e){
    Error = e.getMessage();
  }
}

public Connection getConn(){
  return aCon;
}



public void close() {
  try{
     aCon.close();
   } catch (Exception e){
      Error = e.getMessage();
   }
}
}