package coneccam;
 
import co.org.invemar.siam.ConnectionFactory;
import java.sql.*;
import java.lang.*;
import java.util.*;
 
/**
 * Clase de conexion a la base dedatos
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 21-03-2001.
*/ 

public class Cconexioncam{
private Connection aCon;
public String Error;
public Cconexioncam(){
  try{
   ConnectionFactory cf = new ConnectionFactory();
   aCon = cf.createConnection("redcam");
 
  }
  catch (Exception e){
    Error = e.getMessage();
  }
}
 
public Connection getConn(){
  return aCon;
}
 
 
 
public void close() {
  try{
     aCon.close();
   } catch (Exception e){
      Error = e.getMessage();
   }
}
}

