package ccbm_cepas;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;

public class Ccbm_cepas{
   private String numcepa;
   private String fecha;
   private String aislado_por;
   private String genero;
   private String especie;
   private String fuente;
   private String estacion;
   private String registrada;
   private String morfologia;
   private String gram;
   private String conservacion;
   private String departamento;
   private String degrada;
   private String min;
   private String max;
   private String tipo_foto;
   private String directorio_foto;
   private String archivo_foto;
   private String descripcion_foto;

   private String Error;

   /**
  Metodo constructor de la clase
  */
  public void Ccbm_cepas(){
  }


  /**  Asigna atributos al objeto  */
  
   public void setnumcepa(String anumcepa){
    numcepa = anumcepa;
  }
   public void setfecha(String afecha){
    fecha = afecha;
  }
   public void setaislado_por(String aaislado_por){
    aislado_por = aaislado_por;
  }
   public void setgenero(String agenero){
    genero = agenero;
  }
   public void setespecie(String aespecie){
    especie = aespecie;
  }
   public void setfuente(String afuente){
    fuente = afuente;
  }
   public void setestacion(String aestacion){
    estacion = aestacion;
  }
   public void setregistrada(String aregistrada){
    registrada = aregistrada;
  }
   public void setmorfologia(String amorfologia){
    morfologia = amorfologia;
  }
   public void setgram(String agram){
    gram = agram;
  }
   public void setconservacion(String aconservacion){
    conservacion = aconservacion;
  }
   public void setdepartamento(String adepartamento){
    departamento = adepartamento;
  }
   public void setdegrada(String adegrada){
    degrada = adegrada;
  }
   public void setmin(String amin){
    min = amin;
  }
   public void setmax(String amax){
    max = amax;
  }
   public void settipo_foto(String atipo_foto){
    tipo_foto = atipo_foto;
  }
   public void setdirectorio_foto(String adirectorio_foto){
    directorio_foto = adirectorio_foto;
  }
   public void setarchivo_foto(String aarchivo_foto){
    archivo_foto = aarchivo_foto;
  }
   public void setdescripcion_foto(String adescripcion_foto){
    descripcion_foto = adescripcion_foto;
  }




  /**  Retorna los atributos del objeto  */
   public String get_numcepa()
   {
    return numcepa;
   }
   public String get_fecha()
   {
    return fecha;
   }
   public String get_aislado_por()
   {
    return aislado_por;
   }
   public String get_genero()
   {
    return genero;
   }
   public String get_especie()
   {
    return especie;
   }
   public String get_fuente()
   {
    return fuente;
   }
   public String get_estacion()
   {
    return estacion;
   }
   public String get_registrada()
   {
    return registrada;
   }
   public String get_morfologia()
   {
    return morfologia;
   }
   public String get_gram()
   {
    return gram;
   }
   public String get_conservacion()
   {
    return conservacion;
   }
   public String get_departamento()
   {
    return departamento;
   }
   public String get_degrada()
   {
    return degrada;
   }
   public String get_min()
   {
    return min;
   }
   public String get_max()
   {
    return max;
   }

   public String get_tipo_foto()
   {
    return tipo_foto;
   }
   public String get_directorio_foto()
   {
    return directorio_foto;
   }
   public String get_archivo_foto()
   {
    return archivo_foto;
   }
   public String get_descripcion_foto()
   {
    return descripcion_foto;
   }


}
