package ccbm_cepas;
import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;


public class Ccbm_contcepas{

  private String Error;
  private int tamano;
  private Vector resultados;
  public Ccbm_contcepas(){}

  /**
  * carga todas los elementos de la lista de b�squeda en un vector de objetos
  */
  public boolean contenedor(Connection conn2, String ndepar, String ndegrada, String nmorfo, String ngram)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Ccbm_cepas var = new Ccbm_cepas();
    String select = new String();

    select = "SELECT * from v_web where numcepa is not null ";
    
    if(ndepar != null && !ndepar.equals("")){
      select = select + " and nomdepto = '" + ndepar + "'";
    }

    if(ndegrada != null && !ndegrada.equals("")){
      select = select + " and degrada = '" + ndegrada + "'";
    }

    if(nmorfo != null && !nmorfo.equals("")){
      select = select + " and morfologia = '" + nmorfo + "'";
    }

    if(ngram != null && !ngram.equals("")){
      select = select + " and gram = '" + ngram + "'";
    }

    select = select + " order by numcepa, nomdepto, estacion";
    Error = select;
    try
      {
        stmt = conn2.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
                 var = new Ccbm_cepas();
                 var.setnumcepa(rs.getString(1));
                 var.setfecha(rs.getString(2));
                 var.setaislado_por(rs.getString(3));
                 var.setgenero(rs.getString(4));
                 var.setespecie(rs.getString(5));
                 var.setfuente(rs.getString(6));
                 var.setestacion(rs.getString(7));
                 var.setdepartamento(rs.getString(8));
                 var.setregistrada(rs.getString(9));
                 var.setmorfologia(rs.getString(10));
                 var.setgram(rs.getString(11));
                 var.setconservacion(rs.getString(12));
                 var.setdegrada(rs.getString(13));

              resultados.addElement(var);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }

  /**
  * carga la informaci�n de una cepa en particular
  */
  public boolean contenedor_infocepa(Connection conn2, String numcepa)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Ccbm_cepas var = new Ccbm_cepas();
    String select = new String();

    select = "SELECT distinct numcepa,fecha,aislado_por,genero,especie,fuente,estacion,nomdepto,registrada,morfologia,gram,conservacion from v_web where numcepa is not null ";
    
    if(numcepa != null && !numcepa.equals("")){
      select = select + " and numcepa = " + numcepa;
    }

    Error = select;
    try
      {
        stmt = conn2.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
                 var = new Ccbm_cepas();
                 var.setnumcepa(rs.getString(1));
                 var.setfecha(rs.getString(2));
                 var.setaislado_por(rs.getString(3));
                 var.setgenero(rs.getString(4));
                 var.setespecie(rs.getString(5));
                 var.setfuente(rs.getString(6));
                 var.setestacion(rs.getString(7));
                 var.setdepartamento(rs.getString(8));
                 var.setregistrada(rs.getString(9));
                 var.setmorfologia(rs.getString(10));
                 var.setgram(rs.getString(11));
                 var.setconservacion(rs.getString(12));

              resultados.addElement(var);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }

  /**
  * carga la informaci�n de los ensayos de degradacion de una cepa en particular
  */
  public boolean contenedor_infoensayos(Connection conn2, String numcepa)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Ccbm_cepas var = new Ccbm_cepas();
    String select = new String();

    select = "SELECT DISTINCT DEGRADA, MAX, MIN FROM V_WEB WHERE NUMCEPA IS NOT NULL ";
    
    if(numcepa != null && !numcepa.equals("")){
      select = select + " and numcepa = " + numcepa;
    }

    Error = select;
    try
      {
        stmt = conn2.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
                 var = new Ccbm_cepas();
                 var.setdegrada(rs.getString(1));
                 var.setmax(rs.getString(2));
                 var.setmin(rs.getString(3));

              resultados.addElement(var);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }

  /**
  * carga la informaci�n de las fotos de una cepa en particular
  */
  public boolean contenedor_infofoto(Connection conn2, String numcepa)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Ccbm_cepas var = new Ccbm_cepas();
    String select = new String();

    select = "SELECT C.NUMCEPA,P.DESCRIPCION TIPO,SUBSTR(F.FOTO,1,10) DIRECTORIO,SUBSTR(F.FOTO,12,11) ARCHIVO,F.DESCRIPCION FROM CEPAS C, CEPA_FOTO F, LST_PARAMETROS P WHERE C.NUMCEPA = F.CEPA AND F.TIPO_FOTO=P.CODIGO ";
    
    if(numcepa != null && !numcepa.equals("")){
      select = select + " and c.numcepa = " + numcepa;
    }

    Error = select;
    try
      {
        stmt = conn2.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
                 var = new Ccbm_cepas();
                 var.settipo_foto(rs.getString(2));
                 var.setdirectorio_foto(rs.getString(3));
                 var.setarchivo_foto(rs.getString(4));
                 var.setdescripcion_foto(rs.getString(5));

              resultados.addElement(var);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }


  /**
  * carga los departamentos desde la base de datos
  */
  public boolean contenedorndepar(Connection conn2)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Ccbm_cepas var = new Ccbm_cepas();
    String select = new String();
    select = "select distinct nomdepto from v_web where nomdepto is not null order by nomdepto" ;
     // System.out.println("Sql:"+select);
    Error = select;
    try
      {

        stmt = conn2.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              var = new Ccbm_cepas();
              var.setdepartamento(rs.getString(1));
              resultados.addElement(var);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }

  /**
  * carga los elementos degradados desde la base de datos
  */
  public boolean contenedorndegrada(Connection conn2)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Ccbm_cepas var = new Ccbm_cepas();
    String select = new String();
    select = "select distinct degrada from v_web" ;

    Error = select;
    try
      {

        stmt = conn2.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              var = new Ccbm_cepas();
              var.setdegrada(rs.getString(1));
              resultados.addElement(var);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }


  /**
  * carga los tipos de gram desde la base de datos
  */
  public boolean contenedorngram(Connection conn2)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Ccbm_cepas var = new Ccbm_cepas();
    String select = new String();
    select = "select distinct gram from v_web" ;

    Error = select;
    try
      {

        stmt = conn2.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              var = new Ccbm_cepas();
              var.setgram(rs.getString(1));
              resultados.addElement(var);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }

  /**
  * carga la lista de morfologia desde la base de datos
  */
  public boolean contenedornmorfologia(Connection conn2)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Ccbm_cepas var = new Ccbm_cepas();
    String select = new String();
    select = "select distinct morfologia from v_web" ;

    Error = select;
    try
      {

        stmt = conn2.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              var = new Ccbm_cepas();
              var.setmorfologia(rs.getString(1));
              resultados.addElement(var);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }



  /** retorna una instancia del contenedor de Ccbm_cepas dada su posicion en el
vector */
  public Ccbm_cepas getccbm_cepas(int i) {
    return ((Ccbm_cepas)resultados.elementAt(i));
  }

  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }
}
