package cvariable;

import java.io.*;
import java.sql.*;
import java.lang.*;
import java.util.*;


public class Ccontvariable{

  private String Error;
  private int tamano;
  private Vector resultados;
  public Ccontvariable(){}

  /**
  * carga todas las categorias de la base de datos en un vector de objetos
  */
  public boolean contenedor(Connection conn2, String ntipo)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Cvariable var = new Cvariable();
    String select = new String();
    select = "select * from vvariable where cod is not null";

    if(ntipo != null && !ntipo.equals("")){
      select = select + " and tipo = '" + ntipo + "'";
    }

    select = select + " order by tipo, var";
    Error = select;
    try
      {
        stmt = conn2.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
                var = new Cvariable();
                var.settipo(rs.getString(1));
                var.setcod(rs.getString(2));
                var.setnombre(rs.getString(3));
                var.setunidad(rs.getString(4));


              resultados.addElement(var);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;
     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }

  /**
  * carga todas las areas geograficas de la base de datos
  */
  public boolean contenedorntipo(Connection conn2)
  {
    boolean resp = false;
    resultados = new Vector();
    Statement stmt = null;
    ResultSet rs = null;
    Cvariable var = new Cvariable();
    String select = new String();
    select = "select distinct tipo from vvariable" ;

    Error = select;
    try
      {

        stmt = conn2.createStatement();
        rs = stmt.executeQuery(select);
        if (!rs.next()){
          resp = false;
        }
        else
        {
          do{
              var = new Cvariable();
              var.settipo(rs.getString(1));
              resultados.addElement(var);
              tamano++;
          }while(rs.next());
        }
        if (rs!= null) rs.close();
        if (stmt!= null) stmt.close();
        resp = true;

     }
      catch(SQLException e)
     {
      Error = e.getMessage() + "," + select;
      resp = false;
     }
     return resp;
  }

  /** retorna una instancia del contenedor de Cestacion dada su posicion en el
vector */
  public Cvariable getvariable(int i) {
    return ((Cvariable)resultados.elementAt(i));
  }

  /**
  * retorna el tamano del vector contenedor
  */
  public int gettamano(){
    return tamano;
  }
  /**
  * retorna el error generado por la basse de datos
  */
  public String geterror(){
    return Error;
  }
}
