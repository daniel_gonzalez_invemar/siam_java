<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>SIAM -Sistema de informacion Marino de Colombia</title>
<link href="plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css"/>
   <link rel="stylesheet" href="plantillaSitio/css/jshowoff.css" type="text/css" media="screen, projection" />
    <script type="text/javascript" src="plantillaSitio/js/siam.js"></script> 
    <script type="text/javascript" src="plantillaSitio/js/jquery.min.js"></script> 
  <script type="text/javascript"  src="http://www.google.com/jsapi"> </script> 
    <script type="text/javascript" src="plantillaSitio/js/jquery.jshowoff.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<style type="text/css"  >
body
{
	    margin-top: 0px;
		margin-right: 0px;
		margin-bottom: 0px;
		margin-left: 0px;
		background-image: url('plantillaSitio/img/fondosiam.jpg');
		background-repeat:repeat-x;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		
		
}
</style>
 <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
</head>

<body  >
<div class='centrado'>
  <table width="1056"  style="border:0px;" align="center" cellpadding="00" cellspacing="0"  >
    <tr>
      <td width="16" class=''></td>
      <td width="1008"><table width="970" border="0" align="center" cellpadding="00" cellspacing="0"  >
        <tr>
          <td width="970" height="56"><table width='749' border="0" align="left" cellpadding="00" cellspacing="0">
            <tr>
              <td height="23" colspan="2" rowspan="2" align="right"><a href="http://twitter.com/#!/siam_colombia" target="_blank"></a></td>
              <td width="150" class="textofechaactualizacion">&Uacute;ltima Actualizacion:</td>
              <td width="240"><a href="http://twitter.com/#!/siam_colombia" target="_blank"><img border="0" src="plantillaSitio/img/twitter.png" width="16" height="16" alt="twitter" /></a><a href="http://twitter.com/statuses/user_timeline/124202805.rss" target="_blank"><img  border="0" src="plantillaSitio/img/rss.png" width="16" height="16" alt="rss" /></a></td>
              <td  width="158" rowspan="2"   class="out" id="boton1" onmouseover="this.className='over'" onmouseout="this.className='out'"><a href="http://siam.invemar.org.co/siam/index.jsp"  class='opcionmenuprincipal'>Inicio</a><br /></td>
              <td  width="140" rowspan="2"  class="out" id="boton3" onmouseover="this.className='over'" onmouseout="this.className='out'" ><a href="" class='opcionmenuprincipal' target="_blank">Desarrollo<br />
                Conceptual</a></td>
              <td  width="284" rowspan="2"  class='out' id="boton4" onmouseover="this.className='over'" onmouseout="this.className='out'" ><a href="http://www.invemar.org.co/psubcategorias.jsp?idsub=182&amp;idcat=104" class='opcionmenuprincipal' target="_blank">LABSIS</a></td>
              </tr>
            <tr>
              <td><div id="google_translate_element" style="width:150px;height:20px"></div>
                <script type="text/javascript"> 
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'es',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
  </script>
                <script  type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script></td>
              <td align="right" ></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td ><table width="819"  border="0" cellpadding="00" cellspacing="0" class="tablaCenter" >
            <tr>
              <td width="315" height="106" rowspan="4" class=""  ><table width="315" border="0" cellpadding="00" cellspacing="0">
                <tr>
                  <td  class="tituloMenuSecundario"> Servicios </td>
                  </tr>
                <tr>
                  <td   class="opcionMenu" onmouseover="resaltarOpcion(this,1,'opcionMenuresalte')" onmouseout="restablecerOpcion(this,1,'opcionMenu')" onclick="submenu(1);"><a id="opcion1" href='javascript:void(0)' onclick="submenu(1);" class="opcionlinkmenusecundario"> Biodiversidad Marina</a></td>
                  </tr>
                <tr >
                  <td    class="opcionMenu" onmouseover="resaltarOpcion(this,2,'opcionMenuresalte')" onmouseout="restablecerOpcion(this,2,'opcionMenu')" onclick="submenu(2);"><a  id='opcion2' href='javascript:void(0)' onclick="submenu(2);" class="opcionlinkmenusecundario">Manejo Integrado de Zonas Costeras</a></td>
                  </tr>
                <tr >
                  <td   class="opcionMenu" onmouseover="resaltarOpcion(this,3,'opcionMenuresalte')" onmouseout="restablecerOpcion(this,3,'opcionMenu')" onclick="submenu(3);"><a  id='opcion3' href='javascript:void(0)' onclick="submenu(3);" class="opcionlinkmenusecundario">Uso de los Recursos Pesqueros</a></td>
                  </tr>
                <tr >
                  <td  class="opcionMenu" onmouseover="resaltarOpcion(this,4,'opcionMenuresalte')" onmouseout="restablecerOpcion(this,4,'opcionMenu')" onclick="submenu(4);"><a id='opcion4' href='javascript:void(0)' onclick="submenu(4)" class="opcionlinkmenusecundario">Monitoreo de los Ambientes Marinos</a></td>
                  </tr>
                <tr >
                  <td  class="opcionMenu" onmouseover="resaltarOpcion(this,5,'opcionMenuresalte')" onmouseout="restablecerOpcion(this,5,'opcionMenu')" onclick="submenu(5);"><a  id='opcion5' href='javascript:void(0)' onclick="submenu(5)" class="opcionlinkmenusecundario">Amenazas Naturales</a></td>
                  </tr>
                <tr >
                  <td  class="opcionMenu" onmouseover="resaltarOpcion(this,6,'opcionMenuresalte')" onmouseout="restablecerOpcion(this,6,'opcionMenu')" onclick="submenu(7);"><a id='opcion6' href='javascript:void(0)' onclick="submenu(7)" class="opcionlinkmenusecundario">Servicio de Informaci&oacute;n Geogr&aacute;fica</a></td>
                  </tr>
                <tr >
                  <td  class="opcionMenu" onmouseover="resaltarOpcion(this,7,'opcionMenuresalte')" onmouseout="restablecerOpcion(this,7,'opcionMenu')" onclick="submenu(6);"><a id='opcion7' href='javascript:void(0)' onclick="submenu(6)" class="opcionlinkmenusecundario">Sistemas de Informaci&oacute;n Documental</a></td>
                  </tr>
                <tr >
                  <td  class="opcionMenu" onmouseover="resaltarOpcion(this,8,'opcionMenuresalte')" onmouseout="restablecerOpcion(this,8,'opcionMenu')" onclick="submenu(8);"><a id='opcion8' href='javascript:void(0)' onclick="submenu(8)" class="opcionlinkmenusecundario">Portales Tem&aacute;ticos</a></td>
                  </tr>
                <tr >
                  <td  class="footermenu" onmouseover="resaltarOpcion(this,9,'overfootermenu')" onmouseout="restablecerOpcion(this,9,'footermenu')" onclick="submenu(9);"><a id='opcion9' href='javascript:void(0)' onclick="submenu(9)" class="opcionlinkmenusecundario">Boletines y Redes</a></td>
                  </tr>
                </table></td>
              <td  >&nbsp;</td>
              <td colspan="3"  >&nbsp;</td>
              </tr>
            <tr>
              <td  >&nbsp;</td>
              <td colspan="3"  ><span  id='migadepan' class="migadepan"><a href="http://www.invemar.org.co/" class="migadepan">invemar.org.co &gt; </a> Siam &gt; Noticias</span></td>
              </tr>
            <tr>
              <td  >&nbsp;</td>
              <td width="36">&nbsp;</td>
              <td width="81"  >&nbsp;</td>
              <td width="375" class="titulosubmenu" id='titulosubmenu' >Version movil</td>
              <td width="8" ></td>
              </tr>
            <tr>
              <td width="4" height="330" class=''  >&nbsp;</td>
              <td colspan="3" class=''  id='itemsmenu'><table width="492" align="center"  border="0" cellpadding="0" cellspacing="2" class=''>
                <tr>
                  <td colspan="2" align="left" class="textonoticias">&nbsp;</td>
                  <td class="itemtituloitemsubmenu">&nbsp;</td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td align="left">&nbsp;</td>
                  <td class="itemtituloitemsubmenu">&nbsp;</td>
                  <td class="itemtituloitemsubmenu">&nbsp;</td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td width="25" align="center">&nbsp;</td>
                  <td width="268" class="textonoticias"></td>
                  <td width="20" rowspan="3">&nbsp;</td>
                  <td width="169">&nbsp;</td>
                  </tr>
                <tr>
                  <td align="center">&nbsp;</td>
                  <td align="right"><a href="" target="_blank" ><img border="0" alt='boton de noticias' src="plantillaSitio/img/boton-noticias.png"/></a></td>
                  <td></td>
                  </tr>
                <tr>
                  <td width="25" align="center">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="169">&nbsp;</td>
                  </tr>
                <tr>
                  <td colspan="4"><hr  class="lineaseparacion" /></td>
                  </tr>
                <tr>
                  <td align="center">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td rowspan="3" align="center">&nbsp;</td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td align="center">&nbsp;</td>
                  <td class="textonoticias" ></td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td align="center">&nbsp;</td>
                  <td align="right"><a href="" target="_blank"><img border="0" alt='boton de noticias' src="plantillaSitio/img/boton-noticias.png"/></a></td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td colspan="2">&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            <tr>
              <td height="18"  >&nbsp;</td>
              <td colspan="4"    >&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="18" id='panel3'><table width="971" border="0" cellpadding="00">
            <tr>
              <td><table width="947" border="0" cellpadding="00">
                <tr>
                  <td width="237" align="center"><a href="http://www.siac.gov.co/portal/default.aspx" target="_blank"><img  align="middle" border='0' src="plantillaSitio/img/iconsiac.png" width="119" height="56" alt="iconsiac" /></a></td>
                  <td width="200" align="center"><a href="http://siatac.siac.net.co/web/guest/inicio" target="_blank"><img border="0" src="plantillaSitio/img/iconsiactac.png" width="119" height="56" alt="siac at" /></a></td>
                  <td width="250" align="center"><a href="http://www.siac.net.co/web/sib/home" target="_blank"><img  border="0" src="plantillaSitio/img/iconsib.png" width="119" height="56" alt="sibm" /></a></td>
                  <td width="250" align="center"><a href="http://www.gobiernoenlinea.gov.co/web/guest;jsessionid=B1A84E14EFA658C24E3DCB260E045CCA" target="_blank"><img  border="0" src="plantillaSitio/img/icongel.png" width="119" height="56" alt="gobierno en linea" /></a></td>
                </tr>
                <tr>
                  <td align="center">&nbsp;</td>
                  <td colspan="3" align="center">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="53"   >&nbsp;</td>
        </tr>
      </table></td>
      <td width="30" rowspan="2"  class="">&nbsp;</td>
    </tr>
  </table>
</div>
<script type="text/javascript" language="javascript">
 document.writeln(getVisitas());
</script>
</body>
</html>

