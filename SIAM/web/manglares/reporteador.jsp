<%@page import="java.io.IOException"%>
<%@page import="net.sf.jasperreports.engine.JasperExportManager"%>
<%@page import="net.sf.jasperreports.engine.JasperReport"%>
<%@page import="net.sf.jasperreports.engine.JRExporter"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>
<%@page import="net.sf.jasperreports.engine.util.JRLoader"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.JRExporterParameter"%>
<%@page import="net.sf.jasperreports.j2ee.servlets.ImageServlet"%>
<%@page import="java.io.OutputStream"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporter"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.JasperFillManager"%>
<%@page import="co.org.invemar.siam.manglares.model.DatosEstructurales"%>
<%@page import="java.sql.Connection"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.io.File"%>
<%



   
    File desfile = new File(application.getRealPath("/manglares/imprimeReporte.html"));

    DatosEstructurales de = new DatosEstructurales();
    Connection con = de.getConnection();

    String codigoEspecie = request.getParameter("codigoespecie");
    String variable=null;
    variable = request.getParameter("variable");

    String nivel = request.getParameter("nivel");
    //String columnaserie = request.getParameter("columnaserie");
    String transecto    = request.getParameter("transecto");
    String estacion     = request.getParameter("estacion");
    String parcela     = request.getParameter("parcela");
     File reportFile = new File(application.getRealPath("/manglares/jasperReport/EstructuraManglaresNivelEst"+nivel+".jasper"));
    //variable = "AB";
    //codigoEspecie = "2040.55.325";

    Map parameters = new HashMap();
    parameters.put("codigoEspecie", codigoEspecie);
    parameters.put("variable", variable);   
    parameters.put("nivel",nivel);
    parameters.put("estacion",estacion);
    parameters.put("transecto",transecto);      
    parameters.put("parcela",parcela);  
    

    String reportName = request.getParameter("reportName");
    JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile.getPath());
    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);

    //System.out.println(jasperPrint.getPageHeight());
    //System.out.println(jasperPrint.getPageWidth());
    
    
    if (jasperPrint.getPages().size() == 0) {
        //RequestDispatcher rd = getServletContext().getRequestDispatcher("/reportOption/ErrorReport.jsp");
        //rd.forward(request, response);
        out.println("No hay datos para la variable.");
    }
    JRHtmlExporter exporter = new JRHtmlExporter();

    request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint); 
    exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getContextPath()+ "/servlets/image?image=");
    exporter.setParameter(JRHtmlExporterParameter.HTML_HEADER, "<br/>");
    exporter.setParameter(JRHtmlExporterParameter.HTML_FOOTER, "<br/>");
    exporter.setParameter(JRHtmlExporterParameter.FRAMES_AS_NESTED_TABLES,Boolean.TRUE);
    exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
    exporter.setParameter(JRHtmlExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
    exporter.setParameter(JRHtmlExporterParameter.SIZE_UNIT, "px");
    exporter.setParameter(JRHtmlExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
    exporter.setParameter(JRHtmlExporterParameter.JASPER_PRINT, jasperPrint);
    exporter.setParameter(JRHtmlExporterParameter.OUTPUT_WRITER, out);
    //exporter.setParameter(JRHtmlExporterParameter.FONT_MAP, fontMap);

    try {
        response.setContentType("text/html");   
        exporter.exportReport();
       
    } catch (JRException e) {
        out.println("Error generando el reporte:" + e.toString());
    } finally {
        
        
    }

%>
