<%-- 
    Document   : index
    Created on : 1/08/2012, 03:12:13 PM
    Author     : usrsig15
--%>
<%@page import="co.org.invemar.siam.manglares.vo.Region"%>
<%@page import="co.org.invemar.siam.manglares.vo.Proyectos"%>
<%@page import="co.org.invemar.siam.manglares.vo.Estacion"%>
<%@page import="co.org.invemar.siam.manglares.vo.Variable"%>
<%@page import="co.org.invemar.siam.manglares.vo.Especie"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="co.org.invemar.siam.manglares.model.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Est�disticas Manglares</title>
        <link href="css/Mangle.css" rel="stylesheet"  type="text/css"/>
        <link type="text/css" href="../libreriascomunes/css/redmond/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>        
        <script type="text/javascript" src="js/generaReporte.js"></script>
        <script type="text/javascript" src="js/monitoreo.js"></script>
        <script type="text/javascript" src="../libreriascomunes/js/jquery-ui-1.8.23.custom.min.js"></script> 
        <style type="text/css" rel="stylesheet">
            .ui-widget-header {
                background-color: #399797   
            }
        </style>
        <script>
            $(function() {
                // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
                $( "#dialog:ui-dialog" ).dialog( "destroy" );
	
                $( "#dialog-modal" ).dialog({
                    height: 480,
                    width:870,
                    modal: true
                });                
                
                $( "#tabs" ).tabs();            
                 $("#dialog-modal").("display","none");
                
            });
        </script>
    </head>
    <body onload="configurarParametrosIniciales(0)">
        <!--<body onload="">-->
        <div id="dialog-modal"  style="display:none"  title="Sistema de estad&iacute;sticas REDCAM -Colombia">    
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">INTRODUCCI&Oacute;N</a></li>
                    <li><a href="#tabs-2">INFORMACI&Oacute;N GENERAL</a></li>
                    <li><a href="#tabs-3">SERVICIOS</a></li>
                    <li><a href="#tabs-4">CONSIDERACIONES DE USO</a></li>
                    <li><a href="#tabs-5">D&Iacute;GANOS QUE PIENSA</a></li>
                </ul>
                <div id="tabs-1">
                    <p style="text-align: justify;font-size: 1.0em">                        Este es un producto inform&aacute;tico dise�ado a partir de resultados que hacen parte del proyecto "Monitoreo de las condiciones ambientales y los cambios estructurales y funcionales de las comunidades vegetales y de los recursos pesqueros 
                        durante la rehabilitaci&oacute;n de la Ci&eacute;naga Grande de Santa Marta (CGSM)"; donde se presentan salidas gr&aacute;ficas de variables estructurales (&aacute;rea basal, densidad de &aacute;rboles, di&aacute;metro promedio cuadr&aacute;tico y biomasa &aacute;rea) y de composici&oacute;n de especies 
                        (&iacute;ndice de valor de importancia - IVI-). Los valores de estas variables permiten analizar la estructura, el estado de recuperaci&oacute;n y la din&aacute;mica del bosque a trav&eacute;s del tiempo. Adem&aacute;s se puede acceder a informaci�n cartogr&aacute;fica 
                        que identifica las diferentes coberturas de la tierra y su din&aacute;mica en el sistema Ci&eacute;naga Grande de Santa Marta. 
                        <br/>


                    </p>
                </div>
                <div id="tabs-2" style="text-align: justify;font-size: 1.0em">
                    <p>
                        El proyecto "Monitoreo de las condiciones ambientales y los cambios estructurales y funcionales de las comunidades vegetales y de los recursos pesqueros durante la rehabilitaci&oacute;n de la Ci&eacute;naga Grande de Santa Marta (CGSM)" <a style="color:#399797;font-weight: bold" href="http://www.invemar.org.co/redcostera1/invemar/docs/monitoreoCGSM/Informe_CGSM_2011.pdf"  target="_blank"> http://www.invemar.org.co/redcostera1/invemar/docs/monitoreoCGSM/Informe_CGSM_2011.pdf</a> se inici&oacute; por la necesidad de evaluar la respuesta ante esfuerzos y acciones realizados 
                        para lograr la rehabilitaci&oacute;n del sistema despu&eacute;s de alcanzar un avanzado estado de deterioro ambiental, como consecuencia de actividades antr&oacute;picas. El gran volumen de informaci�n recopilado durante el proyecto, permiti&oacute; la implementaci&oacute;n de este producto web, para facilitar la interpretaci&oacute;n y ampliar la divulgaci&oacute;n de los resultados.
                        Toda la comunidad de investigadores o entidades nacionales e internacionales que deseen conocer el estado y din&aacute;mica de los bosques de mangle colombianos pueden acceder a esta aplicaci&oacute;n a trav&eacute;s de cualquier dispositivo electr&oacute;nico con navegador de p&aacute;ginas web y capacidad de interpretar java script y HTML en su versi�n 4.0 � superior. Las versi&oacute;n web es compatible con todos los navegadores web (FireFox, Safari, Chrome, Internet Explorer, Opera, entre otros).

                    </p>
                </div>
                <div id="tabs-3" style="text-align: justify;font-size: 1.0em">
                    <p>
                    <ol>
                        <li><span style="font-weight: bold">Salidas gr&aacute;ficas y tabuladas:<span><span style="font-weight: normal"> el m&oacute;dulo permite crear gr&aacute;ficas y tablas con resultados de variables estructurales y de composici&oacute;n de especies, medidas desde el a�o 1995 en cinco estaciones de muestreo seleccionadas a partir de un gradiente de perturbaci&oacute;n definido por el desarrollo estructural del bosque y los niveles de salinidad en aguas y suelos. Las siguientes son las variables disponibles <a href="http://siam.invemar.org.co/siam/ayuda/manglares/InfomeBaseDeDatosOnline.doc" target="_blank" style="color:#399797; font-weight: bold">(ver manual de usuario):</a></span>
                            <ul style="font-weight:normal">
                                <li>&Aacute;rea basal</li>
                                <li>Densidad</li>
                                <li>D&iacute;metro promedio cuadr&aacute;tico</li>
                                <li>Biomasa a&eacute;rea</li>
                                <li>&Iacute;ndice de Valor de Importancia</li>
                                <li>Abundancia relativa</li>
                                <li>Frecuencia relativa</li>
                                <li>Dominancia relativa<br/><br/></li>
                            </ul> 
                             Todas, de gran importancia para el an�lisis estructural, del estado de recuperaci�n y de la din�mica del bosque de manglar.<br/><br/>
                        </li>                        
                        <li><span style="font-weight: bold">Geovisor:</span> herramienta que permite acceder a la informaci�n cartogr&aacute;fica del sector Ci&eacute;naga Grande de Santa Marta. El geoservicio tiene varias capas disponibles que pueden ser consultadas para que el usuario pueda hacer comparaciones espaciales y temporales de los cambios de las coberturas de la tierra desde el a�o 1995.
                            
                        </li>
                    </ol>
                    </p>
                </div>
                <div id="tabs-4" style="text-align: justify;font-size: 1.0em">
                    <p>El dise�o y funcionamiento de este m&oacute;dulo web se encuentra en peri&oacute;do de prueba y puede presentar algunos errores, por lo que se advierte al usuario utilizar la informaci&oacute;n aqu&iacute; contenida de manera correcta y bajo su propia responsabilidad en conocimiento de las limitaciones mencionadas. 
                       Quedan reservados todos los derechos de propiedad intelectual y derechos de autor.<br/></br>
                        
                    </p>
                </div>
                <div id="tabs-5" style="text-align: justify;font-size: 1.0em">
                    <p>
                        Consideramos la importancia de la opini&oacute;n del usuario, por tanto, le solicitamos que despu&eacute;s de usado este servicio llene la siguiente encuesta de satisfacci&oacute;n: <a href="https://spreadsheets0.google.com/embeddedform?formkey=dHB1ZHdIVk5nbEs0VlRNaW9VdjU2X0E6MA" target="_blank" style="color:red">Encuesta de satisfacci&oacute;n de usuarios</a>.
                        Sus aportes y sugerencias ser&aacute;n considerados para mejorar el producto. Si tiene alg&uacute;n comentario adicional o inquietud por favor cont&aacute;ctese con <a href="mailto:labsis@invemar.org.co." style="color:red">labsis@invemar.org.co.</a>
                   </p>
                </div>
            </div>
        </div>

        <input type="hidden" id="context" value="siam.invemar.org.co"  />
        <%
            DatosEstructurales datosestructurales = new DatosEstructurales();

        %>
        <div id="header">
            <div id="logos" ><img  style="border:0;width:79px; height:90px" id="logoinvemar" src="../plantillaSitio/img/icono_invemar.png" /><div id="titulo"> Estructura y Composici�n de Bosques de Mangle</div><img  id="logomads" src="http://siam.invemar.org.co/siam/plantillaSitio/img/MADSminambientergbhorizontal2012.png" /></div>
            <div id="menu" ></div>
            <div id="migapanmanglar"></div>
        </div>
        <div id="content">
            <div id="migadepan">
                Estas en: <a href="http://www.invemar.org.co/" >invemar.org.co</a>/<a href="http://siam.invemar.org.co/siam">siam</a>/<a href="index.jsp" id="ubicacionactual">manglares</a>

                <a href="http://siam.invemar.org.co/siam/ayuda/manglares/TutorialManglares.wmv" target="_blank" id="textotutorial"><img  id="tutorial" src="img/videotutorial.png" alt="" title="Video tutorial"/>Video tutorial</a>
                <a href="http://siam.invemar.org.co/siam/ayuda/manglares/InfomeBaseDeDatosOnline.doc" target="_blank" id="textotutorial">Tutorial<img  id="tutorial" src="img/help.png" alt="" title="Tutorial"/></a>


            </div>
            <div id="panel">
                <div id="parametros">
                    <span id="tituloestadistica" >
                        Genenar Estad&iacute;sticas   a Nivel:                    
                    </span>
                    <div class="panel" id="panelestadisticos" >
                        <select id="nivelestadistico" name="nivelestadistico"  onchange="visualizarParametrosPorNiveles()">
                            <option value="0">Seleccione un nivel</option>
                            <option value="3">Sector</option>
                            <option value="2">Estaciones</option>
                            <option value="4">Transecto</option>
                            <option value="1">Parcelas</option>
                        </select>
                    </div>
                    <span id="ayuda" >

                    </span>
                    <div class="panel" id="panelayuda" >

                    </div>                    


                    <span id="tituloregion">
                        Regi&oacute;n:                        
                    </span>
                    <div class="panel" id="panelregiones">
                        <select id='nivelregiones' name='nivelregiones' onchange="getComboBoxDepartamentos()" >   
                            <%
                                ArrayList regiones = datosestructurales.getRegiones();
                                Iterator it = regiones.iterator();
                                out.println("<option value='-'>-</option>");
                                while (it.hasNext()) {
                                    Region region = (Region) it.next();
                                    out.println("<option value='" + region.getCodigo() + "'>" + region.getNombre() + "</option>");

                                }

                            %>         
                        </select>
                    </div>
                    <span id="titulodepartamento">
                        Departamento:                        
                    </span>
                    <div class="panel" id="paneldepartamentos">

                    </div>
                    <span id="titulosector">
                        Sectores:                        
                    </span>
                    <div class="panel" id="panelsectores">

                    </div> 
                    <span id="tituloproyeto">
                        Proyectos:                        
                    </span>
                    <div id="panelproyectos">

                    </div>
                    <span id="tituloestaciones">
                        Estaciones:                        
                    </span>
                    <div class="panel" id="panelestaciones">

                    </div>
                    <span id="titulotransecto" >
                        Transectos:                        
                    </span>
                    <div class="panel" id="paneltransecto">

                    </div>
                    <span id="tituloparcelas" >
                        Parcelas:                        
                    </span>
                    <div class="panel" id="panelparcelas">

                    </div>                     
                    <span id="titulovariable">
                        Variable a consultar:                        
                    </span>
                    <div class="panel" id="panelvariables">
                        <select id="variables" name="variables" >
                            <%

                                ArrayList lista = datosestructurales.getVariables();
                                it = lista.iterator();

                                while (it.hasNext()) {
                                    Variable variable = (Variable) it.next();
                                    out.println("<option value='" + variable.getCodigo() + "'>" + variable.getNombre() + "</option>");
                                }
                            %>
                        </select>
                    </div><br/>
                    <span id="tituloespecie">
                        Especies:
                    </span><br/>   
                    <div class="panel" id="panelespecies">
                        <%

                            lista = datosestructurales.getEspecies();
                            it = lista.iterator();
                        %>  
                        <select id="especies" name="especies"  onchange="visualizarVariablesActivas()">
                            <%
                                out.println("<option value='-'>-</option>");
                                out.println("<option value='t'>Todas las especies</option>");
                                while (it.hasNext()) {
                                    Especie especie = (Especie) it.next();
                                    out.println("<option value='" + especie.getCodigo() + "'>" + especie.getNombre() + "</option>");
                                }
                            %>              
                        </select>
                    </div> 
                    <input type="button" id="generarReporte" name="generarReporte" value="Generar Reporte" onclick="generarReporte()" /> 

                </div>
            </div>
            <div id="visualizador">
                <iframe id="grafica"  >

                </iframe> 
                <div id="menuopciones">
                    <a onclick="exportarExcel();"><img src="../plantillaSitio/img/exportarExcel.png" id="exportaexcel"/>Expotar a Excel</a>
                    <div id="indicadordatosexportar"></div>
                </div>
                <div id="tablaestadisticas">

                </div>    
            </div>


        </div>
        <div id="footer">
            <div id="linea"></div>
            <div id="piepagina">
                <p id="textopiepagina">Sede principal: Cerro Punta Bet�n - Sta Marta, Colombia |A.A. 1016<br/>
                    Tel�fonos: (+57)(+5)4328600 - Fax: (+57) (+5) 4328682<br/>
                    Horario de Atenci�n (Sta Marta): De 7:00 a 11:30 am y de 1:00 a 5:00 pm de Lunes a Viernes<br/>
                    Contacto | otras sedes | webmaster@invemar.org.co |<a href="http://www.invemar.org.co/pdirectriz.jsp" target="_blank"> Revise nuestra directriz de privacidad</a><br/>
                    Copyright � 2012 Todos los derechos reservados</p>
                <img src="../plantillaSitio/img/iso.png" id="logocalidad">
            </div>
        </div>   
    </body>
</html>
