/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Comment
 */

var nivelestadistico = 0;
var addreshost = "";
var modovista = 0;

/**
 * Comment
 */
function configurarParametrosIniciales(modevista) {
    addreshost = document.getElementById("context").value;
    modovista = modevista;
}

function generarReporte() {



    var variable = document.getElementById("variables").value;
    var codigoespecie = document.getElementById("especies").value;
    var auxnivelestadistico = document.getElementById("nivelestadistico").value;
    if (codigoespecie == 't') {
        if (nivelestadistico == 3) {
            nivelestadistico = 5;
        } else if (nivelestadistico == 2) {
            nivelestadistico = 6;
        } else if (nivelestadistico == 4) {
            nivelestadistico = 7;
        } else if (nivelestadistico == 1) {
            nivelestadistico = 8;
        }

    } else {
        nivelestadistico = auxnivelestadistico;
    }

    var transecto = null;
    var estacion = null;
    var parcela = null;
    if (document.getElementById("niveltransecto") != null) {
        transecto = document.getElementById("niveltransecto").value;
    }
    if (document.getElementById("nivelestacion") != null) {
        estacion = document.getElementById("nivelestacion").value;
    }
    if (document.getElementById("nivelparcelas") != null) {
        parcela = document.getElementById("nivelparcelas").value;
    }



    var ColumnaDBGraficar = "";
    //console.log(nivelestadistico);
    switch (nivelestadistico) {
        case 3:
            ColumnaDBGraficar = 'sector';

            break;
        case 2:
            ColumnaDBGraficar = 'nomest';
            break;

        case 1:
            ColumnaDBGraficar = 'parcela';
            break;

        case 4:
            ColumnaDBGraficar = 'Transecto';
            break;
    }


    document.getElementById('grafica').src = "reporteador.jsp?variable=" + variable + "&codigoespecie=" + codigoespecie + "&nivel=" + nivelestadistico + "&transecto=" + transecto + "&estacion=" + estacion + "&parcela=" + parcela;

    $.ajax({
        type: "POST",
        contentType: "application/text/html",
        url: "http://" + addreshost + "/siam/ServicioComponentes?componente=manglares&action=TablaEstadisticosDeVariable&codigoespecie=" + codigoespecie + "&variable=" + variable + "&nivelestadistica=" + nivelestadistico + "&transecto=" + transecto + "&estacion=" + estacion + "&parcela=" + parcela,
        beforeSend: function(msg) {
            $('#tablaestadisticas').html("<img border='0'   style='text-align:center;margin-right:32px; margin-top: 32px;' src='../manglares/img/Loader.gif' alt='Loading...'/>");

        },
        success: function(msg) {
            $('#tablaestadisticas').html('');
            $("#tablaestadisticas").html(msg);
            $('#menuopciones').css("display", "inline");

        },
        error: function(xml, msg) {

            $("#tablaestadisticas").html('<span class="error">Error generando la  tabla estad&iacute;cas.</span>');

        }
    });
}
function getComboBoxTransectos() {

    var codEstacion = document.getElementById("nivelestacion").value;
    console.log(nivelestadistico);
    if (nivelestadistico == 1 || nivelestadistico == 4 || nivelestadistico == 7) {
        // alert(codEstacion);
        $.ajax({
            type: "POST",
            contentType: "application/text/html",
            url: "http://" + addreshost + "/siam/ServicioComponentes?componente=manglares&action=TransectoPorIDEstaciones&codestacion=" + codEstacion,
            beforeSend: function(msg) {
                $('#paneltransecto').html("<img border='0'   style='text-align:center;margin-right:32px; margin-top: 32px;' src='../manglares/img/Loader.gif' alt='Loading...'/>");

            },
            success: function(msg) {
                document.getElementById("titulotransecto").style.display = "inline";
                $('#paneltransecto').html('');
                $("#paneltransecto").html(msg);


            },
            error: function(xml, msg) {

                $("#paneltransecto").html('Error obteniendo los transectos por estaciones');

            }
        });
        limpiarHTMLElementoDOM('panelparcelas');
        document.getElementById("tituloparcelas").style.display = "none";
    }
}
function getComboBoxParcelas() {
    var inicialtransecto = document.getElementById("niveltransecto").value;
    var codEstacion = document.getElementById("nivelestacion").value;
    if (nivelestadistico == 1 || nivelestadistico == 8) {
        $.ajax({
            type: "POST",
            contentType: "application/text/html",
            url: "http://" + addreshost + "/siam/ServicioComponentes?componente=manglares&action=ParcelasPorIDTransecto&niveltransecto=" + inicialtransecto + "&codigoestacion=" + codEstacion,
            beforeSend: function(msg) {
                $('#panelparcelas').html("<img border='0'   style='text-align:center;margin-right:32px; margin-top: 32px;' src='../manglares/img/Loader.gif' alt='Loading...'/>");

            },
            success: function(msg) {
                document.getElementById("tituloparcelas").style.display = "inline";
                $('#panelparcelas').html('');
                $("#panelparcelas").html(msg);


            },
            error: function(xml, msg) {

                $("#panelparcelas").html('<span class="error">Error obteniendo las parcelas por transecto.</span>');

            }
        });
    }
}
function getComboBoxRegiones() {
    var nombreproyecto = document.getElementById("proyectos").value;

    // alert(addreshost);
    $.ajax({
        type: "POST",
        contentType: "application/text/html",
        url: "http://" + addreshost + "/siam/ServicioComponentes?componente=manglares&action=RegionesPorIDProyecto&nombreproyecto=" + nombreproyecto,
        beforeSend: function(msg) {
            $('#panelregiones').html("<img border='0'   style='text-align:center;margin-right:32px; margin-top: 32px;' src='../manglares/img/Loader.gif' alt='Loading...'/>");

        },
        success: function(msg) {
            document.getElementById("tituloregion").style.display = "inline";
            $('#panelregiones').html('');
            $("#panelregiones").html(msg);


        },
        error: function(xml, msg) {

            $("#panelregiones").html('<span class="error">Error obteniendo las regions del proyecto.</span>');

        }
    });
}
function getComboBoxDepartamentos() {
    var nombreregion = document.getElementById("nivelregiones").value;

    // alert(codEstacion);
    $.ajax({
        type: "POST",
        contentType: "application/text/html",
        url: "http://" + addreshost + "/siam/ServicioComponentes?componente=manglares&action=DepartamentoPorIDRegion&nombreregion=" + nombreregion,
        beforeSend: function(msg) {
            $('#paneldepartamentos').html("<img border='0'   style='text-align:center;margin-right:32px; margin-top: 32px;' src='../manglares/img/Loader.gif' alt='Loading...'/>");

        },
        success: function(msg) {
            document.getElementById("titulodepartamento").style.display = "inline";
            $('#paneldepartamentos').html('');
            $("#paneldepartamentos").html(msg);


        },
        error: function(xml, msg) {

            $("#paneldepartamentos").html('<span class="error">Error obteniendo los departamentos.</span>');

        }
    });
}
function getComboBoxSectores() {
    var nombredepartamento = document.getElementById("niveldepartamento").value;

    // alert(codEstacion);
    $.ajax({
        type: "POST",
        contentType: "application/text/html",
        url: "http://" + addreshost + "/siam/ServicioComponentes?componente=manglares&action=SectorPorIDDepartamento&nombredepartamento=" + nombredepartamento,
        beforeSend: function(msg) {
            $('#panelsectores').html("<img border='0'   style='text-align:center;margin-right:32px; margin-top: 32px;' src='../manglares/img/Loader.gif' alt='Loading...'/>");

        },
        success: function(msg) {
            document.getElementById("titulosector").style.display = "inline";
            $('#panelsectores').html('');
            $("#panelsectores").html(msg);


        },
        error: function(xml, msg) {

            $("#panelsectores").html('<span class="error">Error obteniendo los sectores .</span>');

        }
    });
}
/**
 * Comment
 */
function getProyectos() {

    //getComboBoxEstaciones();

    var nombresector = document.getElementById("nivelsector").value;
    //console.log(nombresector);
    $.ajax({
        type: "POST",
        contentType: "application/text/html",
        url: "http://" + addreshost + "/siam/ServicioComponentes?componente=manglares&action=ProyectosPorSector&nombresector=" + nombresector,
        beforeSend: function(msg) {
            $('#panelproyectos').html("<img border='0'   style='text-align:center;margin-right:32px; margin-top: 32px;' src='../manglares/img/Loader.gif' alt='Loading...'/>");

        },
        success: function(msg) {
            //document.getElementById("tituloestaciones").style.display="inline";
            $('#tituloproyeto').css('display', 'inline');
            $('#panelproyectos').html('');
            $("#panelproyectos").html(msg);


        },
        error: function(xml, msg) {

            $("#panelproyectos").html('<span class="error">Error obteniendo los proyecto por el sector.</span>');

        }
    });


}
function getComboBoxEstaciones() {
    var nombresector = document.getElementById("nivelsector").value;
    //console.log('ejecutado');
    if (nivelestadistico == 1 || nivelestadistico == 4) {


        $.ajax({
            type: "POST",
            contentType: "application/text/html",
            url: "http://" + addreshost + "/siam/ServicioComponentes?componente=manglares&action=EstacionesPorIDSector&nombresector=" + nombresector,
            beforeSend: function(msg) {
                $('#panelestaciones').html("<img border='0'   style='text-align:center;margin-right:32px; margin-top: 32px;' src='../manglares/img/Loader.gif' alt='Loading...'/>");

            },
            success: function(msg) {
                document.getElementById("tituloestaciones").style.display = "inline";
                $('#panelestaciones').html('');
                $("#panelestaciones").html(msg);


            },
            error: function(xml, msg) {

                $("#panelestaciones").html('<span class="error">Error obteniendo las estaciones .</span>');

            }
        });
    }
    cargarEspeciesYVariables();
}
function getComboBoxEstacionesPorProyecto() {
    var proyecto = document.getElementById("proyectos").value;
    //console.log('ejecutado');
    if (nivelestadistico == 1 || nivelestadistico == 4) {


        $.ajax({
            type: "POST",
            contentType: "application/text/html",
            url: "http://" + addreshost + "/siam/ServicioComponentes?componente=manglares&action=EstacionesPorProyecto&proyecto=" + proyecto,
            beforeSend: function(msg) {
                $('#panelestaciones').html("<img border='0'   style='text-align:center;margin-right:32px; margin-top: 32px;' src='../manglares/img/Loader.gif' alt='Loading...'/>");

            },
            success: function(msg) {
                document.getElementById("tituloestaciones").style.display = "inline";
                $('#panelestaciones').html('');
                $("#panelestaciones").html(msg);


            },
            error: function(xml, msg) {

                $("#panelestaciones").html('<span class="error">Error obteniendo las estaciones por proyecto.</span>');

            }
        });
    }
    cargarEspeciesYVariables();
}

function cargarEspeciesYVariables() {
    document.getElementById("tituloespecie").style.display = "inline";
    document.getElementById("panelespecies").style.display = "inline";
    document.getElementById("titulovariable").style.display = "inline";
    document.getElementById("panelvariables").style.display = "inline";


}
function visualizarParametrosPorNiveles() {

    /*if(document.getElementById("proyectos")!=null){
     document.getElementById("proyectos").style.display="inline";
     }
     if(document.getElementById("tituloproyeto")!=null){
     document.getElementById("tituloproyeto").style.display="inline";
     }*/
    nivelestadistico = document.getElementById("nivelestadistico").value;
    document.getElementById("nivelregiones").style.display = "inline";
    document.getElementById("tituloregion").style.display = "inline";

    switch (parseInt(nivelestadistico)) {
        case 3://Nivel por Sector

            document.getElementById("tituloestaciones").style.display = "none";
            document.getElementById("panelestaciones").innerHTML = '';

            document.getElementById("titulotransecto").style.display = "none";
            document.getElementById("paneltransecto").innerHTML = '';

            document.getElementById("tituloparcelas").style.display = "none";
            document.getElementById("panelparcelas").innerHTML = '';

            visualizarBotonEstadisticas('Generar reporte a nivel sector');
            visualizaTextoAyuda('<br/>Debe seleccionar: <br/> 1. Regi&oacute;n <br/> 2. Departamento<br/>  3. Sector <br/> 4. Variable  <br/> 5. Especie');
            nivelestadistico = 3;
            break;
        case 2://Nivel por estaciones
            if (document.getElementById("panelestaciones").innerHTML == '') {

                //alert('Debe seleccionar: \n 1.)Sector.\n 2.)Estaciones. ');
                ocultarBotonEstadisticas();
            }
            visualizaTextoAyuda('<br/>Debe seleccionar: <br/> 1. Regi&oacute;n <br/> 2. Departamento<br/>  3. Sector <br/> 4. Variable <br/> 5. Especie ');
            document.getElementById("titulotransecto").style.display = "none";
            document.getElementById("paneltransecto").innerHTML = '';

            document.getElementById("tituloparcelas").style.display = "none";
            document.getElementById("panelparcelas").innerHTML = '';

            document.getElementById("tituloestaciones").style.display = "none";
            document.getElementById("panelestaciones").innerHTML = '';

            visualizarBotonEstadisticas('Generar reporte a nivel estaciones');

            nivelestadistico = 2;
            break;
        case 4:// nivel por Transecto

            if (document.getElementById("paneltransecto").innerHTML == '') {
                // alert('Debe seleccionar: \n 1.)Estaciones. ');  
                ocultarBotonEstadisticas();
            }

            visualizaTextoAyuda('<br/>Debe seleccionar: <br/> 1. Regi&oacute;n <br/> 2. Departamento<br/>  3. Sector <br/> 4. Proyecto <br/> 5. Estaciones <br/> 6. Transepto <br/> 7. Variable <br/> 8. Especie <br/> ');
            document.getElementById("tituloparcelas").style.display = "none";
            document.getElementById("panelparcelas").innerHTML = '';
            visualizarBotonEstadisticas('Generar reporte a nivel transecto');
            nivelestadistico = 4;
            break;
        case 1://nivel por parcela
            if (document.getElementById("panelparcelas").innerHTML == '') {
                //alert('Debe seleccionar: \n 1.)Estaciones.\n 2.)Transectos. \n 3.)Parcelas');  
                ocultarBotonEstadisticas();
            }
            /*if(modovista==0){
             visualizaTextoAyuda('<br/>Debe seleccionar: <br/> 1. Proyecto <br/> 2. Regi&oacute;n <br/> 3. Departamento<br/>  4. Sector <br/> 5. Estaciones <br/> 6. Transecto <br/> 7. Parcela <br/> 8. Variable <br/> 9. Especie <br/> ');  
             }else if(modovista==1){*/
            visualizaTextoAyuda('<br/>Debe seleccionar: <br/> 1. Regi&oacute;n <br/> 2. Departamento <br/>  3. Sector <br/> 4. Proyecto <br/> 5. Estaciones <br/> 6. Transepto <br/> 7. Parcela <br/> 8. Variable <br/> 9. Especie <br/> ');
            //}
            visualizarBotonEstadisticas('Generar reporte a nivel parcela');

            nivelestadistico = 1;
            break;
    }
}

function visualizarBotonEstadisticas(label) {
    document.getElementById("generarReporte").value = label;
    document.getElementById("generarReporte").style.display = "inline";

}
/**
 * Comment
 */
function ocultarBotonEstadisticas() {
    document.getElementById("generarReporte").style.display = "none";
}
/**
 * Comment
 */
function visualizaTextoAyuda(mensajeAyuda) {
    document.getElementById('panelayuda').innerHTML = mensajeAyuda;
}
/**
 * Comment
 */
function limpiarHTMLElementoDOM(elemento) {
    document.getElementById(elemento).innerHTML = '';
}
/**
 * 
 */
function visualizarVariablesActivas() {
    valorseleccionado = document.getElementById("especies").value;

    var urlAux = "";
    if (valorseleccionado == 't') {
        urlAux = "http://" + addreshost + "/siam/ServicioComponentes?componente=manglares&action=ObtieneVariablesTotales";
    } else {
        urlAux = "http://" + addreshost + "/siam/ServicioComponentes?componente=manglares&action=ObtieneVariables";
    }

    $.ajax({
        type: "POST",
        contentType: "application/text/html",
        url: urlAux,
        beforeSend: function(msg) {
            $('#panelvariables').html("<img border='0'   style='text-align:center;margin-right:32px; margin-top: 32px;' src='../manglares/img/Loader.gif' alt='Loading...'/>");

        },
        success: function(msg) {
            $('#panelvariables').html('');
            $("#panelvariables").html(msg);


        },
        error: function(xml, msg) {

            $("#panelvariables").html('<span class="error">Error cargando las variables</span>');

        }
    });
}
/**
 * 
 */
function exportarExcel() {
    var htmlexportarAExcel = document.getElementById("paneltabla").innerHTML;
    //alert(htmlexportarAExcel);
    document.location.href = "../libreriascomunes/exportaExcel.jsp?nombrearchivo=TablaDatos&mihtml=" + htmlexportarAExcel;
    /*$.ajax({
     type: "POST",
     contentType: "application/text/html",
     url:"http://"+addreshost+"/siam/ServicioComponentes?componente=manglares&action=ObtieneVariablesTotales",
     
     beforeSend: function(msg){                            
     $('#indicadordatosexportar').html("<img border='0'   style='text-align:center;margin-right:32px; margin-top: 32px;' src='../manglares/img/Loader.gif' alt='Loading...'/>");
     
     },
     success: function(msg){
     $('#indicadordatosexportar').html('');
     $("#indicadordatosexportar").html(msg);
     
     
     },
     error: function(xml,msg){
     
     $("#indicadordatosexportar").html('<span class="error">Error cargando las variables</span>');
     
     }
     });*/
}
/**
 * Comment
 */
function mostrarModuloEstructura() {
    document.location.href = "../manglares/estructura.jsp";
}
/**
 * 
 */
function mostrarMapaMangles() {
    document.location.href = "http://gis.invemar.org.co/sigma_geo/";
}


