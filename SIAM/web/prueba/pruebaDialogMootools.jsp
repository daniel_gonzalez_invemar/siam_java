<%-- 
    Document   : pruebaDialogMootools
    Created on : 10/10/2012, 09:46:39 AM
    Author     : usrsig15
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" href="../libreriascomunes/mootools/Assets/lightface.css" />
        <link rel="stylesheet" href="../libreriascomunes/mootools/Assets/LightFace.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/mootools/1.3.0/mootools.js"></script>
        <script src="../libreriascomunes/mootools/mootools-more-drag.js"></script>
        <script src="../libreriascomunes/mootools/LightFace.js"></script>
        <script src="../libreriascomunes/mootools/LightFace.IFrame.js"></script>
        <script src="../libreriascomunes/mootools/LightFace.Image.js"></script>
        <script src="../libreriascomunes/mootools/LightFace.Request.js"></script>
        <script src="../libreriascomunes/mootools/LightFace.Static.js"></script>

        <script type="text/javascript">
           var lf;
          function Dialog() {       
           
                lf = new LightFace({
                    title: $('demo1title').innerHTML,
                    content: $('demo1content').innerHTML,
                    width:300,
                    height:300,
                    draggable: true
                });
                lf.open();
	 	
            
            }
           
            function CerrarDialgo() {
             lf.close();
            }
        </script>
        <title>Prueba de Dialog Mootolos</title>
    </head>
    <body onload="Dialog()">
       	<div id="demo1title" style="display:none">
            <span>SIAM</span>
            <a onclick="CerrarDialgo()" style="float:right">Cerrrar</a>
        </div>	
        <p id="demo1content" style="display:none">dddddddddfdfdfsfgfdsfsdfsdafsdfds</p>	

    </body>
</html>
