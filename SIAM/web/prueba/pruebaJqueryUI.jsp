<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Prueba Jquery UI</title>
        <link type="text/css" href="css/redmond/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>


        <script>
            $(function() {
                // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
                $( "#dialog:ui-dialog" ).dialog( "destroy" );
	
                $( "#dialog-modal" ).dialog({
                    height: 140,
                    width:300,
                    modal: true
                });
            });
        </script>
    </head>
    <body>
        <div class="demo">

            <div id="dialog-modal" title="Basic modal dialog">
                <p>Adding the modal overlay screen makes the dialog look more prominent because it dims out the page content.</p>
            </div>
