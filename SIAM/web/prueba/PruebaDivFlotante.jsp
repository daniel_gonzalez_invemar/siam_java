<%-- 
    Document   : PruebaDivFlotante
    Created on : 10/10/2012, 02:33:24 PM
    Author     : usrsig15
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body style="background-color: black">
        <style type="text/css"  >
            #popup
            {
                opacity: 0.95;               
                filter: alpha(opacity=95);
                padding: 10px;
                background: #CCE6FF;
                position: absolute;
                top: 200px;
                left: 50%;
                width: 710px;
                height: 310px;
                border: 3px #7796AA solid;                
                margin-left: -350px;
                -moz-border-radius: 7px;
                border-radius: 7px;
                z-index: 2000;

            }

        </style>
        <div id="popup">
            <p>Soy un popup flotante. puedes <a href="javascript: cerrarPopup('popup');">cerrarme</a> si quieres</p>
        </div>
    </body>
</html>
