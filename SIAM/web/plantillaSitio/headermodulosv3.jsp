<%@ page import="co.org.invemar.siam.sitio.model.CComponentensSiamDAO"%>
<%@ page import="co.org.invemar.siam.sitio.vo.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%

    Calendar c = Calendar.getInstance();
    String dia = Integer.toString(c.get(Calendar.DATE));
    String mes = Integer.toString(c.get(Calendar.MONTH) + 1);
    String annio = Integer.toString(c.get(Calendar.YEAR));
    String fecha = dia + "/" + mes + "/" + annio;
    String misitio = request.getParameter("idsitio");


    String nombresitio = "";
    String nombrecorto = "";
    String urlConceptual = "";
    String urlsubportal = "";
    String nombrefilial = "";
    String urlPaginaAyuda = "";

    try {
        if (misitio != null) {
            CComponentensSiamDAO micomponentesiam = new CComponentensSiamDAO();
            ArrayList list = micomponentesiam.AdministradoresComponente(misitio);
            Iterator it = list.iterator();

            while (it.hasNext()) {
                CComponenteSiam cs = (CComponenteSiam) it.next();
                nombresitio = cs.getNombreCompleto();
                nombrecorto = cs.getNombreCorto();
                urlConceptual = cs.getUrlConceptual();
                urlsubportal = cs.getUrl();
                nombrefilial = cs.getNombreFilial();
                fecha = cs.getFechaultimaActualizacionComponente();
                urlPaginaAyuda = cs.getUrlAyuda();

            }
        }
    } catch (Exception e) {
    }
    String idsubsitio = request.getParameter("idsubsitio");



%>
<script>
    /*(function(w,d) {
        w.hj = w.hj || function() { (w.hj.q=w.hj.q||[]).push(arguments); };
        w._hjSettings = { hjid: 1524 };
        var s = d.createElement('script');
        s.src = '//insights.hotjar.com/static/client/insights.js';
        d.getElementsByTagName('head')[0].appendChild(s);
    }(window,document));*/
</script>

<div class="centrado" >
    <table width="996"  style="border:3px;width:963px;padding:0px"   >
        <tr   >
            <td  height="20px" colspan="3"  style="text-align:center;"  >
                <div class="centrado" style="position:relative;top:0px;height:20px;"> 
                     <div class="menuContainer">
                        <div style="float:left" class="textofechaactualizacion"><a href="http://twitter.com/#!/siam_colombia" target="_blank"><img  src="http://siam.invemar.org.co/siam/plantillaSitio/img/twitter.png" width="16" height="16" alt="twitter" style="border:0px" /></a><a href="http://twitter.com/statuses/user_timeline/124202805.rss" target="_blank"><img  border="0" src="http://siam.invemar.org.co/siam/plantillaSitio/img/rss.png" width="16" height="16" alt="rss" /></a></div>
                        <ul id="menu">
                            <li><a href="http://siam.invemar.org.co/siam/mapadelsitio.jsp">Mapa de Sitio | </a>
                            </li>
                            <li><a href="https://spreadsheets0.google.com/embeddedform?formkey=dHB1ZHdIVk5nbEs0VlRNaW9VdjU2X0E6MA" target="_blank"> D&iacute;ganos Que Piensa|</a></li>
                            <li><a href="#">Otros<img src="http://siam.invemar.org.co/siam/plantillaSitio/img/arrow.png" width="12" height="12" /></a>
                                <ul>
                                    <li><a href="http://www.invemar.org.co/pciudadania.jsp" target="_blank">Servicio al ciudadano</a></li>
                                    <li><a href="http://siam.invemar.org.co/siam/contactenos.jsp?idsitio=10" target="_blank">Cont&aacute;ctenos </a></li>
                                    <li><a href="#">Documento</a></li>
                                </ul>
                            </li>


                        </ul>
                        <div class="clear"></div>
                        <div style="float:left;color:white;font-weight: bold;font-size: 0.8em;">&Uacute;ltima Actualizacion:<%=fecha%> </div>
                    </div>
                    <div style="display:none;float:left;width: 432px">
                        <div  class="headerTexto">
                            Fecha ultima Actualizacion:<%=fecha%>
                            <a href="http://twitter.com/#!/siam_colombia" target="_blank"><img style="border:0px" src="http://siam.invemar.org.co/siam/plantillaSitio/img/twitter.png" width="16" height="16" alt="twitter" /></a> <a href="http://twitter.com/statuses/user_timeline/124202805.rss" target="_blank"><img  style="border:0px;"  src="http://siam.invemar.org.co/siam/plantillaSitio/img/rss.png" width="16" height="16" alt="rss" /></a>
                            <a href="http://siam.invemar.org.co/siam/contactenos.jsp?idsitio=<%=misitio%>" target="_blank" class='headerTexto' >&nbsp; Cont&aacute;ctenos</a>
                        </div>
                        <div style="">
                            <div style="float:left" ><a href="http://siam.invemar.org.co/siam/mapadelsitio.jsp" class="headerTexto" target="_blank"> Mapa de Sitio |&nbsp; </a><a target="_blank" class="headerTexto" href='http://www.invemar.org.co/pciudadania.jsp' >Servicio al Ciudadano | </a><a href="https://spreadsheets0.google.com/embeddedform?formkey=dHB1ZHdIVk5nbEs0VlRNaW9VdjU2X0E6MA" target="_blank" class="headerTexto">D&iacute;ganos que Piensa </a>  </div>
                        </div>

                    </div>
                    <div style="float:right;width:535x;">
                        <%if (urlPaginaAyuda != null) {%>
                        <div  id="btboton1" style="float:right"><a  href="<%=urlPaginaAyuda%>" title="Video tutorial" target="_blanck">Ayuda</a></div>
                        <%
                            }
                        %>
                        <div style="float:right;" id="btboton3" ><a href="http://www.invemar.org.co/psubcategorias.jsp?idsub=182&amp;idcat=104">labsis</a>
                        </div>
                        <div title="Desarrollo Conceptual" style="float:right;" id="btboton2" ><a href="<%=urlConceptual%>">Desarrollo <br/>Conceptual <%=nombrecorto%></a> 
                        </div>

                        <div  id="btboton1" style="float:right"><a  href="http://siam.invemar.org.co/siam/index.jsp">Inicio<br /></a></div>

                    </div>


                </div></td>
        </tr>
        <tr >
            <td height="25" colspan="4"><table width="989" style="border:0px;padding:0px;border-collapse:collapse" class="headermodulos">
                    <tr>
                        <td width="249"  style="text-align:center" height="178"><table width="240" style="border:0px;padding:0px;border-collapse:collapse;height:159">
                                <tr>
                                    <td width="14" height="83">&nbsp;</td>
                                    <td width="220" height="83">
                                        <img style="width:79px; height:90px"  src="http://siam.invemar.org.co/siam/plantillaSitio/img/icono_invemar.png"
                                             width="217" height="81" alt="icon invemar"
                                             longdesc="http://www.invemar.org.co"
                                             title="Pagina Principal de Invemar"
                                             style="border:0px"/>
                                    </td>
                                </tr><tr>
                                    <td width="14">&nbsp;</td>
                                    <td width="220">
                                        <p>
                                            &nbsp;
                                        </p>
                                        <p>
                                            &nbsp;
                                        </p>
                                    </td>
                                </tr>
                            </table></td>
                        <td colspan="2"  height="178">
                            <div class="titulosubportal nombrecortosubportal" style="width:400px;padding-left:5px;text-align: center;">
                                <%=nombrefilial%> 
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="249"  class="titulosubportal nombrecortosubportal" style="text-align:right"></td>
                        <td width="383">&nbsp;</td>
                        <td width="327" style='padding-bottom:0px;vertical-align:bottom'>
                            <%if (misitio != null && !misitio.equals("34") && !misitio.equals("38") && !misitio.equals("30")) {%>

                            <%                    }%></td>
                    </tr>
                </table></td>
        </tr>
        <tr style="border:2px solid #0000CC">

            <!--td width="162" style="text-align:righ;">
            <%if (misitio != null && !misitio.equals("34") && !misitio.equals("38") && !misitio.equals("30")) {%>
            <%       } else {
            %>
            <div  style="height:22px;width:150px"></div>
            <%                  }

            %></td-->
            <td   width="787"  style="padding-left: 10px;" >
                <span class="migadepan">Estas en: </span><a href="http://www.invemar.org.co/" class="migadepan">invemar.org.co &gt;  </a>
                <a href="http://siam.invemar.org.co/siam/index.jsp" class="migadepan">SIAM&nbsp; &gt;</a>
                <a href="<%=urlsubportal%>" class="migadepan"><%=nombrecorto%></a>
            </td>
            <td width="13" style="text-align:right" >&nbsp;</td>
        </tr>
    </table>


</div>