<%@ page import="co.org.invemar.siam.sitio.model.CComponentensSiamDAO"%>
<%@ page import="co.org.invemar.siam.sitio.vo.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%

    response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevent caching at the proxy server

    Calendar c = Calendar.getInstance();
    String dia = Integer.toString(c.get(Calendar.DATE));
    String mes = Integer.toString(c.get(Calendar.MONTH) + 1);
    String annio = Integer.toString(c.get(Calendar.YEAR));
    String fecha = dia + "/" + mes + "/" + annio;

    String misitio = request.getParameter("idsitio");
    misitio = "10";
    String nombresitio = "";
    String nombrecorto = "";
    String urlConceptual = "";
    String tituloNoticia = "";
    String urlnoticia = "";

    String tituloNoticia2 = "";
    String urlnoticia2 = "";
    String urlPaginaAyuda = "";
    try {
        if (misitio != null) {
            CComponentensSiamDAO micomponentesiam = new CComponentensSiamDAO();
            ArrayList list = micomponentesiam.AdministradoresComponente(misitio);
            Iterator it = list.iterator();

            while (it.hasNext()) {
                CComponenteSiam cs = (CComponenteSiam) it.next();
                nombresitio = cs.getNombreCompleto();
                nombrecorto = cs.getNombreCorto();
                urlConceptual = cs.getUrlConceptual();
                tituloNoticia = cs.getTitulonoticia1();
                System.out.println(tituloNoticia);
                urlnoticia = cs.getHtmlnoticia1();
                tituloNoticia2 = cs.getTitulonotica2();
                urlnoticia2 = cs.getHtmlnoticia2();
                fecha = cs.getFechaultimaActualizacionComponente();
                urlPaginaAyuda = cs.getUrlAyuda();
            }
        }

    } catch (Exception e) {
    }

    String idsubsitio = request.getParameter("idsubsitio");
    //contador siam


%>


<div class="centrado">
    <div  class="centrado" style="margin:auto;text-align:center;width:1000px;padding-top:2px;">
        <div class="menuContainer">
            <div style="float:left" class="textofechaactualizacion"><a  style='display:none' href="http://twitter.com/#!/siam_colombia" target="_blank"><img  src="plantillaSitio/img/twitter.png" width="16" height="16" alt="twitter" style="border:0px" /></a><a style='display:none' href="http://twitter.com/statuses/user_timeline/124202805.rss" target="_blank"><img  border="0" src="plantillaSitio/img/rss.png" width="16" height="16" alt="rss" /></a></div>
            <ul id="menu">
                <li><a href="http://siam.invemar.org.co/siam/mapadelsitio.jsp">Mapa de Sitio | </a>
                </li>
                <li><a href="https://spreadsheets0.google.com/embeddedform?formkey=dHB1ZHdIVk5nbEs0VlRNaW9VdjU2X0E6MA" target="_blank"> D&iacute;ganos Que Piensa|</a></li>
                <li><a href="#">Otros<img src="plantillaSitio/img/arrow.png" width="12" height="12" /></a>
                    <ul>
                        <li><a href="http://www.invemar.org.co/pciudadania.jsp" target="_blank">Servicio al ciudadano</a></li>
                        <li><a href="contactenos.jsp?idsitio=10" target="_blank">Cont&aacute;ctenos </a></li>
                        <li><a href="http://www.invemar.org.co/web/guest/acuerdo-de-acceso-uso-a-datos" target="_blank">Acuerdo de  acceso y uso a datos</a></li>
                    </ul>
                </li>


            </ul>
            <div class="clear"></div>
            <div style="float:left;color:white;font-weight: bold;font-size: 0.8em;">&Uacute;ltima Actualizacion:<%=fecha%> </div>
        </div>

        <div style="display:none;float:left;width:400px" >

            <div style="positon:relative;width:550px;">                
                <div style="float:left" class="textofechaactualizacion"><a href="http://twitter.com/#!/siam_colombia" target="_blank"><img  src="plantillaSitio/img/twitter.png" width="16" height="16" alt="twitter" style="border:0px" /></a><a href="http://twitter.com/statuses/user_timeline/124202805.rss" target="_blank"><img  border="0" src="plantillaSitio/img/rss.png" width="16" height="16" alt="rss" /></a></div>
                <div style="text-align:left;color:white;font-weight:bold" >
                    <a href="http://siam.invemar.org.co/siam/mapadelsitio.jsp" class="headerTexto" target="_blank">  Mapa de Sitio | </a><a target="_blank" class="headerTexto" href='http://www.invemar.org.co/pciudadania.jsp' >Servicio al Ciudadano</a><a href="https://spreadsheets0.google.com/embeddedform?formkey=dHB1ZHdIVk5nbEs0VlRNaW9VdjU2X0E6MA" target="_blank" class="headerTexto"> | D&iacute;ganos Que Piensa|<a href="http://localhost:8084/siam/contactenos.jsp?idsitio=10" target="_blank" class='headerTexto' >Cont&aacute;ctenos</a> </a> 
                </div>
                <div style="float:left;color:white;font-weight: bold;font-size: 0.8em;">&Uacute;ltima Actualizacion:<%=fecha%> </div>
            </div>  
        </div>
        <div style="float:right">
            <div  id="btboton1" style="float:right"><a  href="<%=urlPaginaAyuda%>" title="Video tutorial" target="_blanck">Ayuda</a></div>
            <div  id="btboton1" style="float:right"><a  href="http://siam.invemar.org.co/siam/index.jsp">Inicio<br /></a></div>
            <div title="Desarrollo Conceptual" style="float:right;" id="btboton2" ><a href="http://www.invemar.org.co/web/guest/descripcion-general-siam ">Desarrollo<br/>Conceptual <%=nombrecorto%></a></div>
            <div style="float:right;display:none" id="btboton3" ><a href="http://www.invemar.org.co/web/guest/descripcion-general-siam">labsis</a></div>
        </div>



    </div>
    <table width="1057"   style="border:0;padding:0;border-spacing:0;top:0px"  >
        <tr>
            <td width="19" class=''></td>
            <td width="1032">
                <table width="1008" style="border:0;text-align:center;padding:0;border-spacing:0" >    
                    <tr style='text-align:center' class='header' >
                        <td  ><table width="1000" style="border:0;padding:00">
                                <tr>
                                    <td width="18" rowspan="2"></td>
                                    <td width="73" rowspan="2" style="vertical-align:middle;position:relative;padding-top:10px"><a href='http://www.invemar.org.co'  target="_blank"><img style="border:0px;width:79px; height:90px"  src="plantillaSitio/img/icono_invemar.png" alt='icono invemar' title="Pagina Principal de Invemar"/></a></td>
                                    <td width="893">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align:left">&nbsp;</td>            
                                </tr>
                                <tr>
                                    <td colspan="3"><table width="200" style="border:0;text-align:center;cellpadding:00" >
                                            <tr>
                                                <td><div id="features">
                                                        <div><div id="bannerimagen01"></div> 
                                                            <div style="position:relative"> 
                                                                <div class="imgInfo">Moluscos -Libro de biodiversidad del mar Caribe Colombiano.| Gracia M; Ardila N. 2010.
                                                                </div> 
                                                            </div></div>
                                                        <div><div id="bannerimagen02"></div> 
                                                            <div style="position:relative"> 
                                                                <div class="imgInfo">Boca r&iacute;o Magdalena-Barranquilla Colombia. </div> 
                                                            </div></div>               
                                                        <div><div id="bannerimagen03"></div> 
                                                            <div style="position:relative"> 
                                                                <div class="imgInfo">Perfil sedimentologico Uraba 2006 | Julio P. </div> 
                                                            </div></div> 

                                                        <div><div id="bannerimagen04"></div> 
                                                            <div style="position:relative"> 
                                                                <div class="imgInfo">Fusii&oacute;n Aster R3G4B2 y TerraSar por m&eacute;todo de An&acute;lisis de Componentes Principales | Julio P. </div> 
                                                            </div></div> 

                                                        <div><div id="bannerimagen05"></div> 
                                                            <div style="position:relative"> 
                                                                <div class="imgInfo">Bah&iacute;a de Chengue.Santa Marta Colombia | Programa GEZ.</div> 
                                                            </div></div> 
                                                        <div><div id="bannerimagen06"></div> 
                                                            <div style="position:relative"> 
                                                                <div class="imgInfo">Cartograf&iacute;a de Santa Marta | Programa GEZ.</div> 
                                                            </div>
                                                        </div>               



                                                    </div>
                                                    <script type="text/javascript">

                                                        $(document).ready(function() {
                                                            $('#features').jshowoff({speed: 9000, links: true});
                                                        });

                                                    </script><noscript>Su navegador no soporta JavaScript!</noscript></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="303" ><table width="963px" class="tablaCenter"  style="border:0;padding:00;border-spacing:0" >
                                <tr>
                                    <td width="315" height="12" class=""  ></td>
                                    <td  >&nbsp;</td>
                                    <td colspan="3"  ><span  id='migadepan' class="migadepan"><a href="http://www.invemar.org.co/" class="migadepan">invemar.org.co &gt; </a> Siam &gt; Noticias</span></td>
                                </tr>
                                <tr>
                                    <td width="315" height="25" class=""  ></td>
                                    <td  >&nbsp;</td>
                                    <td width="30">&nbsp;</td>
                                    <td width="68"  >&nbsp;</td>
                                    <td width="529" class="titulosubmenu" id='titulosubmenu' >Noticias SIAM</td>
                                    <td width="0" ></td>
                                </tr>
                                <tr>
                                    <td width="315" height="121" class=""  ><table width="315"  style="border:0;padding:0px0;border-spacing:0;height:169">
                                            <tr>
                                                <td height="118"  ><div id="menuv">
                                                        <ul>
                                                            <li class="tituloMenuSecundario">Servicios</li>
                                                            <li><a id="bm" href="javascript:void(0)" onclick="submenu(1);" >Biodiversidad Marina</a></li>
                                                            <li><a href="javascript:void(0)" onclick="submenu(2);" >Manejo Integrado de Zonas Costeras</a></li>
                                                            <li><a href="javascript:void(0)" onclick="submenu(3);" >Uso de los Recursos Pesqueros</a></li>
                                                            <li><a href="javascript:void(0)" onclick="submenu(4);" >Monitoreo de los Ambientes Marinos</a></li>
                                                            <li><a href="javascript:void(0)" onclick="submenu(5);" >Amenazas Naturales</a></li>
                                                            <li><a href="javascript:void(0)" onclick="submenu(7);" >Servicio de Informaci&oacute;n Geogr&aacute;fica</a></li>
                                                            <li><a href="javascript:void(0)" onclick="submenu(6);" >Sistemas de Informaci&oacute;n Documental</a></li>
                                                            <li><a href="javascript:void(0)" onclick="submenu(8);" >Sitios Tem&aacute;ticos</a></li>                                                          
                                                            <li><a href="javascript:void(0)"  onclick="submenu(9);" >Boletines y Redes</a></li>
                                                            <li><a id="footermenu" href="javascript:void(0)"  onclick="submenu(10);" >Enlaces</a></li>
                                                        </ul>
                                                    </div></td>
                                            </tr>
                                        </table></td>
                                    <td width="7" height="245" class=''  >&nbsp;</td>
                                    <td colspan="3"  >
                                        <div id="itemsmenu" style="position:absoluto;top:0px;border:0px solid #ccffaa">
                                            <!--<table width="572" style="border:0px;position:absuluto;top:0px;text-align:center;padding:0;border-spacing:2">
                                                <tr>
                                                    <td colspan="2" style='text-align:left' class="textonoticias">&nbsp;</td>
                                                    <td class="itemtituloitemsubmenu">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align:left'>&nbsp;</td>
                                                    <td class="itemtituloitemsubmenu">&nbsp;</td>
                                                    <td class="itemtituloitemsubmenu">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="25" style="text-align:center">&nbsp;</td>
                                                    <td width="268" class="textonoticias"><%=tituloNoticia%></td>
                                                    <td width="20" rowspan="3">&nbsp;</td>
                                                    <td width="249">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">&nbsp;</td>
                                                    <td style="text-align:right"><a href="<%=urlnoticia%>" target="_blank" ><img style="border:0" alt='boton de noticias' src="plantillaSitio/img/boton-noticias.png"/></a></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="25" style="text-align:center">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td width="249">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4"><hr  class="lineaseparacion" /></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td rowspan="3" style="text-align:center">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">&nbsp;</td>
                                                    <td class="textonoticias" ><%=tituloNoticia2%></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">&nbsp;</td>
                                                    <td style="text-align:right"><a href="<%=urlnoticia2%>" target="_blank"><img style="border:0" alt='boton de noticias' src="plantillaSitio/img/boton-noticias.png"/></a></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                            </table>-->
                                           
                                            
                                           <!-- <iframe frameborder="0" height="450px" src="http://www.invemar.org.co/widget/web/guest/noticias-siam/-/101_INSTANCE_ZFgNqynD3DWD" width="100%"></iframe>-->
                                            
                                        </div>

                                    </td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="21" >&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="18" id='panel3'><table width="971" style="border:0;padding:00">
                                <tr>
                                    <td><table width="947" style="border:0;padding:00">
                                             <tr>
                                                <td width="237" style="text-align:center"><a href="http://www.siac.gov.co/portal/default.aspx" target="_blank"><img  style="text-align:middle;border:0" src="plantillaSitio/img/iconsiac.png" width="119" height="56" alt="iconsiac"/></a></td>
                                                <td width="200" style="text-align:center"><a href="http://siatac.siac.net.co/web/guest/inicio" target="_blank"><img style="border:0" src="plantillaSitio/img/iconsiactac.png" width="119" height="56" alt="siac at" /></a></td>
                                                <td width="250" style="text-align:center"><a href="http://www.siac.net.co/web/sib/home" target="_blank"><img  style="border:0" src="plantillaSitio/img/iconsib.png" width="119" height="56" alt="sibm" /></a></td>
                                                <td width="250" style="text-align:center"><a href="http://www.gobiernoenlinea.gov.co/web/guest;jsessionid=B1A84E14EFA658C24E3DCB260E045CCA" target="_blank"><img style="border:0" src="plantillaSitio/img/icongel.png" width="119" height="56" alt="gobierno en linea" /></a></td>
                                            </tr>
                                            <tr>
                                                 <iframe   frameborder="0" height="500px" src="http://www.invemar.org.co/widget/web/guest/noticias-siam/-/101_INSTANCE_ZFgNqynD3DWD" width="100%"></iframe>
                                            </tr>
                                           
                                        </table></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="179"   class="footersitio">
                            <table width="996" style="border:0;text-align:center;padding:0;border-spacing:0">
                                <tr>
                                    <td style="text-align:left">&nbsp;</td>
                                    <td class="textfooter"></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left"> <p> <!--<a href="http://validator.w3.org/check?uri=referer"></a>-->
                                        </p></td>
                                    <td class="textfooter">sede principal:Playa Salguero - Rodadero -Santa Marta, Colombia- | a.a. 1016 |   Tel&eacute;fonos </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="textfooter"> (+57)(+5)4328600 - Fax: (+57) (+5) 4328682 </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="textfooter">  Horario de Atenci&oacute;n (Sta Marta): De 7:00 a 11:15 am y de 1:00 a 4:00 pm de </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="textfooter"> Lunes a viernes </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="textfooter"> Copyright &copy; 2012 Todos los derechos reservados webmaster@invemar.org.co </td>
                                </tr>
                                <tr>
                                    <td width="949" colspan="2" class="textfooter"> directiva de privacidad |<a href="http://siam.invemar.org.co/siam/legal.jsp" target="_blank" class="linkfooter"> Compromiso de Uso</a>| </td>
                                </tr>
                            </table></td>     
                    </tr>
                </table></td>
            <td width="10" rowspan="2"  class="fondoheader">&nbsp;</td>
        </tr>
    </table>
    <p>&nbsp;</p>
</div>
<script type="text/javascript">
   

         
                   submenu(1);
          
          
</script> 