<%@ page import="co.org.invemar.siam.sitio.model.CComponentensSiamDAO"%>
<%@ page import="co.org.invemar.siam.sitio.vo.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>

<%
    Calendar c = Calendar.getInstance();
    String dia = Integer.toString(c.get(Calendar.DATE));
    String mes = Integer.toString(c.get(Calendar.MONTH) + 1);
    String annio = Integer.toString(c.get(Calendar.YEAR));
    String fecha = dia + "/" + mes + "/" + annio;

    String misitio = request.getParameter("idsitio");
    misitio = "10";
    String nombresitio = null;
    String nombrecorto = null;
    String urlConceptual = null;
    String tituloNoticia = null;
    String urlnoticia = null;

    String tituloNoticia2 = null;
    String urlnoticia2 = null;

    if (misitio != null) {
        CComponentensSiamDAO micomponentesiam = new CComponentensSiamDAO();
        ArrayList list = micomponentesiam.AdministradoresComponente(misitio);
        Iterator it = list.iterator();

        while (it.hasNext()) {
            CComponenteSiam cs = (CComponenteSiam) it.next();
            nombresitio = cs.getNombreCompleto();
            nombrecorto = cs.getNombreCorto();
            urlConceptual = cs.getUrlConceptual();
            tituloNoticia = cs.getTitulonoticia1();
            urlnoticia = cs.getHtmlnoticia1();
            tituloNoticia2 = cs.getTitulonotica2();
            urlnoticia2 = cs.getHtmlnoticia2();

        }
    }


%>
<%

    String idsubsitio = request.getParameter("idsubsitio");


%>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-19778914-17']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script><noscript>Su navegador no soporta JavaScript!</noscript>
<div class="centrado" >
    <table width="1056"   style="border:0;text-align:center;padding:0;border-spacing:0"  > <!--top-->
        <tr>
            <td width="19" class=''></td>
            <td width="1032"><table width="1008"  style="border:0;text-align:center;padding:0;border-spacing:0"  >
                    <tr>
                        <td width="1008" height="56"><table width='991' style="border:0px;text-align:left;padding:0;border-spacing:0">
                                <tr>
                                    <!--         <td height="23" colspan="2" rowspan="2" style="text-align:left'> <a href="http://twitter.com/#!/siam_colombia" target="_blank"></a>
                                             </td> -->
                                    <td width="150" class="textofechaactualizacion">�ltima Actualizacion:<%=fecha%></td>
                                    <td width="464"><a href="http://twitter.com/#!/siam_colombia" target="_blank"><img style="border:0" src="plantillaSitio/img/twitter.png" width="16" height="16" alt="twitter" /></a><a href="http://twitter.com/statuses/user_timeline/124202805.rss" target="_blank"><img  style="border:0" src="plantillaSitio/img/rss.png" width="16" height="16" alt="rss" /></a><a href="http://siam.invemar.org.co/siam/mapadelsitio.jsp" class="headerTexto" target="_blank">  Mapa de Sitio | </a><a target="_blank" class="headerTexto" href='http://www.invemar.org.co/pciudadania.jsp' >Servicio al Ciudadano</a><a href="https://spreadsheets0.google.com/embeddedform?formkey=dHB1ZHdIVk5nbEs0VlRNaW9VdjU2X0E6MA" target="_blank" class="headerTexto"> | D�ganos Que Piensa| </a> <a href="http://siam.invemar.org.co/siam/contactenos.jsp?idsitio=<%=misitio%>" target="_blank" class='headerTexto' >Cont&aacute;ctenos</a></td>
                                    <td  width="117" rowspan="2"   class="out" id="boton1" onfocus="this.className='over'" onblur="this.className='out'"><a href="http://siam.invemar.org.co/siam/index.jsp"  class='opcionmenuprincipal'>Inicio</a><br />
                                    </td>

                                    <td  width="122" rowspan="2"  class="out" id="boton3" onfocus="this.className='over'" onblur="this.className='out'" ><a href="<%=urlConceptual%>" class='opcionmenuprincipal' target="_blank">Desarrollo<br />
                                            Conceptual</a></td>
                                    <td  width="119" rowspan="2"  class='out' id="boton4" onfocus="this.className='over'" onblur="this.className='out'" ><a href="http://www.invemar.org.co/psubcategorias.jsp?idsub=182&amp;idcat=104" class='opcionmenuprincipal' target="_blank">LABSIS</a></td>
                                </tr>
                                <tr>
                                    <td><!--<div id="google_translate_element" style="text-align:left;font-size:0.5em;width:105px;height:18px"></div>--> 


                                    </td>
                                    <td style="text-align:right" >                                       


                                    </td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr style='text-align:center' class='header' >
                        <td  ><table width="1000" style="border:0;padding:00">
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><table width="967" style="border:0;padding:00">
                                            <tr>
                                                <td width="18">&nbsp;</td>
                                                <td width="943" style='text-align:left'><a href='http://www.invemar.org.co'  target="_blank"><img style="border:0;width:79px; height:90px" src="plantillaSitio/img/icono_invemar.png" alt='icono invemar' title="Pagina Principal de Invemar"/></a></td>
                                            </tr>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td><table width="200" style="border:0;text-align:center;cellpadding:00" >
                                            <tr>
                                                <td><div id="features">
                                                        <div><div id="bannerimagen01"></div> 
                                                            <div style="position:relative"> 
                                                                <div class="imgInfo">Moluscos -Libro de Biodiversidad del mar Caribe Colombiano.| Gracia M; Ardila N. 2010.
                                                                </div> 
                                                            </div></div>
                                                        <div><div id="bannerimagen02"></div> 
                                                            <div style="position:relative"> 
                                                                <div class="imgInfo">Boca rio Magdalena-Barranquilla Colombia. </div> 
                                                            </div></div>               
                                                        <div><div id="bannerimagen03"></div> 
                                                            <div style="position:relative"> 
                                                                <div class="imgInfo">Perfil sedimentologico Uraba 2006 | Julio P </div> 
                                                            </div></div> 

                                                        <div><div id="bannerimagen04"></div> 
                                                            <div style="position:relative"> 
                                                                <div class="imgInfo">Fusi�n Aster R3G4B2 y TerraSar por m�todo de An�lisis de Componentes Principales | Julio P </div> 
                                                            </div></div> 

                                                        <div><div id="bannerimagen05"></div> 
                                                            <div style="position:relative"> 
                                                                <div class="imgInfo">Bahia de Chengue.Santa Marta Colombia | Programa GEZ</div> 
                                                            </div></div> 
                                                        <div><div id="bannerimagen06"></div> 
                                                            <div style="position:relative"> 
                                                                <div class="imgInfo">Cartografia de Santa Marta | Programa GEZ</div> 
                                                            </div>
                                                        </div>               



                                                    </div>
                                                    <script type="text/javascript">		

                                                        $(document).ready(function(){ $('#features').jshowoff({ speed:9000, links: true }); });

                                                    </script><noscript>Su navegador no soporta JavaScript!</noscript></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td ><table width="963px"  style="border:0;padding:00;border-spacing:0" class="tablaCenter" >
                                <tr>
                                    <td width="315" height="106" rowspan="4" class=""  ><table width="315" style="border:0;padding:00;border-spacing:0">
                                            <tr>
                                                <td  class="tituloMenuSecundario"> Servicios </td>
                                            </tr>
                                            <tr>
                                                <td   class="opcionMenu" onfocus="resaltarOpcion(this,1,'opcionMenuresalte')" onblur="restablecerOpcion(this,1,'opcionMenu')" onclick="submenu(1);"><a id="opcion1" href='javascript:void(0)' onclick="submenu(1);" class="opcionlinkmenusecundario"> Biodiversidad Marina</a></td>
                                            </tr>
                                            <tr >
                                                <td    class="opcionMenu" onfocus="resaltarOpcion(this,2,'opcionMenuresalte')" onblur="restablecerOpcion(this,2,'opcionMenu')" onclick="submenu(2);"><a  id='opcion2' href='javascript:void(0)' onclick="submenu(2);" class="opcionlinkmenusecundario">Manejo Integrado de Zonas Costeras</a></td>
                                            </tr>
                                            <tr >
                                                <td   class="opcionMenu" onfocus="resaltarOpcion(this,3,'opcionMenuresalte')" onblur="restablecerOpcion(this,3,'opcionMenu')" onclick="submenu(3);"><a  id='opcion3' href='javascript:void(0)' onclick="submenu(3);" class="opcionlinkmenusecundario">Uso de los Recursos Pesqueros</a></td>
                                            </tr>
                                            <tr >
                                                <td  class="opcionMenu" onfocus="resaltarOpcion(this,4,'opcionMenuresalte')" onblur="restablecerOpcion(this,4,'opcionMenu')" onclick="submenu(4);"><a id='opcion4' href='javascript:void(0)' onclick="submenu(4)" class="opcionlinkmenusecundario">Monitoreo de los Ambientes Marinos</a></td>
                                            </tr>
                                            <tr >
                                                <td  class="opcionMenu" onfocus="resaltarOpcion(this,5,'opcionMenuresalte')" onblur="restablecerOpcion(this,5,'opcionMenu')" onclick="submenu(5);"><a  id='opcion5' href='javascript:void(0)' onclick="submenu(5)" class="opcionlinkmenusecundario">Amenazas Naturales</a></td>
                                            </tr>
                                            <tr >
                                                <td  class="opcionMenu" onfocus="resaltarOpcion(this,6,'opcionMenuresalte')" onblur="restablecerOpcion(this,6,'opcionMenu')" onclick="submenu(7);"><a id='opcion6' href='javascript:void(0)' onclick="submenu(7)" class="opcionlinkmenusecundario">Servicio de Informaci&oacute;n Geogr&aacute;fica</a></td>
                                            </tr>
                                            <tr >
                                                <td  class="opcionMenu" onfocus="resaltarOpcion(this,7,'opcionMenuresalte')" onblur="restablecerOpcion(this,7,'opcionMenu')" onclick="submenu(6);"><a id='opcion7' href='javascript:void(0)' onclick="submenu(6)" class="opcionlinkmenusecundario">Sistemas de Informaci&oacute;n Documental</a></td>
                                            </tr>
                                            <tr >
                                                <td  class="opcionMenu" onfocus="resaltarOpcion(this,8,'opcionMenuresalte')" onblur="restablecerOpcion(this,8,'opcionMenu')" onclick="submenu(8);"><a id='opcion8' href='javascript:void(0)' onclick="submenu(8)" class="opcionlinkmenusecundario">Portales Tem&aacute;ticos</a></td>
                                            </tr>
                                            <tr >
                                                <td  class="footermenu" onfocus="resaltarOpcion(this,9,'overfootermenu')" onblur="restablecerOpcion(this,9,'footermenu')" onclick="submenu(9);"><a id='opcion9' href='javascript:void(0)' onclick="submenu(9)" class="opcionlinkmenusecundario">Boletines y Redes</a></td>
                                            </tr>                 
                                        </table></td>
                                    <td  >&nbsp;</td>
                                    <td colspan="3"  >&nbsp;</td>
                                </tr>
                                <tr>
                                    <td  >&nbsp;</td>
                                    <td colspan="3"  ><span  id='migadepan' class="migadepan"><a href="http://www.invemar.org.co/" class="migadepan">invemar.org.co &gt; </a> Siam &gt; Noticias</span></td>
                                </tr>
                                <tr>
                                    <td  >&nbsp;</td>
                                    <td width="30">&nbsp;</td>
                                    <td width="68"  >&nbsp;</td>
                                    <td width="529" class="titulosubmenu" id='titulosubmenu' >Noticias SIAM</td>
                                    <td width="0" ></td>
                                </tr>
                                <tr>
                                    <td width="7" height="330" class=''  >&nbsp;</td>
                                    <td colspan="3" class=''  id='itemsmenu'><table width="572"  style="border:0;text-align:center;padding:0;border-spacing:2" class=''>
                                            <tr>
                                                <td colspan="2" style='text-align:left' class="textonoticias">&nbsp;</td>
                                                <td class="itemtituloitemsubmenu">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style='text-align:left'>&nbsp;</td>
                                                <td class="itemtituloitemsubmenu">&nbsp;</td>
                                                <td class="itemtituloitemsubmenu">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="25" style="text-align:center">&nbsp;</td>
                                                <td width="268" class="textonoticias"><%=tituloNoticia%></td>
                                                <td width="20" rowspan="3">&nbsp;</td>
                                                <td width="249">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center">&nbsp;</td>
                                                <td style="text-align:right"><a href="<%=urlnoticia%>" target="_blank" ><img style="border:0" alt='boton de noticias' src="plantillaSitio/img/boton-noticias.png"/></a></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td width="25" style="text-align:center">&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td width="249">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><hr  class="lineaseparacion" /></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center">&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td rowspan="3" style="text-align:center">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center">&nbsp;</td>
                                                <td class="textonoticias" ><%=tituloNoticia2%></td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center">&nbsp;</td>
                                                <td style="text-align:right"><a href="<%=urlnoticia2%>" target="_blank"><img style="border:0" alt='boton de noticias' src="plantillaSitio/img/boton-noticias.png"/></a></td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td height="24"   >&nbsp;</td>
                                    <td colspan="4"   >&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="22"  >&nbsp;</td>
                                    <td colspan="4"    >&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="22"  >&nbsp;</td>
                                    <td colspan="4"    >&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="22"  >&nbsp; </td>
                                    <td colspan="4"    >&nbsp;</td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="18" id='panel3'><table width="971" style="border:0;padding:00">
                                <tr>
                                    <td><table width="947" style="border:0;padding:00">
                                            <tr>
                                                <td width="237" style="text-align:center"><a href="http://www.siac.gov.co/portal/default.aspx" target="_blank"><img  style="text-align:middle;border:0" src="plantillaSitio/img/iconsiac.png" width="119" height="56" alt="iconsiac"/></a></td>
                                                <td width="200" style="text-align:center"><a href="http://siatac.siac.net.co/web/guest/inicio" target="_blank"><img style="border:0" src="plantillaSitio/img/iconsiactac.png" width="119" height="56" alt="siac at" /></a></td>
                                                <td width="250" style="text-align:center"><a href="http://www.siac.net.co/web/sib/home" target="_blank"><img  style="border:0" src="plantillaSitio/img/iconsib.png" width="119" height="56" alt="sibm" /></a></td>
                                                <td width="250" style="text-align:center"><a href="http://www.gobiernoenlinea.gov.co/web/guest;jsessionid=B1A84E14EFA658C24E3DCB260E045CCA" target="_blank"><img style="border:0" src="plantillaSitio/img/icongel.png" width="119" height="56" alt="gobierno en linea" /></a></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center">&nbsp;</td>
                                                <td colspan="3" style="text-align:center">&nbsp;</td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="202"   class="footersitio">
                            <table width="996" style="border:0;text-align:center;padding:0;border-spacing:0">
                                <tr>
                                    <td style="text-align:left">&nbsp;</td>
                                    <td class="textfooter"><a href="http://validator.w3.org/check?uri=referer"><img style="border:0"
                                                                                                                    src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a><a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border:0;width:88px;height:31px"
                                                                                                                                                                                                        src="http://jigsaw.w3.org/css-validator/images/vcss"
                                                                                                                                                                                                        alt="&iexcl;CSS V&aacute;lido!" /></a></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left"> <p> <!--<a href="http://validator.w3.org/check?uri=referer"></a>-->
                                        </p></td>
                                    <td class="textfooter">sede principal:cerro punta bet&iacute;n-santa marta, colombia- | a.a. 1016 |   Tel&eacute;fonos </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="textfooter"> (+57)(+5)4328600 - Fax: (+57) (+5) 4328682 </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="textfooter">  Horario de Atenci&oacute;n (Sta Marta): De 7:00 a 11:30 am y de 1:00 a 5:00 pm de </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="textfooter"> Lunes a Viernes </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="textfooter"> Copyright &copy; 2012 Todos los derechos reservados webmaster@invemar.org.co </td>
                                </tr>
                                <tr>
                                    <td width="949" colspan="2" class="textfooter"> directiva de privacidad |<a href="http://siam.invemar.org.co/siam/legal.jsp" target="_blank" class="linkfooter"> Compromiso de Uso</a>| </td>
                                </tr>
                            </table></td>     
                    </tr>
                </table></td>
            <td width="10" rowspan="2"  class="fondoheader">&nbsp;</td>
        </tr>
    </table>
</div>
