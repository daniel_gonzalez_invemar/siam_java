// JavaScript Document

var cambieCaptcha = false;

function inicializarTexto(){

	
	tinyMCE.init({
		mode : "textareas",
		theme : "simple",
		elements : "queja"
	});


}

function cambiarCaptcha(){
	
	document.complaintForm.action="";
	cambieCaptcha = true;

}


function validarEnvioQueja(){

	if(cambieCaptcha)
		return true;

	var queja = tinyMCE.get('queja').getContent();

	if(isNullOrEmpty(document.complaintForm.nombre.value)){
		alert(checkAccents("Debe ingresar su nombre en el campo correspondiente"));
		document.complaintForm.nombre.focus();
		return false;					
	}
	
	if(!validarCorreoe(document.complaintForm.email.value)){
		alert(checkAccents("Debe ingresar un correo electr&oacute;nico v&aacute;lido en el campo correspondiente"));
		document.complaintForm.email.focus();
		return false;					
	}	

	if(isNullOrEmpty(queja)){
		alert(checkAccents("Debe ingresar la inquietud a enviar"));
		return false;	
	}
	
	if(queja.length >= 3990){
		alert(checkAccents("La inquietud a enviar no debe sobrepasar los 4000 caracteres"));
		return false;			
	}
	
	if(isNullOrEmpty(document.complaintForm.inCaptchaChars.value)){
		alert(checkAccents("Debe ingresar las letras que aparecen en la imagen"));
		document.complaintForm.inCaptchaChars.focus();
		return false;	
	}	
		
	return true;
	
}

inicializarTexto();