// JavaScript Document

function inicializarTexto(){
	
	tinyMCE.init({
		mode : "textareas",
		theme : "simple",
		elements : "respuesta"
	});

}

function validarEnvioRespuesta(){

	var resp = tinyMCE.get('respuesta').getContent();
	
	if(isNullOrEmpty(resp)){
		alert(checkAccents("Debe ingresar el mensaje a enviar"));
		return false;	
	}
	
	if(resp.length >= 3990){
		alert(checkAccents("El mensaje a enviar no debe sobrepasar los 4000 caracteres"));
		return false;			
	}
	
	return true;
	
}

inicializarTexto();