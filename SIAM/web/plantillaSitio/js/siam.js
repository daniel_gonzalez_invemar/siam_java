// JavaScript Document
var panel3renderizado=false;



// Create the analytics service object
function  getVisitas(){
google.load('gdata', '2.x', {packages: ['analytics']});


var visitas;
var analyticsService =new google.gdata.analytics.AnalyticsService('iSample_dataVisits_v1.0');
   

// The feed URI that is used for retrieving the analytics data
var feedUri = 'https://www.google.com/analytics/feeds/data' +
    '?start-date=2009-01-01' +
    '&end-date=2040-01-31' +
    '&metrics=ga:visits' +
    '&max-results=50' +
    '&ids=ga:50240935';

// callback method to be invoked when getDataFeed() returns data
var callback = function(result) {

  // An array of analytics feed entries
  var entries = result.feed.entry;

  // create an HTML Table using an array of elements
  var outputTable = ['<table>'];
  outputTable.push('<tr><th>Page Title</th><th>Page Path</th><th>Pageviews</th></tr>');

  // Iterate through the feed entries and add the data as table rows
  for (var i = 0; i < entries.length; i++) {
    var entry = entries[i];

    // add a row in the HTML Table array
    var row = [
      entry.getValueOf('ga:visits')
    ].join('</td><td>');
    outputTable.push('<tr><td>', row, '</td></tr>'); 
  }
  outputTable.push('</table>'); 
  visitas= entry.getValueOf('ga:visits');
  // print the generated table 
  PRINT(outputTable.join(''));
}

// Error handler
var handleError = function(error) {
  PRINT(error);
 
}

// Submit the request using the analytics service object
analyticsService.getDataFeed(feedUri, callback, handleError);
return visitas;
}
function resaltarOpcion(objeto, idmenu, nombreclase ){
	
	objeto.className=nombreclase;
    $("#opcion"+idmenu).removeClass('opcionlinkmenusecundario');
	$("#opcion"+idmenu).addClass('opcionlinkmenusecundarioresaltado');
	//console.log(objeto.className);
  //alert(objeto);
}
function restablecerOpcion(objeto, idmenu,nombreclase){   
    objeto.className=nombreclase;
    $("#opcion"+idmenu).removeClass('opcionlinkmenusecundarioresaltado');
	$("#opcion"+idmenu).addClass('opcionlinkmenusecundario');
	//console.log(objeto.className);
	//alert(objeto);
}


function submenu(submenu){
	var opcion = 'plantillaSitio/menu'+submenu+'.html';	
	$("#itemsmenu").load(opcion);
	 
	 switch(submenu){
		 case 1:
		  $("#migadepan").html("<a  target='_blank' class='migadepan' href='http://www.invemar.org.co/'>invemar.org.co</a>  > siam > Servicios > Biodiversidad Marina");
		  $("#titulosubmenu").html('Biodiversidad Marina');
		 break;
		 case 2:
		   $("#migadepan").html("<a  target='_blank' class='migadepan' href='http://www.invemar.org.co/'>invemar.org.co</a> > siam > Servicios > Manejo Integrado de Zonas costeras");
		   $("#titulosubmenu").html('Manejo Integrado de Zonas Costeras');
		 break;
		 case 3:
		  $("#migadepan").html("<a  target='_blank' class='migadepan' href='http://www.invemar.org.co/'>invemar.org.co</a>  > siam > Servicios > Uso de los Recursos Pesqueros");
		  $("#titulosubmenu").html('Uso de los Recursos Pesqueros');
		 break;
		 case 4:
		  $("#migadepan").html("<a  target='_blank'  class='migadepan' href='http://www.invemar.org.co/'>invemar.org.co</a> > siam > Servicios > Monitoreo de los Ambientes Marinos");
		  $("#titulosubmenu").html('Monitoreo de los Ambientes Marinos');
		 break;
		 case 5:
		  $("#migadepan").html("<a   target='_blank' class='migadepan' href='http://www.invemar.org.co/'>invemar.org.co</a> > siam > Servicios > Amenazas Naturales");
		  $("#titulosubmenu").html('Amenazas Naturales');
		 break;
		 case 6:
		  $("#migadepan").html("<a   target='_blank' class='migadepan' href='http://www.invemar.org.co/'>invemar.org.co</a> > siam > Servicios > Sistema de Informacion Documental");
		  $("#titulosubmenu").html('Sistema de Información Documental');
		 break;
		 case 7:
		  $("#migadepan").html("<a   target='_blank' class='migadepan' href='http://www.invemar.org.co/'>invemar.org.co</a> > siam > Servicios > Servicio de Informacion Geografica");
		  $("#titulosubmenu").html('Servicio de información Geográfica');
		 break;
		 case 8:
		  $("#migadepan").html("<a   target='_blank' class='migadepan' href='http://www.invemar.org.co/'>invemar.org.co</a> > siam > Servicios > Portales Temáticos");
		  $("#titulosubmenu").html('Portales Temáticos');
		 break;
		  case 9:
		  $("#migadepan").html("<a   target='_blank' class='migadepan' href='http://www.invemar.org.co/'>invemar.org.co</a> > siam > Servicios > Boletines y Redes");
		  $("#titulosubmenu").html('Boletines y Redes');
		 break;
		 
	 }
	 
	/*if (panel3renderizado==false){
		 $("#panel3").load('plantillaSitio/panel3.jsp');
		 panel3renderizado=true;
	}*/
	
}
