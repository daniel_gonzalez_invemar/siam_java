<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
    <head>
        <title>Geoportal SIAM
        </title>
        <meta name="Title" content="Geoportal SIAM"/>
        <meta name="Author" content="webmster siam"/>
        <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
        <meta name="Subject" content="Geovisor del siam"/>
        <meta name="Description" content="Geovisor del sistema de informacion marino de colombia"/>
        <meta name="Keywords" content="lenguaje html, manual, tutorial, curso, dise�o web"/>
        <meta name="Generator" content="Notepad++"/>
        <meta name="Language" content="Spanish"/>
        <meta name="Revisit" content="1 day"/>
        <meta name="Distribution" content="Global"/>
        <meta name="Robots" content="All"/>
        <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css"/>
        <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
    </head>
    <body>
        <jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=90"/>
        <div class="centrado">
            <table width="1016" style="width:850px">      
                <tr>             
                    <td width="1004" style="padding-left:4px;vertical-align:top; padding-right:4px;">&nbsp;</td>
                </tr>           
                <tr>
                    <td style="width:5px;height:5px;"></td>
                </tr>

                <tr>

                    <td  style=" padding-left:4px;padding-right:2px;"><table width="95%" style="width:100%;">
                            <tr>        

                                <td><table style="width:100%;" class="colorfondo2">

                                        <tr>

                                            <!-- <td class="titulo_secciones">
                                                   <h1>Geoportal SIAM</h1>					  </td> -->

                                            <td height="47" colspan="2" style="text-align:center;font-size:1.2em;vertical-align:middle;background-image:url(../plantillaSitio/images/fondo_geoportal_menu.png); background-repeat:no-repeat">
                                                <div style="text-align:right" class="style15">                    
                                                    <div style="text-align:left;padding:5px"><a href="#"><span style="color:#036; font-size:1.52em"> Geo </span><span style="color:#FFF; font-size:1.4em"> Portal SIAM </span></a></div>
                                                </div></td>

                                        </tr>

                                        <tr>

                                            <td class="titulo_sec_smb" height="9"></td>
                                        </tr>
                                        <tr>
                                            <td  style="height:10px;background-color:#F8FAFC" >
                                                <div class="centrado">
                                                    <table width="963px"  style="border:0px"   >
                                                        <tr>
                                                            <td  style="background-color" class="titulosnegros"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>          </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="background-color:#E1EBF4"><p><span class="texttitulo" style="color:#006699; font-size:1.2em"><strong>Bienvenidos</strong></span></p>
                                                                <p class="texttablas">El Geoportal del Sistema de Informaci&oacute;n Ambiental Marino de Colombia SIAM es una herramienta que permite el acceso a la informaci&oacute;n disponible para la toma de decisiones en el manejo ambiental de zonas marino costeras en Colombia. </p>
                                                                <table style="width:100%;border:0px;padding:0xp;border-collapse:collapse" >
                                                                    <tr></tr>
                                                                    <tr>
                                                                        <td >&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                                                                            <div class="centrado">
                                                                                <table  style="width:869;border:0px;padding:0px;border-collapse:collapse">
                                                                                    <tr>
                                                                                        <td width="202" style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><strong>GEO Visor</strong>                                    
                                                                                        </td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                        <td width="33">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2"><img src="../plantillaSitio/images/geoportal_img_01.jpg" alt="geo visor" width="177" height="100" /></td>
                                                                                        <td colspan="2"><span class="texttablas">Herramienta de visualizaci&oacute;n de informaci&oacute;n geogr&aacute;fica ambiental de acceso libre</span>
                                                                                        </td>
                                                                                        <td rowspan="2">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr style="display:none">
                                                                                        <td colspan="2"><a href="http://geoportal.invemar.org.co/?q=node/4" target="_blank"><img src="../plantillaSitio/images/geopota_boton_ingresar.pg.jpg" alt='boton ingreso' width="67" height="15" style="position:relative;left:300px"/></a></td>
                                                                                    </tr>
                                                                                    <tr style="display:none" >
                                                                                        <td colspan="2"><a href="http://geoportal.invemar.org.co/?q=node/1" target="_blank"><img src="../plantillaSitio/images/geopota_boton_ingresar.pg.jpg" alt='boton ingreso' width="67" height="15" style="position:relative;left:300px"/></a></td>
                                                                                    </tr>
                                                                                    <tr  >
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/siam/" target="_blank"><img src="../plantillaSitio/images/geopota_boton_ingresar.pg.jpg" alt='boton ingreso' width="67" height="15" style="position:relative;left:300px"/></a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><strong> Cat&aacute;logo de&nbsp;Geo-Metadatos </strong></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2"><img src="../plantillaSitio/images/geoportal_metadatos.jpg" alt="catalago de metadatos" width="177" height="100" /></td>
                                                                                        <td width="3">&nbsp;</td>
                                                                                        <td width="378"  style="text-align:justify"><p  class="texttablas">Buscador de metadatos de informaci&oacute;n cartogr&aacute;fica y&nbsp;<br/>
                                                                                                de sensores remotos disponible en el SIAM.</p></td>
                                                                                        <td rowspan="2">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="22" colspan="2"><a href="http://cinto.invemar.org.co/metabuscador/searchPV1.jsp?dirSearch=metINVEMAR" target="_blank"><img src="../plantillaSitio/images/geopota_boton_ingresar.pg.jpg" alt="boton de ingreso" width="67" height="15" style="position:relative;left:300px" /></a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><strong> Est&aacute;ndares y&nbsp;WMS </strong></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2"><img src="../plantillaSitio/images/geoportal_estandares_wms.jpg" alt="estandares y wms" width="177" height="100" /></td>
                                                                                        <td height="70">&nbsp;</td>
                                                                                        <td><p class="texttablas" style="text-align:justify" > Conozca los est&aacute;ndares y normatividad que usamos para la generacion y administraci&oacute;n de la informaci&oacute;n geogr&aacute;fica.<br/>
                                                                                                Encuentre tambi&eacute;n los servicios WMS e IMS. </p></td>
                                                                                        <td rowspan="2">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2"><a href="http://siam.invemar.org.co/siam/geoportal/estandares.jsp" target="_blank"><img src="../plantillaSitio/images/geopota_boton_ingresar.pg.jpg" alt="boton de ingreso" width="67" height="15" style="position:relative;left:300px"/></a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><strong> Zona&nbsp;Descargas </strong></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2"><img src="../plantillaSitio/images/geoportal_descargas.jpg" alt="zona de descarga" width="177" height="100" /></td>
                                                                                        <td>&nbsp;</td>
                                                                                        <td><p class="texttablas"  style="text-align:justify" >Salidas gr&aacute;ficas para descarga libre (pdf's) y tablas de datos.</p></td>
                                                                                        <td rowspan="2">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2"><a href="http://siam.invemar.org.co/siam/geoportal/descargas.jsp" target="_blank"><img src="../plantillaSitio/images/geopota_boton_ingresar.pg.jpg" alt="boton de ingreso" width="67" height="15" style="position:relative;left:300px"/></a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td ></td>
                                                                    </tr>
                                                                </table>
                                                                <p class="texttablas" style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><strong>&iquest;C&oacute;mo funciona?</strong></p>
                                                                <p class="texttablas">El Geoportal del Sistema de Informaci&oacute;n Ambiental Marino de Colombia SIAM, funciona mediante consulta en internet y esta desarrollado bajo una estructura l&oacute;gica y simple de b&uacute;squeda, consta de cuatro servicios (un geovisor, catalogo de geo-metatadatos, estandares y wms, y una zona de descargas).</p>

                                                                <ul type="circle">            
                                                                    <li>
                                                                        <ul type="disc">
                                                                            <li>El <span style="font-weight:bold;color:#006699;font-family:Arial, Helvetica, sans-serif">geovisor</span> y su descripci&oacute;n de uso puede ser consultada en la pagina
                                                                                de bienvenida.</li>
                                                                            <li>El <span style="font-weight:bold;color:#006699;font-family:Arial, Helvetica, sans-serif">catalogo de geo-metatadatos</span>, esta desarrollado sobre la herramienta Cassia
                                                                                al ingresar encontrar&aacute; dos espacios donde deben ingresar la o las palabras claves que desea buscar.</li>
                                                                            <li><span style="font-weight:bold;color:#006699;font-family:Arial, Helvetica, sans-serif">Estandares y wms</span>, En este servicio se encuentran citados los est&aacute;ndares
                                                                                utilizados en el desarrollo de esta herramienta y pone a disposici&oacute;n las URL de los servicios WMS para que puedan ser vinculados por instituciones y usuarios SIG.<br/></li>
                                                                            <li>  <span style="font-weight:bold;color:#006699;font-family:Arial, Helvetica, sans-serif">Zona de descargas</span>, muestra un listado disponible de descargas,  se presenta
                                                                                una imagen de previsualizaci&oacute;n sobre la cual podr&aacute;n iniciarse las descargas mediante un click.</li>
                                                                        </ul>
                                                                    </li>  
                                                                </ul>                   
                                                                <p class="texttablas" style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><span class="texttitulo"><strong>&iquest;Para quien ?</strong></span></p>
                                                                <p class="texttablas">El Geoportal del Sistema de Informaci&oacute;n Ambiental Marino de Colombia SIAM, est&aacute; dirigido a toda la comunidad cient&iacute;fica, acad&eacute;mica, tomadores de decisiones y p&uacute;blico en general interesada en consultar la informaci&oacute;n geogr&aacute;fica ambiental disponible por el pa&iacute;s para mares y costas.erencia
                                                                    e implantaci&oacute;n de tecnolog&iacute;as de la informaci&oacute;n. </p>
                                                                <p class="texttablas" style="color:#006699"><strong>Cr&eacute;ditos</strong></p>
                                                                <p class="texttablas">El Geoportal del Sistema de Informaci&oacute;n Ambiental Marino de Colombia SIAM es creado por el Laboratorio Sistemas de Informaci&oacute;n LabSIS del INVEMAR</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:center">&nbsp;</td>
                                                        </tr>
                                                    </table></div> 
                                                <script type="text/javascript">
                                                    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
                                                    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
                                                </script>
                                                <script type="text/javascript">
                                                    try {
                                                        var pageTracker = _gat._getTracker("UA-2300620-1");
                                                        pageTracker._trackPageview();
                                                    } catch(err) {}</script>

                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>

                        </table>
                        <td width="0"></td>
                        <td width="10"></td>
                </tr>

                <tr>

                    <td style="width:5px; height:4px;"><%@ include file="../plantillaSitio/footermodulesV3.jsp" %></td>

                </tr> 
            </table> </div> <!--abre en lin 23-->
    </body><h1></h1>
</html>



