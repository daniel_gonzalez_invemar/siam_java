<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
<title>Geoportal SIAM
</title>
<meta name="Title" content="Geoportal SIAM"/>
<meta name="Author" content="webmster siam"/>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
<meta name="Subject" content="Geovisor del siam"/>
<meta name="Description" content="Geovisor del sistema de informacion marino de colombia"/>
<meta name="Keywords" content=" Salidas graficas, SIAM, conservación marina, planificacion ecorregional, INVEMAR, ecosistemas marinos, calidad ambiental, erosion costera, poster, Labsis"/>
<meta name="Generator" content="Notepad++"/>
<meta name="Language" content="Spanish"/>
<meta name="Revisit" content="1 day"/>
<meta name="Distribution" content="Global"/>
<meta name="Robots" content="All"/>
 <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css"/>
 <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
</head>
<body>
<jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=90"/>
          <div class="centrado">
          <table width="1016" style="width:950px">      
            <tr>             
              <td width="1004" style="padding-left:4px;vertical-align:top; padding-right:4px;">&nbsp;</td>
            </tr>           
            <tr>
              <td style="width:5px;height:5px;"></td>
            </tr>

            <tr>

              <td  style=" padding-left:4px;padding-right:2px;"><table width="95%" style="width:100%;">
                <tr>        

                  <td><table width="95%" class="colorfondo2" style="width:97%;">
                    <tr>

                     <!-- <td class="titulo_secciones">
                            <h1>Geoportal SIAM</h1>					  </td> -->
                            
                    <td height="47" colspan="2" style="text-align:center;font-size:1.2em;vertical-align:middle;background-image:url(../plantillaSitio/images/fondo_geoportal_menu_2.png) ;width:100%;background-repeat:no-repeat">
                    <div style="text-align:right" class="style15">                    
        			<div style="text-align:left"><a href="#"><span style="color:#036; font-size:1.52em"> Geo </span><span style="color:#FFF; font-size:1.4em"> Portal SIAM - Zona de Descargas</span></a></div>
      				</div></td>
                    
                    </tr>
                    <tr>

                      <td class="titulo_sec_smb" height="9"></td>
                    </tr>
                    <tr>
                      <td  style="height:10px;background-color:#F8FAFC" >
                      <div class="centrado">
                     <table width="939"  style="border:0px">       
        <tr>
          <td width="941" height="1141" style="background-color:#E1EBF4"><p><span class="texttitulo" style="color:#006699; font-size:1.2em"><strong>Zona de descargas</strong></span></p>
            <p class="texttablas">En esta zona esperamos ofrecer la descarga de mapas pre diseñados y que puedan ser de utilidad como herramientas para la investigación y el manejo ambiental marino costero en Colombia. </p>
          <ul type="square"><span class="texttablas" style="color:#006699;font-size:1.1em"><a href="#Salidas" class="blue" target="_parent"><strong>Salidas Gráficas Planificación Ecorregional</strong></a></span>
          <ul type="square" style="font-size:1.1em;color:#666">
            <li><a class="blue" href="#CartillaP" target="_parent">Cartilla de Prioridades de Conservación Marina en Caribe y Pacífico Colombiano.</a></li>
            <li><a class="blue" href="#InformeTecnico">Informe Técnico de Planificación Ecorregional In Situ</a></li>
            <li><a class="blue" href="#Descargas">Descarga de planchas:</a></li>
            	<ul>
            	<li><a class="blue" href="#Caribe">Caribe</a></li>
            	<li><a class="blue" href="#Pacifico">Pacífico</a></li>            
            	</ul>
          </ul></ul>
		 
          <ul type="square" style="font-size:1.1em;color:#666">
		  <a class="blue" href="doc/atlasdeuraba.pdf" target="_blank">Atlas del Golfo de Uraba. Una mirada al Caribe de Antioquia y Choc&oacute;.</a><br/>
		  <a class="blue" href="#Ecosistemas">Mapa de ecosistemas continentales costeros y marinos escala 1:500.000</a><br />		   				
           <a class="blue" href="#Erosion">Diagnóstico de la Erosión en la Zona Costera del Caribe Colombiano" escala 1:100.000</a>
             <ul type="square">
            	<li><a class="blue" href="#LibroErosion">Libro</a></li>
            	<li><a class="blue" href="#SalidasErosion">Salidas Gráficas</a></li>  
               
            </ul>
            <a class="blue" href="#Erosion">Posters LabSiS</a>
          </ul>           
   
            
            <table style="width:100%;border:0px;padding:0xp;border-collapse:collapse" >
              <tr></tr>
              <tr>
                <td >&nbsp;&nbsp;&nbsp;
                  <div>
                    <table height="697" style="width:869;;padding:0px;border:3px solid #78BCD3">
                    <tr>
                      <td colspan="2"><table height="697" style="width:869;;padding:2px;border-spacing:5px;border:3px solid #78BCD3">
                        <tr>                        
                              <td colspan="2"><table height="697" style="width:869;;padding:2px;border-spacing:0px;border:0px solid #78BCD3">
                                <tr>
                                  <td colspan="2"><table height="697" style="width:869;;padding:0px;border-spacing:0px;border:0px solid #78BCD3; background-image:url(../plantillaSitio/images/fondoJ3.png);background-repeat:no-repeat">
                                    <tr></tr>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr style="border:0px solid #78BCD3">
                                      <td colspan="4" style="border:0px solid #78BCD3;color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:3;text-decoration:underline"><p class="texttablas"><a style="color:#000" name="Salidas"><strong>Salidas Gr&aacute;ficas Planificaci&oacute;n Ecorregional</strong></a></p>
                                        <br /></td>
                                    </tr>
                                    <tr>
                                      <td width="352" style="color:#666;font-family:Arial, Helvetica, sans-serif;font-size:2"><strong><span lang="ES-CO" xml:lang="ES-CO">Descargue aqu&iacute; los documentos:</span></strong></td>
                                      <td colspan="2"><a href="../docs/descargas/PE/1_CartillaPrioridadesMarinasColombia_2008.pdf" target="_blank" name="CartillaP"> Cartilla de Prioridades de Conservaci&oacute;n Marina en Caribe y Pac&iacute;fico Colombiano </a></td>
                                      <td width="24" style="text-align:center;padding:4px;border-spacing:2px" rowspan="1"><!--<a href="../docs/descargas/PE/1_CartillaPrioridadesMarinasColombia_2008.pdf"><img src="../plantillaSitio/images/pe_cartilla_1.png" alt="cartilla" target="_blank"/></a>--></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/2_LibroInformePLANECO_Vcompleta_2010.pdf" target="_blank" name="InformeTecnico"> Informe Técnico de Planificación Ecorregional In Situ </a></td>
                                      <td width="24" style="text-align:center;padding:4px;border-spacing:2px"  rowspan="1"><!--<a href="../docs/descargas/PE/2_LibroInformePLANECO_Vcompleta_2010.pdf"><img src="../plantillaSitio/images/pe_informe_1.png" alt="informe" width="69" height="70" target="_blank"/></a>--></td>
                                    </tr>
                                    <tr></tr>
                                    <tr>
                                      <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><a name="Descargas" style="color:#666"><strong><span lang="ES-CO" xml:lang="ES-CO">Descargue  aqu&iacute; las planchas:</span></strong></a></p></td>
                                      <td colspan="2"></td>
                                      <td rowspan="14">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><a name="Caribe" style="color:#666"><strong>Caribe</strong></a></td>
                                      <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                      <td>Portafolio Caribe</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/3M_Cartilla/Portafolio_Caribe_Cartilla_doblecarta_v2.pdf" target="_blank">Portafolio Caribe - Cartilla Doble Carta</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>Objetos de Conservaci&oacute;n (ODC):</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/2M_ODC/ANEXO1_M_ODC_VENTANA1_PLANCHA_1_DE_5_v1.pdf" target="_blank">Mapa ODC - Plancha 1 de 5</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/2M_ODC/ANEXO1_M_ODC_VENTANA2_PLANCHA_2_DE_5_v1.pdf" target="_blank">Mapa ODC - Plancha 2 de 5</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/2M_ODC/ANEXO1_M_ODC_VENTANA3_PLANCHA_3_DE_5_v1.pdf" target="_blank">Mapa ODC - Plancha 3 de 5</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/2M_ODC/ANEXO1_M_ODC_VENTANA4_PLANCHA_4_DE_5_v1.pdf" target="_blank">Mapa ODC - Plancha 4 de 5</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/2M_ODC/ANEXO1_M_ODC_VENTANA5_PLANCHA_5_DE_5_v1.pdf" target="_blank">Mapa ODC - Plancha 5 de 5</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>Amenazas</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA1_PLANCHA_1_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 1 de 10</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA2_PLANCHA_2_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 2 de 10</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA3_PLANCHA_3_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 3 de 10</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;
                                        <p></p></td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA4_PLANCHA_4_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 4 de 10</a></td>
                                      <td rowspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA5_PLANCHA_5_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 5 de 10</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA1_PLANCHA_6_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 6 de 10</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA2_PLANCHA_7_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 7 de 10</a></td>
                                      <td rowspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA3_PLANCHA_8_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 8 de 10</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;
                                        <p></p></td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA4_PLANCHA_9_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 9 de 10</a></td>
                                      <td rowspan="6">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA5_PLANCHA_10_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 10 de 10</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><a name="Pacifico" style="color:#666"><strong>Pacf&iacute;fico</strong></a></td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>Portafolio Pac&iacute;fico</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/3M_Cartilla/Portafolio_Pacifico_Cartilla_doblecarta_impresión_carta_v2.pdf target="_blank">Portafolio Pac&iacute;fico - Cartilla Doble Carta</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td >Objetos de Conservaci&oacute;n (ODC):</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano1.pdf" target="_blank">Mapa ODC - Plancha 1 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano2.pdf" target="_blank">Mapa ODC - Plancha 2 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano3.pdf" target="_blank">Mapa ODC - Plancha 3 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano4.pdf" target="_blank">Mapa ODC - Plancha 4 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano5.pdf" target="_blank">Mapa ODC - Plancha 5 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano6.pdf" target="_blank">Mapa ODC - Plancha 6 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano7.pdf" target="_blank">Mapa ODC - Plancha 7 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano8.pdf" target="_blank">Mapa ODC - Plancha 8 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano9.pdf" target="_blank">Mapa ODC - Plancha 9 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano10.pdf" target="_blank">Mapa ODC - Plancha 10 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano11.pdf" target="_blank">Mapa ODC - Plancha 11 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano12.pdf" target="_blank">Mapa ODC - Plancha 12 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano13.pdf" target="_blank">Mapa ODC - Plancha 13 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/2M_ODC/RAMPPacificoODCPlano14.pdf" target="_blank">Mapa ODC - Plancha 14 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td >Amenazas</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano1.pdf" target="_blank">Mapa de Amenazas - Plancha 1 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano2.pdf" target="_blank">Mapa de Amenazas - Plancha 2 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano3.pdf" target="_blank">Mapa de Amenazas - Plancha 3 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano4.pdf" target="_blank">Mapa de Amenazas - Plancha 4 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano5.pdf" target="_blank">Mapa de Amenazas - Plancha 5 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano6.pdf" target="_blank">Mapa de Amenazas - Plancha 6 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano7.pdf" target="_blank">Mapa de Amenazas - Plancha 7 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano8.pdf" target="_blank">Mapa de Amenazas - Plancha 8 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano9.pdf" target="_blank">Mapa de Amenazas - Plancha 9 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/ANEXO2_M_RAMPPacificoAmenazasPlano10.pdf" target="_blank">Mapa de Amenazas - Plancha 10 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano11.pdf" target="_blank">Mapa de Amenazas - Plancha 11 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano12.pdf" target="_blank">Mapa de Amenazas - Plancha 12 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano13.pdf" target="_blank">Mapa de Amenazas - Plancha 13 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/PE/PE_Pacifico/1M_AME/RAMPPacificoAmenazasPlano14.pdf" target="_blank">Mapa de Amenazas - Plancha 14 de 14</a></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2; text-decoration:underline"><a name="Ecosistemas" style="color:#000""><strong>Mapa de ecosistemas continentales costeros y marinos escala 1:500.000</strong></a><br /></td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td >Descarga de documento y hojas (1 a la 30) a&ntilde;o 2008</td>
                                      <td colspan="2">Acceda <a href="http://www.invemar.org.co/noticias.jsp?id=3458&amp;idcat=104" style="text-decoration:underline;color:#006699" target="_blank">aqu&iacute;.</a> (Click sobre cada cuadro del mapa para descargar las hojas)</td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2; text-decoration:underline"><a name="Erosion" style="color:#000"">&quot;Diagn&oacute;stico de la Erosi&oacute;n en la Zona   Costera del Caribe Colombiano&quot; escala 1:100.000, 2008</a></td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td >Libro</td>
                                      <td colspan="2"><a href="../docs/descargas/LibroErosionPDF/605220080501_AErosionCaribeContinentalColombia.pdf" style="text-decoration:underline;color:#006699"
                          target="_blank" name="LibroErosion">Parte 1</a> - <a href="../docs/descargas/LibroErosionPDF/604620080501_BErosionCaribeContinentalColombia.pdf" style="text-decoration:underline;color:#006699" target="_blank"> Parte 2</a> - <a href="../docs/descargas/LibroErosionPDF/604720080501_CErosionCaribeContinentalColombia.pdf" style="text-decoration:underline;color:#006699" target="_blank"> Parte 3</a> - <a href="../docs/descargas/LibroErosionPDF/606320080501_DErosionCaribeContinetalColombia.pdf" style="text-decoration:underline;color:#006699" target="_blank"> Parte 4</a> - <a href="../docs/descargas/LibroErosionPDF/605120080501_EErosionCaribeContinentalColombia.pdf" style="text-decoration:underline;color:#006699" target="_blank"> Parte 5</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td >Salidas Gr&aacute;ficas</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.1.pdf" target="_blank" name="SalidasErosion">Mapa Geomorfol&oacute;gico del Departamento de la Guajira 6.1</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.2.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de la Guajira 6.2</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.3.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de la Guajira 6.3</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.4.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de la Guajira 6.4</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.5.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de la Guajira 6.5</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.6.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de la Guajira 6.6</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.7.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de la Guajira 6.7</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.8.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de la Guajira 6.8</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.9.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento del Magdalena 6.9</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.10.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento del Magdalena 6.10</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.11.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento del Magdalena 6.11</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.12.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento del Magdalena 6.12</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.13.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de Atl&aacute;ntico 6.13</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.14.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de Atl&aacute;ntico 6.14</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.15.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de Bol&iacute;var 6.15</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.16.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de Bol&iacute;var 6.16</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.17.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de Bol&iacute;var 6.17</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.18.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de Sucre 6.18</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.19.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de Sucre 6.19</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.20.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de C&oacute;rdoba 6.20</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.21.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de C&oacute;rdoba 6.21</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.22.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de Antioquia 6.22</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.23.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de Antioquia 6.23</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.24.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de Antioquia y Choc&oacute; 6.24</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.25.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de </a><a href="../docs/descargas/erosionsg/6.24.pdf" target="_blank">Antioquia y Choc&oacute;</a><a href="../docs/descargas/erosionsg/6.25.pdf" target="_blank"> 6.25</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/erosionsg/6.26.pdf" target="_blank">Mapa Geomorfol&oacute;gico del Departamento de Choc&oacute; 6.26</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2; text-decoration:underline"><a name="Posters" style="color:#000">Poster</a></td>
                                      <td colspan="2"><a href="../docs/descargas/poster/0Introduccion1.pdf" target="_blank">Instroducci&oacute;n INVEMAR</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/poster/1.AnalisisEspacioTemporal.pdf" target="_blank">An&aacute;lisis espacio-temporal con sensoramiento remoto</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/poster/2.BahiaColombiaEvolucion_SELPER2008.pdf" target="_blank">Din&aacute;mica (1938-2001) de los fondos de Bah&iacute;a Colombia, Sur del Golfo de Urab&aacute;</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/poster/3.SIG_SR_CaracterizacionDiagnostico_ZPoster.pdf" target="_blank"> SIG y Sensores remotos, Herramientas de apoyo en la caracterizaci&oacute;n y diagn&oacute;stico de las zonas costeras en Colombia. </a><a href="../docs/descargas/poster/4.GIS_RS_CoastalZonePoster.pdf target="_blank" ">(English Version Here)</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/poster/5.UseGIS_RSinCoastalZonePoster.PDF" target="_blank"> Use of information systems and 
                                        remote sensors for marine and coastal studies in colombia </a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/poster/6.LozanoetalCGSMV5.pdf" target="_blank">Aplicacion de datos satelitales durante los &uacute;ltimos	
                                        50 a&ntilde;os para el manejo de bosque de manglar en la CGSM, Caribe colombiano</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/poster/7.LozanoetalManglaresV6a.pdf" target="_blank">Zonificaci&oacute;n del bosque de manglar del 
                                        departamento del Atl&aacute;ntico a partir de percepci&oacute;n remota y SIG</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/poster/8.PosterCAM.pdf" target="_blank">Red de Vigilancia de la calidad ambiental Marina en Colombia - REDCAM</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/poster/9.PosterFanerogamas.pdf" target="_blank">Distribuci&oacute;n de Faner&oacute;gamas en Colombia</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/poster/10.SURGENCIARguajira_Poster_COLACMAR2007_5.pdf" target="_blank">Variabilidad Intra e Interanual de la surgencia de la Guajira, Colombia</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2"><a href="../docs/descargas/poster/11.Vulnerabilidad2.pdf" target="_blank">&iquest;Somos vulnerables ante un eventual ascenso del nivel del mar?</a></td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td >&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                    </tr>
                                  </table></td>
                                </tr>                              
                          </table></td>
                        </tr>
                      </table></tr>
                    </table>
                  </div></td>
              </tr>
              <tr>
                <td ></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td style="text-align:center">&nbsp;</td>
        </tr>
      </table></div> 
                    <script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
                    <script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2300620-1");
pageTracker._trackPageview();
} catch(err) {}</script>
                  
                </td>
                    </tr>
                  </table></td>
                </tr>

              </table>
              <td width="10"></td>
			</td>
            </tr>

            <tr>

              <td style="width:5px; height:4px;"><%@ include file="../plantillaSitio/footermodulesV3.jsp" %></td>
              
            </tr> 
</table> </div> <!--abre en lin 23-->
</body><h1></h1>
</html>

          
          
