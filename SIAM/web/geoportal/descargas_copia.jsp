<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
<title>Geoportal SIAM
</title>
<meta name="Title" content="Geoportal SIAM"/>
<meta name="Author" content="webmster siam"/>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
<meta name="Subject" content="Geovisor del siam"/>
<meta name="Description" content="Geovisor del sistema de informacion marino de colombia"/>
<meta name="Keywords" content="lenguaje html, manual, tutorial, curso, dise�o web"/>
<meta name="Generator" content="Notepad++"/>
<meta name="Language" content="Spanish"/>
<meta name="Revisit" content="1 day"/>
<meta name="Distribution" content="Global"/>
<meta name="Robots" content="All"/>
 <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css"/>
 <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
</head>
<body>
<jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=90"/>
          <div class="centrado">
          <table width="1016" style="width:950px">      
            <tr>             
              <td width="1004" style="padding-left:4px;vertical-align:top; padding-right:4px;">&nbsp;</td>
            </tr>           
            <tr>
              <td style="width:5px;height:5px;"></td>
            </tr>

            <tr>

              <td  style=" padding-left:4px;padding-right:2px;"><table width="95%" style="width:100%;">
                <tr>        

                  <td><table style="width:100%;" class="colorfondo2">
                    <tr>

                     <!-- <td class="titulo_secciones">
                            <h1>Geoportal SIAM</h1>					  </td> -->
                            
                    <td height="47" colspan="2" style="text-align:center;font-size:1.2em;vertical-align:middle;background-image:url(../plantillaSitio/images/fondo_geoportal_menu.jpg)">
                    <div style="text-align:right" class="style15">                    
        			<div style="text-align:left"><a href="#"><span style="color:#036; font-size:1.52em"> Geo </span><span style="color:#FFF; font-size:1.4em"> Portal SIAM - Zona de Descargas</span></a></div>
      				</div></td>
                    
                    </tr>
                    <tr>

                      <td class="titulo_sec_smb" height="9"></td>
                    </tr>
                    <tr>
                      <td  style="height:10px;background-color:#F8FAFC" >
                      <div class="centrado">
                     <table width="963px"  style="border:0px">       
        <tr>
          <td height="1141" style="background-color:#E1EBF4"><p><span class="texttitulo" style="color:#006699; font-size:1.2em"><strong>Zona de descargas</strong></span></p>
            <p class="texttablas">En esta zona esperamos ofrecer la descarga de mapas pre dise�ados y que puedan ser de utilidad como herramientas para la investigaci�n y el manejo ambiental marino costero en Colombia. </p>
            <table style="width:100%;border:0px;padding:0xp;border-collapse:collapse" >
              <tr></tr>
              <tr>
                <td >&nbsp;&nbsp;&nbsp;
                  <div>
                    <table height="697" style="width:869;;padding:0px">
                         <tr style="border:2px solid #78BCD3"> 
                        <td colspan="4" style="border:3px solid #78BCD3;color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>Salidas Gr&aacute;ficas Planificacion Ecorregional</strong></p></td> 
                         </tr>                                                       
                     
                      <tr>                        
                        <td width="236" style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><strong><span lang="ES-CO" xml:lang="ES-CO">Descargue aqu&iacute; los documentos:</span></strong></td>
                        <td colspan="2"><a href="../docs/descargas/PE/1_CartillaPrioridadesMarinasColombia_2008.pdf" target="_blank">
                        Cartilla de Prioridades de Conservaci&oacute;n Marina en Caribe y Pac&iacute;fico Colombiano  </a></td>
                        <td width="125" style="text-align:center;padding:4px;border-spacing:2px" rowspan="1"><a href="../docs/descargas/PE/1_CartillaPrioridadesMarinasColombia_2008.pdf"><img src="../plantillaSitio/images/pe_cartilla_1.png" alt="cartilla" target="_blank"/></a></td>                                                  
                      </tr>
                      <tr>
                        <td >&nbsp;</td>
                        <td colspan="2"><a href="../docs/descargas/PE/2_LibroInformePLANECO_Vcompleta_2010.pdf" target="_blank" >
                        Informe T�cnico de Planificaci�n Ecorregional In Situ </a></td>
                         <td width="125" style="text-align:center;padding:4px;border-spacing:2px"  rowspan="1"><a href="../docs/descargas/PE/2_LibroInformePLANECO_Vcompleta_2010.pdf"><img src="../plantillaSitio/images/pe_informe_1.png" alt="informe" width="69" height="70" target="_blank"/></a></td> 
                      </tr>
                      <tr>
                     
                      </tr>                     
                    <tr>
                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong><span lang="ES-CO" xml:lang="ES-CO">Descargue  aqu&iacute; las planchas:</span></strong></p></td> 
                        <td colspan="2"></td>
                        <td rowspan="14">&nbsp;</td>                 
                      </tr>
                      <tr>                        
                        <td>&nbsp;</td>
                        <td colspan="2">&nbsp;</td>                        
                      </tr>
                      <tr>
                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><strong>Caribe</strong></td>
                        <td colspan="2"></td>
                      </tr>                     
                       <tr>
                        <td>Portafolio Caribe</td>
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/3M_Cartilla/Portafolio_Caribe_Cartilla_doblecarta_v2.pdf" target="_blank">Portafolio Caribe - Cartilla Doble Carta</a></td>
                      </tr>                              
                       <tr>
                        <td>&nbsp;</td>
                         <td colspan="2">&nbsp;</td>
                      </tr>                            
                       <tr>
                        <td>Objetos de Conservaci&oacute;n (ODC):</td>
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/2M_ODC/ANEXO1_M_ODC_VENTANA1_PLANCHA_1_DE_5_v1.pdf" target="_blank">Mapa ODC - Plancha 1 de 5</a></td>
                      </tr>                               
                       <tr>
                        <td>&nbsp;</td>
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/2M_ODC/ANEXO1_M_ODC_VENTANA2_PLANCHA_2_DE_5_v1.pdf" target="_blank">Mapa ODC - Plancha 2 de 5</a></td>
                      </tr>                               
                       <tr>
                        <td>&nbsp;</td>
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/2M_ODC/ANEXO1_M_ODC_VENTANA3_PLANCHA_3_DE_5_v1.pdf" target="_blank">Mapa ODC - Plancha 3 de 5</a></td>
                      </tr>                               
                       <tr>
                        <td>&nbsp;</td>
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/2M_ODC/ANEXO1_M_ODC_VENTANA4_PLANCHA_4_DE_5_v1.pdf" target="_blank">Mapa ODC - Plancha 4 de 5</a></td>
                      </tr>                            
                       <tr>
                        <td>&nbsp;</td>
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/2M_ODC/ANEXO1_M_ODC_VENTANA5_PLANCHA_5_DE_5_v1.pdf" target="_blank">Mapa ODC - Plancha 5 de 5</a></td>
                      </tr>                           
                       <tr>
                        <td>&nbsp;</td>
                         <td colspan="2">&nbsp;</td>
                      </tr>                               
                       <tr>
                        <td>Amenazas</td>
                         <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA1_PLANCHA_1_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 1 de 10</a></td>
                      </tr>                          
                       <tr>
                        <td>&nbsp;</td>
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA2_PLANCHA_2_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 2 de 10</a></td>
                      </tr>                   
                        <tr>
                        <td >&nbsp;</td>
                         <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA3_PLANCHA_3_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 3 de 10</a></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</p></td> 
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA4_PLANCHA_4_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 4 de 10</a></td>
                        <td rowspan="3">&nbsp;</td>                 
                      </tr>
                      <tr>                        
                        <td>&nbsp;</td>
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA5_PLANCHA_5_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 5 de 10</a></td>                        
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA1_PLANCHA_6_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 6 de 10</a></td>
                      </tr>
                       <tr>
                        <td>&nbsp;</td> 
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA2_PLANCHA_7_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 7 de 10</a></td>
                        <td rowspan="2">&nbsp;</td>                 
                      </tr>
                      <tr>                        
                        <td>&nbsp;</td>
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA3_PLANCHA_8_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 8 de 10</a></td>                        
                      </tr>
                       <tr>
                        <td>&nbsp;</p>
                        </td> 
                        <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA4_PLANCHA_9_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 9 de 10</a></td>
                        <td rowspan="6">&nbsp;</td>                 
                      </tr>
                       <tr>
                        <td>&nbsp;</td>
                         <td colspan="2"><a href="../docs/descargas/PE/PE_Caribe/1M_Ame/ANEXO2_M_AMENAZAS_VENTANA5_PLANCHA_10_DE_10_v1.pdf" target="_blank">Mapa de Amenazas - Plancha 10 de 10</a></td>
                      </tr>                          
                       <tr>
                        <td>&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                      </tr>                   
                        <tr>
                        <td ><strong>Pacf&iacute;fico</strong></td>
                         <td colspan="2">&nbsp;</td>
                      </tr>
                       <tr>
                        <td>Estado de suelos y Tierras</td>
                        <td colspan="2"><a heref="http://xue.ideam.gov.co/geoserver/ows?namespace=estado">
                        http://xue.ideam.gov.co/geoserver/ows?namespace=estado</a></td>
                      </tr>                   
                        <tr>
                        <td >Monitoreo de Par&aacute;metros Ambientales</td>
                         <td colspan="2"><a heref="http://chiminichagua.ideam.gov.co/OGCDivisionpolitica?">
                        http://xue.ideam.gov.co/geoserver/ows?namespace=monambiental</a></td>
                      </tr>                               
                                
                    </table>
                  </div></td>
              </tr>
              <tr>
                <td ></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td style="text-align:center">&nbsp;</td>
        </tr>
      </table></div> 
                    <script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
                    <script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2300620-1");
pageTracker._trackPageview();
} catch(err) {}</script>
                  
                </td>
                    </tr>
                  </table></td>
                </tr>

              </table>
              <td width="10"></td>
			</td>
            </tr>

            <tr>

              <td style="width:5px; height:4px;"><%@ include file="../plantillaSitio/footermodulesV3.jsp" %></td>
              
            </tr> 
</table> </div> <!--abre en lin 23-->
</body><h1></h1>
</html>

          
          
