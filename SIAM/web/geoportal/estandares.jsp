<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
    <head>
        <title>Geoportal SIAM
        </title>
        <meta name="Title" content="Geoportal SIAM"/>
        <meta name="Author" content="webmster siam"/>
        <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
        <meta name="Subject" content="Geovisor del siam"/>
        <meta name="Description" content="Geovisor del sistema de informacion marino de colombia"/>
        <meta name="Keywords" content="WMS, Geoportal, SIAM, Estandar, Servicios WMS, mapas,INVEMAR, servicios INVEMAR"/>
        <meta name="Generator" content="Notepad++"/>
        <meta name="Language" content="Spanish"/>
        <meta name="Revisit" content="1 day"/>
        <meta name="Distribution" content="Global"/>
        <meta name="Robots" content="All"/>
        <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css"/>
        <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
    </head>
    <body>
        <jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=90"/>
        <div class="centrado">
            <table width="1016" style="width:950px">      
                <tr>             
                    <td width="1004" style="padding-left:4px;vertical-align:top; padding-right:4px;">&nbsp;</td>
                </tr>           
                <tr>
                    <td style="width:5px;height:5px;"></td>
                </tr>

                <tr>

                    <td  style=" padding-left:4px;padding-right:2px;"><table width="95%" style="width:100%;">
                            <tr>        

                                <td><table width="97%" class="colorfondo2">

                                        <tr>
                                            <!-- <td class="titulo_secciones">
                                                   <h1>Geoportal SIAM</h1>					  </td> -->

                                            <td height="47" colspan="2" style="text-align:center;font-size:1.2em;vertical-align:middle;background-image:url(../plantillaSitio/images/fondo_geoportal_menu_2.png);background-repeat:no-repeat">
                                                <div style="text-align:right" class="style15">                    
                                                    <div style="text-align:left"><a href="#"><span style="color:#036; font-size:1.52em"> Geo </span><span style="color:#FFF; font-size:1.4em"> Portal SIAM - Estándares y WMS </span></a></div>
                                                </div></td>

                                        </tr>

                                        <tr>

                                            <td class="titulo_sec_smb" height="9"></td>
                                        </tr>
                                        <tr>
                                            <td  style="height:10px;background-color:#F8FAFC" >
                                                <div class="centrado">
                                                    <table width="936"  style="border:0px">       
                                                        <tr>
                                                            <td width="938" height="1141" style="background-color:#E1EBF4"><p><span class="texttitulo" style="color:#006699; font-size:1.2em"><strong>Zona de acceso a los servicios WMS de INVEMAR y estándares usados</strong></span></p>
                                                                <p class="texttablas">El Geoportal del Sistema de Informaci&oacute;n Ambiental Marino de Colombia SIAM est&aacute; construido siguiendo los estandares para servicios web WMS 1.1 y para metadatos FGDC. </p>
                                                                <table style="width:100%;border:0px;padding:0xp;border-collapse:collapse" >
                                                                    <tr></tr>
                                                                    <tr>
                                                                        <td >&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                                                                            <div class="centrado">
                                                                                <table width="921" height="697"  style="border:1px;border-style:outset;padding:0px;border-spacing:5px">
                                                                                    <tr>
                                                                                        <td width="184" style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>Servicios WMS de cartograf&iacute;a del Sistema de Información Ambiental Marino de Colombia, SIAM</strong></p></td>
                                                                                        <td width="725" colspan="2">&nbsp;</td>
                                                                                        <!-- <td rowspan="2" width="2">&nbsp;</td> -->
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="1">SIAM: </td>
                                                                                        <td colspan="2" ><a href="http://gis.invemar.org.co/ArcGIS/services/SIAM/1_TM_SIAMTematico/MapServer/WMSServer" target="_blank">http://gis.invemar.org.co/ArcGIS/services/SIAM/1_TM_SIAMTematico/MapServer/WMSServer</a></td>                      
                                                                                    </tr>                   
                                                                                    <tr>
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"></td>
                                                                                        <td colspan="2"></td>
                                                                                        <!-- <td rowspan="4">&nbsp;</td> -->
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>Servicio WMS de Ecosistemas Marinos de Colombia 
                                                                                                </strong></td>
                                                                                        <td colspan="2">&nbsp;</td> 
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td >Mapa ecosistemas y paisajes del fondo marino 1:500000</td>
                                                                                        <td colspan="2" style="text-align:justify"><a href="http://gis.invemar.org.co/arcgis/services/MECCM/1_TM_MECCM500k_EcosistemasMarinoCosteros/MapServer/WMSServer" target="_blank">http://gis.invemar.org.co/arcgis/services/MECCM/1_TM_MECCM500k_EcosistemasMarinoCosteros/MapServer/WMSServer</a></td>                     
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Ecosistemas Marinos</td> 
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/iabin_ecosistemas/1_TM_IABIN_ecosistemas/MapServer/WMSServer">http://gis.invemar.org.co/arcgis/services/iabin_ecosistemas/1_TM_IABIN_ecosistemas/MapServer/WMSServer</a></td>
                                                                                    </tr>                   
                                                                                    <tr>
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong> Servicio WMS &Aacute;reas significativas de biodiversidad marina en los bloques de exploraci&oacute;n de hidrocarburos</strong></p></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                        <!--<td rowspan="2">&nbsp;</td> -->
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="1">Biodiversidad Marina en bloques de exploraci&oacute;n de hidrocarburos </td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/ANH2/1_TM_ANH2/MapServer/WMSServer">http://gis.invemar.org.co/arcgis/services/ANH2/1_TM_ANH2/MapServer/WMSServer</a></td>                       
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong> Servicio WMS Caladeros de Pesca</strong></p></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                        <!--<td rowspan="2">&nbsp;</td> -->
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="1">Extens&iacute;on de Caladeros de Pesca Artesanal e Industrial</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/CaladerosdePesca/Tematica_Caladeros_de_Pesca/MapServer/WMSServer?">http://gis.invemar.org.co/arcgis/services/CaladerosdePesca/Tematica_Caladeros_de_Pesca/MapServer/WMSServer?</a></td>                       
                                                                                    </tr>
                                                                                    <tr style="display:none">
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>Afectaci&oacute;n del cambio clim&aacute;tico sobre los mares y costas (Ascenso Nivel Mar - ANM)</strong></p></td> 
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                        <!--<td rowspan="12">&nbsp;</td>-->          
                                                                                    </tr>
                                                                                    <tr style="display:none">                        
                                                                                        <td> Caso Mar Caribe</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/ANMCARIBE/MapServer/WMSServer">http://gis.invemar.org.co/arcgis/services/ANMCARIBE/MapServer/WMSServer</a></td>                        
                                                                                    </tr>
                                                                                    <tr style="display:none">
                                                                                        <td>Caso Oc&eacute;ano Pac&iacute;fico</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/ANMPACIFICO/MapServer/WMSServer">
                                                                                                http://gis.invemar.org.co/arcgis/services/ANMPACIFICO/MapServer/WMSServer</a></td>
                                                                                    </tr>
                                                                                    <tr style="display:none">
                                                                                        <td>Caso Cartagena</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/CasoCartagenaTematico/MapServer/WMSServer">http://gis.invemar.org.co/arcgis/services/CasoCartagenaTematico/MapServer/WMSServer</a></td>
                                                                                    </tr>
                                                                                    <tr style="display:none">
                                                                                        <td>Caso Guapil</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/CasoGuapiIsc/MapServer/WMSServer">"http://gis.invemar.org.co/arcgis/services/CasoGuapiIsc/MapServer/WMSServer</a></td>
                                                                                    </tr>
                                                                                    <tr style="display:none">
                                                                                        <td>Caso Morrosquillo</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/CasoMorrosquillo/MapServer/WMSServer">
                                                                                                http://gis.invemar.org.co/arcgis/services/CasoMorrosquillo/MapServer/WMSServer</a></td>
                                                                                    </tr>
                                                                                    <tr style="display:none">
                                                                                        <td>Caso San Andr&eacute;s</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/CasoSanAndres/MapServer/WMSServer">
                                                                                                http://gis.invemar.org.co/arcgis/services/CasoSanAndres/MapServer/WMSServer</a></td>
                                                                                    </tr>
                                                                                    <tr style="display:none">
                                                                                        <td>Caso Tumaco Rural</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/CasoTumacoRural/MapServer/WMSServer">http://gis.invemar.org.co/CasoTumacoRural/MapServer/WMSServer</a></td>
                                                                                    </tr>
                                                                                    <tr style="display:none">
                                                                                        <td>Caso Tumaco Urbano</td>
                                                                                        <td colspan="2"><a href ="http://gis.invemar.org.co/CasoTumacoUrbano/MapServer/WMSServer">http://gis.invemar.org.co/CasoTumacoUrbano/MapServer/WMSServer</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>Incompatibilidad de Usos Marinos y Costeros</strong></p>
                                                                                        </td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Mapa de conflictos de Usos del territorio, componente marino costero.</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/Conflictos/0_BS_Incompatibilidad_usos/MapServer/WMSServer">
                                                                                                http://gis.invemar.org.co/arcgis/services/Conflictos/0_BS_Incompatibilidad_usos/MapServer/WMSServer</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>Din&aacute;mica multitemporal de las coberturas asociadas al manglar en la Ci&eacute;naga Grande de Santa Marta</strong></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Manglares CGSM</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/Manglares_CGSM/1_TM_Manglares_CGSM/MapServer/WMSServer">http://gis.invemar.org.co/arcgis/services/Manglares_CGSM/1_TM_Manglares_CGSM/MapServer/WMSServer</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>Servicios WMS Planificaci&oacute;n ecorregional </strong></p></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>

                                                                                    </tr>                     
                                                                                    <tr>
                                                                                        <td>PLANECO Caribe</p></td> 
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/PERCaribe/1_TM_PERCaribe/MapServer/WMSServer">http://gis.invemar.org.co/arcgis/services/PERCaribe/1_TM_PERCaribe/MapServer/WMSServer</a></td>
                                                                                        <!--<td rowspan="14">&nbsp;</td>-->                 
                                                                                    </tr>
                                                                                    <tr>                        
                                                                                        <td>PLANECO Pac&iacute;fico</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/PERPacifico/1_TM_PERPacifico/MapServer/WMSServer">http://gis.invemar.org.co/arcgis/services/PERPacifico/1_TM_PERPacifico/MapServer/WMSServer</a></td>                        
                                                                                    </tr>
                                                                                                       
                                                                                    <tr style="display:none">
                                                                                        <td>Monitoreo de Calidad Ambiental de Aguas</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/REDCAMVariablesColombia/MapServer/WMSServer">http://gis.invemar.org.co/arcgis/services/REDCAMVariablesColombia/MapServer/WMSServer</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>Servicios sobre Erosi&oacute;n Costera en Colombia</strong></p></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                    </tr>  
																					 <tr>
                                                                                        <td>Erosi&oacute;n costera </td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/ErosionCostera/1_TM_ErosionCostera/MapServer/WMSServer" target='_blank'>http://gis.invemar.org.co/arcgis/services/ErosionCostera/1_TM_ErosionCostera/MapServer/WMSServer</a></td>
                                                                                    </tr>
																					<tr>
                                                                                        <td style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>Servicios  Red de Calidad Ambiental Marina RedCAM</strong></p></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                    </tr>  																					
                                                                                    <tr>
                                                                                        <td>Monitoreo de Calidad Sanitaria de las Playas</td>
                                                                                        <td colspan="2"><a href="http://gis.invemar.org.co/arcgis/services/REDCAM/1_TM_SanitariosColombia/MapServer/WMSServer">http://gis.invemar.org.co/arcgis/services/REDCAM/1_TM_SanitariosColombia/MapServer/WMSServer</a></td>
                                                                                    </tr>    
                                                                                    <tr>
                                                                                        <td ></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                    </tr>
                                                                                </table></br><p>&nbsp;</p>
                                                                                <p style="text-align:left"><span class="texttitulo" style="color:#006699; font-size:1.2em;"><strong> Otros Servicios WMS de inter&eacute;s</strong></span></p>

                                                                                <table width="906" height="718"  style="border:1px;border-style:outset;padding:0px;border-spacing:5px">                                         
                                                                                    <tr>
                                                                                        <td width="369" height="33" style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong> Servicios WMS del IGAC </strong></p></td>                        <td width="525" colspan="2"></td>                                 
                                                                                    </tr>
                                                                                    <tr>                        
                                                                                        <td height="35">Cartograf&iacute;a B&aacute;sica, 1:500.000</td>
                                                                                        <td colspan="2"><a href="http://geocarto.igac.gov.co:8399/arcgis/services/WMS/Carto_500000/MapServer/WMSServer?">http://geocarto.igac.gov.co:8399/arcgis/services/WMS/Carto_500000/MapServer/WMSServer?</a></td>                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="33">Cartograf&iacute;a B&aacute;sica, 1:100.000</td>
                                                                                        <td colspan="2"><a href="http://geocarto.igac.gov.co:8399/arcgis/services/WMS/Carto_100000/MapServer/WMSServer?">http://geocarto.igac.gov.co:8399/arcgis/services/WMS/Carto_100000/MapServer/WMSServer?</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="33">Imágenes Ráster (Landsat)</td>
                                                                                        <td colspan="2"><a heref="http://geoservice.igac.gov.co/WMSLandsat?">
                                                                                                http://geoservice.igac.gov.co/WMSLandsat?</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="33">Ortofotos de Colombia</td>
                                                                                        <td colspan="2"><a href="http://geoservice.igac.gov.co/WMSOrtofotos?">http://geoservice.igac.gov.co/WMSOrtofotos?</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="35" style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>Servicios WMS IDEAM</strong>
                                                                                            </p> </td> 
                                                                                        <td colspan="2"></td>                                 
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="33">Servicio de Mapas del IDEAM (amenazas, meteorolog&iacute;a, coberturas, vulnerabilidad, etc.)</td>
                                                                                        <td colspan="2"><a href="http://bacata.ideam.gov.co/geoserver/wms?">http://bacata.ideam.gov.co/geoserver/wms?</a></td>
                                                                                    </tr>
                                                                                    <tr>                        
                                                                                        <td height="33">Servicio WMS de Ecosistemas continentales y 
                                                                                            costeros, escala 1:500.000</td>
                                                                                        <td colspan="2"><a href="http://chiminichagua.ideam.gov.co/OGCEcosistemas?">http://chiminichagua.ideam.gov.co/OGCEcosistemas?</a></td>                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="33">Corporaciones Aut&oacute;nomas Regionales CAR</td>
                                                                                        <td colspan="2"><a heref="http://chiminichagua.ideam.gov.co/OGCCorporaciones?">
                                                                                                http://chiminichagua.ideam.gov.co/OGCCorporaciones?</a></td>
                                                                                    </tr>                   
                                                                                    <tr>
                                                                                        <td height="33" >Divisi&oacute;n Política DANE</td>
                                                                                        <td colspan="2"><a heref="http://chiminichagua.ideam.gov.co/OGCDivisionpolitica?">
                                                                                                http://chiminichagua.ideam.gov.co/OGCDivisionpolitica?</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="33">Temperatura </td>
                                                                                        <td colspan="2"><a heref="http://xue.ideam.gov.co/geoserver/ows?namespace=ind_climaticos?">http://xue.ideam.gov.co/geoserver/ows?namespace=ind_climaticos</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="33">Estado de suelos y Tierras</td>
                                                                                        <td colspan="2"><a heref="http://xue.ideam.gov.co/geoserver/ows?namespace=estado">
                                                                                                http://xue.ideam.gov.co/geoserver/ows?namespace=estado</a></td>
                                                                                    </tr>                   
                                                                                    <tr>
                                                                                        <td height="33" >Monitoreo de Par&aacute;metros Ambientales</td>
                                                                                        <td colspan="2"><a heref="http://xue.ideam.gov.co/geoserver/ows?namespace=monambiental">
                                                                                                http://xue.ideam.gov.co/geoserver/ows?namespace=monambiental</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="35" style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>Servicios Marine Geoscience Data System - MGDS</strong>
                                                                                            </p> </td> 
                                                                                        <td colspan="2"></td>                                 
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="33" >Global Multi-Resolution Topography (GMRT)</td>
                                                                                        <td colspan="2"><a heref="http://www.marine-geo.org/services/wms?">
                                                                                                http://www.marine-geo.org/services/wms?</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="35" style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>NOAA</strong></p></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="33" >Real-Time Coastal Observations and NOAA forecast</td>
                                                                                        <td colspan="2"><a href="http://nowcoast.noaa.gov/wms/com.esri.wms.Esrimap/wwa?">http://nowcoast.noaa.gov/wms/com.esri.wms.Esrimap/wwa?</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="35" style="color:#006699;font-family:Arial, Helvetica, sans-serif;font-size:2"><p class="texttablas"><strong>Enlaces a otros WMS  </strong></p></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="33">Infraestructura Colombiana de Datos Espaciales ICDE</td>
                                                                                        <td colspan="2"><a href="http://www.icde.org.co/web/guest/listado_servicios_web">http://www.icde.org.co/web/guest/listado_servicios_web</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="33">Federal Geographic Data Committee (FGDC)</td>
                                                                                        <td colspan="2"><a href="http://registry.fgdc.gov/statuschecker/reportResults.php?catalog=gos&serviceType=wms">
                                                                                                http://registry.fgdc.gov/statuschecker/reportResults.php?serviceType=wms&amp;catalogID=2</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="20"></td>
                                                                                        <td colspan="2">&nbsp;</td>
                                                                                    </tr>


                                                                                </table>
                                                                            </div></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td ></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:center">&nbsp;</td>
                                                        </tr>
                                                    </table></div> 
                                                <script type="text/javascript">
                                                    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
                                                    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
                                                </script>
                                                <script type="text/javascript">
                                                    try {
                                                        var pageTracker = _gat._getTracker("UA-2300620-1");
                                                        pageTracker._trackPageview();
                                                    } catch(err) {}</script>

                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>

                        </table>
                        <td width="10"></td>
                    </td>
                </tr>

                <tr>

                    <td style="width:5px; height:4px;"><%@ include file="../plantillaSitio/footermodulesV3.jsp" %></td>

                </tr> 
            </table> </div> <!--abre en lin 23-->
    </body><h1></h1>
</html>



