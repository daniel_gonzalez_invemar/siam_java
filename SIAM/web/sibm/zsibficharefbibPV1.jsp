<%@ include file="common.jsp" %>
<%@ page import="especies.*" %>

<%
  /*Cconexsib con1 = null;
  Connection conn1 = null;
  con1 = new Cconexsib();
  conn1 = con1.getConn();*/
  Connection conn1 = sibmDS.getConnection();

  especies.Cinforeferencias inforeferencias = new especies.Cinforeferencias();
  especies.Ccontinforeferencias continforeferencias = new especies.Ccontinforeferencias();
  continforeferencias.contenedor(conn1, request.getParameter("clave"), request.getParameter("fichamuseo"), request.getParameter("referencia"), request.getParameter("phylum"));

  /*if (con1!= null)
    con1.close();*/
  DbUtils.close(conn1);  


  int l= 0;

%>
<jsp:include page="../plantillaSitio/headermodulos.jsp?idsitio=34"/>
<tr style="padding:20px; border-color:Blue; border-width:thin;">
<td style="padding:5px;">
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
    <td width="21"><img src="../images/arrow2.gif" width="20" height="20"></td>
    <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td width="310" align="left" class="linksnegros">Referencias bibliogr&aacute;ficas</td>
          <td width="419" align="right">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table align="center" border="0" width="963px">
  <tbody>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">*Tax&oacute;n:</td>
    <td width="575" bgcolor="#e6e6e6" ><span class="txttaxon2"><%=request.getParameter("nombre")%></span>&nbsp;&nbsp;<span class="txttaxon1"><%=request.getParameter("autor")%></span></td>
  </tr>
  <tr align="right">
    <td colspan="2" ><font face="Arial, Helvetica, sans-serif"> <strong> <font color="#ff0000" size="2"> <%=continforeferencias.gettamano()%> </font></strong><font color="#000066" size="2">registro(s)
      encontrado(s)</font></font></td>
    </tr>
</table>
<table align="center" border="0" width="963px">
  <tbody>
    <tr>
      <td colspan="3"><img src="../images/spacer.gif" width="1" height="1"></td>
    </tr>
    <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
    <tr bgcolor="#B5D7DE">
      <td colspan="3">&nbsp; <img src="../images/arrowx.gif" width="5" height="7">&nbsp; <span class="linksnegros">Referencias
          bibliogr&aacute;ficas asociadas</span></td>
    </tr>
    <%
      for(l = 0; l < continforeferencias.gettamano(); l++ ){
        inforeferencias = new especies.Cinforeferencias();
        inforeferencias = continforeferencias.getinforeferencias(l);
    %>
    <tr bgcolor="#ffffff">
      <td width="165" valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="texttablas"><%=inforeferencias.get_acodigo()%></td>
          </tr>
        </table>
      </td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="txttaxon4"><%=inforeferencias.get_areferencia()%></td>
          </tr>
        </table>
      </td>
    </tr>
    <% } %>
    <tr bgcolor="#ffffff">
      <td colspan="3"><div align="right"><a href="#top" class="linksbotton">Volver arriba</a> </div></td>
    </tr>

</table>
<%@ include file="sibm_fondoPV1.jsp" %>
</tr>
</td>
<%@ include file="../plantillaSitio/footermodules.jsp" %>
