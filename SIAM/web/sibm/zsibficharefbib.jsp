<%@ include file="common.jsp" %>
<%@ include file="sibm_cabezote.jsp" %>
<%@ page import="especies.*" %>

<%
  /*Cconexsib con1 = null;
  Connection conn1 = null;
  con1 = new Cconexsib();
  conn1 = con1.getConn();*/
  Connection conn1 = sibmDS.getConnection();

  especies.Cinforeferencias inforeferencias = new especies.Cinforeferencias();
  especies.Ccontinforeferencias continforeferencias = new especies.Ccontinforeferencias();
  continforeferencias.contenedor(conn1, request.getParameter("clave"), request.getParameter("fichamuseo"), request.getParameter("referencia"), request.getParameter("phylum"));

  /*if (con1!= null)
    con1.close();*/
  DbUtils.close(conn1);  


  int l= 0;

%>
<script type="text/javascript">

var pagina = 'http://siam.invemar.org.co/siam/sibm/zsibficharefbibPV1.jsp';
document.location.href=pagina;
</script
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#096A95">
    <td colspan="2" class="titulo_ubicacion"><img src="../images/spacer.gif" width="5" height="5"><a href="../index.htm" class="titulo_ubicacion">Inicio</a> &gt; <a href="../siam.htm" class="titulo_ubicacion">SIAM</a> &gt; <a href="index.htm" class="titulo_ubicacion">Sistema
      de Informaci&oacute;n sobre Biodiversidad Marina</a> &gt; Referencias bibliogr&aacute;ficas</td>
  </tr>
  <tr>
    <td colspan="2"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
    <td width="21"><img src="../images/arrow2.gif" width="20" height="20"></td>
    <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td width="310" align="left" class="linksnegros">Referencias bibliogr&aacute;ficas</td>
          <td width="419" align="right"><table border="0" cellpadding="0" cellspacing="0" width="320">
              <tr>
                <td width="17"><a href="index.htm"><img src="../images/orange_home.gif" width="17" height="17" border="0"></a></td>
                <td width="4">&nbsp;</td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="78"><a href="index.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Inicio
                    SIBM</a></td>
                <td width="19"><a href="legal.htm"><img src="../images/orange_legal.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="45"><a href="legal.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Legal</a></td>
                 <td width="19"><a href="index_guia_sibm.htm"><img src="../images/help.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="45"><a href="index_guia_sibm.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Ayuda</a></td>
                <td width="22"><a href="contactenos.htm"><img src="../images/orange_contact.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="75"><div align="left"><a href="contactenos.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Contactenos</a></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table align="center" border="0" width="750">
  <tbody>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">*Tax&oacute;n:</td>
    <td width="575" bgcolor="#e6e6e6" ><span class="txttaxon2"><%=request.getParameter("nombre")%></span>&nbsp;&nbsp;<span class="txttaxon1"><%=request.getParameter("autor")%></span></td>
  </tr>
  <tr align="right">
    <td colspan="2" ><font face="Arial, Helvetica, sans-serif"> <strong> <font color="#ff0000" size="2"> <%=continforeferencias.gettamano()%> </font></strong><font color="#000066" size="2">registro(s)
      encontrado(s)</font></font></td>
    </tr>
</table>
<table align="center" border="0" width="750">
  <tbody>
    <tr>
      <td colspan="3"><img src="../images/spacer.gif" width="1" height="1"></td>
    </tr>
    <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
    <tr bgcolor="#B5D7DE">
      <td colspan="3">&nbsp; <img src="../images/arrowx.gif" width="5" height="7">&nbsp; <span class="linksnegros">Referencias
          bibliogr&aacute;ficas asociadas</span></td>
    </tr>
    <%
      for(l = 0; l < continforeferencias.gettamano(); l++ ){
        inforeferencias = new especies.Cinforeferencias();
        inforeferencias = continforeferencias.getinforeferencias(l);
    %>
    <tr bgcolor="#ffffff">
      <td width="165" valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="texttablas"><%=inforeferencias.get_acodigo()%></td>
          </tr>
        </table>
      </td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="txttaxon4"><%=inforeferencias.get_areferencia()%></td>
          </tr>
        </table>
      </td>
    </tr>
    <% } %>
    <tr bgcolor="#ffffff">
      <td colspan="3"><div align="right"><a href="#top" class="linksbotton">Volver arriba</a> </div></td>
    </tr>

</table>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2300620-1";
urchinTracker();
</script>
<%@ include file="sibm_fondo.jsp" %>
