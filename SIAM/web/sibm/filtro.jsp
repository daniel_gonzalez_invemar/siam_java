<%@ include file="../common.jsp" %>
<%!
static HashMap criterios = new HashMap();
static {
  criterios.put("clase", "CLASE");
  criterios.put("orden", "ORDEN");
  criterios.put("familia", "FAMILIA");
  criterios.put("genero", "GENERO");
  criterios.put("especie", "ESPECIE");
}
%>
<%
JSONObject obj = (JSONObject)json;
JSONObject elements = obj.getJSONObject("elements");
String criterio = (String)criterios.get(elements.getString("criterio"));
ArrayList params = new ArrayList();
params.add("%" + obj.getString("value") + "%");
String division = elements.getString("phylum");
boolean hasDivision = StringUtils.isNotEmpty(division);
if (hasDivision) params.add(division);
%>
<%= sibm.query(limit("SELECT DISTINCT " + criterio + " AS value FROM VM_FICHAC WHERE " + criterio + " IS NOT NULL AND ROUND(MOD(TRUNC((NODOS_ACTIVOS/1),1),10),0) = 1 AND UPPER(" + criterio + ") LIKE UPPER(?)" + (hasDivision? " AND DIVISION = ?": "") + " ORDER BY " + criterio, obj.getInt("position"), obj.getInt("length")), params.toArray(), all) %>