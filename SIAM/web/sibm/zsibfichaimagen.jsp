<%@ include file="common.jsp" %>

<%@ page import="especies.*" %>

<%
  /*Cconexsib con1 = null;
  Connection conn1 = null;
  con1 = new Cconexsib();
  conn1 = con1.getConn();*/
  Connection conn1 = sibmDS.getConnection();

  especies.Cinfoimagen infoimagen = new especies.Cinfoimagen();
  especies.Ccontinfoimagen continfoimagen = new especies.Ccontinfoimagen();
  continfoimagen.contenedor1(conn1, request.getParameter("clave"), request.getParameter("numero"));

  /*if (con1!= null)
    con1.close();*/
  DbUtils.close(conn1);  


  int l= 0;

%>
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<%@ page import="conex.*" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>
<link href="../siamccs.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META name="keywords" content="invemar, ciencia marina, investigaci�n, marino, costera, costero">
<meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

<script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/lib/init.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/lib/lang.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/lib/dom.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/lib/ajax.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/lib/grammar.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/json/json.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/whatwg/whatwg.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/whatwg/lang/en.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/whatwg/parser.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/whatwg/css.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/whatwg/validate.js" type="text/javascript"></script>
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script type="text/javascript">

var pagina = 'http://siam.invemar.org.co/siam/sibm/zsibfichaimagenPV1.jsp';
document.location.href=pagina;
</script>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="3"><img name="head_r1_c1" src="../images/head_r1_c1.jpg" width="3" height="81" border="0" alt=""></td>
    <td width="105" bgcolor="#096A95"><img name="head_r1_c1" src="../images/head_r1_c3.gif" border="0" alt=""></td>
    <td width="507" bgcolor="#096A95"><p class="titulohead">Sistema de Informaci&oacute;n
        sobre Biodiversidad<br>
Marina de Colombia - SIBM</p>
    </td>
    <td width="38" align="right" bgcolor="#096A95"><a href="../index.htm"><img name="head_r1_c4" src="../images/head_r1_c4.jpg" width="38" height="81" border="0" alt="Home"></a></td>
    <td width="43" align="right" bgcolor="#096A95"><a href="../contactenos.htm"><img name="head_r1_c5" src="../images/head_r1_c5.jpg" width="43" height="81" border="0" alt="Cont&aacute;ctenos"></a></td>
    <td width="34" align="right" bgcolor="#096A95"><a href="../mapadelsitio.htm"><img name="head_r1_c6" src="../images/head_r1_c6.jpg" width="34" height="81" border="0" alt="Mapa del sitio"></a></td>
    <td colspan="2" align="right" valign="top" bgcolor="#096A95"><img name="head_r1_c7" src="../images/head_r1_c7.jpg" width="4" height="81" border="0" alt=""></td>
  </tr>
  <tr>
    <td width="3"></td>
    <td width="105"></td>
    <td></td>
    <td width="38"></td>
    <td width="43"></td>
    <td width="34"></td>
    <td width="12"></td>
    <td width="8"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#096A95">
    <td colspan="2" class="titulo_ubicacion"><img src="../images/spacer.gif" width="5" height="5"><a href="../index.htm" class="titulo_ubicacion">Inicio</a> &gt; <a href="../siam.htm" class="titulo_ubicacion">SIAM</a> &gt; <a href="index.htm" class="titulo_ubicacion">Sistema
      de Informaci&oacute;n sobre Biodiversidad Marina</a> &gt; Im&aacute;genes</td>
  </tr>
  <tr>
    <td colspan="2"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
    <td width="21"><img src="../images/arrow2.gif" width="20" height="20"></td>
    <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td width="439" align="left" class="linksnegros">Im&aacute;genes</td>
          <td width="290" align="right"><table border="0" cellpadding="0" cellspacing="0" width="270">
              <tr>
                <td width="17"><a href="index.htm"><img src="../images/orange_home.gif" width="17" height="17" border="0"></a></td>
                <td width="4">&nbsp;</td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="78"><a href="index.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Inicio
                    SIBM</a></td>
                <td width="19"><a href="legal.htm"><img src="../images/orange_legal.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="45"><a href="legal.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Legal</a></td>
                <td width="22"><a href="contactenos.htm"><img src="../images/orange_contact.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="75"><div align="left"><a href="contactenos.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Contactenos</a></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table align="center" border="0" width="750">
  <tbody>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">*Tax&oacute;n:</td>
    <td width="575" bgcolor="#e6e6e6" ><span class="txttaxon2"><%=request.getParameter("nombre")%></span>&nbsp;&nbsp;<span class="txttaxon1"><%=request.getParameter("autor")%></span></td>
  </tr>
  <tr align="right">
    <td colspan="2" ><font face="Arial, Helvetica, sans-serif"> <strong> <font color="#ff0000" size="2"> <%=continfoimagen.gettamano()%> </font></strong><a color="#000066" size="2">registro(s)
      encontrado(s) buscar en google <a href="http://images.google.com.co/images?sourceid=navclient-ff&amp;ie=UTF-8&amp;rls=GGGL,GGGL:2005-09,GGGL:es&amp;q=<%=request.getParameter("nombre")%>"><img src="../images/google.gif" width="34" height="13" border="0"></a></font></font></td>
    </tr>
</table>
<table align="center" border="0" width="750">
  <tbody>
    <tr>
      <td><img src="../images/spacer.gif" width="1" height="1"></td>
    </tr>
    <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
    <tr bgcolor="#B5D7DE">
      <td>&nbsp; <img src="../images/arrowx.gif" width="5" height="7">&nbsp; <span class="linksnegros">Imagenes
      asociadas (Hacer click en el link para visualizar la imagen) </span></td>
    </tr>
    <%
      for(l = 0; l < continfoimagen.gettamano(); l++ ){
        infoimagen = new especies.Cinfoimagen();
        infoimagen = continfoimagen.getinfoimagen(l);

        String pal = "";
        if(infoimagen.get_anota() == null)
          pal = request.getParameter("nombre");
        else
          pal = infoimagen.get_anota();
    %>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas"><em><a href="#" onclick="toogleShowHide(parentNode.parentNode.parentNode.parentNode.rows[1]); return false"><%=pal%></a></em></td>
        </tr>
        <tr style="display: none">
          <td class="texttablas"><img src="consIMG2.jsp?clave=<%=request.getParameter("clave")%>&amp;consecutivo=<%=infoimagen.get_aconsecutivo()%>&amp;numero=<%=request.getParameter("numero")%>"></td>
        </tr>
      </table></td>
    </tr>
    <% } %>
</table>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2300620-1";
urchinTracker();
</script>
<%@ include file="sibm_fondo.jsp" %>
