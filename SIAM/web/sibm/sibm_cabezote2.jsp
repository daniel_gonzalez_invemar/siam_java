<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>
<link href="../siamccs.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META name="keywords" content="invemar, ciencia marina, investigac&iacute;on, marino, costera, costero">
<meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">


</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="3"><img name="head_r1_c1" src="../images/head_r1_c1.jpg" width="3" height="81" border="0" alt=""></td>
    <td width="105" bgcolor="#096A95"><img name="head_r1_c1" src="../images/head_r1_c3.gif" border="0" alt=""></td>
    <td width="507" bgcolor="#096A95"><p class="titulohead">Sistema de Informaci&oacute;n
        sobre Biodiversidad<br>
Marina de Colombia - SIBM</p>
    </td>
    <td width="38" align="right" bgcolor="#096A95"><a href="../index.htm"><img name="head_r1_c4" src="../images/head_r1_c4.jpg" width="38" height="81" border="0" alt="Home"></a></td>
    <td width="43" align="right" bgcolor="#096A95"><a href="../contactenos.htm"><img name="head_r1_c5" src="../images/head_r1_c5.jpg" width="43" height="81" border="0" alt="Cont&aacute;ctenos"></a></td>
    <td width="34" align="right" bgcolor="#096A95"><a href="../mapadelsitio.htm"><img name="head_r1_c6" src="../images/head_r1_c6.jpg" width="34" height="81" border="0" alt="Mapa del sitio"></a></td>
    <td colspan="2" align="right" valign="top" bgcolor="#096A95"><img name="head_r1_c7" src="../images/head_r1_c7.jpg" width="4" height="81" border="0" alt=""></td>
  </tr>
  <tr>
    <td width="3"></td>
    <td width="105"></td>
    <td></td>
    <td width="38"></td>
    <td width="43"></td>
    <td width="34"></td>
    <td width="12"></td>
    <td width="8"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
