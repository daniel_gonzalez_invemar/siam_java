<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
  <title>SIBM - Gu&iacute;a del usuario</title>
  <link href="../siamccs.css" rel="stylesheet" type="text/css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <link type='text/css' rel="stylesheet"
        href="../plantillaSitio/css/misiamccs.css"/>
  <style type="text/css">
         a{
          color:#096A95;          
         }
        </style>
 </head>
 <body>
  <jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=34"/>
  <div class="centrado"
       style="width:963px;margin:auto;text-align:left;">
   <ul>
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_00Preliminar.html">Conceptos preliminares (0.5 Mb)</a>
    </li>
     
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_00catalogo02.html"> Informaci&oacute;n contenida en el
                                           catalogo de especies (12 Mb)</a>
    </li>
     
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_00Registrob.html"> Informaci&oacute;n contenida en
                                          los registros biol&oacute;gicos (1.3
                                          Mb)</a>
    </li>
     
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
    <a href="guia/sibm_01home.html" >Servicios
                ofrecidos desde la p&aacute;gina principal (2.5 Mb) </a>

    </li>
     
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_01Resultados.html" >La
                hoja de resultados (21 Mb; 130 s)</a>

    </li>
     
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_02BusquedaSimple.html" >B&uacute;squeda
                sencilla (2.8 Mb; 120 s)</a>

    </li>
     
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
    <a href="guia/sibm_07busquedaAvanza01.html" >B&uacute;squeda
                avanzada (3.0 Mb; 160 s)</a>

    </li>
     
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_08Bespacial.html" >B&uacute;squeda
                espacial (9.2 Mb; 65 s)</a>

    </li>
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_10documentos.html" >Consulta
                    documentos sobre biodiversidad (2.8 
                Mb; 28 s)</a>


    </li>
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_11Estadisticas.html" >Consulta
                    res&uacute;menes estad&iacute;sticos de las colecciones,  los datos
                    almacenados, <br/> distribuci&oacute;n por grupos taxon&oacute;micos
                    de los registros biol&oacute;gicos (21.5 Mb; 230 s)</a>


    </li>
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_14distribuido.html" >Ingreso
                    al Sistema distribuido de acopio de datos 
                (1.5 Mb)</a>


    </li>
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_13Proyectos.html" >Consulta
                    campa&ntilde;as y proyectos con informaci&oacute;n en el SIBM (2.1 Mb; 60 s)</a>


    </li>
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_15ccatalogo.html" >Consulta
                    guiada al cat&aacute;logo de especies (1.6 Mb; 40 s)</a>


    </li>
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_16Investiga.html" >Consulta
                    Investigadores y personas vinculadas a la iniciativa
                    (2.7 Mb; 65 s)</a>


    </li>
    <li>
     <img src="../plantillaSitio/img/videoSibm.png" alt="imagen"
          style="width:32px;height:32px"></img>
     <a href="guia/sibm_19Entidades.html" >Consulta
                    entidades referenciadas en el SIBM (2.0 Mb; 46 s)</a>



    </li>
   </ul>
  </div>
  <%@ include file="../plantillaSitio/footermodulesV3.jsp"%>
 </body>
</html>