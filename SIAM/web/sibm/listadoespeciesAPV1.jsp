<%@ include file="common.jsp" %>
<!--%@ page contentType="text/html;charset=ISO-8859-1"%-->
<%@ page import="java.util.regex.Pattern" %>
<%@ page import="java.util.regex.Matcher" %>
<%!
static HashMap criterios = new HashMap();
static {
  criterios.put("clase", "f.CLASE");
  criterios.put("orden", "f.ORDEN");
  criterios.put("familia", "f.FAMILIA");
  criterios.put("genero", "f.GENERO");
  criterios.put("especie", "f.ESPECIE");
}

static HashMap icriterios=new HashMap();
static{
	 icriterios.put("scientificname", "f.especie");
     icriterios.put("commonname", "f.NOMBRE_COMUN");
	 icriterios.put("genus", "f.GENERO");
}

static HashMap ioperators=new HashMap();
static{
	ioperators.put("begin","like");
	ioperators.put("igual","=");
	ioperators.put("contains","like");
	

}

static HashMap fields = new HashMap();
static {

  fields.put("familia", "familia");
  fields.put("genero", "genero");	
  fields.put("especie", "especie");
  fields.put("vulgar", "nombre_comun");
  fields.put("fechamin", "fechamin");
  fields.put("fechamax", "fechamax");
  fields.put("frecuencia", "frecuencia");
  
}


static List columns = new ArrayList();
static {
  columns.add("PHYLUM");
  columns.add("DIVISION");
  columns.add("FAMILIA");
  columns.add("SECCION");
  columns.add("NOCAT");
  columns.add("NOINV");
  columns.add("ESPECIE");
  columns.add("AUTOR");
  columns.add("NOMBRE_COMUN");
  columns.add("CLAVE");
  
}


%>
<%
ArrayList params = new ArrayList();
String especie = request.getParameter("nespecie");
String genero = param("ngenero", "");
String division = param("phylum", "");
String criterioParam = param("criterio", "");
String criterio = (String)criterios.get(criterioParam);
String valor = param("valor", "");

//Criterios de busquedas de la pagina index.htm
String pcriterio=param("icriterio","");
String icriterio=(String)icriterios.get(pcriterio);
String poperator=param("ioperator","");
String ioperator=(String)ioperators.get(poperator);
String ivalue=param("ivalue","");


String cites = param("ncites", "");
String librorojo = param("nlibrorojo", "");
String catlibrorojo = param("ncatlibrorojo", "");
String ano1 = param("ano1", "");
String ano2 = param("ano2", "");

String lat1 = param("lat1", "");
String lat2 = param("lat2", "");
String lng1 = param("lng1", "");
String lng2 = param("lng2", "");

String prof1 = param("nprof1", "");
String prof2 = param("nprof2", "");
String vulgar = param("vulgar2", "");
String ambiente = param("ambiente", "");
String zona = param("zona", "");
String region = param("nreg", "");
String ecoregion = param("ecoregion", "");
String proyecto=param("proyecto","");
String estacion=param("estacion","");

String otherCriteria=param("othercriterios","");
String otherColumns=param("columns","");

String pages=param("pages","");

boolean hasEspecie = StringUtils.isNotEmpty(especie);
System.out.println("hasEspecie:.. "+hasEspecie);
boolean hasGenero = StringUtils.isNotEmpty(genero);
System.out.println("hasGenero:.. "+hasGenero);
boolean hasDivision = StringUtils.isNotEmpty(division);
boolean hasCriterio = StringUtils.isNotEmpty(criterio);
System.out.println("hasCriterio:.. "+hasCriterio);
boolean hasValor = StringUtils.isNotEmpty(valor);

//verificamos que se tengan criterios de busquedas para la busqueda simple en index.htm
boolean hasICriterio=StringUtils.isNotEmpty(icriterio);
boolean hasIOperator=StringUtils.isNotEmpty(ioperator);
boolean hasIValue=StringUtils.isNotEmpty(ivalue);

boolean hasCites = StringUtils.isNotEmpty(cites);
boolean hasLibrorojo = StringUtils.isNotEmpty(librorojo);
boolean hasCatlibrorojo = StringUtils.isNotEmpty(catlibrorojo);
/*boolean hasAno1 = StringUtils.isNotEmpty(ano1);
boolean hasAno2 = StringUtils.isNotEmpty(ano2);*/


boolean hasLat1 = StringUtils.isNotEmpty(lat1);
boolean hasLat2 = StringUtils.isNotEmpty(lat2);
boolean hasLng1 = StringUtils.isNotEmpty(lng1);
boolean hasLng2 = StringUtils.isNotEmpty(lng2);


boolean hasProf1 = StringUtils.isNotEmpty(prof1);
boolean hasProf2 = StringUtils.isNotEmpty(prof2);
boolean hasVulgar = StringUtils.isNotEmpty(vulgar);
boolean hasAmbiente = StringUtils.isNotEmpty(ambiente);
boolean hasZona = StringUtils.isNotEmpty(zona);
boolean hasRegion = StringUtils.isNotEmpty(region);
boolean hasEcoregion = StringUtils.isNotEmpty(ecoregion);
boolean hasProyecto=StringUtils.isNotEmpty(proyecto);
boolean hasEstacion=StringUtils.isNotEmpty(estacion);

boolean hasOtherCriteria=StringUtils.isNotEmpty(otherCriteria);
boolean hasOtherColumns=StringUtils.isNotEmpty(otherColumns);

if (hasDivision) params.add(division);

if (hasGenero){ 
	params.add("%" + genero + "%");
	params.add("%" + genero + "%");
}

if (hasEspecie)
{ 
	params.add("%" + especie + "%");
	params.add("%" + especie + "%");
}
//parameter de la busqueda simple
if(hasICriterio && hasIOperator && hasIValue){
	
		if(poperator.equalsIgnoreCase("begin")){
			if(!pcriterio.equalsIgnoreCase("commonname")){
				params.add(ivalue+"%");
			}
			
			params.add(ivalue+"%");
		}else if(poperator.equalsIgnoreCase("contains")){
			if(!pcriterio.equalsIgnoreCase("commonname")){
				params.add("%"+ivalue+"%");
			}
			params.add("%"+ivalue+"%");
		}else {
			if(!pcriterio.equalsIgnoreCase("commonname")){
				params.add(ivalue);
			}
			params.add(ivalue);
		}
}


if (hasCriterio && hasValor && !criterio.equalsIgnoreCase("f.clase")) {
	
	params.add("%" + valor + "%");
	params.add("%" + valor + "%");
}else if (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.clase")) {
	params.add("%" + valor + "%");
}


Pattern p = Pattern.compile(",");
Matcher m;
if (hasLat1)
{
	m = p.matcher(lat1);
	lat1=m.replaceAll(".");
	System.out.println("LAT1: "+lat1);
	params.add(Double.parseDouble(lat1));
 }
if (hasLat2){ 
	m = p.matcher(lat2);
	lat2=m.replaceAll(".");
	System.out.println("LAT2: "+lat2);
	
	params.add(Double.parseDouble(lat2));
}
if (hasLng1){
	m = p.matcher(lng1);
	lng1=m.replaceAll(".");
	System.out.println("LNG1: "+lng1);
	
	 params.add(Double.parseDouble(lng1));
}
if (hasLng2){
	m = p.matcher(lng2);
	lng2=m.replaceAll(".");
	System.out.println("LNG2: "+lng2);
	
 	params.add(Double.parseDouble(lng2));
 }
System.out.println("================");

if (hasProf1) params.add(prof1);
if (hasProf2) params.add(prof2);

if (hasVulgar) params.add("%" + vulgar + "%");
if (hasAmbiente) params.add(ambiente);


/*if (hasAno1) params.add(ano1);
if (hasAno2) params.add(ano2);*/


if (hasZona) params.add(zona);

if (hasEcoregion) params.add(ecoregion);

if (hasRegion) params.add(region + "%");

if (hasLibrorojo && !librorojo.equals("N") && hasCatlibrorojo) params.add(catlibrorojo);

if(hasProyecto) params.add(proyecto);
if(hasEstacion) params.add(estacion);

int position = intParam("position", 0);
int length = 20;
String field = param("field", "especie");
boolean reversed = StringUtils.isNotBlank(request.getParameter("reversed"));
if (!fields.containsKey(field)) field = "familia,genero,especie";



String query=" SELECT   f.familia AS familia,f.especie AS nombrecientifico, f.genero, f.nombre_comun,f.UICN_NACIONAL,f.CITES, "+
	         " f.clave AS clave,TO_CHAR(min(f.fechacap),'MM/DD/YYYY') fechamin,TO_CHAR(max(f.fechacap),'MM/DD/YYYY') fechamax, count(1) frecuencia  "
+" FROM VM_FICHAC f "
+" where ROUND(MOD(TRUNC((NODOS_ACTIVOS/1),1),10),0) = 1  "

+ (hasDivision? " AND  f.division = ?": "") 

+ (hasGenero? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "") 

+(hasEspecie? " AND UPPER(f.EPITETO) IN (SELECT vigente FROM (SELECT NULL anterior, especie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT especie, epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")

+ (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.especie") ? " AND UPPER(f.especie) IN (SELECT vigente FROM (SELECT NULL anterior, genero ||' ' || especie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero ||' ' || especie, genero_vigente ||' ' || epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior "+ioperator +" UPPER (?) OR vigente "+ioperator +" UPPER (?))": "") 
+ (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.GENERO") ? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior "+ioperator +" UPPER (?) OR vigente "+ioperator +" UPPER (?))": "") 
+ (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.NOMBRE_COMUN") ?  " AND  UPPER(f.NOMBRE_COMUN) "+ioperator +" UPPER(?)": "") 
/*+ (hasCriterio && hasValor? " AND " + criterio + " like UPPER(?)": "")*/
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.genero") ? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.especie") ? " AND UPPER(f.especie) IN (SELECT vigente FROM (SELECT NULL anterior, genero ||' ' || especie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero ||' ' || especie, genero_vigente ||' ' || epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.orden") ? " AND UPPER (f.orden) IN ( SELECT vigente FROM (SELECT NULL anterior, orden vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT orden, orden_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.familia") ? " AND UPPER (f.familia) IN ( SELECT vigente FROM (SELECT NULL anterior, familia vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT familia, familia_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "") 
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.clase") ? " AND UPPER (f.clase) like UPPER(?)": "")  

  /* + (hasAno1? " AND f.ANOCAPTURA >= ?": "") + (hasAno2? " AND f.ANOCAPTURA <= ?": "")*/ 
 + (hasLat1? " AND f.LATITUD >= ?": "") 
 + (hasLat2? " AND f.LATITUD <= ?": "") 
 + (hasLng1? " AND f.LONGITUD >= ?": "") 
 + (hasLng2? " AND f.LONGITUD <= ?": "")  
  + (hasProf1? " AND f.PROF_CAPTURA >= ?": "") 
  + (hasProf2? " AND f.PROF_CAPTURA <= ?": "") 
  + (hasVulgar? " AND UPPER(f.NOMBRE_COMUN) LIKE UPPER(?)": "") 
  + (hasAmbiente? " AND f.AMBIENTE = ?": "") 
  + (hasZona? " AND f.ZONA = ?": "") 
  //+ (hasAmbiente? " AND f.AMBIENTE = ?": "") 
  //+ (hasZona? " AND f.ZONA = ?": "") 
  + (hasEcoregion? " AND f.ECORREGION LIKE ?": "") 
  + (hasRegion? " AND f.REGION LIKE ?": "")
   + (hasCites? " AND  f.CITES IS "  + (cites.equals("N")? "NULL": "NOT NULL"): "")
   +(hasLibrorojo? " AND f.UICN_NACIONAL IS " + (librorojo.equals("N")? "NULL": "NOT NULL" + (hasCatlibrorojo? " AND substr(f.UICN_NACIONAL,1,2) = ?": "")): "")
   + (hasProyecto? " AND f.proyecto = ?":"")
   + (hasEstacion? " AND f.estacion=?":"")
  + (hasOtherCriteria? " AND "+otherCriteria: "") 
  + " GROUP BY f.clave, f.familia, f.genero, f.especie, f.nombre_comun,f.UICN_NACIONAL,f.CITES "
 + " ORDER BY " + fields.get(field) + " " + (reversed? "DESC": "ASC");
 
System.out.println("QUERY:.. "+query);
System.out.println("PARAMS SIZE:.. "+params.size());
Object[] parameters = params.toArray();
List list = (List)sibm.query(limit(query, position + 1, length), parameters, new MapListHandler(), false);
int total = ((Number)sibm.query(count(query), parameters, one)).intValue();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<head>

<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>

<link href="../siamccs.css" rel="stylesheet" type="text/css">

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content="invemar, ciencia marina, investigaci�n, marino, costera, costero">

<meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC">

<meta http-equiv="pragma" content="no-cache">

<meta http-equiv="cache-control" content="no-cache">

<meta http-equiv="expires" content="0">



<script type="text/javascript" src="../1ibre/prototype.js"></script>

<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>

<script type="text/javascript" src="../1ibre/1ibre.js"></script>

<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>
<script type="text/javascript">
	function ubio(valor){
		
		var link=document.getElementById("link");
		var url="<a id='urlubio' href='http://www.ubio.org/tools/linkit.php?map%5B%5D=all&nb=1&synonyms="+valor+"&url=http://www.invemar.org.co/siam/sibm/sibmuseo.jsp?position=0&field=especie&ngenero=&phylum=ARTHROPODA&criterio=&valor=&ncites=&ncatlibrorojo=&nlibrorojo=&nprof1=&nprof2=&lat1=&lat2=&lng1=&lng2=&ano1=&ano2=&vulgar=&ambiente=&zona=&nreg=&ecoregion='><img src='images/ubioLogo.png' width='64' height='43' border='0'></a>"
		
		link.removeChild(document.getElementById('urlubio'));
		link.innerHTML=url;
	}
	

</script>
 <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css"/>
</head>
<body>
<jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=34"/>

<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="10" bgcolor="#E6E6E6">&nbsp;</td>
    <td width="733" bgcolor="#E6E6E6" class="texttablas"> Descargar listado de especies en formato Microsoft Excel <a href="/siam/sibm/downloadServices?action=DownloadLEspecie&<%=request.getQueryString() %>">Aqui</a></td>
    <td width="10" bgcolor="#E6E6E6">&nbsp;</td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table align="center" border="0" width="100%">
  <tbody>
  <form method="post" action="">
  </form>
  <tr bgcolor="#E6E6E6">
    <td height="13" colspan="10"><div align="right"><font face="Arial, Helvetica, sans-serif"> <strong> <font color="#ff0000" size="2"> <%=total%> </font></strong><font color="#000066" size="2">registro(s)
            encontrado(s)</font></font></div>
    </td>
  </tr>
  <tr>
      <td width="124" align="center" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("familia")? " sorted" + (reversed? " reversed": ""): "" %>" style="text-decoration: none" href="listadoespeciesAPV1.jsp?idsitio=6&idisitio=3&position=<%=position%>&amp;field=familia<%= !reversed && field.equals("familia")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>">
      <span class="titulostablas">Familia</span>
    </td>
    <td width="75" align="center" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("genero")? " sorted" + (reversed? " reversed": ""): "" %>" style="text-decoration: none" href="listadoespeciesAPV1.jsp?idsitio=6&idisitio=3position=<%=position%>&amp;field=lote<%= !reversed && field.equals("lote")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>">
      <span class="titulostablas">Genero</span>
    </td>
    <td width="297" align="center" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("especie")? " sorted" + (reversed? " reversed": ""): "" %>"
     style="text-decoration: none" href="listadoespeciesAPV1.jsp?idsitio=6&idsubsitio=3&position=<%=position%>&amp;field=especie<%= !reversed && field.equals("especie")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>">
      <span class="titulostablas">Nombre cient&iacute;fico</span>
    </td>
    <td width="225" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("vulgar")? " sorted" + (reversed? " reversed": ""): "" %>" 
    
    style="text-decoration: none" href="listadoespeciesAPV1.jsp?idsitio=6&idsubsitio=3&position=<%=position%>&amp;field=vulgar<%= !reversed && field.equals("vulgar")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>">
      <span class="titulostablas">Nombre Com&uacute;n</span>
    </td >
    <td width="128" align="center" bgcolor="#B5D7DE" class="titulostablas" style="text-decoration: none" >
      <span class="titulostablas">UICN Nacional</span>
    </td>
    <td width="128" align="center" bgcolor="#B5D7DE" class="titulostablas" style="text-decoration: none" >
      <span class="titulostablas">Cites</span>
    </td>
     <td width="225" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("fechamin")? " sorted" + (reversed? " reversed": ""): "" %>" 
     style="text-decoration: none" href="listadoespeciesAPV1.jsp?idsitio=6&idsubsitio=3&position=<%=position%>&amp;field=fechamin<%= !reversed && field.equals("fechamin")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>">
      <span class="titulostablas">Primera fecha de captura</span>
    </td >
    <td width="225" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("fechamax")? " sorted" + (reversed? " reversed": ""): "" %>" 
    style="text-decoration: none" href="listadoespeciesAPV1.jsp?idsitio=6&idsubsitio=3&position=<%=position%>&amp;field=fechamax<%= !reversed && field.equals("fechamax")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>">
      <span class="titulostablas">&Uacute;ltima fecha de captura</span>
    </td >
       <td width="225" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("frecuencia")? " sorted" + (reversed? " reversed": ""): "" %>" 
       style="text-decoration: none" href="listadoespeciesAPV1.jsp?idsitio=6&idsubsitio=3&position=<%=position%>&amp;field=frecuencia<%= !reversed && field.equals("frecuencia")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>">
      <span class="titulostablas">Frecuencia</span>
    </td >
     <td width="128" align="center" bgcolor="#B5D7DE" class="titulostablas" style="text-decoration: none" >
      <span class="titulostablas">Sinonimias</span>
    </td>
  </tr>
  <%
    
      for (int i = 0; i < list.size(); i++) {
        Map map = (Map)list.get(i);
        
    %>
  <tr valign="top" bgcolor="#E6E6E6">
    
    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("familia"))%></td>
    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("genero"))%></td>
    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("nombrecientifico"))%></td>
    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("nombrecomun"))%></td>
    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("uicnnacional"))%></td>
    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("cites"))%></td>
    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("fechamin"))%></td>    
    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("fechamax"))%></td>
     <td class="texttablas"><%=(Number)map.get("frecuencia")%></td>   
    <td class="texttablas"><a href="javascript:(function(){requestPopup('sinonimos.jsp?clave=<%=StringUtils.defaultString((String)map.get("clave"))%>&specie=<%=StringUtils.defaultString((String)map.get("nombrecientifico"))%>').showPopup();})()">Ver Sinonimias</a></td>   
    
  </tr>
  <% } %>
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    	<div align="center" class="linksnavinferior">
    		<% if(position > 0){ %>
    		<a href="listadoespeciesAPV1.jsp?idsitio=6&idsubsitio=3&position=<%=Math.max(0, position - length)%>&amp;field=<%=field%><%=reversed? "&amp;reversed=1": ""%>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>" class="linksnavinferior">&lt;&lt;</a> <a href="listadoespecies.jsp?position=<%=Math.max(0, position - length)%>&amp;field=<%=field%><%=reversed? "&amp;reversed=1": ""%>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;othercriterios=<%=URLEncoder.encode(otherCriteria, "UTF-8")%>&amp;columns=<%=URLEncoder.encode(otherColumns, "UTF-8")%>&amp;pages=<%=pages%>" class="linksnavinferior">Anterior</a> -<% } %>
    P&aacute;gina <%=position / length + 1%> de <%=total / length + 1%> <% if(total > position + length){ %>- <a href="listadoespeciesAPV1.jsp?idsitio=6&idsubsitio=3&position=<%=position + length%>&amp;field=<%=field%><%=reversed? "&amp;reversed=1": ""%>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;othercriterios=<%=StringEscapeUtils.escapeHtml(otherCriteria)%>&amp;columns=<%=StringEscapeUtils.escapeHtml(otherColumns)%>&amp;pages=<%=pages%>" class="linksnavinferior">Siguiente</a> <a href="listadoespecies.jsp?position=<%=position + length%>&amp;field=<%=field%><%=reversed? "&amp;reversed=1": ""%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>&amp;othercriterios=<%=URLEncoder.encode(otherCriteria, "UTF-8")%>&amp;columns=<%=URLEncoder.encode(otherColumns, "UTF-8")%>&amp;pages=<%=pages%> " class="linksnavinferior">&gt;&gt;</a><% } %> </div></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
</table>
<%@ include file="sibm_fondoPV1.jsp" %>
 
 <table align="center">
 <tr>
 <td><%@ include file="../plantillaSitio/footermodulesV3.jsp" %>
 </td>
 </tr>
 </table>
