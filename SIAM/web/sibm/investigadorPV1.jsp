<%@ include file="../common.jsp" %>
<%
String param = request.getParameter("id");
String field;
if (param == null) {
  field = "NOMBRE_CO || ' ' || APELLIDOS_CO";
  param = request.getParameter("nombre");
}
else field = "CODIGO_CO";
response.setContentType("text/html");
Map investigador = (Map)sibm.query("SELECT DISTINCT NOMBRE_CO || ' ' || APELLIDOS_CO AS nombre, TITULO_CO AS titulo, INTERES_CO AS interes, NOMBRE_PA AS nacionalidad, GRADOU_CO AS estudio, INSTITUTO_CO AS instituto, CARGO_CO AS cargo, SECCION_CO AS seccion, CORREO_CO AS correo, TELEFONO_CO AS telefono, FAX_CO AS fax, CVLAC AS cvlat FROM CDIRCOLECTOR, CPAISES WHERE NACIONALIDAD_CO = CODIGO_PA(+) AND " + field + " = ?", new Object[] {param}, row, false);
if (investigador == null) {
  investigador = new HashMap();
  investigador.put("nombre", param);
}
%>
<jsp:include page="../plantillaSitio/headermodulos.jsp?idsitio=34"/>
<tr style="padding:20px; border-color:Blue; border-width:thin;">
<td style="padding:5px;">
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#8DC0E3">
   <td width="21"><img src="../images/arrow2.gif" width="20" height="20"></td>
   <td width="729" bgcolor="#8DC0E3" class="linksnegros"><%= investigador.get("nombre") %></td>
    <td width="290" align="right">&nbsp;</td>

  </tr>
</table>

<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table align="center" border="0" width="963px">
  <tbody>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">Nombre:</td>
    <td width="575" bgcolor="#e6e6e6" class="txttaxon2"><%= investigador.get("nombre") %></td>
  </tr>
  <tr>
    <td colspan="2" class="txttaxon1"><img src="../images/spacer.gif" width="1" height="1"></td>
    </tr>
</table>
<table align="center" border="0" width="963px">
  <tbody>
    <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
    <tr>
      <td colspan="3" bgcolor="#B5D7DE">&nbsp; <img src="../images/arrowx.gif" width="5" height="7">&nbsp; <span class="linksnegros">Detalles</span></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">T&iacute;tulo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("titulo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Inter&eacute;s</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("interes")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Nacionalidad</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("nacionalidad")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Estudios</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("estudio")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Instituto</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><a href="institutoPV1.jsp?id=<%= escape(investigador.get("instituto")) %>"><%= escape(investigador.get("instituto")) %></a></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Cargo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("cargo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Secci&oacute;n</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("seccion")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Correo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("correo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Tel&eacute;fono</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("telefono")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Fax</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("fax")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="165"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Curriculum vitae para latinoam&eacute;rica y el caribe - CVLAC</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4">
            <% String cvlat = (String)investigador.get("cvlat"); if (StringUtils.isNotBlank(cvlat)) { %>
              <a href="http://garavito.colciencias.gov.co/pls/curriculola/gn_imprime.visualiza_cvlac?f_check=DUMMY&amp;f_check=0&amp;f_check=1&amp;f_check=2&amp;f_check=3&amp;f_check=4&amp;f_check=7&amp;f_check=5&amp;f_check=6&amp;f_check=15&amp;f_check=16&amp;f_check=8&amp;f_check=9&amp;f_check=10&amp;f_check=12&amp;f_check=17&amp;f_check=13&amp;f_check=11&amp;f_check=14&amp;f_check=21&amp;f_check=19&amp;f_check=20&amp;f_tpo=P&amp;f_fmt=H&amp;f_padrao=A&amp;f_plv=0&amp;f_setor=0&amp;f_area=0&amp;f_per_atu=0&amp;f_ano_atu=&amp;f_inf=1&amp;f_cit=1&amp;f_per_prod=0&amp;f_ano_prod=&amp;f_cod=<%= cvlat %>" target="_blank">Ver</a>
            <% } %>
          </td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#ffffff"><div align="right"><a href="#top" class="linksbotton">Volver arriba</a> </div></td>
    </tr>

</table>

<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
   <td bgcolor="#B5D7DE"><img src="../images/spacer.gif" width="2" height="2"></td>
  </tr>


</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#E6E6E6">
    <td colspan="4"><img src="../images/spacer.gif" width="3" height="3"></td>
  </tr>
  <tr>
    <td width="8" bgcolor="#B5D7DE">&nbsp;</td>
    <td width="103" align="left" valign="top" bgcolor="#B5D7DE" class="linksnegros"><table width="100" border="0" align="left" cellpadding="2" cellspacing="2">
        <tr>
          <td class="linksnegros">Miembros de: </td>
        </tr>
      </table>
    </td>
    <td width="155" bgcolor="#B5D7DE"><div align="center">
      <table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.siac.net.co/Home.php" target="_blank"><img src="../images/banner_sib.gif" width="115" height="49" border="0"></a></td>
          </tr>
        </table>
    </div></td>
    <td width="484" bgcolor="#B5D7DE"><div align="center">
      <table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.iobis.org/" target="_blank"><img src="../images/banner_obis.gif" width="150" height="43" border="0"></a></td>
          </tr>
        </table>
    </div></td>
    <td width="484" bgcolor="#B5D7DE"><div align="center">
      <table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.iabin.net/" target="_blank"><img src="../images/iabin.gif" alt="IABIN" border="0"></a></td>
          </tr>
      </table>
    </div></td>

  </tr>
</table>
</tr>
</td>
  <%@ include file="../plantillaSitio/footermodules.jsp" %>