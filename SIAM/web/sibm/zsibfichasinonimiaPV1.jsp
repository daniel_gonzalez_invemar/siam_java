<%@ include file="common.jsp" %>
<%@ page import="especies.*" %>

<%
  /*Cconexsib con1 = null;
  Connection conn1 = null;
  con1 = new Cconexsib();
  conn1 = con1.getConn();*/
  Connection conn1 = sibmDS.getConnection();

  especies.Cinfosinonimia infosinonimia = new especies.Cinfosinonimia();
  especies.Ccontinfosinonimia continfosinonimia = new especies.Ccontinfosinonimia();
  continfosinonimia.contenedor(conn1, request.getParameter("clave"));

  /*if (con1!= null)
    con1.close();*/
  DbUtils.close(conn1);  


  int l= 0;

%>
<jsp:include page="../plantillaSitio/headermodulos.jsp?idsitio=34"/>
<tr style="padding:20px; border-color:Blue; border-width:thin;">
<td style="padding:5px;">
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
    <td width="21"><img src="../images/arrow2.gif" width="20" height="20"></td>
    <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td width="326" align="left" class="linksnegros">Sinonimias </td>
          <td width="403" align="right">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table align="center" border="0" width="963px">
  <tbody>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">*Tax&oacute;n:</td>
    <td width="575" bgcolor="#e6e6e6"> <span class="txttaxon2"><%=request.getParameter("nombre")%></span>&nbsp;&nbsp;<span class="txttaxon1"><%=request.getParameter("autor")%></span></td>


  </tr>
  <tr align="right">
    <td colspan="2" ><font face="Arial, Helvetica, sans-serif"> <strong> <font color="#ff0000" size="2"> <%=continfosinonimia.gettamano()%> </font></strong><font color="#000066" size="2">registro(s)
      encontrado(s)</font></font></td>
    </tr>
</table>
<table align="center" border="0" width="963px">
  <tbody>
    <tr>
      <td colspan="5"><img src="../images/spacer.gif" width="1" height="1"></td>
    </tr>
    <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
    <tr bgcolor="#B5D7DE">
      <td colspan="5">&nbsp; <img src="../images/arrowx.gif" width="5" height="7">&nbsp; <span class="linksnegros">Sinonimias
          registradas</span></td>
    </tr>
    <%
      for(l = 0; l < continfosinonimia.gettamano(); l++ ){
        infosinonimia = new especies.Cinfosinonimia();
        infosinonimia = continfosinonimia.getinfosinonimia(l);
    %>
    <tr bgcolor="#ffffff">
      <td width="356" valign="top" bgcolor="#F2F2F2"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td ><span class="txttaxon2"><%=infosinonimia.get_anombre()%></span>&nbsp;&nbsp;&nbsp;<span class="txttaxon1"><%=infosinonimia.get_aautor()%></span></td>
          </tr>
        </table>
      </td>
      <td colspan="2" valign="top" class="texttablas">
	    <table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="txttaxon4">
			<%if(infosinonimia.get_anotas() == null) infosinonimia.setanotas("");%><%=infosinonimia.get_anotas()%>
			</td>
          </tr>
        </table>
      </td>
      <td width="76" colspan="2" valign="top" class="texttablas">
	    <table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="txttaxon4">
<div align="center"><a href="zsibficharefbibPV1.jsp?clave=<%=request.getParameter("clave")%>&amp;nombre=<%=request.getParameter("nombre")%>&amp;fichamuseo=<%=false%>&amp;referencia=<%=infosinonimia.get_abibliografia()%>&amp;phylum=<%=infosinonimia.get_aphylum()%>&amp;autor=<%=request.getParameter("autor")%>">Bibliograf&iacute;a</a></div>
		    </td>
          </tr>
        </table>
      </td>

    </tr>
    <% } %>
    <tr bgcolor="#ffffff">
      <td colspan="3"><div align="right"><a href="#top" class="linksbotton">Volver arriba</a> </div></td>
    </tr>

</table>
 <%@ include file="sibm_fondoPV1.jsp" %>
</tr>
</td>
<%@ include file="../plantillaSitio/footermodules.jsp" %>