<%@ include file="common.jsp" %>
<%@ page language="java" import="especies.*"%>
<%@ include file="sibm_cabezote.jsp" %>
<%
  /*Cconexsib con1 = null;
  Connection conn1 = null;
  con1 = new Cconexsib();
  conn1 = con1.getConn();*/
  Connection conn1 = sibmDS.getConnection();

  especies.Cinfoficha infoficha = new especies.Cinfoficha();
  infoficha.setaclave(request.getParameter("clave"));
  infoficha.cargainfoficha(conn1);

  /*if (con1!= null)
    con1.close();*/
  //conn1.close();
  DbUtils.close(conn1);  


  if(infoficha.get_acoloracion() == null)
    infoficha.setacoloracion("");
  if(infoficha.get_adescripcion() == null)
    infoficha.setadescripcion("");
  if(infoficha.get_adimensiones() == null)
    infoficha.setadimensiones("");
  if(infoficha.get_adiagnosis() == null)
    infoficha.setadiagnosis("");
  if(infoficha.get_anotas() == null)
    infoficha.setanotas("");
  if(infoficha.get_ahabitat() == null)
    infoficha.setahabitat("");
  if(infoficha.get_asustrato() == null)
    infoficha.setasustrato("");
  if(infoficha.get_aprofundidadmin() == null)
    infoficha.setaprofundidadmin("");
  if(infoficha.get_aprofundidadmax() == null)
    infoficha.setaprofundidadmax("");
  if(infoficha.get_adistglobal() == null)
    infoficha.setadistglobal("");
  if(infoficha.get_adistlocal() == null)
    infoficha.setadistlocal("");
  if(infoficha.get_agenerales() == null)
    infoficha.setagenerales("");
  if(infoficha.get_ataxonomicos() == null)
    infoficha.setataxonomicos("");

  int l= 0;

%>
<script type="text/javascript">

var pagina = 'http://siam.invemar.org.co/siam/sibm/zsibficha2PV1.jsp';
document.location.href=pagina;
</script>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#096A95">
    <td colspan="2" class="titulo_ubicacion"><img src="../images/spacer.gif" width="5" height="5"><a href="../index.htm" class="titulo_ubicacion">Inicio</a> &gt; <a href="../siam.htm" class="titulo_ubicacion">SIAM</a> &gt; <a href="index.htm" class="titulo_ubicacion">Sistema
      de Informaci&oacute;n sobre Biodiversidad Marina</a> &gt; Datos de especie</td>
  </tr>
  <tr>
    <td colspan="2"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
    <td width="21"><img src="../images/arrow2.gif" width="20" height="20"></td>
    <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td width="345" align="left" class="linksnegros">Datos de especie</td>
          <td width="384" align="right"><table border="0" cellpadding="0" cellspacing="0" width="320">
              <tr>
                <td width="17"><a href="index.htm"><img src="../images/orange_home.gif" width="17" height="17" border="0"></a></td>
                <td width="4">&nbsp;</td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="75"><a href="index.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Inicio
                    SIBM</a></td>
                <td width="18"><a href="legal.htm"><img src="../images/orange_legal.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="44"><a href="legal.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Legal</a></td>
                 <td width="18"><a href="index_guia_sibm.htm"><img src="../images/help.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="48"><a href="index_guia_sibm.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Ayuda</a></td>
                <td width="17"><a href="contactenos.htm"><img src="../images/orange_contact.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="79"><div align="left"><a href="contactenos.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Contactenos</a></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table align="center" border="0" width="750">
  <tbody>

  <tr>
    <td bgcolor="#B5D7DE" class="txttaxon1">*Phylum:</td>
    <td width="575" bgcolor="#e6e6e6" class="txttaxon3"><%=request.getParameter("nomphylum")%> </td>
  </tr>
  <tr>
    <td bgcolor="#B5D7DE" class="txttaxon1">*Tax&oacute;n:</td>
    <td bgcolor="#e6e6e6"><span class="txttaxon2"><%=request.getParameter("nombre")%></span>&nbsp;&nbsp;<span class="txttaxon1"><%=request.getParameter("autor")%></span></td>
  </tr>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">M&aacute;s informaci&oacute;n...</td>
    <td bgcolor="#e6e6e6" class="texttablas">
    <a href="zsibfichafilo.jsp?clave=<%=request.getParameter("clave")%>&amp;nombre=<%=request.getParameter("nombre")%>&amp;autor=<%=request.getParameter("autor")%>">Taxonom&iacute;a &gt;&gt;</a> <br>
	  <a href="zsibfichasinonimia.jsp?clave=<%=request.getParameter("clave")%>&amp;nombre=<%=request.getParameter("nombre")%>&amp;autor=<%=request.getParameter("autor")%>">Sinonimias &gt;&gt;</a> <br>
    
      <a href="zsibficharefbib.jsp?clave=<%=request.getParameter("clave")%>&amp;nombre=<%=request.getParameter("nombre")%>&amp;fichamuseo=<%=false%>&amp;referencia=<%=request.getParameter("referencia")%>&amp;phylum=null&amp;autor=<%=request.getParameter("autor")%>">Referencias
      bibliograficas &gt;&gt;</a> <br>
                <a href="zsibfichaimagen.jsp?clave=<%=request.getParameter("clave")%>&amp;nombre=<%=request.getParameter("nombre")%>&amp;numero=<%=request.getParameter("numero")%>&amp;autor=<%=request.getParameter("autor")%>">Im&aacute;genes &gt;&gt;</a><br>
                <a href="zsibfichaconserv.jsp?clave=<%=request.getParameter("clave")%>&amp;nombre=<%=request.getParameter("nombre")%>&amp;autor=<%=request.getParameter("autor")%>">Datos
                de conservaci&oacute;n&gt;&gt;</a> <br>
<font color="#999999">
		<a href="javascript:(function(){requestPopup('autorficha.jsp?clave=<%=request.getParameter("clave")%>').showPopup();})();" >Autores de la ficha &gt;&gt; </a></font>
		</br>
				<a href="javascript:location.href='formespacial.htm?phylum=<%=request.getParameter("nomphylum")%>&amp;criterio=especie&amp;valor=<%=request.getParameter("nombre")%>'">Distribuci&oacute;n espacial registros biol&oacute;gico &gt;&gt;</a> <br>
</td>
  </tr>
  <tr>
    <td colspan="2"></td>
    </tr>
  <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
  <!-- FIN DEL BLOQUE QUE GENERA LA FILOGENIA -->
</table>
<table align="center" border="0" width="750">
  <tbody>
    <tr>
      <td colspan="3"><img src="../images/spacer.gif" width="1" height="1"></td>
    </tr>
    <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
    <tr bgcolor="#B5D7DE">
      <td colspan="3">&nbsp; <img src="../images/arrowx.gif" width="5" height="7">&nbsp; <span class="linksnegros">Datos
          relacionados</span></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td width="165" valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="texttablas">Coloraci&oacute;n:</td>
          </tr>
        </table>
      </td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="txttaxon4"><%=infoficha.get_acoloracion()%></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Descripci&oacute;n:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_adescripcion()%></td>
        </tr>
      </table></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Dimensiones:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_adimensiones()%></td>
        </tr>
      </table></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Diagn&oacute;sis:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_adiagnosis()%></td>
        </tr>
      </table></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Notas ecol&oacute;gicas:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_anotas()%></td>
        </tr>
      </table></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Habitat:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_ahabitat()%></td>
        </tr>
      </table></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Sustrato:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_asustrato()%></td>
        </tr>
      </table></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Profundidad m&iacute;nima:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_aprofundidadmin()%></td>
        </tr>
      </table></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Profundidad m&aacute;xima:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_aprofundidadmax()%></td>
        </tr>
      </table></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Distribuci&oacute;n global:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_adistglobal()%></td>
        </tr>
      </table></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Distribuci&oacute;n local:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_adistlocal()%></td>
        </tr>
      </table></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Comentarios generales:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_agenerales()%></td>
        </tr>
      </table></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Comentarios taxon&oacute;micos:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_ataxonomicos()%></td>
        </tr>
      </table></td>
    </tr>

</table>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2300620-1";
urchinTracker();
</script>
<%@ include file="sibm_fondo.jsp" %>