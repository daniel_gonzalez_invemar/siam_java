var map = null;
var markers = [];
var icons = {};
var defaultIcon = new GIcon(null);
defaultIcon.image = "../images/icon.png";
defaultIcon.shadow = "../images/sombra.png";
defaultIcon.iconSize = new GSize(12, 20);
defaultIcon.shadowSize = new GSize(22, 20);
defaultIcon.iconAnchor = new GPoint(5, 19);
defaultIcon.infoWindowAnchor = new GPoint(5, 2);
//defaultIcon.infoShadowAnchor = new GPoint(0, 0);
var iconPoint = new GIcon(null);
iconPoint.image = "../images/point.png";
iconPoint.shadow = "../images/point.png";
iconPoint.iconSize = new GSize(7, 7);
iconPoint.shadowSize = new GSize(7, 7);
iconPoint.iconAnchor = new GPoint(3, 3);
iconPoint.infoWindowAnchor = new GPoint(3, 3);
//iconPoint.infoShadowAnchor = new GPoint(0, 0);
var mLeftTop = null;
var mCenterTop = null;
var mRightTop = null;
var mLeftMiddle = null;
var mCenterMiddle = null;
var mRightMiddle = null;
var mLeftBottom = null;
var mCenterBottom = null;
var mRightBottom = null;
function makeTable(lista) {
  var html = "<table style='font-size: 12px' cellpadding='1'><tbody>";
  for (var i = 0; i < lista.length; i++) {
    var item = lista[i];
    html += "<tr><td style='font-weight: bold'>" + item[0] + "</td><td>" + (item[1] != null? item[1]: "<span style='font-style: italic'>No&nbsp;Asignado</span>") + "</td></tr>";
  }
  return html + "</tbody></table><br>";
}
function load(ds) {
  map = new GMap2($("map"), {mapTypes: [G_NORMAL_MAP, G_HYBRID_MAP, G_SATELLITE_MAP]});
  map.addControl(new GLargeMapControl(), new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(10, 10)));
  map.addControl(new GOverviewMapControl());
  map.addControl(new GMapTypeControl(), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(10, 10)));
  map.addControl(new GScaleControl());
  //map.enableDoubleClickZoom();
  map.enableContinuousZoom();
  //map.enableScrollWheelZoom();
  map.setCenter(new GLatLng(11, -75), 5);
  showArea();
  var baseIcon = new GIcon(null);
  //baseIcon.image = "../img/icon.png";
  baseIcon.shadow = "../images/sombra.png";
  baseIcon.iconSize = new GSize(12, 20);
  baseIcon.shadowSize = new GSize(22, 20);
  baseIcon.iconAnchor = new GPoint(5, 19);
  baseIcon.infoWindowAnchor = new GPoint(5, 2);
  //baseIcon.infoShadowAnchor = new GPoint(0, 0);
  ds.datasource.divisiones.each(function (division) {
  	icon=new GIcon(baseIcon);
	icon.image =  "../images/icons/"+division+".png";
	
    icons[division] = icon;
	
		
  });
  
  var pphylum=ibre.params.phylum;
  var pcriterio=ibre.params.criterio;
  var pcatlibrorojo=ibre.params.ncatlibrorojo;
  var plibrorojo=ibre.params.nlibrorojo;
  var pprof1=ibre.params.nprof1;
  var pprof2=ibre.params.nprof2;
  var plat1=ibre.params.lat1;
  var plat2=ibre.params.lat2;
  var plng1=ibre.params.lng1;
  var plng2=ibre.params.lng2;
  var pano1=ibre.params.ano1;
  var pano2=ibre.params.ano2;
  var pvulgar=ibre.params.vulgar2;
  var pambiente=ibre.params.ambiente;
  var pzona=ibre.params.zona;
  var preg=ibre.params.nreg;
  var pecoregion=ibre.params.ecoregion;
  var pproyecto=ibre.params.proyecto;
  var pstacion=ibre.params.estacion;
  var pothercriterios=ibre.params.othercriterios
  var pcolumns=ibre.params.columns
  if (pphylum || pcriterio || pcatlibrorojo || plibrorojo || pprof1 || pprof2 || plat1 || plat2 || plng1 || plng2 || pano1 || pano2 || pvulgar || pambiente || pzona || preg || pecoregion || pproyecto || pstacion || pothercriterios || pcolumns) 
  		f.whatwgSubmit();
}
function setArea(m1, m2) {
  var p1 = m1.getPoint();
  var p2 = m2.getPoint();
  setAreaLatLng(p1.lat(), p2.lat(), p1.lng(), p2.lng())
}
function setAreaLatLng(lat1, lat2, lng1, lng2) {
  setInputArea(Math.min(lat1, lat2), Math.max(lat1, lat2), Math.min(lng1, lng2), Math.max(lng1, lng2));
}
function showInfoMarker(item) {
  var investigadores = item.identificado_por? item.identificado_por.split(", "): [];
  var htmls = [];
  for (var i = 0; i < investigadores.length; i++)
    htmls.push("<a href='investigador.jsp?nombre=" + escape(investigadores[i]) + "' target='_blank'>" + investigadores[i] + "</a>");
  this.openInfoWindowTabsHtml([
    new GInfoWindowTab("Especie", makeTable([
      ["Especie", "<a href='zsibficha2.jsp?clave=" + item.clave + "&phyl=&niv=&esp=null&nombre=" + escape(item.especie) + "&phylum=&nomphylum=" + escape(item.division) + "&autor=" + escape(item.autores) + "' target='_blank'>" + item.especie +" "+ item.autores+ "</a>"],
      ["Reino", item.reino],
      ["Divisi\xF3n", item.division],
      ["Clase", item.clase],
      ["Orden", item.orden],
      ["Familia", item.familia]
    ])),
    new GInfoWindowTab("General", makeTable([
      ["\xDAltimo Cambio", item.ultimocambio],
      ["C\xF3digo Colecci\xF3n", item.codigocoleccion],
      ["N\xFAmero Catalogo", item.numerocatalogo],
      ["A\xF1o de Captura", item.anocaptura],
      ["Profundidad de Captura", item.prof_captura],
      ["Lugar", (item.localidad? item.localidad + " ": "") + (item.depto? item.depto: "")],
      ["Latitud", item.latitud],
      ["Longitud", item.longitud]
    ])),
    new GInfoWindowTab("Investigador", makeTable([
      ["Identificado por", htmls.join(", ")],
      ["Colectado por", item.colectado_por]
    ]))
  ]);
}
function showInfoMarkers(items) {
  var html = "<table style='font-size: 12px' cellpadding='1'><tbody>";
  for (var i = 0; i < items.length; i++) {
    var item = items[i];
    html += "<tr><td><a href='javascript: showInfoMarker.bind(markers[" + this.index + "], ibre.datasource.values.datos.items[" + item.index + "])()'>" + (item.especie? item.especie: (item.genero? item.genero: (item.familia? item.familia: (item.orden? item.orden: (item.clase? item.clase: (item.division? item.division: "No identificado")))))) + "</a></td></tr>";
  }
  html + "</tbody></table><br>";
  this.openInfoWindowHtml(html);
}
function render() {
  for (var i = 0; i < markers.length; i++) map.removeOverlay(markers[i]);
  var items = ibre.datasource.values.datos.items;
  if (!items.length) return;
  var minLatitud, maxLatitud, minLongitud, maxLongitud;
  minLatitud = minLongitud = maxLatitud = maxLongitud = null;
  markers = [];
  resMarkers = [];
  var listLatLng = {};
  for (var i = 0; i < items.length; i++) {
    var item = items[i];
    item.index = i;
    var latitud = item.latitud;
    var longitud = item.longitud;
    if (latitud == null || longitud == null) continue;
    if (listLatLng[latitud + "_" + longitud] != null) {
      var index = listLatLng[latitud + "_" + longitud];
      var listMarker = resMarkers[index];
      if (!(listMarker instanceof Array)) {
        var oldItem = listMarker;
        listMarker = [];
        resMarkers[index] = listMarker;
        listMarker.push(oldItem);
        listMarker.especie = oldItem.especie;
        listMarker.division = oldItem.division;
      }
      listMarker.push(item);
      if (listMarker.especie && item.especie != listMarker.especie)
        listMarker.especie = null;
      if (listMarker.division && item.division != listMarker.division)
        listMarker.division = null;
      continue;
    }
    listLatLng[latitud + "_" + longitud] = resMarkers.length;
    resMarkers.push(item);
  }
  var item, title, handler, param, icon;
  for (var i = 0; i < resMarkers.length; i++) {
    var resMarker = resMarkers[i];
    if (resMarker instanceof Array) {
      item = resMarker[0];
      if (resMarker.especie) title = resMarker.especie + " (" + resMarker.length + " registros)";
      else title = resMarker.length + " registros";
      handler = showInfoMarkers;
      param = resMarker;
      icon = icons[resMarker.division]? icons[resMarker.division]: defaultIcon;
    }
    else {
      item = resMarker;
      title = item.especie;
      handler = showInfoMarker;
      param = item;
      icon = icons[item.division]? icons[item.division]: defaultIcon;
    }
    var latitud = item.latitud;
    var longitud = item.longitud;
    var marker = new GMarker(new GLatLng(latitud, longitud), {title: title, icon: icon/*defaultIcon*/});
    if (minLatitud == null || latitud < minLatitud) minLatitud = latitud;
    if (maxLatitud == null || latitud > maxLatitud) maxLatitud = latitud;
    if (minLongitud == null || longitud < minLongitud) minLongitud = longitud;
    if (maxLongitud == null || longitud > maxLongitud) maxLongitud = longitud;
    markers[i] = marker;
    marker.index = i;
    GEvent.addListener(marker, "click", handler.bind(marker, param));
    map.addOverlay(marker);
  }
  if (pol) {
    var sw = mLeftBottom.getPoint();
    var ne = mRightTop.getPoint();
    /*if (sw.lat() < minLatitud) */minLatitud = sw.lat();
    /*if (sw.lng() < minLongitud) */minLongitud = sw.lng();
    /*if (ne.lat() < maxLatitud) */maxLatitud = ne.lat();
    /*if (ne.lng() < maxLongitud) */maxLongitud = ne.lng();
  }
  map.setCenter(new GLatLng((minLatitud + maxLatitud) / 2, (minLongitud + maxLongitud) / 2));
  var zoomLevel = map.getBoundsZoomLevel(new GLatLngBounds(new GLatLng(minLatitud, minLongitud), new GLatLng(maxLatitud, maxLongitud)));
  map.setZoom(zoomLevel < 10 ? zoomLevel: 10);
}
function guiones(codigo) {
  codigo = "" + codigo;
  var res = "";
  for (var i = codigo.length; i > 2; i--) if (codigo.charAt(i) != 0) break;
  for (var j = 0; j < i - 3; j++) res += "-";
  return res;
}
var pol = null;
function clearArea() {
  
  
  if (pol) {
    map.removeOverlay(pol);
    map.removeOverlay(mLeftTop);
    map.removeOverlay(mCenterTop);
    map.removeOverlay(mRightTop);
    map.removeOverlay(mLeftMiddle);
    map.removeOverlay(mCenterMiddle);
    map.removeOverlay(mRightMiddle);
    map.removeOverlay(mLeftBottom);
    map.removeOverlay(mCenterBottom);
    map.removeOverlay(mRightBottom);
    pol = null;
  }
 
  f.reset();
 
  $('othercriterios').value="";
  
}
function showArea() {
  if (pol) {
    map.removeOverlay(pol);
    map.removeOverlay(mLeftTop);
    map.removeOverlay(mCenterTop);
    map.removeOverlay(mRightTop);
    map.removeOverlay(mLeftMiddle);
    map.removeOverlay(mCenterMiddle);
    map.removeOverlay(mRightMiddle);
    map.removeOverlay(mLeftBottom);
    map.removeOverlay(mCenterBottom);
    map.removeOverlay(mRightBottom);
    pol = null;
  }
  var lat1 = $FF(f.lat1);
  var lat2 = $FF(f.lat2);
  var lng1 = $FF(f.lng1);
  var lng2 = $FF(f.lng2);
  if (isNaN(lat1) || isNaN(lat2) || isNaN(lng1) || isNaN(lng2) || lat1 > lat2 || lng1 > lng2 || lat1 < -90 || lat2 > 90 || lng1 < -180 || lng2 > 180) return;
  pol = new GPolyline([
    new GLatLng(lat1, lng1),
    new GLatLng(lat1, lng2),
    new GLatLng(lat2, lng2),
    new GLatLng(lat2, lng1),
    new GLatLng(lat1, lng1)
  ], "#FF0000", 1, 1);
  map.addOverlay(pol);
  var lat3 = (lat1 + lat2) / 2;
  var lng3 = (lng1 + lng2) / 2;
  mLeftTop = new GMarker(new GLatLng(0, 0), {
    icon: iconPoint,
    title: "Redimensionar \xE1rea",
    draggable: true,
    dragCrossMove: true
  });
  mCenterTop = mLeftTop.copy();
  mRightTop = mLeftTop.copy();
  mLeftMiddle = mLeftTop.copy();
  mCenterMiddle = new GMarker(new GLatLng(0, 0), {
    icon: iconPoint,
    title: "Mover \xE1rea",
    draggable: true,
    dragCrossMove: true
  });
  mRightMiddle = mLeftTop.copy();
  mLeftBottom = mLeftTop.copy();
  mCenterBottom = mLeftTop.copy();
  mRightBottom = mLeftTop.copy();
  mLeftTop.setPoint(new GLatLng(lat2, lng1));
  mCenterTop.setPoint(new GLatLng(lat2, lng3));
  mRightTop.setPoint(new GLatLng(lat2, lng2));
  mLeftMiddle.setPoint(new GLatLng(lat3, lng1));
  mCenterMiddle.setPoint(new GLatLng(lat3, lng3));
  mRightMiddle.setPoint(new GLatLng(lat3, lng2));
  mLeftBottom.setPoint(new GLatLng(lat1, lng1));
  mCenterBottom.setPoint(new GLatLng(lat1, lng3));
  mRightBottom.setPoint(new GLatLng(lat1, lng2));
  GEvent.addListener(mLeftTop, "dragend", setArea.params(mLeftTop, mRightBottom));
  GEvent.addListener(mCenterTop, "dragend", function () {
    setAreaLatLng($FF(f.lat1), mCenterTop.getPoint().lat(), $FF(f.lng1), $FF(f.lng2));
  });
  GEvent.addListener(mRightTop, "dragend", setArea.params(mLeftBottom, mRightTop));
  GEvent.addListener(mLeftMiddle, "dragend", function () {
    setAreaLatLng($FF(f.lat1), $FF(f.lat2), mLeftMiddle.getPoint().lng(), $FF(f.lng2));
  });
  GEvent.addListener(mCenterMiddle, "dragend", function () {
    var p1 = mLeftBottom.getPoint();
    var p2 = mRightTop.getPoint();
    var p3 = mCenterMiddle.getPoint();
    var lat = p3.lat();
    var lng = p3.lng();
    var latD = (p2.lat() - p1.lat()) / 2;
    var lngD = (p2.lng() - p1.lng()) / 2;
    setAreaLatLng(lat - latD, lat + latD, lng - lngD, lng + lngD);
  });
  GEvent.addListener(mRightMiddle, "dragend", function () {
    setAreaLatLng($FF(f.lat1), $FF(f.lat2), $FF(f.lng1), mRightMiddle.getPoint().lng());
  });
  GEvent.addListener(mLeftBottom, "dragend", setArea.params(mLeftBottom, mRightTop));
  GEvent.addListener(mCenterBottom, "dragend", function () {
    setAreaLatLng(mCenterBottom.getPoint().lat(), $FF(f.lat2), $FF(f.lng1), $FF(f.lng2));
  });
  GEvent.addListener(mRightBottom, "dragend", setArea.params(mLeftTop, mRightBottom));
  map.addOverlay(mLeftTop);
  map.addOverlay(mCenterTop);
  map.addOverlay(mRightTop);
  map.addOverlay(mLeftMiddle);
  map.addOverlay(mCenterMiddle);
  map.addOverlay(mRightMiddle);
  map.addOverlay(mLeftBottom);
  map.addOverlay(mCenterBottom);
  map.addOverlay(mRightBottom);
}
function showRect() {
  var bounds = map.getBounds();
  var sw = bounds.getSouthWest();
  var ne = bounds.getNorthEast();
  var lat = sw.lat();
  var lng = sw.lng();
  var latD = (ne.lat() - lat) / 3;
  var lngD = (ne.lng() - lng) / 3;
  setInputArea(lat + latD, lat + 2 * latD, lng + lngD, lng + 2 * lngD);
}
function setInputArea(lat1, lat2, lng1, lng2) {
  f.lat1.value = lat1;
  f.lat2.value = lat2;
  f.lng1.value = lng1;
  f.lng2.value = lng2;
  showArea();
}
function validateMinMax(name) {
  var min = parseInt($F(f[name + "1"]));
  var max = parseInt($F(f[name + "2"]));
  return isNaN(min) || isNaN(max) || min <= max;
}
function kgsmapper() {
  open("http://hercules.kgs.ku.edu/website/Specimen_Mapper/mxmapit.cfm?xmlsource=" + escape(ibre.path + "kgsmapper.jsp?" + Form.serialize(f)), "kgsmapper");
}
function csquares() {
  open("csquares.jsp?" + Form.serialize(f), "csquares");
}


/*
function tabular() {
//!$F(f.phylum)|| !$F(f.criterio) || !$F(f.vulgar2) || !$F(f.ncites) || !$F(f.nlibrorojo) || !$F(f.ncatlibrorojo) || !$F(f.nprof1) || !$F(f.nprof2)
var campo =$F(f.nprof2);
 if (!$F(f.phylum)&& !$F(f.criterio) && !$F(f.vulgar2) && !$F(f.ncites) && !$F(f.nlibrorojo) && !$F(f.ncatlibrorojo) && !$F(f.nprof1) && !$F(f.nprof2) && !$F(f.lat1) && !$F(f.lat2) && !$F(f.lng1) && !$F(f.lng2) && !$F(f.ano1) && !$F(f.ano2) && !$F(f.ambiente) && !$F(f.zona) && !$F(f.nreg) && !$F(f.ecoregion) && !$F(f.proyecto) && !$F(f.estacion) ) {
	 var msg="Debe indicar por lo menos un campo ";
    alert(msg);
    f.phylum.focus();
    return;
  }
  var old = f.action;
  
  
  
  f.action = "sibmuseo.jsp";
  var datasource = f.getAttribute("datasource");
  f.removeAttribute("datasource");
  f.whatwgSubmit();
  f.action = old;
  f.setAttribute("datasource", datasource);
}*/