<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>
<link href="../siamccs.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="keywords" content="invemar, ciencia marina, investigacion, marino, costera, costero">
<meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">


<script type="text/javascript" src="jscript/mootools-1.2-core.js"></script>
<script type="text/javascript" src="jscript/documents.js"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script type="text/javascript">

var pagina = 'http://siam.invemar.org.co/siam/sibm/documentsPV1.jsp';
document.location.href=pagina;
</script>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="3"><img name="head_r1_c1" src="../images/head_r1_c1.jpg" width="3" height="81" border="0" alt=""></td>
    <td width="105" bgcolor="#096A95"><img name="head_r1_c1" src="../images/head_r1_c3.gif" border="0" alt=""></td>
    <td width="507" bgcolor="#096A95"><p class="titulohead">Sistema de Informaci&oacute;n
        sobre Biodiversidad<br>
Marina de Colombia - SIBM</p>
    </td>
    <td width="38" align="right" bgcolor="#096A95"><a href="../index.htm"><img name="head_r1_c4" src="../images/head_r1_c4.jpg" width="38" height="81" border="0" alt="Home"></a></td>
    <td width="43" align="right" bgcolor="#096A95"><a href="../contactenos.htm"><img name="head_r1_c5" src="../images/head_r1_c5.jpg" width="43" height="81" border="0" alt="Cont&aacute;ctenos"></a></td>
    <td width="34" align="right" bgcolor="#096A95"><a href="../mapadelsitio.htm"><img name="head_r1_c6" src="../images/head_r1_c6.jpg" width="34" height="81" border="0" alt="Mapa del sitio"></a></td>
    <td colspan="2" align="right" valign="top" bgcolor="#096A95"><img name="head_r1_c7" src="../images/head_r1_c7.jpg" width="4" height="81" border="0" alt=""></td>
  </tr>
  <tr>
    <td width="3"></td>
    <td width="105"></td>
    <td></td>
    <td width="38"></td>
    <td width="43"></td>
    <td width="34"></td>
    <td width="12"></td>
    <td width="8"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#096A95">
    <td colspan="2" class="titulo_ubicacion"><img src="../images/spacer.gif" width="5" height="5"><a href="../index.htm" class="titulo_ubicacion">Inicio</a> &gt; <a href="../siam.htm" class="titulo_ubicacion">SIAM</a> &gt; <a href="index.htm" class="titulo_ubicacion">Sistema
      de Informaci&oacute;n sobre Biodiversidad Marina</a> &gt; Documentos</td>
  </tr>
  <tr>
    <td colspan="2"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
    <td width="21"><img src="../images/arrow2.gif" width="20" height="20"></td>
    <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td width="371" align="left" class="linksnegros">Documentos dobre biodiversidad<br></td>
          <td width="358" align="right"><table border="0" cellpadding="0" cellspacing="0" width="320">
              <tr>
                <td width="17"><a href="index.htm"><img src="../images/orange_home.gif" width="17" height="17" border="0"></a></td>
                <td width="4">&nbsp;</td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="75"><a href="index.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Inicio
                    SIBM</a></td>
                <td width="18"><a href="legal.htm"><img src="../images/orange_legal.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="44"><a href="legal.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Legal</a></td>
                 <td width="18"><a href="index_guia_sibm.htm"><img src="../images/help.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="48"><a href="index_guia_sibm.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Ayuda</a></td>
                <td width="17"><a href="contactenos.htm"><img src="../images/orange_contact.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="79"><div align="left"><a href="contactenos.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Contactenos</a></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">

  <tr>
  	<td bgcolor="#E6E6E6">&nbsp;</td>
    <td height="16" bgcolor="#E6E6E6">
    <div align="left" class="linksnavinferior">
	    			<form method="get" action="fichaCatagoloService" id="formproys">
	    				<input type="hidden" name="action" value="Proyecto" />
	    				<table cellpadding="0" border="0">
	    				  	
	    				<tr><td><span class="linksnegros">Seleccione tem&aacute;tica</span></td>
	    				<td>
	    					
	    					<select name="topics" id="topics" >
	    						<option>-------------</option> 
	    					</select>
	    					
	    				</td>
	    				</tr></table>
	    				 
	    			</form>
	    				
		    </div></td>
		<td bgcolor="#E6E6E6">&nbsp;</td>
  </tr>
  
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="10" bgcolor="#B5D7DE">&nbsp;</td>
    <td width="733" bgcolor="#B5D7DE" class="texttablas">
    	&Uacute;ltimos documentos publicados
    </td>
    <td width="10" bgcolor="#B5D7DE">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td bgcolor="#E6E6E6">&nbsp;</td>
    <td bgcolor="#E6E6E6" class="texttablas" id="ldoc"></td>
    <td bgcolor="#E6E6E6"></td>
  </tr>
  
 </table> 
 <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="10" bgcolor="#B5D7DE">&nbsp;</td>
    <td width="733" bgcolor="#B5D7DE" class="texttablas">Documentos 
    </td>
    <td width="10" bgcolor="#B5D7DE">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td bgcolor="#E6E6E6">&nbsp;</td>
    <td bgcolor="#E6E6E6" class="texttablas" id="docs"></td>
    <td bgcolor="#E6E6E6"></td>
  </tr>
  
 </table> 
 <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>

