<%@ include file="../common.jsp" %>
<%
String param = request.getParameter("id");
String field;
if (param == null) {
  field = "NOMBRE_CO || ' ' || APELLIDOS_CO";
  param = request.getParameter("nombre");
}
else field = "CODIGO_CO";
response.setContentType("text/html");
Map investigador = (Map)sibm.query("SELECT DISTINCT NOMBRE_CO || ' ' || APELLIDOS_CO AS nombre, TITULO_CO AS titulo, INTERES_CO AS interes, NOMBRE_PA AS nacionalidad, GRADOU_CO AS estudio, INSTITUTO_CO AS instituto, CARGO_CO AS cargo, SECCION_CO AS seccion, CORREO_CO AS correo, TELEFONO_CO AS telefono, FAX_CO AS fax, CVLAC AS cvlat FROM CDIRCOLECTOR, CPAISES WHERE NACIONALIDAD_CO = CODIGO_PA(+) AND " + field + " = ?", new Object[] {param}, row, false);
if (investigador == null) {
  investigador = new HashMap();
  investigador.put("nombre", param);
}
%>
<html>
<head>
<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../siamccs.css" rel="stylesheet" type="text/css">
<meta name="keywords" content="Sistema de Informaci&oacute;n Ambiental Territorial del Pac&iacute;fico Colombiano, biolog&iacute;a, bases de datos, metadatos, SIAT, corales, Pac&iacute;fico, Colombia, calidad ambiental, REDCAM, INVEMAR, IIAP">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script type="text/javascript">

var pagina = 'http://siam.invemar.org.co/siam/sibm/investigadorPV1.jsp';
document.location.href=pagina;
</script>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="3"><img name="head_r1_c1" src="../images/head_r1_c1.jpg" width="3" height="81" border="0" alt=""></td>
    <td width="105" bgcolor="#096A95"><img name="head_r1_c1" src="../images/head_r1_c3.gif" border="0" alt=""></td>
    <td width="507" bgcolor="#096A95"><p class="titulohead">Sistema de Informaci&oacute;n
        Ambiental<br>
Marina de Colombia - SIAM</p>
    </td>
    <td width="38" align="right" bgcolor="#096A95"><a href="../index.htm"><img name="head_r1_c4" src="../images/head_r1_c4.jpg" width="38" height="81" border="0" alt="Home"></a></td>
    <td width="43" align="right" bgcolor="#096A95"><a href="../contactenos.htm"><img name="head_r1_c5" src="../images/head_r1_c5.jpg" width="43" height="81" border="0" alt="Cont&aacute;ctenos"></a></td>
    <td width="34" align="right" bgcolor="#096A95"><a href="../mapadelsitio.htm"><img name="head_r1_c6" src="../images/head_r1_c6.jpg" width="34" height="81" border="0" alt="Mapa del sitio"></a></td>
    <td colspan="2" align="right" valign="top" bgcolor="#096A95"><img name="head_r1_c7" src="../images/head_r1_c7.jpg" width="4" height="81" border="0" alt=""></td>
  </tr>
  <tr>
    <td width="3"></td>
    <td width="105"></td>
    <td></td>
    <td width="38"></td>
    <td width="43"></td>
    <td width="34"></td>
    <td width="12"></td>
    <td width="8"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#096A95">
    <td colspan="3" class="titulo_ubicacion"><img src="../images/spacer.gif" width="5" height="5"><a href="../index.htm" class="titulo_ubicacion">Inicio</a> &gt; <a href="../siam.htm" class="titulo_ubicacion">SIAM</a> &gt; <a href="index.htm" class="titulo_ubicacion">SIBM</a> &gt; <a href="investigadores.htm" class="titulo_ubicacion">Listado de investigadores</a> &gt; <%= investigador.get("nombre") %></td>
  </tr>
  <tr>
    <td colspan="3"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
   <td width="21"><img src="../images/arrow2.gif" width="20" height="20"></td>
   <td width="729" bgcolor="#8DC0E3" class="linksnegros"><%= investigador.get("nombre") %></td>
    <td width="290" align="right"><table border="0" cellpadding="0" cellspacing="0" width="320">
          <tr>
            <td width="17"><a href="index.htm"><img src="../images/orange_home.gif" width="17" height="17" border="0"></a></td>
            <td width="4">&nbsp;</td>
            <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="75"><a href="index.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Inicio
                SIBM</a></td>
            <td width="18"><a href="legal.htm"><img src="../images/orange_legal.gif" width="17" height="17" border="0"></a></td>
            <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="44"><a href="legal.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Legal</a></td>
             <td width="18"><a href="index_guia_sibm.htm"><img src="../images/help.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="48"><a href="index_guia_sibm.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Ayuda</a></td>
            <td width="17"><a href="contactenos.htm"><img src="../images/orange_contact.gif" width="17" height="17" border="0"></a></td>
            <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="79"><div align="left"><a href="contactenos.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Contactenos</a></div>
            </td>
          </tr>
        </table></td>

  </tr>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table align="center" border="0" width="750">
  <tbody>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">Nombre:</td>
    <td width="575" bgcolor="#e6e6e6" class="txttaxon2"><%= investigador.get("nombre") %></td>
  </tr>
  <tr>
    <td colspan="2" class="txttaxon1"><img src="../images/spacer.gif" width="1" height="1"></td>
    </tr>
</table>
<table align="center" border="0" width="750">
  <tbody>
    <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
    <tr>
      <td colspan="3" bgcolor="#B5D7DE">&nbsp; <img src="../images/arrowx.gif" width="5" height="7">&nbsp; <span class="linksnegros">Detalles</span></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">T&iacute;tulo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("titulo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Inter&eacute;s</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("interes")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Nacionalidad</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("nacionalidad")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Estudios</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("estudio")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Instituto</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><a href="instituto.jsp?id=<%= escape(investigador.get("instituto")) %>"><%= escape(investigador.get("instituto")) %></a></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Cargo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("cargo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Secci&oacute;n</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("seccion")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Correo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("correo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Tel&eacute;fono</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("telefono")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="120"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Fax</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= escape(investigador.get("fax")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="165"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Curriculum vitae para latinoam&eacute;rica y el caribe - CVLAC</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4">
            <% String cvlat = (String)investigador.get("cvlat"); if (StringUtils.isNotBlank(cvlat)) { %>
              <a href="http://garavito.colciencias.gov.co/pls/curriculola/gn_imprime.visualiza_cvlac?f_check=DUMMY&amp;f_check=0&amp;f_check=1&amp;f_check=2&amp;f_check=3&amp;f_check=4&amp;f_check=7&amp;f_check=5&amp;f_check=6&amp;f_check=15&amp;f_check=16&amp;f_check=8&amp;f_check=9&amp;f_check=10&amp;f_check=12&amp;f_check=17&amp;f_check=13&amp;f_check=11&amp;f_check=14&amp;f_check=21&amp;f_check=19&amp;f_check=20&amp;f_tpo=P&amp;f_fmt=H&amp;f_padrao=A&amp;f_plv=0&amp;f_setor=0&amp;f_area=0&amp;f_per_atu=0&amp;f_ano_atu=&amp;f_inf=1&amp;f_cit=1&amp;f_per_prod=0&amp;f_ano_prod=&amp;f_cod=<%= cvlat %>" target="_blank">Ver</a>
            <% } %>
          </td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#ffffff"><div align="right"><a href="#top" class="linksbotton">Volver arriba</a> </div></td>
    </tr>

</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
   <td bgcolor="#B5D7DE"><img src="../images/spacer.gif" width="2" height="2"></td>
  </tr>


</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#E6E6E6">
    <td colspan="4"><img src="../images/spacer.gif" width="3" height="3"></td>
  </tr>
  <tr>
    <td width="8" bgcolor="#B5D7DE">&nbsp;</td>
    <td width="103" align="left" valign="top" bgcolor="#B5D7DE" class="linksnegros"><table width="100" border="0" align="left" cellpadding="2" cellspacing="2">
        <tr>
          <td class="linksnegros">Miembros de: </td>
        </tr>
      </table>
    </td>
    <td width="155" bgcolor="#B5D7DE"><div align="center">
      <table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.siac.net.co/Home.php" target="_blank"><img src="../images/banner_sib.gif" width="115" height="49" border="0"></a></td>
          </tr>
        </table>
    </div></td>
    <td width="484" bgcolor="#B5D7DE"><div align="center">
      <table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.iobis.org/" target="_blank"><img src="../images/banner_obis.gif" width="150" height="43" border="0"></a></td>
          </tr>
        </table>
    </div></td>
    <td width="484" bgcolor="#B5D7DE"><div align="center">
      <table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.iabin.net/" target="_blank"><img src="../images/iabin.gif" alt="IABIN" border="0"></a></td>
          </tr>
      </table>
    </div></td>

  </tr>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><div align="center"><a href="../index.htm" class="textfooter">Sistema de
          Informaci&oacute;n Ambiental Marina</a> <span class="textfooter">-</span> <a href="../legal.htm" class="textfooter">Condiciones
          de acceso y uso</a> <span class="textfooter">-</span> <span class="textfooter">Instituciones
          participantes:</span> <a href="http://www.invemar.org.co" class="textfooter">INVEMAR</a></div>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2300620-1";
urchinTracker();
</script>

</body>
</html>