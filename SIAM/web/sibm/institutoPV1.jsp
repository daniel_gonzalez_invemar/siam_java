<%@ include file="../common.jsp" %>
<%
response.setContentType("text/html");
Map instituto = (Map)sibm.query("SELECT DISTINCT NOMBRE_IN AS nombre, CODIGO_IN AS codigo, DIRECCION_IN AS direccion, CIUDAD_IN AS ciudad, DPTO_IN AS departamento, NOMBRE_PA AS nacionalidad, CORREOE_IN AS correo, WEBSITE_IN AS web, POSTAL_IN AS postal, AP_AEREO_IN AS apartado, TELEFONO_IN AS telefono, FAX_IN AS fax, TITULOD_IN AS titulo, DIRECTOR_IN AS director, OFICINA_IN AS oficina, CONTACTONAME_IN AS contacto, CARGOCONTACTO_IN AS cargo FROM CDIRINSTITUTO, CPAISES WHERE PAIS_IN = CODIGO_PA(+) AND CODIGO_IN = ?", new Object[] {request.getParameter("id")}, row, false);
%>
<jsp:include page="../plantillaSitio/headermodulos.jsp?idsitio=30"/>
<tr style="padding:20px; border-color:Blue; border-width:thin;">
<td style="padding:5px;">
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#8DC0E3">
   <td width="20"><img src="../images/arrow2.gif" width="20" height="20"></td>
   <td width="342" bgcolor="#8DC0E3" class="linksnegros"><%= instituto.get("nombre") %></td>
    <td width="388" align="right">&nbsp;</td>

  </tr>
</table>

<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table align="center" border="0" width="963px">
  <tbody>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">Nombre:</td>
    <td width="575" bgcolor="#e6e6e6" class="txttaxon2"><%= instituto.get("nombre") %></td>
  </tr>
  <tr>
    <td colspan="2" class="txttaxon1"><img src="../images/spacer.gif" width="1" height="1"></td>
    </tr>
</table>
<table align="center" border="0" width="963px">
  <tbody>
    <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
    <tr>
      <td colspan="3" bgcolor="#B5D7DE">&nbsp; <img src="../images/arrowx.gif" width="5" height="7">&nbsp; <span class="linksnegros">Detalles</span></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="165"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">C&oacute;digo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("codigo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Direcci&oacute;n</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("direccion")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Ciudad</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("ciudad")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Departamento</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("departamento")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Pa&iacute;s</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("nacionalidad")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Sitio Web</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><a href="http://<%= StringUtils.defaultString((String)instituto.get("web")) %>"><%= StringUtils.defaultString((String)instituto.get("web")) %></a></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">C&oacute;digo postal</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("postal")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Apartado Aereo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("apartado")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Correo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("correo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Tel&eacute;fono</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("telefono")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Fax</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("fax")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">T&iacute;tulo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("titulo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Director</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("director")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Oficina</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("oficina")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Contacto</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("contacto")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Cargo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("cargo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#ffffff"><div align="right"><a href="#top" class="linksbotton">Volver arriba</a> </div></td>
    </tr>

</table>

<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
   <td bgcolor="#B5D7DE"><img src="../images/spacer.gif" width="2" height="2"></td>
  </tr>


</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#E6E6E6">
    <td colspan="4"><img src="../images/spacer.gif" width="3" height="3"></td>
  </tr>
  <tr>
    <td width="8" bgcolor="#B5D7DE">&nbsp;</td>
    <td width="103" align="left" valign="top" bgcolor="#B5D7DE" class="linksnegros"><table width="100" border="0" align="left" cellpadding="2" cellspacing="2">
        <tr>
          <td class="linksnegros">Miembros de: </td>
        </tr>
      </table>
    </td>
    <td width="155" bgcolor="#B5D7DE"><div align="center">
      <table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.siac.net.co/Home.php" target="_blank"><img src="../images/banner_sib.gif" width="115" height="49" border="0"></a></td>
          </tr>
        </table>
    </div></td>
    <td width="484" bgcolor="#B5D7DE"><div align="center">
      <table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.iobis.org/" target="_blank"><img src="../images/banner_obis.gif" width="150" height="43" border="0"></a></td>
          </tr>
        </table>
    </div></td>
    <td width="484" bgcolor="#B5D7DE"><div align="center">
      <table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.iabin.net/" target="_blank"><img src="../images/iabin.gif" alt="IABIN" border="0"></a></td>
          </tr>
      </table>
    </div></td>

  </tr>
</table>
</tr>
</td>
  <%@ include file="../plantillaSitio/footermodules.jsp" %>