<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
    <head>
        <title>.::Sistema de Informaci&oacute;n Ambiental Marina:: SIBM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="keywords"  content="invemar, ciencia marina, investigacion, marino, costera, costero" />
        <meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />

        <link type="application/jsonrequest" href="totales.jsp" rel="datasource" name="totales" />
        <script type="text/javascript" src="../1ibre/prototype.js"></script>
        <script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>

        <script type="text/javascript" src="../1ibre/1ibre.js"></script>
        <script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>

        <script type="text/javascript" src="jscript/validate.js"></script>
        <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css"/>
        <script type="text/javascript">


            function spacial() {

                var fr = $('advancedSearch');
                var old = fr.action;



                fr.action = "formespacial.jsp";
                fr.submit();
                fr.action = old;

            }

        </script>

        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-19778914-31']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>


    </head>
    <body>
        <jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=34"/>

        <div class="centrado">
            <table width="963px"  style="border:0px;padding:0px;;border-spacing:0px">
                <tr style="background-color:#8DC0E3">
                    <td><table width="958" style="border:0px;padding:00" class="migadepan">
                            <tr>
                                <td style="text-align:center;color:#FFF">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table>
        </div>

        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td></td>
                </tr> 
                <tr>
                    <td></td>
                </tr>
            </table>
        </div>

        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
                    <td width="733" style="background-color:#B5D7DE" class="texttablas">	ES: B&uacute;squeda de registros biol&oacute;gicos a nivel de organismo. Principalmente: esponjas, corales, camarones y cangrejos, estrellas de mar, bryozoos, peces.<strong>El sistema no contiene informaci&oacute;n de anfibios,aves,reptiles, mam&iacute;feros</strong><br/> 
                        EN: Search for biological record to the individuals level. Mainly: sponges, corals, shrimps and crabs, starfish, Bryozoa, fishes.<strong>El system does not contain information of amphibians, birds, reptiles, mammals</strong> </td>
                    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td style="background-color:#E6E6E6">&nbsp;</td>
                    <td style="background-color:#E6E6E6">
                        <form action="sibmuseoPV1.jsp" method="get" style="margin: 0px"    onsubmit="return valSearchSimple();" >
                            <input type="hidden" name="idsitio" value="6"/>
                            <input type="hidden" name="idsubsitio" value="9"/>
                            <div class="centrado">
                                <table width="438" style="border:0px;padding:0;border-spacing:0">
                                    <tr>
                                        <td width="212">&nbsp;</td>
                                        <td width="212"><div style="text-align:center" class="texttablas"><br/></div>          </td>
                                        <td width="226"><div style="text-align:center" class="texttablas"><br/></div></td>
                                        <td width="226"><div style="text-align:center" class="texttablas"><br/></div></td>
                                        <td width="226"><div style="text-align:center" class="texttablas"><br/></div></td>
                                        <td width="226"><div style="text-align:center" class="texttablas"><br/></div></td>
                                        <td width="226"><div style="text-align:center" class="texttablas"><br/></div></td>
                                    </tr>
                                    <tr style="background-color:#FFFFFF">
                                        <td colspan="7"><div style="text-align:center"><img src="../images/spacer.gif" width="2" height="1" alt="separador" /></div>          </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><div style="text-align:center" >
                                                <select id="icriterio" name="icriterio" class="inputText2" title="Nivel taxon�mico">
                                                    <option value="scientificname">Nombre Cient&iacute;fico/Scientific Name</option>
                                                    <option value="commonname">Nombre Com&uacute;n/Common Name</option>
                                                    <option value="genus">Genero/Genus</option>
                                                </select>
                                            </div>          </td>
                                        <td style="background-color:#E6E6E6">&nbsp;</td>
                                        <td>
                                            <div>
                                                <select id="ioperator" name="ioperator" class="inputText2" title="Precisi�n B�squeda">
                                                    <option value="begin">comienza por/begins with</option>
                                                    <option value="igual">igual/is in full</option>
                                                    <option value="contains">contiene/contains</option>
                                                </select>
                                            </div>          </td> 
                                        <td style="background-color:#E6E6E6">&nbsp;</td> 

                                        <td><div style="text-align:center">
                                                <input name="ivalue" type="text" class="inputText2" id="ivalue" title="Palabra clave"/>
                                            </div>          </td>
                                        <td style="background-color:#E6E6E6">&nbsp;</td>
                                        <td height="30" colspan="2"><div style="text-align:center">
                                                <input name="submitBuscar" type="submit" value="Buscar/Search"/>
                                            </div></td>
                                        <td><img src="../images/xmag_small.png" style="vertical-align:middle;border:0px" alt="buscar" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" style="background-color:#FFFFFF"></td>
                                    </tr>
                                    <tr><td style="background-color:#E6E6E6" colspan="7">&nbsp;</td> </tr>
                                </table></div>
                        </form>



                    </td>
                    <td style="background-color:#E6E6E6">&nbsp;</td>
                </tr>
            </table></div>

        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
                    <td width="733" style="background-color:#B5D7DE" class="texttablas">&nbsp;<br/></td>
                    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td style="background-color:#E6E6E6">&nbsp;</td>
                    <td style="background-color:#E6E6E6">
                        <div class="centrado">
                            <table width="100%" style="border:0px;padding:0px;border-spacing:0px">
                                <tr>
                                    <td width="599" height="30" style="text-align:center;vertical-align:middle"><div class="centrado" style="text-align:center">

                                            <form id="advancedSearch" action="form.jsp" style="margin: 0px">
                                                <div class="centrado">
                                                    <table width="411" style="border:0px;padding:0px;border-spacing:1px">
                                                        <tr>
                                                            <td width="37%"><strong>Buscar/Search&nbsp;&nbsp;</strong></td>
                                                            <td width="5%"><img src="../images/find.png" style="vertical-align:middle;border:0px" alt="busqueda"/></td>
                                                            <td width="28%">
                                                                <input value="Avanzada / Advanced" type="submit" />
                                                            </td>
                                                            <td width="23%">
                                                                <input value="Espacial / Spatial"  type="button" onclick="spacial()" />
                                                            </td>
                                                            <td width="7%"><div style="text-align:left"><a href="formespacial.jsp" style="margin-bottom:4px"><img src="../images/pknetwork.png" style="vertical-align:middle;border:0px" alt="pknetwork"/></a></div></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            </table></div>
                    </td>
                    <td style="background-color:#E6E6E6">&nbsp;</td>
                </tr>
            </table></div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td></td>
                </tr>
            </table></div>
        <div class="centrado">
            <table style="border:0px;padding:0px;border-spacing:0px" width="963px">
                <tr>
                    <td style="background-color:#b5d7de" width="10">&nbsp;</td>
                    <td style="background-color:#b5d7de" width="733" class="texttablas">ES: Para registros sobre grupos taxon�micos y datos complementarios  de 
                        inter&eacute;s no incluidos en el sistema.
                        <br/>
                        EN: Other taxa included in another  systems.</td>
                    <td style="background-color:#b5d7de" width="10">&nbsp;</td>
                </tr>  
                <tr>
                    <td style="background-color:#e6e6e6">&nbsp;</td>
                    <td style="background-color:#e6e6e6">

                        <table style="border:0px;padding:0px;border-spacing:0px" width="100%">
                            <tbody><tr>
                                    <td height="30" width="599" style="vertical-align:middle;">
                                        <table style="border:0px;padding:0px;border-spacing:1px" width="100%">
                                            <tr>
                                                <td width="100%"><img height="7" width="5" src="../images/arrowx.gif" style="vertical-align: middle;" alt="flecha" />Aves: <a target="_blank" href="http://ebird.org/ebird/colombia/eBirdReports?cmd=Start" class="linksnegros"><span class="linksnegros">Red Nacional de Observadores de Aves de Colombia</span></a> </td>                    
                                            </tr>
                                            <tr>
                                                <td width="100%"><img height="7" width="5" src="../images/arrowx.gif" style="vertical-align: middle;" alt="flecha"/>Otros: <a target="_blank" href="http://www.biovirtual.unal.edu.co/ICN/" class="linksnegros"><span class="linksnegros">Instituto de Ciencias Naturales</span></a> </td>                    
                                            </tr>                  
                                        </table>
                                    </td>
                                </tr>
                            </tbody></table>
                    </td>
                    <td style="background-color:#e6e6e6">&nbsp;</td>
                </tr>
            </table></div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
                    <td width="733" style="background-color:#B5D7DE" class="texttablas">
                        <img src="../images/folder_documents.png" alt="documentos sobre biodiversidad" style="vertical-align:middle;border:0px"/><img style="vertical-align: middle;" src="../images/arrowx.gif" width="5" height="7" alt="flecha"/>	
                        <a href="documentsPV1.jsp?idsitio=6&amp;idsubsitio=5" class="linksnegros"><span class="linksnegros">Documentos sobre biodiversidad </span> </a>
                    </td>
                    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
                </tr>  
            </table>
        </div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td></td>
                </tr> 
                <tr>
                    <td></td>
                </tr>
            </table></div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
                    <td width="733" style="background-color:#B5D7DE" class="texttablas"><a href="estadisticas/indexPV1.jsp?idsitio=6&amp;idsubsitio=6"><img src="../images/statistics.png" alt="resumen estadistico de las colleciones" style="vertical-align:middle;border:0"/></a>
                        <img src="../images/arrowx.gif" width="5" height="7" alt="flecha" /><a href="estadisticas/indexPV1.jsp?idsitio=6&amp;idsubsitio=6" class="linksnegros" > <span class="linksnegros">Resumenes estad&iacute;sticos  de las colecciones, proyectos y/o conjuntos de referencia almacenados en el SIBM </span></a> </td>
                    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
                </tr>  
            </table></div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table></div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
                    <td width="733" style="background-color:#B5D7DE" class="texttablas"><a href="http://cinto.invemar.org.co/argos/login.jsp" ><img src="../images/distributed.png" alt="sistema distribuido de recopilacion de registros biologicos" style="vertical-align:middle;border:0" /></a>
                        <img src="../images/arrowx.gif" width="5" height="7" alt="flecha" /><a href="http://cinto.invemar.org.co/argos/login.jsp" class="linksnegros" > <span class="linksnegros">	Sistema distribuido de recopilaci&oacute;n de registros biol&oacute;gicos</span> </a> </td>
                    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
                </tr>  
            </table></div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
                    <td width="733" style="background-color:#B5D7DE" class="texttablas"><a href="#"><img src="../images/navigator.png" alt="campa&ntilde;as y proyectos con informaci&oacute;n de SIBM"  style="vertical-align:middle;border:0" /></a>
                        <img src="../images/arrowx.gif" width="5" height="7" alt="flecha" /><a href="./fichaCatagoloService?action=Proyecto&amp;position=0&amp;length=1&amp;cdo=1" class="linksnegros" > <span class="linksnegros">Campa&ntilde;as y proyectos con informaci&oacute;n en el SIBM</span></a> </td>
                    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
                </tr>  
            </table>
        </div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr style="background-color:#E6E6E6">
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td style="background-color:#E6E6E6">&nbsp;</td>
                    <td style="background-color:#E6E6E6"><table style="border:0px;padding:0px;border-spacing:0px" width="100%">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td colspan="2" class="texttablas">Informaci&oacute;n
                                    disponible / Available data:</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td width="11">&nbsp;</td>
                                <td width="704">Total
                                    registros biol&oacute;gicos disponibles /
                                    Biological records availables: <span style="color:#FF0000">`totales.lotes`</span></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>Informaci&oacute;n
                                    proveniente de lotes almacenados en el MHNMC / Records from specimens
                                    of the MHNMC: <span style="color:#FF0000">`totales.lotesMuseo`</span></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td style='font-color:'>N&uacute;mero de especies descritas /number of species described <span style="color:#FF0000">`totales.especies`</span></td>
                            </tr>
                        </table>
                    </td>
                    <td style="background-color:#E6E6E6">&nbsp;</td>
                </tr>
            </table></div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td ></td>
                </tr>
            </table></div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td></td>
                </tr>
            </table></div>
        <div class="centrado">
            <table width="963px" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td style="background-color:#B5D7DE">&nbsp;</td>
                    <td colspan="4" style="text-align:left;background-color:#B5D7DE;vertical-align:top"  class="linksnegros"><div class="izquierda"><table width="963px" style="border:0px;padding:1px;border-spacing:1px">
                                <tr>
                                    <td width="466" height="31"><p><a href="investigadoresVP1.jsp?idsitio=6&amp;idsubsitio=8"><img src="../images/people.png" alt="Busqueda de Investigadores" style="vertical-align:middle;border:0px"/></a><img src="../images/arrowx.gif" width="5" height="7" alt="flecha" /><a href="investigadoresVP1.jsp?idsitio=6&amp;idsubsitio=8" class="linksnegros">B&uacute;squeda  de
                                                investigadores y personas vinculadas a la iniciativa SIBM</a> </p>
                                    </td>
                                    <td width="199"><div style="text-align:left">
                                            <p><a href="institutosPV1.jsp?idsitio=6&amp;idsubsitio="><img src="../images/network.png" alt="Busqueda de Entidades" style="vertical-align:middle;border:0"/></a><img src="../images/arrowx.gif" width="5" height="7" alt="flechas" /><a href="institutosPV1.jsp?idsitio=6&amp;idsubsitio=7" class="linksnegros">B&uacute;squeda  de
                                                    entidades</a></p>
                                        </div>
                                    </td>
                                </tr>
                            </table></div></td>
                </tr>
                <tr style="background-color:#E6E6E6">
                    <td colspan="5"></td>
                </tr>
                <tr>
                    <td width="8" style="background-color:#B5D7DE">&nbsp;</td>
                    <td width="103" style="text-align:left; background-color:#B5D7DE;vertical-align:top" class="linksnegros"><div class="izquierda"><table width="100" style="border:0px;padding:2px;border-spacing:2px">
                                <tr>
                                    <td class="linksnegros">Miembros de: </td>
                                </tr>
                            </table></div>
                    </td>
                    <td width="155" style="background-color:#B5D7DE"><table width="100" style="border:0px;padding:2px;border-spacing:2px">
                            <tr>
                                <td><a href="http://www.siac.net.co/Home.php" target="_blank"><img src="../images/banner_sib.gif" alt="SIB" width="115" height="49" style="border:0px" /></a></td>
                            </tr>
                        </table>
                    </td>
                    <td width="484" style="background-color:#B5D7DE"><table width="100" style="text-align:center;border:0px;padding:2px;border-spacing:2px">
                            <tr>
                                <td><a href="http://www.iobis.org/" target="_blank"><img src="../images/banner_obis.gif" alt="OBIS" width="150" height="43" style="border:0px"/></a></td>
                            </tr>
                        </table>
                    </td>

                    <td width="484" style="background-color:#B5D7DE"><table width="100" style="border:0px;padding:2px;border-spacing:2px">
                            <tr>
                                <td><a href="http://www.iabin.net/" target="_blank"><img src="../images/iabin.gif" alt="IABIN" style="border:0px"/></a></td>
                            </tr>
                        </table></td>
                </tr>
            </table></div><h1></h1>
            <%@ include file="../plantillaSitio/footermodulesV3.jsp" %>
    </body>
</html>