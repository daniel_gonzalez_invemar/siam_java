<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta name="keywords" content="invemar, ciencia marina, investigacion, marino, costera, costero"/>
<meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC"/>
<meta http-equiv="pragma" content="no-cache"/>
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="0"/>

 
<script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>

<script type="text/javascript" src="../1ibre/1ibre.js"></script>
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>

<script type="text/javascript" src="jscript/validate.js"></script>
 <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css"/>
 
<script type="text/javascript" src="../index.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/AdvanceSearchUtilPV1.js"></script>

<link type="application/jsonrequest" href="info.jsp" rel="datasource" name="info" >

  
<style type="text/css">
.selected {
  background-color: #ddd;
}
.steps {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
	background-color: #CCCCCC;	
}
</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="font-size: 12px">
<jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=30"/>

  <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#01BDD6">
   <td width="750" class="headerTexto">Sistema
      de Informaci&oacute;n sobre Biodiversidad Marina &gt; B&uacute;squeda avanzada</td>
  </tr>
  <tr>
    <td></td>
  </tr>
      </table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="3" height="3"></td>
  </tr>
</table>
<form method="get" action="listadoespecies.jsp" id="forma" name="forma" datasource="datos" prefilled style="margin: 0px"><table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="200"><table border="0" cellpadding="0" cellspacing="0" width="200">
        <tr>
          <td width="2"><img src="../images/orange_box_r1_c1.gif" width="2" height="2"></td>
          <td width="200" background="../images/orange_box_r1_c2.gif"><img src="../images/orange_box_r1_c2.gif" width="200" height="2"></td>
          <td width="10"><img src="../images/orange_box_r1_c3.gif" width="2" height="2"></td>
        </tr>
        <tr>
          <td width="2" background="../images/orange_box_r2_c1.gif"><img src="../images/orange_box_r2_c1.gif" width="2" height="51"></td>
          <td width="196" align="center" valign="top"><table width="240" border="0" align="center" cellpadding="1" cellspacing="1">
              <tr>
                <td class="titulosnegros">Phylum/Divisi&oacute;n</td>
              </tr>
              <tr>
                <td><select id="phylum" name="phylum" class="inputText2">
                  <option value="">--</option>
                  <option for-each="info.divisiones" item="division" value="`division`">`division`</option>
                </select>
                </td>
              </tr>
            </table>
          </td>
          <td width="10" background="../images/orange_box_r2_c3.gif"><img src="../images/spacer.gif" width="1" height="1"></td>
        </tr>
        <tr>
          <td width="2"><img src="../images/orange_box_r3_c1.gif" width="2" height="2"></td>
          <td width="200" background="../images/orange_box_r3_c2.gif"><img src="../images/orange_box_r3_c2.gif" width="200" height="2"></td>
          <td width="10"><img src="../images/orange_box_r3_c3.gif" width="2" height="2"></td>
        </tr>
      </table>
    </td>
    <td width="75">&nbsp;</td>
    <td width="200"><table border="0" cellpadding="0" cellspacing="0" width="200">
        <tr>
          <td width="2"><img src="../images/orange_box_r1_c1.gif" width="2" height="2"></td>
          <td width="200" background="../images/orange_box_r1_c2.gif"><img src="../images/orange_box_r1_c2.gif" width="200" height="2"></td>
          <td width="10"><img src="../images/orange_box_r1_c3.gif" width="2" height="2"></td>
        </tr>
        <tr>
          <td width="2" background="../images/orange_box_r2_c1.gif"><img src="../images/orange_box_r2_c1.gif" width="2" height="51"></td>
          <td width="196" align="center" valign="top"><table width="240" border="0" align="center" cellpadding="1" cellspacing="1">
              <tr>
                <td class="titulosnegros">Nivel taxon&oacute;mico:</td>
              </tr>
              <tr>
                <td><select class="inputText2" name="criterio" id="niveltax">
                  <option value="">--</option>
                  <option value="clase">Clase</option>
                  <option value="orden">Orden</option>
                  <option value="familia">Familia</option>
                  <option value="genero">G&eacute;nero</option>
                  <option value="especie">Especie</option>
                </select>
                </td>
              </tr>
            </table>
          </td>
          <td width="10" background="../images/orange_box_r2_c3.gif"><img src="../images/spacer.gif" width="1" height="1"></td>
        </tr>

        <tr>
          <td width="2"><img src="../images/orange_box_r3_c1.gif" width="2" height="2"></td>
          <td width="200" background="../images/orange_box_r3_c2.gif"><img src="../images/orange_box_r3_c2.gif" width="200" height="2"></td>
          <td width="10"><img src="../images/orange_box_r3_c3.gif" width="2" height="2"></td>
        </tr>
      </table>
    </td>
    <td width="75">&nbsp;</td>
    <td width="200"><table border="0" cellpadding="0" cellspacing="0" width="200">
        <tr>
          <td width="2"><img src="../images/orange_box_r1_c1.gif" width="2" height="2"></td>
          <td width="200" background="../images/orange_box_r1_c2.gif"><img src="../images/orange_box_r1_c2.gif" width="200" height="2"></td>
          <td width="10"><img src="../images/orange_box_r1_c3.gif" width="2" height="2"></td>
        </tr>
        <tr>
          <td width="2" background="../images/orange_box_r2_c1.gif"><img src="../images/orange_box_r2_c1.gif" width="2" height="51"></td>
          <td width="196" align="center" valign="top"><table width="240" border="0" align="center" cellpadding="1" cellspacing="1">
              <tr>
                <td class="titulosnegros">Taxon<img src="../images/spacer.gif" width="13" height="13"></td>
              </tr>
              <tr>
                <td><input type="text" name="valor" size="40" class="inputText2" enabledWhen="$F(f.criterio)" required datalist="filtro.jsp" elements="f.phylum,f.criterio" labelText="Grupo taxon&oacute;mico">
                </td>
              </tr>
            </table>
          </td>
          <td width="10" background="../images/orange_box_r2_c3.gif"><img src="../images/spacer.gif" width="1" height="1"></td>
        </tr>
        <tr>
          <td width="2"><img src="../images/orange_box_r3_c1.gif" width="2" height="2"></td>
          <td width="200" background="../images/orange_box_r3_c2.gif"><img src="../images/orange_box_r3_c2.gif" width="200" height="2"></td>
          <td width="10"><img src="../images/orange_box_r3_c3.gif" width="2" height="2"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="5"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr >
  <tr >
  <td colspan="5">
  	<table border="0" cellpadding="1" cellspacing="1" width="100%">
     <tr>
       <td  bgcolor="#B5D7DE" class="texttablas"><strong>Nombre Com&uacute;n</strong></td>
     </tr>
     <tr>
        <td bgcolor="#E6E6E6" class="texttablas" style="padding:2px;"><font face="Arial, Helvetica, sans-serif" size="3"><b><font face="Arial, Helvetica, sans-serif" size="3"><b><font color="#1A5477" size="2">
          <input name="vulgar2" id="vulgar" value="" size="60" datalist="vulgares.jsp" class="inputText2">
        </font></b></font></b></font></td>
        
      </tr>
     </table>
   </td>
   </tr>
   <tr>
    <td colspan="5"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr >
  
  <tr style="display: none">
   <td width="246">
   <table border="0" cellpadding="1" cellspacing="1" width="100%" >
     <tr>
       <td bgcolor="#B5D7DE" class="texttablas"><strong>Organismos capturados listados en CITES</strong></td>
     </tr>
     <tr>
       <td bgcolor="#E6E6E6"><span class="texttablas">
         <font face="Arial, Helvetica, sans-serif" size="3"><b><font color="#1A5477" size="2">
         <select name="ncites" id="ncites" class="inputText2">
           <option value="">--</option>
           <option value="S">Si</option>
           <option value="N">No</option>
         </select>
         </font></b></font>       </span></td>
     </tr>
   </table></td>
   <td width="5">&nbsp;</td>
   <td width="250"><table border="0" cellpadding="1" cellspacing="1" width="100%">
     <tr>
       <td bgcolor="#B5D7DE" class="texttablas"><strong>&iquest;Incluidos en Libros
          Rojos de Colombia?</strong></td>
     </tr>
     <tr>
       <td bgcolor="#E6E6E6"><span class="texttablas">
         <font face="Arial, Helvetica, sans-serif" size="3"><b><font color="#1A5477" size="2">
         <select name="nlibrorojo" id="nlibrorojo" class="inputText2">
           <option value="">--</option>
           <option value="S">Si</option>
           <option value="N">No</option>
         </select>
         </font></b></font>       </span></td>
     </tr>
   </table></td>
   <td width="1" align="center">&nbsp;</td>
   <td width="248" align="left" valign="top"><table border="0" cellpadding="1" cellspacing="1" width="100%">
     <tr>
       <td bgcolor="#B5D7DE" class="texttablas"><strong>Categoria Libros Rojos de Colombia</strong></td>
     </tr>
     <tr>
       <td bgcolor="#E6E6E6"><select name="ncatlibrorojo" id="ncatlibrorojo" class="inputText2" enabledWhen="$(f.nlibrorojo).value == 'S'">
         <option value="">--</option>
         <option value="CR">En peligro critico</option>
         <option value="EN">En peligro</option>
         <option value="VU">Vulnerable</option>
         <option value="LR">Bajo riesgo</option>
         <option value="LC">Preocupaci&oacute;n menor</option>
         <option value="NT">Casi amenazado</option>
         <option value="DD">Datos insuficientes</option>
       </select>
       </td>
     </tr>
   </table></td>

  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr style="display:none">
    <td colspan="4"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td width="363" valign="top">
    <table border="0" cellpadding="1" cellspacing="1" width="100%" style="padding:0; margin:0; border:0;display:none">
        <tr>
          <td bgcolor="#B5D7DE" class="texttablas" ><strong>Profundidad de la captura (en mts)</strong></td>
        </tr>
        <tr>
          <td bgcolor="#E6E6E6"><font size="1" face="Arial, Helvetica, sans-serif"> desde

              <input class="inputText2" name="nprof1" id="nprof1" type ="number" value="" size="12" labelText="Profundidad M&iacute;nima" min="0">
hasta
<input class="inputText2" name= "nprof2" id="nprof2" type="number" value="" size="12" labelText="Profundidad M&aacute;xima" validate="validateMinMax('nprof1')" validationMessage="La profundidad m&aacute;xima debe ser un n&uacute;mero v&aacute;lido y ser mayor a la profundidad m&iacute;nima">
 </font></td>
        </tr>
        <tr>
          <td bgcolor="#E6E6E6" class="texttablas">Utilice punto como separador decimal<br></td>
        </tr>
      </table>    </td>
    
    <td width="3">&nbsp;</td>
    <td width="4">&nbsp; </td>
    <td width="380" align="center" valign="top"><table border="0" cellpadding="1" cellspacing="1" width="100%" style='display:none'>
      <tr>
        <td bgcolor="#B5D7DE"class="texttablas"><strong>Organismos capturados en el periodo</strong></td>
          </tr>
      <tr>
        <td bgcolor="#E6E6E6"><font size="1" face="Arial, Helvetica, sans-serif">desde</font>            <select name= "ano1" class="inputText2">
          <option value="">----</option>
          <option for-each="info.anos" item="ano" value="`ano`" visibleWhen="!$F(f.ano2) || $F(f.ano2) &gt;= value">`ano`</option>
          </select>
  &nbsp;&nbsp;&nbsp;<font size="1" face="Arial, Helvetica, sans-serif">hasta</font><select name="ano2" class="inputText2">
    <option value="">---</option>
    <option for-each="info.anos" item="ano" value="`ano`" visibleWhen="$F(f.ano1) &lt;= value">`ano`</option>
  </select>          </td>
          </tr>
      <tr>
        <td bgcolor="#E6E6E6" class="texttablas">&nbsp;</td>
          </tr>
    </table></td>
    </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <td class="texttablas"><img src="../images/spacer.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td>
    <table width="963px" border="0" align="center" cellpadding="1" cellspacing="1">
      <tr>
          <td colspan="7" bgcolor="#B5D7DE" class="texttablas" ><strong>Otros Criterios de busqueda. Siga los siguientes pasos</strong></td>
        </tr>
      <tr>
        <td width="175" bgcolor="#E6E6E6" class="texttablas" style="border-top:1px solid #000000; border-left:1px solid #000000; border-right:1px solid #000000;"><table border="0" width="100%"> <tr><td><div align="right"><span class="steps">1 </span></div></td><td><div align="right"><img src="../images/next.gif" alt="next"/></div></td></tr></table></td>
        <td width="5" bgcolor="#E6E6E6">&nbsp;</td>
        <td  bgcolor="#E6E6E6" class="texttablas" style="border-top:1px solid #000000; border-left:1px solid #000000; border-right:1px solid #000000;"><table border="0" width="100%"> <tr><td><div align="right"><span class="steps">2</span></div></td><td><div align="right"><img src="../images/next.gif" alt="next"/></div></td></tr></table></td>
        <td width="5" bgcolor="#E6E6E6">&nbsp;</td>
        <td  bgcolor="#E6E6E6" class="texttablas" style="border-top:1px solid #000000; border-left:1px solid #000000; border-right:1px solid #000000;"><table border="0" width="100%"> <tr><td><div align="right"><span class="steps">3 </span></div></td><td><div align="right"><img src="../images/next.gif" alt="next"/></div></td></tr></table></td>
        <td width="5" bgcolor="#E6E6E6">&nbsp;</td>
        <td  bgcolor="#E6E6E6" style="border-top:1px solid #000000; border-left:1px solid #000000; border-right:1px solid #000000;"><div align="center"><span class="steps">4</span></div></td>
        </tr>
         <tr>
        <td width="175" bgcolor="#DCECEF" class="texttablas" style="border-left:1px solid #000000; border-right:1px solid #000000;"><strong>Seleccionar los registros que en el campo:</strong></td>
        <td width="5" bgcolor="#DCECEF">&nbsp;</td>
        <td  bgcolor="#DCECEF" class="texttablas" style="border-left:1px solid #000000; border-right:1px solid #000000;"><strong>Tengan un valor:</strong></td>
        <td width="5" bgcolor="#DCECEF">&nbsp;</td>
        <td  bgcolor="#DCECEF" class="texttablas" style="border-left:1px solid #000000; border-right:1px solid #000000;"><strong>A:</strong></td>
        <td width="5" bgcolor="#DCECEF">&nbsp;</td>
        <td  bgcolor="#DCECEF" style="border-left:1px solid #000000; border-right:1px solid #000000;"><strong>Agregar  criterio</strong></td>
        </tr>

      <tr>
        <td bgcolor="#E6E6E6" class="texttablas" style="border-left:1px solid #000000; border-right:1px solid #000000;">
        <select id="campo" name="campo" class="inputText2">
          <option value="">--</option>
          <option for-each="info.campos" item="campo" value="`campo.column_name`"> `campo.comments` </option>
        </select></td>
        <td bgcolor="#E6E6E6">&nbsp;</td>
        <td bgcolor="#E6E6E6" style="border-left:1px solid #000000; border-right:1px solid #000000;">
        <select id="operador" name="operador" class="inputText2">
          <option value="">--</option>
          <option value="&#61;"> igual </option>
          <option value="like"> contiene </option>
          <option value="!="> diferente </option>
          <option value="&gt;"> mayor que </option>
          <option value="&lt;"> menor que </option>
        </select></td>
        <td bgcolor="#E6E6E6">&nbsp;</td>
        <td width="162" bgcolor="#E6E6E6" style="border-left:1px solid #000000; border-right:1px solid #000000;">
        
        <input type="text" id="criteriovalue" name="criteriovalue"  size="40" maxlength="55" class="inputText2" enabledWhen="$F(f.campo)" required datalist="filtroOCriterios.jsp" elements="f.campo"/><br></td>
        
        <td width="5" bgcolor="#E6E6E6">&nbsp;</td>
        <td width="291" bgcolor="#E6E6E6" style="border-left:1px solid #000000; border-right:1px solid #000000;">
        <input name="addToSearch_and" type="button" value="Agregar ( Y )" onClick="addCriteria(this.name)" />
        </td>
      </tr>
	  <tr>
	  <td bgcolor="#E6E6E6" style="border-bottom:1px solid #000000; border-left:1px solid #000000; border-right:1px solid #000000;">&nbsp;</td><td bgcolor="#E6E6E6">&nbsp;</td><td bgcolor="#E6E6E6" style="border-bottom:1px solid #000000; border-left:1px solid #000000; border-right:1px solid #000000;">&nbsp;</td><td bgcolor="#E6E6E6">&nbsp;</td><td bgcolor="#E6E6E6" style="border-bottom:1px solid #000000; border-left:1px solid #000000; border-right:1px solid #000000;">&nbsp;</td><td bgcolor="#E6E6E6">&nbsp;</td>
	  <td bgcolor="#E6E6E6" style="border-bottom:1px solid #000000; border-left:1px solid #000000; border-right:1px solid #000000;"><input name="addToSearch_or" type="button" value="Agregar ( O )" onClick="addCriteria(this.name)" /></td>
	  </tr>	
      <tr>
        <td colspan="7" bgcolor="#E6E6E6" class="texttablas">
        <table width="100%" border="0"><tr><td width="46%" rowspan="3"> <label>
          <textarea name="othercriterios" id="othercriterios" cols="45" rows="5">
          </textarea>
        </label></td><td width="50%">&nbsp;</td>
        </tr><tr>
          <td><img src="../images/bef.gif" alt="bef"/ style="vertical-align:middle"> Criterios seleccionados</td>
        </tr><tr><td>&nbsp;</td></tr></table>
       </td>
        </tr>

      <tr>
        <td bgcolor="#DCECEF" class="texttablas" colspan="7"><strong>Repetir los pasos 1,2,3,4 para cada criterio adicional</strong></td>
      </tr>
       <tr>
        <td bgcolor="#E6E6E6" class="texttablas" colspan="7"><br></td>
      </tr>
     
    </table></td>
    
  </tr>
  
</table>

<table width="963px" border="0" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <td><img src="../images/spacer.gif" width="2" height="2"></td>
  </tr>
  <tr>
    <td align="right">
	<input type="hidden" name="pages" value="advancedSearch"/>
	<input type="button" name="buscar" value="Buscar / Search" onClick="tabular(this.name)" />
	<input type="button" name="listado" value="Listado especies" onClick="tabular(this.name)"/>
    <input type="button" value="Nueva b&uacute;squeda / New Search" onClick="resetForm()"  /></td>
  </tr>
   <tr>
     <td align="right">&nbsp;</td>
   </tr>
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
        </tr>
      </table>    </td>
  </tr>
  <tr if="ibre.datasource.values.datos &amp;&amp; !datos.items.length">
    <td><img src="../images/spacer.gif" width="4" height="4"></td>
  </tr>
</table>
</form>
<script type="text/javascript">
var f = document.forma;
function comparator(str1,str2){
	return (str1 == str2);
}
</script>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="2"><img src="../images/orange_box_r1_c1.gif" width="2" height="2"></td>
    <td width="2"><img src="../images/orange_box_r1_c3.gif" width="2" height="2"></td>
  </tr>
</table>

<table align="center" border="0" width="963px" if="ibre.datasource.values.datos &amp;&amp; datos.items.length">
  <tbody>
    <tr>
      <td width="640">&nbsp;</td>
    </tr>
  </tbody>
</table>
<div align="center">
  <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="4"></td>
    </tr>
    <tr>
      <td width="8" bgcolor="#B5D7DE">&nbsp;</td>
      <td width="103" align="left" valign="top" bgcolor="#B5D7DE" class="linksnegros"><table width="100" border="0" align="left" cellpadding="2" cellspacing="2">
          <tr>
            <td class="linksnegros">Miembros de: </td>
          </tr>
        </table>
      </td>
      <td width="155" bgcolor="#B5D7DE"><table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.siac.net.co/Home.php" target="_blank"><img src="../images/banner_sib.gif" alt="sib" width="115" height="49" border="0"></a></td>
          </tr>
        </table>
      </td>
      <td width="484" bgcolor="#B5D7DE"><table width="100" border="0" align="center" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.iobis.org/" target="_blank"><img src="../images/banner_obis.gif" alt="obis" width="150" height="43" border="0"></a></td>
          </tr>
        </table>
      </td>
    <td width="484" bgcolor="#B5D7DE"><table width="100" border="0" cellpadding="2" cellspacing="2">
      <tr>
        <td><a href="http://www.iabin.net/" target="_blank"><img src="../images/iabin.gif" alt="IABIN" border="0"></a></td>
      </tr>
    </table></td>

    </tr>
  </table>

  <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><div align="center"><%@ include file="/plantillaSitio/footermodulesV3.jsp"%></div>
      </td>
    </tr>
  </table>
  <p>&nbsp;
        
      </p>
</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2300620-1");
pageTracker._trackPageview();
} catch(err) {}
</script>

</body>
</html>
