/*
datasource(Fuente de datos)--
se carga informacion que se encuentra registrada de una consulta previa realizada en un archivo jsp.
este resultado contiene los registros que seran plasmados en la busqueda

-- cada vez que se cliquea en buscar es llamada nuevamente la función
-- la funcion recargar hace un refresh con los nuevos parametros definidos en el formulario.

*/
function recargar() {
  var f = document.formasmc;
  $("datasource").loadDatasource("institutos.jsp?nombre=" + escape($F(f.nombre)) + "&nacionalidad=" + $F(f.nacionalidad));
}