<%@ include file="common.jsp" %>
<!--%@ page contentType="text/html;charset=ISO-8859-1"%-->
<%@ page import="java.util.regex.Pattern" %>
<%@ page import="java.util.regex.Matcher" %>


<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%@ page import="categorias.*" %>
<%@ page import="conex.*" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="utilidades.*" %>
<%@ page import="estadistica.*" %>


<%!
static HashMap criterios = new HashMap();
static {
  criterios.put("clase", "f.CLASE");
  criterios.put("orden", "f.ORDEN");
  criterios.put("familia", "f.FAMILIA");
  criterios.put("genero", "f.GENERO");
  criterios.put("especie", "f.ESPECIE");
}

static HashMap icriterios=new HashMap();
static{
	 icriterios.put("scientificname", "f.especie");
     icriterios.put("commonname", "f.NOMBRE_COMUN");
	 icriterios.put("genus", "f.GENERO");
}

static HashMap ioperators=new HashMap();
static{
	ioperators.put("begin","like");
	ioperators.put("igual","=");
	ioperators.put("contains","like");
	

}

static HashMap fields = new HashMap();
static {
  fields.put("phylum", "phylum");
  fields.put("familia", "familia");
  fields.put("lote", "lote");
  fields.put("especie", "especie");
  fields.put("autores", "autores");
  fields.put("vulgar", "vulgar");
}


static List columns = new ArrayList();
static {
  columns.add("PHYLUM");
  columns.add("DIVISION");
  columns.add("FAMILIA");
  columns.add("SECCION");
  columns.add("NOCAT");
  columns.add("NOINV");
  columns.add("ESPECIE");
  columns.add("AUTOR");
  columns.add("NOMBRE_COMUN");
  columns.add("CLAVE");
  
}


%>
<%
ArrayList params = new ArrayList();
String especie = request.getParameter("nespecie");
String genero = param("ngenero", "");
String division = param("phylum", "");
String criterioParam = param("criterio", "");
String criterio = (String)criterios.get(criterioParam);
String valor = param("valor", "");

//Criterios de busquedas de la pagina index.htm
String pcriterio=param("icriterio","");
String icriterio=(String)icriterios.get(pcriterio);
String poperator=param("ioperator","");
String ioperator=(String)ioperators.get(poperator);
String ivalue=param("ivalue","");

//System.out.println("icriterio:.. "+icriterio);
//System.out.println("ioperator:.. "+ioperator);
//System.out.println("ivalue:.. "+ivalue);


String cites = param("ncites", "");
String librorojo = param("nlibrorojo", "");
String catlibrorojo = param("ncatlibrorojo", "");
String ano1 = param("ano1", "");
String ano2 = param("ano2", "");

String lat1 = param("lat1", "");
String lat2 = param("lat2", "");
String lng1 = param("lng1", "");
String lng2 = param("lng2", "");

//String lat1 = request.getParameter("lat1");
//String lat2 = request.getParameter("lat2");
//String lng1 = request.getParameter("lng1");
//String lng2 = request.getParameter("lng2");

String prof1 = param("nprof1", "");
String prof2 = param("nprof2", "");
String vulgar = param("vulgar2", "");
String ambiente = param("ambiente", "");
String zona = param("zona", "");
String region = param("nreg", "");
String ecoregion = param("ecoregion", "");
String proyecto=param("proyecto","");
String estacion=param("estacion","");

String otherCriteria=param("othercriterios","");
String otherColumns=param("columns","");

String pages=param("pages","");

boolean hasEspecie = StringUtils.isNotEmpty(especie);
//System.out.println("hasEspecie:.. "+hasEspecie);
boolean hasGenero = StringUtils.isNotEmpty(genero);
//System.out.println("hasGenero:.. "+hasGenero);
boolean hasDivision = StringUtils.isNotEmpty(division);
boolean hasCriterio = StringUtils.isNotEmpty(criterio);
//System.out.println("hasCriterio:.. "+hasCriterio);
boolean hasValor = StringUtils.isNotEmpty(valor);

//verificamos que se tengan criterios de busquedas para la busqueda simple en index.htm
boolean hasICriterio=StringUtils.isNotEmpty(icriterio);
boolean hasIOperator=StringUtils.isNotEmpty(ioperator);
boolean hasIValue=StringUtils.isNotEmpty(ivalue);

boolean hasCites = StringUtils.isNotEmpty(cites);
boolean hasLibrorojo = StringUtils.isNotEmpty(librorojo);
boolean hasCatlibrorojo = StringUtils.isNotEmpty(catlibrorojo);
/*boolean hasAno1 = StringUtils.isNotEmpty(ano1);
boolean hasAno2 = StringUtils.isNotEmpty(ano2);*/


boolean hasLat1 = StringUtils.isNotEmpty(lat1);
boolean hasLat2 = StringUtils.isNotEmpty(lat2);
boolean hasLng1 = StringUtils.isNotEmpty(lng1);
boolean hasLng2 = StringUtils.isNotEmpty(lng2);


boolean hasProf1 = StringUtils.isNotEmpty(prof1);
boolean hasProf2 = StringUtils.isNotEmpty(prof2);
boolean hasVulgar = StringUtils.isNotEmpty(vulgar);
boolean hasAmbiente = StringUtils.isNotEmpty(ambiente);
boolean hasZona = StringUtils.isNotEmpty(zona);
boolean hasRegion = StringUtils.isNotEmpty(region);
boolean hasEcoregion = StringUtils.isNotEmpty(ecoregion);
boolean hasProyecto=StringUtils.isNotEmpty(proyecto);
boolean hasEstacion=StringUtils.isNotEmpty(estacion);

boolean hasOtherCriteria=StringUtils.isNotEmpty(otherCriteria);
boolean hasOtherColumns=StringUtils.isNotEmpty(otherColumns);

if (hasDivision) params.add(division);

if (hasGenero){ 
	params.add("%" + genero + "%");
	params.add("%" + genero + "%");
}

if (hasEspecie)
{ 
	params.add("%" + especie + "%");
	params.add("%" + especie + "%");
}
//parameter de la busqueda simple
if(hasICriterio && hasIOperator && hasIValue){
	
		if(poperator.equalsIgnoreCase("begin")){
			if(!pcriterio.equalsIgnoreCase("commonname")){
				params.add(ivalue+"%");
			}
			
			params.add(ivalue+"%");
		}else if(poperator.equalsIgnoreCase("contains")){
			if(!pcriterio.equalsIgnoreCase("commonname")){
				params.add("%"+ivalue+"%");
			}
			params.add("%"+ivalue+"%");
		}else {
			if(!pcriterio.equalsIgnoreCase("commonname")){
				params.add(ivalue);
			}
			params.add(ivalue);
		}
}


if (hasCriterio && hasValor && !criterio.equalsIgnoreCase("f.clase")) {
	
	params.add("%" + valor + "%");
	params.add("%" + valor + "%");
}else if (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.clase")) {
	params.add("%" + valor + "%");
}

//if (hasLat1) params.add(lat1);
//if (hasLat2) params.add(lat2);
//if (hasLng1) params.add(lng1);
//if (hasLng2) params.add(lng2);
//System.out.println("WHERECRITERIA: .. "+otherCriteria);
//System.out.println("Other columns "+otherColumns);
//System.out.println("====sibmuseo====");

Pattern p = Pattern.compile(",");
Matcher m;
if (hasLat1)
{
	m = p.matcher(lat1);
	lat1=m.replaceAll(".");
	//System.out.println("LAT1: "+lat1);
	params.add(Double.parseDouble(lat1));
 }
if (hasLat2){ 
	m = p.matcher(lat2);
	lat2=m.replaceAll(".");
	//System.out.println("LAT2: "+lat2);
	
	params.add(Double.parseDouble(lat2));
}
if (hasLng1){
	m = p.matcher(lng1);
	lng1=m.replaceAll(".");
	//System.out.println("LNG1: "+lng1);
	
	 params.add(Double.parseDouble(lng1));
}
if (hasLng2){
	m = p.matcher(lng2);
	lng2=m.replaceAll(".");
	//System.out.println("LNG2: "+lng2);
	
 	params.add(Double.parseDouble(lng2));
 }
//System.out.println("================");

if (hasProf1) params.add(prof1);
if (hasProf2) params.add(prof2);

if (hasVulgar) params.add("%" + vulgar + "%");
if (hasAmbiente) params.add(ambiente);


/*if (hasAno1) params.add(ano1);
if (hasAno2) params.add(ano2);*/





if (hasZona) params.add(zona);

if (hasEcoregion) params.add(ecoregion);

if (hasRegion) params.add(region + "%");

if (hasLibrorojo && !librorojo.equals("N") && hasCatlibrorojo) params.add(catlibrorojo);

if(hasProyecto) params.add(proyecto);
if(hasEstacion) params.add(estacion);

int position = intParam("position", 0);
int length = 20;
String field = param("field", "especie");
boolean reversed = StringUtils.isNotBlank(request.getParameter("reversed"));
if (!fields.containsKey(field)) field = "especie";

//System.out.println("Lat1 "+lat1);
//System.out.println("Lat2 "+lat1);
//System.out.println("Lng "+lng1);
//System.out.println("Lng2 "+lng2);

String query="SELECT f.PHYLUM AS cod_phylum, f.division AS phylum, f.FAMILIA AS familia, f.SECCION || '-' || f.NOCAT AS lote, f.NOINV AS numero, f.ESPECIE AS especie,f.basedelregistro as baseregistro, f.AUTOR AS autores, f.NOMBRE_COMUN AS vulgar, f.CLAVE AS clave, f.PROCEDENCIA AS procedencia "
//+ (hasOtherColumns && !columns.contains(otherColumns)? " ,"+otherColumns: "") 

+" FROM VM_FICHAC f "
+" where ROUND(MOD(TRUNC((NODOS_ACTIVOS/1),1),10),0) = 1  "

+ (hasDivision? " AND  f.division = ?": "") 

+ (hasGenero? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "") 

+(hasEspecie? " AND UPPER(f.EPITETO) IN (SELECT vigente FROM (SELECT NULL anterior, especie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT especie, epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")

+ (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.especie") ? " AND UPPER(replace(f.especie,' ','')) IN (SELECT UPPER(replace(vigente,' ','')) FROM ( SELECT NULL anterior, genero ||decode(subgenero, NULL,'',' ') ||subgenero ||' ' || decode(especie,NULL,'SP.',especie)||decode(subespecie, NULL,'',' ') ||subespecie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero ||' ' || especie, genero_vigente ||' ' || epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior "+ioperator +" UPPER (?) OR vigente "+ioperator +" UPPER (?)) ": "") 
+ (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.GENERO") ? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior "+ioperator +" UPPER (?) OR vigente "+ioperator +" UPPER (?))": "") 
+ (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.NOMBRE_COMUN") ?  " AND  UPPER(f.NOMBRE_COMUN) "+ioperator +" UPPER(?)": "") 
/*+ (hasCriterio && hasValor? " AND " + criterio + " like UPPER(?)": "")*/
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.genero") ? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.especie") ? " AND UPPER(replace(f.especie,' ','')) IN (SELECT UPPER(replace(vigente,' ','')) FROM ( SELECT NULL anterior, genero ||decode(subgenero, NULL,'',' ') ||subgenero ||' ' || decode(especie,NULL,'SP.',especie)||decode(subespecie, NULL,'',' ') ||subespecie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero ||' ' || especie, genero_vigente ||' ' || epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?)) ": "")
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.orden") ? " AND UPPER (f.orden) IN ( SELECT vigente FROM (SELECT NULL anterior, orden vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT orden, orden_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.familia") ? " AND UPPER (f.familia) IN ( SELECT vigente FROM (SELECT NULL anterior, familia vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT familia, familia_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "") 
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.clase") ? " AND UPPER (f.clase) like UPPER(?)": "")  

  /* + (hasAno1? " AND f.ANOCAPTURA >= ?": "") + (hasAno2? " AND f.ANOCAPTURA <= ?": "")*/ 
 + (hasLat1? " AND f.LATITUD >= ?": "") 
 + (hasLat2? " AND f.LATITUD <= ?": "") 
 + (hasLng1? " AND f.LONGITUD >= ?": "") 
 + (hasLng2? " AND f.LONGITUD <= ?": "")  
  + (hasProf1? " AND f.PROF_CAPTURA >= ?": "") 
  + (hasProf2? " AND f.PROF_CAPTURA <= ?": "") 
  + (hasVulgar? " AND UPPER(f.NOMBRE_COMUN) LIKE UPPER(?)": "") 
  + (hasAmbiente? " AND f.AMBIENTE = ?": "") 
  + (hasZona? " AND f.ZONA = ?": "") 
  //+ (hasAmbiente? " AND f.AMBIENTE = ?": "") 
  //+ (hasZona? " AND f.ZONA = ?": "") 
  + (hasEcoregion? " AND f.ECORREGION LIKE ?": "") 
  + (hasRegion? " AND f.REGION LIKE ?": "")
   + (hasCites? " AND  f.CITES IS "  + (cites.equals("N")? "NULL": "NOT NULL"): "")
   +(hasLibrorojo? " AND f.UICN_NACIONAL IS " + (librorojo.equals("N")? "NULL": "NOT NULL" + (hasCatlibrorojo? " AND substr(f.UICN_NACIONAL,1,2) = ?": "")): "")
   + (hasProyecto? " AND f.proyecto = ?":"")
   + (hasEstacion? " AND f.estacion=?":"")
  + (hasOtherCriteria? " AND "+otherCriteria: "") 
 + " ORDER BY " + fields.get(field) + " " + (reversed? "DESC": "ASC");
 
//System.out.println("QUERY:.. "+query);
//System.out.println("PARAMS SIZE:.. "+params.size());
Object[] parameters = params.toArray();
List list = (List)sibm.query(limit(query, position + 1, length), parameters, new MapListHandler(), false);
int total = ((Number)sibm.query(count(query), parameters, one)).intValue();
%>
<!--%@ page contentType="text/html;charset=ISO-8859-1"%-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>

<link href="../siamccs.css" rel="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="keywords" content="invemar, ciencia marina, investigaci�n, marino, costera, costero" />
<meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC"/>
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />

<script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="../1ibre/1ibre.js"></script>
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>

<script type="text/javascript">
<!--
function popup(mylink, windowname)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
href=mylink;
else
href=mylink.href;
window.open(href, windowname, 'width=400,height=200,scrollbars=no,toolbar=no,status=no,resizable=no');
return false;
}
//-->
</script>
<script type="text/javascript">
	function ubio(valor){
		
		var link=document.getElementById("link");
		var url="<a id='urlubio' href='http://www.ubio.org/tools/linkit.php?map%5B%5D=all&amp;nb=1&amp;synonyms="+valor+"&amp;url="+document.URL+"'><img src='images/ubioLogo.png' width='64' height='43' border='0' alt=''/></a>";
		
		link.removeChild(document.getElementById('urlubio'));
		link.innerHTML=url;
	}
	

</script>
 <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css" />
</head>
<body>
 <jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=34"/>
 <table align="center">
<tr style="padding:20px; border-color:Blue; border-width:thin;">
<td style="padding:5px;"> 
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">

  <tr>
    <td colspan="2"><img src="../images/spacer.gif" width="5" height="5" alt="spacer"/></td>
  </tr>
  <tr bgcolor="#8DC0E3">
    <td width="21">&nbsp;</td>
    <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td width="320" align="left" class="linksnegros">Resultados de la consulta</td>
          <td width="409" align="right">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5" alt="spacer"/></td>
  </tr>
  <tr>
    <td ><img src="../images/dotted_line.gif" width="12" height="3" alt="dotted lne" /></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5" alt="spacer" /></td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="10" bgcolor="#E6E6E6">&nbsp;</td>
    <td width="733" bgcolor="#E6E6E6" class="texttablas">Haga click sobre el enlace del lote para observar los
      datos relacionados.<br/>
      En caso de estar activo, haga click sobre el enlace de historia natural
      para observar la ficha de la especie almacenada en el SIBM.<br/>
      Para informacion adicional en la web, de click sobre el enlace correpondiente
    (Google Scholar).</td>
    <td width="10" bgcolor="#E6E6E6">&nbsp;</td>
  </tr>
</table>

<!--table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td width="10" bgcolor="#FFCA95">&nbsp;</td>
    <td width="733" bgcolor="#FFCA95" class="texttablas">
  Si desea descargar los datos seleccione uno de los siguientes tipos:
  <a href="http://www.invemar.org.co/museohnmc/downloadfile?ext=txt" class="links">.TXT</a>
  o
  <a href="http://www.invemar.org.co/museohnmc/downloadfile?ext=xls" class="links">.XLS</a>  </td>
    <td width="10" bgcolor="#FFCA95">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><img src="../images/spacer.gif" width="5" height="5" alt='' /></td>
  </tr>
</table-->
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5" alt="" /></td>
  </tr>
  <tr>
    <td ><img src="../images/dotted_line.gif" width="12" height="3" alt=""/></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5" alt="" /></td>
  </tr>
</table>
<table align="center" border="0" width="100%">
  
 
  <tr bgcolor="#E6E6E6">
    <td height="13" colspan="8"><div align="right"><font face="Arial, Helvetica, sans-serif"> <strong> <font color="#ff0000" size="2"> <%=total%> </font></strong><font color="#000066" size="2">registro(s)
            encontrado(s)</font></font></div>
    </td>
  </tr>
  <tr>
    <td width="102" align="center" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("phylum")? " sorted" + (reversed? " reversed": ""): "" %>" style="text-decoration: none;cursor:pointer" onclick="location.href='sibmuseoPV1.jsp?idsitio=6&amp;idsubsitio=9&amp;position=<%=position%>&amp;field=phylum<%= !reversed && field.equals("phylum")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>'"  ><span class="titulostablas">Fuente</span></td>
    <td width="102" height="24" align="center" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("phylum")? " sorted" + (reversed? " reversed": ""): "" %>" style="text-decoration: none;cursor:pointer" onclick="location.href='sibmuseoPV1.jsp?idsitio=6&amp;idsubsitio=9&amp;position=<%=position%>&amp;field=phylum<%= !reversed && field.equals("phylum")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>'">
      <span class="titulostablas">Phylum</span>
    </td>
    <td width="124" align="center" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("familia")? " sorted" + (reversed? " reversed": ""): "" %>" style="text-decoration: none;cursor:pointer" onclick="location.href='sibmuseoPV1.jsp?idsitio=6&amp;idsubsitio=9&amp;position=<%=position%>&amp;field=familia<%= !reversed && field.equals("familia")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>'">
      <span class="titulostablas">Familia</span>
    </td>
    <td width="75" align="center" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("lote")? " sorted" + (reversed? " reversed": ""): "" %>" style="text-decoration: none;cursor:pointer" onclick="location.href='sibmuseoPV1.jsp?idsitio=6&amp;idsubsitio=9&amp;position=<%=position%>&amp;field=lote<%= !reversed && field.equals("lote")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>'">
      <span class="titulostablas"># Lote</span>
    </td>
    <td width="75" align="center" bgcolor="#B5D7DE" >
      <span class="titulostablas">Base del Registro</span>
    </td>
    <td width="297" align="center" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("especie")? " sorted" + (reversed? " reversed": ""): "" %>" style="text-decoration: none;cursor:pointer" onclick="location.href='sibmuseoPV1.jsp?idsitio=6&amp;idsubsitio=9&amp;position=<%=position%>&amp;field=especie<%= !reversed && field.equals("especie")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>'">
      <span class="titulostablas">Historia Natural</span>
    </td>
    <td width="128" align="center" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("autores")? " sorted" + (reversed? " reversed": ""): "" %>" style="text-decoration: none;cursor:pointer" onclick="location.href='sibmuseoPV1.jsp?idsitio=6&amp;idsubsitio=9&amp;position=<%=position%>&amp;field=autores<%= !reversed && field.equals("autores")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>'">
      <span class="titulostablas">Autores</span>
    </td>
    <td width="225" bgcolor="#B5D7DE" class="titulostablas<%= field.equals("vulgar")? " sorted" + (reversed? " reversed": ""): "" %>" style="text-decoration: none;cursor:pointer" onclick="location.href='sibmuseoPV1.jsp?idsitio=6&amp;idsubsitio=9&amp;position=<%=position%>&amp;field=vulgar<%= !reversed && field.equals("vulgar")? "&amp;reversed=1": "" %>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>'">
      <span class="titulostablas">Nombres vulgares</span>
    </td >
  </tr>
  <%
    /*for(y = (pag -1) * registros; (y < pag * registros) && y <  contficha.gettamano(); y++){
      ficha = new especies.Cficha();
      ficha = contficha.getficha((int)y);

      if(ficha.get_aautor() == null)
        ficha.setaautor("");
      if(ficha.get_anvulgar() == null)
        ficha.setanvulgar("");
      if(ficha.get_acatalogo() == null)
        ficha.setacatalogo("");*/
      for (int i = 0; i < list.size(); i++) {
        Map map = (Map)list.get(i);
    %>
  <tr valign="top" bgcolor="#E6E6E6">
    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("procedencia"))%></td>
    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("phylum"))%></td>
    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("familia"))%></td>
    <td><a href="ficha_catalogo.jsp?clave=<%=map.get("clave")%>&amp;numero=<%=map.get("numero")%>&amp;phylum=<%=map.get("phylum")%>&amp;autor=<%=map.get("autores")%>&amp;nombre=<%=map.get("especie")%>&amp;catalogo=<%=map.get("lote")%> " title="Clic para ver ficha de la especie" class="texttablas"><%=map.get("lote")%></a></td>
	<td class="texttablas"><%if(map.get("baseregistro")==null){%>
								&nbsp;
							<%}else{%>
									<%= map.get("baseregistro")%>
							<% }%> 

			
							</td>
<%--  if(ficha.get_aclaveesp() == null)
  {   %>
      <td bgcolor="#E6E6E6"><span class="texttablasespecie"><font face="Arial, Helvetica, sans-serif" size="2"><i><%=ficha.get_adescripcion()%></i> <a href="http://scholar.google.com/scholar?q=%22<%=ficha.get_adescripcion()%>%22&amp;ie=UTF-8&amp;oe=UTF-8&amp;hl=en&amp;btnG=Search"><img src="../images/google.gif" width="34" height="13" border="0" alt='' /> </a><a href="http://www.iobis.org/IndexSearch?sciname=<%=ficha.get_adescripcion()%>&amp;category=all&amp;names=all"><img src="../images/obis_logo.gif" width="35" height="13" border="0" alt=''/></a></font></td>

      <% } %>

<%  if(ficha.get_aclaveesp() != null)
  {   --%>
      <td bgcolor="#E6E6E6">
      	<span class="texttablasespecie">
      		<font face="Arial, Helvetica, sans-serif" size="2">
      			<b>
      				<a   href="zsibficha2PV1.jsp?idsitio=6&amp;idsubsitio=10&amp;clave=<%=map.get("clave")%>&amp;phyl=<%=map.get("phylum")%>&amp;niv=<%--=niv--%>&amp;esp=<%--=request.getParameter("basura2")--%>&amp;nombre=<%=map.get("especie")%>&amp;phylum=<%=map.get("cod_phylum")%>&amp;nomphylum=<%=map.get("phylum")%>&amp;autor=<%=map.get("autores")%>" title="Clic para ver informaci&oacute;n taxon&oacute;mica">
      					<i><%=map.get("especie")%></i>
      				</a>
      			</b>
	    </font>
      	</span>
      	<font face="Arial, Helvetica, sans-serif" size="2">
      	<b>  
      		<a href="http://scholar.google.com/scholar?q=%22<%=map.get("especie")%>%22&amp;ie=UTF-8&amp;oe=UTF-8&amp;hl=en&amp;amp;btnG=Search">
      			<img src="../images/google.gif" width="34" height="13" border="0" alt='' />
      		</a>
      	</b></font><a href="http://www.iobis.org/IndexSearch?sciname=<%=map.get("especie")%>&amp;category=all&amp;names=all"><img src="../images/obis_logo.gif" width="35" height="13" border="0" alt='' /></a></td>
      <% // } %>

    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("autores"))%></td>
    <td class="texttablas"><%=StringUtils.defaultString((String)map.get("vulgar"))%></td>
  </tr>
  <% } %>
  <tr>
    <td colspan="7">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="7">&nbsp;</td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    	<div align="center" class="linksnavinferior">
    		<% if(position > 0){ %>
    		<a id='' href="sibmuseoPV1.jsp?idsitio=6&amp;amp;idsubsitio=9&amp;position=<%=Math.max(0, position - length)%>&amp;field=<%=field%><%=reversed? "&amp;reversed=1": ""%>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>" class="linksnavinferior">&lt;&lt;</a> <a href="sibmuseoPV1.jsp?idsitio=&amp;idsubsitio=9&amp;position=<%=Math.max(0, position - length)%>&amp;field=<%=field%><%=reversed? "&amp;reversed=1": ""%>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;othercriterios=<%=URLEncoder.encode(otherCriteria, "UTF-8")%>&amp;columns=<%=URLEncoder.encode(otherColumns, "UTF-8")%>&amp;pages=<%=pages%>" class="linksnavinferior">Anterior</a> -<% } %>
    P&aacute;gina <%=position / length + 1%> de <%=total / length + 1%> <% if(total > position + length){ %>- <a href="sibmuseoPV1.jsp?idsitio=6&amp;idsubsitio=9&amp;position=<%=position + length%>&amp;field=<%=field%><%=reversed? "&amp;reversed=1": ""%>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;othercriterios=<%=StringEscapeUtils.escapeHtml(otherCriteria)%>&amp;columns=<%=StringEscapeUtils.escapeHtml(otherColumns)%>&amp;pages=<%=pages%>" class="linksnavinferior">Siguiente</a> <a href="sibmuseoPV1.jsp?idsitio=6&amp;idsubsitio=9&amp;position=<%=position + length%>&amp;field=<%=field%><%=reversed? "&amp;reversed=1": ""%>&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;othercriterios=<%=StringEscapeUtils.escapeHtml(otherCriteria)%>&amp;columns=<%=StringEscapeUtils.escapeHtml(otherColumns)%>&amp;pages=<%=pages%>" class="linksnavinferior">&gt;&gt;</a><% } %> </div></td>
  </tr>
  <tr>
    <td ><img src="../images/dotted_line.gif" width="12" height="3" alt=''/></td>
  </tr>
</table>
<div style="text-align: center; margin-top: 5px">
<%
if (icriterio != null) {
  
  //if (StringUtils.isBlank(especie)) {
    //criterioP = "genero";
    //valorP = genero;
  //}
  //else {
    //criterioP = "especie";
    //valorP = especie;
  //}
  if(pcriterio.equalsIgnoreCase("commonname")){
  %>
     <input type="button" value="Ver en mapa / Show on map" onclick="location.href='formespacial.htm?vulgar2=<%= StringEscapeUtils.escapeHtml(ivalue) %>'" />
  <%
  }else{
	  String criterioP="", valorP="";
  	 if(pcriterio.equalsIgnoreCase("scientificname")){
		  criterioP="especie";
  		  valorP=ivalue;
      }if(pcriterio.equalsIgnoreCase("genus")){
      	  criterioP="genero";
  		  valorP=ivalue;
      }
  
  %>
  <input type="button" value="Ver en mapa / Show on map" onclick="location.href='formespacial.jsp?criterio=<%= criterioP %>&amp;valor=<%= StringEscapeUtils.escapeHtml(valorP) %>'"/>
<% } }%>
<% 
	if(null!=pages && pages.equals("advancedSearch")){
%>
	<input type="button" value="Ver en mapa / Show on map" onclick="location.href='formespacial.jsp?phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=StringEscapeUtils.escapeHtml(valor)%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar2=<%=vulgar%>&amp;ambiente=<%=URLEncoder.encode(ambiente, "UTF-8")%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>&amp;othercriterios=<%=URLEncoder.encode(otherCriteria, "UTF-8")%>&amp;columns=<%=URLEncoder.encode(otherColumns, "UTF-8")%>'" />
	
<%
}
%>
<input type="button" value="Registros biol&oacute;gicos en formato tabular / Biological registries in tabular format" onclick="location.href='regisbio.jsp?&amp;icriterio=<%=pcriterio%>&amp;ioperator=<%=poperator%>&amp;ivalue=<%=ivalue%>&amp;ngenero=<%=genero%><%= especie == null? "": "&amp;nespecie=" + especie%>&amp;phylum=<%=division%>&amp;criterio=<%=criterioParam%>&amp;valor=<%=valor%>&amp;ncites=<%=cites%>&amp;ncatlibrorojo=<%=catlibrorojo%>&amp;nlibrorojo=<%=librorojo%>&amp;nprof1=<%=prof1%>&amp;nprof2=<%=prof2%>&amp;lat1=<%=lat1%>&amp;lat2=<%=lat2%>&amp;lng1=<%=lng1%>&amp;lng2=<%=lng2%>&amp;ano1=<%=ano1%>&amp;ano2=<%=ano2%>&amp;vulgar=<%=vulgar%>&amp;ambiente=<%=ambiente%>&amp;zona=<%=zona%>&amp;nreg=<%=region%>&amp;ecoregion=<%=ecoregion%>&amp;proyecto=<%=proyecto%>&amp;estacion=<%=estacion%>&amp;othercriterios=<%=URLEncoder.encode(otherCriteria, "ISO-8859-1")%>&amp;columns=<%=URLEncoder.encode(otherColumns, "UTF-8")%>'" />

</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="21%">&nbsp;</td>
      <td width="26%">&nbsp;</td>
      <td width="15%">&nbsp;</td>
      <td width="38%">&nbsp;</td>
    </tr>
    <tr bgcolor="#B5D7DE">
      <td colspan="4" class="texttablas"><div align="left"><strong>.::Recursos
            de informaci&oacute;n
      complementaria::.</strong></div></td>
    </tr>
    <tr bgcolor="#FFFFCC" class="texttablas">
      <td><ul>
        <li>Buscador taxon&oacute;mico sem&aacute;ntico (uBio)</li>
      </ul>
      </td>
      <td>Seleccione el idioma para los nombres vernaculares y luego de click
        en el logo para ver los resultados:
      </td>
      <td>
	  <select name="synonyms" size="1" onchange="ubio(this.value)">
        <option value="0">&nbsp;&nbsp;- - Seleccione un lenguaje - -&nbsp; </option>
        <option value="0"> </option>
        <option value="English">English </option>
        <option value="French">French </option>
        <option value="Spanish" selected='selected' >Spanish </option>
        <option value="German">German </option>
        <option value="Japanese">Japanese </option>
        <option value="Portuguese">Portuguese </option>
        <option value="Russian">Russian </option>
      </select>
	  </td>
      <td id="link"><a   href="http://www.ubio.org/tools/linkit.php?map%5B%5D=all&amp;nb=1&amp;synonyms=Spanish&amp;url=<%=request.getRequestURL()%>?<%=request.getQueryString().replaceAll("&","&amp;")%>"><img src="images/ubioLogo.png" width="64" height="43" border="0" alt='' /></a></td>
  </tr>
</table>
<%@ include file="sibm_fondoPV1.jsp" %>
</td>
</tr>

</table>
<%@ include file="../plantillaSitio/footermodulesV3.jsp" %>
</body>
</html>