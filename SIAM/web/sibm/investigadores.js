/*
datasource(Fuente de datos)--
se carga informacion que se encuentra registrada de una consulta previa realizada en un archivo jsp.
este resultado contiene los registros que seran plasmados en la busqueda

-- cada vez que se cliquea en buscar es llamada nuevamente la función
-- la funcion recargar hace un refresh con los nuevos parametros definidos en el formulario.

*/

function recargar() {
  var f = document.formasmc;
  $("datasource").loadDatasource("investigadores.jsp?nombre=" + escape($F(f.nombre)) + "&nacionalidad=" + $F(f.nacionalidad) + "&titulo=" + $F(f.titulo) + "&estudio=" + $F(f.estudio) + "&instituto=" + $F(f.instituto));
}


/*
cada una de las siguientes funciones definen las opciones de cada select que hacen refresh por cada select dependiente de otro, siendo asi cada funcion por selected.

-- los nomrbres de estas funciones son descriptivos y estan ligados al nombre del select dentro del codigo HTML.



function mostrarNacionalidad(nacionalidad)
   es llamada para cada option del select, devolviendo true o false. en caso de que deba mostrarse o no.

*/
function mostrarNacionalidad(nacionalidad) {
  var titulo = $('nodoTitulo');
  var estudio = $('nodoEstudio');
  var instituto = $('nodoInstituto');
  var info = ibre.datasource.values.info;
  return (!titulo.selectedIndex || info.nacionalidadesTitulos[titulo.value].contains(nacionalidad)) && (!estudio.selectedIndex || info.nacionalidadesEstudios[estudio.value].contains(nacionalidad)) && (!instituto.selectedIndex || info.nacionalidadesInstitutos[instituto.value].contains(nacionalidad));
}
function mostrarTitulo(titulo) {
  var nacionalidad = $('nodoNacionalidad');
  var estudio = $('nodoEstudio');
  var instituto = $('nodoInstituto');
  var info = ibre.datasource.values.info;
  return (!nacionalidad.selectedIndex || info.nacionalidadesTitulos[titulo].contains(nacionalidad.value)) && (!estudio.selectedIndex || info.titulosEstudios[estudio.value].contains(titulo)) && (!instituto.selectedIndex || info.titulosInstitutos[instituto.value].contains(titulo));
}
function mostrarEstudio(estudio) {
  var nacionalidad = $('nodoNacionalidad');
  var titulo = $('nodoTitulo');
  var instituto = $('nodoInstituto');
  var info = ibre.datasource.values.info;
  return (!nacionalidad.selectedIndex || info.nacionalidadesEstudios[estudio].contains(nacionalidad.value)) && (!titulo.selectedIndex || info.titulosEstudios[estudio].contains(titulo.value)) && (!instituto.selectedIndex || info.estudiosInstitutos[instituto.value].contains(estudio));
}
function mostrarInstituto(instituto) {
  var nacionalidad = $('nodoNacionalidad');
  var titulo = $('nodoTitulo');
  var estudio = $('nodoEstudio');
  var info = ibre.datasource.values.info;
  return (!nacionalidad.selectedIndex || info.nacionalidadesInstitutos[instituto].contains(nacionalidad.value)) && (!titulo.selectedIndex || info.titulosInstitutos[instituto].contains(titulo.value)) && (!estudio.selectedIndex || info.estudiosInstitutos[instituto].contains(estudio.value));
}