<%@ page language="java" import="co.org.invemar.catalogo.model.*, java.sql.*, java.util.*" %>
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color: white; width: 300">
  <tr>
    <td width="2"><img src="../images/orange_box_r1_c1.gif" width="2" height="2"></td>
    <td background="../images/orange_box_r1_c2.gif"><img src="../images/orange_box_r1_c2.gif" width="200" height="2"></td>
    <td width="2"><img src="../images/orange_box_r1_c3.gif" width="2" height="2"></td>
  </tr>
  <tr>
    <td width="2" background="../images/orange_box_r2_c1.gif"><img src="../images/orange_box_r2_c1.gif" width="2" height="51"></td>
    <td align="center" valign="top"><table width="100%" border="0" cellpadding="1" cellspacing="1">
        <tr>
          <td width="10" valign="top" class="texttablas" align="center">
  <table id="sinonimos" border="0" cellpadding="0" cellspacing="4" width="100%" >
  <thead>
    <tr class="draggable" style="background-color: #B5D7DE"><td colspan="3" align="center">Listado de las sinonimias incluidas en el SIBM para<span style="font-style: italic;"> <%=request.getParameter("specie")%></span></td></tr>
    <tr><td colspan="3" align="center"></td></tr>
	<tr><th >Sinonimias</th><th>Autor</th><th>Tipo</th></tr>
  </thead>
  <tfoot></tfoot>
  
  <tbody>
    
  <%
        String clave=request.getParameter("clave");
        
        List sinonimias=null;
        DAOFactory df;
        df = new DAOFactory();
        Connection conn=null;
        SearchCatalogoDAO sinonimiaDao=df.getSearchCatalogoDAO();;
        try{
            conn=df.createConnection();
            sinonimias=sinonimiaDao.findSinonimiasByClave(clave,conn);
                    
        }catch(Exception e){
            System.out.println("EXCEPTION ....."+e);
        }
        if(!sinonimias.isEmpty())    {
            Iterator it=sinonimias.iterator();
        
            while(it.hasNext()){
            Sinonimias sinonimo=(Sinonimias)it.next();
  %>
        <tr><td style="font-style: italic; white-space: nowrap; border:1px solid #CCCCCC; padding:2px;"><%=sinonimo.getSinonimia()%></td><td style="white-space: nowrap; border:1px solid #CCCCCC; padding:2px;"><%=sinonimo.getAutor()%></td><td style="white-space: nowrap; border:1px solid #CCCCCC; padding:2px;"><%=sinonimo.getTipo()%></td></tr>
        <%  }
        }else{
    %>
    <tr><td colspan="3"><p align="center">En el SIBM no se encuentran sinonimias para el organismo seleccionado.</p></td></tr>
    <%}%>
  </tbody>
  </table>
            <div style="text-align: center; margin: 3px">
              <a href="#" onclick="Element.getParentPopup(this).hidePopup(); return false">Cerrar</a>
            </div>
          </td>
        </tr>
      </table>
    </td>
    <td width="2" background="../images/orange_box_r2_c3.gif"><img src="../images/spacer.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td width="2"><img src="../images/orange_box_r3_c1.gif" width="2" height="2"></td>
    <td background="../images/orange_box_r3_c2.gif"><img src="../images/orange_box_r3_c2.gif" width="200" height="2"></td>
    <td width="2"><img src="../images/orange_box_r3_c3.gif" width="2" height="2"></td>
  </tr>
</table>
