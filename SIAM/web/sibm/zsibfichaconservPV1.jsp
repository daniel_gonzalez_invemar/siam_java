<%@ include file="common.jsp" %>
<%@ page import="especies.*" %>

<%
  /*Cconexsib con1 = null;
  Connection conn1 = null;
  con1 = new Cconexsib();
  conn1 = con1.getConn();*/
  Connection conn1 = sibmDS.getConnection();

  especies.Cinfoficha infoficha = new especies.Cinfoficha();
  infoficha.setaclave(request.getParameter("clave"));
  infoficha.cargainfoficha(conn1);

  /*if (con1!= null)
    con1.close();*/
  DbUtils.close(conn1);  

  if(infoficha.get_aamenazas() == null)
    infoficha.setaamenazas("");
  if(infoficha.get_aconserv_actual() == null)
    infoficha.setaconserv_actual("");
  if(infoficha.get_aconserv_propuesta() == null)
    infoficha.setaconserv_propuesta("");
  if(infoficha.get_acites() == null)
    infoficha.setacites("");
  if(infoficha.get_acatlibrorojo() == null)
    infoficha.setacatlibrorojo("");
  if(infoficha.get_acatuicn() == null)
    infoficha.setacatuicn("");

  int l= 0;

%>
<jsp:include page="../plantillaSitio/headermodulos.jsp?idsitio=34"/>
<tr style="padding:20px; border-color:Blue; border-width:thin;">
<td style="padding:5px;">
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" height="8"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
    <td width="21"><img src="../images/arrow2.gif" width="20" height="20"></td>
    <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td width="353" align="left" class="linksnegros">Datos de conservaci&oacute;n</td>
          <td width="376" align="right">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table align="center" border="0" width="963px">
  <tbody>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">*Tax&oacute;n:</td>
    <td width="575" bgcolor="#e6e6e6" ><span class="txttaxon2"><%=request.getParameter("nombre")%></span>&nbsp;&nbsp;<span class="txttaxon1"><%=request.getParameter("autor")%></span></td>
  </tr>
  <tr>
    <td colspan="2" class="txttaxon1"><img src="../images/spacer.gif" width="1" height="1"></td>
    </tr>
</table>
<table align="center" border="0" width="963px">
  <tbody>
    <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
    <tr>
      <td colspan="3" bgcolor="#B5D7DE">&nbsp; <img src="../images/arrowx.gif" width="5" height="7">&nbsp; <span class="linksnegros">Datos
          de conservaci&oacute;n</span></td>
    </tr>
    <tr>
      <td width="165" valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="texttablas">Amenazas</td>
          </tr>
        </table>
      </td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="txttaxon4"><%=infoficha.get_aamenazas()%></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Medidas de conservaci&oacute;n actual:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_aconserv_actual()%></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Medidas de conservaci&oacute;n propuestas:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_aconserv_propuesta()%></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">CITES</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_acites()%></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Categor&iacute;a Libro Rojo (Colombia)</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_acatlibrorojo()%></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Categor&iacute;a UICN (global)</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=infoficha.get_acatuicn()%></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#ffffff"><div align="right"><a href="#top" class="linksbotton">Volver arriba</a> </div></td>
    </tr>

</table>
<%@ include file="sibm_fondoPV1.jsp" %>
</tr>
</td>
<%@ include file="../plantillaSitio/footermodules.jsp" %>