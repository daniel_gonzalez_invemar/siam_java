<%@ include file="maps.jsp" %>
<%!
static String getCMapStr(double lat, double lon, double res) {
  StringBuffer sb = new StringBuffer();
  if (lat >= 0)
    if (lon >= 0) sb.append('1');
    else sb.append('7');
  else
    if (lon >= 0) sb.append('3');
    else sb.append('5');
  int i,j;
  double llat = Math.abs(lat);
  if (llat >= 90) llat = 89.9;
  double llon = Math.abs(lon);
  if (llon >= 180) llon = 179.9;
  i = (int)(llat / 10);
  sb.append(i);
  j = (int)(llon / 10);
  if (j < 10) sb.append('0');
  sb.append(j);
  if (res == 10) return sb.toString();
  sb.append(':');
  llat -= i * 10;
  llon -= j * 10;
  i = (int)llat;
  j = (int)llon;
  if (i < 5)
    if (j < 5) sb.append('1');
    else sb.append('2');
  else
    if (j < 5) sb.append('3');
    else sb.append('4');
  if (res == 5) return sb.toString();
  sb.append(i);
  sb.append(j);
  if (res == 1) return sb.toString();
  sb.append(':');
  i = (int)((llat-i) * 10);
  j = (int)((llon-j) * 10);
  if (i < 5)
    if (j < 5) sb.append('1');
    else sb.append('2');
  else
    if (j < 5) sb.append('3');
    else sb.append('4');
  if (res == 0.5) return sb.toString();
  sb.append(i);
  sb.append(j);
  return sb.toString();
}
%>
<%
response.setContentType("text/html");
List list = (List)sibm.query(/*limit(*/query/*, position + 1, length)*/, parameters, new MapListHandler(), false);
//if (length > 80) list = list.subList(0, 79);
HashMap map = new HashMap();
double res = Double.parseDouble(request.getParameter("csquaresResolucion"));
for (int i = 0; i < list.size(); i++) {
  Map item = (Map)list.get(i);
  /*Object phylum = item.get("division");
  Map subMap = map.get(phylum);
  if (subMap == null) {
    subMap = new HashMap();
    map.put(phylum, subMap);
  }*/
  map.put(getCMapStr(((Number)item.get("latitud")).doubleValue(), ((Number)item.get("longitud")).doubleValue(), res), null);
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>
</head>
<body>
<form action="http://www.obis.org.au/cgi-bin/cs_map.pl" method="post" name="forma">
<input type="hidden" name="csq" value="<%= StringUtils.join(map.keySet(), '|') %>">
</form>
<script type="text/javascript">
document.forma.submit();
</script>
</body>
</html>