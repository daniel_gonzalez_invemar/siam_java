<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es"> 
<head> 
<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title> 
<link href="../siamccs.css" rel="stylesheet" type="text/css"/> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/> 
<META name="keywords" content="invemar, ciencia marina, investigaci&oacute;n, marino, costera, costero"/> 
<meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC"/> 
<meta http-equiv="pragma" content="no-cache"/> 
<meta http-equiv="cache-control" content="no-cache"/> 
<meta http-equiv="expires" content="0"/> 
<script type="text/javascript" src="../1ibre/scriptaculous-combo.js"></script> 
<!--script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script--> 
<script type="text/javascript" src="../1ibre/1ibre.js"></script> 
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script> 
<script type="text/javascript" src="../index.js"></script> 
<script type="text/javascript" src="institutos.js"></script> 
<link type="application/jsonrequest" href="institutos-info.jsp" rel="datasource" name="info"/> 
<link type="application/jsonrequest" rel="datasource" name="registros"/> 
<link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css"/>
</head> 
 <body>
<jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=34"/>
<div class="centrado">
<table width="963px" style="border:0px;padding:0;border-spacing:0" >
  <tr style="background-color:#8DC0E3">   
   <td width="729" style="background-color:#8DC0E3" class="linksnegros">Listado de entidades</td>
    <td width="290" style="text-align:right">&nbsp;</td>
  </tr>
</table>
<table width="963px" style="border:0px;padding:0;border-spacing:0" >
  <tr>   <td width="290" style="text-align:right">&nbsp;</td>
  </tr>
</table>
<form method="get" action="javascript: recargar()" name="formasmc" style="margin:0px">
<table width="963px" style="border:0px;padding:0;border-spacing:0;background-image:url(../images/orange_bar2.jpg)">
  <tr>
    <td width="10"><img src="../images/orange_bar2.jpg" width="3" height="69" alt="barra"/></td>
    <td width="131" style="text-align:left;vertical-align:top"><table style="border:0px;padding:1;border-spacing:1" width="130">
      <tr>
        <td width="18" style="text-align:left;vertical-align:top">&nbsp;</td>
        <td width="212"><a href="#" class="linksnegros">Nombre</a></td>
      </tr>
    </table>
      <table style="border:0px;padding:1;border-spacing:1" width="130">
        <tr>
          <td width="18">&nbsp;</td>
          <td width="212"><input name="nombre" class="inputText2" title="nombre"/>
          </td>
        </tr>
      </table></td>
    <td width="1">&nbsp;</td>
    <td width="130" style="text-align:left;vertical-align:top"><table style="border:0px;padding:1;border-spacing:1" width="130">
      <tr>
        <td width="18" style="text-align:left;vertical-align:top">&nbsp;</td>
        <td width="212"><a href="#" class="linksnegros">Pa&iacute;s</a></td>
      </tr>
    </table>
      <table style="border:0px;padding:1;border-spacing:1" width="130">
        <tr>
          <td width="18">&nbsp;</td>
          <td width="212"><select name="nacionalidad" id="nodoNacionalidad" class="inputText2" title="pais">
                    <option value="">--</option>
                    <option for-each="info.nacionalidades" item="nombre" key="id" value="`id`">`nombre`</option>
                  </select><!--input name="textfield2322" type="text" class="inputText2" size="20"-->
          </td>
        </tr>
      </table></td>
   <td width="478" style="text-align:left;vertical-align:top"><table style="border:0px;padding:1;border-spacing:1" width="180">
     <tr>
       <td style="text-align:left;vertical-align:top">&nbsp;</td>
       </tr>
   </table>
    </td>

  </tr>
</table>

<table width="963px" style="border:0px;padding:1;border-spacing:1">  
  <tr>
    <td style="text-align:right"><input type="submit" value="Buscar"/></td>
  </tr>
</table>
</form>
<table width="963px" style="border:0px;padding:0;border-spacing:0" if="registros">
  <tr>
    <td><table style="border:0px;padding:1;border-spacing:1" width="100%">
      <tr>
        <td><span class="titulostablas">Listado de entidades</span></td>
      </tr>
    </table></td>
  </tr>
  <tr if="registros.items.length">
    <td whatwg="datagrid" datasource="registros" length="10" id="datasource"><table style="border:0" width="963px">
      <thead>
        <tr style="background-color:#cccccc">
          <td style="background-color:#B5D7DE" class="sortable titulostablas" field="codigo" width="80">
            C&oacute;digo
          </td>
          <td style="background-color:#B5D7DE" class="sortable sorted titulostablas" field="nombre" width="80">
            Nombre
          </td>
          <td style="background-color:#B5D7DE" class="sortable titulostablas" field="nacionalidad" width="80">
            Pa&iacute;s
          </td>
          <td style="background-color:#B5D7DE" width="80">&nbsp;</td>
        </tr>
      </thead>
      <tbody>
        <tr for-each="registros.items" item="registro">
          <td style="background-color:#e6e6e6" height="20"><span class="texttablas">`registro.codigo`</span> </td>
          <td style="background-color:#e6e6e6" height="20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="texttablas">`registro.nombre`</span></td>
          <td style="background-color:#e6e6e6" height="20"><span class="texttablas">`info.nacionalidades[registro.nacionalidad]`</span> </td>
          <td style="background-color:#e6e6e6;text-align:center" height="20"  href="institutoPV1.jsp?id=`registro.codigo`"><span style="font-family:Arial, Helvetica, sans-serif;font-size:
          1">Detalles</span></td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="4"><table width="100%" style="border:0px;padding:0;border-spacing:0">
  <tr>   
   </tr>


</table></td>
          </tr>
        <tr>
          <td colspan="4">
            <div style="text-align:center">
              <span if="registros.position > 0"><a href="javascript: $('datasource').previousPage()" class="linksnavinferior">&lt;&lt; Anterior</a><span class="linksnavinferior"> -</span></span>
                  <span class="linksnavinferior">`registros.position + 1` a `registros.position + registros.items.length`</span><!--P&aacute;gina 1 de 8--><span if="registros.hasNext"><span> -</span> <a href="javascript: $('datasource').nextPage()" class="linksnavinferior">Siguiente &gt;&gt;</a></span>
              </div>
          </td>
        </tr>
      </tfoot>
    </table></td>
  </tr>
  <tr else>
    <td>No hay resultados para mostrar</td>
  </tr>
  <tr>
    <td style="background-color:#8F5444"><img src="../images/spacer.gif" width="1" height="1" alt="espaciador"/></td>
  </tr>
</table>

<table width="963px" style="border:0px;padding:0;border-spacing:0">
  <tr style="background-color:#E6E6E6">
    <td colspan="5"><img src="../images/spacer.gif" width="3" height="3" alt="espaciador"/></td>
  </tr>
  <tr>
    <td width="8" style="background-color:#B5D7DE">&nbsp;</td>
    <td width="103" style="text-align:left;vertical-align:top;background-color:#B5D7DE" class="linksnegros"><table width="100" style="text-align:left;border:0px;padding:2;border-spacing:2">
        <tr>
          <td class="linksnegros">Miembros de: </td>
        </tr>
      </table>
    </td>
    <td width="484" style="background-color:#B5D7DE"><table width="100" style="border:0px;padding:2;border-spacing:2">
        <tr>
          <td><a href="http://www.siac.net.co/Home.php" target="_blank"><img src="../images/banner_sib.gif" width="115" height="49" style="border:0" alt="SIB"/></a></td>
        </tr>
      </table>
    </td>
    <td width="484" style="background-color:#B5D7DE"><table width="100" style="border:0px;padding:2;border-spacing:2">
        <tr>
          <td><a href="http://www.iobis.org/" target="_blank"><img src="../images/banner_obis.gif" width="150" height="43" style="border:0" alt="Obis"/></a></td>
        </tr>
      </table>
    </td>
    <td width="484" style="background-color:#B5D7DE"><table width="100" style="border:0px;padding:2;border-spacing:2">
      <tr>
        <td><a href="http://www.iabin.net/" target="_blank"><img src="../images/iabin.gif" alt="IABIN" style="border:0"/></a></td>
      </tr>
    </table></td>
  </tr>
</table></div>
<%@ include file="../plantillaSitio/footermodules.jsp" %>
</body><h1></h1>
</html>
