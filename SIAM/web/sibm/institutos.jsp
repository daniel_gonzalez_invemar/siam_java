<%@ include file="../common.jsp" %>
<%!
static HashMap fields = new HashMap();
static {
  fields.put("nombre", "NOMBRE_IN");
  fields.put("codigo", "CODIGO_IN");
  fields.put("nacionalidad", "PAIS_IN");
}
%>
<%
ArrayList params = new ArrayList();
String nombre = request.getParameter("nombre");
String nacionalidad = request.getParameter("nacionalidad");
boolean hasNombre = StringUtils.isNotEmpty(nombre);
boolean hasNacionalidad = StringUtils.isNotEmpty(nacionalidad);
if (hasNombre) {
  params.add("%" + nombre + "%");
  params.add("%" + nombre + "%");
}
if (hasNacionalidad) params.add(nacionalidad);
JSONObject obj = (JSONObject)json;
int position = obj.optInt("position", 0);
int length = obj.optInt("length", defaultLength);
String field = obj.optString("field", "nombre");
boolean reversed = obj.optBoolean("reversed", false);
if (!fields.containsKey(field)) field = "nombre";
List list = (List)sibm.query(limit("SELECT DISTINCT NOMBRE_IN AS nombre, CODIGO_IN AS codigo, PAIS_IN AS nacionalidad FROM CDIRINSTITUTO WHERE 0 = 0" + (hasNombre? " AND (UPPER(NOMBRE_IN) LIKE UPPER(?) OR UPPER(CODIGO_IN) LIKE UPPER(?))": "") + (hasNacionalidad? " AND PAIS_IN = ?": "") + " ORDER BY " + fields.get(field) + " " + (reversed? "DESC": "ASC"), position + 1, length + 1), params.toArray(), all, false);
boolean hasNext;
if (list.size() == length + 1) {
  hasNext = true;
  list.remove(length - 1);
}
else hasNext = false;
%>
{
"position": <%= position %>,
"items": <%= new JSONArray(list) %>,
"hasNext": <%= hasNext %>
}