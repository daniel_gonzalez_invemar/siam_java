<%--
Retorna en formato JSON el listado de nacionalidades, titulos, estudios e institutos disponibles que se mostraran en el formulario para filtrar la busqueda del usuario.
Ademas retorna informacion de las relaciones existentes entre cada listado, por ejemplo, retorna los filos que se encuentran en cada area
--%>
<%@ include file="../common.jsp" %>
<%response.setContentType("application/jsonrequest; charset=ISO-8859-1");%>
{
  "nacionalidadesTitulos": <%= sibm.query("SELECT DISTINCT TITULO_CO, NACIONALIDAD_CO FROM CDIRCOLECTOR WHERE TITULO_CO IS NOT NULL AND (VINCULO_CO = '1' OR VINCULO_CO = '2' OR VINCULO_CO = '3' OR VINCULO_CO = '8')", group) %>,
  "nacionalidadesEstudios": <%= sibm.query("SELECT DISTINCT GRADOU_CO, NACIONALIDAD_CO FROM CDIRCOLECTOR WHERE GRADOU_CO IS NOT NULL AND (VINCULO_CO = '1' OR VINCULO_CO = '2' OR VINCULO_CO = '3' OR VINCULO_CO = '8')", group) %>,
  "nacionalidadesInstitutos": <%= sibm.query("SELECT DISTINCT INSTITUTO_CO, NACIONALIDAD_CO FROM CDIRCOLECTOR WHERE INSTITUTO_CO IS NOT NULL AND (VINCULO_CO = '1' OR VINCULO_CO = '2' OR VINCULO_CO = '3' OR VINCULO_CO = '8')", group) %>,
  "titulosEstudios": <%= sibm.query("SELECT DISTINCT GRADOU_CO, TITULO_CO FROM CDIRCOLECTOR WHERE GRADOU_CO IS NOT NULL AND (VINCULO_CO = '1' OR VINCULO_CO = '2' OR VINCULO_CO = '3' OR VINCULO_CO = '8')", group) %>,
  "titulosInstitutos": <%= sibm.query("SELECT DISTINCT INSTITUTO_CO, TITULO_CO FROM CDIRCOLECTOR WHERE INSTITUTO_CO IS NOT NULL AND (VINCULO_CO = '1' OR VINCULO_CO = '2' OR VINCULO_CO = '3' OR VINCULO_CO = '8')", group) %>,
  "estudiosInstitutos": <%= sibm.query("SELECT DISTINCT INSTITUTO_CO, GRADOU_CO FROM CDIRCOLECTOR WHERE INSTITUTO_CO IS NOT NULL AND (VINCULO_CO = '1' OR VINCULO_CO = '2' OR VINCULO_CO = '3' OR VINCULO_CO = '8')", group) %>,
  "nacionalidades": <%= sibm.query("SELECT DISTINCT NACIONALIDAD_CO, NOMBRE_PA FROM CDIRCOLECTOR, CPAISES WHERE NACIONALIDAD_CO = CODIGO_PA AND (VINCULO_CO = '1' OR VINCULO_CO = '2' OR VINCULO_CO = '3' OR VINCULO_CO = '8') ORDER BY NOMBRE_PA", assoc) %>,
  "titulos": <%= sibm.query("SELECT DISTINCT TITULO_CO FROM CDIRCOLECTOR WHERE TITULO_CO IS NOT NULL AND (VINCULO_CO = '1' OR VINCULO_CO = '2' OR VINCULO_CO = '3' OR VINCULO_CO = '8') ORDER BY TITULO_CO", col) %>,
  "estudios": <%= sibm.query("SELECT DISTINCT GRADOU_CO FROM CDIRCOLECTOR WHERE GRADOU_CO IS NOT NULL AND (VINCULO_CO = '1' OR VINCULO_CO = '2' OR VINCULO_CO = '3' OR VINCULO_CO = '8') ORDER BY GRADOU_CO", col) %>,
  "institutos": <%= sibm.query("SELECT DISTINCT INSTITUTO_CO FROM CDIRCOLECTOR WHERE INSTITUTO_CO IS NOT NULL AND (VINCULO_CO = '1' OR VINCULO_CO = '2' OR VINCULO_CO = '3' OR VINCULO_CO = '8') ORDER BY INSTITUTO_CO", col) %>
}