    var isIE=false;
    var loadstatustext="<img src='../images/loading.gif'/> "
   
    /************ FUNCIONES DE UTILERIA*****************/
    function initRequest()
    {
       isIE=false	
       req=null;
       if (window.XMLHttpRequest) 
       {
            req = new XMLHttpRequest();
       }else if (window.ActiveXObject) 
             {
                    req = new ActiveXObject("Microsoft.XMLHTTP");
                    isIE=true;
            }
            
        return req;
    }
    
     function searchTargetOption(sel)
    {
         var ret;
         var options=document.getElementById(sel).childNodes;
         var option=null;
        
         for(var i=0;i < options.length;i++)
         {
              option=options[i];
              if(option.selected)
              {
                  ret=option.value;
                  break; 
              }
         }
         return ret;
    }
                
    function XMLDoc(reqs)
    {
        var xmldoc=null;
        if(!isIE)
        {
          xmldoc=reqs.responseXML;
        }else{
               //IE no toma el responseXML como un documento DOM
          xmldoc = new ActiveXObject("Microsoft.XMLDOM");
          xmldoc.async=false;
          xmldoc.loadXML(reqs.responseText);					 
          xmldoc.documentElement
        }
        return xmldoc;
                            
    }
    
    function clearTableTBODY(tb){
        if (tb)
        {
           for (var i = tb.childNodes.length -1; i >= 0 ; i--)
           {
                tb.removeChild(tb.childNodes[i]);
           }
        }
    }
    
    function clearSelect(p) 
    {
         if (p)
         {
            for (var i = p.childNodes.length -1; i >= 0 ; i--)
            {
                p.removeChild(p.childNodes[i]);
            }
         }
    }
    
    function appendOpcionesSelect(valor,name,pp)
    {
       var optionElement;
       optionElement=document.createElement("option");
       optionElement.setAttribute("value",valor);
       optionElement.appendChild(document.createTextNode(name));
       pp.appendChild(optionElement);
    }
    
    
    
    function sumarLotesTable(tbodyId){
        var tbod=document.getElementById(tbodyId);
        suma=0;
        
        for (var i=0; i<tbod.rows.length; i++)
        {
            suma=suma+parseInt(tbod.rows[i].lastChild.childNodes[0].nodeValue);
        
        }
        
        return suma;
    
    }
    
    /*********************************************************************/
	
	/******************* FILL COLECCION SELECT ***************************/
	
	function doFillColeccion(idColecc){
		var url="../servletAjaxProcess?action=doFillColeccion&ms="+new Date().getTime();
        var reqs=initRequest();
		
		reqs.onreadystatechange=function processRequest(){
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages(idColecc,"colecciones", reqs,true);
                                              
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }
            }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    
		
	}
	
	/***************Fill proyectos select ***************/
	
	function doFillListProyectos(idProy){
		var url="../servletAjaxProcess?action=doFillListProyectos&ms="+new Date().getTime();
        var reqs=initRequest();
		
		reqs.onreadystatechange=function processRequest(){
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages(idProy,"proyectos", reqs,true);
                                              
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }
            }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    
		
	}
	
	
	/*******************************/
	
	function doFillListPhylums(){
		var url="../servletAjaxProcess?action=doFillListPhylums&ms="+new Date().getTime();
        var reqs=initRequest();
		
		reqs.onreadystatechange=function processRequest(){
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages("listPhylum","phylums", reqs,false);
                                              
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }
            }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    
		
	}
	
	
	/*********************************************************************/
   
   /**************** SECCION - FEHCA DE RECIBIDO*********************************/
    function doFillFechaRecibido(container)
    {
            
            var url="../servletAjaxProcess?action=doFillFechaRecibido&coleccion="+document.getElementById("colecciones").value+"&ms="+new Date().getTime();
            var reqs=initRequest();
            document.getElementById("loadingFR").innerHTML=loadstatustext
            reqs.onreadystatechange=function processRequest()
                                    {
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages("fechaRecibido","secciones", reqs,false);
                                              doEstadisticaFechaRecibido(document.getElementById("fechaRecibido").value);
                                               document.getElementById("loadingFR").removeChild(document.getElementById("loadingFR").childNodes[0]); 
                                              document.getElementById(container).style.display="block";
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }else  {
                                        document.getElementById("loadingFR").innerHTML=loadstatustext
                                     
                                     }
            }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    
    }
    
    
    
    function parseSelectMessages(id,tagName,reqs,withFirstLine)
    {
            var pp=document.getElementById(id);
            clearSelect(pp);
            var responses=XMLDoc(reqs);
            if(responses==null){
                    
            }else{
    
                    var secciones = responses.getElementsByTagName(tagName)[0];
					if(withFirstLine){
						appendOpcionesSelect(null,"Seleccione una opci\xF3n",pp);
					}
                    for (i = 0; i < secciones.childNodes.length; i++) {
                        var seccion = secciones.childNodes[i];
                        var valor = seccion.getElementsByTagName("value")[0];
                        var name = seccion.getElementsByTagName("name")[0];
         
                        appendOpcionesSelect(valor.childNodes[0].nodeValue,name.childNodes[0].nodeValue,pp);
                    }
            }
     }
    
  
  
    
    
    function doEstadisticaFechaRecibido(optValue){
        var url="../servletAjaxProcess?action=doEstadisticaFechaRecibido&coleccion="+document.getElementById("colecciones").value+"&valor="+optValue+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingCont1").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseTableMessages("tbodyEstad","Cont1ShowResult",reqs,"cantidad-muestras","ano","lotes","Total Lotes por secci�n: ");
                                          document.getElementById("loadingCont1").removeChild(document.getElementById("loadingCont1").childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         document.getElementById("loadingCont1").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);
    }
    
    function parseTableMessages(id,container,reqs,tagName,element1,element2,msg){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var muestras = responses.getElementsByTagName(tagName)[0];
           for (i = 0; i < muestras.childNodes.length; i++) {
                var cantidad = muestras.childNodes[i];
                var var1 = cantidad.getElementsByTagName(element1)[0];
                var var2 = cantidad.getElementsByTagName(element2)[0];
                appendDataTable(var1.childNodes[0].nodeValue,var2.childNodes[0].nodeValue,tb,"center","center",i);
           }
        }
        /*Se agrega el total obtenido con la variable count */
        var divCont=document.getElementById(container);
        /*si el elemento existe en el arbol se remueve */
        var ID=id+container;
        if(document.getElementById(ID)){
            divCont.removeChild(document.getElementById(ID));
        }
       /*creamos el elemeto donde se mostrara el toral obtenido y se agrega al contenedor*/
        var pelement=document.createElement("h4");
        pelement.id=ID;
        pelement.appendChild(document.createTextNode(msg));
        var spanE=document.createElement("span");
        spanE.appendChild(document.createTextNode(sumarLotesTable(id)));
        pelement.appendChild(spanE);
        
        /*se agrega el elemento al arbol*/    
        divCont.appendChild(pelement);
        
      
   }
 
   
   function appendDataTable(valor1,valor2,tb,align1,align2,i){
      var valor1Cell;
      var valor2Cell;
      /*si el browser es Internet Explorer*/
      if(isIE){
        row=tb.insertRow(tb.rows.length);
        valor1Cell=row.insertCell(0);    
        valor2Cell=row.insertCell(1);     
      }else{
         row=document.createElement("tr");
         valor1Cell=document.createElement("td");
         valor2Cell=document.createElement("td");
      }
     
      row.appendChild(valor1Cell);
      row.appendChild(valor2Cell);
      tb.appendChild(row);
      row.setAttribute("border",0);
      
      if(i%2!=0)
        row.style.backgroundColor="#eff7ff";
      else
          row.style.backgroundColor="#ffffff";
      
      valor1Cell.setAttribute("align",align1);
      valor1Cell.appendChild(document.createTextNode(valor1));
      valor2Cell.setAttribute("align",align2);
      valor2Cell.appendChild(document.createTextNode(valor2));
      
    }
    
    function doFillMuestraFI(container){
        var url="../servletAjaxProcess?action=doFillMuestraFI&coleccion="+document.getElementById("colecciones").value+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingFI").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseSelectMessages("muestraFI","secciones", reqs,false);
                                          doFillFechaIdent(document.getElementById("muestraFI").value);
                                          document.getElementById("loadingFI").removeChild(document.getElementById("loadingFI").childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                   }else if(reqs.readyState==1) {
                                        document.getElementById("loadingFI").innerHTML=loadstatustext
                                   }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
    
    }
    
    function doFillFechaIdent(optValue){
        var url="../servletAjaxProcess?action=doFillFechaIdent&coleccion="+document.getElementById("colecciones").value+"&valor="+optValue+"&ms="+new Date().getTime();
        var reqs=initRequest();
         document.getElementById("loadingCont2").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseSelectMessages("fechaident","fechasIndent", reqs,false);
                                          var seccion=document.getElementById("muestraFI").value;
                                          var ano=document.getElementById("fechaident").value;
                                          doEstadFechaIdent(seccion,ano);
                                           document.getElementById("loadingCont2").removeChild(document.getElementById("loadingCont2").childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         document.getElementById("loadingCont2").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);
    }
    
    function doEstadFechaIdent(optValueSeccion, optValueAno){
    
        var url="../servletAjaxProcess?action=doEstadFechaIdent&coleccion="+document.getElementById("colecciones").value+"&valorSeccion="+optValueSeccion+"&valorAno="+optValueAno+"&ms="+new Date().getTime();
        var reqs=initRequest();
         document.getElementById("loadingCont2").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadSeccFechaMessages("fechaident","valor-fecha","Cont2ShowResult", reqs);
                                           document.getElementById("loadingCont2").removeChild(document.getElementById("loadingCont2").childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         document.getElementById("loadingCont2").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
    }
    
    function parseEstadSeccFechaMessages(id, tagName,contenedor,reqs){
    
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var valor = responses.getElementsByTagName(tagName)[0];
           
           var divCont=document.getElementById(contenedor);
           clearElement(divCont);
            /*si el elemento existe en el arbol se remueve */
            var newId=id+tagName;
             /*if(document.getElementById(newId)){
                  divCont.removeChild(document.getElementById(newId));
             }*/
             /*creamos el elemeto donde se mostrara el toral obtenido y se agrega al contenedor*/
            var pelement=document.createElement("h4");
            pelement.id=newId;
            pelement.appendChild(document.createTextNode("Total Numero de Lotes: "));
            var spanE=document.createElement("span");
            spanE.appendChild(document.createTextNode(valor.firstChild.childNodes[0].nodeValue));
            pelement.appendChild(spanE);
            
            /*se agrega el elemento al arbol*/    
            divCont.appendChild(pelement);
               
        }
        
    }
    
    
     function doFillMuestraFC(container){
        var url="../servletAjaxProcess?action=doFillMuestraFC&coleccion="+document.getElementById("colecciones").value+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingFC").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseSelectMessages("muestraFC","secciones", reqs,false);
                                          doFillFechaCaptura(document.getElementById("muestraFC").value);
                                          document.getElementById("loadingFC").removeChild(document.getElementById("loadingFC").childNodes[0]); 
                                           document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else if(reqs.readyState==1){
                                        document.getElementById("loadingFC").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
    
    }
    
     function doFillFechaCaptura(optValue){
        var url="../servletAjaxProcess?action=doFillFechaCaptura&coleccion="+document.getElementById("colecciones").value+"&valor="+optValue+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingCont3").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseSelectMessages("fechacaptura","fechasCapturas", reqs,false);
                                          var seccion=document.getElementById("muestraFC").value;
                                          var ano=document.getElementById("fechacaptura").value;
                                          doEstadFechaCaptura(seccion,ano);
                                           document.getElementById("loadingCont3").removeChild(document.getElementById("loadingCont3").childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                        document.getElementById("loadingCont3").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);
    }
    
     function doEstadFechaCaptura(optValueSeccion, optValueAno){
    
        var url="../servletAjaxProcess?action=doEstadFechaCaptura&coleccion="+document.getElementById("colecciones").value+"&valorSeccion="+optValueSeccion+"&valorAno="+optValueAno+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingCont3").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadSeccFechaMessages("fechacaptura","valor-fecha","Cont3ShowResult", reqs);
                                          document.getElementById("loadingCont3").removeChild(document.getElementById("loadingCont3").childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                        document.getElementById("loadingCont3").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
    }
    
    
    function doSeccionImages(container){
        var url="../servletAjaxProcess?action=doSeccionImages&coleccion="+document.getElementById("colecciones").value+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingEIMG").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseSeccImagesMessages("tbodyEstad4",reqs);
                                          document.getElementById("loadingEIMG").removeChild(document.getElementById("loadingEIMG").childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                        document.getElementById("loadingEIMG").innerHTML=loadstatustext
                                    
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
    
    }
    function parseSeccImagesMessages(id,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var lotes = responses.getElementsByTagName("lotes")[0];
           for (i = 0; i < lotes.childNodes.length; i++) {
                var lote = lotes.childNodes[i];
                var seccion = lote.getElementsByTagName("seccion")[0];
                var conImages = lote.getElementsByTagName("conimages")[0];
                var sinImages = lote.getElementsByTagName("sinimages")[0];
                appendDataTables(seccion.childNodes[0].nodeValue,conImages.childNodes[0].nodeValue,sinImages.childNodes[0].nodeValue,tb,i);
           }
        }
    }
    
    function appendDataTables(valor1,valor2,valor3,tb,i){
          var valor1Cell;
          var valor2Cell;
          var valor3Cell;
          /*si el browser es Internet Explorer*/
          if(isIE){
            row=tb.insertRow(tb.rows.length);
            valor1Cell=row.insertCell(0);    
            valor2Cell=row.insertCell(1);     
            valor3Cell=row.insertCell(1);     
          }else{
             row=document.createElement("tr");
             valor1Cell=document.createElement("td");
             valor2Cell=document.createElement("td");
             valor3Cell=document.createElement("td");
          }
         
          row.appendChild(valor1Cell);
          row.appendChild(valor2Cell);
          row.appendChild(valor3Cell);
          
          if(i%2!=0)
            row.style.backgroundColor="#eff7ff";
          else
            row.style.backgroundColor="#ffffff";
          
          tb.appendChild(row);
          row.setAttribute("border",0);
          
          /*valor1Cell.setAttribute("align","left");*/
          valor1Cell.style.paddingLeft="24px";
          valor1Cell.style.whiteSpace="nowrap";
          valor1Cell.appendChild(document.createTextNode(valor1));
          
          valor2Cell.setAttribute("align","right");
          valor2Cell.style.paddingRight="50px";
          valor2Cell.style.whiteSpace="nowrap";
          valor2Cell.appendChild(document.createTextNode(valor2)); 
          
          valor3Cell.setAttribute("align","right");
          valor3Cell.style.paddingRight="50px";
          valor3Cell.style.whiteSpace="nowrap";
          valor3Cell.appendChild(document.createTextNode(valor3)); 
        
    }
    
    function doFillMuestraPROY(container){
        var url="../servletAjaxProcess?action=doFillMuestraPROY&coleccion="+document.getElementById("colecciones").value+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingPROY").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseSelectMessages("seccionesPROY","secciones", reqs,false);
                                          doEstadSeccProyectos(document.getElementById("seccionesPROY").value);
                                          document.getElementById("loadingPROY").removeChild(document.getElementById("loadingPROY").childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                        document.getElementById("loadingPROY").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
    
    }
    
    
    function doEstadSeccProyectos(optValue){
       
        var url="../servletAjaxProcess?action=doEstadSeccProyectos&coleccion="+document.getElementById("colecciones").value+"&valor="+optValue+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingCont7").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadProyMessages("tbodyEstad7","Cont7ShowResult",reqs);
                                          document.getElementById("loadingCont7").removeChild(document.getElementById("loadingCont7").childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                    
                                        document.getElementById("loadingCont7").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);
    }
    
     function parseEstadProyMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var proyectos = responses.getElementsByTagName("proyectos")[0];
           for (var i = 0; i < proyectos.childNodes.length; i++) {
                var proyecto = proyectos.childNodes[i];
                var titulo = proyecto.getElementsByTagName("titulo")[0];
                var lotes = proyecto.getElementsByTagName("lotes")[0];
                appendDataTable(titulo.childNodes[0].nodeValue,lotes.childNodes[0].nodeValue,tb,"left","center",i);
           }
        }
       
          
   }
   
   function clearElement(element){
        if(element){
           for (var i = element.childNodes.length -1; i >= 0 ; i--)
            {
               element.removeChild(element.childNodes[i]);
            }
         }
   }
   function doViewAll(optValue, container,action,contLoading){
    
    
        var divCont=document.getElementById(container);
        clearElement(divCont);    
        
        var table=document.createElement("table");
        table.style.border="1px solid #d7edfb";
        
        
        var thead=document.createElement("thead");
        var th1=document.createElement("th");
        var th2=document.createElement("th");
        
        var tr=document.createElement("tr");
        th1.appendChild(document.createTextNode("A�o"));
        th2.appendChild(document.createTextNode("Numero Lotes"));
        tr.appendChild(th1);
        tr.appendChild(th2);
        thead.appendChild(tr);
        
        var tbody=document.createElement("tbody");
        var ID=container+optValue;
        tbody.id=ID;
        
        table.appendChild(thead);
        table.appendChild(tbody);
        divCont.appendChild(table);
        
        var url="../servletAjaxProcess?action="+action+"&coleccion="+document.getElementById("colecciones").value+"&valor="+optValue+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById(contLoading).innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseViewAllMessages(ID,container,reqs);
                                           document.getElementById(contLoading).removeChild(document.getElementById(contLoading).childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                        document.getElementById(contLoading).innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);
  
   }
   
   function parseViewAllMessages(id,container,reqs){
        parseTableMessages(id,container,reqs,"cantidad-muestras","ano","lotes","Total Lotes por secci�n: ");
    
   }
   
   
   function doFillMuestraPRES(container)
    {
            
            var url="../servletAjaxProcess?action=doFillMuestraPRES&coleccion="+document.getElementById("colecciones").value+"";
            var reqs=initRequest();
            var spanLoading=document.getElementById("loadingPRES");
            spanLoading.innerHTML=loadstatustext;
            reqs.onreadystatechange=function processRequest()
                                    {
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages("muestraPRES","secciones", reqs,false);
                                              doEstadPreservativos(document.getElementById("muestraPRES").value);
                                               spanLoading.removeChild(spanLoading.childNodes[0]); 
                                              document.getElementById(container).style.display="block";
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }else  {
                                        spanLoading.innerHTML=loadstatustext;
                                     
                                     }
            }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    
    }
    
    function doEstadPreservativos(optValue){
        var url="../servletAjaxProcess?action=doEstadPreservativos&coleccion="+document.getElementById("colecciones").value+"&valor="+optValue;
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont8");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadPresMessages("tbodyEstad8","Cont8ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
  }
  
  function parseEstadPresMessages(id,container,reqs){
        parseTableMessages(id,container,reqs,"cantidad-muestras","preservativo","lotes","Total Lotes por secci�n: ");
        
   }
 
 function doFillMuestraObjRelac(container)
    {
            
            var url="../servletAjaxProcess?action=doFillMuestraObjRelac&coleccion="+document.getElementById("colecciones").value+"";
            var reqs=initRequest();
            var spanLoading=document.getElementById("loadingOBJ");
            spanLoading.innerHTML=loadstatustext;
            reqs.onreadystatechange=function processRequest()
                                    {
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages("muestraOBJR","secciones", reqs,false);
                                              doEstadObjRelacionado(document.getElementById("muestraOBJR").value);
                                               spanLoading.removeChild(spanLoading.childNodes[0]); 
                                              document.getElementById(container).style.display="block";
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }else  {
                                        spanLoading.innerHTML=loadstatustext;
                                     
                                     }
            }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    
    }
    
    function doEstadObjRelacionado(optValue){
        var url="../servletAjaxProcess?action=doEstadObjRelacionado&coleccion="+document.getElementById("colecciones").value+"&valor="+optValue;
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont9");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadObjRelacMessages("tbodyEstad9","Cont9ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
  }
  
  function parseEstadObjRelacMessages(id,container,reqs){
        parseTableMessages(id,container,reqs,"cantidad-muestras","objeto","lotes","Total Nro de Objetos Relacionados: " );
        
   }
   
   function doFillSeccionINV(container){
   
        var url="../servletAjaxProcess?action=doFillSeccionINV&coleccion="+document.getElementById("colecciones").value+"";
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEINV");
        spanLoading.innerHTML=loadstatustext;
        reqs.onreadystatechange=function processRequest()
                                {
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages("MuestraINV","secciones", reqs,false);
                                              doEstadInvestigadores(document.getElementById("MuestraINV").value);
                                               spanLoading.removeChild(spanLoading.childNodes[0]); 
                                              document.getElementById(container).style.display="block";
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }else  {
                                        spanLoading.innerHTML=loadstatustext;
                                     
                                     }
                               }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    }

   function doEstadInvestigadores(optValue){
   		var colec=document.getElementById("colecciones").value;
        var url="../servletAjaxProcess?action=doEstadInvestigadores&coleccion="+colec+"&valor="+optValue;
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont5");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadInvestMessages("tbodyEstad5","Cont5ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
										  
										  var params="action=doEstadInvestigadores&valor="+escape(optValue)+"&coleccion="+colec;
										  createLinkDowndload(params,"linkDownload5");
										  
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
   }
   
   function parseEstadInvestMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var investigadores = responses.getElementsByTagName("investigadores")[0];
           
           for (var i = 0; i < investigadores.childNodes.length; i++) {
                var investigador = investigadores.childNodes[i];
                var nombre = investigador.getElementsByTagName("nombre")[0].childNodes[0].nodeValue;
                var tarea = investigador.getElementsByTagName("tarea")[0].childNodes[0].nodeValue;
                var ano = investigador.getElementsByTagName("ano")[0].childNodes[0].nodeValue;
                var lotes = investigador.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                appendData(nombre,tarea,ano,lotes,tb,i);
           }
        }
        
       
    
   }
   
   function appendData(valor1,valor2,valor3,valor4,tb,i, aling){
   
          var valor1Cell;
          var valor2Cell;
          var valor3Cell;
          var valor4Cell;
          /*si el browser es Internet Explorer*/
          if(isIE){
            row=tb.insertRow(tb.rows.length);
            valor1Cell=row.insertCell(0);    
            valor2Cell=row.insertCell(1);     
            valor3Cell=row.insertCell(2);     
            valor4Cell=row.insertCell(3);     
          }else{
             row=document.createElement("tr");
             valor1Cell=document.createElement("td");
             valor2Cell=document.createElement("td");
             valor3Cell=document.createElement("td");
             valor4Cell=document.createElement("td");
          }
         
          row.appendChild(valor1Cell);
          row.appendChild(valor2Cell);
          row.appendChild(valor3Cell);
          row.appendChild(valor4Cell);
          if(i%2!=0)
             row.style.backgroundColor="#eff7ff";
           else
             row.style.backgroundColor="#ffffff";
          tb.appendChild(row);
          row.setAttribute("border",0);
          
          valor1Cell.setAttribute("align",aling);
          valor1Cell.style.padding="0 10px";
          valor1Cell.style.whiteSpace="nowrap";
          valor1Cell.appendChild(document.createTextNode(valor1));
          
          valor2Cell.setAttribute("align","left");
          valor2Cell.style.padding="0 10px";
          valor2Cell.style.whiteSpace="nowrap";
          valor2Cell.appendChild(document.createTextNode(valor2)); 
          
          valor3Cell.setAttribute("align","left");
          valor3Cell.style.padding="0 10px";
          valor3Cell.style.whiteSpace="nowrap";
          valor3Cell.appendChild(document.createTextNode(valor3)); 
   
          valor4Cell.setAttribute("align","left");
          valor4Cell.style.padding="0 10px";
          valor4Cell.style.whiteSpace="nowrap";
          valor4Cell.appendChild(document.createTextNode(valor4)); 
   
   }
   
   function doFillInvestigadores(container){
      
        var url="../servletAjaxProcess?action=doFillInvestigadores&coleccion="+document.getElementById("colecciones").value+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingInvest");
        spanLoading.innerHTML=loadstatustext;
        reqs.onreadystatechange=function processRequest()
                                {
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages("invest","investigadores", reqs,false);
                                              doEstadInvest(document.getElementById("invest").value);
                                               spanLoading.removeChild(spanLoading.childNodes[0]); 
                                              document.getElementById(container).style.display="block";
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }else  {
                                        spanLoading.innerHTML=loadstatustext;
                                     
                                     }
                               }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    }

   function doEstadInvest(optValue){
        
		var colec=document.getElementById("colecciones").value;
		var url="../servletAjaxProcess?action=doEstadInvest&coleccion="+colec+"&valor="+escape(optValue)+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont6");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstInvMessages("tbodyEstad6","Cont6ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
										  
										  var params="action=doEstadInvest&valor="+escape(optValue)+"&coleccion="+colec;
										  createLinkDowndload(params,"linkDownload6");
										  
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   }
 
   function parseEstInvMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var investigadores = responses.getElementsByTagName("investigador")[0];
           
           for (var i = 0; i < investigadores.childNodes.length; i++) {
                var investigador = investigadores.childNodes[i];
                var ano = investigador.getElementsByTagName("ano")[0].childNodes[0].nodeValue;
                var seccion = investigador.getElementsByTagName("seccion")[0].childNodes[0].nodeValue;
                var tarea = investigador.getElementsByTagName("tarea")[0].childNodes[0].nodeValue;
                var lotes = investigador.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                appendData(ano,seccion,tarea,lotes,tb,i,"left");
           }
        }
        
       
           
   }
   
   
   /****************************************************************************************/
   function doEstadByPhylum(container){
          doMuestrasByPhylum(container);
         // doFichaEspecByPhylum(container);
         // doEspecieByPhylum(container);  
          
          
   }
   
   function doMuestrasByPhylum(container){
        var url="../servletAjaxProcess?action=doMuestrasByPhylum&coleccion="+document.getElementById("colecciones").value+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEstdPhylum");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadMByPhylumMessages("tbodyEstad10",reqs,"muestras");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
   }
   
   function parseEstadMByPhylumMessages(id,reqs,tagName){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var muestras = responses.getElementsByTagName(tagName)[0];
           
           for (i = 0; i < muestras.childNodes.length; i++) {
                var muestra = muestras.childNodes[i];
                
                var phylum = muestra.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                
                var cantidad = muestra.getElementsByTagName("cantidad")[0].childNodes[0].nodeValue;
                //alert(cantidad.childNodes[0].nodeValue)
                var porcentage = muestra.getElementsByTagName("porcentages")[0].childNodes[0].nodeValue;
               // alert(procentage.childNodes[0].nodeValue)
                appendDataTables(phylum,cantidad,porcentage,tb,i);
           }
        }
   }
   
   function doFichaEspecByPhylum(container){
       var url="../servletAjaxProcess?action=doFichaEspecByPhylum&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEstdPhylum1");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadMByPhylumMessages("tbodyEstad101",reqs,"fichas");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
   }
   
   
   
   function doEspecieByPhylum(container){
        var url="../servletAjaxProcess?action=doEspecieByPhylum&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEstdPhylum2");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                         parseEstadMByPhylumMessages("tbodyEstad102",reqs,"especies");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null); 
   }
   
   /*************************************************************************************************/
   /***************************************************************************************************/
   
   function datos(){
    this.newfuncion;
    this.id;
    this.spanLoadId;
   
   }
   
   function doFillPhylum(container){
   
        
        var data=new datos();
        
        if(container=="contenedor11"){
            data.id="nivelPhylum";
            data.newfuncion=doEstadNivelPhylum;
            data.spanLoadId="loadingPhylum";
            
            
        }else if(container=="contenedor12"){
            data.id='RnivelTaxPhylum';
            data.newfuncion=doEstadNivelTax;
            data.spanLoadId="loadingPhylums";
            
        }else if(container=="contenedor13"){
            data.id='especienivelTax';
            data.newfuncion=doEstadEspeciesNivelTax;
            data.spanLoadId="loadingPhyl";
            
        }else if(container=="contenedor14"){
            data.id='elemphylum';
            data.newfuncion=doEstadElementosNivelTax;
            data.spanLoadId="loadingPhylu";
            
        }else if(container=="contenedor15"){
            data.id='phylum';
            data.newfuncion=doEstadElementDiccNivelTax;
            data.spanLoadId="loadingP";
        }
           
            
        
        
        doFillPhyl(container,data);
        
   
   }
   function doFillPhyl(container,data){
        var url="../servletAjaxProcess?action=doFillPhylum&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById(data.spanLoadId);
        spanLoading.innerHTML=loadstatustext;
       // alert(datos.dat[0].funcion)
        //var newData=datos.dat[0];
        reqs.onreadystatechange=function processRequest()
                                {
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages(data.id,"phylums", reqs,true);
                                              //doEstadNivelPhylum(document.getElementById("nivelPhylum").value);
                                              data.newfuncion();
                                               spanLoading.removeChild(spanLoading.childNodes[0]); 
                                              document.getElementById(container).style.display="block";
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }else  {
                                        spanLoading.innerHTML=loadstatustext;
                                     
                                     }
                               }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    
   }
   
   function doEstadNivelPhylum(){
        var optValue=document.getElementById("nivelPhylum").value;
		var colecc=document.getElementById("nivelColeccion").value;
		var proy=document.getElementById("nivelProyectos").value;
        var url="../servletAjaxProcess?action=doEstadNivelPhylum&valor="+escape(optValue)+"&coleccion="+escape(colecc)+"&proyecto="+escape(proy)+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont11");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          
                                          parseEstNivelPhylumMessages("tbodyEstad11","Cont11ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   
   }
   
   
   function  parseEstNivelPhylumMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var taxonomias = responses.getElementsByTagName("taxonomias")[0];
           
           for (var i = 0; i < taxonomias.childNodes.length; i++) {
                var taxon = taxonomias.childNodes[i];
                var codigo = taxon.getElementsByTagName("codigo")[0].childNodes[0].nodeValue;
                var nombre = taxon.getElementsByTagName("nombre")[0].childNodes[0].nodeValue;
                var phylum = taxon.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                var lotes = taxon.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                appendData(codigo,phylum,nombre,lotes,tb,i,'center');
           }
        }
        
      
           
   }
   
   
   /**************************************************************************************************/
   /*********************************************************************************************/
   
   function doEstadNivelTax(){
        var optValue=document.getElementById("RnivelTaxPhylum").value;
        var url="../servletAjaxProcess?action=doEstadNivelTax&valor="+escape(optValue);
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont12");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstNivelTaxMessages("tbodyEstad12","Cont12ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   
   }
   
   
   function  parseEstNivelTaxMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var taxonomias = responses.getElementsByTagName("taxonomias")[0];
           
           for (var i = 0; i < taxonomias.childNodes.length; i++) {
                var taxon = taxonomias.childNodes[i];
                var codigo = taxon.getElementsByTagName("codigo")[0].childNodes[0].nodeValue;
                var phylum = taxon.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                var lotes = taxon.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                var elementos = taxon.getElementsByTagName("elementos")[0].childNodes[0].nodeValue;
                
                appendData(codigo,phylum,elementos,lotes,tb,i,'center');
           }
        }
        
        
   }
   
   /***************************************************************************************************/
  /***********************************************************************************************/
  
  function doEstadEspeciesNivelTax(){
        var optValue=document.getElementById("especienivelTax").value;
        var url="../servletAjaxProcess?action=doEstadEspeciesNivelTax&valor="+escape(optValue)+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont13");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadEspeciesNivelTaxMessages("tbodyEstad13","Cont13ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   
    }
    
    function parseEstadEspeciesNivelTaxMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var taxonomias = responses.getElementsByTagName("taxonomias")[0];
           
           for (var i = 0; i < taxonomias.childNodes.length; i++) {
                var taxon = taxonomias.childNodes[i];
                var codigo = taxon.getElementsByTagName("codigo")[0].childNodes[0].nodeValue;
                var phylum = taxon.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                var nombre = taxon.getElementsByTagName("nombre")[0].childNodes[0].nodeValue;
                var especimenes = taxon.getElementsByTagName("especimenes")[0].childNodes[0].nodeValue;
                
                appendData(codigo,phylum,nombre,especimenes,tb,i,'center');
           }
        }
        
        
    }
  /*****************************************************************************************/
  function doEstadElementosNivelTax(){
       var optValue=document.getElementById("elemphylum").value;
        var url="../servletAjaxProcess?action=doEstadElementosNivelTax&valor="+escape(optValue)+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont14");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadElementNivelTaxMessages("tbodyEstad14","Cont14ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   
    }
    
    function parseEstadElementNivelTaxMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var taxonomias = responses.getElementsByTagName("taxonomias")[0];
           
           for (var i = 0; i < taxonomias.childNodes.length; i++) {
                var taxon = taxonomias.childNodes[i];
                var codigo = taxon.getElementsByTagName("codigo")[0].childNodes[0].nodeValue;
                var phylum = taxon.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                var nombre = taxon.getElementsByTagName("nivel")[0].childNodes[0].nodeValue;
                var especimenes = taxon.getElementsByTagName("elementos")[0].childNodes[0].nodeValue;
                
                appendData(codigo,phylum,nombre,especimenes,tb,i,'center');
           }
        }
        
      
    }
    
    /*********************************************************************************************/
    /**********************************************************************************************************/
    function doEstadElementDiccNivelTax(){ 
        var optValue=document.getElementById("phylum").value;
        var url="../servletAjaxProcess?action=doEstadElementDiccNivelTax&valor="+escape(optValue)+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont15");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadElemDiccNivelTaxMessages("tbodyEstad15","Cont15ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   
    }
    
    function parseEstadElemDiccNivelTaxMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var taxonomias = responses.getElementsByTagName("taxonomias")[0];
           
           for (var i = 0; i < taxonomias.childNodes.length; i++) {
                var taxon = taxonomias.childNodes[i];
                var codigo = taxon.getElementsByTagName("codigo")[0].childNodes[0].nodeValue;
                var phylum = taxon.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                var nivel = taxon.getElementsByTagName("nivel")[0].childNodes[0].nodeValue;
                var nombre = taxon.getElementsByTagName("nombre")[0].childNodes[0].nodeValue;
                
                appendData(codigo,phylum,nivel,nombre,tb,i,'center');
           }
        }
        
        }
        
      /*************************************************************************************************/
      function doEstadLotesByAreaGeograf(container){
        var url="../servletAjaxProcess?action=doEstadLotesByAreaGeograf";
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEstdareaGeograf");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadLotesAreaGeoMessages("tbodyEstad18",reqs,"areas");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
     }
     
      function parseEstadLotesAreaGeoMessages(id,reqs,tagName){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var areas = responses.getElementsByTagName(tagName)[0];
           
           for (i = 0; i < areas.childNodes.length; i++) {
                var area = areas.childNodes[i];
                
                var latitud = area.getElementsByTagName("latitud")[0].childNodes[0].nodeValue;
                
                var longitud = area.getElementsByTagName("longitud")[0].childNodes[0].nodeValue;
                //alert(cantidad.childNodes[0].nodeValue)
                var lotes = area.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
               // alert(procentage.childNodes[0].nodeValue)
                appendDataTables(latitud,longitud,lotes,tb,i);
           }
        }
   }
   
   function doEstadMantenimiento(container){
   
            doEstadMantenim(container);
            doEstadMantenimtoConFaltantes(container);
            
   }
   function doEstadMantenim(container){
        var url="../servletAjaxProcess?action=doEstadMantenimiento&coleccion="+document.getElementById("colecciones").value;
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingManten");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadMantenimiento("tbodyEstad19",reqs,"datos");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
   
   }
   
   function parseEstadMantenimiento(id,reqs,tagname){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var datos = responses.getElementsByTagName("datos")[0];
           
           for (var i = 0; i < datos.childNodes.length; i++) {
                var dato = datos.childNodes[i];
                var seccion = dato.getElementsByTagName("seccion")[0].childNodes[0].nodeValue;
                var ano = dato.getElementsByTagName("ano")[0].childNodes[0].nodeValue;
                var tarea = dato.getElementsByTagName("tarea")[0].childNodes[0].nodeValue;
                var lotes = dato.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                
                appendData(seccion,ano,tarea,lotes,tb,i,'center');
           }
        }
        
        }
   
    function doEstadMantenimtoConFaltantes(container){
        var url="../servletAjaxProcess?action=doEstadMantenimtoConFaltantes&coleccion="+document.getElementById("colecciones").value;
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingManten");
        //spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadMantenimConFaltantes("tbodyEstad191",reqs,"datos");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
   
   }
   
   function parseEstadMantenimConFaltantes(id,reqs,tagname){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var datos = responses.getElementsByTagName("datos")[0];
           
           for (var i = 0; i < datos.childNodes.length; i++) {
                var dato = datos.childNodes[i];
                var seccion = dato.getElementsByTagName("seccion")[0].childNodes[0].nodeValue;
                var tarea = dato.getElementsByTagName("tarea")[0].childNodes[0].nodeValue;
                var lotes = dato.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                var faltantes = dato.getElementsByTagName("faltantes")[0].childNodes[0].nodeValue;
                appendData(seccion,tarea,lotes,faltantes,tb,i,'center');
           }
        }
        
    }
   
   
   
    function doEstadNomComuneSinonimias(container){
        var url="../servletAjaxProcess?action=doEstadNomComuneSinonimias";
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingSinNom");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadVulgaresSinonimias("tbodyEstad20",reqs,"datos");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
   
   }
   
   function  parseEstadVulgaresSinonimias(id,reqs,tagname){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var datos = responses.getElementsByTagName("datos")[0];
           
           for (var i = 0; i < datos.childNodes.length; i++) {
                var dato = datos.childNodes[i];
                var phylum = dato.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                var vulgares = dato.getElementsByTagName("vulgares")[0].childNodes[0].nodeValue;
                var sinonimias = dato.getElementsByTagName("sinonimias")[0].childNodes[0].nodeValue;
                
                appendDataTables(phylum,vulgares,sinonimias,tb,i,'center');
           }
        }
        
    }
    
      function doLotesOtrasEntidades(container){
        var url="../servletAjaxProcess?action=doLotesOtrasEntidades&coleccion="+document.getElementById("colecciones").value;
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEstdOENT");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadLotesOEntidad("tbodyEstad21",reqs,"datos");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
   
   }
   
   function parseEstadLotesOEntidad(id,reqs,tagname){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var datos = responses.getElementsByTagName("datos")[0];
           
           for (var i = 0; i < datos.childNodes.length; i++) {
                var dato = datos.childNodes[i];
                var seccion = dato.getElementsByTagName("seccion")[0].childNodes[0].nodeValue;
                var institucion = dato.getElementsByTagName("institucion")[0].childNodes[0].nodeValue;
                var modalidad = dato.getElementsByTagName("modalidad")[0].childNodes[0].nodeValue;
                var lotes = dato.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                appendData(seccion,institucion,modalidad,lotes,tb,i,'center');
           }
        }
        
    }
    
    
      function doLotesByTipos(container){
        var url="../servletAjaxProcess?action=doLotesByTipos&coleccion="+document.getElementById("colecciones").value;
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEstdLTIPOS");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadLTipos("tbodyEstad22",reqs,"datos");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
   
   }
   
   function  parseEstadLTipos(id,reqs,tagname){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var datos = responses.getElementsByTagName("datos")[0];
           
           for (var i = 0; i < datos.childNodes.length; i++) {
                var dato = datos.childNodes[i];
                var seccion = dato.getElementsByTagName("seccion")[0].childNodes[0].nodeValue;
                var tipo = dato.getElementsByTagName("tipo")[0].childNodes[0].nodeValue;
                var lotes = dato.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                
                appendDataTables(seccion,tipo,lotes,tb,i,'center');
           }
        }
        
    }

	
	function searchBasic(valor){
		var sBasic=document.getElementById("searchBasic");
		var sAdvance=document.getElementById("searchAdvance");
		sBasic.style.display="block";
		if(sAdvance.style.display=="block"){
			sAdvance.style.display="none"
		}
	}
	
	function searchAdvance(valor){
		var sBasic=document.getElementById("searchBasic");
		var sAdvance=document.getElementById("searchAdvance");
		sAdvance.style.display="block";	
		
		if(sBasic.style.display=="block"){
			sBasic.style.display="none"
		}
	}
	function getSearchType(fm,inpname){
	
		var radioValue = Form.getInputs(fm,'radio',inpname).find(function(radio) { return radio.checked; }).value;
	/*radioValue="";
    var i
    for (i=0;i<document.searchForms.opcnivel.length;i++){
       if (document.searchForms.opcnivel[i].checked)
           radioValue = document.searchForms.opcnivel[i].value
    }*/
   

		return  radioValue;
	}
	function searchNivelTaxBA(){
		var searchType=getSearchType('searchForms','opcnivel');
		//console.log(searchType);
		if(searchType=="basic"){
			doEstadNivelPhylumBasic();
		}else if(searchType=="advance"){
			doEstadNivelPhylum($('nivelPhylum').value);
		}else {
			alert("Seleccione el tipo de busqueda que desea realizar");
		}
		
	}
	
	function doEstadNivelPhylumBasic(){
		var phylumOpt = document.getElementById("nivelPhylum");
		var phylum= phylumOpt.options[phylumOpt.selectedIndex].text
        var optValue=document.getElementById("nivelPhylum").value;
		var colecc=document.getElementById("nivelColeccion").value;
		var proy=document.getElementById("nivelProyectos").value;
		var nivel54=document.getElementById("identificados");
		var nivel="false";
		if(nivel54.checked){
			nivel="true";
		}
		
        var url="../servletAjaxProcess?action=doEstadNivelPhylumBasic&valor="+escape(optValue)+"&coleccion="+escape(colecc)+"&proyecto="+escape(proy)+"&nivel54="+nivel+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont11");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                               			   dataRetriveEstad('searchBasic',reqs);	           
                                          //parseEstNivelPhylumMessages("tbodyEstad11","Cont11ShowResult",reqs);
										  var params="action=doEstadNivelPhylumBasic&phylum="+phylum+"&valor="+optValue+"&coleccion="+colecc+"&proyecto="+proy+"&nivel54="+nivel
										  createLinkDowndload(params,"linkDownload2");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   
   
	}
	
	function dataRetriveEstad(divID,reqs){
		var container = document.getElementById(divID);
		clearTableTBODY(container);
		//console.log("dataRetrive");
		
		var newdiv = document.createElement("div");
		newdiv.innerHTML = reqs.responseText;
		
		
		container.appendChild(newdiv);
		
		
	}
	
	function doFillSelects(container,idColecc,idProy){
		doFillPhylum(container);
		doFillColeccion(idColecc);
		doFillListProyectos(idProy);
		
		
	}
	
	function createLinkDowndload(params,objID){
		 var linkElement = document.createElement("a");
		 var url="downloadExcelService?"+params;
		 var obj=document.getElementById(objID);
		 linkElement.setAttribute("href",url);
		 
		 linkElement.appendChild(document.createTextNode("Descargar datos en formato Microsoft Excel"));
		 obj.removeChild(obj.childNodes[0]);
		 obj.appendChild(linkElement);
		 obj.style.display="block";
	}
	
	function searchRMuseoByNivelBA(){
		
        var optValue=document.getElementById("RnivelTaxPhylum").value;
		var colecc=document.getElementById("RnivelTaxColeccion").value;
		var proy=document.getElementById("RnivelTaxProyectos").value;
		
		
        var url="../servletAjaxProcess?action=dosearchRMuseoByNivel&valor="+escape(optValue)+"&coleccion="+escape(colecc)+"&proyecto="+escape(proy)+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont12");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                               			   dataRetriveEstad('RsearchB',reqs);	           
                                          										  
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
										  
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
	}
	