<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%
	String user=(String)session.getAttribute("USER");

	if(user==null){
		session.removeAttribute("USER");
		session.invalidate();
		response.sendRedirect("index.jsp");
	}
 %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
  <head>
<title>Estadisticas del Museo de Historia Natural Marina de Colombia</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<style type="text/css">
    a:link {
        text-decoration: none;
    }
    a:visited {
        text-decoration: none;
    }
    a:hover {
        text-decoration: underline;
   }
   
   .classTHeader{
        color: #006666; 
        padding: 5px 20px;
   }
   
   table {
   	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
   
   }
   
  table thead th{
      color: #f7f7f7;
      background-color: #295d94; 
      padding: 5px 30px;
  
  }
  
  table tfoot{
  
        background-color: #295d94; 
  }

</style>
<script type="text/javascript" src="../../1ibre/prototype.js"></script>
<script type="text/javascript" src="../jscript/panel.js"></script>
<script type="text/javascript" src="js/stats.js"></script>
<script type="text/javascript">
<!--

//-->
//if (window.addEventListener)
	//window.addEventListener("load", doFillColeccion, false)
//else if (window.attachEvent)
	//window.attachEvent("onload", doFillColeccion)
</script>
</head>
  
  <body style=" background-color: #d7edfb;" >

	<%@ include file="header.htm" %>
	<form id="searchForms" name="searchForms" method="post" action="#">
	<div id="main" style="margin-left: 100px; ">
	 <h1>.:: Informaci&oacute;n por grupos, Registros Biol&oacute;gicos ::.</h1>
		 <p />
        <b style="color: #006666;">1.</b> 
    <a href="javascript:doEstadLotesByAreaGeograf('contenedor18');" style="color: #006666; font-weight: bold;">
       Distribucion de registros biol&oacute;gicos por area geogr&aacute;fica (55 km)
    </a>
    <span id="loadingEstdareaGeograf" style="padding-left: 20px;"></span>
    <div id="contenedor18" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px;border: 1px solid #006666;" >
        
        <table id="estadistica18" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Latitud</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Longitud</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad18"></tbody>
        </table>
        <p ></p>
        
        <a href="javascript:mpanel('contenedor18');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    
     <p></p>
   <b style="color: #006666;">2.</b>
    <a href="javascript:doFillSelects('contenedor11','nivelColeccion','nivelProyectos');" style="color: #006666; font-weight: bold;" >
       Distribuci&oacute;n de registros biol&oacute;gicos por niveles taxon&oacute;micos.
    </a>
    <span id="loadingPhylum" style="padding-left: 20px;"></span>

    <div id="contenedor11" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 90px; border: 1px solid #006666;" >
    	<p>
    		
        <label>
          <input type="radio" name="opcnivel" value="basic" onclick="searchBasic(this.value);" checked="checked" />
          Grupos Taxon&oacute;micos principales</label>
        
        <label>
          <input type="radio" name="opcnivel" value="advance" onclick="searchAdvance(this.value);" />
          Todos los grupos taxon&oacute;micos (Esta operación puede tardar varios minutos)</label>
        <br />
      </p>
    	<div>
    		<input id="identificados" name="identificados" type="checkbox" value="nivel54"/>
			Solo registros biol&oacute;gicos identificados 
    	</div>
    	
        <p style="color: #003063; font-weight: bold;">
			<table border="0" cellpadding="0" cellspacing="0">
			 <tr><td width="121">C&oacute;digo Phylum: </td>
            	  <td width="413"><select id="nivelPhylum" >
                    	<option value="" selected="selected" >----------------</option>
            		  </select>
			   </td>
			</tr>
			<tr>
    		<td><img src="../../images/spacer.gif" width="5" height="5"></td> <td><img src="../../images/spacer.gif" width="5" height="5"></td>
  		</tr>
			<tr>
            <td>Colecci&oacute;n:</td>
			<td>
				<select id="nivelColeccion" >
						<option value="" selected="selected" >----------------</option>
				</select>
			</td>
			</tr>
			<tr>
    		<td><img src="../../images/spacer.gif" width="5" height="5"></td> <td><img src="../../images/spacer.gif" width="5" height="5"></td>
  		</tr>
			<tr>
            <td>Proyecto:</td>
			 <td>
				<select id="nivelProyectos" >
						<option value="" selected="selected" >----------------</option>
				</select>
			  </td>
			</tr>
			<tr>
    		<td><img src="../../images/spacer.gif" width="5" height="5"></td> <td><img src="../../images/spacer.gif" width="5" height="5"></td>
  		</tr>
			<tr>
			<td>&nbsp;</td>
            <td><input name="Buscar" value="Buscar" type="button" onclick="searchNivelTaxBA();" /></td>
			</tr>
			</table>
            <span id="loadingCont11" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  		<div id="linkDownload2" style="display:none"><a href="downloadExcelService?valor=54&coleccion=&amp;proyecto=">Descargar datos en formato Microsoft Excel</a></div>
  		<div id="searchBasic" style="display:block">
  			
        </table>
  			
  		
  		</div>
  		
  		<div id="searchAdvance" style="display:none">
	        <table id="estadistica11" style="margin-left: 50px; border: 1px solid #d7edfb;">
	            <caption></caption>
	            <thead>
	                <tr>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Cd. Nivel</th>
	                    
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Nivel</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Nombre</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
	                </tr>
	            </thead>
	            <tfoot></tfoot>
	            <tbody id="tbodyEstad11"></tbody> 
	        </table>
	    </div>
        <div id="Cont11ShowResult"></div>
        <a href="javascript:mpanel('contenedor11');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
       
   <b style="color: #006666;">3.</b>
    <a href="javascript:doFillSelects('contenedor12','RnivelTaxColeccion','RnivelTaxProyectos');" style="color: #006666; font-weight: bold;" >
       Registros biol&oacute;gicos en museo por nivel por phylum
    </a>
    <span id="loadingPhylums" style="padding-left: 20px;"></span>

    <div id="contenedor12" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display:none; margin-right: 450px; border: 1px solid #006666;" >
        		
  		 <p style="color: #003063; font-weight: bold;">
			<table border="0" cellpadding="0" cellspacing="0">
			 <tr><td width="121">C&oacute;digo Phylum: </td>
            	  <td width="413"><select id="RnivelTaxPhylum" >
                    	<option value="" selected="selected" >----------------</option>
            		  </select>
			   </td>
			</tr>
			<tr>
    		<td><img src="../../images/spacer.gif" width="5" height="5"></td> <td><img src="../../images/spacer.gif" width="5" height="5"></td>
  		</tr>
			<tr>
            <td>Colecci&oacute;n:</td>
			<td>
				<select id="RnivelTaxColeccion" >
						<option value="" selected="selected" >----------------</option>
				</select>
			</td>
			</tr>
			<tr>
    		<td><img src="../../images/spacer.gif" width="5" height="5"></td> <td><img src="../../images/spacer.gif" width="5" height="5"></td>
  		</tr>
			<tr>
            <td>Proyecto:</td>
			 <td>
				<select id="RnivelTaxProyectos" >
						<option value="" selected="selected" >----------------</option>
				</select>
			  </td>
			</tr>
			<tr>
    		<td><img src="../../images/spacer.gif" width="5" height="5"></td> <td><img src="../../images/spacer.gif" width="5" height="5"></td>
  		</tr>
			<tr>
			<td>&nbsp;</td>
            <td><input name="Buscar" value="Buscar" type="button" onclick="searchRMuseoByNivelBA();" /></td>
			</tr>
			</table>
            <span id="loadingCont12" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
		<div id="RsearchB">
			
		
		</div>
		<div id="RsearchA"  style="display:none">
        <table id="estadistica12" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Cd. Nivel</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Nivel</th>
                    <th style="margin: 6px 10px; padding:10px 15px; ">Elementos en este nivel</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                    
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad12"></tbody> 
        </table>
	    </div>
          <div id="Cont12ShowResult"></div>
        <a href="javascript:mpanel('contenedor12');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
       
 
    <p></p>
	
	
	
	</div>
	</form>
	 <%@ include file="footer.htm" %>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2300620-1");
pageTracker._trackPageview();
} catch(err) {}
</script>

  </body>
</html>
