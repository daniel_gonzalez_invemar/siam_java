
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">

<head>

<title>.::Sistema de Informaci&oacute;n Ambiental Marina de Colombia - SIAM::.</title>

<meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC, SIPEIN,pequeria"/>

<meta http-equiv="pragma" content="no-cache"/>

<meta http-equiv="cache-control" content="no-cache"/>

<meta http-equiv="expires" content="0"/>
 <link type='text/css' rel="stylesheet" href="../../plantillaSitio/css/misiamccs.css"/>
 <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
</head>
<body>
<jsp:include page="../../plantillaSitio/headermodulosEstadisticas.jsp?idsitio=30"/>
<div class="centrado">
<table width="963px" style="border:0px;padding:0px;border-spacing:0px">
  <tr>
    <td colspan="2"><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td>
  </tr>
  <tr style="background-color:#8DC0E3">
   <td width="21"><img src="../../images/arrow2.gif" width="20" height="20" alt="" /></td>
   <td width="963px" style="background-color:#8DC0E3" class="linksnegros">Sistema de Informaci&oacute;n Ambiental Marina de Colombia - SIAM</td>
  </tr>
</table>
<table width="963px" style="border:0px;padding:0px;border-spacing:0px">
  <tr>
    <td><img src="../../images/spacer.gif" width="5" height="5" alt='SPACER' /></td>
  </tr>
  <tr>
  </tr>
  <tr>
    <td><img src="../../images/spacer.gif" width="5" height="5" alt='SPACER' /></td>
  </tr>
</table>
<table width="963px" style="border:0px;padding:0px;border-spacing:0px">
  <tr>
    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
    <td width="730" style="background-color:#B5D7DE" class="texttablas">::Listado de Estadisticas ::</td>
    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td style="background-color:#E6E6E6">&nbsp;</td>
    <td style="background-color:#E6E6E6;font-family:Arial, Helvetica, sans-serif;font-size:1em;">A continuaci&oacute;n se presenta el listado de estadisticas que pueden ser consultadas:<br/>
    <table style="border:0px;padding:0px;border-spacing:0px" width="730">
      <tr>
        <td width="16"><img src="../../images/grey_arrow.jpg" width="16" height="16" style="border:0" alt='SPACER' /></td>
        <td width="714" style="font-family:Arial, Helvetica, sans-serif;font-size:1em;">Estad�sticas para la administraci&oacute;n del Museo de Historia Natural Marina de Colombia: N�mero de lotes por colecci�n, por preservativo usado, por fecha de ingreso y otras</td>
      </tr>
    </table> <table style="border:0px;padding:0px;border-spacing:0px" width="730">
      <tr>
        <td width="16"><img src="../../images/grey_arrow.jpg" width="16" height="16" style="border:0" alt='SPACER' /></td>
        <td width="714" style="font-family:Arial, Helvetica, sans-serif;font-size:1em;">Informaci&oacute;n para la administraci�n del SIBM: numero de registros en los diccionarios de especies, cat�logo de especies y numero de sinonimias</td>
      </tr>
    </table>
	 <table style="border:0px;padding:0px;border-spacing:0px" width="730">
      <tr>
        <td width="16"><img src="../../images/grey_arrow.jpg" width="16" height="16" style="border:0"  alt='SPACER' /></td>
        <td width="714" style="font-family:Arial, Helvetica, sans-serif;font-size:1em;">Informaci&oacute;n estad�stica por grupos tax�nomicos de los registros biol&oacute;gicos almacenados en el SIBM</td>
      </tr>
    </table>
	
	</td>
    <td style="background-color:#E6E6E6">&nbsp;</td>
  </tr>
      
  
</table>


<table width="963px" style="border:0px;padding:0px;border-spacing:0px">
  <tr>
    <td><img src="../../images/spacer.gif" width="5" height="5" alt='SPACER' /></td>
  </tr>
  <tr>
  </tr>
  <tr>
    <td><img src="../../images/spacer.gif" width="5" height="5"  alt="" /></td>
  </tr>
</table>
<table width="963px" style="border:0px;padding:0px;border-spacing:0px">
  <tr>
    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
    <td width="730" style="background-color:#B5D7DE" class="texttablas">:: Ingreso ::</td>
    <td width="10" style="background-color:#B5D7DE">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td>
  </tr>
  <tr>
    <td style="background-color:#E6E6E6">&nbsp;</td>
    <td style="background-color:#E6E6E6">
	<form action="loginService" method="post">
	<input type="hidden" name="page" value="statSIBM" />
	<table style="border:0px;padding:0px;border-spacing:0px" width="730">
		<tr>
    		<td><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td> <td><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td>
  		    <td>&nbsp;</td>
		</tr>
  		<tr>
    		<td><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td> <td><img src="../../images/spacer.gif" width="5" height="5" alt=" " /></td>
  		    <td>&nbsp;</td>
  		</tr>
  		<tr>
    		<td><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td> <td><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td>
  		    <td>&nbsp;</td>
  		</tr>
  		<tr>
    		<td><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td> <td><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td>
  		    <td>&nbsp;</td>
  		</tr>
    	<tr>
        <td width="99" class="texttablas">Nombre de usuario: </td> 
        <td width="195"> <input name="username" type="text" title="Nombre"/></td>
        <td width="436" rowspan="4" style="font-family:Arial, Helvetica, sans-serif;font-size:1em;vertical-align:top">Para obtener usuario y contrase&ntilde;a por favor enviar correo electronico a: <a href="mailto:sinam@invemar.org.co">Administrador SIAM</a></td>
    	</tr>
	  <tr>
    	<td><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td><td><img src="../../images/spacer.gif" width="5" height="5" alt='SPACER' /></td>
  	    </tr>
	  <tr>
        <td width="99" class="texttablas">Contrase&ntilde;a: </td> 
        <td width="195"> <input name="password" type="password" title="Password"/></td>
        </tr>
	  <tr>
    	<td><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td><td><img src="../../images/spacer.gif" width="5" height="5" alt='SPACER' /></td>
  	    </tr>
  	<%
		String login=request.getParameter("login");
		if(login!=null && login.equalsIgnoreCase("NOTUser")){
   %>		
   			<tr>
    	<td><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td><td colspan="2" style="color:#AF0A0A;">Usuario o contrase&ntilde;a incorrecto</td>
  	    </tr>
   <%
		}
   %>
	  <tr>
        <td width="99" class="texttablas">&nbsp;        </td> 
        <td width="195"><input type="submit" name="Submit" value="Ingresar" /></td>
        <td width="436">&nbsp;</td>
	  </tr>
    </table>
	</form>
	</td>
    <td style="background-color:#E6E6E6">&nbsp;</td>
  </tr>
   
  
  
  
</table>

<table width="963px" style="border:0px;padding:0px;border-spacing:0px">
  <tr>
    <td><img src="../../images/spacer.gif" width="5" height="5" alt='SPACER' /></td>
  </tr>
  <tr>
  </tr>
  <tr>
    <td><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td>
  </tr>
</table>

<table width="963px" style="border:0px;padding:0px;border-spacing:0px">
  <tr>
   <td style="background-color:#8DC0E3"><img src="../../images/spacer.gif" width="2" height="2" alt='SPACER' /></td>
  </tr>

</table>
</div>

  <%@ include file="../../plantillaSitio/footermodulesEstadisticas.jsp" %>
<h1></h1>
</body>
</html>
