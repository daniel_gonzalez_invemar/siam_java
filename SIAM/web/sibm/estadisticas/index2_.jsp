<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
 <head>
<title>Estadisticas del Museo de Historia Natural Marina de Colombia</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<style type="text/css">
    a:link {
        text-decoration: none;
    }
    a:visited {
        text-decoration: none;
    }
    a:hover {
        text-decoration: underline;
   }
   
   .classTHeader{
        color: #006666; 
        padding: 5px 20px;
   }
   
  table thead th{
      color: #f7f7f7;
      background-color: #295d94; 
      padding: 5px 30px;
  
  }
  
  table tfoot{
  
        background-color: #295d94; 
  }

</style>
<script src="../jscript/panel.js" type="text/javascript"></script>
<script type="text/javascript">
    
    var isIE=false;
    var loadstatustext="<img src='../images/loading.gif'/> "
   
    /************ FUNCIONES DE UTILERIA*****************/
    function initRequest()
    {
       isIE=false	
       req=null;
       if (window.XMLHttpRequest) 
       {
            req = new XMLHttpRequest();
       }else if (window.ActiveXObject) 
             {
                    req = new ActiveXObject("Microsoft.XMLHTTP");
                    isIE=true;
            }
            
        return req;
    }
    
     function searchTargetOption(sel)
    {
         var ret;
         var options=document.getElementById(sel).childNodes;
         var option=null;
        
         for(var i=0;i < options.length;i++)
         {
              option=options[i];
              if(option.selected)
              {
                  ret=option.value;
                  break; 
              }
         }
         return ret;
    }
                
    function XMLDoc(reqs)
    {
        var xmldoc=null;
        if(!isIE)
        {
          xmldoc=reqs.responseXML;
        }else{
               //IE no toma el responseXML como un documento DOM
          xmldoc = new ActiveXObject("Microsoft.XMLDOM");
          xmldoc.async=false;
          xmldoc.loadXML(reqs.responseText);					 
          xmldoc.documentElement
        }
        return xmldoc;
                            
    }
    
    function clearTableTBODY(tb){
        if (tb)
        {
           for (var i = tb.childNodes.length -1; i >= 0 ; i--)
           {
                tb.removeChild(tb.childNodes[i]);
           }
        }
    }
    
    function clearSelect(p) 
    {
         if (p)
         {
            for (var i = p.childNodes.length -1; i >= 0 ; i--)
            {
                p.removeChild(p.childNodes[i]);
            }
         }
    }
    
    function appendOpcionesSelect(valor,name,pp)
    {
       var optionElement;
       optionElement=document.createElement("option");
       optionElement.setAttribute("value",valor);
       optionElement.appendChild(document.createTextNode(name));
       pp.appendChild(optionElement);
    }
    
    
    
    function sumarLotesTable(tbodyId){
        var tbod=document.getElementById(tbodyId);
        suma=0;
        
        for (var i=0; i<tbod.rows.length; i++)
        {
            suma=suma+parseInt(tbod.rows[i].lastChild.childNodes[0].nodeValue);
        
        }
        
        return suma;
    
    }
    
    /*********************************************************************/
   
   /**************** SECCION - FEHCA DE RECIBIDO*********************************/
    function doFillFechaRecibido(container)
    {
            
            var url="../servletAjaxProcess?action=doFillFechaRecibido&ms="+new Date().getTime();
            var reqs=initRequest();
            document.getElementById("loadingFR").innerHTML=loadstatustext
            reqs.onreadystatechange=function processRequest()
                                    {
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages("fechaRecibido","secciones", reqs);
                                              doEstadisticaFechaRecibido(document.getElementById("fechaRecibido").value);
                                               document.getElementById("loadingFR").removeChild(document.getElementById("loadingFR").childNodes[0]); 
                                              document.getElementById(container).style.display="block";
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }else  {
                                        document.getElementById("loadingFR").innerHTML=loadstatustext
                                     
                                     }
            }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    
    }
    
    
    
    function parseSelectMessages(id,tagName,reqs)
    {
            var pp=document.getElementById(id);
            clearSelect(pp) 
            var responses=XMLDoc(reqs);
            if(responses==null){
                    
            }else{
    
                    var secciones = responses.getElementsByTagName(tagName)[0];
                    for (i = 0; i < secciones.childNodes.length; i++) {
                        var seccion = secciones.childNodes[i];
                        var valor = seccion.getElementsByTagName("value")[0];
                        var name = seccion.getElementsByTagName("name")[0];
         
                        appendOpcionesSelect(valor.childNodes[0].nodeValue,name.childNodes[0].nodeValue,pp);
                    }
            }
     }
    
  
  
    
    
    function doEstadisticaFechaRecibido(optValue){
        var url="../servletAjaxProcess?action=doEstadisticaFechaRecibido&valor="+optValue+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingCont1").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseTableMessages("tbodyEstad","Cont1ShowResult",reqs,"cantidad-muestras","ano","lotes","Total Lotes por secci�n: ");
                                          document.getElementById("loadingCont1").removeChild(document.getElementById("loadingCont1").childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         document.getElementById("loadingCont1").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);
    }
    
    function parseTableMessages(id,container,reqs,tagName,element1,element2,msg){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var muestras = responses.getElementsByTagName(tagName)[0];
           for (i = 0; i < muestras.childNodes.length; i++) {
                var cantidad = muestras.childNodes[i];
                var var1 = cantidad.getElementsByTagName(element1)[0];
                var var2 = cantidad.getElementsByTagName(element2)[0];
                appendDataTable(var1.childNodes[0].nodeValue,var2.childNodes[0].nodeValue,tb,"center","center",i);
           }
        }
        /*Se agrega el total obtenido con la variable count */
        var divCont=document.getElementById(container);
        /*si el elemento existe en el arbol se remueve */
        var ID=id+container;
        if(document.getElementById(ID)){
            divCont.removeChild(document.getElementById(ID));
        }
       /*creamos el elemeto donde se mostrara el toral obtenido y se agrega al contenedor*/
        var pelement=document.createElement("h4");
        pelement.id=ID;
        pelement.appendChild(document.createTextNode(msg));
        var spanE=document.createElement("span");
        spanE.appendChild(document.createTextNode(sumarLotesTable(id)));
        pelement.appendChild(spanE);
        
        /*se agrega el elemento al arbol*/    
        divCont.appendChild(pelement);
        
      
   }
 
   
   function appendDataTable(valor1,valor2,tb,align1,align2,i){
      var valor1Cell;
      var valor2Cell;
      /*si el browser es Internet Explorer*/
      if(isIE){
        row=tb.insertRow(tb.rows.length);
        valor1Cell=row.insertCell(0);    
        valor2Cell=row.insertCell(1);     
      }else{
         row=document.createElement("tr");
         valor1Cell=document.createElement("td");
         valor2Cell=document.createElement("td");
      }
     
      row.appendChild(valor1Cell);
      row.appendChild(valor2Cell);
      tb.appendChild(row);
      row.setAttribute("border",0);
      
      if(i%2!=0)
        row.style.backgroundColor="#eff7ff";
      else
          row.style.backgroundColor="#ffffff";
      
      valor1Cell.setAttribute("align",align1);
      valor1Cell.appendChild(document.createTextNode(valor1));
      valor2Cell.setAttribute("align",align2);
      valor2Cell.appendChild(document.createTextNode(valor2));
      
    }
    
    function doFillMuestraFI(container){
        var url="../servletAjaxProcess?action=doFillMuestraFI&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingFI").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseSelectMessages("muestraFI","secciones", reqs);
                                          doFillFechaIdent(document.getElementById("muestraFI").value);
                                          document.getElementById("loadingFI").removeChild(document.getElementById("loadingFI").childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                   }else if(reqs.readyState==1) {
                                        document.getElementById("loadingFI").innerHTML=loadstatustext
                                   }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
    
    }
    
    function doFillFechaIdent(optValue){
        var url="../servletAjaxProcess?action=doFillFechaIdent&valor="+optValue+"&ms="+new Date().getTime();
        var reqs=initRequest();
         document.getElementById("loadingCont2").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseSelectMessages("fechaident","fechasIndent", reqs);
                                          var seccion=document.getElementById("muestraFI").value;
                                          var ano=document.getElementById("fechaident").value;
                                          doEstadFechaIdent(seccion,ano);
                                           document.getElementById("loadingCont2").removeChild(document.getElementById("loadingCont2").childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         document.getElementById("loadingCont2").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);
    }
    
    function doEstadFechaIdent(optValueSeccion, optValueAno){
    
        var url="../servletAjaxProcess?action=doEstadFechaIdent&valorSeccion="+optValueSeccion+"&valorAno="+optValueAno+"&ms="+new Date().getTime();
        var reqs=initRequest();
         document.getElementById("loadingCont2").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadSeccFechaMessages("fechaident","valor-fecha","Cont2ShowResult", reqs);
                                           document.getElementById("loadingCont2").removeChild(document.getElementById("loadingCont2").childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         document.getElementById("loadingCont2").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
    }
    
    function parseEstadSeccFechaMessages(id, tagName,contenedor,reqs){
    
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var valor = responses.getElementsByTagName(tagName)[0];
           
           var divCont=document.getElementById(contenedor);
           clearElement(divCont);
            /*si el elemento existe en el arbol se remueve */
            var newId=id+tagName;
             /*if(document.getElementById(newId)){
                  divCont.removeChild(document.getElementById(newId));
             }*/
             /*creamos el elemeto donde se mostrara el toral obtenido y se agrega al contenedor*/
            var pelement=document.createElement("h4");
            pelement.id=newId;
            pelement.appendChild(document.createTextNode("Total Numero de Lotes: "));
            var spanE=document.createElement("span");
            spanE.appendChild(document.createTextNode(valor.firstChild.childNodes[0].nodeValue));
            pelement.appendChild(spanE);
            
            /*se agrega el elemento al arbol*/    
            divCont.appendChild(pelement);
               
        }
        
    }
    
    
     function doFillMuestraFC(container){
        var url="../servletAjaxProcess?action=doFillMuestraFC&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingFC").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseSelectMessages("muestraFC","secciones", reqs);
                                          doFillFechaCaptura(document.getElementById("muestraFC").value);
                                          document.getElementById("loadingFC").removeChild(document.getElementById("loadingFC").childNodes[0]); 
                                           document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else if(reqs.readyState==1){
                                        document.getElementById("loadingFC").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
    
    }
    
     function doFillFechaCaptura(optValue){
        var url="../servletAjaxProcess?action=doFillFechaCaptura&valor="+optValue+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingCont3").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseSelectMessages("fechacaptura","fechasCapturas", reqs);
                                          var seccion=document.getElementById("muestraFC").value;
                                          var ano=document.getElementById("fechacaptura").value;
                                          doEstadFechaCaptura(seccion,ano);
                                           document.getElementById("loadingCont3").removeChild(document.getElementById("loadingCont3").childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                        document.getElementById("loadingCont3").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);
    }
    
     function doEstadFechaCaptura(optValueSeccion, optValueAno){
    
        var url="../servletAjaxProcess?action=doEstadFechaCaptura&valorSeccion="+optValueSeccion+"&valorAno="+optValueAno+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingCont3").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadSeccFechaMessages("fechacaptura","valor-fecha","Cont3ShowResult", reqs);
                                          document.getElementById("loadingCont3").removeChild(document.getElementById("loadingCont3").childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                        document.getElementById("loadingCont3").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
    }
    
    
    function doSeccionImages(container){
        var url="../servletAjaxProcess?action=doSeccionImages&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingEIMG").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseSeccImagesMessages("tbodyEstad4",reqs);
                                          document.getElementById("loadingEIMG").removeChild(document.getElementById("loadingEIMG").childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                        document.getElementById("loadingEIMG").innerHTML=loadstatustext
                                    
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
    
    }
    function parseSeccImagesMessages(id,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var lotes = responses.getElementsByTagName("lotes")[0];
           for (i = 0; i < lotes.childNodes.length; i++) {
                var lote = lotes.childNodes[i];
                var seccion = lote.getElementsByTagName("seccion")[0];
                var conImages = lote.getElementsByTagName("conimages")[0];
                var sinImages = lote.getElementsByTagName("sinimages")[0];
                appendDataTables(seccion.childNodes[0].nodeValue,conImages.childNodes[0].nodeValue,sinImages.childNodes[0].nodeValue,tb,i);
           }
        }
    }
    
    function appendDataTables(valor1,valor2,valor3,tb,i){
          var valor1Cell;
          var valor2Cell;
          var valor3Cell;
          /*si el browser es Internet Explorer*/
          if(isIE){
            row=tb.insertRow(tb.rows.length);
            valor1Cell=row.insertCell(0);    
            valor2Cell=row.insertCell(1);     
            valor3Cell=row.insertCell(1);     
          }else{
             row=document.createElement("tr");
             valor1Cell=document.createElement("td");
             valor2Cell=document.createElement("td");
             valor3Cell=document.createElement("td");
          }
         
          row.appendChild(valor1Cell);
          row.appendChild(valor2Cell);
          row.appendChild(valor3Cell);
          
          if(i%2!=0)
            row.style.backgroundColor="#eff7ff";
          else
            row.style.backgroundColor="#ffffff";
          
          tb.appendChild(row);
          row.setAttribute("border",0);
          
          /*valor1Cell.setAttribute("align","left");*/
          valor1Cell.style.paddingLeft="24px";
          valor1Cell.style.whiteSpace="nowrap";
          valor1Cell.appendChild(document.createTextNode(valor1));
          
          valor2Cell.setAttribute("align","right");
          valor2Cell.style.paddingRight="50px";
          valor2Cell.style.whiteSpace="nowrap";
          valor2Cell.appendChild(document.createTextNode(valor2)); 
          
          valor3Cell.setAttribute("align","right");
          valor3Cell.style.paddingRight="50px";
          valor3Cell.style.whiteSpace="nowrap";
          valor3Cell.appendChild(document.createTextNode(valor3)); 
        
    }
    
    function doFillMuestraPROY(container){
        var url="../servletAjaxProcess?action=doFillMuestraPROY&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingPROY").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseSelectMessages("seccionesPROY","secciones", reqs);
                                          doEstadSeccProyectos(document.getElementById("seccionesPROY").value);
                                          document.getElementById("loadingPROY").removeChild(document.getElementById("loadingPROY").childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                        document.getElementById("loadingPROY").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
    
    }
    
    
    function doEstadSeccProyectos(optValue){
       
        var url="../servletAjaxProcess?action=doEstadSeccProyectos&valor="+optValue+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById("loadingCont7").innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadProyMessages("tbodyEstad7","Cont7ShowResult",reqs);
                                          document.getElementById("loadingCont7").removeChild(document.getElementById("loadingCont7").childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                    
                                        document.getElementById("loadingCont7").innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);
    }
    
     function parseEstadProyMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var proyectos = responses.getElementsByTagName("proyectos")[0];
           for (var i = 0; i < proyectos.childNodes.length; i++) {
                var proyecto = proyectos.childNodes[i];
                var titulo = proyecto.getElementsByTagName("titulo")[0];
                var lotes = proyecto.getElementsByTagName("lotes")[0];
                appendDataTable(titulo.childNodes[0].nodeValue,lotes.childNodes[0].nodeValue,tb,"left","center",i);
           }
        }
       
          
   }
   
   function clearElement(element){
        if(element){
           for (var i = element.childNodes.length -1; i >= 0 ; i--)
            {
               element.removeChild(element.childNodes[i]);
            }
         }
   }
   function doViewAll(optValue, container,action,contLoading){
    
    
        var divCont=document.getElementById(container);
        clearElement(divCont);    
        
        var table=document.createElement("table");
        table.style.border="1px solid #d7edfb";
        
        
        var thead=document.createElement("thead");
        var th1=document.createElement("th");
        var th2=document.createElement("th");
        
        var tr=document.createElement("tr");
        th1.appendChild(document.createTextNode("A�o"));
        th2.appendChild(document.createTextNode("Numero Lotes"));
        tr.appendChild(th1);
        tr.appendChild(th2);
        thead.appendChild(tr);
        
        var tbody=document.createElement("tbody");
        var ID=container+optValue;
        tbody.id=ID;
        
        table.appendChild(thead);
        table.appendChild(tbody);
        divCont.appendChild(table);
        
        var url="../servletAjaxProcess?action="+action+"&valor="+optValue+"&ms="+new Date().getTime();
        var reqs=initRequest();
        document.getElementById(contLoading).innerHTML=loadstatustext
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseViewAllMessages(ID,container,reqs);
                                           document.getElementById(contLoading).removeChild(document.getElementById(contLoading).childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                        document.getElementById(contLoading).innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);
  
   }
   
   function parseViewAllMessages(id,container,reqs){
        parseTableMessages(id,container,reqs,"cantidad-muestras","ano","lotes","Total Lotes por secci�n: ");
    
   }
   
   
   function doFillMuestraPRES(container)
    {
            
            var url="../servletAjaxProcess?action=doFillMuestraPRES";
            var reqs=initRequest();
            var spanLoading=document.getElementById("loadingPRES");
            spanLoading.innerHTML=loadstatustext;
            reqs.onreadystatechange=function processRequest()
                                    {
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages("muestraPRES","secciones", reqs);
                                              doEstadPreservativos(document.getElementById("muestraPRES").value);
                                               spanLoading.removeChild(spanLoading.childNodes[0]); 
                                              document.getElementById(container).style.display="block";
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }else  {
                                        spanLoading.innerHTML=loadstatustext;
                                     
                                     }
            }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    
    }
    
    function doEstadPreservativos(optValue){
        var url="../servletAjaxProcess?action=doEstadPreservativos&valor="+optValue;
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont8");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadPresMessages("tbodyEstad8","Cont8ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
  }
  
  function parseEstadPresMessages(id,container,reqs){
        parseTableMessages(id,container,reqs,"cantidad-muestras","preservativo","lotes","Total Lotes por secci�n: ");
        
   }
 
 function doFillMuestraObjRelac(container)
    {
            
            var url="../servletAjaxProcess?action=doFillMuestraObjRelac";
            var reqs=initRequest();
            var spanLoading=document.getElementById("loadingOBJ");
            spanLoading.innerHTML=loadstatustext;
            reqs.onreadystatechange=function processRequest()
                                    {
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages("muestraOBJR","secciones", reqs);
                                              doEstadObjRelacionado(document.getElementById("muestraOBJR").value);
                                               spanLoading.removeChild(spanLoading.childNodes[0]); 
                                              document.getElementById(container).style.display="block";
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }else  {
                                        spanLoading.innerHTML=loadstatustext;
                                     
                                     }
            }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    
    }
    
    function doEstadObjRelacionado(optValue){
        var url="../servletAjaxProcess?action=doEstadObjRelacionado&valor="+optValue;
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont9");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadObjRelacMessages("tbodyEstad9","Cont9ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
  }
  
  function parseEstadObjRelacMessages(id,container,reqs){
        parseTableMessages(id,container,reqs,"cantidad-muestras","objeto","lotes","Total Nro de Objetos Relacionados: " );
        
   }
   
   function doFillSeccionINV(container){
   
        var url="../servletAjaxProcess?action=doFillSeccionINV";
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEINV");
        spanLoading.innerHTML=loadstatustext;
        reqs.onreadystatechange=function processRequest()
                                {
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages("MuestraINV","secciones", reqs);
                                              doEstadInvestigadores(document.getElementById("MuestraINV").value);
                                               spanLoading.removeChild(spanLoading.childNodes[0]); 
                                              document.getElementById(container).style.display="block";
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }else  {
                                        spanLoading.innerHTML=loadstatustext;
                                     
                                     }
                               }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    }

   function doEstadInvestigadores(optValue){
   
        var url="../servletAjaxProcess?action=doEstadInvestigadores&valor="+optValue;
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont5");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadInvestMessages("tbodyEstad5","Cont5ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
   }
   
   function parseEstadInvestMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var investigadores = responses.getElementsByTagName("investigadores")[0];
           
           for (var i = 0; i < investigadores.childNodes.length; i++) {
                var investigador = investigadores.childNodes[i];
                var nombre = investigador.getElementsByTagName("nombre")[0].childNodes[0].nodeValue;
                var tarea = investigador.getElementsByTagName("tarea")[0].childNodes[0].nodeValue;
                var ano = investigador.getElementsByTagName("ano")[0].childNodes[0].nodeValue;
                var lotes = investigador.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                appendData(nombre,tarea,ano,lotes,tb,i);
           }
        }
        
       
    
   }
   
   function appendData(valor1,valor2,valor3,valor4,tb,i, aling){
   
          var valor1Cell;
          var valor2Cell;
          var valor3Cell;
          var valor4Cell;
          /*si el browser es Internet Explorer*/
          if(isIE){
            row=tb.insertRow(tb.rows.length);
            valor1Cell=row.insertCell(0);    
            valor2Cell=row.insertCell(1);     
            valor3Cell=row.insertCell(2);     
            valor4Cell=row.insertCell(3);     
          }else{
             row=document.createElement("tr");
             valor1Cell=document.createElement("td");
             valor2Cell=document.createElement("td");
             valor3Cell=document.createElement("td");
             valor4Cell=document.createElement("td");
          }
         
          row.appendChild(valor1Cell);
          row.appendChild(valor2Cell);
          row.appendChild(valor3Cell);
          row.appendChild(valor4Cell);
          if(i%2!=0)
             row.style.backgroundColor="#eff7ff";
           else
             row.style.backgroundColor="#ffffff";
          tb.appendChild(row);
          row.setAttribute("border",0);
          
          valor1Cell.setAttribute("align",aling);
          valor1Cell.style.padding="0 10px";
          valor1Cell.style.whiteSpace="nowrap";
          valor1Cell.appendChild(document.createTextNode(valor1));
          
          valor2Cell.setAttribute("align","left");
          valor2Cell.style.padding="0 10px";
          valor2Cell.style.whiteSpace="nowrap";
          valor2Cell.appendChild(document.createTextNode(valor2)); 
          
          valor3Cell.setAttribute("align","left");
          valor3Cell.style.padding="0 10px";
          valor3Cell.style.whiteSpace="nowrap";
          valor3Cell.appendChild(document.createTextNode(valor3)); 
   
          valor4Cell.setAttribute("align","left");
          valor4Cell.style.padding="0 10px";
          valor4Cell.style.whiteSpace="nowrap";
          valor4Cell.appendChild(document.createTextNode(valor4)); 
   
   }
   
   function doFillInvestigadores(container){
      
        var url="../servletAjaxProcess?action=doFillInvestigadores&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingInvest");
        spanLoading.innerHTML=loadstatustext;
        reqs.onreadystatechange=function processRequest()
                                {
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages("invest","investigadores", reqs);
                                              doEstadInvest(document.getElementById("invest").value);
                                               spanLoading.removeChild(spanLoading.childNodes[0]); 
                                              document.getElementById(container).style.display="block";
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }else  {
                                        spanLoading.innerHTML=loadstatustext;
                                     
                                     }
                               }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    }

   function doEstadInvest(optValue){
        var url="../servletAjaxProcess?action=doEstadInvest&valor="+escape(optValue)+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont6");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstInvMessages("tbodyEstad6","Cont6ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   }
 
   function parseEstInvMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var investigadores = responses.getElementsByTagName("investigador")[0];
           
           for (var i = 0; i < investigadores.childNodes.length; i++) {
                var investigador = investigadores.childNodes[i];
                var ano = investigador.getElementsByTagName("ano")[0].childNodes[0].nodeValue;
                var seccion = investigador.getElementsByTagName("seccion")[0].childNodes[0].nodeValue;
                var tarea = investigador.getElementsByTagName("tarea")[0].childNodes[0].nodeValue;
                var lotes = investigador.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                appendData(ano,seccion,tarea,lotes,tb,i,"left");
           }
        }
        
       
           
   }
   
   
   /****************************************************************************************/
   function doEstadByPhylum(container){
          doMuestrasByPhylum(container);
         // doFichaEspecByPhylum(container);
         // doEspecieByPhylum(container);  
          
          
   }
   
   function doMuestrasByPhylum(container){
        var url="../servletAjaxProcess?action=doMuestrasByPhylum&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEstdPhylum");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadMByPhylumMessages("tbodyEstad10",reqs,"muestras");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
   }
   
   function parseEstadMByPhylumMessages(id,reqs,tagName){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var muestras = responses.getElementsByTagName(tagName)[0];
           
           for (i = 0; i < muestras.childNodes.length; i++) {
                var muestra = muestras.childNodes[i];
                
                var phylum = muestra.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                
                var cantidad = muestra.getElementsByTagName("cantidad")[0].childNodes[0].nodeValue;
                //alert(cantidad.childNodes[0].nodeValue)
                var porcentage = muestra.getElementsByTagName("porcentages")[0].childNodes[0].nodeValue;
               // alert(procentage.childNodes[0].nodeValue)
                appendDataTables(phylum,cantidad,porcentage,tb,i);
           }
        }
   }
   
   function doFichaEspecByPhylum(container){
       var url="../servletAjaxProcess?action=doFichaEspecByPhylum&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEstdPhylum1");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadMByPhylumMessages("tbodyEstad101",reqs,"fichas");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
   }
   
   
   
   function doEspecieByPhylum(container){
        var url="../servletAjaxProcess?action=doEspecieByPhylum&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEstdPhylum2");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                         parseEstadMByPhylumMessages("tbodyEstad102",reqs,"especies");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null); 
   }
   
   /*************************************************************************************************/
   /***************************************************************************************************/
   
   function datos(){
    this.newfuncion;
    this.id;
    this.spanLoadId;
   
   }
   
   function doFillPhylum(container){
   
        
        var data=new datos();
        
        if(container=="contenedor11"){
            data.id="nivelPhylum";
            data.newfuncion=doEstadNivelPhylum;
            data.spanLoadId="loadingPhylum";
            
            
        }else if(container=="contenedor12"){
            data.id='nivelTax';
            data.newfuncion=doEstadNivelTax;
            data.spanLoadId="loadingPhylums";
            
        }else if(container=="contenedor13"){
            data.id='especienivelTax';
            data.newfuncion=doEstadEspeciesNivelTax;
            data.spanLoadId="loadingPhyl";
            
        }else if(container=="contenedor14"){
            data.id='elemphylum';
            data.newfuncion=doEstadElementosNivelTax;
            data.spanLoadId="loadingPhylu";
            
        }else if(container=="contenedor15"){
            data.id='phylum';
            data.newfuncion=doEstadElementDiccNivelTax;
            data.spanLoadId="loadingP";
        }
           
            
        
        
        doFillPhyl(container,data);
        
   
   }
   function doFillPhyl(container,data){
        var url="../servletAjaxProcess?action=doFillPhylum&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById(data.spanLoadId);
        spanLoading.innerHTML=loadstatustext;
       // alert(datos.dat[0].funcion)
        //var newData=datos.dat[0];
        reqs.onreadystatechange=function processRequest()
                                {
                                       if(reqs.readyState==4)
                                       {
                                          if(reqs.status==200)
                                          {
                                              parseSelectMessages(data.id,"phylums", reqs);
                                              //doEstadNivelPhylum(document.getElementById("nivelPhylum").value);
                                              data.newfuncion();
                                               spanLoading.removeChild(spanLoading.childNodes[0]); 
                                              document.getElementById(container).style.display="block";
                                           }else if(reqs.status==204)
                                           {
                                                  alert("Status 204");
                                           }
                                     }else  {
                                        spanLoading.innerHTML=loadstatustext;
                                     
                                     }
                               }
    
            reqs.open("GET",url,true);
            reqs.send(null);
    
   }
   
   function doEstadNivelPhylum(){
        var optValue=document.getElementById("nivelPhylum").value;
        var url="../servletAjaxProcess?action=doEstadNivelPhylum&valor="+escape(optValue)+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont11");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          
                                          parseEstNivelPhylumMessages("tbodyEstad11","Cont11ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   
   }
   
   
   function  parseEstNivelPhylumMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var taxonomias = responses.getElementsByTagName("taxonomias")[0];
           
           for (var i = 0; i < taxonomias.childNodes.length; i++) {
                var taxon = taxonomias.childNodes[i];
                var codigo = taxon.getElementsByTagName("codigo")[0].childNodes[0].nodeValue;
                var nombre = taxon.getElementsByTagName("nombre")[0].childNodes[0].nodeValue;
                var phylum = taxon.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                var lotes = taxon.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                appendData(codigo,phylum,nombre,lotes,tb,i,'center');
           }
        }
        
      
           
   }
   
   
   /**************************************************************************************************/
   /*********************************************************************************************/
   
   function doEstadNivelTax(){
        var optValue=document.getElementById("nivelTax").value;
        var url="../servletAjaxProcess?action=doEstadNivelTax&valor="+escape(optValue);
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont12");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstNivelTaxMessages("tbodyEstad12","Cont12ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   
   }
   
   
   function  parseEstNivelTaxMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var taxonomias = responses.getElementsByTagName("taxonomias")[0];
           
           for (var i = 0; i < taxonomias.childNodes.length; i++) {
                var taxon = taxonomias.childNodes[i];
                var codigo = taxon.getElementsByTagName("codigo")[0].childNodes[0].nodeValue;
                var phylum = taxon.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                var lotes = taxon.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                var elementos = taxon.getElementsByTagName("elementos")[0].childNodes[0].nodeValue;
                
                appendData(codigo,phylum,elementos,lotes,tb,i,'center');
           }
        }
        
        
   }
   
   /***************************************************************************************************/
  /***********************************************************************************************/
  
  function doEstadEspeciesNivelTax(){
        var optValue=document.getElementById("especienivelTax").value;
        var url="../servletAjaxProcess?action=doEstadEspeciesNivelTax&valor="+escape(optValue)+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont13");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadEspeciesNivelTaxMessages("tbodyEstad13","Cont13ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   
    }
    
    function parseEstadEspeciesNivelTaxMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var taxonomias = responses.getElementsByTagName("taxonomias")[0];
           
           for (var i = 0; i < taxonomias.childNodes.length; i++) {
                var taxon = taxonomias.childNodes[i];
                var codigo = taxon.getElementsByTagName("codigo")[0].childNodes[0].nodeValue;
                var phylum = taxon.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                var nombre = taxon.getElementsByTagName("nombre")[0].childNodes[0].nodeValue;
                var especimenes = taxon.getElementsByTagName("especimenes")[0].childNodes[0].nodeValue;
                
                appendData(codigo,phylum,nombre,especimenes,tb,i,'center');
           }
        }
        
        
    }
  /*****************************************************************************************/
  function doEstadElementosNivelTax(){
       var optValue=document.getElementById("elemphylum").value;
        var url="../servletAjaxProcess?action=doEstadElementosNivelTax&valor="+escape(optValue)+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont14");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadElementNivelTaxMessages("tbodyEstad14","Cont14ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   
    }
    
    function parseEstadElementNivelTaxMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var taxonomias = responses.getElementsByTagName("taxonomias")[0];
           
           for (var i = 0; i < taxonomias.childNodes.length; i++) {
                var taxon = taxonomias.childNodes[i];
                var codigo = taxon.getElementsByTagName("codigo")[0].childNodes[0].nodeValue;
                var phylum = taxon.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                var nombre = taxon.getElementsByTagName("nivel")[0].childNodes[0].nodeValue;
                var especimenes = taxon.getElementsByTagName("elementos")[0].childNodes[0].nodeValue;
                
                appendData(codigo,phylum,nombre,especimenes,tb,i,'center');
           }
        }
        
      
    }
    
    /*********************************************************************************************/
    /**********************************************************************************************************/
    function doEstadElementDiccNivelTax(){ 
        var optValue=document.getElementById("phylum").value;
        var url="../servletAjaxProcess?action=doEstadElementDiccNivelTax&valor="+escape(optValue)+"&ms="+new Date().getTime();
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingCont15");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadElemDiccNivelTaxMessages("tbodyEstad15","Cont15ShowResult",reqs);
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext;
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);  
   
    }
    
    function parseEstadElemDiccNivelTaxMessages(id,container,reqs){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var taxonomias = responses.getElementsByTagName("taxonomias")[0];
           
           for (var i = 0; i < taxonomias.childNodes.length; i++) {
                var taxon = taxonomias.childNodes[i];
                var codigo = taxon.getElementsByTagName("codigo")[0].childNodes[0].nodeValue;
                var phylum = taxon.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                var nivel = taxon.getElementsByTagName("nivel")[0].childNodes[0].nodeValue;
                var nombre = taxon.getElementsByTagName("nombre")[0].childNodes[0].nodeValue;
                
                appendData(codigo,phylum,nivel,nombre,tb,i,'center');
           }
        }
        
        }
        
      /*************************************************************************************************/
      function doEstadLotesByAreaGeograf(container){
        var url="../servletAjaxProcess?action=doEstadLotesByAreaGeograf";
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEstdareaGeograf");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadLotesAreaGeoMessages("tbodyEstad18",reqs,"areas");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);    
     }
     
      function parseEstadLotesAreaGeoMessages(id,reqs,tagName){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var areas = responses.getElementsByTagName(tagName)[0];
           
           for (i = 0; i < areas.childNodes.length; i++) {
                var area = areas.childNodes[i];
                
                var latitud = area.getElementsByTagName("latitud")[0].childNodes[0].nodeValue;
                
                var longitud = area.getElementsByTagName("longitud")[0].childNodes[0].nodeValue;
                //alert(cantidad.childNodes[0].nodeValue)
                var lotes = area.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
               // alert(procentage.childNodes[0].nodeValue)
                appendDataTables(latitud,longitud,lotes,tb,i);
           }
        }
   }
   
   function doEstadMantenimiento(container){
   
            doEstadMantenim(container);
            doEstadMantenimtoConFaltantes(container);
            
   }
   function doEstadMantenim(container){
        var url="../servletAjaxProcess?action=doEstadMantenimiento";
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingManten");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadMantenimiento("tbodyEstad19",reqs,"datos");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
   
   }
   
   function parseEstadMantenimiento(id,reqs,tagname){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var datos = responses.getElementsByTagName("datos")[0];
           
           for (var i = 0; i < datos.childNodes.length; i++) {
                var dato = datos.childNodes[i];
                var seccion = dato.getElementsByTagName("seccion")[0].childNodes[0].nodeValue;
                var ano = dato.getElementsByTagName("ano")[0].childNodes[0].nodeValue;
                var tarea = dato.getElementsByTagName("tarea")[0].childNodes[0].nodeValue;
                var lotes = dato.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                
                appendData(seccion,ano,tarea,lotes,tb,i,'center');
           }
        }
        
        }
   
    function doEstadMantenimtoConFaltantes(container){
        var url="../servletAjaxProcess?action=doEstadMantenimtoConFaltantes";
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingManten");
        //spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadMantenimConFaltantes("tbodyEstad191",reqs,"datos");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
   
   }
   
   function parseEstadMantenimConFaltantes(id,reqs,tagname){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var datos = responses.getElementsByTagName("datos")[0];
           
           for (var i = 0; i < datos.childNodes.length; i++) {
                var dato = datos.childNodes[i];
                var seccion = dato.getElementsByTagName("seccion")[0].childNodes[0].nodeValue;
                var tarea = dato.getElementsByTagName("tarea")[0].childNodes[0].nodeValue;
                var lotes = dato.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                var faltantes = dato.getElementsByTagName("faltantes")[0].childNodes[0].nodeValue;
                appendData(seccion,tarea,lotes,faltantes,tb,i,'center');
           }
        }
        
    }
   
   
   
    function doEstadNomComuneSinonimias(container){
        var url="../servletAjaxProcess?action=doEstadNomComuneSinonimias";
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingSinNom");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadVulgaresSinonimias("tbodyEstad20",reqs,"datos");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
   
   }
   
   function  parseEstadVulgaresSinonimias(id,reqs,tagname){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var datos = responses.getElementsByTagName("datos")[0];
           
           for (var i = 0; i < datos.childNodes.length; i++) {
                var dato = datos.childNodes[i];
                var phylum = dato.getElementsByTagName("phylum")[0].childNodes[0].nodeValue;
                var vulgares = dato.getElementsByTagName("vulgares")[0].childNodes[0].nodeValue;
                var sinonimias = dato.getElementsByTagName("sinonimias")[0].childNodes[0].nodeValue;
                
                appendDataTables(phylum,vulgares,sinonimias,tb,i,'center');
           }
        }
        
    }
    
      function doLotesOtrasEntidades(container){
        var url="../servletAjaxProcess?action=doLotesOtrasEntidades";
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEstdOENT");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadLotesOEntidad("tbodyEstad21",reqs,"datos");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
   
   }
   
   function parseEstadLotesOEntidad(id,reqs,tagname){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var datos = responses.getElementsByTagName("datos")[0];
           
           for (var i = 0; i < datos.childNodes.length; i++) {
                var dato = datos.childNodes[i];
                var seccion = dato.getElementsByTagName("seccion")[0].childNodes[0].nodeValue;
                var institucion = dato.getElementsByTagName("institucion")[0].childNodes[0].nodeValue;
                var modalidad = dato.getElementsByTagName("modalidad")[0].childNodes[0].nodeValue;
                var lotes = dato.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                appendData(seccion,institucion,modalidad,lotes,tb,i,'center');
           }
        }
        
    }
    
    
      function doLotesByTipos(container){
        var url="../servletAjaxProcess?action=doLotesByTipos";
        var reqs=initRequest();
        var spanLoading=document.getElementById("loadingEstdLTIPOS");
        spanLoading.innerHTML=loadstatustext;
        
        reqs.onreadystatechange=function processRequest()
                                {
                                   if(reqs.readyState==4)
                                   {
                                      if(reqs.status==200)
                                      {
                                          parseEstadLTipos("tbodyEstad22",reqs,"datos");
                                          spanLoading.removeChild(spanLoading.childNodes[0]); 
                                          document.getElementById(container).style.display="block";
                                      }else if(reqs.status==204)
                                            {
                                               alert("Status 204");
                                            }
                                    }else {
                                         spanLoading.innerHTML=loadstatustext
                                    }
                                 }
         reqs.open("GET",url,true);
         reqs.send(null);   
   
   }
   
   function  parseEstadLTipos(id,reqs,tagname){
        var tb=document.getElementById(id);
        clearTableTBODY(tb);
        var responses=XMLDoc(reqs);
        if(responses==null){
                 
        }else{
           var datos = responses.getElementsByTagName("datos")[0];
           
           for (var i = 0; i < datos.childNodes.length; i++) {
                var dato = datos.childNodes[i];
                var seccion = dato.getElementsByTagName("seccion")[0].childNodes[0].nodeValue;
                var tipo = dato.getElementsByTagName("tipo")[0].childNodes[0].nodeValue;
                var lotes = dato.getElementsByTagName("lotes")[0].childNodes[0].nodeValue;
                
                appendDataTables(seccion,tipo,lotes,tb,i,'center');
           }
        }
        
    }
</script>

</head>

<body style=" background-color: #d7edfb;" >

<%@ include file="header.htm" %>

<div id="main" style="margin-left: 100px; ">
    <h2>Estadisticas</h2>
    <b style="color: #006666;">1.</b>
    <a id="fr" href="javascript:doFillFechaRecibido('contenedor1');" style="color: #006666; font-weight: bold;" >
    Numero de lotes por secci&oacute;n - fecha recibido
    </a>
    <span id="loadingFR" style="padding-left: 20px;"></span>

    <div id="contenedor1" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">
            Secci&oacute;n de muestra: 
            <select id="fechaRecibido" name="fechaRecibido" onchange="doEstadisticaFechaRecibido(this.value);">
                <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont1" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr style="background-color: #295d94;">
                    <th style="background-color: #295d94; color: #f7f7f7; padding: 5px 20px;" >A�o</th>
                    <th style="background-color: #295d94; color: #f7f7f7; padding: 5px 20px;">Numero Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad"></tbody> 
        </table>
        <div id="Cont1ShowResult"></div>
        <a href="javascript:mpanel('contenedor1');" style="color: #003063;" >[Ocultar Panel]</a>
    </div>
    <p></p>



    <b style="color: #006666;">2.</b> 
    <a href="javascript:doFillMuestraFI('contenedor2');" style="color: #006666; font-weight: bold;">
        Numero de lotes por secci&oacute;n - fecha identificacion
    </a>
    <span id="loadingFI" style="padding-left: 20px;"></span>
    <div id="contenedor2" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">
            Secci&oacute;n de muestra: 
            <select id="muestraFI" onchange="doFillFechaIdent(this.value);" style="margin-right: 15px;"> 
                    <option value="" selected="selected" >----------------</option>
            </select>
            Fecha Identificacion:
            <select id="fechaident"  onchange="doEstadFechaIdent(document.getElementById('muestraFI').value,this.value);"> 
                <option value="" selected="selected" >----------------</option>
            </select> 
        
            <span id="loadingCont2" style="padding-left: 30px; margin-left: 25px; border: 1px solid #d7edfb;"></span>
        </p>
 
        <div id="Cont2ShowResult"></div>
  
        <a href="javascript:mpanel('contenedor2');" style="color: #003063;" >
             [Ocultar Panel]
        </a>
        <a href="javascript:doViewAll(document.getElementById('muestraFI').value,'Cont2ShowResult', 'viewAllSFI','loadingCont2');" style="color: #003063;">
          [Ver todos los lotes de esta secci&oacute;n]
        </a>
    </div>
    <p></p>



    <b style="color: #006666;">3.</b> 
    <a href="javascript:doFillMuestraFC('contenedor3');" style="color: #006666; font-weight: bold;">
      Numero de lotes por secci&oacute;n - fecha de captura
    </a>
    <span id="loadingFC" style="padding-left: 20px;"></span>
    <div id="contenedor3" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">
            Secci&oacute;n de muestra: 
            <select id="muestraFC" onchange="doFillFechaCaptura(this.value);" style="margin-right: 15px;"> 
                    <option value="" selected="selected" >----------------</option>
            </select>
            Fecha captura:
            <select id="fechacaptura"  onchange="doEstadFechaCaptura(document.getElementById('muestraFC').value,this.value);"> 
                    <option value="" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont3" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
   
        <div id="Cont3ShowResult"></div>
  
        <a href="javascript:mpanel('contenedor3');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
        <a href="javascript:doViewAll(document.getElementById('muestraFC').value,'Cont3ShowResult', 'viewAllSFC','loadingCont3');" style="color: #003063; padding-left: 20px;">
            [Ver todos los lotes de esta secci&oacute;n]
        </a>
    </div>


    <p></p>
    <b style="color: #006666;">4.</b> 
    <a href="javascript:doSeccionImages('contenedor4');" style="color: #006666; font-weight: bold;">
        Numero de lotes por secci&oacute;n - con imagen y sin imagen
    </a>
    <span id="loadingEIMG" style="padding-left: 20px;"></span>
    <div id="contenedor4" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
 
        <table id="estadistica4" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Secci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Con imagenes</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Sin imagenes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad4"></tbody>
        </table>
        <p></p>
        <a href="javascript:mpanel('contenedor4');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
    
    
   <b style="color: #006666;">5.</b>
    <a  href="javascript:doFillSeccionINV('contenedor5');" style="color: #006666; font-weight: bold;" >
        Numero de lotes por secci&oacute;n, investigador y tarea
    </a>
    <span id="loadingEINV" style="padding-left: 20px;"></span>

    <div id="contenedor5" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 120px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">Secci&oacute;n de muestra: 
            <select id="MuestraINV" onchange="doEstadInvestigadores(this.value);">
                    <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont5" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica5" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px;" >Investigador</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Tarea</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">A�o</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad5"></tbody> 
        </table>
        
        <a href="javascript:mpanel('contenedor5');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
    
   <b style="color: #006666;">6.</b>
    <a id="frs" href="javascript:doFillInvestigadores('contenedor6');" style="color: #006666; font-weight: bold;" >
        Numero de lotes por investigador - a&ntilde;o
    </a>
    <span id="loadingInvest" style="padding-left: 20px;"></span>

    <div id="contenedor6" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 120px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">Investigador: 
            <select id="invest" onchange="doEstadInvest(this.value);">
                    <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont6" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica6" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >a&ntilde;o</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Secci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Tarea</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad6"></tbody> 
        </table>
        <div id="Cont6ShowResult"></div>
        <a href="javascript:mpanel('contenedor6');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
    
    <b style="color: #006666;">7.</b> 
    <a href="javascript:doFillMuestraPROY('contenedor7');" style="color: #006666; font-weight: bold;">
        Numero de lotes por secci&oacute;n aportado por proyectos
    </a>
    <span id="loadingPROY" style="padding-left: 20px;"></span>
    <div id="contenedor7" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">
            Secci&oacute;n de muestra: 
            <select id="seccionesPROY" onchange="doEstadSeccProyectos(this.value);"> 
                    <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont7" style="padding-left: 45px; margin-left: 60px;"></span>  
        </p>
  
        <table id="estadistica7" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Titulo del Proyecto</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Numero Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad7"></tbody>
       </table>
         <a href="javascript:mpanel('contenedor7');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>


    <p></p>
    <b style="color: #006666;">8.</b> 
    <a href="javascript:doFillMuestraPRES('contenedor8');" style="color: #006666; font-weight: bold;" >
        Numero de lotes por secci&oacute;n - preservativo
    </a>
    <span id="loadingPRES" style="padding-left: 20px;"></span>
    <div id="contenedor8" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">
            Secci&oacute;n de muestra: 
            <select id="muestraPRES" onchange="doEstadPreservativos(this.value);"> 
                <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont8" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica8"  style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="padding: 5px 20px;">Preservativo </th>
                    <th style="padding: 5px 20px;">Numero Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad8"></tbody>
        </table>
        <div id="Cont8ShowResult"></div>
        <a href="javascript:mpanel('contenedor8');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
    
    
    <b style="color: #006666;">9.</b>
    <a href="javascript:doFillMuestraObjRelac('contenedor9');" style="color: #006666; font-weight: bold;" >
        Numero de lotes por secci&oacute;n - Objeto relacionado
    </a>
    <span id="loadingOBJ" style="padding-left: 20px;"></span>
    <div id="contenedor9" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">Secci&oacute;n de muestra: 
            <select id="muestraOBJR" onchange="doEstadObjRelacionado(this.value);"> 
                <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont9" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica9" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="padding: 5px 20px;">Objeto </th>
                    <th style="padding: 5px 20px;">Numero Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad9"></tbody>
        </table>
        <div id="Cont9ShowResult"></div>
        <a href="javascript:mpanel('contenedor9');" style="color: #003063;" >[Ocultar Panel]</a>
    </div>
    
    <p></p>
     <b style="color: #006666;">10.</b>
    <a href="javascript:doEstadMantenimiento('contenedor19');" style="color: #006666; font-weight: bold;" >
        Estadisticas mantenimiento de los lotes por secci�n por a�o
    </a>
    <span id="loadingManten" style="padding-left: 20px;"></span>

    <div id="contenedor19" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 120px; border: 1px solid #006666;" >
        
        <table id="estadistica19" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Secci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">A&ntilde;o</th>
                    <th style="margin: 6px 10px; padding:10px 15px; ">Tarea</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                    
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad19"></tbody> 
        </table>
        
         <p> <b>Resumen de datos de mantenimiento </b></p>
         
          <table id="estadistica191" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Secci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; ">Tarea</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Faltantes</th>
                    
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad191"></tbody> 
        </table>
         
        <a href="javascript:mpanel('contenedor19');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
   <b style="color: #006666;">11.</b> 
    <a href="javascript:doEstadNomComuneSinonimias('contenedor20');" style="color: #006666; font-weight: bold;">
        Numero de sinonimias y nombres comunes indexados por phylum
    </a>
    <span id="loadingSinNom" style="padding-left: 20px;"></span>
    <div id="contenedor20" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
        
        <table id="estadistica20" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Phylum</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Nombres Comunes</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Sinonimias</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad20"></tbody>
        </table>
        <p ></p>
        
        <a href="javascript:mpanel('contenedor20');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
     <p></p>
     <b style="color: #006666;">12.</b> 
    <a href="javascript:doLotesOtrasEntidades('contenedor21');" style="color: #006666; font-weight: bold;">
        Lotes del Museo recibidos de otras entidades por seccion
    </a>
    <span id="loadingEstdOENT" style="padding-left: 20px;"></span>
    <div id="contenedor21" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
        
        <table id="estadistica21" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Secci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Instituci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Modalidad</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad21"></tbody>
        </table>
        <p ></p>
        
        <a href="javascript:mpanel('contenedor21');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
     <p></p>
      <b style="color: #006666;">13.</b> 
    <a href="javascript:doLotesByTipos('contenedor22');" style="color: #006666; font-weight: bold;">
        Lotes del MHNMC por tipos 
    </a>
    <span id="loadingEstdLTIPOS" style="padding-left: 20px;"></span>
    <div id="contenedor22" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
        
        <table id="estadistica22" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Secci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Tipo</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad22"></tbody>
        </table>
        <p ></p>
        
        <a href="javascript:mpanel('contenedor22');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
     <p></p>
     
     <b style="color: #006666;">14.</b> 
    <a href="javascript:doEstadByPhylum('contenedor10');" style="color: #006666; font-weight: bold;">
        Lotes en el MHNMC  por phylum
    </a>
    <span id="loadingEstdPhylum" style="padding-left: 20px;"></span>
    <div id="contenedor10" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
        
        <table id="estadistica10" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Phylum</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Muestras</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Porcentaje(%)</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad10"></tbody>
        </table>
        <p ></p>
        
        <a href="javascript:mpanel('contenedor10');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    
     <p></p>
        <b style="color: #006666;">15.</b> 
    <a href="javascript:doEstadLotesByAreaGeograf('contenedor18');" style="color: #006666; font-weight: bold;">
       Distribucion de lotes por area geogr&aacute;fica (55 km)
    </a>
    <span id="loadingEstdareaGeograf" style="padding-left: 20px;"></span>
    <div id="contenedor18" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px;border: 1px solid #006666;" >
        
        <table id="estadistica18" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Latitud</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Longitud</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad18"></tbody>
        </table>
        <p ></p>
        
        <a href="javascript:mpanel('contenedor18');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    
     <p></p>
   <b style="color: #006666;">16.</b>
    <a href="javascript:doFillPhylum('contenedor11');" style="color: #006666; font-weight: bold;" >
        Lotes en museo  identificados hasta el nivel de especie o inferior por nivel por phylum
    </a>
    <span id="loadingPhylum" style="padding-left: 20px;"></span>

    <div id="contenedor11" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 120px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">C&oacute;digo Phylum: 
            <select id="nivelPhylum" onchange="doEstadNivelPhylum(this.value);">
                    <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont11" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica11" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Cd. Nivel</th>
                    
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Nivel</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Nombre</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad11"></tbody> 
        </table>
        <div id="Cont11ShowResult"></div>
        <a href="javascript:mpanel('contenedor11');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
       
   <b style="color: #006666;">17.</b>
    <a href="javascript:doFillPhylum('contenedor12');" style="color: #006666; font-weight: bold;" >
        Lotes en museo por nivel por phylum
    </a>
    <span id="loadingPhylums" style="padding-left: 20px;"></span>

    <div id="contenedor12" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 120px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">C&oacute;digo Phylum: 
            <select id="nivelTax" onchange="doEstadNivelTax(this.value);">
                    <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont12" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica12" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Cd. Nivel</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Nivel</th>
                    <th style="margin: 6px 10px; padding:10px 15px; ">Elementos en este nivel</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                    
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad12"></tbody> 
        </table>
          <div id="Cont12ShowResult"></div>
        <a href="javascript:mpanel('contenedor12');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
       
   <b style="color: #006666;">18.</b>
    <a href="javascript:doFillPhylum('contenedor13');" style="color: #006666; font-weight: bold;" >
        Especimenes en museo con nombre por nivel por phylum
    </a>
    <span id="loadingPhyl" style="padding-left: 20px;"></span>

    <div id="contenedor13" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 120px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">C&oacute;digo Phylum: 
            <select id="especienivelTax" onchange="doEstadEspeciesNivelTax(this.value);">
                    <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont13" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica13" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Nivel Tax.</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Phylum</th>
                    <th style="margin: 6px 10px; padding:10px 15px; ">Nombre</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Num. Especimenes</th>
                    
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad13"></tbody> 
        </table>
          <div id="Cont13ShowResult"></div>
        <a href="javascript:mpanel('contenedor13');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
 
    <b style="color: #006666;">19.</b>
    <a href="javascript:doFillPhylum('contenedor14');" style="color: #006666; font-weight: bold;" >
        Grupos taxom&oacute;nicos con nombres vigentes incluidos en el diccionario por phylum
    </a>
    <span id="loadingPhylu" style="padding-left: 20px;"></span>

    <div id="contenedor14" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 120px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">C&oacute;digo Phylum: 
            <select id="elemphylum" onchange="doEstadElementosNivelTax(this.value);">
                    <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont14" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica14" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Nivel Tax.</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Phylum</th>
                    <th style="margin: 6px 10px; padding:10px 15px; ">Nivel</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Num. Elementos</th>
                    
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad14"></tbody> 
        </table>
          <div id="Cont14ShowResult"></div>
        <a href="javascript:mpanel('contenedor14');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
 
   <p></p>
 
    <b style="color: #006666;">20.</b>
    <a href="javascript:doFillPhylum('contenedor15');" style="color: #006666; font-weight: bold;" >
        Listado completo de los grupos taxon&oacute;micos incluidos en el diccionario 
por phylum (Nombres vigentes)
    </a>
    <span id="loadingP" style="padding-left: 20px;"></span>

    <div id="contenedor15" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 120px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">C&oacute;digo Phylum: 
            <select id="phylum" onchange="doEstadElementDiccNivelTax(this.value);">
                    <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont15" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica15" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Cod. Phylum</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Phylum</th>
                    <th style="margin: 6px 10px; padding:10px 15px; ">Nivel</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Nombre</th>
                    
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad15"></tbody> 
        </table>
         
        <a href="javascript:mpanel('contenedor15');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
     <b style="color: #006666;">21.</b> 
          
    <a href="javascript:doFichaEspecByPhylum('contenedor16');" style="color: #006666; font-weight: bold;">
        Especies con descripci&oacute;n de su historia natural por phylum
    </a>
    <span id="loadingEstdPhylum1" style="padding-left: 20px;"></span>
    <div id="contenedor16" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px;border: 1px solid #006666;" >
        
        <table id="estadistica101" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Phylum</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Fichas</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Porcentaje(%)</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad101"></tbody>
        </table>
        
        <a href="javascript:mpanel('contenedor16');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    
     <p></p>
    <b style="color: #006666;">22.</b>  
          
    <a href="javascript:doEspecieByPhylum('contenedor17');" style="color: #006666; font-weight: bold;">
      Nombres vigentes para especies en el diccionario por phylum
    </a>
    <span id="loadingEstdPhylum2" style="padding-left: 20px;"></span>
    <div id="contenedor17" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
        
         <p ></p>
        <table id="estadistica102" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Phylum</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Especies</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Porcentaje(%)</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad102"></tbody>
        </table>
        
        <p></p>
        <a href="javascript:mpanel('contenedor17');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    
     <p></p>
     
   
    
   
</div>


 <%@ include file="footer.htm" %>


</body>
</html>
