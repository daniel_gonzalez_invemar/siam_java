<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%
	String user=(String)session.getAttribute("USER");
	
	if(user==null){
		session.removeAttribute("USER");
		session.invalidate();
		response.sendRedirect("index.jsp");
	}
 %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
<title>Estadisticas del Museo de Historia Natural Marina de Colombia</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<style type="text/css">
    a:link {
        text-decoration: none;
    }
    a:visited {
        text-decoration: none;
    }
    a:hover {
        text-decoration: underline;
   }
   
   .classTHeader{
        color: #006666; 
        padding: 5px 20px;
   }
   table {
   	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
   
   }
  table thead th{
      color: #f7f7f7;
      background-color: #295d94; 
      padding: 5px 30px;
  
  }
  
  table tfoot{
  
        background-color: #295d94; 
  }

</style>
<script type="text/javascript" src="../jscript/panel.js"></script>
<script type="text/javascript" src="js/stats.js"></script>

</head>
  
  <body style=" background-color: #d7edfb;" >
	<%@ include file="header.htm" %>
	
	<div id="main" style="margin-left: 100px; ">
	    <h2>.:: Informaci&oacute;n diccionarios de especies y conjuntos de referencia ::.</h2>
	       
	    <p></p>
	   <b style="color: #006666;">1.</b> 
	    <a href="javascript:doEstadNomComuneSinonimias('contenedor20');" style="color: #006666; font-weight: bold;">
	        Numero de sinonimias y nombres comunes indexados por phylum
	    </a>
	    <span id="loadingSinNom" style="padding-left: 20px;"></span>
	    <div id="contenedor20" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
	        
	        <table id="estadistica20" style="border: 1px solid #d7edfb;">
	            <caption></caption>
	            <thead>
	                <tr>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Phylum</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Nombres Comunes</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Sinonimias</th>
	                </tr>
	            </thead>
	            <tfoot></tfoot>
	            <tbody id="tbodyEstad20"></tbody>
	        </table>
	        <p ></p>
	        
	        <a href="javascript:mpanel('contenedor20');" style="color: #003063;" >
	            [Ocultar Panel]
	        </a>
	    </div>
	     <p></p>
	     
	     <b style="color: #006666;">2.</b>
	    <a href="javascript:doFillPhylum('contenedor14');" style="color: #006666; font-weight: bold;" >
	        Grupos taxom&oacute;nicos con nombres vigentes incluidos en el diccionario por phylum
	    </a>
	    <span id="loadingPhylu" style="padding-left: 20px;"></span>
	
	    <div id="contenedor14" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 120px; border: 1px solid #006666;" >
	        <p style="color: #003063; font-weight: bold;">C&oacute;digo Phylum: 
	            <select id="elemphylum" onchange="doEstadElementosNivelTax(this.value);">
	                    <option value="--------------" selected="selected" >----------------</option>
	            </select> 
	            <span id="loadingCont14" style="padding-left: 45px; margin-left: 25px;"></span>
	        </p>
	  
	        <table id="estadistica14" style="margin-left: 50px; border: 1px solid #d7edfb;">
	            <caption></caption>
	            <thead>
	                <tr>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Nivel Tax.</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Phylum</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; ">Nivel</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Num. Elementos</th>
	                    
	                </tr>
	            </thead>
	            <tfoot></tfoot>
	            <tbody id="tbodyEstad14"></tbody> 
	        </table>
	          <div id="Cont14ShowResult"></div>
	        <a href="javascript:mpanel('contenedor14');" style="color: #003063;" >
	            [Ocultar Panel]
	        </a>
	    </div>
	    <p></p>
	    <p></p>
	 
	    <b style="color: #006666;">3.</b>
	    <a href="javascript:doFillPhylum('contenedor15');" style="color: #006666; font-weight: bold;" >
	        Listado completo de los grupos taxon&oacute;micos incluidos en el diccionario 
	por phylum (Nombres vigentes)
	    </a>
	    <span id="loadingP" style="padding-left: 20px;"></span>
	
	    <div id="contenedor15" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 120px; border: 1px solid #006666;" >
	        <p style="color: #003063; font-weight: bold;">C&oacute;digo Phylum: 
	            <select id="phylum" onchange="doEstadElementDiccNivelTax(this.value);">
	                    <option value="--------------" selected="selected" >----------------</option>
	            </select> 
	            <span id="loadingCont15" style="padding-left: 45px; margin-left: 25px;"></span>
	        </p>
	  
	        <table id="estadistica15" style="margin-left: 50px; border: 1px solid #d7edfb;">
	            <caption></caption>
	            <thead>
	                <tr>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Cod. Phylum</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Phylum</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; ">Nivel</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Nombre</th>
	                    
	                </tr>
	            </thead>
	            <tfoot></tfoot>
	            <tbody id="tbodyEstad15"></tbody> 
	        </table>
	         
	        <a href="javascript:mpanel('contenedor15');" style="color: #003063;" >
	            [Ocultar Panel]
	        </a>
	    </div>
	    <p></p>
	    <p></p>
	    <b style="color: #006666;">4.</b>  
	          
	    <a href="javascript:doEspecieByPhylum('contenedor17');" style="color: #006666; font-weight: bold;">
	      Nombres vigentes para especies en el diccionario por phylum
	    </a>
	    <span id="loadingEstdPhylum2" style="padding-left: 20px;"></span>
	    <div id="contenedor17" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
	        
	         <p ></p>
	        <table id="estadistica102" style="border: 1px solid #d7edfb;">
	            <caption></caption>
	            <thead>
	                <tr>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Phylum</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Especies</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Porcentaje(%)</th>
	                </tr>
	            </thead>
	            <tfoot></tfoot>
	            <tbody id="tbodyEstad102"></tbody>
	        </table>
	        
	        <p></p>
	        <a href="javascript:mpanel('contenedor17');" style="color: #003063;" >
	            [Ocultar Panel]
	        </a>
	    </div>
	    
	     <p></p>
	      <h2>.:: Informaci&oacute;n catalogo de especies ::.</h2>
	      
	      <p></p>
	     <b style="color: #006666;">5.</b> 
	          
	    <a href="javascript:doFichaEspecByPhylum('contenedor16');" style="color: #006666; font-weight: bold;">
	        Especies con descripci&oacute;n de su historia natural por phylum
	    </a>
	    <span id="loadingEstdPhylum1" style="padding-left: 20px;"></span>
	    <div id="contenedor16" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px;border: 1px solid #006666;" >
	        
	        <table id="estadistica101" style="border: 1px solid #d7edfb;">
	            <caption></caption>
	            <thead>
	                <tr>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Phylum</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Fichas</th>
	                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Porcentaje(%)</th>
	                </tr>
	            </thead>
	            <tfoot></tfoot>
	            <tbody id="tbodyEstad101"></tbody>
	        </table>
	        
	        <a href="javascript:mpanel('contenedor16');" style="color: #003063;" >
	            [Ocultar Panel]
	        </a>
	    </div>
	    
	     <p></p>
	    </div>
    <%@ include file="footer.htm" %>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2300620-1");
pageTracker._trackPageview();
} catch(err) {}
</script>

  </body>
</html>
