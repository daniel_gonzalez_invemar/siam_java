<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>.::Sistema de Informaci&oacute;n Ambiental Marina de Colombia - SIAM::.</title>
<meta name="keywords" content="Sistema de Información Ambiental Marina de Colombia, biología, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC, SIPEIN,pequeria">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="../siamccs.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="3"><img name="head_r1_c1" src="../images/head_r1_c1.jpg" width="3" height="81" border="0" alt=""></td>
    <td width="105" bgcolor="#096A95"><img name="head_r1_c1" src="../images/head_r1_c3.gif" border="0" alt=""></td>
    <td width="507" bgcolor="#096A95"><p class="titulohead">Sistema de Informaci&oacute;n
        sobre Biodiversidad<br>
Marina de Colombia - SIBM</p>
    </td>
    <td width="38" align="right" bgcolor="#096A95"><a href="../index.htm"><img name="head_r1_c4" src="../images/head_r1_c4.jpg" width="38" height="81" border="0" alt="Home"></a></td>
    <td width="43" align="right" bgcolor="#096A95"><a href="../contactenos.htm"><img name="head_r1_c5" src="../images/head_r1_c5.jpg" width="43" height="81" border="0" alt="Cont&aacute;ctenos"></a></td>
    <td width="34" align="right" bgcolor="#096A95"><a href="../mapadelsitio.htm"><img name="head_r1_c6" src="../images/head_r1_c6.jpg" width="34" height="81" border="0" alt="Mapa del sitio"></a></td>
    <td colspan="2" align="right" valign="top" bgcolor="#096A95"><img name="head_r1_c7" src="../images/head_r1_c7.jpg" width="4" height="81" border="0" alt=""></td>
  </tr>
  <tr>
    <td width="3"></td>
    <td width="105"></td>
    <td></td>
    <td width="38"></td>
    <td width="43"></td>
    <td width="34"></td>
    <td width="12"></td>
    <td width="8"></td>
  </tr>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#096A95">
    <td colspan="2" class="titulo_ubicacion"><img src="../images/spacer.gif" width="5" height="5"><a href="../index.htm" class="titulo_ubicacion">Inicio</a> &gt; <a href="../siam.htm" class="titulo_ubicacion">SIAM</a> &gt; <a href="index.htm" class="titulo_ubicacion">Sistema
      de Informaci&oacute;n sobre Biodiversidad Marina</a> &gt; Sistema distribuido de acopio de datos</td>
  </tr>
  <tr>
    <td colspan="2"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
   <td width="21"><img src="../images/arrow2.gif" width="20" height="20"></td>
   <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
     <tr>
       <td width="384" align="left" class="linksnegros">Sistema de Informaci&oacute;n sobre Biodiversidad
         Marina</td>
       <td width="345" align="right"><table border="0" cellpadding="0" cellspacing="0" width="320">
              <tr>
                <td width="17"><a href="index.htm"><img src="../images/orange_home.gif" width="17" height="17" border="0"></a></td>
                <td width="4">&nbsp;</td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="75"><a href="index.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Inicio
                    SIBM</a></td>
                <td width="18"><a href="legal.htm"><img src="../images/orange_legal.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="44"><a href="legal.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Legal</a></td>
                 <td width="18"><a href="index_guia_sibm.htm"><img src="../images/help.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="48"><a href="index_guia_sibm.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Ayuda</a></td>
                <td width="17"><a href="contactenos.htm"><img src="../images/orange_contact.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="79"><div align="left"><a href="contactenos.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Contactenos</a></div>
                </td>
              </tr>
            </table></td>
     </tr>
   </table></td>

  </tr>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="10" bgcolor="#B5D7DE">&nbsp;</td>
    <td width="730" bgcolor="#B5D7DE" class="texttablas">:: Ingreso ::</td>
    <td width="10" bgcolor="#B5D7DE">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td bgcolor="#E6E6E6">&nbsp;</td>
    <td bgcolor="#E6E6E6">
	<form action="loginService" method="post">
	<input type="hidden" name="page" value="statSIBM" >
	<table border="0" cellpadding="0" cellspacing="0" width="730">
		<tr>
    		<td><img src="../images/spacer.gif" width="5" height="5"></td> <td><img src="../images/spacer.gif" width="5" height="5"></td>
  		</tr>
  		<tr>
    		<td><img src="../images/spacer.gif" width="5" height="5"></td> <td><img src="../images/spacer.gif" width="5" height="5"></td>
  		</tr>
  		<tr>
    		<td><img src="../images/spacer.gif" width="5" height="5"></td> <td><img src="../images/spacer.gif" width="5" height="5"></td>
  		</tr>
  		<tr>
    		<td><img src="../images/spacer.gif" width="5" height="5"></td> <td><img src="../images/spacer.gif" width="5" height="5"></td>
  		</tr>
    	<tr>
        <td width="106" class="texttablas">Nombre de usuario: </td> 
        <td width="624"> <input name="username" type="text"></td>
      </tr>
	  <tr>
    	<td><img src="../images/spacer.gif" width="5" height="5"></td><td><img src="../images/spacer.gif" width="5" height="5"></td>
  	</tr>
	  <tr>
        <td width="106" class="texttablas">Contraseña: </td> 
        <td width="624"> <input name="password" type="password"></td>
      </tr>
	  <tr>
    	<td><img src="../images/spacer.gif" width="5" height="5"></td><td><img src="../images/spacer.gif" width="5" height="5"></td>
  	</tr>
  	<%
		String login=request.getParameter("login");
		if(login!=null && login.equalsIgnoreCase("NOTUser")){
   %>		
   			<tr>
    	<td><img src="../images/spacer.gif" width="5" height="5"></td><td style="color:#AF0A0A;">Usuario o contraseña incorrecto</td>
  	</tr>
   <%
		}
   %>
	  <tr>
        <td width="106" class="texttablas">&nbsp;
        	
        	
        </td> 
        <td width="624"><input type="submit" name="Submit" value="Ingresar"></td>
      </tr>
	  
    </table>
	</form>
	</td>
    <td bgcolor="#E6E6E6">&nbsp;</td>
  </tr>
   
  
  
  
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
   <td bgcolor="#8DC0E3"><img src="../images/spacer.gif" width="2" height="2"></td>
  </tr>


</table>
<div align="center"><a href="../index.htm" class="textfooter">Sistema de Informaci&oacute;n
      Ambiental Marina</a> <span class="textfooter">-</span> <a href="../legal.htm" class="textfooter">Condiciones
  de acceso y uso</a> <span class="textfooter">-</span> <span class="textfooter">Instituciones
participantes:</span> <a href="http://www.invemar.org.co" class="textfooter">INVEMAR</a></div>


</body>
</html>


