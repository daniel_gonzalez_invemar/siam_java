<%@ page import="co.org.invemar.catalogo.model.*" %>
<%@ include file="common.jsp" %>
<%@ page import="java.util.regex.Pattern" %>
<%@ page import="java.util.regex.Matcher" %>

<%!    static HashMap criterios = new HashMap();

    static {
        criterios.put("clase", "f.CLASE");
        criterios.put("orden", "f.ORDEN");
        criterios.put("familia", "f.FAMILIA");
        criterios.put("genero", "f.GENERO");
        criterios.put("especie", "f.ESPECIE");
    }

    static HashMap icriterios = new HashMap();

    static {
        icriterios.put("scientificname", "f.especie");
        icriterios.put("commonname", "f.NOMBRE_COMUN");
        icriterios.put("genus", "f.GENERO");
    }

    static HashMap ioperators = new HashMap();

    static {
        ioperators.put("begin", "like");
        ioperators.put("igual", "=");
        ioperators.put("contains", "like");

    }

    static List columns = new ArrayList();

    static {
        //f.REINO) AS reino, DECODE(f.DIVISION, NULL, '-', f.DIVISION) AS division, DECODE(f.CLASE, NULL, '-', f.CLASE) AS clase, DECODE(f.ORDEN, NULL, '-', f.ORDEN) AS orden, DECODE(f.FAMILIA, NULL, '-', f.FAMILIA) AS familia, DECODE(f.GENERO, NULL, '-', f.GENERO) AS genero, DECODE(f.EPITETO, NULL, '-', f.EPITETO) AS epiteto, DECODE(f.AUTORESPECIE, NULL, '-', f.AUTORESPECIE) AS autorEspecie, DECODE(f.IDENTIFICADO_POR, NULL, '-', f.IDENTIFICADO_POR) AS identificadoPor, DECODE(f.ANOIDENTIFICACION, NULL, '-', f.ANOIDENTIFICACION) AS anoIdentificacion, DECODE(f.TIPO, NULL, '-', f.TIPO) AS tipo, DECODE(f.COLECTADO_POR, NULL, '-', f.COLECTADO_POR) AS colectadoPor, DECODE(f.ANOCAPTURA, NULL, '-', f.ANOCAPTURA) AS anoCaptura, DECODE(f.NOMBRECAGUA, NULL, '-', f.NOMBRECAGUA) AS ContinentOcean, DECODE(f.PAIS, NULL, '-', f.PAIS) AS pais, DECODE(f.DEPTO, NULL, '-', f.DEPTO) AS departamento, DECODE(f.LOCALIDAD, NULL, '-', f.LOCALIDAD) AS localidad, DECODE(f.LONGITUD, NULL, '-', f.LONGITUD) AS longitud, DECODE(f.LATITUD, NULL, '-', f.LATITUD) AS latitud, DECODE(f.PROF_CAPTURA, NULL, '-', f.PROF_CAPTURA) AS profCaptura, DECODE(f.EJEMPLARES, NULL, '-', f.EJEMPLARES) AS ejemplares, f.proyecto, DECODE(f.estacion, NULL, '-', f.estacion) as estacion, f.id_metadato as metadato
        columns.add("CLAVE");
        columns.add("NOINV");
        columns.add("ULTIMOCAMBIO");
        columns.add("COLECCION");
        columns.add("CODIGOCOLECCION");
        columns.add("NUMEROCATALOGO");
        columns.add("ESPECIE");
        columns.add("BASEDELREGISTRO");
        columns.add("REINO");
        columns.add("DIVISION");

    }
%>

<%    ArrayList params = new ArrayList();
    String especie = request.getParameter("nespecie");
    String genero = param("ngenero", "");
    String division = request.getParameter("phylum");
    String criterio = (String) criterios.get(request.getParameter("criterio"));
    String valor = request.getParameter("valor");

//Criterios de busquedas de la pagina index.htm
    String pcriterio = param("icriterio", "");
    String icriterio = (String) icriterios.get(pcriterio);
    String poperator = param("ioperator", "");
    String ioperator = (String) ioperators.get(poperator);
    String ivalue = param("ivalue", "");

    String cites = request.getParameter("ncites");
    String librorojo = request.getParameter("nlibrorojo");
    String catlibrorojo = request.getParameter("ncatlibrorojo");
    String ano1 = request.getParameter("ano1");
    String ano2 = request.getParameter("ano2");

//String lat1 = request.getParameter("lat1");
//String lat2 = request.getParameter("lat2");
//String lng1 = request.getParameter("lng1");
//String lng2 = request.getParameter("lng2");
    String lat1 = param("lat1", "");
    String lat2 = param("lat2", "");
    String lng1 = param("lng1", "");
    String lng2 = param("lng2", "");

    String prof1 = request.getParameter("nprof1");
    String prof2 = request.getParameter("nprof2");
    String vulgar = request.getParameter("vulgar");
    String ambiente = request.getParameter("ambiente");
    String zona = request.getParameter("zona");
    String region = request.getParameter("nreg");
    String ecoregion = request.getParameter("ecoregion");
    String proyecto = request.getParameter("proyecto");
    String estacion = request.getParameter("estacion");

    String otherCriteria = request.getParameter("othercriterios");
    String otherColumns = request.getParameter("columns");

    boolean hasEspecie = StringUtils.isNotEmpty(especie);

    boolean hasGenero = StringUtils.isNotEmpty(genero);
    boolean hasDivision = StringUtils.isNotEmpty(division);
    boolean hasCriterio = StringUtils.isNotEmpty(criterio);
    boolean hasValor = StringUtils.isNotEmpty(valor);

//verificamos que se tengan criterios de busquedas para la busqueda simple en index.htm
    boolean hasICriterio = StringUtils.isNotEmpty(icriterio);
    boolean hasIOperator = StringUtils.isNotEmpty(ioperator);
    boolean hasIValue = StringUtils.isNotEmpty(ivalue);

    boolean hasCites = StringUtils.isNotEmpty(cites);
    boolean hasLibrorojo = StringUtils.isNotEmpty(librorojo);
    boolean hasCatlibrorojo = StringUtils.isNotEmpty(catlibrorojo);
    boolean hasAno1 = StringUtils.isNotEmpty(ano1);
    boolean hasAno2 = StringUtils.isNotEmpty(ano2);
    boolean hasLat1 = StringUtils.isNotEmpty(lat1);
    boolean hasLat2 = StringUtils.isNotEmpty(lat2);
    boolean hasLng1 = StringUtils.isNotEmpty(lng1);
    boolean hasLng2 = StringUtils.isNotEmpty(lng2);
    boolean hasProf1 = StringUtils.isNotEmpty(prof1);
    boolean hasProf2 = StringUtils.isNotEmpty(prof2);
    boolean hasVulgar = StringUtils.isNotEmpty(vulgar);
    boolean hasAmbiente = StringUtils.isNotEmpty(ambiente);
    boolean hasZona = StringUtils.isNotEmpty(zona);
    boolean hasRegion = StringUtils.isNotEmpty(region);
    boolean hasEcoregion = StringUtils.isNotEmpty(ecoregion);
    boolean hasProyecto = StringUtils.isNotEmpty(proyecto);
    boolean hasEstacion = StringUtils.isNotEmpty(estacion);

    boolean hasOtherCriteria = StringUtils.isNotEmpty(otherCriteria);
    boolean hasOtherColumns = StringUtils.isNotEmpty(otherColumns);

    if (hasGenero) {
        params.add("%" + genero + "%");
        params.add("%" + genero + "%");
    }

    if (hasEspecie) {
        params.add("%" + especie + "%");
        params.add("%" + especie + "%");
    }

    if (hasCriterio && hasValor && !criterio.equalsIgnoreCase("f.clase")) {
        params.add("%" + valor + "%");
        params.add("%" + valor + "%");
    } else if (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.clase")) {
        params.add("%" + valor + "%");
    }

//parameter de la busqueda simple
    if (hasICriterio && hasIOperator && hasIValue) {

        if (poperator.equalsIgnoreCase("begin")) {
            if (!pcriterio.equalsIgnoreCase("commonname")) {
                params.add(ivalue + "%");
            }

            params.add(ivalue + "%");
        } else if (poperator.equalsIgnoreCase("contains")) {
            if (!pcriterio.equalsIgnoreCase("commonname")) {
                params.add("%" + ivalue + "%");
            }
            params.add("%" + ivalue + "%");
        } else {
            if (!pcriterio.equalsIgnoreCase("commonname")) {
                params.add(ivalue);
            }
            params.add(ivalue);
        }
    }

    if (hasDivision) {
        params.add(division);
    }

    if (hasLibrorojo && !librorojo.equals("N") && hasCatlibrorojo) {
        params.add(catlibrorojo);
    }
    if (hasAno1) {
        params.add(ano1);
    }
    if (hasAno2) {
        params.add(ano2);
    }

//if (hasLat1) params.add(lat1);
//if (hasLat2) params.add(lat2);
//if (hasLng1) params.add(lng1);
//if (hasLng2) params.add(lng2);
    if (hasLat1) {
        params.add(Double.parseDouble(lat1));
    }
    if (hasLat2) {
        params.add(Double.parseDouble(lat2));
    }
    if (hasLng1) {
        params.add(Double.parseDouble(lng1));
    }
    if (hasLng2) {
        params.add(Double.parseDouble(lng2));
    }

    if (hasProf1) {
        params.add(prof1);
    }
    if (hasProf2) {
        params.add(prof2);
    }
    if (hasVulgar) {
        params.add("%" + vulgar + "%");
    }
    if (hasAmbiente) {
        params.add(ambiente);
    }
    if (hasZona) {
        params.add(zona);
    }
    if (hasEcoregion) {
        params.add(ecoregion);
    }

    if (hasRegion) {
        params.add(region + "%");
    }

    if (hasProyecto) {
        params.add(proyecto);
    }
    if (hasEstacion) {
        params.add(estacion);
    }

    String query = "SELECT f.clave, DECODE(f.NOINV, NULL, '-', f.NOINV) AS nroInvemar, DECODE(f.ULTIMOCAMBIO, NULL, '-', f.ULTIMOCAMBIO) AS ultimoCambio, DECODE(f.COLECCION, NULL, '-', f.COLECCION) as coleccion, DECODE(f.CODIGOCOLECCION, NULL, '-', f.CODIGOCOLECCION) AS codColeccion, DECODE(f.NUMEROCATALOGO, NULL, '-', f.NUMEROCATALOGO) AS numCatalogo, DECODE(f.ESPECIE, NULL, '-', f.ESPECIE) AS especie, DECODE(f.BASEDELREGISTRO, NULL, '-', f.BASEDELREGISTRO) AS baseRegistro, DECODE(f.REINO, NULL, '-', f.REINO) AS reino, DECODE(f.DIVISION, NULL, '-', f.DIVISION) AS division, DECODE(f.CLASE, NULL, '-', f.CLASE) AS clase, DECODE(f.ORDEN, NULL, '-', f.ORDEN) AS orden, DECODE(f.FAMILIA, NULL, '-', f.FAMILIA) AS familia, DECODE(f.GENERO, NULL, '-', f.GENERO) AS genero, DECODE(f.EPITETO, NULL, '-', f.EPITETO) AS epiteto, DECODE(f.AUTORESPECIE, NULL, '-', f.AUTORESPECIE) AS autorEspecie, DECODE(f.IDENTIFICADO_POR, NULL, '-', f.IDENTIFICADO_POR) AS identificadoPor, DECODE(f.ANOIDENTIFICACION, NULL, '-', f.ANOIDENTIFICACION) AS anoIdentificacion, DECODE(f.TIPO, NULL, '-', f.TIPO) AS tipo, DECODE(f.COLECTADO_POR, NULL, '-', f.COLECTADO_POR) AS colectadoPor, DECODE(f.ANOCAPTURA, NULL, '-', f.ANOCAPTURA) AS anoCaptura, DECODE(f.NOMBRECAGUA, NULL, '-', f.NOMBRECAGUA) AS ContinentOcean, DECODE(f.PAIS, NULL, '-', f.PAIS) AS pais, DECODE(f.DEPTO, NULL, '-', f.DEPTO) AS departamento, DECODE(f.LOCALIDAD, NULL, '-', f.LOCALIDAD) AS localidad, DECODE(f.LONGITUD, NULL, '-', f.LONGITUD) AS longitud, DECODE(f.LATITUD, NULL, '-', f.LATITUD) AS latitud, DECODE(f.PROF_CAPTURA, NULL, '-', f.PROF_CAPTURA) AS profCaptura, DECODE(f.EJEMPLARES, NULL, '-', f.EJEMPLARES) AS ejemplares, f.proyecto, DECODE(f.estacion, NULL, '-', f.estacion) as estacion, f.id_metadato as metadato, "
            + " DECODE(f.ubi_geo , NULL, '-', f.ubi_geo) AS ubicacionGeo,"
            + " DECODE(f.autor , NULL, '-',f.autor ) AS autor,"
            + " DECODE(f.nivel , NULL, '-',f.nivel ) AS nivel,"
            + " DECODE(f.nombre_comun , NULL, '-',f.nombre_comun ) AS nombreComun,"
            + " DECODE(f.variedad , NULL, '-',f.variedad ) AS variedad,"
            + " DECODE(f.autorvariedad , NULL, '-',f.autorvariedad ) AS autorVariedad,"
            + " DECODE(f.fecha_recibido , NULL, '-', f.fecha_recibido) AS fechaRecibido,"
            + " DECODE(f.tipo_especimen , NULL, '-',f.tipo_especimen ) AS tipoEspecimen,"
            + " DECODE(f.preservativo , NULL, '-',f.preservativo ) AS preservativo,"
            + " DECODE(f.adquisicion , NULL, '-',f.adquisicion ) AS adquisicion,"
            + " DECODE(f.procedencia , NULL, '-', f.procedencia) AS procedencia,"
            + " DECODE(f.colectado_por_cdg , NULL, '-', f.colectado_por_cdg) AS colectadoPorCDG,"
            + " DECODE(f.fechacap , NULL, '-',f.fechacap ) AS fechaCaptura,"
            + " DECODE(f.metodo_captura , NULL, '-',f.metodo_captura ) AS metodoCaptura,"
            + " DECODE(f.mcpio , NULL, '-',f.mcpio ) AS municipio,"
            + " DECODE(f.cuer_agua , NULL, '-',f.cuer_agua) AS cuerpoAgua,"
            + " DECODE(f.latini , NULL, '-',f.latini ) AS latitudInicial,"
            + " DECODE(f.latfin , NULL, '-',f.latfin ) AS latitudFinal,"
            + " DECODE(f.lonini , NULL, '-',f.lonini ) AS longitudInicial ,"
            + " DECODE(f.lonfin , NULL, '-',f.lonfin ) AS longitudFinal,"
            + " DECODE(f.barco , NULL, '-',f.barco ) AS barco,"
            + " DECODE(f.prof_agua , NULL, '-', f.prof_agua) AS profAgua,"
            + " DECODE(f.ambiente , NULL, '-',f.ambiente ) AS ambiente,"
            + " DECODE(f.temp_aire , NULL, '-',f.temp_aire ) AS temperaturaAire,"
            + " DECODE(f.temp_agua , NULL, '-',f.temp_agua ) AS temperaturaAgua,"
            + " DECODE(f.salinidad , NULL, '-',f.salinidad ) AS salinidad,"
            + " DECODE(f.publicado , NULL, '-',f.publicado ) AS publicado,"
            + " DECODE(f.fechaidentifacacion , NULL, '-',f.fechaidentifacacion ) AS fechaIdentificacion,"
            + " DECODE(f.fechaactualizo , NULL, '-',f.fechaactualizo ) AS fechaActualizo,"
            + " DECODE(f.fechainicial , NULL, '-',f.fechainicial ) AS fechaInicial,"
            + " DECODE(f.profundidadmin , NULL, '-',f.profundidadmin ) AS profundidadMin,"
            + " DECODE(f.profundidadmax , NULL, '-',f.profundidadmax ) AS profundidadMax,"
            + " DECODE(f.profundidadexacta , NULL, '-',f.profundidadexacta ) AS profundidadExacta,"
            + " DECODE(f.ecorregion , NULL, '-',f.ecorregion ) AS ecorregion,"
            + " DECODE(f.ecorregion_cdg , NULL, '-',f.ecorregion_cdg ) AS ecorregionCDG,"
            + " DECODE(f.region , NULL, '-',f.region ) AS region,"
            + " DECODE(f.tev_preser , NULL, '-', f.tev_preser) AS tevPreservado,"
            + " DECODE(f.tev_ejemplares , NULL, '-',f.tev_ejemplares ) AS tevEjemplares,"
            + " DECODE(f.zona , NULL, '-',f.zona ) AS zona,"
            + " DECODE(f.uicn_global , NULL, '-',f.uicn_global ) AS uicnGlobal,"
            + " DECODE(f.uicn_nacional , NULL, '-',f.uicn_nacional ) AS uicnNacional,"
            + " DECODE(f.cites , NULL, '-',f.cites ) AS cites,"
            + " DECODE(f.notas_muestra , NULL, '-',f.notas_muestra ) AS notasMuestra,"
            + " DECODE(f.notas_id , NULL, '-',f.notas_id ) AS notasIdentificacion,"
            + " DECODE(f.notas_captura , NULL, '-',f.notas_captura ) AS notasCaptura,"
            + " DECODE(f.notas_es , NULL, '-',f.notas_es ) AS notasEstacion,"
            + " DECODE(f.notas_cr , NULL, '-',f.notas_cr ) AS notasCrucero,"
            + " DECODE(f.consecutivo_muestra , NULL, '-',f.consecutivo_muestra ) AS consecutivoMuestra,"
            + " DECODE(f.tipocosta_con , NULL, '-',f.tipocosta_con ) AS tipoCostaCon,"
            + " DECODE(f.olas , NULL, '-',f.olas ) AS olas,"
            + " DECODE(f.mareas_con , NULL, '-',f.mareas_con ) AS mareasCon,"
            + " DECODE(f.corriente_con , NULL, '-',f.corriente_con ) AS corrienteCon,"
            + " DECODE(f.transp_con , NULL, '-',f.transp_con ) AS transpCon,"
            + " DECODE(f.tiempo , NULL, '-',f.tiempo ) AS tiempo,"
            + " DECODE(f.notas_con , NULL, '-',f.notas_con ) AS notasCon,"
            + " DECODE(f.hora_con , NULL, '-',f.hora_con ) AS horaCon, "
            + " DECODE(f.ubicacion , NULL, '-',f.ubicacion ) AS ubicacion, "
            + " decode(camp,null,'-',camp) as camp, "
            + " decode(qualityflag,null,'-',qualityflag) as qualityflag,"
            + " decode(objetos,null,'-',objetos) as objetos, "   
            + " decode(tecnica_ident,null,'-',tecnica_ident) as tecnica_ident, "
            + " decode(longfindecimal,null,'-',longfindecimal) as longfindecimal, "
            + " decode(latfindecimal,null,'-',latfindecimal) as latfindecimal "
            + " FROM VM_FICHAC f "
            + " WHERE ROUND(MOD(TRUNC((NODOS_ACTIVOS/1),1),10),0) = 1 "
            //+ (hasGenero? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "") 

            //+(hasEspecie? " and UPPER(f.EPITETO) IN (SELECT vigente FROM (SELECT NULL anterior, especie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT especie, epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")

            + (hasGenero ? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))" : "")
            + (hasEspecie ? " AND UPPER(f.EPITETO) IN (SELECT vigente FROM (SELECT NULL anterior, especie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT especie, epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))" : "")
            + (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.especie") ? " AND UPPER(replace(f.especie,' ','')) IN (SELECT UPPER(replace(vigente,' ','')) FROM ( SELECT NULL anterior, genero ||decode(subgenero, NULL,'',' ') ||subgenero ||' ' || decode(especie,NULL,'SP.',especie)||decode(subespecie, NULL,'',' ') ||subespecie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero ||' ' || especie, genero_vigente ||' ' || epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior " + ioperator + " UPPER (?) OR vigente " + ioperator + " UPPER (?)) " : "")
            + (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.GENERO") ? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior " + ioperator + " UPPER (?) OR vigente " + ioperator + " UPPER (?))" : "")
            + (hasIOperator && hasIValue && icriterio.equalsIgnoreCase("f.NOMBRE_COMUN") ? " AND  UPPER(f.NOMBRE_COMUN) " + ioperator + " UPPER(?)" : "")
            /*+ (hasCriterio && hasValor? " AND " + criterio + " like UPPER(?)": "")*/
            + (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.genero") ? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))" : "")
            + (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.especie") ? " AND UPPER(replace(f.especie,' ','')) IN (SELECT UPPER(replace(vigente,' ','')) FROM ( SELECT NULL anterior, genero ||decode(subgenero, NULL,'',' ') ||subgenero ||' ' || decode(especie,NULL,'SP.',especie)||decode(subespecie, NULL,'',' ') ||subespecie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero ||' ' || especie, genero_vigente ||' ' || epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?)) " : "")
            + (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.orden") ? " AND UPPER (f.orden) IN ( SELECT vigente FROM (SELECT NULL anterior, orden vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT orden, orden_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))" : "")
            + (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.familia") ? " AND UPPER (f.familia) IN ( SELECT vigente FROM (SELECT NULL anterior, familia vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT familia, familia_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))" : "")
            + (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.clase") ? " AND UPPER (f.clase) like UPPER(?)" : "")
            + (hasDivision ? " AND f.DIVISION = ?" : "")
            //+ (hasCriterio && hasValor? " AND UPPER(" + criterio + ") like UPPER(?)": "")
            + (hasCites ? " AND  f.CITES IS " + (cites.equals("N") ? "NULL" : "NOT NULL") : "")
            + (hasLibrorojo ? " AND f.UICN_NACIONAL IS " + (librorojo.equals("N") ? "NULL" : "NOT NULL" + (hasCatlibrorojo ? " AND substr(f.UICN_NACIONAL,1,2) = ?" : "")) : "")
            + (hasAno1 ? " AND f.ANOCAPTURA >= ?" : "")
            + (hasAno2 ? " AND f.ANOCAPTURA <= ?" : "")
            + (hasLat1 ? " AND f.LATITUD >= ?" : "")
            + (hasLat2 ? " AND f.LATITUD <= ?" : "")
            + (hasLng1 ? " AND f.LONGITUD >= ?" : "")
            + (hasLng2 ? " AND f.LONGITUD <= ?" : "")
            + (hasProf1 ? " AND f.PROFUNDIDADMAX >= ?" : "")
            + (hasProf2 ? " AND f.PROFUNDIDADMIN <= ?" : "")
            + (hasVulgar ? " AND UPPER(f.NOMBRE_COMUN) LIKE UPPER(?)" : "")
            + (hasAmbiente ? " AND f.AMBIENTE = ?" : "")
            + (hasZona ? " AND f.ZONA = ?" : "")
            + (hasEcoregion ? " AND f.ECORREGION LIKE ?" : "")
            + (hasRegion ? " AND f.REGION LIKE ?" : "")
            + (hasProyecto ? " AND f.proyecto = ?" : "")
            + (hasEstacion ? " AND f.estacion=?" : "")
            + (hasOtherCriteria ? " AND " + otherCriteria : "")
            + " ORDER BY ESPECIE";
    //System.out.println("REGBIOQUERY: " + query);
    Object[] parameters = params.toArray();
    List list = (List) sibm.query(query, parameters, new BeanListHandler(Catalogo.class, new BasicRowProcessor() {
        public Object toBean(ResultSet rs, Class type) throws SQLException {
            Catalogo cat = (Catalogo) super.toBean(rs, type);
            Imagenes img = new Imagenes();
            img.setArchivoIm("");
            img.setClaveIm("");
            img.setNroInvemarIm("");
            cat.setImg(img);
            return cat;
        }
    }), false);
    session.removeAttribute("LISTOBJECT");
    session.setAttribute("LISTOBJECT", list);
    request.getRequestDispatcher("catalogo.jsp").forward(request, response);
//response.sendRedirect("catalogo.jsp");
%><%--=list--%>