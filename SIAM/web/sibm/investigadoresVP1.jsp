
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es"> 

<head> 

<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title> 
<link href="../siamccs.css" rel="stylesheet" type="text/css"/> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"> 
<meta name="keywords" content="invemar, ciencia marina, investigaci�n, marino, costera, costero"> 
<meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC"> 
<meta http-equiv="pragma" content="no-cache"> 
<meta http-equiv="cache-control" content="no-cache"> 
<meta http-equiv="expires" content="0"> 
<script type="text/javascript" src="../1ibre/scriptaculous-combo.js"></script> 
<!--script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script--> 
<script type="text/javascript" src="../1ibre/1ibre.js"></script> 
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script> 
<script type="text/javascript" src="../index.js"></script> 
<script type="text/javascript" src="investigadores.js"></script> 
<link type="application/jsonrequest" href="investigadores-info.jsp" rel="datasource" name="info"> 
 <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css">
<link type="application/jsonrequest" rel="datasource" name="registros">
<!--</head> "El validador no reconoce la etiqueta de apertura del head por tanto marca error al cerrar la etiqueta"-->

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> 
<jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=34"/>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3"><img src="../images/spacer.gif" width="5" height="5" alt="espaciador"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
   <td width="346" bgcolor="#8DC0E3" class="linksnegros">Listado de investigadores</td>
    <td width="382" align="right">&nbsp;</td>
  </tr>
</table>

<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5" alt="espaciador"></td>
  </tr>
</table>
<form method="get" action="javascript: recargar()" name="formasmc" style="margin: 0px">
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0" background="../images/orange_bar2.jpg">
  <tr>
    <td width="10"><img src="../images/orange_bar2.jpg" width="3" height="69" alt="barra"></td>
    <td width="131" align="left" valign="top"><table border="0" cellpadding="1" cellspacing="1" width="130">
      <tr>
        <td width="18" align="left" valign="top">&nbsp;</td>
        <td width="212"><a href="#" class="linksnegros">Nombre</a></td>
      </tr>
    </table>
      <table border="0" cellpadding="1" cellspacing="1" width="130">
        <tr>
          <td width="18">&nbsp;</td>
          <td width="212"><input name="nombre" class="inputText2" title="nombre">
          </td>
        </tr>
      </table></td>
    <td width="1">&nbsp;</td>
    <td width="130" align="left" valign="top"><table border="0" cellpadding="1" cellspacing="1" width="130">
      <tr>
        <td width="18" align="left" valign="top">&nbsp;</td>
        <td width="212"><a href="#" class="linksnegros">Nacionalidad</a></td>
      </tr>
    </table>
      <table border="0" cellpadding="1" cellspacing="1" width="130">
        <tr>
          <td width="18">&nbsp;</td>
          <td width="212"><select name="nacionalidad" id="nodoNacionalidad" class="inputText2" title="Nacionalidad">
                    <option value="">--</option>
                    <option for-each="info.nacionalidades" item="nombre" key="id" visibleWhen="mostrarNacionalidad('`id`')" value="`id`">`nombre`</option>
                  </select><!--input name="textfield2322" type="text" class="inputText2" size="20"-->
          </td>
        </tr>
      </table></td>
   <td width="478" align="left" valign="top"><table border="0" cellpadding="1" cellspacing="1" width="180">
     <tr>
       <td align="left" valign="top">&nbsp;</td>
       </tr>
   </table>
    </td>

  </tr>
</table>
<table width="963px" align="center"  style="border:0px;padding:0px;border-spacing:0px">
  <tr>
    <td width="260"><table style="border:0px;padding:0px;border-spacing:0px" width="200">
      <tr></tr>
      <tr>
        <td width="2"></td>
        <td width="196" align="center" valign="top"><table  width="240" bordercolor="#8DC0E3" border="2"  align="center" cellpadding="1" cellspacing="1">
          <tr>
            <td class="titulosnegros">T&iacute;tulo</td>
          </tr>
          <tr>
            <td><select name="titulo" id="nodoTitulo" class="inputText2" title="Titulo">
              <option value="">--</option>
              <option for-each="info.titulos" item="titulo" visiblewhen="mostrarTitulo('`titulo`')" value="`titulo`">`titulo`</option>
            </select></td>
          </tr>
        </table></td>
      </tr>     
    </table></td>
    <td width="85">&nbsp;</td>
    <td width="252"><table style="border:0px;padding:0px;border-spacing:0px" width="200"> 
      <tr>
        <td width="2"></td>
        <td width="196" align="center" valign="top"><table width="240" bordercolor="#8DC0E3" border="2" align="center" cellpadding="1" cellspacing="1">
            <tr>
              <td class="titulosnegros">Estudios</td>
            </tr>
            <tr>
              <td><select name="estudio" id="nodoEstudio" class="inputText2" title="Estudios">
                <option value="">--</option>
                <option for-each="info.estudios" item="estudio" visiblewhen="mostrarEstudio('`estudio`')" value="`estudio`">`estudio`</option>
              </select></td>
            </tr>
          </table>
        </td>
      </tr>    
    </table></td>
    <td width="83">&nbsp;</td>
   <td width="259"><table style="border:0px;padding:0px;border-spacing:0px" width="200">   
     <tr>
       <td width="2">&nbsp;</td>
       <td width="196" align="center" valign="top"><div class="centrado"><table width="240" bordercolor="#8DC0E3" border="2" align="center" cellpadding="1" cellspacing="1">
           <tr>
             <td class="titulosnegros">Instituto</td>
           </tr>
           <tr>
             <td><select name="instituto" id="nodoInstituto" class="inputText2" title="Instituto">
                    <option value="">--</option>
                    <option for-each="info.institutos" item="instituto" visibleWhen="mostrarInstituto('`instituto`')" value="`instituto`">`instituto`</option>
               </select>
             </td>
           </tr>
         </table></div>
       </td>
     </tr>  
   </table></td>

  </tr>
</table>

<table width="963px" style="border:0px;padding:1px;border-spacing:1px" align="center" >
  <tr>
    <td><img src="../images/spacer.gif" width="2" height="2" alt="espaciador"></td>
  </tr>
  <tr>
    <td align="right"><input type="submit" value="Buscar"></td>
  </tr>
  <tr>
    <td><table width="100%" align="center" style="border:0px;padding:0px;border-spacing:0px">     
    </table></td>
  </tr>
</table>
</form>
<table align="center" style="width:963px;border:0px;padding:0px;border-spacing:0px" if="registros">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5" alt="espaciador"></td>
  </tr>
  <tr>
    <td><table style="border:0px;padding:1px;border-spacing:1px"  width="100%">
      <tr>
        <td><span class="titulostablas">Listado de Investigadores</span></td>
      </tr>
    </table></td>
  </tr>
  <tr if="registros.items.length">
    <td whatwg="datagrid" datasource="registros" length="10" id="datasource"><table align="center" border="0" width="963px">
      <thead>
        <tr bgcolor="#cccccc">
          <td bgcolor="#B5D7DE" class="sortable sorted titulostablas" field="nombre" width="80">
            Nombre
          </td>
          <td bgcolor="#B5D7DE" class="sortable titulostablas" field="apellido" width="80">
            Apellido
          </td>
          <td bgcolor="#B5D7DE" class="sortable titulostablas" field="titulo">
            T&iacute;tulo
          </td>
          <td bgcolor="#B5D7DE" class="sortable titulostablas" field="nacionalidad">
            Pa&iacute;s
          </td>
          <td bgcolor="#B5D7DE" class="sortable titulostablas" field="estudio" width="80">
            Estudios
          </td>
          <td bgcolor="#B5D7DE" class="sortable titulostablas" field="instituto" width="80">
            Instituto
          </td>
          <td bgcolor="#B5D7DE" width="80">&nbsp;</td>
        </tr>
      </thead>
      <tbody>
        <tr for-each="registros.items" item="registro">
          <td bgcolor="#e6e6e6" height="20">&nbsp;<span class="texttablas">`registro.nombre`</span></td>
          <td bgcolor="#e6e6e6" height="20"><span class="texttablas">`registro.apellido`</span> </td>
          <td bgcolor="#e6e6e6" height="20"><span class="texttablas">`registro.titulo`</span> </td>
          <td bgcolor="#e6e6e6" height="20"><span class="texttablas">`info.nacionalidades[registro.nacionalidad]`</span> </td>
          <td bgcolor="#e6e6e6" height="20"><span class="texttablas">`registro.estudio`</span></td>
          <td bgcolor="#e6e6e6" height="20"><span style="font-family:Arial, Helvetica, sans-serif;font-size:0.7em">`registro.instituto` </span></td>
          <td bgcolor="#e6e6e6" height="20" align="center" href="investigadorPV1.jsp?id=`registro.id`"><span style="font-family:Arial, Helvetica, sans-serif;font-size:0.7em">Detalles</span></td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="7"><table width="100%" align="center" style="border:0px;padding:0px;border-spacing:0px">
 


</table></td>
          </tr>
        <tr>
          <td colspan="7">
            <div align="center">
              <span if="registros.position > 0"><a href="javascript: $('datasource').previousPage()" class="linksnavinferior">&lt;&lt; Anterior</a><span class="linksnavinferior"> -</span></span>
                  <span class="linksnavinferior">`registros.position + 1` a `registros.position + registros.items.length`</span><!--P&aacute;gina 1 de 8--><span if="registros.hasNext"><span> -</span> <a href="javascript: $('datasource').nextPage()" class="linksnavinferior">Siguiente &gt;&gt;</a></span>
              </div>
          </td>
        </tr>
      </tfoot>
    </table></td>
  </tr>
  <tr else>
    <td>No hay resultados para mostrar</td>
  </tr>
  <tr>
    <td bgcolor="#8F5444"><img src="../images/spacer.gif" width="1" height="1" alt="espaciador"></td>
  </tr>
</table>
<table width="963px" align="center" style="border:0px;padding:0px;border-spacing:0px"> 
  <tr>
    <td width="8" bgcolor="#B5D7DE">&nbsp;</td>
    <td width="103" align="left" valign="top" bgcolor="#B5D7DE" class="linksnegros"><table width="100" align="left" style="border:0px;padding:2px;border-spacing:2px">
        <tr>
          <td class="linksnegros">Miembros de: </td>
        </tr>
      </table>
    </td>
    <td width="484" bgcolor="#B5D7DE"><table width="100" style="border:0px;padding:2px;border-spacing:2px">
        <tr>
          <td><a href="http://www.siac.net.co/Home.php" target="_blank"><img src="../images/banner_sib.gif" width="115" height="49" border="0" alt="banner"></a></td>
        </tr>
      </table>
    </td>
    <td width="484" bgcolor="#B5D7DE"><table width="100" style="border:0px;padding:2px;border-spacing:2px">
        <tr>
          <td align="center"><a href="http://www.iobis.org/" target="_blank"><img src="../images/banner_obis.gif" width="150" height="43" border="0" alt="banner2"></a></td>
        </tr>
      </table>
    </td>
    <td width="484" bgcolor="#B5D7DE"><table width="100" style="border:0px;padding:2px;border-spacing:2px">
      <tr>
        <td><a href="http://www.iabin.net/" target="_blank"><img src="../images/iabin.gif" alt="IABIN" border="0"></a></td>
      </tr>
    </table></td>
  </tr>
</table>
  <%@ include file="../plantillaSitio/footermodulesV3.jsp" %>
</body><h1></h1>
</html>

