var MapSibmEstacion=new Class.create({
	initialize:function(divmap){
		this.map=new GMap2($(divmap), {mapTypes: [G_NORMAL_MAP, G_HYBRID_MAP, G_SATELLITE_MAP]});
		this.markers=[];
		
		this.queryParams=location.href.toQueryParams();
		
		this.minLatitud= null;
		this.maxLatitud= null;
		this.minLongitud= null;
		this.maxLongitud= null;
		
		this.redImg='http://siam.invemar.org.co/siam/sibm/images/estacionRa.png';
		this.yellowImg='http://siam.invemar.org.co/siam/sibm/images/estacionAa.png';
		this.shadowImg='http://siam.invemar.org.co/siam/sibm/images/estacionSombra.png';
		
		this._initializeMapBase();
	},
	_initializeMapBase:function(){
		
		this.map.addControl(new GLargeMapControl(), new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(10, 10)));
		this.map.addControl(new GOverviewMapControl());
		this.map.addControl(new GMapTypeControl(), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(10, 10)));
		this.map.addControl(new GScaleControl());
		this.map.enableContinuousZoom();
		this.map.setCenter(new GLatLng(11, -75), 8);
		this.renderEstation();
	},
	getIcon:function(img){
		
		var icon = new GIcon();
		
		icon.image = img;
		icon.shadow = this.shadowImg;
		icon.iconSize = new GSize(32, 32);
		icon.shadowSize = new GSize(56, 32);
		icon.iconAnchor = new GPoint(16, 32);
		icon.infoWindowAnchor = new GPoint(16, 0);
		return icon;
	},
	renderEstation:function(){
		
		var param="action=ListaEstaciones&proyect="+this.queryParams.proyecto;
		
		new Ajax.Request('/siam/sibm/proyectsServices',{
				parameters:param,
				onSuccess:this._renderEstationCallback.bind(this)
										
		});
		
	},
	_renderEstationCallback:function(request){
		
		var response=request.responseText.evalJSON(true);
		var _marker;
		var _polyline;
		var info;
		var markerOptionsRed = { icon:this.getIcon(this.redImg) };
		var markerOptionsYellow = { icon:this.getIcon(this.yellowImg) };
		for(var i=0;i<response.length;i++){
			
			if(!response[i].latfin && !response[i].longfin){
				if(response[i].latinicio || response[i].longinicio){
					info={proyecto:this.queryParams.nombre,titulo:'Estaci\xF3n',nombre:response[i].estacion,latitud:response[i].latinicio,longitud:response[i].longinicio}
					
					
					_marker=this.createMarker(info,markerOptionsRed);
					this.markers.push(_marker);
					this.map.addOverlay(_marker);
				}
			}else{
				_polyline=this.createPolyLine(response[i],markerOptionsYellow);
				this.map.addOverlay(_polyline);
			}
			
			 if (this.minLatitud == null || response[i].latinicio < this.minLatitud) this.minLatitud = response[i].latinicio;
		     if (this.maxLatitud == null || response[i].latinicio > this.maxLatitud) this.maxLatitud = response[i].latinicio;
		     if (this.minLongitud == null || response[i].longinicio < this.minLongitud) this.minLongitud = response[i].longinicio;
		     if (this.maxLongitud == null || response[i].longinicio > this.maxLongitud) this.maxLongitud = response[i].longinicio;
		
		}
		
		this.zoomMap(parseFloat(this.minLatitud),parseFloat(this.maxLatitud),parseFloat(this.minLongitud),parseFloat(this.maxLongitud));
			
		
	},
	zoomMap:function(minLatitud,maxLatitud,minLongitud,maxLongitud){
				
		this.map.setCenter(new GLatLng((minLatitud + maxLatitud) / 2, (minLongitud + maxLongitud) / 2));
		this.map.setZoom(8);
  		//var zoomLevel = this.map.getBoundsZoomLevel(new GLatLngBounds(new GLatLng(minLatitud, minLongitud), new GLatLng(maxLatitud, maxLongitud)));
		//console.log('zoomLevel '+zoomLevel);
  		//this.map.setZoom(zoomLevel < 10 ? zoomLevel: 10);	
	},
	createMarker:function(info,markerOptions) {
		
		  var marker = new GMarker(new GLatLng(info.latitud,info.longitud),markerOptions);
		 
		   GEvent.addListener(marker, "click", this.openInfoWin.bind(this,{object:marker,messaje:info}));
		
		  return marker;
	},
	createPolyLine:function(data,markerOptions){
		var point1=new GLatLng(data.latinicio,data.longinicio);
		var point2=new GLatLng(data.latfin,data.longfin);
		
		var polyline = new GPolyline([
			  point1,
			  point2
	  ], "#f2bf24", 10);
	  
	  var info={
	  			proyecto:this.queryParams.nombre,
	  			titulo:'Inicio de Estaci\xF3n',
				nombre:data.estacion,
				latitud:data.latinicio,
				longitud:data.longinicio
			}
	  this.map.addOverlay(this.createMarker(info,markerOptions));
	
	  info={
	  			proyecto:this.queryParams.nombre,
	  			titulo:'Fin de Estaci\xF3n',
				nombre:data.estacion,
				latitud:data.latfin,
				longitud:data.longfin
			}
	  this.map.addOverlay(this.createMarker(info,markerOptions));
	  return polyline;
	},
	openInfoWin:function(){
		
		var options=arguments[0];
		
		var myHtml = '<table border="0" cellpadding="4" cellspacing="2">'+
					 '  <tr> <td> <span style="font-weight: bold">'+options.messaje.titulo+' :</span></td> <td><span style="font-weight: bold">'+options.messaje.nombre+'</span></td> </tr>'+
					 '  <tr> <td><span style="font-weight: bold">Latitud: </span> </td> <td>'+options.messaje.latitud+'</td> </tr>'+
					 '  <tr> <td><span style="font-weight: bold">Longitud: </span></td> <td>'+options.messaje.longitud+'</td> </tr>'+
					  '  <tr> <td><span style="font-weight: bold">Proyecto: </span></td> <td>'+options.messaje.proyecto+'</td> </tr>'+
					 '</table> ';
		
		
		//console.log('myHtml '+myHtml);
					
		options.object.openInfoWindowHtml(myHtml);
	}
	
		
});

Event.observe(window, 'load',function(event){
			new MapSibmEstacion('map');									  
	});
	
function PopWindow(popPage,windowName,winWidth,winHeight)
{

    var winLeft = (screen.width -winWidth)/2;
    var winTop = (screen.height -winHeight)/2;
    newWindow = window.open(popPage,windowName,'width=' + winWidth + ',height='+ winHeight + ',top=' + winTop +',left=' + winLeft + ',resizable=yes,scrollbars=yes,status=yes');
}