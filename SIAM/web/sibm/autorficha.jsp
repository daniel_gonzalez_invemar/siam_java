<%@ page language="java" import="co.org.invemar.catalogo.model.*, java.sql.*, java.util.*" %>
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color: white;">
  <tr>
    <td width="2"><img src="../images/orange_box_r1_c1.gif" width="2" height="2"></td>
    <td background="../images/orange_box_r1_c2.gif"><img src="../images/orange_box_r1_c2.gif" width="200" height="2"></td>
    <td width="2"><img src="../images/orange_box_r1_c3.gif" width="2" height="2"></td>
  </tr>
  <tr>
    <td width="2" background="../images/orange_box_r2_c1.gif"><img src="../images/orange_box_r2_c1.gif" width="2" height="51"></td>
    <td align="center" valign="top"><table width="100%" border="0" cellpadding="1" cellspacing="1">
        <tr>
          <td width="10" valign="top" class="texttablas" align="center">



  <table id="autorias" border="0" cellpadding="3" cellspacing="1" width="100%">
  <thead>
    <tr class="draggable" style="background-color:#B5D7DE;"><td colspan="4" align="center">Autores de la ficha</td></tr>
  <tr><th>Nombre</th><th>Seccion</th><th>Fecha</th><th>Tarea</th></tr>
  </thead>
  <tfoot></tfoot>
  <tbody>
  <%
        String clave=request.getParameter("clave");
        
        List autores=null;
        DAOFactory df;
        df = new DAOFactory();
        Connection conn=null;
        SearchCatalogoDAO autorDao=df.getSearchCatalogoDAO();
        try{
            conn=df.createConnection();
            autores=autorDao.findAutoresFicha(clave,conn,SearchCatalogoDAO.SQL_AUTORESFICHA);
                    
        }catch(Exception e){
            System.out.println("EXCEPTION ....."+e);
        }
        if(!autores.isEmpty())    {
            Iterator it=autores.iterator();
        
            while(it.hasNext()){
            AutorFicha autor=(AutorFicha)it.next();
  %>
        <tr><td style="font-style: italic;white-space: nowrap;"><%=autor.getNombre()%></td>
            <td style="font-style: italic; white-space: nowrap;"><%=autor.getDescripcion()%></td><td style="font-style: italic; white-space: nowrap;"><%=autor.getFecha()%></td>
            <td style="font-style: italic; white-space: nowrap;"><%=autor.getTarea()%></td>
        </tr>
        <%  }
        }else{
    %>
    <tr><td colspan="4"><p align="center">No se encontraron registros</p></td></tr>
    <%}%>
    
  </tbody>
  </table>
            <div style="text-align: center; margin: 3px">
              <a href="#" onclick="Element.getParentPopup(this).hidePopup(); return false">Cerrar</a>
            </div>
          </td>
        </tr>
      </table>
    </td>
    <td width="2" background="../images/orange_box_r2_c3.gif"><img src="../images/spacer.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td width="2"><img src="../images/orange_box_r3_c1.gif" width="2" height="2"></td>
    <td background="../images/orange_box_r3_c2.gif"><img src="../images/orange_box_r3_c2.gif" width="200" height="2"></td>
    <td width="2"><img src="../images/orange_box_r3_c3.gif" width="2" height="2"></td>
  </tr>
</table>
