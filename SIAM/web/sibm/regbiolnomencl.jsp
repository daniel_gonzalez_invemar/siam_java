<%@ page language="java" import="co.org.invemar.siam.sibm.vo.Nomenclatura"%>
<%@ include file="sibm_cabezote.jsp" %>


<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#096A95">
    <td colspan="2" class="titulo_ubicacion"><img src="../images/spacer.gif" width="5" height="5"><a href="../index.htm" class="titulo_ubicacion">Inicio</a> &gt; <a href="../siam.htm" class="titulo_ubicacion">SIAM</a> &gt; <a href="index.htm" class="titulo_ubicacion">Sistema
      de Informaci&oacute;n sobre Biodiversidad Marina</a> &gt;Objetos relacionados registros biol&oacute;gicos </td>
  </tr>
  <tr>
    <td colspan="2"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
    <td width="21"><img src="../images/arrow2.gif" width="20" height="20"></td>
    <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td width="354" align="left" class="linksnegros">Datos de especie</td>
          <td width="375" align="right"><table border="0" cellpadding="0" cellspacing="0" width="320">
              <tr>
                <td width="17"><a href="index.htm"><img src="../images/orange_home.gif" width="17" height="17" border="0"></a></td>
                <td width="4">&nbsp;</td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="75"><a href="index.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Inicio
                    SIBM</a></td>
                <td width="18"><a href="legal.htm"><img src="../images/orange_legal.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="44"><a href="legal.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Legal</a></td>
                 <td width="18"><a href="index_guia_sibm.htm"><img src="../images/help.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="48"><a href="index_guia_sibm.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Ayuda</a></td>
                <td width="17"><a href="contactenos.htm"><img src="../images/orange_contact.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="79"><div align="left"><a href="contactenos.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Contactenos</a></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table align="center" border="0" width="750">
  <tbody>

  <tr>
    <td bgcolor="#B5D7DE" class="txttaxon1">*Phylum:</td>
    <td width="575" bgcolor="#e6e6e6" class="txttaxon3"><%=request.getParameter("phylum")%> </td>
  </tr>
  <tr>
    <td bgcolor="#B5D7DE" class="txttaxon1">*Tax&oacute;n:</td>
    <td bgcolor="#e6e6e6"><span class="txttaxon2"><%=request.getParameter("nombre")%></span>&nbsp;&nbsp;<span class="txttaxon1"><%=request.getParameter("autor")%></span></td>
  </tr>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">*N&uacute;mero catalogo:</td>
    <td bgcolor="#e6e6e6" class="texttablas"><%=request.getParameter("catalogo")%><br>
</td>
  </tr>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">*N&uacute;mero Invemar:</td>
    <td bgcolor="#e6e6e6" class="texttablas"><%=request.getParameter("numero")%></td>
  </tr>
  <tr>
    <td colspan="2"></td>
    </tr>
  <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
  <!-- FIN DEL BLOQUE QUE GENERA LA FILOGENIA -->
</table>
<table align="center" border="0" width="750">
  <tbody>
    <tr>
      <td colspan="3"><img src="../images/spacer.gif" width="1" height="1"></td>
    </tr>
    <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
    <tr bgcolor="#B5D7DE">
      <td ><span class="linksnegros">Nombre</span></td>
	  <td width="118" ><span class="linksnegros">Fecha</span></td>
	  <td width="52" ><span class="linksnegros">Vigente</span></td>
	  <td width="265" ><span class="linksnegros">Notas</span></td>
	   <td width="154" ><span class="linksnegros">Tecnicas</span></td>
    </tr>
    <%	List<Nomenclatura> nomenclaturas=(ArrayList)request.getAttribute("NOMENC");
    	Nomenclatura nomenclatura=null;
    
    	for(Iterator<Nomenclatura> i=nomenclaturas.iterator();i.hasNext();){
    	   nomenclatura=i.next();
     %>
    <tr bgcolor="#ffffff">
      <td width="139" valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="texttablas"><%=nomenclatura.getName() %></td>
          </tr>
      </table>      </td>
        <td  valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="txttaxon4"><%=nomenclatura.getFecha() %></td>
          </tr>
        </table>      </td>
		 <td  valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="txttaxon4"><%=nomenclatura.getActual()%></td>
          </tr>
        </table>      </td>
		 <td  valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="txttaxon4"><%=nomenclatura.getNotas() %></td>
          </tr>
        </table>      </td>
		 <td  valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="txttaxon4"><%=nomenclatura.getTecnica() %></td>
          </tr>
        </table>      </td>
    </tr>
   <%} %>
   
</table>

<%@ include file="sibm_fondo.jsp" %>