<%@ include file="common.jsp" %>
<%@ page import="conex.*" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="especies.*" %>
<%
  /*Cconexsib con1 = null;
  Connection conn1 = null;
  con1 = new Cconexsib();
  conn1 = con1.getConn();*/
  Connection conn1 = sibmDS.getConnection();

  int k = 0;

  //carga phylum
  especies.Cphylum phylum = new especies.Cphylum();
  especies.Ccontphylum contphylum = new especies.Ccontphylum();
  contphylum.contenedor(conn1);

  //niveles taxonomicos
  especies.Cniveltax niveltax = new especies.Cniveltax();
  especies.Ccontniveltax contniveltax = new especies.Ccontniveltax();
  contniveltax.contenedor(conn1);

  especies.Ctaxones taxones =  new especies.Ctaxones();
  taxones.a_phylum = request.getParameter("phylum");
  taxones.a_niveltx = request.getParameter("nivel");
  taxones.a_consecutivo = request.getParameter("nivel1");
  taxones.cargataxon1(conn1);

  especies.Cficha ficha = new especies.Cficha();
  especies.Ccontficha contficha = new especies.Ccontficha();


  int h = 0;

  String phyl = "";
  String niv = "";

//BUSQUEDA DEL NOMBRE DEL PHYLUM Y GENERO
  for(h = 0; h < contphylum.gettamano(); h++){
    phylum = new especies.Cphylum();
    phylum = contphylum.getphylum(h);
    if(phylum.a_codigo.equals(taxones.a_phylum))
      phyl = phylum.a_descripcion;
  }
  for(h = 0; h < contniveltax.gettamano(); h++){
    niveltax = new especies.Cniveltax();
    niveltax = contniveltax.getniveltax(h);
    if(niveltax.a_nivel.equals(taxones.a_niveltx))
      niv = niveltax.a_descripcion;
  }


  float y = 0;
  int tam = 0;
  int paginas = 1;

  //pagina actual del resultado 1= valor inicial
  int pag = 1;

  //numero de registros por pagina
  float registros = 20;
  String pagina = request.getParameter("pagina");

  contficha.contenedor_sibficha1(conn1, taxones.a_phylum, taxones.a_consecutivo, taxones.a_consecutivo1, request.getParameter("ncites"), request.getParameter("ncatlibrorojo"), request.getParameter("nlibrorojo"), request.getParameter("nprof1"), request.getParameter("nprof2"), request.getParameter("nvulgar"), request.getParameter("nambiente"), request.getParameter("nzona"), request.getParameter("neco"), request.getParameter("nreg"), request.getParameter("clase"), request.getParameter("orden"), request.getParameter("genero"), request.getParameter("reino"), request.getParameter("especie"), request.getParameter("subclase"), request.getParameter("superfamilia"), request.getParameter("infraorden"), request.getParameter("nphylum"), request.getParameter("ngenero"), request.getParameter("nespecie"));

  paginas =  (int)Math.ceil((contficha.gettamano() / registros));
  if(pagina != null)
     pag = Integer.parseInt(pagina);
  if(paginas == 0)
      paginas = 1;

   /*if (con1!= null)
    con1.close();*/
   conn1.close();
%>
<!-- MENSAJE CON EL TIPO Y NOMBRE DE NIVEL SELECCIONADO -->
<%-- if (request.getParameter("phylum") != null ) { %>
<strong><font color="#336666" size="1" face="Arial, Helvetica, sans-serif">Phylum </font><font color="#336666" face="Arial, Helvetica, sans-serif"><%=phyl%></font></strong>
<% } %>
<% if (request.getParameter("clase") != null ) { %>
<strong><font color="#336666" size="1" face="Arial, Helvetica, sans-serif">Clase </font><font color="#336666" face="Arial, Helvetica, sans-serif"><%=request.getParameter("clase")%></font></strong>
<% } %>
<% if (request.getParameter("orden") != null ) { %>
<strong><font color="#336666" size="1" face="Arial, Helvetica, sans-serif">Orden </font><font color="#336666" face="Arial, Helvetica, sans-serif"><%=request.getParameter("orden")%></font></strong>
<% } %>
<% if (request.getParameter("genero") != null ) { %>
<strong><font color="#336666" size="1" face="Arial, Helvetica, sans-serif">Genero </font><font color="#336666" face="Arial, Helvetica, sans-serif"><%=request.getParameter("genero")%></font></strong>
<% } %>
<% if (request.getParameter("subclase") != null ) { %>
<strong><font color="#336666" size="1" face="Arial, Helvetica, sans-serif">Subclase </font><font color="#336666" face="Arial, Helvetica, sans-serif"><%=request.getParameter("subclase")%></font></strong>
<% } %>
<% if (request.getParameter("superfamilia") != null ) { %>
<strong><font color="#336666" size="1" face="Arial, Helvetica, sans-serif">Superfamlia </font><font color="#336666" face="Arial, Helvetica, sans-serif"><%=request.getParameter("superfamilia")%></font></strong>
<% } %>
<% if (request.getParameter("infraorden") != null ) { %>
<strong><font color="#336666" size="1" face="Arial, Helvetica, sans-serif">Infraorden </font><font color="#336666" face="Arial, Helvetica, sans-serif"><%=request.getParameter("infraorden")%></font></strong>
<% } --%>


<table border="1" align="center" cellpadding="0" cellspacing="0" style="background-color: white; width: 300">
  <tr>
    <td width="2"><img src="../images/orange_box_r1_c1.gif" width="2" height="2"></td>
    <td background="../images/orange_box_r1_c2.gif"><img src="../images/orange_box_r1_c2.gif" width="200" height="2"></td>
    <td width="2"><img src="../images/orange_box_r1_c3.gif" width="2" height="2"></td>
  </tr>
  <tr>
    <td width="2" background="../images/orange_box_r2_c1.gif"><img src="../images/orange_box_r2_c1.gif" width="2" height="51"></td>
    <!--td width="196" align="center" valign="top">
      <div style="overflow: auto; width: 150px; height: 500px" whatwg="datagrid" length="50" id="resultados" datasource="datos">
        <table cellpadding="3" if="ibre.datasource.values.datos">
          <tr for-each="datos.items" item="item" index="i">
            <td href="javascript: markers[`i`].showInfo()">`item.especie`</td>
          </tr>
          <tr if="!datos.total">
            <td>No se encontraron resultados</td>
          </tr>
        </table>
      </div>
    </td-->
    <td align="center" valign="top">
    <table width="100%" border="1" cellpadding="1" cellspacing="1">
        <tr>
          <td width="10" valign="top" class="texttablas" align="center">
            <div style="text-align: center; margin: 3px; background-color: #B5D7DE;" class="titulostablas draggable">
              <%= StringUtils.defaultString(request.getParameter("clase"), StringUtils.defaultString(request.getParameter("orden"), StringUtils.defaultString(request.getParameter("genero"), StringUtils.defaultString(request.getParameter("subclase"), StringUtils.defaultString(request.getParameter("superfamilia"), StringUtils.defaultString(request.getParameter("infraorden"), phyl)))))) %>
            </div>
            <div style="background-color: white; max-height: 400px; overflow: auto">
            <table width="100%" border="0" align="center">
              <%
              for(y = 0; y < contficha.gettamano(); y++){
                ficha = new especies.Cficha();
                ficha = contficha.getficha((int)y);
                if(ficha.get_aautor() == null)
                  ficha.setaautor("");
                if(ficha.get_anvulgar() == null)
                  ficha.setanvulgar("");

              %>
              <tr>
                <td><font face="Arial, Helvetica, sans-serif" size="2"><b><a href="zsibficha2.jsp?clave=<%=ficha.get_aclave()%>&amp;phyl=<%=phyl%>&amp;niv=<%=niv%>&amp;esp=<%=request.getParameter("basura2")%>&amp;nombre=<%=ficha.get_adescripcion()%>&amp;phylum=<%=ficha.get_acodphylum()%>&amp;nomphylum=<%=ficha.get_aphylum()%>"><i><%=ficha.get_adescripcion()%></i></a></b></font></td>
              </tr>
              <% } if (contficha.gettamano() == 0) { %>
              <tr>
                <td class="texttablas" style="text-align: center">No hay informaci&oacute; para mostrar</td>
              </tr>
              <% } %>
            </table>
            </div>
            <div style="text-align: center; margin: 3px">
              <a href="#" onclick="Element.getParentPopup(this).hidePopup(); return false">Cerrar</a>
            </div>
          </td>
        </tr>
      </table>
    </td>
    <td width="2" background="../images/orange_box_r2_c3.gif"><img src="../images/spacer.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td width="2"><img src="../images/orange_box_r3_c1.gif" width="2" height="2"></td>
    <td background="../images/orange_box_r3_c2.gif"><img src="../images/orange_box_r3_c2.gif" width="200" height="2"></td>
    <td width="2"><img src="../images/orange_box_r3_c3.gif" width="2" height="2"></td>
  </tr>
</table>
