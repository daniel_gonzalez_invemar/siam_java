<%--
Este archivo es incluido por todos los mapas del SIBM (Google Maps, KGSMapper y c-squares)
Contiene la informacion inicial para ejecutar la consulta deacuerdo a los parametros del formulario
--%>
<%@ include file="../common.jsp" %>
<%!
/*
Criterios por los cuales se puede efectuar una busqueda en los mapas del SIBM
*/
static HashMap criterios = new HashMap();
static {
  criterios.put("clase", "f.CLASE");
  criterios.put("orden", "f.ORDEN");
  criterios.put("familia", "f.FAMILIA");
  criterios.put("genero", "f.GENERO");
  criterios.put("epiteto", "f.EPITETO");
  criterios.put("especie", "f.ESPECIE");
}
%>
<%
/* Parametros de la consulta SQL */
ArrayList params = new ArrayList();

/* Se leen los parametros de busqueda */
String division = request.getParameter("phylum");
String criterio = (String)criterios.get(request.getParameter("criterio"));
String valor = request.getParameter("valor");
String cites = request.getParameter("ncites");
String librorojo = request.getParameter("nlibrorojo");
String catlibrorojo = request.getParameter("ncatlibrorojo");
String ano1 = request.getParameter("ano1");
String ano2 = request.getParameter("ano2");
String lat1 = request.getParameter("lat1");
String lat2 = request.getParameter("lat2");
String lng1 = request.getParameter("lng1");
String lng2 = request.getParameter("lng2");
String prof1 = request.getParameter("nprof1");
String prof2 = request.getParameter("nprof2");
String vulgar = request.getParameter("vulgar2");
//String ambiente = request.getParameter("ambiente");
//String zona = request.getParameter("zona");
//String region = request.getParameter("nreg");
//String ecoregion = request.getParameter("ecoregion");
//String proyecto=request.getParameter("proyecto");
//String estacion=request.getParameter("estacion");


String ocriterio=request.getParameter("othercriterios");
//System.out.println("OCRITERIO :.. "+ocriterio);
String otherCriteria=URLDecoder.decode(ocriterio,"UTF-8");
//System.out.println("OtherCriterios en maps.jsp:.. "+otherCriteria);
String otherColumns=request.getParameter("columns");


/* Variables que definen que parametros vienen */
boolean hasDivision = StringUtils.isNotEmpty(division);
boolean hasCriterio = StringUtils.isNotEmpty(criterio);
boolean hasValor = StringUtils.isNotEmpty(valor);
boolean hasCites = StringUtils.isNotEmpty(cites);
boolean hasLibrorojo = StringUtils.isNotEmpty(librorojo);
boolean hasCatlibrorojo = StringUtils.isNotEmpty(catlibrorojo);
boolean hasAno1 = StringUtils.isNotEmpty(ano1);
boolean hasAno2 = StringUtils.isNotEmpty(ano2);
boolean hasLat1 = StringUtils.isNotEmpty(lat1);
boolean hasLat2 = StringUtils.isNotEmpty(lat2);
boolean hasLng1 = StringUtils.isNotEmpty(lng1);
boolean hasLng2 = StringUtils.isNotEmpty(lng2);
boolean hasProf1 = StringUtils.isNotEmpty(prof1);
boolean hasProf2 = StringUtils.isNotEmpty(prof2);
boolean hasVulgar = StringUtils.isNotEmpty(vulgar);
//boolean hasAmbiente = StringUtils.isNotEmpty(ambiente);
//boolean hasZona = StringUtils.isNotEmpty(zona);
//boolean hasRegion = StringUtils.isNotEmpty(region);
//boolean hasEcoregion = StringUtils.isNotEmpty(ecoregion);
//boolean hasProyecto=StringUtils.isNotEmpty(proyecto);
//boolean hasEstacion=StringUtils.isNotEmpty(estacion);

boolean hasOtherCriteria=StringUtils.isNotEmpty(otherCriteria);
boolean hasOtherColumns=StringUtils.isNotEmpty(otherColumns);

/* Agregamos los parametros ques eusaran en la consulta SQL */
if (hasDivision) params.add(division);
if (hasCriterio && hasValor && !criterio.equalsIgnoreCase("f.clase")) {
	params.add("%" + valor + "%");
	params.add("%" + valor + "%");
}else if (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.clase")) {
	params.add("%" + valor + "%");
}
if (hasLibrorojo && !librorojo.equals("N") && hasCatlibrorojo) params.add(catlibrorojo);
if (hasAno1) params.add(ano1);
if (hasAno2) params.add(ano2);

//System.out.println("===== maps =====");
//System.out.println("LAT1: "+lat1);
//System.out.println("LAT2: "+lat2);
//System.out.println("LNG1: "+lng1);
//System.out.println("LNG2: "+lng2);
//System.out.println("================");

if (hasLat1) params.add(Double.parseDouble(lat1));
if (hasLat2) params.add(Double.parseDouble(lat2));
if (hasLng1) params.add(Double.parseDouble(lng1));
if (hasLng2) params.add(Double.parseDouble(lng2));
//System.out.println("LAT1:.. "+lat1);
if (hasProf1) params.add(prof1);
if (hasProf2) params.add(prof2);
if (hasVulgar) params.add("%" + vulgar + "%");
//if (hasAmbiente) params.add(ambiente);
//if (hasZona) params.add(zona);
//if (hasRegion) params.add(region + "%");
//if (hasEcoregion) params.add(ecoregion);
//if(hasProyecto) params.add(proyecto);
//if(hasEstacion) params.add(estacion);

/* Leemos la configuracion que desea el cliente: registro inicial y la cantidad */
String jsonParam = request.getParameter("json");
JSONObject obj = StringUtils.isEmpty(jsonParam)? null: new JSONObject(jsonParam);
int position = obj == null? 0: obj.optInt("position", 0);
int length = obj == null? 200: obj.optInt("length", 200);

String query="SELECT f.clave, f.ultimocambio, f.codigocoleccion, f.numerocatalogo, f.especie, f.reino, f.division, f.clase, f.orden, f.familia, f.genero, f.identificado_por, f.colectado_por, f.anocaptura, f.depto, f.localidad, f.longitud, f.latitud, f.prof_captura, f.AUTOR AS autores "
//+ (hasOtherColumns? " ,"+otherColumns: "") 
+" FROM VM_FICHAC f"
+ " WHERE f.LATITUD IS NOT NULL AND f.LONGITUD IS NOT NULL "
+ (hasDivision? " AND f.DIVISION = ?": "") 
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.genero") ? " AND UPPER(f.GENERO) IN (SELECT vigente FROM (SELECT NULL anterior, genero vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero, genero_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.epiteto") ? " AND UPPER(f.EPITETO) IN (SELECT vigente FROM (SELECT NULL anterior, especie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT especie, epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.especie") ? " AND UPPER(f.especie) IN (SELECT vigente FROM (SELECT NULL anterior, genero ||' ' || especie vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT genero ||' ' || especie, genero_vigente ||' ' || epiteto_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.orden") ? " AND UPPER (f.orden) IN ( SELECT vigente FROM (SELECT NULL anterior, orden vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT orden, orden_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "")
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.familia") ? " AND UPPER (f.familia) IN ( SELECT vigente FROM (SELECT NULL anterior, familia vigente FROM vm_planilla WHERE nombre_vigente = 1 UNION SELECT familia, familia_vigente FROM vm_planilla WHERE nombre_vigente = 0) WHERE anterior LIKE UPPER (?) OR vigente LIKE UPPER (?))": "") 
+ (hasCriterio && hasValor && criterio.equalsIgnoreCase("f.clase") ? " AND UPPER (f.clase) like UPPER(?)": "")  
+ (hasCites? " AND  f.CITES IS " + (cites.equals("N")? "NULL": "NOT NULL"): "")
+(hasLibrorojo? " AND f.UICN_NACIONAL IS " + (librorojo.equals("N")? "NULL": "NOT NULL" + (hasCatlibrorojo? " AND substr(f.UICN_NACIONAL,1,2) = ?": "")): "")
+ (hasAno1? " AND f.ANOCAPTURA >= ?": "") + (hasAno2? " AND f.ANOCAPTURA <= ?": "")
 + (hasLat1? " AND f.LATITUD >= ?": "") + (hasLat2? " AND f.LATITUD <= ?": "") 
 + (hasLng1? " AND f.LONGITUD >= ?": "") + (hasLng2? " AND f.LONGITUD <= ?": "") 
 + (hasProf1? " AND f.PROFUNDIDADMAX >= ?": "") + (hasProf2? " AND f.PROFUNDIDADMIN <= ?": "")
  + (hasVulgar? " AND UPPER(f.NOMBRE_COMUN ) LIKE UPPER(?)": "") 
 // + (hasAmbiente? " AND f.AMBIENTE = ?": "") 
 // + (hasZona? " AND f.ZONA = ?": "") 
//+ (hasRegion? " AND f.REGION LIKE ?": "")
 //+ (hasEcoregion? " AND f.ECORREGION = ?": "") 
 // + (hasProyecto? " AND f.proyecto = ?":"")
 //  + (hasEstacion? " AND f.estacion=?":"")
   
 + " and ROUND(MOD(TRUNC((NODOS_ACTIVOS/1),1),10),0) = 1"
 + (hasOtherCriteria? " AND "+otherCriteria: "")
 + " ORDER BY ESPECIE";

/* Es SQL de la consulta */
//System.out.println("QUERY:... "+query);
//System.out.println("PARAMS:... "+params.toArray().toString());

Object[] parameters = params.toArray();
%>