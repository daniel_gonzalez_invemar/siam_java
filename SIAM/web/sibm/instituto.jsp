<%@ include file="../common.jsp" %>
<%
response.setContentType("text/html");
Map instituto = (Map)sibm.query("SELECT DISTINCT NOMBRE_IN AS nombre, CODIGO_IN AS codigo, DIRECCION_IN AS direccion, CIUDAD_IN AS ciudad, DPTO_IN AS departamento, NOMBRE_PA AS nacionalidad, CORREOE_IN AS correo, WEBSITE_IN AS web, POSTAL_IN AS postal, AP_AEREO_IN AS apartado, TELEFONO_IN AS telefono, FAX_IN AS fax, TITULOD_IN AS titulo, DIRECTOR_IN AS director, OFICINA_IN AS oficina, CONTACTONAME_IN AS contacto, CARGOCONTACTO_IN AS cargo FROM CDIRINSTITUTO, CPAISES WHERE PAIS_IN = CODIGO_PA(+) AND CODIGO_IN = ?", new Object[] {request.getParameter("id")}, row, false);
%>
<html>
<head>
<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../siamccs.css" rel="stylesheet" type="text/css">
<meta name="keywords" content="Sistema de Informaci&oacute;n Ambiental Territorial del Pac&iacute;fico Colombiano, biolog&iacute;a, bases de datos, metadatos, SIAT, corales, Pac&iacute;fico, Colombia, calidad ambiental, REDCAM, INVEMAR, IIAP">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="3"><img name="head_r1_c1" src="../images/head_r1_c1.jpg" width="3" height="81" border="0" alt=""></td>
    <td width="105" bgcolor="#096A95"><img name="head_r1_c1" src="../images/head_r1_c3.gif" border="0" alt=""></td>
    <td width="507" bgcolor="#096A95"><p class="titulohead">Sistema de Informaci&oacute;n
        Ambiental<br>
Marina de Colombia - SIAM</p>
    </td>
    <td width="38" align="right" bgcolor="#096A95"><a href="../index.htm"><img name="head_r1_c4" src="../images/head_r1_c4.jpg" width="38" height="81" border="0" alt="Home"></a></td>
    <td width="43" align="right" bgcolor="#096A95"><a href="../contactenos.htm"><img name="head_r1_c5" src="../images/head_r1_c5.jpg" width="43" height="81" border="0" alt="Cont&aacute;ctenos"></a></td>
    <td width="34" align="right" bgcolor="#096A95"><a href="../mapadelsitio.htm"><img name="head_r1_c6" src="../images/head_r1_c6.jpg" width="34" height="81" border="0" alt="Mapa del sitio"></a></td>
    <td colspan="2" align="right" valign="top" bgcolor="#096A95"><img name="head_r1_c7" src="../images/head_r1_c7.jpg" width="4" height="81" border="0" alt=""></td>
  </tr>
  <tr>
    <td width="3"></td>
    <td width="105"></td>
    <td></td>
    <td width="38"></td>
    <td width="43"></td>
    <td width="34"></td>
    <td width="12"></td>
    <td width="8"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#096A95">
    <td colspan="3" class="titulo_ubicacion"><img src="../images/spacer.gif" width="5" height="5"><a href="../index.htm" class="titulo_ubicacion">Inicio</a> &gt; <a href="../siam.htm" class="titulo_ubicacion">SIAM</a> &gt; <a href="index.htm" class="titulo_ubicacion">SIBM</a> &gt; <a href="institutos.htm" class="titulo_ubicacion">Listado de entidades</a> &gt; <%= instituto.get("nombre") %></td>
  </tr>
  <tr>
    <td colspan="3"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
   <td width="20"><img src="../images/arrow2.gif" width="20" height="20"></td>
   <td width="342" bgcolor="#8DC0E3" class="linksnegros"><%= instituto.get("nombre") %></td>
    <td width="388" align="right"><table border="0" cellpadding="0" cellspacing="0" width="320">
          <tr>
            <td width="17"><a href="index.htm"><img src="../images/orange_home.gif" width="17" height="17" border="0"></a></td>
            <td width="4">&nbsp;</td>
            <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="75"><a href="index.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Inicio
                SIBM</a></td>
            <td width="18"><a href="legal.htm"><img src="../images/orange_legal.gif" width="17" height="17" border="0"></a></td>
            <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="44"><a href="legal.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Legal</a></td>
             <td width="18"><a href="index_guia_sibm.htm"><img src="../images/help.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="48"><a href="index_guia_sibm.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Ayuda</a></td>
            <td width="17"><a href="contactenos.htm"><img src="../images/orange_contact.gif" width="17" height="17" border="0"></a></td>
            <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="79"><div align="left"><a href="contactenos.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Contactenos</a></div>
            </td>
          </tr>
        </table></td>

  </tr>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table align="center" border="0" width="750">
  <tbody>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">Nombre:</td>
    <td width="575" bgcolor="#e6e6e6" class="txttaxon2"><%= instituto.get("nombre") %></td>
  </tr>
  <tr>
    <td colspan="2" class="txttaxon1"><img src="../images/spacer.gif" width="1" height="1"></td>
    </tr>
</table>
<table align="center" border="0" width="750">
  <tbody>
    <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
    <tr>
      <td colspan="3" bgcolor="#B5D7DE">&nbsp; <img src="../images/arrowx.gif" width="5" height="7">&nbsp; <span class="linksnegros">Detalles</span></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6" width="165"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">C&oacute;digo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("codigo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Direcci&oacute;n</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("direccion")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Ciudad</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("ciudad")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Departamento</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("departamento")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Pa&iacute;s</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("nacionalidad")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Sitio Web</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><a href="http://<%= StringUtils.defaultString((String)instituto.get("web")) %>"><%= StringUtils.defaultString((String)instituto.get("web")) %></a></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">C&oacute;digo postal</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("postal")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Apartado Aereo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("apartado")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Correo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("correo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Tel&eacute;fono</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("telefono")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Fax</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("fax")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">T&iacute;tulo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("titulo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Director</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("director")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Oficina</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("oficina")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Contacto</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("contacto")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Cargo</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" bgcolor="#ffffff" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%= StringUtils.defaultString((String)instituto.get("cargo")) %></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#ffffff"><div align="right"><a href="#top" class="linksbotton">Volver arriba</a> </div></td>
    </tr>

</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
   <td bgcolor="#B5D7DE"><img src="../images/spacer.gif" width="2" height="2"></td>
  </tr>


</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#E6E6E6">
    <td colspan="4"><img src="../images/spacer.gif" width="3" height="3"></td>
  </tr>
  <tr>
    <td width="8" bgcolor="#B5D7DE">&nbsp;</td>
    <td width="103" align="left" valign="top" bgcolor="#B5D7DE" class="linksnegros"><table width="100" border="0" align="left" cellpadding="2" cellspacing="2">
        <tr>
          <td class="linksnegros">Miembros de: </td>
        </tr>
      </table>
    </td>
    <td width="155" bgcolor="#B5D7DE"><div align="center">
      <table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.siac.net.co/Home.php" target="_blank"><img src="../images/banner_sib.gif" width="115" height="49" border="0"></a></td>
          </tr>
        </table>
    </div></td>
    <td width="484" bgcolor="#B5D7DE"><div align="center">
      <table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.iobis.org/" target="_blank"><img src="../images/banner_obis.gif" width="150" height="43" border="0"></a></td>
          </tr>
        </table>
    </div></td>
    <td width="484" bgcolor="#B5D7DE"><div align="center">
      <table width="100" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><a href="http://www.iabin.net/" target="_blank"><img src="../images/iabin.gif" alt="IABIN" border="0"></a></td>
          </tr>
      </table>
    </div></td>

  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><div align="center"><a href="../index.htm" class="textfooter">Sistema de
          Informaci&oacute;n Ambiental Marina</a> <span class="textfooter">-</span> <a href="../legal.htm" class="textfooter">Condiciones
          de acceso y uso</a> <span class="textfooter">-</span> <span class="textfooter">Instituciones
          participantes:</span> <a href="http://www.invemar.org.co" class="textfooter">INVEMAR</a></div>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2300620-1";
urchinTracker();
</script>

</body>
</html>