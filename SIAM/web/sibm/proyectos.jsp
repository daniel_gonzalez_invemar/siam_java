<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@page import="co.org.invemar.util.ConnectionFactory"%>
<%@page import="co.org.invemar.siam.sibm.model.ProyectosDM"%>
<%@page import="java.sql.Connection"%>
<jsp:directive.page import="co.org.invemar.siam.sibm.vo.Localidad"/>
<jsp:directive.page import="co.org.invemar.siam.sibm.vo.Proyecto"/>
<jsp:directive.page import="org.apache.commons.lang.*"/>
<jsp:directive.page import="co.org.invemar.siam.sibm.model.CountFacade"/>
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META name="keywords" content="invemar, ciencia marina, investigac&iacute;on, marino, costera, costero">
<meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0"><link href="../plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css"/>
 <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
 <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1" />



<script type="text/javascript">
<!--

//-->
function PopWindow(popPage,windowName,winWidth,winHeight)
{
   
//the center properties
    var winLeft = (screen.width -winWidth)/2;
    var winTop = (screen.height -winHeight)/2;

    newWindow = window.open(popPage,windowName,'width=' + winWidth + ',height='+ winHeight + ',top=' + winTop +',left=' + winLeft + ',resizable=yes,scrollbars=yes,status=yes');
}
</script>

</head>
<body>
<jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=30"/>
<div>
	<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
    <td colspan="2"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
    <td width="21"></td>
    <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td align="left" class="linksnegros">Proyectos<br></td>
          </tr>
      </table>
    </td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"></td>
  </tr>
  <tr>
    <td></td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="16" bgcolor="#E6E6E6"><div align="center" class="linksnavinferior">
	    			<form method="get" action="fichaCatagoloService" id="formproys">
	    				<input type="hidden" name="action" value="Proyecto" />
	    				<input type="hidden" name="cdo" value="<%=request.getParameter("cdo") %>" />
					<table cellpadding="0" border="0"><tr><td><span class="linksnegros">Seleccione el proyecto</span></td>
	    				<td>
	    					<%  
	    					String proj=request.getParameter("proy");
	    					List<Proyecto> projects=(List)request.getAttribute("PROYS");
	    					Proyecto proyecto=null;
	    					if(projects!=null){
	    						if(proj==null){
	    							proyecto=projects.get(0);
	    					 %>
	    					<select name="proy" onchange="javascript:document.getElementById('formproys').submit();">
	    						<%for (Proyecto project: projects){ %>
	    							<option value="<%=project.getCodigo() %>"><%=project.getNombreAlterno() %></option>
	    						<%} %>
	    					</select>
	    					<%}else{ 	%>
	    						<select name="proy" onchange="javascript:document.getElementById('formproys').submit();">
	    						<%for (Proyecto project: projects){ 
	    								if(String.valueOf(project.getCodigo()).equals(proj)){
	    									proyecto=project;
	    						%>
	    							<option value="<%=project.getCodigo() %>" selected="selected"><%=project.getNombreAlterno() %></option>
	    						<%		}else{ %>
	    							<option value="<%=project.getCodigo() %>" ><%=project.getNombreAlterno() %></option>
	    							<%} %>
	    					
	    					<%} %>
	    						</select>
	    					<%}
	    					} %>
	    				</td>
	    				</tr></table>
  
	    			</form>
	    				
		    </div></td>
  </tr>
  
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"></td>
  </tr>
  <tr>
    <td></td>
  </tr>
</table>

<%if(projects!=null && proyecto!=null){ %>
	
	<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
 		<tr bgcolor="#E6E6E6"> 
 		   <td  bgcolor="#B5D7DE">
	 		   <table border="0" cellspacing="0">
			 		<td width="200" height="24"  bgcolor="#B5D7DE" align="left" class="linksnegros">Proyecto </td>
			 		<td  bgcolor="#B5D7DE"><span style="vertical-align:middle;">
			 		<%if(proyecto.getMetadato()!=0){ %>
			 			<a class="linksnegros" target="_blank" href="http://cinto.invemar.org.co/metadatos/showMetadato.jsp?conjunto=<%=proyecto.getMetadato() %>">Ver metadatos del proyecto aqu&iacute;</a>
			 		<%} %>
			 		
			 		</span></td>
		 	   </table>
	 	   </td>
 		</tr>
</table>
	
	
	<table width="963px" border="0" align="center" cellpadding="4" cellspacing="1">
	
	 		
			  <tr>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Titulo</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Nombre alterno</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Periodo de ejecucion</td>
			  </tr>
			  <tr>
			  	<td bgcolor="#E6E6E6" class="texttablas"><%=proyecto.getTitulo() %></td>
				<td nowrap="nowrap" class="texttablas" bgcolor="#E6E6E6"><%=proyecto.getNombreAlterno() %></td>
				<td nowrap="nowrap" class="texttablas" bgcolor="#E6E6E6"><%=proyecto.getFechaInicio() %></td>
			</tr>
			</table>
		<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
	<% 
		ConnectionFactory cFactory = null;
		Connection connection = null;
		cFactory = new ConnectionFactory();
		ProyectosDM pdm=new ProyectosDM();
	try{	
	connection = cFactory.createConnection();
	List<Localidad> estaciones= pdm.findStationByProject(connection,proyecto.getCodigo());%>
	<table align="center" border="0" width="100%">
	  <tbody>
	  	<tr bgcolor="#E6E6E6" >
			<td colspan="24" >
			  <table border="0" width="100%" cellspacing="0">
			  <tr>
				<td  width="200" height="24"  align="left" class="linksnegros">Estaciones Asociadas </td>
				<td width="300"><img src="./images/browser.png" border="0" style="vertical-align:middle;"/><a class="linksnegros" href="javascript:PopWindow('estacionmap.jsp?proyecto=<%=proyecto.getCodigo() %>&nombre=<%=proyecto.getNombreAlterno() %>','Estaciones','890','600')">Ver distribuci&oacute;n espacial aqu&iacute;</a></td>
				<td ><img src="./images/Excel_icon.png" border="0" style="vertical-align:middle;"/><a class="linksnegros" href="./fichaCatagoloService?action=ProyectoExcel&proy=<%=proyecto.getCodigo() %>&name=<%=proyecto.getNombreAlterno()%>"> Descargar informaci&oacute;n en formato excel</a></td>
			   </tr>				
			  </table>
			</td>
		</tr>
		
	 	 <tr>
		
		
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas"><span class="titulostablas">Estacion</span></td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Ecorregi&oacute;n</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Top&oacute;nimo</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Zona protegida</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Descripcion</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Pais</td>
				
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Lugar</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Latitud de inicio</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Latitud de inicio</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Latitud de fin</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Latitud de fin</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Longitud de inicio</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Longitud de inicio</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Longitud de fin</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Longitud de fin</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Cuerpo de agua</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Distancia de costa</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Tipo de costa</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Profundidad</td>
				
				
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Barco</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Campa&ntilde;a</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Sustrato</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Ambiente</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Notas</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Mar o R&iacute;o</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Salinidad</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Profundidad min (Metros)</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Profundidad max (Metros)</td>
				<td nowrap="nowrap" bgcolor="#B5D7DE" class="titulostablas">Vigente</td>
				
				
			  </tr>
			  <%for(Localidad estacion:estaciones){ %>
			  <tr>
			  	<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getPrefijoEstacion()==""?"":estacion.getPrefijoEstacion()+" "%><%=estacion.getCodigoEstacion() %></td>
			  	<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getEcorregion() %></td>
			  	<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getToponimo() %></td>
			  	<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getZonaProtegida() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getDescripcionEstacion() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getPais() %></td>
				
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getLugar() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getLatitudInicio() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getLatitudInicioGMS() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getLatitudFin() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getLatitudFinGMS() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getLongitudInicio() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getLongitudInicioGMS() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getLongitudFin() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getLongitudFinGMS() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getCuerpoAgua() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getDistanciaCosta() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getTipoCosta() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getHondoAgua() %></td>
				
				
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getBarco() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getCampana() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getSustrato() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getAmbiente() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getNotas() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getMarrio() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getSalodulce() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getProfMin() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getProfMax() %></td>
				<td nowrap="nowrap" bgcolor="#E6E6E6" class="texttablas"><%=estacion.getVigente() %></td>
			
				
			  </tr>
			  <%} %>
				
	   </tbody>
	</table>
	<%}finally{ConnectionFactory.closeConnection(connection);} %>
<%} %>
		<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="16" bgcolor="#E6E6E6"><div align="center" class="linksnavinferior">
	    			
		    </div></td>
  </tr>
  
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
	</div>
	 <%@ include file="../plantillaSitio/footermodulesV3.jsp" %>
	


