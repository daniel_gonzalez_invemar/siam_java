<%@ include file="maps.jsp" %>
<%
response.setContentType("text/xml");
List list = (List)sibm.query(/*limit(*/query/*, position + 1, length)*/, parameters, new MapListHandler(), false);
HashMap points = new HashMap();
for (int i = 0; i < list.size(); i++) {
  Map item = (Map)list.get(i);
  points.put(item.get("latitud") + " " + item.get("longitud") + " " + item.get("clave"), new Integer(i));
}
%>
<?xml version="1.0" encoding="UTF-8"?>
<OBISModel
  xmlns="http://www.opengis.net/examples"
  xmlns:gml="http://www.opengis.net/gml"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://tsadev.speciesanalyst.net/obis/obis.xsd">

  <gml:boundedBy>
    <gml:Box>
      <gml:coord>
        <gml:X></gml:X>

      </gml:coord>
      <gml:coord>
        <gml:X></gml:X>
      </gml:coord>
    </gml:Box>
  </gml:boundedBy>

<%
Set set = points.keySet();
Iterator iterator = set.iterator();
while (iterator.hasNext()) {
  String key = (String)iterator.next();
  String[] point = StringUtils.split(key);
  int index = ((Integer)points.get(key)).intValue();
  Map item = (Map)list.get(index);
%><OBISMember><BioData><species><%= item.get("especie") %></species><genus><%= item.get("genero") %></genus><gml:location><gml:Point><gml:coord><gml:X><%= point[1] %></gml:X><gml:Y><%= point[0] %></gml:Y></gml:coord></gml:Point></gml:location></BioData></OBISMember><% } %>

</OBISModel>