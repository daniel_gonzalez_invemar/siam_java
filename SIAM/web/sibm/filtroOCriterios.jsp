<%@ include file="../common.jsp" %>

<%
JSONObject obj = (JSONObject)json;
JSONObject elements = obj.getJSONObject("elements");
String criterio = elements.getString("campo");
ArrayList params = new ArrayList();

params.add(obj.getString("value") + "%");

response.setContentType("application/jsonrequest; charset=UTF-8");
response.getWriter().write(sibm.query(limit("SELECT DISTINCT " + criterio + " AS value FROM VM_FICHAC WHERE " + criterio + " IS NOT NULL AND ROUND(MOD(TRUNC((NODOS_ACTIVOS/1),1),10),0) = 1 AND UPPER(" + criterio + ") LIKE UPPER(?)" + " ORDER BY " + criterio, obj.getInt("position"), obj.getInt("length")), params.toArray(), all).toString());
%>
