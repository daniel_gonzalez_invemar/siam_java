<%@ include file="../common.jsp" %>
<%response.setContentType("application/jsonrequest; charset=ISO-8859-1");%>
<%!
static HashMap fields = new HashMap();
static {
  fields.put("nombre", "NOMBRE_CO");
  fields.put("apellido", "APELLIDOS_CO");
  fields.put("titulo", "TITULO_CO");
  fields.put("nacionalidad", "NACIONALIDAD_CO");
  fields.put("estudio", "GRADOU_CO");
  fields.put("instituto", "INSTITUTO_CO");
}
%>
<%
ArrayList params = new ArrayList();
String nombre = request.getParameter("nombre");
String nacionalidad = request.getParameter("nacionalidad");
String titulo = request.getParameter("titulo");
String estudio = request.getParameter("estudio");
String instituto = request.getParameter("instituto");
boolean hasNombre = StringUtils.isNotEmpty(nombre);
boolean hasNacionalidad = StringUtils.isNotEmpty(nacionalidad);
boolean hasTitulo = StringUtils.isNotEmpty(titulo);
boolean hasEstudio = StringUtils.isNotEmpty(estudio);
boolean hasInstituto = StringUtils.isNotEmpty(instituto);
if (hasNombre) params.add("%" + StringUtils.deleteWhitespace(nombre) + "%");
if (hasNacionalidad) params.add(nacionalidad);
if (hasTitulo) params.add(titulo);
if (hasEstudio) params.add(estudio);
if (hasInstituto) params.add(instituto);
JSONObject obj = (JSONObject)json;
int position = obj.optInt("position", 0);
int length = obj.optInt("length", defaultLength);
String field = obj.optString("field", "nombre");
boolean reversed = obj.optBoolean("reversed", false);
if (!fields.containsKey(field)) field = "nombre";


List list = (List)sibm.query(limit("SELECT DISTINCT CODIGO_CO AS id, NOMBRE_CO AS nombre, APELLIDOS_CO AS apellido, TITULO_CO AS titulo, NACIONALIDAD_CO AS nacionalidad, GRADOU_CO AS estudio, INSTITUTO_CO AS instituto FROM CDIRCOLECTOR WHERE (VINCULO_CO = '1' OR VINCULO_CO = '2' OR VINCULO_CO = '3' OR VINCULO_CO = '8')" + (hasNombre? " AND (UPPER(NOMBRE_CO || APELLIDOS_CO) LIKE UPPER(?))": "") + (hasNacionalidad? " AND NACIONALIDAD_CO = ?": "") + (hasTitulo? " AND TITULO_CO = ?": "") + (hasEstudio? " AND GRADOU_CO = ?": "") + (hasInstituto? " AND INSTITUTO_CO = ?": "") + " ORDER BY " + fields.get(field) + " " + (reversed? "DESC": "ASC"), position + 1, length + 1), params.toArray(), all, false);
boolean hasNext;
if (list.size() == length + 1) {
  hasNext = true;
  list.remove(length - 1);
}
else hasNext = false;
%>
{
"position": <%= position %>,
"items": <%= new JSONArray(list) %>,
"hasNext": <%= hasNext %>
}