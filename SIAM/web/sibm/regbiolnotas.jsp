<html>
<head>
<link href="../plantillaSitio/css/siamccs.css" rel="stylesheet" type="text/css" />
</head>
<body>


<jsp:directive.page import="co.org.invemar.siam.sibm.vo.Notas"/>
<%
  Notas notas=(Notas)request.getAttribute("NOTAS");

%>

<jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=30"/>

<table align="center" border="0" width="750">
  <tbody>

  <tr>
    <td bgcolor="#B5D7DE" class="txttaxon1">*Phylum:</td>
    <td width="575" bgcolor="#e6e6e6" class="txttaxon3"><%=request.getParameter("phylum")%> </td>
  </tr>
  <tr>
    <td bgcolor="#B5D7DE" class="txttaxon1">*Tax&oacute;n:</td>
    <td bgcolor="#e6e6e6"><span class="txttaxon2"><%=request.getParameter("nombre")%></span>&nbsp;&nbsp;<span class="txttaxon1"><%=request.getParameter("autor")%></span></td>
  </tr>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">*N&uacute;mero catalogo:</td>
    <td bgcolor="#e6e6e6" class="texttablas"><br>
</td>
  </tr>
  <tr>
    <td width="165" bgcolor="#B5D7DE" class="txttaxon1">*N&uacute;meroInvemar:</td>
    <td bgcolor="#e6e6e6" class="texttablas"><%=request.getParameter("numero") %><br>
</td>
  </tr>
  <tr>
    <td colspan="2"></td>
    </tr>
  <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
  <!-- FIN DEL BLOQUE QUE GENERA LA FILOGENIA -->
</table>
<table align="center" border="0" width="750">
  <tbody>
    <tr>
      <td colspan="3"><img src="../images/spacer.gif" width="1" height="1"></td>
    </tr>
    <!-- INICIO DEL BLOQUE QUE GENERA LA FILOGENIA -->
    <tr bgcolor="#B5D7DE">
      <td colspan="3">&nbsp; <img src="../images/arrowx.gif" width="5" height="7">&nbsp; <span class="linksnegros">Notas</span></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td width="165" valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="texttablas">Captura:</td>
          </tr>
        </table>      </td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
          <tr>
            <td class="txttaxon4"><%=notas.getNotasCap() %></td>
          </tr>
        </table>      </td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Identificaci&oacute;n:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=notas.getNotasId() %></td>
        </tr>
      </table></td>
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Muestra:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=notas.getNotasMuestra() %></td>
        </tr>
      </table></td>
      
    </tr>
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Estaci&oacute;n:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=notas.getNotasEs() %></td>
        </tr>
      </table></td>
      
    </tr>
    
    <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Georeferenciaci&oacute;n:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=notas.getNotasCon() %></td>
        </tr>
      </table></td>
      
    </tr>
   <tr bgcolor="#ffffff">
      <td valign="top" bgcolor="#e6e6e6"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="texttablas">Oceanogr&aacute;ficas:</td>
        </tr>
      </table></td>
      <td colspan="2" valign="top" class="texttablas"><table border="0" cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td class="txttaxon4"><%=notas.getNotasCr() %></td>
        </tr>
      </table></td>
      
    </tr>
</table>

<%@ include file="../plantillaSitio/footermodulesV3.jsp" %>
</body>
</html>