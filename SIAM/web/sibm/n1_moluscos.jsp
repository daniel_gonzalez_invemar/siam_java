<%@ include file="sibm_cabezote.jsp" %>
<style type="text/css">
.tip { 
  font: italic 17px Georgia, serif; 
  padding: 5px; 
  display: block; 
  background: #0F6788; 
  color: #fff; 
}
</style>
<script type="text/javascript" src="../js/tooltip.js"></script>

<script type="text/javascript">
function initTooltip() {
	
	new Effect.Tooltip('Molusco2', 'Quitones', {className: 'tip'});
	new Effect.Tooltip('Molusco3', 'Colmillos de mar', {className: 'tip'});
	new Effect.Tooltip('Molusco4', 'Ostras y mejillones', {className: 'tip'});
	new Effect.Tooltip('Molusco5', 'Caracoles', {className: 'tip'});
	new Effect.Tooltip('Molusco6', 'Pulpos y calamares', {className: 'tip'});

}
Event.observe(window, 'load', initTooltip, false);

</script>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#096A95">
    <td colspan="2" class="titulo_ubicacion"><img src="../images/spacer.gif" width="5" height="5"><a href="../index.htm" class="titulo_ubicacion">Inicio</a> &gt; <a href="../siampc.htm" class="titulo_ubicacion">SIAMP</a> &gt; <a href="index.htm" class="titulo_ubicacion">Sistema
      de Informaci&oacute;n sobre Biodiversidad Marina</a> &gt; Moluscos</td>
  </tr>
  <tr>
    <td colspan="2"><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#8DC0E3">
    <td width="21"><img src="../images/arrow2.gif" width="20" height="20"></td>
    <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td width="349" align="left" class="linksnegros">Moluscos</td>
          <td width="380" align="right"><table border="0" cellpadding="0" cellspacing="0" width="320">
              <tr>
                <td width="17"><a href="index.htm"><img src="../images/orange_home.gif" width="17" height="17" border="0"></a></td>
                <td width="4">&nbsp;</td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="78"><a href="index.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Inicio
                    SIBM</a></td>
                <td width="19"><a href="legal.htm"><img src="../images/orange_legal.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="45"><a href="legal.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Legal</a></td>
                 <td width="19"><a href="index_guia_sibm.htm"><img src="../images/help.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="45"><a href="index_guia_sibm.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Ayuda</a></td>
                <td width="22"><a href="contactenos.htm"><img src="../images/orange_contact.gif" width="17" height="17" border="0"></a></td>
                <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="75"><div align="left"><a href="contactenos.htm" class="linksnegros"><img src="../images/spacer.gif" width="4" height="4" border="0">Contactenos</a></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="../images/dotted_line.gif"><img src="../images/dotted_line.gif" width="12" height="3"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<TABLE cellSpacing=0 cols=4 cellPadding=0 align=center border=1>
  <TBODY>
  <TR>
    <TD
      href="fe_lista.jsp?clase=POLYPLACOPHORA"
      target="_popup"><img src="images/Molusco2a.gif" id="Molusco2"  width="200" height="200"
      border=0></TD>
    <TD
      href="fe_lista.jsp?clase=SCAPHOPODA"
      target="_popup"><img src="images/Molusco3.gif" id="Molusco3"  width="200" height="200"
      border=0></TD>
    <TD
      href="fe_lista.jsp?clase=BIVALVIA"
      target="_popup"><img src="images/Molusco4.gif" id="Molusco4"  width="200" height="200"
      border=0></TD>
  </TR>
  <TR>
    <TD
      href="fe_lista.jsp?clase=GASTROPODA"
      target="_popup"><img src="images/Molusco5.gif" id="Molusco5"  width="200" height="200"
      border=0></TD>
    <TD
      href="fe_lista.jsp?clase=CEPHALOPODA"
      target="_popup"><img src="images/Molusco6.gif" id="Molusco6"  width="200" height="200"
      border=0></TD>
  </TR>
  </TBODY></TABLE>


<%@ include file="sibm_fondo.jsp"%>



