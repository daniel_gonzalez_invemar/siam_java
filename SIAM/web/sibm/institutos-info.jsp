<%--
Retorna en formato JSON el listado de nacionalidades disponibles que se mostraran en el formulario para filtrar la busqueda del usuario.
--%>
<%@ include file="../common.jsp" %>
{
  "nacionalidades": <%= sibm.query("SELECT DISTINCT PAIS_IN, NOMBRE_PA FROM CDIRINSTITUTO, CPAISES WHERE PAIS_IN = CODIGO_PA ORDER BY NOMBRE_PA", assoc) %>
}