<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%
	String user=(String)session.getAttribute("USER");
	
	if(user==null){
		response.sendRedirect("index.jsp");
	}
 %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>.::Sistema de Informaci&oacute;n Ambiental Marina de Colombia - SIAM::.</title>
<meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC, SIPEIN,pequeria">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
 <link type='text/css' rel="stylesheet" href="../../plantillaSitio/css/misiamccs.css"/>
</head>

<body >
<jsp:include page="../../plantillaSitio/headermodulosEstadisticas.jsp?idsitio=30"/>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../../images/spacer.gif" width="5" height="5"></td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0"> 
  <tr>
    <td></td>
  </tr>
  <tr style="background-color:#8DC0E3">
   <td>Productos de informaci&oacute;n Estadisticas</td>
  </tr>
</table>

<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
  <tr>
    <td ></td>
  </tr>
  <tr>
    <td></td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="10" style="background-color:#B5D7DE" >&nbsp;</td>
    <td width="733"  style="background-color:#B5D7DE" class="texttablas">:Estadisticas:</td>
    <td width="10"  style="background-color:#B5D7DE"  >&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td bgcolor="#E6E6E6">&nbsp;</td>
    <td bgcolor="#E6E6E6">
	
	<table border="0" cellpadding="0" cellspacing="0" width="730">
      <tr>
        <td width="16"><a href="admincoleccion.jsp" target="_blank" class="texttablas"><img src="../../images/grey_arrow.jpg" width="16" height="16" border="0"></a></td>
        <td width="714"><a href="admincoleccion.jsp" target="_blank" class="texttablas">Administraci&oacute;n coleccioes biol&oacute;gicas</a></td>
      </tr>
    </table>
	</td>
    <td bgcolor="#E6E6E6">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#E6E6E6">&nbsp;</td>
    <td bgcolor="#E6E6E6"><table border="0" cellpadding="0" cellspacing="0" width="730">
      <tr>
        <td width="16"><a href="infodyc.jsp" target="_blank" class="texttablas"><img src="../../images/grey_arrow.jpg" width="16" height="16" border="0"></a></td>
        <td width="714"><a href="infodyc.jsp" target="_blank" class="texttablas">Informaci&oacute;n diccionarios de especies, conjunto de referencia y catalogo de especie</a></td>
      </tr>
    </table></td>
    <td bgcolor="#E6E6E6">&nbsp;</td>
  </tr>
  
  <tr>
    <td bgcolor="#E6E6E6">&nbsp;</td>
    <td bgcolor="#E6E6E6"><table border="0" cellpadding="0" cellspacing="0" width="730">
      <tr>
        <td width="16"><a href="infogruposrb.jsp" target="_blank" class="texttablas"><img src="../../images/grey_arrow.jpg" width="16" height="16" border="0"></a></td>
        <td width="714"><a href="infogruposrb.jsp" target="_blank" class="texttablas">Informaci&oacute;n por grupos registros biol&oacute;gicos</a></td>
      </tr>
    </table></td>
    <td bgcolor="#E6E6E6">&nbsp;</td>
  </tr>
   
  
  
  
</table>

<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
  <tr>
    <td ></td>
  </tr>
  <tr>
    <td></td>
  </tr>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
   <td bgcolor="#8DC0E3"></td>
  </tr>


</table>
 <%@ include file="../../plantillaSitio/footermodulesEstadisticas.jsp" %>
</body>
</html>


