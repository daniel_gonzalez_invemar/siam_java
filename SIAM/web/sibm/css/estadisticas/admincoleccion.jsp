<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%
	String user=(String)session.getAttribute("USER");
	
	if(user==null){
		session.removeAttribute("USER");
		session.invalidate();
		response.sendRedirect("index.jsp");
	}
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
  <head>
<title>Estadisticas del Museo de Historia Natural Marina de Colombia</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<style type="text/css">
    a:link {
        text-decoration: none;
    }
    a:visited {
        text-decoration: none;
    }
    a:hover {
        text-decoration: underline;
   }
   
   .classTHeader{
        color: #006666; 
        padding: 5px 20px;
   }
   table {
   	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
   
   }
  table thead th{
      color: #f7f7f7;
      background-color: #295d94; 
      padding: 5px 30px;
  
  }
  
  table tfoot{
  
        background-color: #295d94; 
  }

</style>
<script type="text/javascript" src="../jscript/panel.js"></script>
<script type="text/javascript" src="js/stats.js"></script>
<script type="text/javascript">
<!--
//-->
function initColeccion(){
	doFillColeccion('colecciones');
}
if (window.addEventListener)
	window.addEventListener("load", initColeccion, false)
else if (window.attachEvent)
	window.attachEvent("onload", initColeccion)
</script>
</head>
  
  <body style=" background-color: #d7edfb;" >

	<%@ include file="header.htm" %>
	
	<div id="main" style="margin-left: 100px; ">
    <h1>.:: Administraci&oacute;n Colecciones Biol&oacute;gicas ::.</h1>
    <h2>Estadist&iacute;cas SIBM</h2>
    
    <p>Seleccione la colecci&oacute;n: 
    <select id="colecciones">
    	<option value="--------------" selected="selected" >----------------</option>	
    </select></p>
    
    <b style="color: #006666;">1.</b>
    <a id="fr" href="javascript:doFillFechaRecibido('contenedor1');" style="color: #006666; font-weight: bold;" >
    N&uacute;mero de lotes por secci&oacute;n - fecha recibido
    </a>
    <span id="loadingFR" style="padding-left: 20px;"></span>

    <div id="contenedor1" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 350px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">
            Secci&oacute;n de muestra: 
            <select id="fechaRecibido" name="fechaRecibido" onchange="doEstadisticaFechaRecibido(this.value);">
                <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont1" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr style="background-color: #295d94;">
                    <th style="background-color: #295d94; color: #f7f7f7; padding: 5px 20px;" >A�o</th>
                    <th style="background-color: #295d94; color: #f7f7f7; padding: 5px 20px;">Numero Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad"></tbody> 
        </table>
        <div id="Cont1ShowResult"></div>
        <a href="javascript:mpanel('contenedor1');" style="color: #003063;" >[Ocultar Panel]</a>
    </div>
    <p></p>



    <b style="color: #006666;">2.</b> 
    <a href="javascript:doFillMuestraFI('contenedor2');" style="color: #006666; font-weight: bold;">
        Numero de lotes por secci&oacute;n - fecha identificacion
    </a>
    <span id="loadingFI" style="padding-left: 20px;"></span>
    <div id="contenedor2" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 350px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">
            Secci&oacute;n de muestra: 
            <select id="muestraFI" onchange="doFillFechaIdent(this.value);" style="margin-right: 15px;"> 
                    <option value="" selected="selected" >----------------</option>
            </select>
            Fecha Identificacion:
            <select id="fechaident"  onchange="doEstadFechaIdent(document.getElementById('muestraFI').value,this.value);"> 
                <option value="" selected="selected" >----------------</option>
            </select> 
        
            <span id="loadingCont2" style="padding-left: 30px; margin-left: 25px; border: 1px solid #d7edfb;"></span>
        </p>
 
        <div id="Cont2ShowResult"></div>
  
        <a href="javascript:mpanel('contenedor2');" style="color: #003063;" >
             [Ocultar Panel]
        </a>
        <a href="javascript:doViewAll(document.getElementById('muestraFI').value,'Cont2ShowResult', 'viewAllSFI','loadingCont2');" style="color: #003063;">
          [Ver todos los lotes de esta secci&oacute;n]
        </a>
    </div>
    <p></p>



    <b style="color: #006666;">3.</b> 
    <a href="javascript:doFillMuestraFC('contenedor3');" style="color: #006666; font-weight: bold;">
      Numero de lotes por secci&oacute;n - fecha de captura
    </a>
    <span id="loadingFC" style="padding-left: 20px;"></span>
    <div id="contenedor3" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 350px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">
            Secci&oacute;n de muestra: 
            <select id="muestraFC" onchange="doFillFechaCaptura(this.value);" style="margin-right: 15px;"> 
                    <option value="" selected="selected" >----------------</option>
            </select>
            Fecha captura:
            <select id="fechacaptura"  onchange="doEstadFechaCaptura(document.getElementById('muestraFC').value,this.value);"> 
                    <option value="" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont3" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
   
        <div id="Cont3ShowResult"></div>
  
        <a href="javascript:mpanel('contenedor3');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
        <a href="javascript:doViewAll(document.getElementById('muestraFC').value,'Cont3ShowResult', 'viewAllSFC','loadingCont3');" style="color: #003063; padding-left: 20px;">
            [Ver todos los lotes de esta secci&oacute;n]
        </a>
    </div>


    <p></p>
    <b style="color: #006666;">4.</b> 
    <a href="javascript:doSeccionImages('contenedor4');" style="color: #006666; font-weight: bold;">
        N&uacute;mero de lotes por secci&oacute;n - con imagen y sin imagen
    </a>
    <span id="loadingEIMG" style="padding-left: 20px;"></span>
    <div id="contenedor4" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 350px; border: 1px solid #006666;" >
 
        <table id="estadistica4" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Secci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Con imagenes</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Sin imagenes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad4"></tbody>
        </table>
        <p></p>
        <a href="javascript:mpanel('contenedor4');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
    
    
   <b style="color: #006666;">5.</b>
    <a  href="javascript:doFillSeccionINV('contenedor5');" style="color: #006666; font-weight: bold;" >
        N&uacute;mero de lotes por secci&oacute;n, investigador y tarea
    </a>
    <span id="loadingEINV" style="padding-left: 20px;"></span>

    <div id="contenedor5" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 350px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">Secci&oacute;n de muestra: 
            <select id="MuestraINV" onchange="doEstadInvestigadores(this.value);">
                    <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont5" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  		<div id="linkDownload5" style="display:none"><a href="#">Descargar datos en formato Microsoft Excel</a></div>
        <table id="estadistica5" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px;" >Investigador</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Tarea</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">A�o</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad5"></tbody> 
        </table>
        
        <a href="javascript:mpanel('contenedor5');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
    
   <b style="color: #006666;">6.</b>
    <a id="frs" href="javascript:doFillInvestigadores('contenedor6');" style="color: #006666; font-weight: bold;" >
        N&uacute;mero de lotes por investigador - a&ntilde;o
    </a>
    <span id="loadingInvest" style="padding-left: 20px;"></span>

    <div id="contenedor6" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 350px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">Investigador: 
            <select id="invest" onchange="doEstadInvest(this.value);">
                    <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont6" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  		<div id="linkDownload6" style="display:none"><a href="#">Descargar datos en formato Microsoft Excel</a></div>
        <table id="estadistica6" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >a&ntilde;o</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Secci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Tarea</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad6"></tbody> 
        </table>
        <div id="Cont6ShowResult"></div>
        <a href="javascript:mpanel('contenedor6');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
    
    <b style="color: #006666;">7.</b> 
    <a href="javascript:doFillMuestraPROY('contenedor7');" style="color: #006666; font-weight: bold;">
        N&uacute;mero de lotes por secci&oacute;n aportado por proyectos
    </a>
    <span id="loadingPROY" style="padding-left: 20px;"></span>
    <div id="contenedor7" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 350px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">
            Secci&oacute;n de muestra: 
            <select id="seccionesPROY" onchange="doEstadSeccProyectos(this.value);"> 
                    <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont7" style="padding-left: 45px; margin-left: 60px;"></span>  
        </p>
  
        <table id="estadistica7" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Titulo del Proyecto</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Numero Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad7"></tbody>
       </table>
         <a href="javascript:mpanel('contenedor7');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>


    <p></p>
    <b style="color: #006666;">8.</b> 
    <a href="javascript:doFillMuestraPRES('contenedor8');" style="color: #006666; font-weight: bold;" >
        N&uacute;mero de lotes por secci&oacute;n - preservativo
    </a>
    <span id="loadingPRES" style="padding-left: 20px;"></span>
    <div id="contenedor8" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 350px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">
            Secci&oacute;n de muestra: 
            <select id="muestraPRES" onchange="doEstadPreservativos(this.value);"> 
                <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont8" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica8"  style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="padding: 5px 20px;">Preservativo </th>
                    <th style="padding: 5px 20px;">Numero Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad8"></tbody>
        </table>
        <div id="Cont8ShowResult"></div>
        <a href="javascript:mpanel('contenedor8');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
    
    
    <b style="color: #006666;">9.</b>
    <a href="javascript:doFillMuestraObjRelac('contenedor9');" style="color: #006666; font-weight: bold;" >
        N&uacute;mero de lotes por secci&oacute;n - Objeto relacionado
    </a>
    <span id="loadingOBJ" style="padding-left: 20px;"></span>
    <div id="contenedor9" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 350px; border: 1px solid #006666;" >
        <p style="color: #003063; font-weight: bold;">Secci&oacute;n de muestra: 
            <select id="muestraOBJR" onchange="doEstadObjRelacionado(this.value);"> 
                <option value="--------------" selected="selected" >----------------</option>
            </select> 
            <span id="loadingCont9" style="padding-left: 45px; margin-left: 25px;"></span>
        </p>
  
        <table id="estadistica9" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="padding: 5px 20px;">Objeto </th>
                    <th style="padding: 5px 20px;">Numero Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad9"></tbody>
        </table>
        <div id="Cont9ShowResult"></div>
        <a href="javascript:mpanel('contenedor9');" style="color: #003063;" >[Ocultar Panel]</a>
    </div>
    
    <p></p>
     <b style="color: #006666;">10.</b>
    <a href="javascript:doEstadMantenimiento('contenedor19');" style="color: #006666; font-weight: bold;" >
        Estadisticas mantenimiento de los lotes por secci�n por a�o
    </a>
    <span id="loadingManten" style="padding-left: 20px;"></span>

    <div id="contenedor19" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 200px; border: 1px solid #006666;" >
        
        <table id="estadistica19" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Secci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">A&ntilde;o</th>
                    <th style="margin: 6px 10px; padding:10px 15px; ">Tarea</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                    
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad19"></tbody> 
        </table>
        
         <p> <b>Resumen de datos de mantenimiento </b></p>
         
          <table id="estadistica191" style="margin-left: 50px; border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Secci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; ">Tarea</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Faltantes</th>
                    
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad191"></tbody> 
        </table>
         
        <a href="javascript:mpanel('contenedor19');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    <p></p>
   
     <b style="color: #006666;">11.</b> 
    <a href="javascript:doLotesOtrasEntidades('contenedor21');" style="color: #006666; font-weight: bold;">
        Lotes del Museo recibidos de otras entidades por seccion
    </a>
    <span id="loadingEstdOENT" style="padding-left: 20px;"></span>
    <div id="contenedor21" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 350px; border: 1px solid #006666;" >
        
        <table id="estadistica21" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Secci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Instituci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Modalidad</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad21"></tbody>
        </table>
        <p ></p>
        
        <a href="javascript:mpanel('contenedor21');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
     <p></p>
      <b style="color: #006666;">12.</b> 
    <a href="javascript:doLotesByTipos('contenedor22');" style="color: #006666; font-weight: bold;">
        N&uacute;mero de lotes por tipos 
    </a>
    <span id="loadingEstdLTIPOS" style="padding-left: 20px;"></span>
    <div id="contenedor22" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 350px; border: 1px solid #006666;" >
        
        <table id="estadistica22" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Secci&oacute;n</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Tipo</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Lotes</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad22"></tbody>
        </table>
        <p ></p>
        
        <a href="javascript:mpanel('contenedor22');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
     <p></p>
     
     <b style="color: #006666;">13.</b> 
    <a href="javascript:doEstadByPhylum('contenedor10');" style="color: #006666; font-weight: bold;">
        N&uacute;mero de lotes por phylum
    </a>
    <span id="loadingEstdPhylum" style="padding-left: 20px;"></span>
    <div id="contenedor10" style="background-color: #bfe1ff; padding: 8px 8px 8px 20px; display: none; margin-right: 350px; border: 1px solid #006666;" >
        
        <table id="estadistica10" style="border: 1px solid #d7edfb;">
            <caption></caption>
            <thead>
                <tr>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;" >Phylum</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Muestras</th>
                    <th style="margin: 6px 10px; padding:10px 15px; white-space: nowrap;">Porcentaje(%)</th>
                </tr>
            </thead>
            <tfoot></tfoot>
            <tbody id="tbodyEstad10"></tbody>
        </table>
        <p ></p>
        
        <a href="javascript:mpanel('contenedor10');" style="color: #003063;" >
            [Ocultar Panel]
        </a>
    </div>
    
     <p></p>
         
</div>


 <%@ include file="footer.htm" %>

	

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2300620-1");
pageTracker._trackPageview();
} catch(err) {}
</script>
    
  </body>
</html>
