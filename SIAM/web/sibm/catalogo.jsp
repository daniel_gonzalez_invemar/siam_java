<%@ page language="java" import="java.util.*,co.org.invemar.catalogo.model.*,java.util.regex.Pattern" %>
<%
String path = ".";//request.getContextPath();
/*String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";*/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
<meta name="keywords" content="invemar, ciencia marina, investigac&iacute;n, marino, costera, costero" />
<meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />

<script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="../1ibre/1ibre.js"></script>
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>

<script src="jscript/scripts.js" type="text/javascript"></script>
<script src="jscript/panel.js" type="text/javascript"></script>
<script type="text/javascript">
function stat(){
	var url="statService";
	var param="page=312";
	new Ajax.Request(url, 
					{method:"post", 
					parameters:param,
					//requestHeaders: {Accept: 'application/json'},
					onComplete:function(transport){
						
					}
	});
}


//Event.observe(window, 'load', stat, false);

</script>
 <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css"/>
</head>
<body>
<jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=30"/>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
    <td colspan="2"><img src="../images/spacer.gif" width="5" height="5" alt="" /></td>
  </tr>
  <tr bgcolor="#8DC0E3">
    <td width="21">&nbsp;</td>
    <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td align="left" class="linksnegros">Resultados de la consulta</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
  <tr>
    <td ></td>
  </tr>
  <tr>
    <td></td>
  </tr>
</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="10" >&nbsp;</td>
    <td width="733" bgcolor="#B5D7DE" class="texttablas" style="display:none"><a href="#" onClick="mpanel('filter');">Presione clic AQU&Iacute; para Seleccionar
los
campos a presentar </a></td>
    <td width="10" >&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td bgcolor="#E6E6E6">&nbsp;</td>
    <td bgcolor="#E6E6E6"><div id="filter" style="display: none;">
    <form method="get" action="filtercontroller" name="formFilter" style="margin: 0px">
    <table width="730" border="0" align="center" cellpadding="1" cellspacing="1" id="ffilter">
      <tbody>
        <tr>
          <td width="4%">
            <input name="ucambio" value="ucambio" type="checkbox" />
          </td>
          <td width="29%" class="texttablas">Ultimo Cambio</td>
          <td width="4%"><input name="coleccion" value="coleccion" type="checkbox" />
          </td>
          <td width="24%" class="texttablas">Coleccion</td>
          <td width="3%">
            <input name="ccoleccion" value="ccoleccion" type="checkbox" />
          </td>
          <td width="36%" class="texttablas">Codigo Coleccion</td>
        </tr>
        <tr>
          <td width="4%"><input name="ncatalogo" value="ncatalogo" type="checkbox" />
          </td>
          <td width="29%" class="texttablas">Numero Catalogo</td>
          <td width="4%">
            <input name="bregistro" value="bregistro" type="checkbox" />
          </td>
          <td width="24%" class="texttablas">Base del Registro</td>
          <td width="3%"><input name="reino" value="reino" type="checkbox" />
          </td>
          <td width="36%" class="texttablas">Reino</td>
        </tr>
        <tr>
          <td width="4%"><input name="division" value="division" type="checkbox" />
          </td>
          <td width="29%" class="texttablas">Divisi&oacute;n</td>
          <td width="4%"><input name="clase" value="clase" type="checkbox" />
          </td>
          <td width="24%" class="texttablas">Clase</td>
          <td><input name="epiteto" value="epiteto" type="checkbox" />
          </td>
          <td width="36%" class="texttablas">Epiteto</td>
        </tr>
        <tr>
          <td width="4%"><input name="aespecie" value="aespecie" type="checkbox" />
          </td>
          <td width="29%" class="texttablas">Autor especie</td>
          <td width="4%">
            <input name="identPor" value="identPor" type="checkbox" />
          </td>
          <td width="24%" class="texttablas">Identificado por</td>
          <td width="3%">
            <input name="anoIdent" value="anoIdent" type="checkbox" />
          </td>
          <td width="36%" class="texttablas">A&ntilde;o identificaci&oacute;n</td>
        </tr>
        <tr>
          <td width="4%">
            <input name="tipo" value="tipo" type="checkbox" />
          </td>
          <td width="29%" class="texttablas">Tipo</td>
          <td width="4%">
            <input name="colectadoPor" value="colectadoPor" type="checkbox" />
          </td>
          <td width="24%" class="texttablas">Colectado por</td>
          <td width="3%">
            <input name="anocaptura" value="anocaptura" type="checkbox" />
          </td>
          <td width="36%" class="texttablas">A&ntilde;o captura</td>
        </tr>
        <tr>
          <td width="4%"><input name="nombreagua" value="nombreagua" type="checkbox" />
          </td>
          <td width="29%" class="texttablas">Cuerpo de agua</td>
          <td width="4%">
            <input name="pais" value="pais" type="checkbox" />
          </td>
          <td width="24%" class="texttablas">Pais</td>
          <td width="3%"><input name="depto" value="depto" type="checkbox" />
          </td>
          <td width="36%" class="texttablas">Departamento</td>
        </tr>
        <tr>
          <td width="4%"><input name="localidad" value="localidad" type="checkbox" />
          </td>
          <td width="29%" class="texttablas">Localidad</td>
          <td width="4%"><input name="longitud" value="longitud" type="checkbox" />
          </td>
          <td width="24%" class="texttablas">Longitud</td>
          <td width="3%">
            <input name="latitud" value="latitud" type="checkbox" />
          </td>
          <td width="36%" class="texttablas">Latitud</td>
        </tr>
        <tr>
          <td width="4%"><input name="profcaptura" value="profcaptura" type="checkbox" />
          </td>
          <td width="29%" class="texttablas">Profundidad de Captura</td>
          <td width="4%">
            <input name="ejemplares" value="ejemplares" type="checkbox" />
          </td>
          <td width="24%" class="texttablas">Ejemplares</td>
          <td width="4%"> </td>
        </tr>
        <tr>
          <td colspan="3" class="texttablas"><a href="javascript:seleccionar_todo();">Marcar
              todos</a> | <a href="javascript:deseleccionar_todo();">Marcar ninguno</a> </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="2">
            <input name="submit" type="submit" class="inputText2" style="border: 1px solid rgb(0, 51, 0); font-size: 12px; font-family: Tahoma,Verdana,Arial;" value="Aplicar filtro" />

          </td>
        </tr>
      </tfoot>
    </table>
    </form></td>
    <td bgcolor="#E6E6E6">&nbsp;</td>
  </tr>
</table>

<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td width="10" bgcolor="#B5D7DE">&nbsp;</td>
    <td width="733" bgcolor="#B5D7DE" class="texttablas">
  Si desea descargar los datos seleccione uno de los siguientes tipos:
  <img src="../images/file.gif.jpg" alt="" /><a href="downloadfile?ext=txt" class="links">.TXT</a>
  o
  <img src="../images/excel.png"  alt="" /><a href="downloadfile?ext=xls" class="links">.XLS</a>  </td>
    <td width="10" bgcolor="#B5D7DE">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"></td>
  </tr>
</table>
   <%
                    float i = 0;
                    int tam = 0;
                    int paginas = 1;
                                 //pagina actual del resultado 1= valor inicial
                    int pag = 1;
                                 //numero de registros por pagina
                    float registros = 30;
                    String pagina = request.getParameter("pagina");

                     if(pagina == null){
                         pag=1;

                    }else  if(pagina != null){

                         pag = Integer.parseInt(pagina);

                    }

    %>
    <table id="catalogo" class="sortable" border="0" cellpadding="2" cellspacing="1" width="100%">
			<thead>
<tr bgcolor="#B5D7DE">
        <%  
            
            String ocambio=(String)session.getAttribute("OCAMBIO");
            String busqueda=(String)request.getAttribute("INICIO");
            String ocoleccion=(String)session.getAttribute("OCOLECCION");
            String occoleccion=(String)session.getAttribute("OCCOLECCION");
            String onroCatalogo=(String)session.getAttribute("ONROCATALOGO");
            String obaseRegistro=(String)session.getAttribute("OBASEREGISTRO");
            String oreino=(String)session.getAttribute("OREINO");
            String odivision=(String)session.getAttribute("ODIVISION");
            String oclase=(String)session.getAttribute("OCLASE");
            String oepiteto=(String)session.getAttribute("OEPITETO");
            String oaspecie=(String)session.getAttribute("OAESPECIE");
            String oidentpor=(String)session.getAttribute("OIDENPOR");
            String oaident=(String)session.getAttribute("OAIDENT");
            String otipo=(String)session.getAttribute("OTIPO");
            String ocolpor=(String)session.getAttribute("OCOLPOR");
            String oacaptura=(String)session.getAttribute("OACAPTURA");
            String ocagua=(String)session.getAttribute("OCAGUA");
            String opais=(String)session.getAttribute("OPAIS");
            String odepto=(String)session.getAttribute("ODEPTO");
            String olocalidad=(String)session.getAttribute("OLOCALIDAD");
            String olongitud=(String)session.getAttribute("OLONGITUD");
            String olatitud=(String)session.getAttribute("OLATITUD");
            String opcaptura=(String)session.getAttribute("OPCAPTURA");
            String oejemplares=(String)session.getAttribute("OEJEMPLARES");

            if(busqueda!=null && busqueda.equals("busqueda")){%>
                <th style="white-space: nowrap;" class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=ucambio&pagina=<%=pag%>">Ultimo  Cambio</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=coleccion&amp;pagina=<%=pag%>">Colecci&oacute;n</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=codColeccion&amp;pagina=<%=pag%>">Codigo Coleccion</a> </th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=nroCatalogo&amp;pagina=<%=pag%>">Numero Catalogo</a></th>
                <th class="titulostablas">Proyecto</th>
                <th class="titulostablas">Estaci&oacute;n</th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=especie&amp;pagina=<%=pag%>">Especie </a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=bRegistro&amp;pagina=<%=pag%>">Base del Registro</a> </th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=reino&amp;pagina=<%=pag%>">Reino</a> </th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=division&amp;pagina=<%=pag%>">Divisi&oacute;n</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=clase&amp;pagina=<%=pag%>">Clase</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=orden&amp;pagina=<%=pag%>">Orden</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=familia&amp;pagina=<%=pag%>">Familia</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=genero&amp;pagina=<%=pag%>">Genero</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=epiteto&amp;pagina=<%=pag%>">Epiteto</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=autorSpecie&amp;pagina=<%=pag%>">Autor Especie</a></th>
                <th class="titulostablas">Sinonimias</th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=identPor&amp;pagina=<%=pag%>">Identificado Por</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=anoIdent&amp;pagina=<%=pag%>">A&ntilde;o Identificacion</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=tipo&amp;pagina=<%=pag%>">Tipo</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=colectadoPor&amp;pagina=<%=pag%>">Colectado Por</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=anoCaptura&amp;pagina=<%=pag%>">A&ntilde;o Captura</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=cAgua&amp;pagina=<%=pag%>">Cuerpo de Agua</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=pais&amp;pagina=<%=pag%>">Pais  </a>  </th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=dpto&amp;pagina=<%=pag%>">Departamento</a></th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=localidad&amp;pagina=<%=pag%>">Localidad  </a> </th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=longitud&amp;pagina=<%=pag%>">Longitud </a>   </th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=latitud&amp;pagina=<%=pag%>">Latitud </a>   </th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=profCaptura&amp;pagina=<%=pag%>">Prof de Captura</a>    </th>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=ejemplares&amp;pagina=<%=pag%>">Ejemplares</a></th>
                <th class="titulostablas">Imagenes</th>
                <th class="titulostablas">Sistematica</th>
                <th class="titulostablas">Autor&iacute;a</th>
      <%}else{%>
            <%

                if (null==ocambio || ocambio.equals("TRUE") ){
            %>
                    <th nowrap="nowrap" class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=ucambio&amp;pagina=<%=pag%>">Ultimo Cambio</a></th>
            <%  }
                if (ocoleccion==null || ocoleccion.equals("TRUE") ){
            %>
                <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=coleccion&amp;pagina=<%=pag%>">Colecci&oacute;n</a></th>
            <%  }
                if (occoleccion==null || occoleccion.equals("TRUE") ){
            %>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=codColeccion&amp;pagina=<%=pag%>">Codigo Coleccion</a> </th>
            <%  }
                if (onroCatalogo==null || onroCatalogo.equals("TRUE") ){
            %>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=nroCatalogo&amp;pagina=<%=pag%>">Numero Catalogo</a></th>
            <%   }%>
            <th class="titulostablas">Proyecto</th>
            <th class="titulostablas">Estaci&oacute;n</th>
            <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=especie&amp;pagina=<%=pag%>">Especie </a></th>
            <%
                if (obaseRegistro==null || obaseRegistro.equals("TRUE") ){
            %>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=bRegistro&amp;pagina=<%=pag%>">Base del Registro</a> </th>
            <%  }
                if (oreino==null || oreino.equals("TRUE") ){%>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=reino&amp;pagina=<%=pag%>">Reino</a> </th>
            <%  }
                if (odivision==null || odivision.equals("TRUE") ){%>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=division&amp;pagina=<%=pag%>">Divisi&oacute;n</a></th>
            <%  }
                if (oclase==null || oclase.equals("TRUE") ){%>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=clase&amp;pagina=<%=pag%>">Clase</a></th>
            <%  }%>
                      <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=orden&amp;pagina=<%=pag%>">Orden</a></th>

                      <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=familia&amp;pagina=<%=pag%>">Familia</a></th>

                      <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=genero&amp;pagina=<%=pag%>">Genero</a></th>
            <%  if (oepiteto==null || oepiteto.equals("TRUE") ){%>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=epiteto&amp;pagina=<%=pag%>">Epiteto</a></th>
            <%  }
                if (oaspecie==null || oaspecie.equals("TRUE") ){%>
                      <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=autorSpecie&amp;pagina=<%=pag%>">Autor Especie</a></th>
            <%  }%>
                <th class="titulostablas">Sinonimias</th>
            <% if (oidentpor==null || oidentpor.equals("TRUE") ){%>
                      <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=identPor&amp;pagina=<%=pag%>">Identificado Por</a></th>
            <%  }
                if (oaident==null || oaident.equals("TRUE") ){%>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=anoIdent&amp;pagina=<%=pag%>">A&ntilde;o Identificacion</a></th>
            <%  }
                if (otipo==null || otipo.equals("TRUE") ){%>
                      <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=tipo&amp;pagina=<%=pag%>">Tipo</a></th>
            <%  }
                if (ocolpor==null || ocolpor.equals("TRUE") ){%>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=colectadoPor&amp;pagina=<%=pag%>">Colectado Por</a></th>
            <%  }
                if (oacaptura==null || oacaptura.equals("TRUE") ){%>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=anoCaptura&amp;pagina=<%=pag%>">A&ntilde;o Captura</a></th>
            <%  }
                if (ocagua==null || ocagua.equals("TRUE") ){%>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=cAgua&amp;pagina=<%=pag%>">Cuerpo de Agua</a></th>
            <%  }
                if (opais==null || opais.equals("TRUE") ){%>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=pais&amp;pagina=<%=pag%>">Pais  </a>  </th>
            <%  }
                if (odepto==null || odepto.equals("TRUE") ){%>
                      <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=dpto&amp;pagina=<%=pag%>">Departamento</a></th>
            <%  }
                if (olocalidad==null || olocalidad.equals("TRUE") ){%>
                      <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=localidad&amp;pagina=<%=pag%>">Localidad  </a> </th>
            <%  }
                if (olongitud==null || olongitud.equals("TRUE") ){%>
                      <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=longitud&amp;pagina=<%=pag%>">Longitud </a>   </th>
            <%  }
                if (olatitud==null || olatitud.equals("TRUE") ){%>
                      <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=latitud&amp;pagina=<%=pag%>">Latitud </a>   </th>
            <%  }
                if (opcaptura==null || opcaptura.equals("TRUE") ){%>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=profCaptura&amp;pagina=<%=pag%>">Prof de Captura</a>    </th>
            <%  }
                if (oejemplares==null || oejemplares.equals("TRUE") ){%>
                    <th class="titulostablas"><a class="titulostablas" href="<%=path%>/sortcontroller?sortColumn=ejemplares&amp;pagina=<%=pag%>">Ejemplares</a>    </th>
            <%  }%>
            <th class="titulostablas">Imagenes</th>
            <th class="titulostablas">Sistematica</th>
            <th class="titulostablas">Autor&iacute;a</th>
        <% }%>

				</tr>

			</thead>
			<tbody>
                    <%

                        List resultados=(List)session.getAttribute("LISTOBJECT");

                        paginas =  (int)Math.ceil((resultados.size()/ registros));

                        if(paginas == 0)
                            paginas = 1;

                        int ii/*,j=1*/;
                        //String row="class=\'odd\'";

                        for(i = (pag -1) * registros; (i < pag * registros) && i <  resultados.size(); i++){
                                Float v=new Float(i);
                                ii=v.intValue();
                                Catalogo catalogo=(Catalogo)resultados.get(ii);
                                  /*if((j%2)==0){
                                    row="";
                                }else {
                                    row="class=\'odd\'";
                                }
                                j++;*/

                    %>
<tr bgcolor="#E6E6E6">
                               
                                <%
                                  if (null==ocambio || ocambio.equals("TRUE") ){%>
                                        <td style="white-space: nowrap;" class="texttablas"> <%=catalogo.getUltimoCambio()%> </td>
                                <%}
                                  if (ocoleccion==null || ocoleccion.equals("TRUE") ){%>
                                        <td style="white-space: nowrap;" class="texttablas"><%=catalogo.getColeccion()%></td>
                                <%}
                                  if (occoleccion==null || occoleccion.equals("TRUE") ){%>
                                        <td class="texttablas"><%=catalogo.getCodColeccion()%></td>
                                <%}
                                  if (onroCatalogo==null || onroCatalogo.equals("TRUE") ){%>
                                        <td style="white-space: nowrap;" class="texttablas"><%=catalogo.getNumCatalogo()%></td>
                                <%}%>
								  <td style="white-space: nowrap;" class="texttablas"><a href="http://cinto.invemar.org.co/metadatos/showMetadato.jsp?atributoPadreTodo=completo&amp;conjunto=<%=catalogo.getMetadato()%>" target="_blank" ><%=catalogo.getProyecto()%></a></td>  
								  <td style="white-space: nowrap;" class="texttablas"><%=catalogo.getEstacion()%></td>                       
  <td style="white-space: nowrap;" class="texttablas"><%=catalogo.getEspecie()%></td>
                                <%
                                  if (obaseRegistro==null || obaseRegistro.equals("TRUE") ){%>
                                        <td class="texttablas">
										
                                       <%=catalogo.getBaseRegistro() %>
                                        
                                        
                                        
                                        
                                        </td>
                                <%}
                                  if (oreino==null || oreino.equals("TRUE") ){%>
                                        <td class="texttablas"><%=catalogo.getReino()%></td>
                                <%}
                                  if (odivision==null || odivision.equals("TRUE") ){%>
                                        <td class="texttablas"><%=catalogo.getDivision() %></td>
                                <%}
                                  if (oclase==null || oclase.equals("TRUE") ){%>
                                        <td class="texttablas"><%=catalogo.getClase()%></td>
                                <%}%>
                                <td class="texttablas"><%=catalogo.getOrden()%></td>

  <td class="texttablas"><%=catalogo.getFamilia()%></td>

  <td class="texttablas"><%=catalogo.getGenero()%></td>
    <%
      if (oepiteto==null || oepiteto.equals("TRUE") ){%>
            <td class="texttablas"><%=catalogo.getEpiteto()%></td>
    <%}
      if (oaspecie==null || oaspecie.equals("TRUE") ){%>
            <td style="white-space: nowrap;" class="texttablas"><%=catalogo.getAutorEspecie()%></td>
    <%}%>
    <td style="white-space: nowrap;" class="texttablas"><a href="javascript:(function(){requestPopup('sinonimos.jsp?clave=<%=catalogo.getClave()%>&amp;specie=<%=catalogo.getEspecie()%>').showPopup();})()">Ver Sinonimias</a></td>
    <%  if (oidentpor==null || oidentpor.equals("TRUE") ){%>
    		<%if(catalogo.getBaseRegistro().equalsIgnoreCase("O")|| catalogo.getBaseRegistro().equalsIgnoreCase("D")){

				Pattern p = Pattern.compile(",");
	        	String[] autores = p.split(catalogo.getIdentificadoPor());
		%>
        	<td style="white-space: nowrap;" class="texttablas">
        <%
			 for(String s : autores) {
			 	if(s.startsWith(" ")){
			 	 s=s.substring(1);
			 	}
		%>
        		 	
                 <a href="investigadorPV1.jsp?nombre=<%=s%>" target="_blank" ><%=s%>,</a>   
        <%            
			 }
			
		%>
         
           </td>
         <%} else {%>
         <td style="white-space: nowrap;" class="texttablas"><%=catalogo.getIdentificadoPor()%></td>
    <%}}
      if (oaident==null || oaident.equals("TRUE") ){%>
            <td class="texttablas"><%=catalogo.getAnoIdentificacion()%></td>
    <%}
      if (otipo==null || otipo.equals("TRUE") ){%>
            <td class="texttablas"><%=catalogo.getTipo()%></td>
    <%}
      if (ocolpor==null || ocolpor.equals("TRUE") ){%>
      <%if(catalogo.getBaseRegistro().equalsIgnoreCase("O")|| catalogo.getBaseRegistro().equalsIgnoreCase("D")){

				Pattern p = Pattern.compile(",");
	        	String[] autores = p.split(catalogo.getColectadoPor());
		%>
        	<td style="white-space: nowrap;" class="texttablas">
        <%
			 for(String s : autores) {
			 	if(s.startsWith(" ")){
			 	 s=s.substring(1);
			 	}
		%>
        		 	
                 <a href="investigadorPV1.jsp?nombre=<%=s%>" target="_blank" ><%=s%>,</a>   
        <%            
			 }
			
		%>
         
           </td>
         <%} else {%>
            <td style="white-space: nowrap;" class="texttablas"><%=catalogo.getColectadoPor()%></td>
    <%}}
      if (oacaptura==null || oacaptura.equals("TRUE") ){%>
            <td class="texttablas"><%=catalogo.getAnoCaptura()%></td>
    <%}
      if (ocagua==null || ocagua.equals("TRUE") ){%>
            <td class="texttablas"><%=catalogo.getContinentOcean()%></td>
    <%}
      if (opais==null || opais.equals("TRUE") ){%>
            <td class="texttablas"><%=catalogo.getPais()%></td>
    <%}
      if (odepto==null || odepto.equals("TRUE") ){%>
            <td class="texttablas"><%=catalogo.getDepartamento()%></td>
    <%}
      if (olocalidad==null || olocalidad.equals("TRUE") ){%>
            <td style="white-space: nowrap;" class="texttablas"><%=catalogo.getLocalidad()%></td>
    <%}
      if (olongitud==null || olongitud.equals("TRUE") ){%>
            <td style="white-space: nowrap;" class="texttablas"><%=catalogo.getLongitud()%></td>
    <%}
      if (olatitud==null || olatitud.equals("TRUE") ){%>
            <td style="white-space: nowrap;" class="texttablas"><%=catalogo.getLatitud()%></td>
    <%}
      if (opcaptura==null || opcaptura.equals("TRUE") ){%>
            <td class="texttablas"><%=catalogo.getProfCaptura()%></td>
    <%}
      if (oejemplares==null || oejemplares.equals("TRUE") ){%>
            <td class="texttablas"><%= catalogo.getEjemplares()%></td>
    <%}%>
    <td style="white-space: nowrap;" class="texttablas"> <% if(!catalogo.getImg().getArchivoIm().equals("") && !catalogo.getImg().getClaveIm().equals("")&& !catalogo.getImg().getNroInvemarIm().equals("")){%>
    <a href="http://www.invemar.org.co/zsibfichaimagen.jsp?clave=<%=catalogo.getImg().getClaveIm()%>&amp;nombre=<%= catalogo.getEspecie()%>&amp;numero=<%=catalogo.getImg().getNroInvemarIm()%>" target="_blank">Ver Imagenes</a>
        <%}%>
    </td>
      <td style="white-space: nowrap;" class="texttablas"><a href="zsibfichafiloPV1.jsp?clave=<%=catalogo.getClave()%>&nombre=<%=catalogo.getEspecie()%>" target="_blank">INVEMAR</a> | <a href="http://www.itis.gov/servlet/SingleRpt/SingleRpt?search_topic=Scientific_Name&amp;search_kingdom=every&amp;search_span=exactly_for&amp;categories=All&amp;source=html&amp;search_credRating=All&amp;search_value=<%=catalogo.getGenero()%>" target="_blank">ITIS</a></td>
      <% if(!catalogo.getNroInvemar().equals("-")){%>
      <td style="white-space: nowrap;" class="texttablas"><a href="javascript:(function(){requestPopup('autorfichaMuseo.jsp?nroinvemar=<%=catalogo.getNroInvemar()%>').showPopup();})()">Ver Autor</a></td>
      <%}else{ %>
      		<td style="white-space: nowrap;" class="texttablas"></td>
      <%}%>
</tr>

                    <%
                          tam= tam+1;
                    }%>
			</tbody>
      <tfoot>
        <tr>
          <td colspan="28" align="left"><img src="../images/spacer.gif" width="1" height="1" alt="" /></td>
        </tr>
      </tfoot>
		</table>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><div align="center"><% if(pag > 1){
                %><a href="catalogo.jsp?pagina=<%=pag-1%>" class="linksnavinferior">&lt;&lt;</a> <a href="catalogo.jsp?pagina=<%=pag-1%>" class="linksnavinferior">Anterior</a><span class="linksnavinferior"> -</span><% } %>
    <span class="linksnavinferior">P&aacute;gina <%=pag%> de <%=paginas%></span><% if(pag < paginas){%><span class="linksnavinferior"> -</span> <a href="catalogo.jsp?pagina=<%=pag+1%>" class="linksnavinferior">Siguiente</a> <a href="catalogo.jsp?pagina=<%=pag+1%>" class="linksnavinferior">&gt;&gt;</a><% } %> </div></td>
  </tr>
  <tr>
    <td ><img src="../images/dotted_line.gif" width="12" height="3" alt="" /></td>
  </tr>
</table>

<%
/*
	String[] parseAutores(String autores){
			Pattern p = Pattern.compile(",");
	        String[] items = p.split(autores);
			return items;
	}
	*/
%>
<%@ include file="sibm_fondoPV1.jsp" %>
<%@ include file="../plantillaSitio/footermodulesV3.jsp" %>
</body>
</html>
