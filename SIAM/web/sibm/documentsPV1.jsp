
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">

    <head>

        <title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>

        <link href="../siamccs.css" rel="stylesheet" type="text/css" />

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

        <meta name="keywords" content="invemar, ciencia marina, investigacion, marino, costera, costero" />

        <meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC" />

        <meta http-equiv="pragma" content="no-cache" />

        <meta http-equiv="cache-control" content="no-cache" />

        <meta http-equiv="expires" content="0" />





        <script type="text/javascript" src="jscript/mootools-1.2-core.js"></script>

        <script type="text/javascript" src="jscript/documents.js"></script>
        <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css"/>
    </head>
    <body>
        <jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=30"/>
        <table width="963px" border="0" align="center" cellpadding="0" >
            <tr style="padding:20px; border-color:Blue; border-width:thin;">
                <td style="padding:5px;">
                    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr bgcolor="#8DC0E3">
                            <td width="21"></td>
                            <td width="729" bgcolor="#8DC0E3" class="linksnegros"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="371" align="left" class="linksnegros">Documentos dobre biodiversidad<br/></td>
                                        <td width="358" align="right">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td ></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">

                        <tr>
                            <td bgcolor="#E6E6E6">&nbsp;</td>
                            <td height="16" bgcolor="#E6E6E6">
                                <div align="left" class="linksnavinferior">
                                    <form method="get" action="fichaCatagoloService" id="formproys">
                                        <input type="hidden" name="action" value="Proyecto" />
                                        <table cellpadding="0" border="0">

                                            <tr><td><span class="linksnegros">Seleccione tem&aacute;tica</span></td>
                                                <td>

                                                    <select name="topics" id="topics" title="topicos">
                                                        <option>-------------</option> 
                                                    </select>

                                                </td>
                                            </tr></table>

                                    </form>

                                </div></td>
                            <td bgcolor="#E6E6E6">&nbsp;</td>
                        </tr>

                    </table>
                    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td ></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="10" bgcolor="#B5D7DE">&nbsp;</td>
                            <td width="733" bgcolor="#B5D7DE" class="texttablas">
                                &Uacute;ltimos documentos publicados
                            </td>
                            <td width="10" bgcolor="#B5D7DE">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td bgcolor="#E6E6E6">&nbsp;</td>
                            <td bgcolor="#E6E6E6" class="texttablas" id="ldoc">&nbsp;</td>
                            <td bgcolor="#E6E6E6">&nbsp;</td>
                        </tr>

                    </table> 
                    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td ></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="10" bgcolor="#B5D7DE">&nbsp;</td>
                            <td width="733" bgcolor="#B5D7DE" class="texttablas">Documentos 
                            </td>
                            <td width="10" bgcolor="#B5D7DE">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td bgcolor="#E6E6E6">&nbsp;</td>
                            <td bgcolor="#E6E6E6" class="texttablas" id="docs">&nbsp;</td>
                            <td bgcolor="#E6E6E6">&nbsp;</td>
                        </tr>

                    </table> 
                    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td ></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <%@ include file="../plantillaSitio/footermodulesV3.jsp" %>
    </body><h1></h1>
</html>

