_wf2MailPattern = /^("[^"]*"|[^\s\.]\S*[^\s\.])@[^\s\.]+(\.[^\s\.]+)*$/;
_wf2DatePattern = /^[0-9]{4,}-[0-9]{2}-[0-9]{2}$/;
_wf2MonthPattern = /^[0-9]{4,}-[0-9]{2}$/;
_wf2WeekPattern = /^[0-9]{4,}-W[0-9]{2}$/;
_wf2DatetimePattern = /^[0-9]{4,}-[0-9]{2}-[0-9]{2}( [0-9]{2}:[0-9]{2}(:[0-9]{2}([.][0-9]+)?)?)?$/;
_wf2TimePattern = /^[0-9]{2}:[0-9]{2}(:[0-9]{2}([.][0-9]+)?)?$/;
var wf2CSSStyles = {};
function wf2AddCSSStyle(node, pseudo) {
  if (wf2CSSStyles[pseudo])
    node.setAttribute("style", node.style.cssText + wf2CSSStyles[pseudo]);
}
function isInRepititionBlock(node) {
  while (node) {
    if (node.addRepetitionBlock) return true;
    node = Element.parentElement(node);
  }
  return false;
}
Event.observe(window, "load", function () {
  ibreInit();
  AutocompleterWhatwg = Class.create();
  AutocompleterWhatwg.prototype = Object.extend(new Autocompleter.Base(), {
    initialize: function (element, update, options) {
      this.position = 0;
      this.baseInitialize(element, update, options);
      this.pageSize = this.options.choices;
    },
    parseUpdatedChoices: function (values, labels) {
      var ret       = []; // Beginning matches
      var partial   = []; // Inside matches
      var entry     = this.getToken();
      var count     = 0;
      var currentPosition = 0;
      for (var i = 0; i < values.length &&
        ret.length <= this.options.choices ; i++) {
        var label = labels[i]? "</td><td><span class='informal' style='color: #777'>" + labels[i].replace(" ", "&nbsp;") + "</span>": "</td><td>";
			//alert('values['+i+'].escapeHTML(): '+values[i].escapeHTML());
		//alert('values['+i+'].escapeAllHTML(): '+values[i].escapeAllHTML());
        var elem = values[i];
        var foundPos = !entry? 0: this.options.ignoreCase ?
          elem.toLowerCase().indexOf(entry.toLowerCase()) :
          elem.indexOf(entry);
        var isLabel = foundPos == -1 && labels[i];
        if (isLabel)
          foundPos = !entry? 0: this.options.ignoreCase ?
            labels[i].toLowerCase().indexOf(entry.toLowerCase()) :
            labels[i].indexOf(entry);
        if (foundPos != -1) {
          if (currentPosition++ == this.options.choices + this.begin) break;
          if (currentPosition <= this.begin) continue;
          do {
            var str = isLabel? labels[i]: elem;
            if (foundPos == 0 && str.length != entry.length) {
              if (isLabel)
                ret.push("<tbody title='" + elem.escapeHTML() + "'><tr><td>" + elem.escapeHTML() + "</td><td><span class='informal' style='color: #777'><strong>" + labels[i].substr(0, entry.length).escapeHTML() + "</strong>" +
                  labels[i].substr(entry.length).escapeHTML() + "</span>" + "</td></tr></tbody>");
              else
                ret.push("<tbody title='" + elem.escapeHTML() + "'><tr><td><strong>" + elem.substr(0, entry.length).escapeHTML() + "</strong>" +
                  elem.substr(entry.length).escapeHTML() + label + "</td></tr></tbody>");
              break;
            } else if (entry.length >= this.options.partialChars &&
              this.options.partialSearch && foundPos != -1) {
              if (this.options.fullSearch || /\s/.test(str.substr(foundPos-1,1))) {
                if (isLabel)
                  partial.push("<tbody title='" + elem.escapeHTML() + "'><tr><td>" + elem.escapeHTML() + "</td><td><span class='informal' style='color: #777'>" + labels[i].substr(0, foundPos).escapeHTML() + "<strong>" +
                    labels[i].substr(foundPos, entry.length).escapeHTML() + "</strong>" + labels[i].substr(
                    foundPos + entry.length).escapeHTML() + "</span>" + "</td></tr></tbody>");
                else
                  partial.push("<tbody title='" + elem.escapeHTML() + "'><tr><td>" + elem.substr(0, foundPos).escapeHTML() + "<strong>" +
                    elem.substr(foundPos, entry.length).escapeHTML() + "</strong>" + elem.substr(
                    foundPos + entry.length).escapeHTML() + label + "</td></tr></tbody>");
                break;
              }
            }

            foundPos = !entry? foundPos + 1: this.options.ignoreCase ?
              elem.toLowerCase().indexOf(entry.toLowerCase(), foundPos + 1) :
              elem.indexOf(entry, foundPos + 1);
            isLabel = foundPos == -1 && labels[i];
            if (isLabel)
              foundPos = !entry? foundPos + 1: this.options.ignoreCase ?
                labels[i].toLowerCase().indexOf(entry.toLowerCase(), foundPos + 1) :
                labels[i].indexOf(entry, foundPos + 1);

          } while (foundPos != -1);
        }
      }
      if (partial.length)
        ret = ret.concat(partial.slice(0, this.options.choices - ret.length))
      var html = "<table style='cursor: default' width='100%' cellpadding='0' cellspacing='0'>";
      if (this.position) html += "<tbody><tr><td colspan='2' align='center' style='writing-mode: tb-rl'>&#171;</td></tr></tbody>";
      html += ret.join('');
      if (currentPosition - 1 == this.options.choices + this.begin) html += "<tbody><tr><td colspan='2' align='center' style='writing-mode: tb-rl'>&#187;</td></tr></tbody>";
      this.updateChoices(html + "</table>");//alert(ret.join(''));
    },
    getUpdatedChoices: function () {
      var datalist = this.element.getAttribute("list");
      if (datalist) {
        var values = [];
        var labels = [];
        var options = Element.children($(datalist));
        for (var i = 0; i < options.length; i++) {
          var option = options[i];
          values.push(option.getAttribute("value"));
          labels.push(option.getAttribute("label"));
        }
        //this.pageSize = 1;
        this.begin = this.position;
        this.parseUpdatedChoices(values, labels);
        return;
      }
      var elementsAttr = this.element.getAttribute("elements");
      var elements = {};
      if (elementsAttr) {
        try {
          eval("[" + elementsAttr + "]").each(function (input) {
            elements[input.name] = input.value;
          });
        }
        catch (e) { ibre.errors.push(attr + "\n" + e.message); }
      }
      JSONRequest.post(this.element.getAttribute("datalist"), {position: this.position + 1, length: this.options.choices + 1, value: this.getToken(), elements: elements}, function (reqNum, value, exception) {
        if (exception) { alertObj(exception); return; }
        this.element.datasource = value;
        var values = [];
        var labels = [];
        for (var i = 0; i < value.length; i++) {
          var option = value[i];
          values.push(option.value);
          labels.push(option.label);
        }
        //this.pageSize = this.options.choices;
        this.begin = 0;
        this.parseUpdatedChoices(values, labels);
      }.bind(this));

    },
    setOptions: function (options) {
      this.options = Object.extend({
        choices: 20,
        partialSearch: true,
        partialChars: 0,
        ignoreCase: true,
        fullSearch: true/*,
        onShow: function (element, update) {
          if(!update.style.position || update.style.position=='absolute') {
            update.style.position = 'absolute';
            Position.clone(element, update, {
              setHeight: false,
              offsetTop: element.offsetHeight
            });
            update.style.margin
          }
          Effect.Appear(update,{duration:0.15});
        }*/
      }, options || {});
    },
    onHover: function(event) {
      var element = Event.findElement(event, 'TBODY');
      if(this.index != element.autocompleteIndex)
      {
          this.index = element.autocompleteIndex;
          this.render();
      }
      Event.stop(event);
    },
    onKeyPress: function(event) {
      if(this.active)
        switch(event.keyCode) {
        case Event.KEY_TAB:
        case Event.KEY_RETURN:
          if (this.checkPage()) {
            Event.stop(event);
            return;
          }
          this.selectEntry();
          //Event.stop(event);
        case Event.KEY_ESC:
          this.hide();
          this.active = false;
          if (event.keyCode != Event.KEY_TAB) Event.stop(event);
          return;
        case Event.KEY_LEFT:
        case Event.KEY_RIGHT:
          return;
        case Event.KEY_UP:
          this.markPrevious();
          this.render();
          /*if(navigator.appVersion.indexOf('AppleWebKit')>0) */Event.stop(event);
          return;
        case Event.KEY_DOWN:
          this.markNext();
          this.render();
          /*if(navigator.appVersion.indexOf('AppleWebKit')>0) */Event.stop(event);
          return;
        }
      else {
        if (event.keyCode==Event.KEY_DOWN) {
          this.activate();
          return;
        }
        if(event.keyCode==Event.KEY_TAB || event.keyCode==Event.KEY_RETURN ||
          (navigator.appVersion.indexOf('AppleWebKit') > 0 && event.keyCode == 0)) return;
      }

      this.changed = true;
      this.position = 0;
      this.hasFocus = true;

      if(this.observer) clearTimeout(this.observer);
        this.observer =
          setTimeout(this.onObserverEvent.bind(this), this.options.frequency*1000);
    },
    checkPage: function () {
      if (!this.index && this.position) {
        if (this.position > this.pageSize) this.position -= this.pageSize;
        else this.position = 0;
        this.getUpdatedChoices();
        return true;
      }
      if (this.index == this.options.choices + 1 || this.index == this.options.choices && !this.position) {
        this.position += this.pageSize;
        this.getUpdatedChoices();
        return true;
      }
    },
    onBlur: function(event) {
      if (this.skipBlur) {
        this.skipBlur = false;
        return;
      }
      // needed to make click events working
      this.hideTimer = setTimeout(this.hide.bind(this), 250);
      this.hasFocus = false;
      this.active = false;
    },
    onClick: function(event) {
      var element = Event.findElement(event, 'TBODY');
      this.index = element.autocompleteIndex;
      if (this.checkPage()) {
        if (this.hideTimer) {
          clearTimeout(this.hideTimer);
        }
        else this.skipBlur = true;
        this.element.focus();
        this.activate();
        return;
      }
      this.selectEntry();
      this.hide();
    }
  });
  /*function showPopup() {
    this.style.display = "";
    alert("antes");
    wf2ParseHTMLChilds(this, true);
    alert("despues");
  }
  function hidePopup() {
    this.style.display = "none";
  }*/
  ibreParseHTML(document.documentElement);
  ibre.datasource.init(document.documentElement);
  var percentage = 1;
  var previousNumLoaded = 0;
  if (ibre.datasource.loading) runWhile(function () {
    if (ibre.datasource._parsed) return true;
    if (previousNumLoaded != ibre.datasource.numLoaded) {
      previousNumLoaded = ibre.datasource.numLoaded;
      percentage = 1;
    }
    var min = ibre._percentageDS * ibre.datasource.numLoaded / ibre.datasource._numLoading;
    var max = min + ibre._percentageDS / ibre.datasource._numLoading;
    var dist = max - min;
    ibre.progress(max - dist / percentage);
    percentage += ibre._percentageStep;
  });
  else {
    wf2ParseHTML(document.documentElement);
    var percentage2 = 1;
    runWhile(function () {
      if (!_numWF2HTMLChildNodes) {
        ibre.progress(100);
        ibre.parsed = true;
        return true;
      }
      ibre.progress(100 - 100 / percentage2);
      percentage2 += ibre._percentageStep;
    });
  }
  function getElementsBySelector(selector) {
    var nodes = [];
    for (var i = 0; i < selector.length; i++) {
      switch (selector[i].index) {
        case 7:
          var node = document.getElementById(selector[i].data);
          if (node) nodes.push(node);
          break;
      }
    }
    return nodes;
  }
  function loadCSS(href) {
    requestText(href, function (text) {
      try {
        var res = cssGrammar.compile(text);
        for (var i = 0; i < res.length; i++) {
          if (res[i].index != 2 || res[i].data.index != 0) continue;
          var ruleset = res[i].data.data;
          var selector = ruleset[0].data;
          if (!selector) continue;
          var specialSelectors = [];
          var isPseudo = false;
          for (var j = 0; j < selector.length; j++) {
            var data = selector[j].data;
            switch (selector[j].index) {
              case 0:
                if (isPseudo) specialSelectors.push(data);
                break;
              case 5:
                if (data == ":") {
                  isPseudo = true;
                  continue;
                }
                break;
              case 7:
                var node = document.getElementById(data);
                if (node) nodes.push(node);
                break;
            }
            if (isPseudo) isPseudo = false;
          }
          //alertObj(specialSelectors, true);
          for (var j = 0; j < specialSelectors.length; j++) {
            wf2CSSStyles[specialSelectors[j]] = ruleset[1].text;
          }
          /*var declarations = [];
          if (ruleset[1].data) declarations[0] = ruleset[1].data;
          for (var j = 0; j < ruleset[2].length; j++)
            if (ruleset[2][j].data)
              declarations.push(ruleset[2][j].data);
          for (var j = 0; j < declarations.length; j++) {
            var value = declarations[j][1];
            switch (declarations[j][0]) {
              case "position":
                if (value.length == 1 && value[0].index == 0 &&  value[0].data.index == 0 && value[0].data.data == "popup") {
                  var nodes = getElementsBySelector(selector);
                  for (var k = 0; k < nodes.length; k++) {
                    var node = nodes[k];
                    node.style.display = "none";
                    node.style.position = "absolute";
                    node.showPopup = showPopup;
                    node.hidePopup = hidePopup;
                  }
                }
                break;
            }
          }*/
        }
      }
      catch (e) {
        //alertObj(e, true);
        throw e;
      }
    });
  }
  var stylesheets = document.styleSheets;
  for (var i = 0; i < stylesheets.length; i++) {
    var stylesheet = stylesheets[i];
    if (stylesheet.href && stylesheet.href != location.href) loadCSS(stylesheet.href);
    else {
      var imports = stylesheet.imports;
      if (imports) {
        for (var j = 0; j < imports.length; j++) loadCSS(imports[j].href);
      }
      else {
        var rules = stylesheet.cssRules;
        for (var j = 0; j < rules.length; j++) {
          var rule = rules[j];
          if (rule.href) loadCSS(rule.href);
        }
      }
    }
  }
});
function _whatwgApplyFormEvents(target, elements) {
  for (var i = 0; i < elements.length; i++)
    if (!isInRepititionBlock(elements[i]))
      Event.addListener(elements[i], "change", function () {
        this.onformchange();
      }.bind(target));
}
ibre.progress = function (value) {
  if (document.body.onprogress) document.body.onprogress({progress: value});
  //if (value == 100 && ibre.errors.length) alert(ibre.errors.join("\n\n"));
}
ibre.datasource.init = function (element) {
  var links = element.getElementsByTagName("link");
  for (var i = 0; i < links.length; i++) {
    var node = links[i];
    if (node.rel == "datasource") {
      if (node.type == "application/jsonrequest") {
        ibre.datasource.make(node);
      }
    }
  }
  links = ibre.datasource.nodes;
  for (var i = 0; i < links.length; i++) {
    var link = links[i];
    if (link.href && !link.noauto) {
      link.loadDatasource();
      ibre.datasource._numLoading++;
    }
  }
  if (!ibre.datasource._numLoading) ibre.datasource._parsed = true;
}
ibre.datasource.make = function (node, attrName) {
  if (!attrName) attrName = "name";
  var onload = node.getAttribute("ibre-onload");
  if (onload) {
    eval("node.onload = function () { with (this) { " + onload + "; } }.bind(node);");
  }
  var name = node.getAttribute(attrName);
  var old = ibre.datasource.namedNodes[name];
  if (old) ibre.datasource.nodes.remove(old);
  ibre.datasource.nodes.push(node);
  ibre.datasource.namedNodes[name] = node;
  if (/^[a-zA-Z0-9_]+$/.test(name)) node._name = name;
  node._datasourceName = name;
  node.loaded = true;
  node.noauto = node.getAttribute("noauto") != null;
  ibre.datasource.values[name] = null;
  /*node.onloads = [];
  node.addOnload = function (listener) {
    node.onloads.push(listener);
  }*/
  node._receivedHandler = function (reqNum, value, exception) {
    if (!exception) {
      this.datasource = value;
      ibre.datasource.values[this._datasourceName] = value;
      var links = ibre.datasource.nodes;
      this.loaded = true;
      for (var i = 0; i < links.length; i++) {
        var node = links[i];
        if (node != this && !node.loaded) break;
      }
      if (!ibre.datasource._parsed) {
        ibre.datasource.numLoaded++;
        ibre.progress(ibre._percentageDS * ibre.datasource.numLoaded / links.length);
      }
      if (i == links.length) {
        if (ibre.datasource._parsed) {
          runWhen(function () {
            ibre.progress(ibre._percentageTPL);
            ibre.datasource.update();
            var percentage4 = 1;
            //var dist4 = 100 - ibre._percentageTPL;
            runWhile(function () {
              if (document._context.parsed && !_numWF2HTMLChildNodes) {
                if (this.onload) this.onload();
                ibre.progress(100);
                window.status = "Terminado";
                return true;
              }
              ibre.progress(100 - 100 / percentage4);
              percentage4 += ibre._percentageStep;
              //for (var i= 0; i < this.onloads.length; i++) this.onloads[i]();
            }.bind(this));
          }.bind(this), "!_numParseHTMLChildNodes && document._context.parsed && !_numWF2HTMLChildNodes");
        }
        else {
          ibre.datasource._parsed = true;
          runWhen(function () {
            ibre.progress(ibre._percentageTPL);
            ibre.datasource.update();
            var percentage = 1;
            var dist = ibre._percentageParse - ibre._percentageTPL;
            runWhile(function () {
              if (document._context.parsed) {
                ibre.progress(ibre._percentageParse);
                wf2ParseHTML(document.documentElement);
                var percentage2 = 1;
                var dist2 = 100 - ibre._percentageParse;
                runWhile(function () {
                  if (!_numWF2HTMLChildNodes) {
                    ibre.progress(100);
                    window.status = "Terminado";
                    ibre.parsed = true;
                    for (var i = 0; i < links.length; i++) {
                      var node = links[i];
                      if (node.href && node.onload) node.onload();
                      //for (var i= 0; i < node.onloads.length; i++) node.onloads[i]();
                    }
                    return true;
                  }
                  ibre.progress(100 - dist2 / percentage2);
                  percentage2 += ibre._percentageStep;
                });
                return true;
              }
              ibre.progress(ibre._percentageParse - dist / percentage);
              percentage += ibre._percentageStep;
            });
          }, "!_numParseHTMLChildNodes");
        }
      }
    }
    else alertObj(exception);
  }.bind(node);
  node.loadDatasource = function (href, data) {
    if (href) this.href = href;
    window.status = "Cargando " + this.href;
    this.loaded = false;
    ibre.datasource.loading = true;
    /*if (ibre.datasource._parsed) {
      var percentage3 = 1;
      runWhile(function (node) {
        if (node.loaded && document._context.parsed && !_numWF2HTMLChildNodes) {
          ibre.progress(100);
          return true;
        }
        ibre.progress(100 - 100 / percentage3);
        percentage3 += ibre._percentageStep;
      }, [node]);
    }*/
    JSONRequest.post(this.href, data, this._receivedHandler);
  }.bind(node);
}