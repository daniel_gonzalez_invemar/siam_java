function StringCommentItem(text) {
  this.items = [
    new ReferenceItem("COMMENT"),
    new StringWSItem(text)
  ];
}
StringCommentItem.prototype.compile = function (context) {
  return context.grammar.testBlock(this.items, context);
}
var cssGrammar = new Grammar({
  stylesheet: [
    [
      new ZeroOrMoreItem([
        new OrItem([
          new ReferenceItem("CDO"),
          new ReferenceItem("CDC"),
          new ReferenceItem("statement")
        ])
      ]),
      new ReferenceItem("COMMENT"),
      new WSItem("COMMENT")
    ], null, function (data) { return data[0]; }
  ],
  statement: [
    [
      new OrItem([
        new ReferenceItem("ruleset"),
        new ReferenceItem("at-rule")
      ])
    ]
  ],
  "at-rule": [
    [
      new ReferenceItem("ATKEYWORD"),
      new ZeroOrMoreItem([
        new ReferenceItem("any")
      ]),
      new OrItem([
        new ReferenceItem("block"),
        new StringCommentItem(";")
      ])
    ]
  ],
  block: [
    [
      new StringCommentItem("{"),
      new ZeroOrMoreItem([
        new OrItem([
          new ReferenceItem("any"),
          new ReferenceItem("block"),
          new ReferenceItem("ATKEYWORD"),
          new StringCommentItem(";")
        ])
      ]),
      new StringCommentItem("}")
    ]
  ],
  ruleset: [
    [
      new OptionalItem([
        new ReferenceItem("selector")
      ]),
      new StringCommentItem("{"),
      new ReferenceItem("declarations"),
      new StringCommentItem("}")
    ]
  ],
  declarations: [
    [
      new OptionalItem([
        new ReferenceItem("declaration")
      ]),
      new ZeroOrMoreItem([
        new StringCommentItem(";"),
        new OptionalItem([
          new ReferenceItem("declaration")
        ])
      ])
    ], true
  ],
  selector: [
    [
      new OneOrMoreItem([
        new ReferenceItem("any")
      ])
    ]
  ],
  declaration: [
    [
      new ReferenceItem("property"),
      new StringCommentItem(":"),
      new ReferenceItem("value")
    ]
  ],
  property: [
    [
      new ReferenceItem("IDENT")
    ]
  ],
  value: [
    [
      new OneOrMoreItem([
        new OrItem([
          new ReferenceItem("any"),
          new ReferenceItem("block"),
          new ReferenceItem("ATKEYWORD")
        ])
      ])
    ]
  ],
  any: [
    [
      new OrItem([
        new ReferenceItem("IDENT"),
        new ReferenceItem("PERCENTAGE"),
        new ReferenceItem("DIMENSION"),
        new ReferenceItem("NUMBER"),
        new ReferenceItem("STRING"),
        new ReferenceItem("DELIM"),
        new ReferenceItem("URI"),
        new ReferenceItem("HASH"),
        new ReferenceItem("UNICODE-RANGE"),
        new ReferenceItem("INCLUDES"),
        new ReferenceItem("FUNCTION"),
        new ReferenceItem("DASHMATCH"),
        new ListItem([
          new StringCommentItem("("),
          new ZeroOrMoreItem([new ReferenceItem("any")]),
          new StringCommentItem(")")
        ]),
        new ListItem([
          new StringCommentItem("["),
          new ZeroOrMoreItem([new ReferenceItem("any")]),
          new StringCommentItem("]")
        ])
      ])
    ]
  ],
  IDENT: [
    [
      new WSItem(),
      new ReferenceItem("{ident}")
    ]
  ],
  ATKEYWORD: [
    [
      new StringCommentItem("@"),
      new ReferenceItem("{ident}")
    ]
  ],
  STRING: [
    [
      new WSItem(),
      new ReferenceItem("{string}")
    ]
  ],
  DELIM: [
    [
      new WSItem(),
      new OneCharItem(":>+")
    ]
  ],
  HASH: [
    [
      new StringCommentItem("#"),
      new ReferenceItem("{name}")
    ]
  ],
  NUMBER: [
    [
      new WSItem(),
      new ReferenceItem("{num}")
    ]
  ],
  PERCENTAGE: [
    [
      new WSItem(),
      new ReferenceItem("{num}"),
      new StringItem("%")
    ]
  ],
  DIMENSION: [
    [
      new WSItem(),
      new ReferenceItem("{num}"),
      new ReferenceItem("{ident}")
    ]
  ],
  URI: [
    [
      new StringCommentItem("url("),
      new ReferenceItem("COMMENT"),
      new ReferenceItem("{string}"),
      new StringCommentItem(")")
    ]
  ],
  "UNICODE-RANGE": [
    [
      new StringWSItem("U+"),
      new OneToMaxItem(6, [
        new OneCharItem("0-9A-Fa-f?")
      ]),
      new OptionalItem([
        new StringItem("-"),
        new OneToMaxItem(6, [
          new OneCharItem("0-9A-Fa-f?")
        ])
      ])
    ]
  ],
  CDO: [
    [
      new StringCommentItem("<!--")
    ]
  ],
  CDC: [
    [
      new StringCommentItem("-->")
    ]
  ],
  FUNCTION: [
    [
      new WSItem(),
      new ReferenceItem("{ident}"),
      new StringItem("(")
    ]
  ],
  INCLUDES: [
    [
      new StringCommentItem("~=")
    ]
  ],
  DASHMATCH: [
    [
      new StringCommentItem("|=")
    ]
  ],
  COMMENT: [
    [
      new OptionalItem([
        new StringWSItem("/*"),
        new ZeroOrMoreItem([
          new OneCharItem("^*")
        ]),
        new OneOrMoreItem([
          new StringItem("*")
        ]),
        new ZeroOrMoreItem([
          new OneCharItem("^/"),
          new OneOrMoreItem([
            new OneCharItem("^*")
          ]),
          new OneOrMoreItem([
            new StringItem("*")
          ])
        ]),
        new StringItem("/")
      ])
    ]
  ],
  "{ident}": [
    [
      new ReferenceItem("{nmstart}"),
      new ZeroOrMoreItem([
        new ReferenceItem("{nmchar}")
      ])
    ], null, function (data) { return data[0] + data[1].join(""); }
  ],
  "{name}": [
    [
      new OneOrMoreItem([
        new ReferenceItem("{nmchar}")
      ])
    ], null, function (data) { return data.join(""); }
  ],
  "{nmstart}": [
    [
      new OrItem([
        new OneCharItem("a-zA-Z"),
        new ReferenceItem("{nonascii}"),
        new ReferenceItem("{escape}")
      ])
    ], null, function (data) { return data.data; }
  ],
  "{nonascii}": [
    [
      new OneCharItem("^\0-\177")
    ]
  ],
  "{unicode}": [
    [
      new StringItem("\\"),
      new OneToMaxItem(6, [
        new OneCharItem("0-9a-f")
      ]),
      new OptionalItem([
        new OneCharItem(" \n\r\t\f")
      ])
    ], null, function (data) { return parseInt(data[0].join(""), 16); }
  ],
  "{escape}": [
    [
      new OrItem([
        new ReferenceItem("{unicode}")/*,
        new ListItem([
          new StringItem("\\"),
          new OneCharItem(" -~\200-\4177777")
        ])*/
      ])
    ], null, function (data) { return data.data; }
  ],
  "{nmchar}": [
    [
      new OrItem([
        new OneCharItem("a-zA-Z0-9-"),
        new ReferenceItem("{nonascii}"),
        new ReferenceItem("{escape}")
      ])
    ], null, function (data) { return data.data; }
  ],
  "{num}": [
    [
      new OrItem([
        new ListItem([
          new ZeroOrMoreItem([
            new OneCharItem("0-9")
          ]),
          new StringItem("."),
          new OneOrMoreItem([
            new OneCharItem("0-9")
          ])
        ]),
        new OneOrMoreItem([
          new OneCharItem("0-9")
        ])
      ])
    ], null, function (data) {
      var value;
      if (data.index == 1) value = parseInt(data.data.join(""));
      else value = parseFloat(data.data[0].join("") + "." + data.data[1].join(""));
      return value;
    }
  ],
  "{string}": [
    [
      new OrItem([
        new ReferenceItem("{string1}"),
        new ReferenceItem("{string2}")
      ])
    ], null, function (data) { return data.data; }
  ],
  "{string1}": [
    [
      new StringItem("\""),
      new ZeroOrMoreItem([
        new OrItem([
          new OneCharItem("\t !#$%&(-~"),
          new ListItem([
            new StringItem("\\"),
            new ReferenceItem("{nl}")
          ]),
          new StringItem("'"),
          new ReferenceItem("{nonascii}"),
          new ReferenceItem("{escape}")
        ])
      ]),
      new StringItem("\"")
    ], null, function (data) { return data.data; }
  ],
  "{string2}": [
    [
      new StringItem("'"),
      new ZeroOrMoreItem([
        new OrItem([
          new OneCharItem("\t !#$%&(-~"),
          new ListItem([
            new StringItem("\\"),
            new ReferenceItem("{nl}")
          ]),
          new StringItem("\""),
          new ReferenceItem("{nonascii}"),
          new ReferenceItem("{escape}")
        ])
      ]),
      new StringItem("'")
    ]
  ],
  "{nl}": [
    [
      new OrItem([
        new StringItem("\n"),
        new StringItem("\r\n"),
        new StringItem("\r"),
        new StringItem("\f")
      ])
    ]
  ],
  "{w}": [
    [
      new ZeroOrMoreItem([
        new OneCharItem(" \t\r\n\f")
      ])
    ]
  ]
});
