function wf2FormSubmit(form) {
  var input;
  var errors = [];
  function addError(msg) {
    errors.push({msg: msg, input: input});
  }
  function mustCheck() {
    if (input.value == "") {
      if (isRequired) addError(langStrings.valueMissing);
      return false;
    }
    return true;
  }
  function checkMinLength() {
    var minlength = input.getAttribute("minlength");
    if (minlength != null && input.value.length < minlength) {
      addError(langStrings.tooShortBegin + minlength + langStrings.tooShortEnd);
      return false;
    }
    return true;
  }
  function checkPattern() {
    var pattern = input.getAttribute("pattern");
    if (pattern != null && !input.value.match(new RegExp("^" + pattern + "$"))) {
      addError(langStrings.patternMismatch);
      return false;
    }
    return true;
  }
  function testChanged() {
    if (onlySubmitChanged && input.value && input.value == input.defaultValue) {
      form.noSubmitElements.push(input);
      return false;
    }
    return true;
  }
  var onlySubmitChanged = form.getAttribute("onlySubmitChanged") != null;
  var nonVisibleElements = form.getAttribute("nonVisibleElements");
  var noValidateNonVisibleElements = nonVisibleElements && (nonVisibleElements == "skip" || nonVisibleElements == "only-submit");
  var noSubmitNonVisibleElements = nonVisibleElements && (nonVisibleElements == "skip" || nonVisibleElements == "only-validate");
  form.noSubmitElements = [];
  for (var i = 0; i < form.elements.length; i++) {
    input = form.elements[i];
    if (isInRepititionBlock(input)) {
      form.noSubmitElements.push(input);
      continue;
    }
    if ((noValidateNonVisibleElements || noSubmitNonVisibleElements) && !domIsVisible(input)) {
      if (noSubmitNonVisibleElements) form.noSubmitElements.push(input);
      if (noValidateNonVisibleElements) continue;
    }
    if (input.disabled) continue;
    if (input.onvalidate && input.onvalidate() === false || input.validate && !input.validate())
      addError(langStrings.validationError);
    var isRequired = input.getAttribute("required") != null;
    switch (input.tagName.toLowerCase()) {
      case "input":
        var min = parseFloat(input.getAttribute("min"));
        var max = parseFloat(input.getAttribute("max"));
        var step = input.getAttribute("step");
        if (step != "any") {
          step = parseFloat(step);
          if (isNaN(step)) step = 1;
        }
        switch (input.getAttribute("type")) {
          case "add":
          case "remove":
          case "move-up":
          case "move-down":
          case "button":
          case "submit":
          case "reset":
          case "hidden":
          case "image":
            break;
          case "datetime":
            if (testChanged() && mustCheck()) {
              if (!_wf2DatetimePattern.test(input.value)) addError(langStrings.dateMismatch);
            }
            break;
          case "datetime-local":
            if (testChanged() && mustCheck()) {
              if (!_wf2DatetimePattern.test(input.value)) addError(langStrings.dateMismatch);
            }
            break;
          case "date":
            if (testChanged() && mustCheck()) {
              if (!_wf2DatePattern.test(input.value)) addError(langStrings.dateMismatch);
            }
            break;
          case "month":
            if (testChanged() && mustCheck()) {
              if (!_wf2MonthPattern.test(input.value)) addError(langStrings.dateMismatch);
            }
            break;
          case "week":
            if (testChanged() && mustCheck()) {
              if (!_wf2WeekPattern.test(input.value)) addError(langStrings.dateMismatch);
            }
            break;
          case "time":
            if (testChanged() && mustCheck()) {
              if (!_wf2TimePattern.test(input.value)) addError(langStrings.dateMismatch);
            }
            break;
          case "number":
            if (testChanged() && mustCheck()) {
              var number = parseFloat(input.value);
              if (isNaN(number)) addError(langStrings.numberMismatch);
              else {
                if (!isNaN(min) && number < min)
                  addError(langStrings.rangeUnderflowBegin + min + langStrings.rangeUnderflowEnd);
                if (!isNaN(max) && number > max)
                  addError(langStrings.rangeOverflowBegin + max + langStrings.rangeOverflowEnd);
                if (step != "any") {
                  var _min = isNaN(min)? null: new BigDecimal(input.getAttribute("min"));
                  var _max = isNaN(max)? null: new BigDecimal(input.getAttribute("max"));
                  var _step = new BigDecimal(step == 1? "1": input.getAttribute("step"));
                  var _number = new BigDecimal(input.value);
                  /*var n1 = number - (min || max || 0);
                  var n2 = step;
                  var module = n1 - n2 * Math.floor(n1 / n2);*/
                  //var module = Math.module(number - (min || max || 0), step);
                  //addError(langStrings.stepMismatchBegin + max + langStrings.stepMismatchEnd);
                  //if (module) input.value = number - module + (module < step / 2? 0: step);
                  var base;
                  if (min) base = _number.subtract(_min);
                  else if (max) base = _number.subtract(_max);
                  else base = _number;
                  var module = base.remainder(_step);
                  var carry;
                  if (module.compareTo(_step.divide(new BigDecimal("2"), _step.scale() + 1, MathContext.prototype.DEFAULT_ROUNDINGMODE)) == -1)
                    carry = module;
                  else carry = module.subtract(_step);
                  input.value = _number.subtract(carry).finish(BigDecimal.prototype.plainMC, true);
                }
              }
            }
            break;
          case "range":
            if (testChanged() && mustCheck()) {
            }
            break;
          case "email":
            if (testChanged() && mustCheck() && checkMinLength() && checkPattern()) {
              if (!_wf2MailPattern.test(input.value)) addError(langStrings.emailMismatch);
            }
            break;
          case "url":
            if (testChanged() && mustCheck() && checkMinLength() && checkPattern()) {
            }
            break;
          case "password":
            if (mustCheck() && checkMinLength() && checkPattern()) {
            }
            break;
          case "checkbox":
            if (isRequired && !input.checked) addError(langStrings.valueMissing);
            break;
          case "radio":
            if (isRequired) {
              var radios = domGroupInputs(input);
              for (var j = 0; j < radios.length; j++)
                if (radios[j].checked) break;
              if (j == radios.length) addError(langStrings.valueMissing);
            }
            break;
          case "file":
            if (mustCheck()) {
            }
            break;
          default:
            if (testChanged() && mustCheck() && checkMinLength() && checkPattern()) {
            }
        }
        break;
      case "select":
        if (isRequired && (!input.options.length || input.options[input.selectedIndex].value == ""))
          addError(langStrings.valueMissing);
        break;
      case "textarea":
        if (testChanged() && mustCheck() && checkMinLength() && checkPattern()) {
        }
        break;
    }
  }
  var msg = "";
  for (var i = 0; i < errors.length; i++) {
    var error = errors[i];
    var validationMsg = error.input.getAttribute("validationMessage");
    if (validationMsg) msg += validationMsg;
    else {
      var labelText = error.input.getAttribute("labelText");
      if (labelText) msg += langStrings.fieldErrorBegin + labelText + langStrings.fieldErrorEnd;
      else if (error.input.label) msg += langStrings.fieldErrorBegin + getInnerText(error.input.label) + langStrings.fieldErrorEnd;
      else if (error.input.name) msg += langStrings.fieldErrorBegin + error.input.name + langStrings.fieldErrorEnd;
      else msg += langStrings.fieldError;
      msg += "\n" + error.msg;
    }
    if (i < errors.length - 1) {
      msg += "\n\n";
      if (i == 2 && errors.length > 4) {
        msg += langStrings.moreErrorBegin + (errors.length - 3) + langStrings.moreErrorEnd;
        break;
      }
    }
  }
  if (msg) {
    alert(msg);
    errors[0].input.focus();
    return false;
  }
  return true;
}
