<?php
require_once "JSON.php";
//if (!function_exists("json_encode")) {
  $_json_object = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
  function ibre_json_encode($var) {
    return $GLOBALS["_json_object"]->encode($var);
  }
  function ibre_json_decode($str) {
    return $GLOBALS["_json_object"]->decode($str);
  }
//}
function json_send($var) {
  header("Content-type: application/jsonrequest");
  echo ibre_json_encode($var);
}
if (!function_exists("getallheaders")) {
  function getallheaders() {
    foreach($_SERVER as $name => $value)
      if(substr($name, 0, 5) == 'HTTP_')
        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
    return $headers;
  }
}
$headers = getallheaders();
if ($_SERVER["REQUEST_METHOD"] == "POST" && $headers["Content-Type"] == "application/jsonrequest")
  $_JSON = ibre_json_decode($HTTP_RAW_POST_DATA);
else $_JSON = NULL;
?>