/*
    json.js
    2006-04-28

    This file adds these methods to JavaScript:

        object.toJSONString()

            This method produces a JSON text from an object. The
            object must not contain any cyclical references.

        array.toJSONString()

            This method produces a JSON text from an array. The
            array must not contain any cyclical references.

        string.parseJSON()

            This method parses a JSON text to produce an object or
            array. It will return false if there is an error.
*/
(function () {
    var m = {
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        s = {
            array: function (x) {
                var a = ['['], b, f, i, l = x.length, v;
                for (i = 0; i < l; i += 1) {
                    v = x[i];
                    f = s[typeof v];
                    if (f) {
                        v = f(v);
                        if (typeof v == 'string') {
                            if (b) {
                                a[a.length] = ',';
                            }
                            a[a.length] = v;
                            b = true;
                        }
                    }
                }
                a[a.length] = ']';
                return a.join('');
            },
            'boolean': function (x) {
                return String(x);
            },
            'null': function (x) {
                return "null";
            },
            number: function (x) {
                return isFinite(x) ? String(x) : 'null';
            },
            object: function (x) {
                if (x) {
                    if (x instanceof Array) {
                        return s.array(x);
                    }
                    var a = ['{'], b, f, i, v;
                    for (i in x) {
                        v = x[i];
                        f = s[typeof v];
                        if (f) {
                            v = f(v);
                            if (typeof v == 'string') {
                                if (b) {
                                    a[a.length] = ',';
                                }
                                a.push(s.string(i), ':', v);
                                b = true;
                            }
                        }
                    }
                    a[a.length] = '}';
                    return a.join('');
                }
                return 'null';
            },
            string: function (x) {
                if (/["\\\x00-\x1f]/.test(x)) {
                    x = x.replace(/([\x00-\x1f\\"])/g, function(a, b) {
                        var c = m[b];
                        if (c) {
                            return c;
                        }
                        c = b.charCodeAt();
                        return '\\u00' +
                            Math.floor(c / 16).toString(16) +
                            (c % 16).toString(16);
                    });
                }
                return '"' + x + '"';
            }
        };

    /*Object.prototype.toJSONString = function () {
        return s.object(this);
    };

    Array.prototype.toJSONString = function () {
        return s.array(this);
    };

    String.prototype.toJSONString = function () {
        return s.string(this);
    };*/
})();

/*String.prototype.parseJSON = function () {
    try {
        return !(/[^,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]/.test(
                this.replace(/"(\\.|[^"\\])*"/g, ''))) &&
            eval('(' + this + ')');
    } catch (e) {
        return false;
    }
};*/
JSONRequest = {
  _requests: [],
  _common: function (url, method, done, data) {
    //var req = new XMLHttpRequest();
    var req = Ajax.getTransport();
    var requestNumber = this._requests.length;
    this._requests.push(req);
    req.open(method, url, true);
    req.setRequestHeader("Content-Type","application/jsonrequest");
    var alive = true;
    req.onreadystatechange = function () {
      if (req.readyState == 4) {
				alive = false;
        var isOK;
        try { isOK = req.status == 200; } catch (e) { return; }
        if (isOK) {
          var contentType = req.getResponseHeader("Content-Type");
          if (contentType && contentType.startsWith("application/jsonrequest"))
            done(requestNumber, req.responseText.evalJSON());
          else {
            done(requestNumber, null, {name: "JSONRequestError", message: "bad response", native: req});
            //alert("The response isn't a json valid for uri '" + url + "'\n\n" + req.responseText);
          }
        }
        else {
          done(requestNumber, null, {name: "JSONRequestError", message: "not ok", native: req});
          //alert("There was a problem retrieving the data for uri '" + url + "':\n" + req.statusText);
        }
      }
    };
    setTimeout(function () {
      if (alive) {
        req.abort();
        done(requestNumber, null, {name: "JSONRequestError", message: "no response", native: req});
        //alert("Cannot connect to server");
      }
    }, 1000 * timeout);
    req.send(data);
    return requestNumber;
  },
  post: function (url, send, done, timeout) {
    return this._common(url, "POST", done, send == null? "null": Object.toJSON(send));
  },
  get: function (url, done) {
    return this._common(url, "GET", done, null);
  },
  cancel: function (requestNumber) {
    this._requests[requestNumber].abort();
  }
};