function alertObj(obj, showFunctions) {
  if (typeof obj != "object") {
    alert(obj);
    return;
  }
  function showObj(obj, node) {
    node.innerHTML = "";
    var table = jsToDom("table", {border: "1", cellpadding: "4", cellspacing: "0"}, node);
    table.style.borderCollapse = "collapse";
    table = jsToDom("tbody", null, table);
    var s = [];
    for (var a in obj) {
      try {
        if (showFunctions || typeof obj[a] != "function") s.push(a);
      }
      catch (e) {
        s.push(a);
      }
    }
    if (!(obj instanceof Array)) s.sort();
    for (var i = 0; i < s.length; i++) {
      var tr = jsToDom("tr", null, table);
      var tdName = jsToDom("td", {align: "right", width: "1"}, tr);
      var field = s[i];
      var value;
      var valueStr;
      try {
        value = obj[field];
        valueStr = new String(value);
      }
      catch (e) {
        value = e;
        valueStr = e.toString();
      }
      var tdValue = jsToDom("td", null, tr);
      switch (typeof value) {
        case "object":
          if (value == null) {
            setInnerText(tdName, field);
            setInnerText(tdValue, "null");
            tdValue.style.fontStyle = "italic";
            break;
          }
        //case "array":
          var link = jsToDom("a", {href: "javascript: ;"}, tdName);
          tdValue.libreValue = value;
          setInnerText(link, field);
          setInnerText(tdValue, valueStr);
          /*link.onclick = function () {alert(3);
            showObj(this.libreValue, this);
          }.bind(td);*/
          Event.observe(link, "click", function () {
            showObj(this.libreValue, this);
          }.bind(tdValue));
          //td.style.cursor = "pointer";*/
          break;
        default:
          setInnerText(tdName, field);
          setInnerText(tdValue, valueStr);
      }
    }
  }
  var win = open("", "", "width=500,height=300,scrollbars=1");
  win.document.write("<html><head><title>Debug</title></head><body></body></html>");
  showObj(obj, win.document.body);
}
function alertXML(node) {
  alert(new XMLSerializer().serializeToString(node));
}
function getObjectByField(list, field, value, ignoreCase) {
  for (var i = 0; i < list.length; i++)
    if (ignoreCase? new String(list[i][field]).toLowerCase() == new String(value).toLowerCase(): list[i][field] == value) return list[i];
  return null;
}
function equals(o1, o2) {
  if (o1 instanceof Array || o2 instanceof Array) {
    if (o1 instanceof Array && o2 instanceof Array && o1.length == o2.length) {
      for (var i = 0; i < o1.length; i++) if (!equals(o1[i], o2[i])) return false;
      return true;
    }
    return false;
  }
  switch (typeof o1) {
    case "boolean":
    case "function":
    case "number":
    case "string":
      return o1 === o2;
  }
  switch (typeof o2) {
    case "boolean":
    case "function":
    case "number":
    case "string":
      return o1 === o2;
  }
  if (o1 === o2) return true;
  var values = equals._values;
  if (values.contains(o1) || values.contains(o2) || !equals(Object.keys(o1), Object.keys(o2)))
    return false;
  values.push(o1);
  values.push(o2);
  var r = equals(Object.values(o1), Object.values(o2));
  values.removeItem(o1);
  values.removeItem(o2);
  return r;
};
equals._values = [];
String.prototype.startsWith = function (str) {
  return this.substring(0, str.length) == str;
};
String.prototype.contains = function (str) {
  return this.indexOf(str) != -1;
};
String.prototype.substringBefore = function (str) {
  var index = this.indexOf(str);
  if (index == -1) return "";
  return this.substring(0, index - 1);
};
String.prototype.substringAfter = function (str) {
  var index = this.indexOf(str);
  if (index == -1) return "";
  return this.substring(index);
};
String.prototype.leftTrim = function () {
  return this.replace(/^\s+/g, "");
};
String.prototype.rightTrim = function () {
  return this.replace(/\s+$/g, "");
};
String.prototype.trim = function () {
  return this.leftTrim().rightTrim();
};
String.prototype.isWS = function () {
  return this == ' ' || this == '\n' || this == '\r' || this == '\t' || this == '\f';
};
String.prototype.escapeAllHTML = function () {
  return this.escapeHTML().gsub(/\s/, "&nbsp;");
};
Array.prototype.isEmpty = function () {
  return this.length == 0;
};
Array.prototype.clear = function () {
  while (this.length) this.pop();
};
Array.prototype.contains = function (elem) {
  return this.indexOf(elem) != -1;
};
Array.prototype.indexOf = function (elem) {
  for (var i = 0; i < this.length; i++) if (this[i] == elem) return i;
  return -1;
};
Array.prototype.lastIndexOf = function (elem) {
  for (var i = this.length - 1; i >= 0; i--) if (this[i] == elem) return i;
  return -1;
};
Array.prototype.remove = function (index) {
  /*if (index < 0 || index >= this.length) return;
  var value = this[index];
  var last = this.pop();
  if (index < this.length) {
    this[index] = last;
  }
  return value;*/
  return this.splice(index, 1);
};
Array.prototype.removeItem = function (value) {
  return this.splice(this.indexOf(value), 1);
};
Array.prototype.last = function () {
  if (!this.length) return null;
  return this[this.length - 1];
};
Array.prototype.get = function (field) {
  var a = [];
  this.each(function (item) {
    a.push(item[field]);
  });
  return a;
};
Math.module = function (n1, n2) {
  return n1 - n2 * Math.floor(n1 / n2);
}
Function.prototype.params = function() {
  return this.bind.apply(this, [window].concat($A(arguments)));
}
function numberFormat(nStr, decimals, min) {
	decimals = 2 || decimals;
  nStr += '';
  var x = nStr.split('.');
  var x1 = x[0];
  var x2 = x.length > 1? '.' + (x[1].length > decimals? parseInt(x[1].substr(0, decimals)) + 1: x[1]) : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) x1 = x1.replace(rgx, '$1' + ',' + '$2');
  return x1 + x2;
}
function setCookie(name, value, expires, path, domain, secure) {
  // set time, it's in milliseconds
  var today = new Date();
  today.setTime(today.getTime());

  /*
  if the expires variable is set, make the correct
  expires time, the current script below will set
  it for x number of days, to make it for hours,
  delete * 24, for minutes, delete * 60 * 24
  */
  /*if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }*/
  var expires_date = new Date(today.getTime() + (expires));

  document.cookie = name + "=" + escape(value) +
    ((expires)? ";expires=" + expires_date.toGMTString(): "") +
    ((path)? ";path=" + path: "") +
    ((domain)? ";domain=" + domain: "") +
    ((secure)? ";secure": "");
}
// this function gets the cookie, if it exists
function getCookie(name) {

  var start = document.cookie.indexOf(name + "=");
  var len = start + name.length + 1;
  if (!start && name != document.cookie.substring(0, name.length)) {
    return null;
  }
  if (start == -1) return null;
  var end = document.cookie.indexOf(";", len);
  if (end == -1) end = document.cookie.length;
  return unescape(document.cookie.substring(len, end));
}
// this deletes the cookie when called
function deleteCookie(name, path, domain) {
  if (getCookie(name)) document.cookie = name + "=" +
    ((path)? ";path=" + path: "") +
    ((domain)? ";domain=" + domain: "") +
    ";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}
function _runFunctions(funcs, params) {
  if (!params) params = [];
  params = funcs.shift().apply(null, params);
  if (funcs.length) ibreSetTimeout(_runFunctions, 1, funcs, params);
}
function runFunctions(funcs, params) {
  ibreSetTimeout(_runFunctions, 1, funcs, params);
}
function runFor(func, list, params, endFunc, endParams, index) {
  if (index == null) index = 0;
  if (index >= list.length) {
    if (!endParams) endParams = [];
    if (endFunc) endFunc.apply(null, endParams);
    return;
  };
  ibreSetTimeout(function (func, list, params, endFunc, endParams, index) {
    if (!params) params = [];
    var when = func.apply(list[index], params);
    if (when) runWhen(runFor, when, [func, list, params, endFunc, endParams, index + 1]);
    else runFor(func, list, params, endFunc, endParams, index + 1);
  }, 0, func, list, params, endFunc, endParams, index);
  /*var when = func.apply(list[index], params);
  if (when) runWhen(runFor, when, [func, list, params, endFunc, endParams, index + 1]);
  else setTimeout(runFor, 0, func, list, params, endFunc, endParams, index + 1);*/
}
function runWhen(func, cond, params1, params2) {
  if (!params1) params1 = [];
  if (typeof cond == "string") {
    if (eval(cond)) func.apply(null, params1);
    else ibreSetTimeout(runWhen, 200, func, cond, params1, params2);
    return;
  }
  if (!params2) params2 = [];
  if (cond.apply(null, params2)) func.apply(null, params1);
  else ibreSetTimeout(runWhen, 200, func, cond, params1, params2);
}
function runWhile(func, params) {
  if (!params) params = [];
  if (!func.apply(null, params)) ibreSetTimeout(runWhile, 200, func, params);
}
function ibreSetTimeout(func, milliseconds) {
  if (ie) {
    var args = $A(arguments);
    args.splice(0, 2);
    setTimeout(function () {
      func.apply(null, args);
    }, milliseconds);
  }
  else setTimeout.apply(window, arguments);
}
function uniqueId() {
  return "_asgsghsg_" + (++_uniqueIdCounter);
}
_uniqueIdCounter = 0;
function $FF(element) {
  return parseFloat($F(element));
}
function stacktrace() {
  var s = [];
  for(var a = stacktrace.caller; a != null; a = a.caller) {
    s.push(a/*.callee*/);
    if (a.caller == a) break;
  }
  return s;
}