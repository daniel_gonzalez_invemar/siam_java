function domRemoveNode(node) {
  return node.parentNode.removeChild(node);
}
function domReplaceNode(newNode, oldNode) {
  return oldNode.parentNode.replaceChild(newNode, oldNode);
}
function domInsertBefore(node, brother) {
  return brother.parentNode.insertBefore(node, brother);
}
function domInsertAfter(node, brother) {
  return brother.parentNode.insertBefore(node, brother.nextSibling);
}
function domCopyNode(src, dest) {
  return dest.appendChild(src.cloneNode(true));
}
function domCopyChildNodes(src, dest) {
  var nodes = src.childNodes;
  for (var i = 0; i < nodes.length; i++) domCopyNode(nodes[i], dest);
}
function domRemoveChildNodes(node) {
  while (node.lastChild) node.removeChild(node.lastChild);
}
function domNextElement(node) {
  do {
    node = node.nextSibling
    if (node.nodeType == 1) return node;
  } while (node);
  return null;
}
function domIsVisible(node) {
  var parentElement = Element.parentElement(node);
  return node.style.display != "none" && (!parentElement || domIsVisible(parentElement));
}
function domFocus(node) {
  setTimeout(function () {
    node.focus();
    if (!ie) document.activeElement = node;
  }, 50);
}
function domBlur(node) {
  node.blur();
  if (!ie) document.activeElement = null;
}
function domIsFocusable(node) {
  switch (node.tagName) {
    case "INPUT":
      if (node.type == "hidden") return false;
    case "TEXTAREA":
    case "SELECT":
      return !node.disabled;
  }
  return false;
}
function domHasFocus(node) {
  return document.activeElement == node;
}
function domHideNode(node) {
  node.style.display = "none";
}
function domShowNode(node) {
  node.style.display = "";
}
function domGroupInputs(input) {
  if (input.name && input.form) {
    input = input.form[input.name];
    if (!input.nodeType) return input;
  }
  return [input];
}
function domGetCheckables(node) {
  var pendingInputs = [];
  if (ie) {
    var inputs = node.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
      var input = inputs[i];
      if (input.type == "radio" || input.type == "checkbox")
        pendingInputs.push([input, input.checked]);
    }
  }
  return pendingInputs;
}
function domSetCheckables(inputs) {
  for (var i = 0; i < inputs.length; i++)
    inputs[i][0].checked = inputs[i][1];
}
function domMoveChildNodes(src, dest) {
  var pendingInputs = domGetCheckables(src);
  while (src.firstChild) {
    dest.appendChild(src.firstChild);
  }
  domSetCheckables(pendingInputs);
}
function jsToDom(tagName, attrs, parent) {
  var doc;
  if (!parent) doc = document;
  else if (parent.nodeType == 9) {
    doc = parent;
    parent = null;
  }
  else doc = parent.ownerDocument;
  var node = doc.createElement(tagName);
  for (attr in attrs) {
    if (typeof attrs[attr] == "function") continue;
    node.setAttribute(attr, attrs[attr]);
  }
  if (parent) parent.appendChild(node);
  return node;
}
function domSheet(node) {
  if (node.styleSheet) return node.styleSheet;
  return node.sheet;
}
function domInsertRule(sheet, selector, declarations) {
  if (sheet.addRule) sheet.addRule(selector, declarations);
  else sheet.insertRule(selector + "{" + declarations + "}", sheet.cssRules.length);
}
function domAddOption(select, label, value) {
  var option = jsToDom("option", {value: value != null? value: label});
  setInnerText(option, label);
  select.appendChild(option);
}
function toogleShowHide(node) {
  if (node.style.display == "none") domShowNode(node);
  else domHideNode(node);
}
function getInnerText(node) {
  if (node.innerText != null) return node.innerText;
  var r = node.ownerDocument.createRange();
  r.selectNodeContents(node);
  return r.toString();
}
function setInnerText(node, text) {
  if (node.innerText != null) node.innerText = text;
  else node.innerHTML = convertTextToHTML(text);
}
function convertTextToHTML(s) {
  s = new String(s).replace(/\&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\n/g, "<BR>");
  while (/\s\s/.test(s)) s = s.replace(/\s\s/, "&nbsp; ");
  return s.replace(/\s/g, " ");
}
function domNodeIndex(node) {
  var index = 0;
  while (node.previousSibling) {
    node = node.previousSibling;
    index++;
  }
  return index;
}
function domInsertAt(node, parent, index) {
  var nodeRef = parent.firstChild;
  for (var i = 0; i < index; i++)
    nodeRef = nodeRef.nextSibling;
  if (nodeRef) parent.insertBefore(node, nodeRef);
  else parent.appendChild(node);
}
function domOpenPopup(element) {
  element = $(element);
  var body = document.body;
  new Insertion.Bottom(body, "<table style='width: 100%; height: 100%; position: absolute; top: 0'><tr><td align='center'><table><tr><td></td></tr></table></td></tr></table>");
  var popup = body.lastChild;
  var table = Element.firstElement(popup.rows[0].cells[0]);
  var node = table.rows[0].cells[0];
  new Draggable(table);
  table.style.cursor = "move";
  popup.container = element;
  element.popup = popup;
  popup.selects = [];
  popup.showPopup = function () {
    popup.selects = [];
    if (ie) {
      var selects = document.getElementsByTagName("select");
      for (var i = 0; i < selects.length; i++)
        if (selects[i].style.visibility != "hidden") {
          selects[i].style.visibility = "hidden";
          popup.selects.push(selects[i]);
        }
    }
    body.appendChild(this);
    domSetCheckables(popup.whatwgInputs);
    this.style.display = "";
  };
  popup.hidePopup = function () {
    popup.whatwgInputs = domGetCheckables(popup);
    this.style.display = "none";
    //domRemoveNode(this);
    for (var i = 0; i < this.selects.length; i++) this.selects[i].style.visibility = "";
  };
  popup.isVisible = function () {
    return this.parentNode != null;
  };
  node.appendChild(element);
  Element.show(element);
  return popup;
}
Element.setVisible = function (node, visible) {
  Element[visible? 'show': 'hide'](node);
}
Event.addListener = function (element, name, observer, useCapture) {
  element = $(element);
  if (name == "change" && (element.type == "radio" || element.type == "checkbox")) name = "click";
  Event.observe(element, name, observer.bind(element), useCapture);
}
Element.followingElement = function (element, index) {
  if (index == null) index = 0;
  for (var i = 0; i <= index; i++)
    do {
      element = element.followingSibling;
      if (!element) return null;
    } while (element.nodeType != 1);
  return element;
}
Element.previousElement = function (element, index) {
  if (index == null) index = 0;
  for (var i = 0; i <= index; i++)
    do {
      element = element.previousSibling;
      if (!element) return null;
    } while (element.nodeType != 1);
  return element;
}
Element.getParentPopup = function (node) {
  while (!node.showPopup && node.parentNode) node = node.parentNode;
  return node.showPopup? node: null;
}
Element.children = function (node) {
  var children = [];
  for (var i = 0; i < node.childNodes.length; i++)
    if (node.childNodes[i].nodeType == 1) children.push(node.childNodes[i]);
  return children;
}
Element.parentElement = function (node) {
  if (node.parentNode && node.parentNode.nodeType == 1) return node.parentNode;
  return null;
}
Element.firstElement = function (node) {
  for (var i = 0; i < node.childNodes.length; i++)
    if (node.childNodes[i].nodeType == 1) return node.childNodes[i];
  return null;
}
Element.lastElement = function (node) {
  for (var i = node.childNodes.length - 1; i >= 0; i--)
    if (node.childNodes[i].nodeType == 1) return node.childNodes[i];
  return null;
}
Element.childAt = function (node, index) {
  var pos = 0;
  for (var i = 0; i < node.childNodes.length; i++)
    if (node.childNodes[i].nodeType == 1) {
      if (pos == index) return node.childNodes[i];
      pos++;
    }
  return null;
}
Position.scrollOffset = function() {
  var x, y;
  if (self.pageYOffset) // all except Explorer
  {
    x = self.pageXOffset;
    y = self.pageYOffset;
  }
  else if (document.documentElement && document.documentElement.scrollTop)
    // Explorer 6 Strict
  {
    x = document.documentElement.scrollLeft;
    y = document.documentElement.scrollTop;
  }
  else if (document.body) // all other Explorers
  {
    x = document.body.scrollLeft;
    y = document.body.scrollTop;
  }
  return [x, y];
  /*var element = Element.firstElement(document.body);
  var cumulativePosition = Position.cumulativeOffset(element);
  var pagePosition = Position.page(element);
  return [cumulativePosition[0] - pagePosition[0], cumulativePosition[1] - pagePosition[1]];*/
}
Position.clientSize = function () {
  var x,y;
  if (self.innerHeight) // all except Explorer
  {
    x = self.innerWidth;
    y = self.innerHeight;
  }
  else if (document.documentElement && document.documentElement.clientHeight)
    // Explorer 6 Strict Mode
  {
    x = document.documentElement.clientWidth;
    y = document.documentElement.clientHeight;
  }
  else if (document.body) // other Explorers
  {
    x = document.body.clientWidth;
    y = document.body.clientHeight;
  }
  return [x, y];
}
Position.pageSize = function () {
  var x,y;
  var test1 = document.body.scrollHeight;
  var test2 = document.body.offsetHeight
  if (test1 > test2) // all but Explorer Mac
  {
    x = document.body.scrollWidth;
    y = document.body.scrollHeight;
  }
  else // Explorer Mac;
      //would also work in Explorer 6 Strict, Mozilla and Safari
  {
    x = document.body.offsetWidth;
    y = document.body.offsetHeight;
  }
  return [x, y];
}