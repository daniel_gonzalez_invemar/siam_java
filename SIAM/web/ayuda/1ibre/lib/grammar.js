function Grammar(productions) {
  this.testingProducctions = [];
  this.productions = productions;
  for (var production in productions) {
    if (typeof productions[production] == "function") continue;
    this.initProduction = productions[production];
    break;
  }
}
Grammar.prototype.compile = function(expresion) {
  this.pointer = 0;
  var context = {expresion: expresion, grammar: this, pointer: 0};
  var result = this.test(this.initProduction, context);
  if (result && expresion.length == context.pointer)
    return result;
  return false;
};
Grammar.prototype.test = function(production, context) {
  var pointer = context.pointer;
  var result = this.testBlock(production[0], context);
  if (result) {
    if (production[2]) result = production[2](result);
    if (production[1])
      return new Proccess(result, production[1], context.expresion.substring(pointer, context.pointer));
    return result;
  }
  return false;
};
Grammar.prototype.testBlock = function(items, context) {
  var pointer = context.pointer;
  var results = [];
  var result;
  for (var i = 0; i < items.length; i++) {
    var item = items[i];
    if (this.pointer <= context.pointer) {
      this.pointer = context.pointer;
      this.production = item;
    }
    result = item.compile(context);
    if (!result) {
      context.pointer = pointer;
      return false;
    }
    if (result !== true) results.push(result);
  }
  if (!results.length) return true;
  if (results.length == 1) return result;
  return results;
};
function Proccess(data, parser, text) {
  this.data = data;
  this.parser = parser;
  this.text = text;
}
Proccess.prototype.run = function (param) {
  return this.parser(param, this.data);
};
function ReferenceItem(name) {
  this.name = name;
}
ReferenceItem.prototype.compile = function (context) {
  var oldPointer = context.grammar.testingProducctions[this.name];
  if (oldPointer == context.pointer) {
    throw "Recursive exception in production '" + this.name + "'";
    return false;
  }
  context.grammar.testingProducctions[this.name] = context.pointer;
  var production = context.grammar.productions[this.name];
  if (!production) {
    throw "Production '" + this.name + "' don't exists";
    return false;
  }
  var result = context.grammar.test(production, context);
  context.grammar.testingProducctions[this.name] = oldPointer;
  return result;
};
function ZeroOrMoreItem(items) {
  this.items = items;
}
ZeroOrMoreItem.prototype.compile = function (context) {
  var results = [];
  var result;
  while (true) {
    result = context.grammar.testBlock(this.items, context);
    if (!result) return results;
    results.push(result);
  }
};
function OneOrMoreItem(items) {
  this.items = items;
}
OneOrMoreItem.prototype.compile = function (context) {
  var results = [];
  var result = context.grammar.testBlock(this.items, context);
  if (!result) return false;
  while (true) {
    results.push(result);
    result = context.grammar.testBlock(this.items, context);
    if (!result) return results;
  }
};
function MinToMaxItem(min, max, items) {
  this.min = min;
  this.max = max;
  this.items = items;
}
MinToMaxItem.prototype.compile = function (context) {
  var results = [];
  var result;
  var i;
  for (i = 0; i < this.min; i++) {
    result = context.grammar.testBlock(this.items, context);
    if (!result) return false;
    results.push(result);
  }
  for (; i < this.max; i++) {
    result = context.grammar.testBlock(this.items, context);
    if (!result) return results;
    results.push(result);
  }
  return results;
};
function MinItem(min, items) {
  this.min = min;
  this.items = items;
}
MinItem.prototype.compile = function (context) {
  var results = [];
  var result;
  for (var i = 0; i < this.min; i++) {
    result = context.grammar.testBlock(this.items, context);
    if (!result) return false;
    results.push(result);
  }
  while (true) {
    result = context.grammar.testBlock(this.items, context);
    if (!result) return results;
    results.push(result);
  }
};
function MaxItem(max, items) {
  this.max = max;
  this.items = items;
}
MaxItem.prototype.compile = function (context) {
  var results = [];
  var result;
  for (var i = 0; i < this.max; i++) {
    result = context.grammar.testBlock(this.items, context);
    if (!result) return results;
    results.push(result);
  }
  return results;
};
function TimesItem(times, items) {
  this.times = times;
  this.items = items;
}
TimesItem.prototype.compile = function (context) {
  var results = [];
  var result;
  for (var i = 0; i < this.times; i++) {
    result = context.grammar.testBlock(this.items, context);
    if (!result) return false;
    results.push(result);
  }
  return results;
};
function OneToMaxItem(max, items) {
  this.max = max;
  this.items = items;
}
OneToMaxItem.prototype.compile = function (context) {
  var results = [];
  var result = context.grammar.testBlock(this.items, context);
  if (!result) return false;
  results.push(result);
  for (var i = 1; i < this.max; i++) {
    result = context.grammar.testBlock(this.items, context);
    if (!result) return results;
    results.push(result);
  }
  return results;
};
function StringItem(text) {
  this.text = text;
}
StringItem.prototype.compile = function (context) {
  if (this.text == context.expresion.substr(context.pointer, this.text.length)) {
    context.pointer += this.text.length;
    return true;
  }
  return false;
};
function StringWSItem(text) {
  this.text = text;
}
StringWSItem.prototype.compile = function (context) {
  var i = context.pointer;
  while (context.expresion.charAt(i).isWS()) i++;
  if (this.text == context.expresion.substr(i, this.text.length)) {
    context.pointer = i + this.text.length;
    return true;
  }
  return false;
};
function OrItem(items) {
  this.items = items;
}
OrItem.prototype.compile = function (context) {
  var result;
  for (var i = 0; i < this.items.length; i++) {
    result = this.items[i].compile(context);
    if (result) return {data: result, index: i};
  }
  return false;
};
function OptionalItem(items) {
  this.items = items;
}
OptionalItem.prototype.compile = function (context) {
  return {data: context.grammar.testBlock(this.items, context)};
};
function ListItem(items) {
  this.items = items;
}
ListItem.prototype.compile = function (context) {
  return context.grammar.testBlock(this.items, context);
};
function OneCharItem(expr) {
  this.regex = new RegExp("^[" + expr + "]$");
}
OneCharItem.prototype.compile = function (context) {
  var ch = context.expresion.charAt(context.pointer);
  if (this.regex.test(ch)) {
    context.pointer++;
    return ch;
  }
  return false;
};
function OneCharItemWS(expr) {
  this.regex = new RegExp("^[" + expr + "]$");
}
OneCharItemWS.prototype.compile = function (context) {
  var i = context.pointer;
  while (context.expresion.charAt(i).isWS()) i++;
  var ch = context.expresion.charAt(i);
  if (this.regex.test(ch)) {
    context.pointer = i + 1;
    return ch;
  }
  return false;
};
function WSItem() { }
WSItem.prototype.compile = function (context) {
  while (context.expresion.charAt(context.pointer).isWS()) context.pointer++;
  return true;
};
function NoWSItem() { }
NoWSItem.prototype.compile = function (context) {
  return !context.expresion.charAt(context.pointer).isWS();
};
function AlertItem(text) {
  this.text = text;
}
AlertItem.prototype.compile = function (context) {
  alert(this.text + ": " + context.pointer + " '" + context.expresion.charAt(context.pointer) + "'");
  return true;
};
function DebugItem() { }
DebugItem.prototype.compile = function (context) {
  debugger;
  return true;
};
function TryItem(items) {
  this.items = items;
}
TryItem.prototype.compile = function (context) {
  try {alert("begin");
    return context.grammar.testBlock(this.items, context);
  }
  catch (e) {
    alert("Error: " + e);
    return false;
  }
};
