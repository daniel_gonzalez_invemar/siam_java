function xsltLoad() {
  var xpathVariables = [];
  var xslTagParses = {
    "XSL:VALUE-OF": function (node, context) {
      var sequence = xslXPathParse(node.getAttribute("select"), context);
      domInsertBefore(sequence.toDOM(), node);
      domRemoveNode(node);
    },
    "XSL:FOR-EACH": function (node, context) {
      var fragment = document.createDocumentFragment();
      domMoveChildNodes(node, fragment);
      var sequence = xslXPathParse(node.getAttribute("select"), context);
      var items = sequence.items;
      var length = items.length - 1;
      for (var i = 0; i <= length; i++) {
        var content = i < length? fragment.cloneNode(true): fragment;
        domInsertBefore(content, node);
        xsltParse(content, items[i]);
      }
      domRemoveNode(node);
    },
    "XSL:VARIABLE": function (node, context) {
      domRemoveNode(node);
      var select = node.getAttribute("select");
      var sequence;
      if (select == null) {
        var fragment = document.createDocumentFragment();
        domMoveChildNodes(node, fragment);
        xsltParse(fragment, context);
        sequence = new Sequence();
        sequence.add(fragment);
      }
      else sequence = xslXPathParse(select, context);
      xpathVariables[node.getAttribute("name")] = sequence;
    }
  };
  function xsltParse(node, context) {
    var list = node.childNodes;
    for (var i = 0; i < list.length; i++) {
      var current = list[i];
      if (current.nodeType == 1) {
        var tagName = current.tagName;
        if (xslTagParses[tagName]) {
          try {
          //alert(current.parentNode.innerHTML);
            xslTagParses[tagName](current, context);
          }
          catch (e) {
            throw "Error: " + e + "\nIn element " + tagName;
          }
        }
        else {
          xsltParse(current, context);
          /*if (tagName.startsWith("HTML:")) {
            var tag = document.createElement(tagName.substr(5));
            current.parentNode.replaceChild(tag, current);
            domMoveChildNodes(current, tag);
            for (var j = 0; j < current.attributes.length; j++)
              tag.setAttribute(current.attributes[j].nodeName, current.attributes[j].nodeValue);
          }*/
        }
      }
    }
  }
  try {
    xsltParse(document, document);
  }
  catch (e) {
    alert(e);
  }
  alert(document.documentElement.innerHTML);
}
domAddEventListener(window, "load", xsltLoad);
function xslXPathParse(expresion, context) {
  var expr = new XPath(expresion);
  return expr.eval(context);
}
function XPath(expresion) {
  this.parser = xpathGrammar.compile(expresion);
  if (!this.parser) throw "Invalid XPath expresion '" + expresion + "'";
}
XPath.prototype.eval = function(context) {
  return this.parser.run(context);
};
function Sequence() {
  this.items = [];
  for (var i = 0; i < Sequence.arguments.length; i++)
    this.add(Sequence.arguments[i]);
}
Sequence.prototype.add = function(item) {
  this.items.push(item);
};
Sequence.prototype.toDOM = function() {
  if (!this.items.length) return document.createTextNode("");
  if (this.items.length == 1) return this.itemToDOM(0);
  var fragment = document.createDocumentFragment();
  for (var i = 0; i < this.items.length; i++)
    fragment.appendChild(this.itemToDOM(i));
};
Sequence.prototype.itemToDOM = function(index) {
  if (this.items[index].nodeType) return this.items[index];
  return document.createTextNode(this.items[index]);
};
var xpathGrammar = new Grammar({
  Expr: [
    [
      new ReferenceItem("ExprSingle"),
      new ZeroOrMoreItem([
        new StringItem(","),
        new ReferenceItem("ExprSingle")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      for (var i = 0; i < data[1].length; i++)
        sequence.add(data[1][i].run(context));
      return sequence;
    }
  ],
  ExprSingle: [
    [
      new OrItem([
        new ReferenceItem("ForExpr"),
        new ReferenceItem("QuantifiedExpr"),
        new ReferenceItem("IfExpr"),
        new ReferenceItem("OrExpr")
      ])
    ],
    function (context, data) {
      return data[0].data.run(context);
    }
  ],
  ForExpr: [
    [
      new ReferenceItem("SimpleForClause"),
      new StringItem("return"),
      new ReferenceItem("ExprSingle")
    ],
    function (context, data) {
      //return ;
    }
  ],
  SimpleForClause: [
    [
      new StringItem("for"),
      new StringItem("$"),
      new ReferenceItem("VarName"),
      new StringItem("in"),
      new ReferenceItem("ExprSingle"),
      new ZeroOrMoreItem([
        new StringItem(","),
        new StringItem("$"),
        new ReferenceItem("VarName"),
        new StringItem("in"),
        new ReferenceItem("ExprSingle")
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  QuantifiedExpr: [
    [
      new OrItem([
        new StringItem("some"),
        new StringItem("every")
      ]),
      new StringItem("$"),
      new ReferenceItem("VarName"),
      new StringItem("in"),
      new ReferenceItem("ExprSingle"),
      new ZeroOrMoreItem([
        new StringItem(","),
        new StringItem("$"),
        new ReferenceItem("VarName"),
        new StringItem("in"),
        new ReferenceItem("ExprSingle")
      ]),
      new StringItem("satisfies"),
      new ReferenceItem("ExprSingle")
    ],
    function (context, data) {
      //return ;
    }
  ],
  IfExpr: [
    [
      new StringItem("if"),
      new StringItem("("),
      new ReferenceItem("Expr"),
      new StringItem(")"),
      new StringItem("then"),
      new ReferenceItem("ExprSingle"),
      new StringItem("else"),
      new ReferenceItem("ExprSingle")
    ],
    function (context, data) {
      //return ;
    }
  ],
  OrExpr: [
    [
      new ReferenceItem("AndExpr"),
      new ZeroOrMoreItem([
        new StringItem("or"),
        new ReferenceItem("AndExpr")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].length) return sequence;
      if (sequence.toBoolean()) return new Sequence(true);
      for (var i = 0; i < data[1].length; i++)
        if (data[1][i][1].run(context).toBoolean())
          return new Sequence(true);
      return new Sequence(false);
    }
  ],
  AndExpr: [
    [
      new ReferenceItem("ComparisonExpr"),
      new ZeroOrMoreItem([
        new StringItem("and"),
        new ReferenceItem("ComparisonExpr")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].length) return sequence;
      if (!sequence.toBoolean()) return new Sequence(false);
      for (var i = 0; i < data[1].length; i++)
        if (!data[1][i][1].run(context).toBoolean())
          return new Sequence(false);
      return new Sequence(true);
    }
  ],
  ComparisonExpr: [
    [
      new ReferenceItem("RangeExpr"),
      new OptionalItem([
        new OrItem([
          new ReferenceItem("ValueComp"),
          new ReferenceItem("GeneralComp"),
          new ReferenceItem("NodeComp")
        ]),
        new ReferenceItem("RangeExpr")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].data) return sequence;
    }
  ],
  RangeExpr: [
    [
      new ReferenceItem("AdditiveExpr"),
      new OptionalItem([
        new StringItem("to"),
        new ReferenceItem("AdditiveExpr")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].data) return sequence;
    }
  ],
  AdditiveExpr: [
    [
      new ReferenceItem("MultiplicativeExpr"),
      new ZeroOrMoreItem([
        new OrItem([
          new StringItem("+"),
          new StringItem("-")
        ]),
        new ReferenceItem("MultiplicativeExpr")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].length) return sequence;
    }
  ],
  MultiplicativeExpr: [
    [
      new ReferenceItem("UnionExpr"),
      new ZeroOrMoreItem([
        new OrItem([
          new StringItem("*"),
          new StringItem("div"),
          new StringItem("idiv"),
          new StringItem("mod")
        ]),
        new ReferenceItem("UnionExpr")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].length) return sequence;
    }
  ],
  UnionExpr: [
    [
      new ReferenceItem("IntersectExceptExpr"),
      new ZeroOrMoreItem([
        new OrItem([
          new StringItem("union"),
          new StringItem("|")
        ]),
        new ReferenceItem("IntersectExceptExpr")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].length) return sequence;
    }
  ],
  IntersectExceptExpr: [
    [
      new ReferenceItem("InstanceofExpr"),
      new ZeroOrMoreItem([
        new OrItem([
          new StringItem("intersect"),
          new StringItem("except")
        ]),
        new ReferenceItem("InstanceofExpr")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].length) return sequence;
    }
  ],
  InstanceofExpr: [
    [
      new ReferenceItem("TreatExpr"),
      new OptionalItem([
        new StringItem("instance"),
        new StringItem("of"),
        new ReferenceItem("SequenceType")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].data) return sequence;
    }
  ],
  TreatExpr: [
    [
      new ReferenceItem("CastableExpr"),
      new OptionalItem([
        new StringItem("treat"),
        new StringItem("as"),
        new ReferenceItem("SequenceType")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].data) return sequence;
    }
  ],
  CastableExpr: [
    [
      new ReferenceItem("CastExpr"),
      new OptionalItem([
        new StringItem("castable"),
        new StringItem("as"),
        new ReferenceItem("SingleType")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].data) return sequence;
    }
  ],
  CastExpr: [
    [
      new ReferenceItem("UnaryExpr"),
      new OptionalItem([
        new StringItem("cast"),
        new StringItem("as"),
        new ReferenceItem("SingleType")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].data) return sequence;
    }
  ],
  UnaryExpr: [
    [
      new ZeroOrMoreItem([
        new OrItem([
          new StringItem("-"),
          new StringItem("+")
        ])
      ]),
      new ReferenceItem("ValueExpr")
    ],
    function (context, data) {
      var sequence = data[1].run(context);
      if (!data[0].length) return sequence;
    }
  ],
  ValueExpr: [
    [
      new ReferenceItem("PathExpr")
    ],
    function (context, data) {
      return data[0].run(context);
    }
  ],
  GeneralComp: [
    [
      new OrItem([
        new StringItem("="),
        new StringItem("!="),
        new StringItem("<"),
        new StringItem("<="),
        new StringItem(">"),
        new StringItem(">=")
      ])
    ],
    function (context, data) {
      return data[0].index;
    }
  ],
  ValueComp: [
    [
      new OrItem([
        new StringItem("eq"),
        new StringItem("ne"),
        new StringItem("lt"),
        new StringItem("le"),
        new StringItem("gt"),
        new StringItem("ge")
      ])
    ],
    function (context, data) {
      return data[0].index;
    }
  ],
  NodeComp: [
    [
      new OrItem([
        new StringItem("is"),
        new StringItem("<<"),
        new StringItem(">>")
      ])
    ],
    function (context, data) {
      return data[0].index;
    }
  ],
  PathExpr: [
    [
      new OrItem([
        new ListItem([
          new StringItem("/"),
          new OptionalItem([new ReferenceItem("RelativePathExpr")])
        ]),
        new ListItem([
          new StringItem("//"),
          new ReferenceItem("RelativePathExpr")
        ]),
        new ReferenceItem("RelativePathExpr")
      ])
    ],
    function (context, data) {
      switch (data[0].index) {
        case 0:
                break;
        case 1:
                break;
        default:
          return data[0].data.run(context);
      }
    }
  ],
  RelativePathExpr: [
    [
      new ReferenceItem("StepExpr"),
      new ZeroOrMoreItem([
        new OrItem([
          new StringItem("/"),
          new StringItem("//")
        ]),
        new ReferenceItem("StepExpr")
      ])
    ],
    function (context, data) {
      var sequence = data[0].run(context);
      if (!data[1].length) return sequence;
    }
  ],
  StepExpr: [
    [
      new OrItem([
        new ReferenceItem("FilterExpr"),
        new ReferenceItem("AxisStep")
      ])
    ],
    function (context, data) {
      return data[0].data.run(context);
    }
  ],
  AxisStep: [
    [
      new OrItem([
        new ReferenceItem("ReverseStep"),
        new ReferenceItem("ForwardStep")
      ]),
      new ReferenceItem("PredicateList")
    ],
    function (context, data) {
      //return ;
    }
  ],
  ForwardStep: [
    [
      new OrItem([
        new ListItem([
          new ReferenceItem("ForwardAxis"),
          new ReferenceItem("NodeTest")
        ]),
        new ReferenceItem("AbbrevForwardStep")
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  ForwardAxis: [
    [
      new OrItem([
        new ListItem([
          new StringItem("child"),
          new StringItem("::")
        ]),
        new ListItem([
          new StringItem("descendant"),
          new StringItem("::")
        ]),
        new ListItem([
          new StringItem("attribute"),
          new StringItem("::")
        ]),
        new ListItem([
          new StringItem("self"),
          new StringItem("::")
        ]),
        new ListItem([
          new StringItem("descendant-or-self"),
          new StringItem("::")
        ]),
        new ListItem([
          new StringItem("following-sibling"),
          new StringItem("::")
        ]),
        new ListItem([
          new StringItem("following"),
          new StringItem("::")
        ]),
        new ListItem([
          new StringItem("namespace"),
          new StringItem("::")
        ])
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  AbbrevForwardStep: [
    [
      new OptionalItem([
        new StringItem("@")
      ]),
      new ReferenceItem("NodeTest")
    ],
    function (context, data) {
      //return ;
    }
  ],
  ReverseStep: [
    [
      new OrItem([
        new ListItem([
          new ReferenceItem("ReverseAxis"),
          new ReferenceItem("NodeTest")
        ]),
        new ReferenceItem("AbbrevReverseStep")
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  ReverseAxis: [
    [
      new OrItem([
        new ListItem([
          new StringItem("parent"),
          new StringItem("::")
        ]),
        new ListItem([
          new StringItem("ancestor"),
          new StringItem("::")
        ]),
        new ListItem([
          new StringItem("preceding-sibling"),
          new StringItem("::")
        ]),
        new ListItem([
          new StringItem("preceding"),
          new StringItem("::")
        ]),
        new ListItem([
          new StringItem("ancestor-or-self"),
          new StringItem("::")
        ])
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  ReverseStep: [
    [
      new StringItem("..")
    ],
    function (context, data) {
      //return ;
    }
  ],
  NodeTest: [
    [
      new OrItem([
        new ReferenceItem("KindTest"),
        new ReferenceItem("NameTest")
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  NameTest: [
    [
      new OrItem([
        new ReferenceItem("QName"),
        new ReferenceItem("Wildcard")
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  Wildcard: [
    [
      new OrItem([
        new StringItem("*"),
        new ListItem([
          new ReferenceItem("NCName"),
          new NoWSItem(),
          new StringItem(":*")
        ]),
        new ListItem([
          new StringItem("*:"),
          new NoWSItem(),
          new ReferenceItem("NCName")
        ])
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  FilterExpr: [
    [
      new ReferenceItem("PrimaryExpr"),
      new ReferenceItem("PredicateList")
    ],
    function (context, data) {
      return data[1].run(data[0].run(context))
    }
  ],
  PredicateList: [
    [
      new ZeroOrMoreItem([
        new ReferenceItem("Predicate")
      ])
    ],
    function (context, data) {
      if (!data[0].length) return context;
    }
  ],
  Predicate: [
    [
      new StringItem("["),
      new ReferenceItem("Expr"),
      new StringItem("]")
    ],
    function (context, data) {
      //return ;
    }
  ],
  PrimaryExpr: [
    [
      new OrItem([
        new ReferenceItem("Literal"),
        new ReferenceItem("VarRef"),
        new ReferenceItem("ParenthesizedExpr"),
        new ReferenceItem("ContextItemExpr"),
        new ReferenceItem("FunctionCall")
      ])
    ],
    function (context, data) {
      return data[0].data.run(context);
    }
  ],
  Literal: [
    [
      new OrItem([
        new ReferenceItem("NumericLiteral"),
        new ReferenceItem("StringLiteral")
      ])
    ],
    function (context, data) {
      return data[0].data.run();
    }
  ],
  NumericLiteral: [
    [
      new OrItem([
        new ReferenceItem("IntegerLiteral"),
        new ReferenceItem("DecimalLiteral"),
        new ReferenceItem("DoubleLiteral")
      ])
    ],
    function (context, data) {
      return data[0].data.run();
    }
  ],
  VarRef: [
    [
      new StringItem("$"),
      new ReferenceItem("VarName")
    ],
    function (context, data) {
      //return ;
    }
  ],
  VarName: [
    [
      new ReferenceItem("QName")
    ],
    function (context, data) {
      //return ;
    }
  ],
  ParenthesizedExpr: [
    [
      new StringItem("("),
      new OptionalItem([
        new ReferenceItem("Expr")
      ]),
      new StringItem(")")
    ],
    function (context, data) {
      if (data[1].data) return data[1].data[0].run(context);
      return new Sequence();
    }
  ],
  ContextItemExpr: [
    [
      new StringItem(".")
    ],
    function (context, data) {
      //return ;
    }
  ],
  FunctionCall: [
    [
      new ReferenceItem("QName"),
      new StringItem("("),
      new OptionalItem([
        new ReferenceItem("ExprSingle"),
        new ZeroOrMoreItem([
          new StringItem(","),
          new ReferenceItem("ExprSingle")
        ])
      ]),
      new StringItem(")")
    ],
    function (context, data) {
      //return ;
    }
  ],
  SingleType: [
    [
      new ReferenceItem("AtomicType"),
      new OptionalItem([
        new StringItem("?")
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  SequenceType: [
    [
      new OrItem([
        new ListItem([
          new StringItem("empty-sequence"),
          new StringItem("("),
          new StringItem(")")
        ]),
        new ListItem([
          new ReferenceItem("ItemType"),
          new OptionalItem([
            new ReferenceItem("OccurrenceIndicator")
          ])
        ])
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  OccurrenceIndicator: [
    [
      new OrItem([
        new StringItem("?"),
        new StringItem("*"),
        new StringItem("+")
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  ItemType: [
    [
      new OrItem([
        new ReferenceItem("KindTest"),
        new ListItem([
          new StringItem("item"),
          new StringItem("("),
          new StringItem(")")
        ]),
        new ReferenceItem("AtomicType")
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  AtomicType: [
    [
      new ReferenceItem("QName")
    ],
    function (context, data) {
      //return ;
    }
  ],
  KindTest: [
    [
      new OrItem([
        new ReferenceItem("DocumentTest"),
        new ReferenceItem("ElementTest"),
        new ReferenceItem("AttributeTest"),
        new ReferenceItem("SchemaElementTest"),
        new ReferenceItem("SchemaAttributeTest"),
        new ReferenceItem("PITest"),
        new ReferenceItem("CommentTest"),
        new ReferenceItem("TextTest"),
        new ReferenceItem("AnyKindTest")
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  AnyKindTest: [
    [
      new StringItem("node"),
      new StringItem("("),
      new StringItem(")")
    ],
    function (context, data) {
      //return ;
    }
  ],
  DocumentTest: [
    [
      new StringItem("document-node"),
      new StringItem("("),
      new OptionalItem([
        new OrItem([
          new ReferenceItem("ElementTest"),
          new ReferenceItem("SchemaElementTest")
        ])
      ]),
      new StringItem(")")
    ],
    function (context, data) {
      //return ;
    }
  ],
  TextTest: [
    [
      new StringItem("text"),
      new StringItem("("),
      new StringItem(")")
    ],
    function (context, data) {
      //return ;
    }
  ],
  CommentTest: [
    [
      new StringItem("comment"),
      new StringItem("("),
      new StringItem(")")
    ],
    function (context, data) {
      //return ;
    }
  ],
  PITest: [
    [
      new StringItem("processing-instruction"),
      new StringItem("("),
      new OptionalItem([
        new OrItem([
          new ReferenceItem("NCName"),
          new ReferenceItem("StringLiteral")
        ])
      ]),
      new StringItem(")")
    ],
    function (context, data) {
      //return ;
    }
  ],
  AttributeTest: [
    [
      new StringItem("attribute"),
      new StringItem("("),
      new OptionalItem([
        new ReferenceItem("AttribNameOrWildcard"),
        new OptionalItem([
          new StringItem(","),
          new ReferenceItem("TypeName")
        ])
      ]),
      new StringItem(")")
    ],
    function (context, data) {
      //return ;
    }
  ],
  AttribNameOrWildcard: [
    [
      new OrItem([
        new ReferenceItem("AttributeName"),
        new StringItem("*")
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  SchemaAttributeTest: [
    [
      new StringItem("schema-attribute"),
      new StringItem("("),
      new ReferenceItem("AttributeDeclaration"),
      new StringItem(")")
    ],
    function (context, data) {
      //return ;
    }
  ],
  AttributeDeclaration: [
    [
      new ReferenceItem("AttributeName")
    ],
    function (context, data) {
      //return ;
    }
  ],
  ElementTest: [
    [
      new StringItem("element"),
      new StringItem("("),
      new OptionalItem([
        new ReferenceItem("ElementNameOrWildcard"),
        new OptionalItem([
          new StringItem(","),
          new ReferenceItem("TypeName"),
          new OptionalItem([
            new StringItem("?")
          ])
        ])
      ]),
      new StringItem(")")
    ],
    function (context, data) {
      //return ;
    }
  ],
  ElementNameOrWildcard: [
    [
      new OrItem([
        new ReferenceItem("ElementName"),
        new StringItem("*")
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  SchemaElementTest: [
    [
      new StringItem("schema-element"),
      new StringItem("("),
      new ReferenceItem("ElementDeclaration"),
      new StringItem(")")
    ],
    function (context, data) {
      //return ;
    }
  ],
  ElementDeclaration: [
    [
      new ReferenceItem("ElementName")
    ],
    function (context, data) {
      //return ;
    }
  ],
  AttributeName: [
    [
      new ReferenceItem("QName")
    ],
    function (context, data) {
      //return ;
    }
  ],
  ElementName: [
    [
      new ReferenceItem("QName")
    ],
    function (context, data) {
      //return ;
    }
  ],
  TypeName: [
    [
      new ReferenceItem("QName")
    ],
    function (context, data) {
      //return ;
    }
  ],
  IntegerLiteral: [
    [
      new ReferenceItem("Digits")
    ],
    function (context, data) {
      return new Sequence(parseInt(data[0].run()));
    }
  ],
  DecimalLiteral: [
    [
      new OrItem([
        new ListItem([
          new StringItem("."),
          new NoWSItem(),
          new ReferenceItem("Digits")
        ]),
        new ListItem([
          new ReferenceItem("Digits"),
          new NoWSItem(),
          new StringItem("."),
          new ZeroOrMoreItem([
            new NoWSItem(),
            new OneCharItem("0-9")
          ])
        ])
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  DoubleLiteral: [
    [
      new OrItem([
        new ListItem([
          new StringItem("."),
          new NoWSItem(),
          new ReferenceItem("Digits")
        ]),
        new ListItem([
          new ReferenceItem("Digits"),
          new OptionalItem([
            new NoWSItem(),
            new StringItem("."),
            new ZeroOrMoreItem([
              new NoWSItem(),
              new OneCharItem("0-9")
            ])
          ])
        ])
      ]),
      new NoWSItem(),
      new OneCharItem("eE"),
      new OptionalItem([
        new NoWSItem(),
        new OneCharItem("+-")
      ]),
      new NoWSItem(),
      new ReferenceItem("Digits")
    ],
    function (context, data) {
      //return ;
    }
  ],
  StringLiteral: [
    [
      new OrItem([
        new ListItem([
          new StringItem("\""),
          new ZeroOrMoreItem([
            new OrItem([
              new ReferenceItem("EscapeQuot"),
              new OneCharItem("^\"")
            ])
          ]),
          new StringItem("\"")
        ]),
        new ListItem([
          new StringItem("'"),
          new ZeroOrMoreItem([
            new OrItem([
              new ReferenceItem("EscapeApos"),
              new OneCharItem("^'")
            ])
          ]),
          new StringItem("'")
        ])
      ])
    ],
    function (context, data) {
      //return ;
    }
  ],
  EscapeQuot: [
    [
      new StringItem("\"\""),
    ],
    function (context, data) {
      //return ;
    }
  ],
  EscapeApos: [
    [
      new StringItem("''"),
    ],
    function (context, data) {
      //return ;
    }
  ],
  Digits: [
    [
      new OneCharItem("0-9"),
      new ZeroOrMoreItem([
        new NoWSItem(),
        new OneCharItem("0-9")
      ])
    ],
    function (context, data) {
      var s = data[0];
      for (var i = 0; i < data[1].length; i++) {
        s += data[1][i][1];
      }
      return s;
    }
  ],
  QName: [
    [
      new OptionalItem([
        new ReferenceItem("NCName"),
        new NoWSItem(),
        new StringItem(":"),
        new NoWSItem()
      ]),
      new ReferenceItem("NCName")
    ],
    function (context, data) {
      //return ;
    }
  ],
  NCName: [
    [
      new OneCharItem("a-zA-Z_.-"),
      new ZeroOrMoreItem([
        new NoWSItem(),
        new OneCharItem("a-zA-Z0-9_.-")
      ])
    ],
    function (context, data) {
      //return ;
    }
  ]
});
