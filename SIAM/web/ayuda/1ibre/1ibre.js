var ibre = {
    params: location.href.toQueryParams(),
    path: location.href.substring(0, location.href.lastIndexOf("/") + 1),
    errors: [],
    calendar: {},
    datasource: {
        nodes: [],
        namedNodes: {},
        values: {},
        _parsed: false,
        numLoaded: 0,
        loading: false,
        _numLoading: 0
    },
    parsed: false,
    namedNodes: {},
    _percentageDS: 36,
    _percentageTPL: 40,
    _percentageParse: 70,
    _percentageStep: 0.2,
    _makeEventHandler: function (node, name) {
        name = "on" + name;
        var js = node.getAttribute(name);
        if (js)
            eval("node." + name + " = function (event) { with (this) { " + js + " } }.bind(node);");
    },
    _callEventHandler: function (node, name) {
        name = "on" + name;
        if (node[name]) return node[name]({
            targetElement: node
        });
    },
    set: function (node, property, value) {
        switch (node.tagName) {
            case "INPUT":
                if (node.type != "image") break;
            case "IMG":
                if (node.rollover && property == "src") node.src = node.originalSrc = value;
                break;
        }
    }
};
var ie = /MSIE/.test(navigator.userAgent);
var moz = !ie && navigator.product == "Gecko";
function include(src) {
  
    //if (ie) {
    document.write("<script type='text/javascript' src='" + src + "'></script>");
/*}
  else {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = src;
    document.head.appendChild(script);
  //}*/
}
String.prototype.endsWith = function (str) {
    return this.substring(this.length - str.length) == str;
};
(function () {
    document.head = document.getElementsByTagName("head")[0];
    var basePath = "";
    //var scripts = document.getElementsByTagName("script");
    var scripts = document.head.childNodes;
    var commonFile = "1ibre.js";
    for (var i = 0; i < scripts.length; i++) {
        var src = scripts[i].src;
        if (src && src.endsWith(commonFile)) {
            basePath = src.substring(0, src.length - commonFile.length);
            break;
        }
    }
    ibre.basePath = basePath;
    //include(basePath + "prototype.js");
    //include(basePath + "scriptaculous/scriptaculous.js");
    //include(basePath + "ieemu/ieemu.js");
    //include(basePath + "sarissa/sarissa.js");
    //include(basePath + "mathcontext.js");
    //include(basePath + "bigdecimal.js");
    //include(basePath + "jscalendar/calendar.js");
    //include(basePath + "jscalendar/lang/calendar-es.js");
    //include(basePath + "jscalendar/calendar-setup.js");
    include(basePath + "lib/init.js");
    include(basePath + "lib/lang.js");
    include(basePath + "lib/dom.js");
    include(basePath + "lib/ajax.js");
    include(basePath + "lib/grammar.js");
    include(basePath + "json/json.js");
    //include(basePath + "xslt/xslt.js");
    include(basePath + "whatwg/whatwg.js");
    include(basePath + "whatwg/lang/en.js");
    include(basePath + "whatwg/parser.js");
    include(basePath + "whatwg/css.js");
    include(basePath + "whatwg/validate.js");
})();
