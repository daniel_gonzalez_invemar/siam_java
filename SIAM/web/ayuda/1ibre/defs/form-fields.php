<table style="border-collapse: collapse; border-color: #aaa" cellpadding="2" border="1">
  <?php
  foreach ($fields as $field_name => $field) {
    if ($skip_field == $field_name || $field["dinamic"] || $field["referenceMappedField"]) continue;
    $input_name = $prefix? $prefix."[$field_name]": $field_name;
    //$input_id = "_".str_replace("[", "_", str_replace("]", "_", $input_name));
    $input_id = uniqid("a");
    $attrs = "id='$input_id' name='$input_name'".($field["null"]? "": " required");
    $value = $obj[$field_name];
    $a = NULL;
    ?>
    <tr>
      <td align="right">
        <label for="<?php echo $input_id ?>"><?php echo $field["displayName"] ?></label>
      </td>
      <td>
        <?php
        switch ($field["type"]) {
          case "number":
          case "date":
          case "datetime":
            ?><input type="<?php echo $field["type"] ?>" <?php echo $attrs ?> value="<?php echo $value ?>"><?php
            break;
          case "string":
            if ($field["multiline"]) {
              ?><textarea <?php echo $attrs ?>><?php echo add_entities($value) ?></textarea><?php
            }
            elseif ($field["private"]) {
              ?>
              <input type="password" <?php echo $attrs ?> value="<?php echo add_entities($value) ?>">
              <input type="password" validate="value == form.<?php echo $input_name ?>.value" value="<?php echo add_entities($value) ?>">
              <?php
            }
            else {
              ?><input <?php echo $attrs ?> value="<?php echo add_entities($value) ?>"><?php
            }
            break;
          case "enum":
            ?>
              <select <?php echo $attrs ?>>
                <option value="" style="font-style: italic">
                  -<?php echo $field["null"]? "N/A": "Select" ?>-
                </option>
                <?php foreach ($field["values"] as $option) { ?>
                  <option value="<?php echo add_entities($option) ?>"<?php echo $value == $option? " selected": "" ?>>
                    <?php echo add_entities($option) ?>
                  </option>
                <?php } ?>
              </select>
            <?php
            break;
          case "reference":
            $referenced_type = $field["referencedType"];
            if ($field["dependent"]) {
              if (!is_array($value)) $value = ibre_get_field($obj, $field);
              $field_id = uniqid("_");
              ?>
              <div whatwg="details">
                <div whatwg="legend">Edit</div>
                <?php if ($field["null"]) { $is_null = $value || !$value && !$obj; ?>
                <input type="checkbox" name="<?php echo $input_name ?>" id="<?php echo $field_id ?>"<?php echo $is_null? " checked": "" ?> onclick="setNull(this)">
                <label for="<?php echo $field_id ?>">N/A</label>
                <?php } ?>
                <div<?php echo $field["null"] && $is_null? " style='display: none;'": "" ?>>
                <?php include_type($referenced_type, $input_name, $value); ?>
                </div>
              </div>
              <?php
            }
            else {
              if (is_array($value)) $value = $value["id"];
              //$main_field = $referenced_type["mainField"]["name"];
              $referenced_type_name = $referenced_type["name"];
              ?>
              <!--div whatwg="script">
                loadDataList("<?php echo $referenced_type_name ?>");
              </div-->
              <select <?php echo $attrs ?> onchange="addListItem('<?php echo $referenced_type_name ?>', this)">
                <option value="" style="font-style: italic">
                  -<?php echo $field["null"]? "N/A": "Select" ?>-
                </option>
                <?php foreach (ibre_get_objects($referenced_type) as $object) { ?>
                  <option value="<?php echo $object["id"] ?>"<?php echo $value == $object["id"]? " selected": "" ?>>
                    <?php echo add_entities(ibre_get_main_field_value($object))/*$main_field? add_entities($object[$main_field]): $object["id"]*/ ?>
                  </option>
                <?php } ?>
                <option value="" style="font-style: italic">-Add-</option>
              </select>
            <?php
            }
            break;
          case "boolean": ?>
            <input type="hidden" name="<?php echo $input_name ?>">
            <input type="checkbox" id="<?php echo $input_id ?>" name="<?php echo $input_name ?>"<?php echo $value? " checked": "" ?> value="1">
            <?php
            break;
          case "list":
          case "set":
            $element_type = $field["elementType"];
            $element_type_name = $element_type["name"];
            $objects = ibre_get_objects($element_type);
            //$main_field = $element_type["mainField"]["name"];
            $dependent = $field["dependent"];
            $mapped_field = $field["elementMappedField"]["name"];
            if ($obj && !$value) $value = ibre_get_field($obj, $field);
            ?>
            <div whatwg="details">
              <div whatwg="legend">Edit</div>
              <table>
                <tr>
                  <td colspan="2">
                    <input type="add" template="<?php echo $input_id ?>" onclick="addItem(this)">
                  </td>
                </tr>
                <?php if ($value) foreach ($value as $index => $item) { ?>
                  <tr repeat="<?php echo $index ?>">
                    <td>
                      <input type="hidden" name="<?php echo $input_name."[$index][id]" ?>" value="<?php echo $item["id"] ?>">
                      <div whatwg="details">
                        <div whatwg="legend">
                          <?php echo add_entities(ibre_get_main_field_value($item))/*$main_field? add_entities($item[$main_field]): "Edit $item[id]"*/ ?>
                        </div>
                        <?php if (!$dependent) { ?>
                          <select <?php echo "name='$input_name"."[$index]'" ?> onchange="addListItem('<?php echo $element_type_name ?>', this)">
                            <?php foreach ($objects as $object) { ?>
                              <option value="<?php echo $object["id"] ?>"<?php echo $item["id"] == $object["id"]? " selected": "" ?>>
                                <?php echo add_entities(ibre_get_main_field_value($object))/*$main_field? add_entities($object[$main_field]): $item["id"]*/ ?>
                              </option>
                            <?php } ?>
                            <option value="" style="font-style: italic">-Add-</option>
                          </select>
                        <?php } elseif (in_array(array($element_type_name, $item["id"]), $parsed_objects)) { ?>
                          <div>RECURSIVE "<?php echo add_entities(ibre_get_main_field_value($item))/*add_entities($item[$main_field])*/ ?>"</div>
                        <?php
                        }
                        else include_type($element_type, $input_name."[$index]", $item, $mapped_field);
                        ?>
                      </div>
                    </td>
                    <td>
                      <input type="remove">
                    </td>
                  </tr>
                <?php } ?>
                <tr id="<?php echo $input_id ?>" repeat="template">
                  <td>
                    <?php if (!$dependent) { ?>
                      <select <?php echo "name='$input_name"."[]'".($field["null"]? "": " required") ?> onchange="addListItem('<?php echo $element_type_name ?>', this)">
                        <option value="" style="font-style: italic">
                          -Select-
                        </option>
                        <?php foreach ($objects as $object) { ?>
                          <option value="<?php echo $object["id"] ?>">
                            <?php echo add_entities(ibre_get_main_field_value($object))/*$main_field? add_entities($object[$main_field]): $object["id"]*/ ?>
                          </option>
                        <?php } ?>
                        <option value="" style="font-style: italic">-Add-</option>
                      </select>
                    <?php } else { ?>
                      <div src="<?php echo "form.php?type=$element_type_name&amp;skipField=$mapped_field&amp;prefix=$input_name"."[[$input_id]]" ?>"></div>
                    <?php } ?>
                  </td>
                  <td>
                    <input type="remove">
                  </td>
                </tr>
              </table>
            </div>
        <?php } ?>
      </td>
    </tr>
  <?php } ?>
</table>
<?php
if ($obj) {
  foreach ($type["subtypes"] as $subtype)
    if (ibre_extends($obj["_TYPE"], $subtype)) {
      include_subtype($subtype, $subtype["fields"], $type_prefix, $prefix, $obj, $skip_field);
      break;
    }
} else { ?>
  <div whatwg="switch">
    <div id="<?php echo $type_prefix.($type["abstract"]/* || $type["dependent"]*/? "": $type["name"]) ?>"></div>
    <?php foreach ($type["subtypes"] as $subtype) { ?>
      <div>
        <!--div-->
          <?php include_subtype($subtype, $subtype["fields"], $type_prefix, $prefix, $obj, $skip_field) ?>
        <!--/div-->
      </div>
    <?php } ?>
  </div>
<?php } ?>
