<?php
require_once "xml.php";
$fields = get_fields();
$references = array();
foreach ($fields as $field)
  if ($field["dinamic"] || $field["type"] == "reference")
    $references[] = $field["name"];
$mapped_field = NULL;
if ($_GET["id"]) {
  $object_type = $_DEFS[$_GET["objectType"]];
  $mapped_field = $object_type["allFields"][$_GET["field"]]["elementMappedField"]["name"];
  $object = ibre_get_object_by_id($object_type, $_GET["id"], array($_GET["field"] => $references));
  $objects = $object[$_GET["field"]];
}
else $objects = ibre_get_objects($_GET["type"], "1", NULL, $references);
$objects = array_slice($objects, $_GET["begin"], 51);
?>
<data mainField="<?php echo $type["mainField"]["name"] ?>" mappedField="<?php echo $mapped_field ?>">
<?php
list_to_xml("object", $objects);
item_to_xml("field", $fields);
?>
</data>
