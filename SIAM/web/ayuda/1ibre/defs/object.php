<?php
require_once "../defs.php";
$object = ibre_get_object_by_id($_GET["type"], $_GET["id"]);
$type = ibre_get_type($object);
$fields = array();
foreach ($type["allFields"] as $field)
  $fields[] = array_merge($field, array(
    "dinamic" => (boolean)$field["dinamic"],
    "referencedType" => $field["referencedType"]["name"],
    "mainField" => $field["referencedType"]["mainField"]["name"],
    "elementType" => $field["elementType"]["name"],
    "keyType" => $field["keyType"]["name"],
    "valueType" => $field["valueType"]["name"],
    "referenceMappedField" => $field["referenceMappedField"]["name"],
    "elementMappedField" => $field["elementMappedField"]["name"],
    "valueMappedField" => $field["valueMappedField"]["name"],
    "keyMappedField" => $field["keyMappedField"]["name"],
    "ownerType" => NULL
  ));
$references = array();
foreach ($fields as $field)
  if ($field["type"] == "reference" || $field["dinamic"] && $field["type"] != "set" && $field["type"] != "list")
//  if ($field["dinamic"] || $field["type"] == "reference")
    $references[] = $field["name"];
ibre_get_fields($object, $references);
foreach ($fields as $field)
  if ($field["type"] == "reference" && $object[$field["name"]])
      $object[$field["name"]]["mainField"] = ibre_get_main_field_value($object[$field["name"]]);
json_send(array("object" => $object, "fields" => $fields));
?>
