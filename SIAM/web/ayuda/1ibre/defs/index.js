function showList(type, id, objectType, field) {
  var listNode = Element.childAt(document.getElementById("body"), 1);
  listNode.setActive();
  var div = Element.firstElement(listNode);
  setInnerText(Element.firstElement(div), types[type]);
  Element.firstElement(div).href = "javascript: showList('" + type + "')";
  domShowNode(Element.childAt(div, 1));
  Element.childAt(div, 1).href = "javascript: showNewForm('" + type + "')";
  loadList(Element.childAt(listNode, 1), type, id, objectType, field);
}
function showObject(id, type) {
  var objectNode = Element.childAt(document.getElementById("body"), 2);
  objectNode.setActive();
  var div = Element.firstElement(objectNode);
  setInnerText(Element.firstElement(div), types[type]);
  Element.firstElement(div).href = "javascript: showList('" + type + "')";
  Element.childAt(div, 1).href = "javascript: showNewForm('" + type + "')";
  Element.setVisible(Element.childAt(div, 1), !hiddenTypes[type]);
  loadObject(Element.childAt(objectNode, 1), id, type);
}
function showHiddenList(type) {
  var listNode = Element.childAt(document.getElementById("body"), 1);
  listNode.setActive();
  var div = Element.firstElement(listNode);
  setInnerText(Element.firstElement(div), types[type]);
  Element.firstElement(div).href = "javascript: showHiddenList('" + type + "')";
  domHideNode(Element.childAt(div, 1));
  loadList(Element.childAt(listNode, 1), type);
}
function loadList(div, type, id, objectType, fieldName, begin) {
  setInnerHTML(div, "Cargando...");
  requestText("view-list.php", function (text) {
    if (!begin) begin = 0;
    JSONRequest.get("list.php?type=" + type + "&begin=" + begin + (id? "&id=" + id + "&field=" + fieldName + "&objectType=" + objectType: ""), function (reqNum, obj, exception) {
      if (exception) {
        alert(exception.message);
        setInnerHTML(div, "No se pudo completar la petici\xF3n");
        alert(exception.native.responseText);
        return;
      }
      setInnerHTML(div, text);
      div.parentNode.scrollIntoView(true);
      var objects = obj.objects;
      if (!objects || !objects.length) {
        div.innerHTML = "No hay informaci&oacute;n para mostrar";
        div.style.fontStyle = "italic";
        return;
      }
      var table = Element.firstElement(div);
      var fieldNodes = table.tHead.rows[0].cells;
      var fieldTemplate = fieldNodes[fieldNodes.length - 1];
      //alert(fieldTemplate.outerHTML);
      fieldTemplate.removeRepetitionBlocks();
      var fields = obj.fields;
      if (fields) {
        for (var i = 0; i < fields.length; i++) {
          var field = fields[i];
          if (field.name == obj.mappedField) continue;
          var fieldNode = fieldTemplate.addRepetitionBlock(null);
          setInnerText(fieldNode, field.displayName);
          switch (field.type) {
            case "string":
              if (field.private) break;
            case "number":
              fieldNode.setAttribute("type", "number");
            case "reference":
            case "boolean":
            case "enum":
            case "date":
            case "datetime":
              fieldNode.setAttribute("class", "sortable");
          }
        }
      }
      var objectNodes = table.tBodies[0].rows;
      var objectTemplate = objectNodes[objectNodes.length - 1];
      objectTemplate.removeRepetitionBlocks();
      for (var i = 0; i < objects.length; i++) {
        if (i >= pageLength) continue;
        var block = objectTemplate.addRepetitionBlock(null);
        var valueNodes = block.cells;
        setInnerText(valueNodes[0], begin + i + 1);
        var object = objects[i];
        block.objectId = object.id;
        valueNodes[valueNodes.length - 3].setAttribute("href", "javascript: showObject('" + object.id + "', '" + type + "')");
        valueNodes[valueNodes.length - 2].setAttribute("href", "javascript: showEditForm('" + object.id + "', '" + type + "')");
        valueNodes[valueNodes.length - 1].onclick = function () {
          var block = this.parentNode;
          deleteObject(block.objectId, type, id, objectType, fieldName);
          return false;
        }
        var valueTemplate = valueNodes[valueNodes.length - 4];
        //var fields = obj.type.fields;
        if (fields) {
          for (var j = 0; j < fields.length; j++) {
            var field = fields[j];
            if (field.name == obj.mappedField) continue;
            var valueNode = valueTemplate.addRepetitionBlock(null);
            var value = object[field.name];
            switch (field.type) {
              case "reference":
                valueNode.align = "center";
                if (!value || typeof value != "object") {
                  valueNode.setAttribute("value", 0);
                  setInnerText(valueNode, "N/A");
                  valueNode.style.fontStyle = "italic";
                  break;
                }
                //var mainField = value[field.mainField];
                //if (mainField) valueNode.innerText = mainField;
                if (value.mainField) setInnerText(valueNode, value.mainField);
                else {
                  setInnerText(valueNode, "Ver");
                  valueNode.setAttribute("value", value.id);
                  valueNode.style.fontStyle = "italic";
                }
                valueNode.setAttribute("href", "javascript: showObject('" + value.id + "', '" + field.referencedType + "')");
                break;
              case "list":
              case "set":
                setInnerText(valueNode, "Listar");
                valueNode.align = "center";
                valueNode.setAttribute("href", "javascript: showList('" + field.elementType + "', '" + object.id + "', '" + type + "', '" + field.name + "')");
                break;
              case "number":
                valueNode.align = "right";
                valueNode.setAttribute("value", value);
                if (value === "") {
                  setInnerText(valueNode, "N/A");
                  valueNode.style.fontStyle = "italic";
                }
                else setInnerText(valueNode, numberFormat(value));
                break;
              case "boolean":
                valueNode.align = "center";
                setInnerText(valueNode, value && value != "0"? "S\xED": "No");
                break;
              case "string":
                if (field.private) {
                  setInnerText(valueNode, "**********");
                  break;
                }
              default:
                setInnerText(valueNode, value);
            }
          }
        }
      }
      if (begin > 0 || i > pageLength) {
        var foot = Element.childAt(div, 1);
        domShowNode(foot);
        if (begin > 0) {
          var link = Element.firstElement(foot);
          link.onclick = function () {
            loadList(div, type, id, objectType, fieldName, begin - pageLength);
          }
          domShowNode(link);
        }
        if (i > pageLength) {
          var link = Element.childAt(foot, 1);
          link.onclick = function () {
            loadList(div, type, id, objectType, fieldName, begin + pageLength);
          }
          domShowNode(link);
        }
      }
      wf2ParseHTML(div);
    });
  }, null, true);
}
function loadObject(div, id, type) {
  setInnerHTML(div, "Cargando...");
  requestText("view-object.php", function (text) {
    //requestJS("object.php?id=" + id + "&type=" + type, function (obj) {
    JSONRequest.get("object.php?id=" + id + "&type=" + type, function (reqNum, obj, exception) {
      if (exception) {
        alert(exception.message);
        setInnerHTML(div, "No se pudo completar la petici\xF3n");
        alert(exception.native.responseText);
        return;
      }
      setInnerHTML(div, text);
      div.parentNode.scrollIntoView(true);
      var children = Element.children(div);
      children[0].href = "javascript: showObject('" + id + "', '" + type + "')";
      children[1].href = "javascript: showEditForm('" + id + "', '" + type + "')";
      children[2].href = "javascript: deleteObject('" + id + "', '" + type + "')";
      var table = children[3];
      var fieldNodes = table.rows;
      var fieldTemplate = fieldNodes[fieldNodes.length - 1];
      fieldTemplate.removeRepetitionBlocks();
      var fields = obj.fields;
      if (fields) {
        for (var i = 0; i < fields.length; i++) {
          var field = fields[i];
          var fieldNode = fieldTemplate.addRepetitionBlock(null);
          setInnerText(fieldNode.cells[0], field.displayName);
          var object = obj.object;
          var valueNode = fieldNode.cells[1];
          var value = object[field.name];
          switch (field.type) {
            case "reference":
              valueNode.align = "center";
              if (!value || typeof value != "object") {
                setInnerText(valueNode, "N/A");
                valueNode.style.fontStyle = "italic";
                break;
              }
              /*var mainField = value[field.mainField];
              if (mainField) valueNode.innerText = mainField;*/
              if (value.mainField) setInnerText(valueNode, value.mainField);
              else {
                setInnerText(valueNode, "Ver");
                valueNode.style.fontStyle = "italic";
              }
              valueNode.objectId = value.id;
              valueNode.type = field.referencedType;
              //valueNode.setAttribute("href", "javascript: viewObject('" + value.id + "', '" + field.referencedType + "')");
              valueNode.style.cursor = "pointer";
              valueNode.style.color = "blue";
              valueNode.style.textDecoration = "underline";
              valueNode.onclick = function () {
                var div = this;
                div.onclick = null;
                div.style.cursor = "";
                div.style.color = "";
                div.style.textDecoration = "";
                loadObject(div, div.objectId, div.type);
              }.bind(valueNode);
              break;
            case "list":
            case "set":
              setInnerText(valueNode, "Listar");
              valueNode.align = "center";
              valueNode.objectType = field.elementType;
              valueNode.field = field.name;
              //valueNode.setAttribute("href", "javascript: showList('" + field.elementType + "', '" + object.id + "', '" + type + "', '" + field.name + "')");
              valueNode.style.cursor = "pointer";
              valueNode.style.color = "blue";
              valueNode.style.textDecoration = "underline";
              valueNode.onclick = function () {
                var div = this;
                div.onclick = null;
                div.style.cursor = "";
                div.style.color = "";
                div.style.textDecoration = "";
                loadList(div, div.objectType, id, type, div.field);
              }.bind(valueNode);
              break;
            case "number":
              valueNode.align = "right";
              valueNode.setAttribute("value", value);
              if (value === "") {
                setInnerText(valueNode, "N/A");
                valueNode.style.fontStyle = "italic";
                valueNode.align = "center";
              }
              else setInnerText(valueNode, numberFormat(value));
              break;
            case "boolean":
              valueNode.align = "center";
              setInnerText(valueNode, value? "S\xED": "No");
              break;
            case "string":
              if (field.private) {
                setInnerText(valueNode, "**********");
                break;
              }
            default:
              setInnerText(valueNode, value);
          }
        }
      }
      wf2ParseHTML(div);
    }, null, false, function (code, message) {
      alert(message);
      setInnerHTML(div, "No se pudo completar la petici\xF3n");
      return;
    });
  }, null, true);
}
function showNewForm(type, option) {
  requestPopup("form.php?type=" + type, function (popup) {
    var form = Element.firstElement(popup.container);
    form.option = option;
    form.objectType = type;
    popup.showPopup();
  }/*, null, !option*/);
}
function showEditForm(id, type) {
  requestPopup("form.php?id=" + id + "&type=" + type, function (popup) {
    var form = Element.firstElement(popup.container);
    form.objectType = type;
    form.objectId = id;
    popup.showPopup();
  });
}
function newObject(event, form) {
  var obj = parseXMLToJS(event.receivedDocument.documentElement);
  if (obj.message) {
    alert(obj.message);
    alertObj(obj, true);
    return;
  }
  var option = form.option;
  if (option) {
    var node = document.createElement("option");
    domInsertBefore(node, option);
    node.value = obj.id;
    setInnerText(node, obj.text);
    node.selected = true;
    form.parentNode.popup.hidePopup();
    //domRemoveNode(form.parentNode.popup);
  }
  else {
    showList(form.objectType);
    hideForm(form);
  }
}
function editObject(event, form) {
  var obj = parseXMLToJS(event.receivedDocument.documentElement);
  if (obj.message) {
    alert(obj.message);
    alertObj(obj, true);
    return;
  }
  showObject(form.objectId, form.objectType);
  //domRemoveNode(form.parentNode.popup);
  form.parentNode.popup.hidePopup();
}
function deleteObject(id, type, parentId, objectType, field) {
  if (confirm("Sure?")) requestJS("delete.php?id=" + id + "&type=" + type + (parentId? "&parentId=" + parentId + "&field=" + field + "&objectType=" + objectType: ""), function (obj) {
    if (obj.message) {
      alert(obj.message);
      alertObj(obj, true);
      return;
    }
    if (field) showObject(parentId, objectType);
    else showList(type);
  });
}
function hideForm(form) {
  //domRemoveNode(form.parentNode.popup);
  form.parentNode.popup.hidePopup();
}
function addItem(add) {
  var rows = add.parentNode.parentNode.parentNode.rows;
  var div = Element.firstElement(rows[rows.length - 1].cells[0]);
  var src = div.getAttribute("src");
  if (src) {
    div.removeAttribute("src");
    div.innerHTML = requestText(src);
  }
}
function addListItem(type, select) {
  var option = Element.lastElement(select);
  if (option.selected) {
    showNewForm(type, option);
    select.selectedIndex = 0;
  }
}
function selectType(select, prefix) {
  var node = document.getElementById(prefix + select.value);
  do {
    if (node.parentNode.whatwgAttr == "switch") node.setActive();
    node = Element.parentElement(node);
  } while (node);
}
function loadDataList(type) {
  var node = document.getElementById("datalist_" + type);
  if (!node) {
    var div = document.createElement("div");
    div.setAttribute("whatwg", "datalist");
    div.id = "datalist_" + type;
    document.body.appendChild(div);
    div.innerHTML = requestText("datalist.php?type=" + type);
    wf2ParseHTML(div);
  }
}
function setNull(check) {
  var node = domNextElement(domNextElement(check));
  if (check.checked) domHideNode(node);
  else domShowNode(node);
}
var pageLength = 50;