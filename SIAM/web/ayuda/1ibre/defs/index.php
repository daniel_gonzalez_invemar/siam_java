<?php
require_once "../defs.php";
$main_types = array();
$private_types = array();
foreach ($_DEFS as $type)
  if ($type["abstract"] || $type["dependent"]) $private_types[$type["displayName"]] = $type["name"];
  else $main_types[$type["displayName"]] = $type["name"];
ksort($main_types);
ksort($private_types);
$type = $_GET["type"];
?>
<html>
<head>
<title>Administrador</title>
<script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>
<!--script type="text/javascript" src="../1ibre/sarissa/sarissa.js"></script>
<script type="text/javascript" src="../1ibre/ieemu/ieemu.js"></script-->
<script type="text/javascript" src="../1ibre/1ibre.js"></script>
<!--script type="text/javascript" src="../1ibre/json/json.js"></script>
<script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="../1ibre/whatwg/whatwg.js"></script-->
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>
<script type="text/javascript" src="index.js"></script>
<script type="text/javascript">
types = {};
hiddenTypes = {};
</script>
<link href="../1ibre/jscalendar/calendar-system.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php if (!$type) { ?>
<table width="100%" height="100%" border="1" cellpadding="3">
  <tr>
    <td width="200" valign="top">
      <table cellpadding="1" align="right">
        <?php foreach ($main_types as $display_name => $type_name) { ?>
          <tr>
            <td align="right" style="padding-right: 3px; text-decoration: none" href="javascript: showList('<?php echo $type_name ?>')">
              <?php echo $display_name ?>
              <script type="text/javascript">
                types.<?php echo $type_name ?> = "<?php echo $display_name ?>";
              </script>
            </td>
          </tr>
        <?php } ?>
      </table>
    </td>
    <td valign="top" rowspan="2">
      <?php } else { ?>
        <script type="text/javascript" src="../1ibre/sarissa/sarissa.js"></script>
        <script type="text/javascript" src="../1ibre/ieemu/ieemu.js"></script>
        <script type="text/javascript" src="../1ibre/1ibre.js"></script>
        <script type="text/javascript" src="../1ibre/json/json.js"></script>
        <script type="text/javascript" src="../1ibre/prototype.js"></script>
        <script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>
        <script type="text/javascript" src="../1ibre/whatwg/whatwg.js"></script>
        <script type="text/javascript" src="../1ibre/whatwg/lang-es.js"></script>
        <script type="text/javascript" src="index.js"></script>
        <script type="text/javascript">
          types = {
          <?php foreach ($main_types as $display_name => $type_name) { ?>
            <?php echo $type_name ?>: "<?php echo $display_name ?>",
          <?php } ?>
            _: null
          };
          Event.observe(window, "load", function () {
            showList('<?php echo $type ?>');
          });
        </script>
      <?php } ?>
      <div whatwg="switch" id="body">
        <div>
          Administrador de objetos
        </div>
        <div>
          <div>
            <a href="" style="font-weight: bold">Type</a>
            <a href="">Agregar</a>
          </div>
          <div whatwg="datagrid"></div>
        </div>
        <div>
          <div>
            <a href="" style="font-weight: bold">Type</a>
            <a href="">Agregar</a>
          </div>
          <div></div>
        </div>
      </div>
      <?php if (!$type) { ?>
    </td>
  </tr>
  <?php if ($private_types) { ?>
  <tr>
    <td valign="top" width="200">
      <div whatwg="details">
        <div whatwg="legend">M&aacute;s...</div>
        <table cellpadding="1" align="right">
          <?php foreach ($private_types as $display_name => $type_name) { ?>
            <tr>
              <td align="right" style="padding-right: 3px; text-decoration: none" href="javascript: showHiddenList('<?php echo $type_name ?>')">
                <?php echo $display_name ?>
                <script type="text/javascript">
                  hiddenTypes.<?php echo $type_name ?> = true;
                  types.<?php echo $type_name ?> = "<?php echo $display_name ?>";
                </script>
              </td>
            </tr>
          <?php } ?>
        </table>
      </div>
    </td>
  </tr>
  <?php } ?>
</table>
<?php } ?>
</body>
</html>