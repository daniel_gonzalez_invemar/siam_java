<?php
require_once "1ibre/sql.php";
$_IBRE_CACHE = array();
define("IBRE_SKIP", 1);
function ibre_init($dsn, $conf) {
  global $_DEFS;
  sql_connect($dsn, $GLOBALS["_1IBRE_OPTIONS"]);
  $_DEFS = $conf;
  foreach ($_DEFS as $type_name => $tmp_type) {
    $type = &$_DEFS[$type_name];
    $type["name"] = $type_name;
    $type["allFields"] = array();
    $type["allFetchs"] = array();
    if (!isset($type["displayName"]))
      $type["displayName"] = $type_name;
    if (!isset($type["table"])) $type["table"] = $type_name;
    if (!isset($type["subtypes"])) $type["subtypes"] = array();
    if (isset($type["extends"])) {
      $extends = &$_DEFS[$type["extends"]];
      $type["extends"] = &$extends;
      if (!isset($extends["subtypes"])) $extends["subtypes"] = array();
      $extends["subtypes"][] = &$type;
    }
    $type["fetchs"] = array();
    if (!isset($type["fields"])) $type["fields"] = array();
    if (!isset($type["references"])) $type["references"] = array();
    foreach ($type["fields"] as $field_name => $tmp_field) {
      $field = &$type["fields"][$field_name];
      $field["name"] = $field_name;
      $field["ownerType"] = &$type;
      if (!isset($field["displayName"]))
        $field["displayName"] = $field_name;
      if (!isset($field["column"]))
        $field["column"] = $field_name;
      $no_dependent = !isset($field["dependent"]);
      if (isset($field["referencedType"])) {
        $field["referencedType"] = &$_DEFS[$field["referencedType"]];
        $field["referencedType"]["references"][] = &$field;
        if ($field["referencedType"]["dependent"] && $no_dependent)
          $field["dependent"] = TRUE;
      }
      if (isset($field["elementType"])) {
        $field["elementType"] = &$_DEFS[$field["elementType"]];
        $field["elementType"]["references"][] = &$field;
        if ($field["elementType"]["dependent"] && $no_dependent)
          $field["dependent"] = TRUE;
      }
      if (isset($field["keyType"]))
        $field["keyType"] = &$_DEFS[$field["keyType"]];
      if (isset($field["valueType"]))
        $field["valueType"] = &$_DEFS[$field["valueType"]];
      $type["allFields"][$field_name] = &$field;
      if ($field["fetch"]) {
        $type["fetchs"][] = &$field;
        $type["allFetchs"][] = &$field;
      }
    }
		if ($type["orderBy"]) $type["orderBy"] = $type["fields"][$type["orderBy"]];
  }
  foreach ($_DEFS as $type_name => $tmp_type) {
    $type = &$_DEFS[$type_name];
    $supertype = &$type;
    while (isset($supertype["extends"])) {
      $supertype = &$supertype["extends"];
      foreach ($supertype["fields"] as $field_name => $tmp_field) {
        $field = &$supertype["fields"][$field_name];
        $type["allFields"][$field_name] = &$field;
        if ($tmp_field["fetch"]) $type["allFetchs"][] = &$field;
      }
    }
    if (is_string($type["mainField"]) && $type["allFields"][$type["mainField"]])
      _ibre_set_main_field($type, $type["allFields"][$type["mainField"]]);
  }
  foreach ($_DEFS as $type_name => $tmp_type) {
    $type = &$_DEFS[$type_name];
    foreach ($type["fields"] as $field_name => $tmp_field) {
      $field = &$type["fields"][$field_name];
      $no_dependent = !isset($field["dependent"]);
      if (isset($field["referenceMappedField"])) {
        $field["referenceMappedField"] = &$field["referencedType"]["allFields"][$field["referenceMappedField"]];
      }
      if (isset($field["elementMappedField"])) {
        $field["elementMappedField"] = &$field["elementType"]["allFields"][$field["elementMappedField"]];
        if ($no_dependent) {
          $field["dependent"] = TRUE;
          $field["elementMappedField"]["dependent"] = FALSE;
        }
      }
      if (isset($field["valueMappedField"])) {
        $field["valueMappedField"] = &$field["valueType"]["allFields"][$field["valueMappedField"]];
        $field["dependent"] = TRUE;
        $field["valueMappedField"]["dependent"] = FALSE;
      }
      if (isset($field["keyMappedField"])) {
        $field["keyMappedField"] = &$field["keyType"]["allFields"][$field["keyMappedField"]];
        $field["dependent"] = TRUE;
        $field["keyMappedField"]["dependent"] = FALSE;
      }
    }
  }
}
function ibre_extends($type1, $type2) {
  $type1 = ibre_get_type($type1);
  $type2 = ibre_get_type($type2);
  do {
    if ($type1["name"] == $type2["name"]) return TRUE;
    $type1 = $type1["extends"];
  } while ($type1);
}
function _ibre_set_main_field(&$type, &$main_field) {
  $type["mainField"] = &$main_field;
  $subtypes = &$type["subtypes"];
  foreach ($subtypes as $index => $subtype) {
    if (is_string($subtypes[$index]["mainField"])) continue;
    _ibre_set_main_field($subtypes[$index], $main_field);
  }
}
function _ibre_base($type) {
  global $_DEFS;
  $type = ibre_get_type($type);
  while ($type["extends"]) $type = $type["extends"];
  return $type["name"];
}
function ibre_get_object_by_id($type, $id, $other_fields = NULL) {
  global $_IBRE_CACHE;
  $base = _ibre_base($type);
  if ($_IBRE_CACHE[$base][$id]) {
    $obj = $_IBRE_CACHE[$base][$id];
    _ibre_load_fields($obj, $other_fields);
    return $obj;
  }
  return ibre_get_object($type, "`id` = ?", array($id), $other_fields);
}
function ibre_get_type($param) {
  global $_DEFS;
  if (is_string($param)) return $_DEFS[$param];
  if ($param["_TYPE"]) return $_DEFS[$param["_TYPE"]];
  return $param;
}
function ibre_get_main_field_value($obj) {
  global $_DEFS;
  $type = ibre_get_type($obj);
  if (!$type["mainField"]) return $obj["id"];
  $value = ibre_get_field($obj, $type["mainField"]);
  if (is_array($value)) {
    if ($type["mainField"]["type"] == "reference")
      return ibre_get_main_field_value($value);
    foreach ($value as $item) {
      $res[] = ibre_get_main_field_value($item);
    }
    return "[".implode(",", $res)."]";
  }
  return $value;
}
function _ibre_load_fields(&$obj, $other_fields) {
  $type = ibre_get_type($obj);
  /*$main_field = $type["mainField"];
  if ($main_field) ibre_get_field($obj, $main_field, NULL);*/
  ibre_get_fields($obj, $type["allFetchs"]);
  if ($other_fields) ibre_get_fields($obj, $other_fields);
}
function _ibre_load($row, $type, $other_fields = NULL) {
  global $_DEFS, $_IBRE_CACHE;
  $id = $row["id"];
  $base = _ibre_base($type);
  if ($_IBRE_CACHE[$base][$id]) {
    $obj = $_IBRE_CACHE[$base][$id];
    _ibre_load_fields($obj, $other_fields);
    return $obj;
  }
  $supertype = $type["extends"];
  while ($supertype) {
    $row = array_merge(sql_row("SELECT * FROM `$supertype[table]` WHERE `id` = ?", $id), $row);
    $supertype = $supertype["extends"];
  }
  $subtypes = $type["subtypes"];
  $obj_type = $type;
  do {
    $has_subtype = FALSE;
    foreach ($subtypes as $subtype) {
      $record = sql_row("SELECT * FROM `$subtype[table]` WHERE `id` = ?", $id);
      if ($record) {
        $row = array_merge($row, $record);
        $has_subtype = TRUE;
        $obj_type = $subtype;
        $subtypes = $subtype["subtypes"];
        break;
      }
    }
  } while ($has_subtype);
  foreach ($obj_type["allFields"] as $field_name => $field) {
    $column = $field["column"];
    if ($field_name != $column && array_key_exists($column, $row)) {
      $row[$field_name] = $row[$column];
      unset($row[$column]);
    }
    switch ($field["type"]) {
      case "number":
        if (isset($row[$field_name])) $row[$field_name] = (double)$row[$field_name];
        break;
      case "boolean":
        if (isset($row[$field_name])) $row[$field_name] = $row[$field_name] == 1;
        break;
    }
  }
  $row["_TYPE"] = $obj_type["name"];
  _ibre_load_fields($row, $other_fields);
  $_IBRE_CACHE[$base][$id] = $row;
  return $row;
}
function ibre_get_objects($type, $cond = "1", $params = NULL, $other_fields = NULL) {
  $type = ibre_get_type($type);
  return ibre_get_objects_by_sql($type, "SELECT * FROM `$type[table]` WHERE $cond".($type["orderBy"]? " ORDER BY `{$type[orderBy][column]}`": ""), $params, $other_fields);
}
function ibre_get_object($type, $cond = "1", $params = NULL, $other_fields = NULL) {
  $type = ibre_get_type($type);
  return ibre_get_object_by_sql($type, "SELECT * FROM `$type[table]` WHERE $cond", $params, $other_fields);
}
function ibre_get_objects_by_sql($type, $query, $params = NULL, $other_fields = NULL) {
  $type = ibre_get_type($type);
  $rows = sql_all($query, $params);
  if (PEAR::isError($rows)) return $rows;
  $res = array();
  foreach ($rows as $row) $res[] = _ibre_load($row, $type, $other_fields);
  return $res;
}
function ibre_get_object_by_sql($type, $query, $params = NULL, $other_fields = NULL) {
  $type = ibre_get_type($type);
  $row = sql_row($query, $params);
  if (!$row) return NULL;
  if (PEAR::isError($row)) return $row;
  return _ibre_load($row, $type, $other_fields);
}
function ibre_get_field(&$obj, $field, $other_fields = NULL) {
  $type = ibre_get_type($obj);
  if (is_string($field))
    $field = $type["allFields"][$field];
  $field_name = $field["name"];
  $value = $obj[$field_name];
  if (isset($field["dinamic"])) {
    if (!$value)
      $obj[$field_name] = $field["dinamic"]($obj, $field, $other_fields);
  }
  else switch ($field["type"]) {
    case "reference":
      $mapped_field = $field["referenceMappedField"];
      if ($mapped_field)
        $obj[$field_name] = ibre_get_object($mapped_field["ownerType"], "$mapped_field[column] = ?", $obj["id"], $other_fields);
      elseif ($value && !is_array($value))
        $obj[$field_name] = ibre_get_object_by_id($field["referencedType"], $value, $other_fields);
      break;
    case "list":
    case "set":
      $element_type = $field["elementType"];
      if (is_array($value)) break;
      $order_by = $element_type["orderBy"];
      $field_index = $field["index"];
      if (isset($field["table"])) {
        $table = $field["table"];
        $obj[$field_name] = ibre_get_objects_by_sql($element_type, "SELECT `_a`.* FROM `$element_type[table]` as `_a`, `$table[name]` as `_b` WHERE `_a`.`id` = `_b`.`$table[join]` AND `_b`.`$table[element]` = ?".($field_index? " ORDER BY `_b`.`$field_index`": ($order_by? " ORDER BY `_a`.`$order_by[column]`": "")), $obj["id"], $other_fields);
      }
      else {
        $mapped_field = $field["elementMappedField"];
        $owner_type = $mapped_field["ownerType"];
        $element_type_table = $element_type["table"];
        if ($owner_type["name"] == $element_type["name"])
          $obj[$field_name] = ibre_get_objects_by_sql($element_type, "SELECT * FROM `$element_type_table` WHERE `$mapped_field[column]` = ?".($field_index? " ORDER BY `$field_index`": ($order_by? " ORDER BY `$order_by[column]`": "")), $obj["id"], $other_fields);
        else {
          $owner_type_table = $owner_type["table"];
          $obj[$field_name] = ibre_get_objects_by_sql($element_type, "SELECT `_a`.* FROM `$element_type_table` as `_a`, `$owner_type_table` as `_b` WHERE `$mapped_field[column]` = ? AND `_a`.`id` = `_b`.`id`".($field_index? " ORDER BY `_b`.`$field_index`": ($order_by? " ORDER BY `_a`.`$order_by[column]`": "")), $obj["id"], $other_fields);
        }
      }
      break;
  }
  return $obj[$field_name];
}
function ibre_get_fields(&$obj, $fields) {
  $fields = (array)$fields;
  foreach ($fields as $name => $field) {
    if (is_string($name)) {
      ibre_get_field($obj, $name, $field);
      if ($field === IBRE_SKIP) unset($obj[$name]);
    }
    else ibre_get_field($obj, $field, NULL);
  }
}
function ibre_new_object($type, $values) {
  $type = ibre_get_type($type);
  if ($type["abstract"] || $type["dependent"]) return NULL;
  return _ibre_new_object($type, $values);
}
function _ibre_new_object($type, $values) {
  $type = ibre_get_type($type);
  if ($type["extends"]) {
    $new_obj = _ibre_new_object($type["extends"], $values);
    if (PEAR::isError($new_obj)) return $new_obj;
    $id = $new_obj["id"];
    unset($GLOBALS["_IBRE_CACHE"][_ibre_base($type["extends"])][$id]);
    //$new_obj["_TYPE"] = $type;
  }
  else {
    $id = sql_one("SELECT MAX(id) FROM `$type[table]`") + 1;
    /*$res = sql_query("INSERT INTO `$type[table]` () VALUES()");
    if (PEAR::isError($res)) return $res;
    $id = sql_id();*/
  }
  $field_names[] = "id";
  $field_values[] = "?";
  $field_params[] = $id;
  //if (!is_array($type["fields"])) echo_r(debug_backtrace(), 5);
  foreach ($type["fields"] as $field_name => $field) {
    if (isset($field["dinamic"]) || $field["referenceMappedField"]) continue;
    $value = $values[$field_name];
    $column = $field["column"];
    switch ($field["type"]) {
      case "reference":
        $reference_type = $field["referencedType"];
        if ($field["dependent"]) {
          if ($value || !$field["null"]) {
            $field_names[] = $column;
            $field_values[] = "?";
            $reference = _ibre_new_object($value["_TYPE"]? $value["_TYPE"]: $reference_type, $value);
            if (PEAR::isError($reference)) return $reference;
            $field_params[] = $reference["id"];
          }
        }
        else if ($value) {
          $field_names[] = $column;
          $field_values[] = "?";
          if (is_array($value)) {
            if (isset($value["id"])) {
              $field_params[] = $value["id"];
            }
            else {
              $reference = _ibre_new_object($value["_TYPE"]? $value["_TYPE"]: $reference_type, $value);
              if (PEAR::isError($reference)) return $reference;
              $field_params[] = $reference["id"];
            }
          }
          else {
            $field_params[] = $value;
          }
        }
        break;
      case "list":
      case "set":
        break;
      default:
        if (is_a($value, "ibre_sql_param")) {
          $field_names[] = $column;
          $field_values[] = $value->str;
        }
        elseif ($values && array_key_exists($field_name, $values)) {
          if (!isset($value) && $field["null"]) {
            $field_names[] = $column;
            $field_values[] = "NULL";
          }
          else {
            if (!$field["null"] && $value == NULL) continue;
            else {
              $field_names[] = $column;
              $field_values[] = "?";
              $field_params[] = $field["type"] == "boolean" && !$value? 0: $value;
            }
          }
        }
    }
  }
  $res = sql_query("INSERT INTO `$type[table]` (`".implode("`, `", $field_names)."`) VALUES(".implode(", ", $field_values).")", $field_params);
  if (PEAR::isError($res)) return $res;
  /*if ($assign) {
    $params[] = $id;
    $res = sql_query("UPDATE `$type[table]` SET ".implode(", ", $assign)." WHERE id = ?", $params);
    if (PEAR::isError($res)) return $res;
  }*/
  foreach ($type["fields"] as $field_name => $field) {
    if ($field["type"] == "list" || $field["type"] == "set") {
      $items = $values[$field_name];
      if ($items) {
        $res = ibre_add_items(array("_TYPE" => $type["name"], "id" => $id), $field, $items);
        if (PEAR::isError($res)) return $res;
      }
    }
  }
  $new_obj = ibre_get_object_by_id($type, $id);
  if ($type["onNew"]) $type["onNew"]($new_obj);
  return $new_obj;
}
function ibre_edit_object_by_id($type, $id, $values) {
  return ibre_edit_object(ibre_get_object_by_id($type, $id), $values);
}
function ibre_edit_object($obj, $values) {
  //$type = $obj["_TYPE"];
  $type = ibre_get_type($obj);
  $id = $obj["id"];
  if ($type["extends"]) {
    $obj["_TYPE"] = $type["extends"]["name"];
    $res = ibre_edit_object($obj, $values);
    if (PEAR::isError($res)) return $res;
    $obj["_TYPE"] = $type["name"];
  }
  foreach ($values as $field_name => $value) {
    $field = $type["fields"][$field_name];
    if (isset($field["dinamic"]) || $field["referenceMappedField"]) continue;
    $obj_value = $obj[$field_name];
    if (!$field) continue;
    $column = $field["column"];
    switch ($field["type"]) {
      case "reference":
        $referenced_type = $field["referencedType"];
        if ($field["dependent"]) {
          if ($obj_value) {
            if (!$value || $value["id"] != $obj_value["id"]) {
              if (!is_array($obj_value)) {
                $obj_value = ibre_get_object_by_id($referenced_type, $obj_value);
                $res = $obj_value? ibre_delete_object($obj_value): TRUE;
              }
              else $res = ibre_delete_object($obj_value);
              if (PEAR::isError($res)) return $res;
            }
            elseif ($value["id"] == $obj_value["id"])
              ibre_edit_object($obj_value, $value);
          }
          if ($value && (!$obj_value || $value["id"] != $obj_value["id"])) {
            $assign[] = "`$column` = ?";
            $reference = _ibre_new_object($value["_TYPE"]? $value["_TYPE"]: $referenced_type, $value);
            if (PEAR::isError($reference)) return $reference;
            $params[] = $reference["id"];
          }
          elseif (!$value && $obj_value) $assign[] = "`$column` = NULL";
        }
        elseif (!$value) $assign[] = "`$column` = NULL";
        else {
          $assign[] = "`$column` = ?";
          if (is_array($value)) {
            if (isset($value["id"]))
              $params[] = $value["id"];
            else {
              $reference = _ibre_new_object($value["_TYPE"]? $value["_TYPE"]: $referenced_type, $value);
              if (PEAR::isError($reference)) return $reference;
              $params[] = $reference["id"];
            }
          }
          else $params[] = $value;
        }
        break;
      case "list":
      case "set":
        break;
      default:
        if (is_a($value, "ibre_sql_param")) {
          $assign[] = "`$column` = $value->str";
        }
        else {
          if (!$field["null"] || $value != NULL && $value != "") {
            $assign[] = "`$column` = ?";
            $params[] = $value;
          }
          else $assign[] = "`$column` = NULL";
        }
    }
  }
  if ($assign) {
    $params[] = $id;
    $res = sql_query("UPDATE `$type[table]` SET ".implode(", ", $assign)." WHERE id = ?", $params);
    if (PEAR::isError($res)) return $res;
    unset($GLOBALS["_IBRE_CACHE"][_ibre_base($type)][$id]);
  }
  foreach ($values as $field_name => $value) {
    $field = $type["fields"][$field_name];
    if ($field["type"] == "list" || $field["type"] == "set") {
      $items = ibre_get_field($obj, $field);
      $old_items = array();
      foreach ($items as $item) $old_items[$item["id"]] = $item;
      $field_type = $field["elementType"];
      foreach ($value as $item) {
        if (is_array($item)) {
          $item_id = $item["id"];
          if ($item_id) {
            $res = ibre_edit_object_by_id($field_type, $item_id, $item);
            unset($old_items[$item_id]);
          }
          else $res = ibre_add_item($obj, $field, $item);
          if (PEAR::isError($res)) return $res;
        }
        elseif (!isset($old_items[$item])) {
          $res = ibre_add_item($obj, $field, $item);
          if (PEAR::isError($res)) return $res;
        }
        else unset($old_items[$item]);
      }
      foreach ($old_items as $item) {
        $res = ibre_delete_item($obj, $field, $item);
        if (PEAR::isError($res)) return $res;
      }
    }
  }
  $new_obj = ibre_get_object_by_id($type, $id);
  if ($type["onEdit"]) $type["onEdit"]($obj, $new_obj);
  return $new_obj;
}
function ibre_delete_object_by_id($type, $id) {
  $obj = ibre_get_object_by_id($type, $id);
  if (PEAR::isError($obj)) return $obj;
  return ibre_delete_object($obj);
}
function ibre_delete_object($obj) {
  $type = ibre_get_type($obj);
  $id = $obj["id"];
  foreach ($type["fields"] as $field) {
    if (isset($field["dinamic"]) || $field["referenceMappedField"]) continue;
    switch ($field["type"]) {
      case "reference":
        if ($field["dependent"]) {
          $value = $obj[$field["name"]];
          if ($value) {
            if (is_array($value)) $res = ibre_delete_object($value);
            else $res = ibre_delete_object_by_id($field["referencedType"], $value);
            if (PEAR::isError($res)) return $res;
          }
        }
        break;
      case "list":
      case "set":
        $res = ibre_clear_list($obj, $field);
        if (PEAR::isError($res)) return $res;
        break;
    }
  }
  $res = sql_query("DELETE FROM `$type[table]` WHERE `id` = ?", $id);
  if (PEAR::isError($res)) return $res;
  if ($type["onDelete"]) $type["onDelete"]($obj);
  if ($type["extends"]) {
    $obj["_TYPE"] = $type["extends"]["name"];
    return ibre_delete_object($obj);
  }
  unset($GLOBALS["_IBRE_CACHE"][_ibre_base($type)][$id]);
  return TRUE;
}
function ibre_contains_item($obj, $field, $item) {
  $type = ibre_get_type($obj);
  if (is_string($field))
    $field = $type["allFields"][$field];
  $id = $obj["id"];
  $table = $field["table"];
  $item_id = is_array($item)? $item["id"]: $item;
  if ($table)
    $sql = "SELECT * FROM `$table[name]` WHERE `$table[element]` = ? AND `$table[join]` = ?";
  else {
    $mapped_field = $field["elementMappedField"];
    $sql = "SELECT `id` FROM `{$mapped_field[ownerType][table]}` WHERE `$mapped_field[column]` = ? AND `id` = ?";
  }
  return sql_row($sql, array($id, $item_id)) != NULL;
}
function ibre_add_item($obj, $field, $item) {
  $type = ibre_get_type($obj);
  if (is_string($field))
    $field = $type["allFields"][$field];
  if (!is_array($item)) $item_id = $item;
  else if (isset($item["id"])) $item_id = $item["id"];
  $element_type = $field["elementType"];
  if ($item_id && $field["type"] == "set" && ibre_contains_item($obj, $field, $item_id))
    return ibre_get_object_by_id($element_type, $item_id);
  $id = $obj["id"];
  $table = $field["table"];
  $index = $field["index"];
  if ($table) {
    if (!$item_id) {
      $object = _ibre_new_object($item["_TYPE"]? $item["_TYPE"]: $field["elementType"], $item);
      if (PEAR::isError($object)) return $object;
      $item_id = $object["id"];
    }
    //if ($index) $index_value = sql_one("SELECT MAX(`$index`) FROM `$table[name]` WHERE `$table[element]` = ?", $id);
    $res = sql_query("INSERT INTO `$table[name]` (`$table[element]`, `$table[join]`".($index? ", `$index`": "").") VALUES(?, ?".($index? ", MAX(`$index`) + 1": "").")", array($id, $item_id));
    if (PEAR::isError($res)) return $res;
    return ibre_get_object_by_id($element_type, $item_id);
  }
  if (is_array($item)) {
    $item[$field["elementMappedField"]["name"]] = $id;
    if ($item["id"]) return ibre_edit_object($item, $item);
    else return _ibre_new_object($item["_TYPE"]? $item["_TYPE"]: $element_type, $item);
  }
  else return ibre_edit_object_by_id($element_type, $item, array($field["elementMappedField"]["name"] => $id));
}
function ibre_add_items($obj, $field, $items) {
  foreach ($items as $item) {
    $res = ibre_add_item($obj, $field, $item);
    if (PEAR::isError($res)) return $res;
  }
  return TRUE;
}
function ibre_delete_items($obj, $field, $items) {
  foreach ($items as $item) {
    $res = ibre_delete_item($obj, $field, $item);
    if (PEAR::isError($res)) return $res;
  }
  return TRUE;
}
function ibre_delete_item($obj, $field, $item) {
  $type = ibre_get_type($obj);
  if (is_string($field))
    $field = $type["allFields"][$field];
  $id = $obj["id"];
  $table = $field["table"];
  $item_id = is_array($item)? $item["id"]: $item;
  if ($table) {
    $res = sql_query("DELETE FROM `$table[name]` WHERE `$table[element]` = ? AND `$table[join]` = ?", array($id, $item_id));
    if (PEAR::isError($res) || !$field["dependent"] || $res == 0) return $res;
  }
  $element_type = $field["elementType"];
  $item = ibre_get_object_by_id($element_type, $item_id);
  $mapped_field = $field["elementMappedField"]["name"];
  if ($mapped_field) {
    if ($item[$mapped_field] != $id) return TRUE;
    if ($field["nullOnDelete"] || !$field["dependent"])
      return ibre_edit_object($item, array($mapped_field => NULL));
  }
  return ibre_delete_object($item);
}
function ibre_clear_list($obj, $field) {
  return ibre_delete_items($obj, $field, ibre_get_field($obj, $field));
}
class ibre_sql_param {
  function ibre_sql_param($str) {
    $this->str = $str;
  }
}
function ibre_sql($str) {
  return new ibre_sql_param($str);
}
?>
