<?php
function include_type($type, $prefix = "", $obj = NULL, $skip_field = NULL) {
  global $parsed_objects;
  //$skip_field = NULL;
  include "form-type.php";
}
function include_subtype($type, $fields, $type_prefix, $prefix = "", $obj = NULL, $skip_field = NULL) {
  global $parsed_objects;
  include "form-fields.php";
}
function show_type_option($type/*, $current*/) {
  if (!$type["abstract"]/* && !$type["dependent"]*/) {
    $name = $type["name"];
    //echo "<option value='$name'".($current == $name? " selected": "").">$name</option>";
    echo "<option value='$name'>$name</option>";
  }
  foreach ($type["subtypes"] as $subtype)
    show_type_option($subtype, $current);
}
require_once "../defs.php";
$skip_field = $_GET["skipField"];
$prefix = $_GET["prefix"];
$id = $_GET["id"];
if ($id) {
  $obj = ibre_get_object_by_id($_GET["type"], $id);
  $text = "edit";
  $type_name = $obj["_TYPE"];
}
else {
  $obj = NULL;
  $text = "new";
  $type_name = $_GET["type"];
}
$type = $_DEFS[$type_name];
if ($prefix) {
  include_type($type, $prefix, NULL, $skip_field);
  exit;
}
$parsed_objects = array();
?>
<form action="<?php echo "$text.php?type=$type_name&amp;id=$id" ?>" method="post" onreceived="<?php echo $text ?>Object(event, this); return false;" nonVisibleElements="skip" onlySubmitChanged>
  <table style="background-color: #eee; border-collapse: collapse; border: #555 solid 1px" cellpadding="5">
    <tr>
      <th colspan="2"><?php echo $type["displayName"] ?></th>
    </td>
    <tr>
      <td>
        <?php include "form-type.php" ?>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <input type="submit" value="Aceptar">
        &nbsp;
        <input type="button" value="Cancelar" onclick="hideForm(form)">
      </td>
    </tr>
  </table>
</form>
