<%-- 
    Document   : index
    Created on : 18/09/2012, 03:29:44 PM
    Author     : usrsig15
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Video SIAM-2012</title>
        <link href="../plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css" />
        <link href="css/videos.css" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="../plantillaSitio/js/monitoreo.js"></script>
        <script type="text/javascript" >
            /**
             * Comment
             */
            function mostrarTutorial(ruta) {
                document.getElementById('peliculas').src=ruta;
            }
            
        </script>
    </head>
    <body>
        <jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=10"/> 

        <div id="contenedor">          
            <div id="panelizquierdo"  >
                <p id="tablacontenido" >Tabla de Contenido</p>
                <ul>
                    <li  ><a onclick="mostrarTutorial('http://siam.invemar.org.co/siam/ayuda/siam_01.html')">Introducción al SIAM</a></li>
                    <li ><a  onclick="mostrarTutorial('http://siam.invemar.org.co/siam/ayuda/sibm_2012Introduccion.html')" >SIBM - componente ITA</a></li>  
                    <li ><a href="http://siam.invemar.org.co/siam/ayuda/redcam/videotutorial.swf" target='blank_'>Sistema de Información de la REDCAM</a></li>
                    <li ><a href="http://cinto.invemar.org.co/anh/geoservicios.htm" target='blank_'>Geoservicios Biodiversidad Marina <br/>exploración hidrocarburos ANH</a></li>
                    <li ><a onclick="mostrarTutorial('http://siam.invemar.org.co/siam/ayuda/jamaica/Jamaica.swf')" target='blank_'> Línea base ambiental ARC Jamaica</a></li>
                    <li ><a onclick="mostrarTutorial('http://siam.invemar.org.co/siam/ayuda/anhjamaica/ANH_Biodiversidad.swf')" target='blank_'> ANH Biodiversidad</a></li>
                   


                </ul>
            </div>
            <div id="panelderecho">
                <iframe  id="peliculas" src="" width="650px" height="603px"  >

                </iframe>
            </div>            
        </div>

        <%@ include file="../plantillaSitio/footermodulesV3.jsp" %>
    </body>
</html>
