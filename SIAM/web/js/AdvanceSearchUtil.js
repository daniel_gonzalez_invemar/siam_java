var columns="";
var wherecriteria="";
	
	
function tabular(name) {
	
	if(validate()){
		 var msg="Debe indicar por lo menos un campo ";
	    alert(msg);
	    f.phylum.focus();
	    return;
	  }
	  var old = f.action;
	  
	  
	
	 switch(name){
	 	case 'buscar': f.action = "sibmuseo.jsp"; break;
		case 'listado': f.action = "listadoespecies.jsp"; break;
		default:  f.action = "sibmuseo.jsp"; break;
	 }  
	  //f.action = "sibmuseo.jsp";
	  var datasource = f.getAttribute("datasource");
	  f.removeAttribute("datasource");
	
	  
	  
	  var textBo=$('othercriterios');
	  textBo.value=wherecriteria;
	  textBo.disabled=false;
	  
	  
	  
	  var columnHide=document.createElement('input');
	  columnHide.id="columns";
	  columnHide.name="columns";
	  columnHide.type="hidden";
	  columnHide.value=columns;
	  
	  document.getElementById("forma").appendChild(columnHide);
	  
	  
	  f.whatwgSubmit();
	  textBo.disabled=true;
	  f.action = old;
	  f.setAttribute("datasource", datasource);
}

function gmaps(){
	
	if(validate()){
		 var msg="Debe indicar por lo menos un campo ";
    	 alert(msg);
    	 f.phylum.focus();
    	return;
  	}
		
	  var textBo=$('othercriterios');
	  textBo.value=wherecriteria;
	  
	  
	  var columnHide=document.createElement('input');
	  columnHide.id="columns";
	  columnHide.name="columns";
	  columnHide.type="hidden";
	  columnHide.value=columns;
	  
	  document.getElementById("forma").appendChild(columnHide);
	  
	  f.whatwgSubmit();
		
}


function validate(){
	
	return (!$F(f.phylum)&& !$F(f.criterio) && !$F(f.vulgar2) && !$F(f.ncites) && !$F(f.nlibrorojo) && !$F(f.ncatlibrorojo) && !$F(f.nprof1) && !$F(f.nprof2)  && !$F(f.ano1) && !$F(f.ano2) && !$F(f.othercriterios) );
	
}

function fillEstaciones(value){
	var url="servletAjaxProcess";
	var param="action=fillEstaciones?proyecto=" + value;
	var id="estacion";
	
	doFillSelect(url,param,id);
	
}


var percent=" &#37; ".unescapeHTML();

function addCriteria(conector){
	var conect=conector.substring(12);
	var textBox=$('othercriterios');
	var campo=$('campo');
	//var campopt = campo.selectedIndex;
	var operator=$('operador').value;
	var cvalue=$('criteriovalue').value
	
	if( !$F(campo) || !$F($('operador')) || !$F($('criteriovalue')) ){
		var msg="Debe ingresar el campo, el operador y el valor del campo ";
    	 alert(msg);
    	 campo.focus();
    	return;
	}
	
	if (textBox.value == "" ){
			columns=campo.options[campo.selectedIndex].value;
			
			if (operator == "like"){
				wherecriteria+=columns+"  "+operator+"  '"+percent.strip()+cvalue+percent.strip()+"' ";
				//alert(wherecriteria);
			}else if (operator == "!=" ){
				wherecriteria+=columns +" <> '"+cvalue+"' ";
			}else {
				wherecriteria+=columns+"  "+operator +"  '"+cvalue+"' ";
			}
			
			
			textBox.value = textBox.value + "  " + campo.options[campo.selectedIndex].text + "  " + operator + "  " + cvalue ;
	}else{
			
			columns+=","+campo.options[campo.selectedIndex].value;
			
			if (operator == "like"){
				wherecriteria+=conect+campo.options[campo.selectedIndex].value+" "+operator+" '"+percent.strip()+cvalue+percent.strip()+"' ";
				
			}else if (operator == "!=" ){
				wherecriteria+="  "+conect+" "+campo.options[campo.selectedIndex].value +" <> '"+cvalue+"' ";
			}else {
				wherecriteria+="  "+conect+" "+campo.options[campo.selectedIndex].value+" "+operator +" '"+cvalue+"' ";
			}
			
			
			textBox.value = textBox.value + " "+conect+" "+ campo.options[campo.selectedIndex].text + "  " + operator + "  " + cvalue
	}
	
	campo.selectedIndex=0;
	$('operador').selectedIndex=0;
	$('criteriovalue').value="";
	

}

function deleteCriteria(){
	$('othercriterios').value="";
}

function resetForm(){
	
	  f.reset();
 
  $('othercriterios').value="";
}

Event.observe(window, 'load', function(event){
		var textb=$('othercriterios');
		
		if(ibre.params.othercriterios){
		  textb.value=ibre.params.othercriterios.replace(/\+/g,' ');
		}else{
			textb.value="";
		}
		textb.readOnly=true;
	} );
	
	

	 var Url = {  
   
     // public method for url encoding  
     encode : function (string) {  
         return escape(this._utf8_encode(string));  
     },  
   
     // public method for url decoding  
     decode : function (string) {  
         return this._utf8_decode(unescape(string));  
     },  
   
     // private method for UTF-8 encoding  
     _utf8_encode : function (string) {  
         string = string.replace(/\r\n/g,"\n");  
         var utftext = "";  
   
         for (var n = 0; n < string.length; n++) {  
   
             var c = string.charCodeAt(n);  
   
             if (c < 128) {  
                 utftext += String.fromCharCode(c);  
             }  
             else if((c > 127) && (c < 2048)) {  
                 utftext += String.fromCharCode((c >> 6) | 192);  
                 utftext += String.fromCharCode((c & 63) | 128);  
             }  
             else {  
                 utftext += String.fromCharCode((c >> 12) | 224);  
                 utftext += String.fromCharCode(((c >> 6) & 63) | 128);  
                 utftext += String.fromCharCode((c & 63) | 128);  
             }  
   
         }  
   
         return utftext;  
     },  
   
     // private method for UTF-8 decoding  
     _utf8_decode : function (utftext) {  
         var string = "";  
         var i = 0;  
         var c = 0;//c1 = c2 = 0;  
   		 var c1=0;
		 var c2=0;
         while ( i < utftext.length ) {  
   
             c = utftext.charCodeAt(i);  
   
             if (c < 128) {  
                 string += String.fromCharCode(c);  
                 i++;  
             }  
             else if((c > 191) && (c < 224)) {  
                 c2 = utftext.charCodeAt(i+1);  
                 string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));  
                 i += 2;  
             }  
             else {  
                 c2 = utftext.charCodeAt(i+1);  
                 c3 = utftext.charCodeAt(i+2);  
                 string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));  
                 i += 3;  
             }  
   
         }  
   
         return string;  
     }  
   
 }  