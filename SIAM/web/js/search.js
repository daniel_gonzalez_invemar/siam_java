function lastDocs(){
    var url="searchArticles";
    var params="action=ldocs";
    ul=document.getElementById("lastdocs");
    var obj=new Ajax.Request(url,{
        method:"post",
        parameters:params,
        onComplete:function(transport){
            data=eval("("+transport.responseText+")");
            for(var i=0;i<data.length;i++){
                var linkElement = document.createElement("a");
                linkElement.setAttribute("href", "http://www.invemar.org.co/noticias.jsp?id="+data[i].articulo);
                linkElement.innerHTML=data[i].titulo;
                linkElement.target="_blank";
                li=document.createElement('li');
                li.appendChild(linkElement);
                ul.appendChild(li);
            }
            }
        });
}
function doSearch(){
    var url="searchArticles";
    var text1=$F('searchtext1');
    var text2=$F('searchtext2');
    var op=$F('operator');
    var params="action=search&text1="+text1+"&text2="+text2+"&operator="+op;
    new Ajax.Request(url,{
        method:"post",
        parameters:params,
        onComplete:function(transport){
            dataManager(transport);
        }
    });
}
function countRecord(){
    var url="searchArticles";
    var params="action=allrecord";
    var result;
    var obj=new Ajax.Request(url,{
        method:"post",
        parameters:params,
        onComplete:function(transport){
            data=eval("("+transport.responseText+")");
            $('totalrecord').innerHTML= data.allrecord;
        }
    });
}
function dataManager(transport){
    var dataset=eval("("+transport.responseText+")");
    var record=$('record');
    record.innerHTML=dataset.length;
    countRecord();
    $('showrecord').show();
    var oTBody=$('bdatas');
    removeTable('bdatas');
    for(var i=0;i<dataset.length;i++){
        mdata(oTBody,i,dataset[i]);
    }
    $('tdatas').show();
}
function mdata(oTBody,i,data){
    var linkElement = document.createElement("a");
    linkElement.setAttribute("href", "http://www.invemar.org.co/portal_old/noticias.jsp?id="+data.articulo);
    linkElement.innerHTML=data.titulo;
    linkElement.target="_blank";
    oTBody.insertRow(i);
    oTBody.rows[i].insertCell(0);
    oTBody.rows[i].cells[0].className='texttablas';
    oTBody.rows[i].cells[0].appendChild(linkElement);
   // oTBody.rows[i].insertCell(1);
    //oTBody.rows[i].cells[1].className='texttablas';
    //oTBody.rows[i].cells[1].appendChild(document.createTextNode(data.categoria));
    oTBody.rows[i].insertCell(1);
    oTBody.rows[i].cells[1].className='texttablas';
    oTBody.rows[i].cells[1].appendChild(document.createTextNode(data.pcalves));
}
function removeTable(id){
    var tb=$(id);
    if (tb){
        for (var i = tb.childNodes.length -1; i >= 0 ; i--){
            tb.removeChild(tb.childNodes[i]);
        }
        }
}
var sort="ASC";
function sorted(){}
function doSearchByParam(){
    var url="searchArticles";
    var prmeter=location.href.toQueryParams();
    if(prmeter.action!=null && prmeter.action!='undefined'){
        var text1=prmeter.text1||'';
        var text2=prmeter.text2||'';
        var op=prmeter.operator||'';
        var params="action=search&text1="+text1+"&text2="+text2+"&operator="+op;
        new Ajax.Request(url,{
            method:"post",
            parameters:params,
            onComplete:function(transport){
                dataManager(transport);
            }
        });
}
}
Event.observe(window, 'load',function(event){
    lastDocs();
    doSearchByParam();
});