<html>
<head>
<link href="plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css"/>
 <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19778914-22']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<jsp:include page="plantillaSitio/headerv2.jsp?idsitio=10"/>

          <table width="963px" align="center" >

            <tr>

              <td></td>

            </tr>

            <tr>
             
              <td style="padding-leftht:4px;vertical-align:top; padding-right:4px;"><table style="width:100%;">

                <tr>

                  <td style="width:5px; height:20px;"></td>

                  <td>

                        <div class="migas"><a href="http://www.invemar.org.co">invemar.org.co</a> / <a href="index.jsp">Siam</a>/<a href="http://siam.invemar.org.co/siam/desarrollo.jsp">Desarrollo Conceptual</a> </div>

                  </td>

                </tr>

              </table></td>

            </tr>
            
            <tr>

              <td style="width:5px;height:5px;"></td>

            </tr>

            <tr>

              <td  style=" padding-left:4px;padding-right:4px;"><table style="width:100%;">

                <tr>

                  <td><table style="width:100%;" class="colorfondo2">

                    <tr>

                      <td class="titulo_secciones">
                            <h1>Desarrollo Conceptual</h1>					  </td>
                    </tr>

                    <tr>

                      <td class="titulo_sec_smb" height="9"></td>
                    </tr>
                    <tr>
                      <td style="height:10px;" >
                     <table width="750" border="0" align="center" cellpadding="1" cellspacing="1">
        <tr>
          <td bgcolor="#e6e6e6" class="titulosnegros"></td>
        </tr>
        <tr>
          <td>          </td>
        </tr>
        <tr>
          <td><p><span class="texttitulo">Definici&oacute;n</span></p>
            <p class="texttablas">El SIAM es el conjunto integrado de elementos conceptuales, pol&iacute;ticas,
              normas, procesos, recursos humanos y tecnolog&iacute;as que articulan
              la informaci&oacute;n ambiental marino costera generada, administrada
            y/o requerida en los &aacute;mbitos nacional, regional y local.</p>
            <p><span class="texttitulo">Objetivo General</span></p>
            <p class="texttablas">Desarrollar los instrumentos de acopio, an&aacute;lisis
              y gesti&oacute;n de la informaci&oacute;n ambiental y de uso de
              los recursos marinos y costeros de Colombia como elementos de apoyo
              a la generaci&oacute;n de conocimiento, a la toma de decisiones
              y a la gesti&oacute;n orientada al desarrollo sostenible en un
            entorno que favorezca la participaci&oacute;n ciudadana.</p>
            <p class="texttablas"><span class="texttitulo">Objetivos Espec&iacute;ficos</span></p>
            <ol>
              <li class="texttablas">Adaptar, adoptar &oacute; desarrollar est&aacute;ndares,
                protocolos, procesos y soluciones tecnol&oacute;gicas para la captura,
                generaci&oacute;n, procesamiento, flujo, divulgaci&oacute;n y administraci&oacute;n
              de la informaci&oacute;n generada por el sector.</li>
              <li class="texttablas">                Facilitar el acceso y la disponibilidad de
                la informaci&oacute;n
                ambiental, como estrategia de respuesta a las demandas de informaci&oacute;n
              en los entornos local, regional, nacional e internacional.</li>
              <li class="texttablas">                Generar los elementos de informaci&oacute;n que permitan establecer
                el estado y aprovechamiento de los ambientes y recursos marinos
              y costeros y encauzar la gesti&oacute;n ambiental. </li>
              <li class="texttablas">                Apoyar la gesti&oacute;n ambiental integral a nivel nacional,
                mediante la prestaci&oacute;n de servicios, la incorporaci&oacute;n
                y desarrollo de las tecnolog&iacute;as que permitan su integraci&oacute;n
              con el Sistema de Informaci&oacute;n Ambiental para Colombia</li>
              <li class="texttablas">Apoyar los procesos de generaci&oacute;n de conocimiento, transferencia
              e implantaci&oacute;n de tecnolog&iacute;as de la informaci&oacute;n. </li>
            </ol>            
            <p class="texttablas"><span class="texttitulo">Componentes del Sistema</span></p>
            <p class="texttablas">El SIAM cuenta con dos  elementos importantes que <strong>articulados</strong> orientan su desarrollo :</p>
            <p align="center"><img src="plantillaSitio/images/siam.jpg" alt="Componentes del SIAM"
                                   width="95%" border="0" align="absmiddle"></p>            
            
		  <ol>
			    <li class="texttablas">	El soporte conceptual, fundamentado en ejes tem&aacute;ticos: Biodiversidad, Vigilancia de los ambientes marinos, Aprovechamiento eficiente de los recursos, Convivencia segura con la naturaleza y Apoyo a la gesti&oacute;n ambiental integral.
			      Los ejes conceptuales tem&aacute;ticos que orientan los procesos de investigaci&oacute;n y desarrollo  en tecnolog&iacute;as de informaci&oacute;n del SIAM est&aacute;n pensados para dar respuesta a  preguntas sobre&hellip;		            
			      <blockquote>
			        <ul>
		              <li><strong>La Biodiversidad</strong>, a trav&eacute;s de la generaci&oacute;n de informaci&oacute;n y conocimiento sobre la diversidad biol&oacute;gica marina y costera, as&iacute; como los fen&oacute;menos y procesos que regulan su existencia. </li>
		              <li><strong>El aprovechamiento eficiente de los recursos</strong>, produciendo informaci&oacute;n sobre el estado y valoraci&oacute;n del potencial biol&oacute;gico, econ&oacute;mico y social de los recursos naturales marinos y costeros. </li>
		              <li><strong>El apoyo a la gesti&oacute;n ambiental integral</strong>, a trav&eacute;s de instrumentos que incorporen el conocimiento cient&iacute;fico biof&iacute;sico, sociocultural, econ&oacute;mico y de gobernabilidad en el manejo de los recursos naturales marinos y costeros implementando conceptos, metodolog&iacute;as, t&eacute;cnicas e instrumentos que contribuyan al Manejo Integrado de Zonas Costeras en Colombia. </li>
		              <li><strong>La vigilancia de los ambientes marinos</strong>, generando informaci&oacute;n que responde los interrogantes sobre la calidad qu&iacute;mica y sanitaria de las aguas marinas y costeras del pa&iacute;s y aportando conocimiento sobre el impacto de los fen&oacute;menos originados por actividades humanas en la estructura y din&aacute;mica de los ecosistemas marinos. </li>
		              <li><strong>La convivencia segura con la naturaleza</strong>, generando conocimiento e informaci&oacute;n sobre los aspectos f&iacute;sicos de las zonas marinas y costeras en cuanto a los procesos geol&oacute;gicos, oceanogr&aacute;ficos y climatol&oacute;gicos. </li>
	                </ul>
			      </blockquote>
		        </li>
		        <li class="texttablas">La base tecnol&oacute;gica que brinda soporte a partir de las herramientas necesarias para garantizar y asegurar la eficiencia y operatividad del sistema, es el componente que tiene toda la carga l&oacute;gica, brinda  todo el soporte tecnol&oacute;gico necesario y es soporte t&eacute;cnico de los ejes tem&aacute;ticos, esta compuesto  por:
		          <ol>
		            <li><strong>Bases de datos</strong>
                      <ul>
                        <li>Sistema de datos estructurados </li>
                        <li>Sistema de datos espaciales </li>
                        <li>Sistema de informaci&oacute;n documental</li>
                      </ul>
		            </li>
	                <li> Los sistemas que acopian <strong>informaci&oacute;n</strong> obtenida durante las actividades de investigaci&oacute;n ejecutadas de manera <strong>peri&oacute;dica o eventuales</strong>.</li>
		            <li> Los sistemas basados en tecnolog&iacute;as que permiten realizar <strong>monitoreos ambientales </strong>de manera <strong>autom&aacute;tica</strong>. </li>
		          </ol>
		        </li>
            </ol>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td background="images/dotted_line.gif"><img src="images/dotted_line.gif" width="12" height="3"></td>
      </tr>
    </table>
			<p align="center"><br>
		    </p>		  </td>
        </tr>
        <tr>
          <td align="center">&nbsp;</td>
        </tr>
      </table> 
                    <script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
                    <script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2300620-1");
pageTracker._trackPageview();
} catch(err) {}</script>
                  
                </td>
                    </tr>
                    <tr>

                      <td>
                        <table style="width:100%;">

                        <tr>

                          <td style="width:11px;"></td>

                          <td></td>
                          <td style="width:11px;"></td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td style="height:20px;" ></td>
                    </tr>                    
                    <tr>
                      <td style="height:15px;" ><div class="regla" style="width:700px;"></div></td>
                    </tr>

                  </table></td>
                </tr>

              </table></td>

            </tr>

            <tr>

              <td style="width:5px; height:4px;"></td>

            </tr>  


          <% %> 
          <%@ include file="plantillaSitio/footer.jsp" %>
