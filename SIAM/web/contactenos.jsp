<%@ page import="co.org.invemar.siam.sitio.model.*"%>
<%@ page import="co.org.invemar.siam.sitio.vo.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%
    String misitio = request.getParameter("idsitio");
    //out.println(misitio);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
    <head><title >Contactenos SIAM -Sistema de información Marino de Colombia</title>
        <link href='plantillaSitio/css/misiamccs.css' rel='stylesheet' type='text/css'  />
        <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
        <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1" />
        <script type="text/javascript" src="plantillaSitio/js/monitoreo.js"></script>
    </head>
    <body>
        <%String url="plantillaSitio/headerv2.jsp?idsitio="+misitio; %>
        
        <jsp:include page="<%=url%>"  />
        <div class="centrado">
            <table width="936px"  style="text-align:center"  >
                <tr>
                    <td class="titulo_secciones" style="text-align:left"><h1>Cont&aacute;ctenos</h1></td>
                </tr>
                <tr>
                    <td class="titulo_sec_smb"></td>
                </tr>
                <tr>
                    <td style="height:10px;text-align:justify"><%

                        String datosFuncionario[] = null;
                        String funcionario = null;
                        String cargo = null;
                        String email = null;
                        String filiales[] = null;
                        String extension = null;
                        String areaTrabajo = null;
                        String nombreDep = null;
                        String nombreComponente = null;
                        ArrayList lista = null;
                        Iterator it = null;
                        String mensaje = null;
                        try {
                            CComponentensSiamDAO cComponentensSiamDAO = new CComponentensSiamDAO();
                            lista = cComponentensSiamDAO.AdministradoresComponente(misitio);
                            it = lista.iterator();
                            while (it.hasNext()) {
                                CComponenteSiam cs = (CComponenteSiam) it.next();
                                datosFuncionario = cs.getFuncionario().split(":");
                                funcionario = datosFuncionario[1];
                                filiales = cs.getNombreFilial().split(",");
                                nombreComponente = cs.getNombreCompleto();
                                cargo = cs.getCargo();

                                email = cs.getEmail();
                                extension = cs.getExtension();
                                areaTrabajo = cs.getAreaTrabajo();
                                nombreDep = cs.getNombredep();
                                if (Integer.parseInt(misitio) != 10) {
                                    mensaje = "Usted esta en en el componente:" + filiales[filiales.length - 1];
                                } else {
                                    mensaje = "El " + nombreComponente + " hace parte del Sistema Informaci&oacute;n Ambiental de Colombia.  ";

                                }
                        %>
                        <p class="textnormal"><%=mensaje%>
                            Si requiere soporte para el uso del sistema o mas informaci&oacute;n sobre los  datos por favor dirigirse a:</p>
                        <p class="textnormal"><%=datosFuncionario[0]%>:<a href="mailto:<%=email%>"><%=funcionario%></a><br/>
                            <%=cargo%><br/>
                            Email:<a href="mailto:<%=email%>"><%=email%></a><br/>
                            Extension:(57) 54328600 <%=extension%>.  (Atendemos llamadas telef&oacute;nicas entre las 2 PM y las 5 PM  hora de Colombia -GMT&nbsp; -5). </p>
                            <%

                                    }
                                } catch (Exception e) {
                                    out.println("Error" + e.getMessage());
                                }
                            %></td>
                </tr>
            </table></div>
            <%@ include file="plantillaSitio/footer.jsp" %>
    </body>
</html>