<%@ page import="co.org.invemar.siam.sitio.model.CComponentensSiamDAO"%>
<%@ page import="co.org.invemar.siam.sitio.vo.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%

      Calendar c = Calendar.getInstance();
      String dia = Integer.toString(c.get(Calendar.DATE));
      String mes = Integer.toString(c.get(Calendar.MONTH)+1);
      String annio = Integer.toString(c.get(Calendar.YEAR));
	  String fecha = dia+"/"+mes+"/"+annio;
      String misitio=request.getParameter("idsitio");
	 
		
		String nombresitio=null;
		String nombrecorto = null;
        String urlConceptual = null;
		String urlsubportal           = null;
        
		if (misitio!=null){
        CComponentensSiamDAO micomponentesiam = new CComponentensSiamDAO();
        ArrayList list = micomponentesiam.AdministradoresComponente(misitio);
        Iterator it = list.iterator();
		
		while (it.hasNext()) {
		   CComponenteSiam cs = (CComponenteSiam)it.next();
           nombresitio  = cs.getNombreCompleto();
           nombrecorto  = cs.getNombreCorto();	
           urlConceptual = cs.getUrlConceptual();
		   urlsubportal  = cs.getUrl();
           }  
	}  
		
     	
%>
<%

 String idsubsitio=request.getParameter("idsubsitio");
 
 

%>
<div class="centrado">
<table width="963px" border="0" cellpadding="00" cellspacing="0" >
  <tr>
    <td height="56" colspan="3" align="left"><table width='921px' border="0" align="center" cellpadding="00" cellspacing="0">
      <tr>
        <td height="23" colspan="2" rowspan="2" align="right"><a href="http://twitter.com/#!/siam_colombia" target="_blank"></a></td>
        <td width="214"><span style="text-align:right" class="headerTexto">Ultima Actualizacion:<%=fecha%>&nbsp;&nbsp;&nbsp;Visitas:102</span></td>
        <td width="364"><a href="http://twitter.com/#!/siam_colombia" target="_blank"><img border="0" src="../../plantillaSitio/img/twitter.png" width="16" height="16" alt="twitter" /></a><a href="http://twitter.com/statuses/user_timeline/124202805.rss" target="_blank"><img  border="0" src="../../plantillaSitio/img/rss.png" width="16" height="16" alt="rss" /></a><a href="http://siam.invemar.org.co/siam/mapadelsitio.jsp" class="headerTexto" target="_blank">  &nbsp; Mapa de Sitio |</a><a target="_blank" class="headerTexto" href='http://www.invemar.org.co/pciudadania.jsp' > Servicio al Ciudadano |</a><a href="http://siam.invemar.org.co/siam/contactenos.jsp?idsitio=<%=misitio%>" target="_blank" class='headerTexto' >Cont&aacute;ctenos</a></td>
        <td  width="115" rowspan="2"   class="out" id="boton1" onmouseover="this.className='over'" onmouseout="this.className='out'"><a href="http://siam.invemar.org.co/siam/index.jsp"  class='opcionmenuprincipal'>Inicio</a><br /></td>
        <td  width="121" rowspan="2"  class="out" id="boton3" onmouseover="this.className='over'" onmouseout="this.className='out'" ><a href="<%=urlConceptual%>" class='opcionmenuprincipal' target="_blank">Desarrollo<br />
          Conceptual <%=nombrecorto%></a></td>
        <td  width="105" rowspan="2"  class='out' id="boton4" onmouseover="this.className='over'" onmouseout="this.className='out'" ><a href="http://www.invemar.org.co/psubcategorias.jsp?idsub=182&amp;idcat=104" class='opcionmenuprincipal' target="_blank">LABSIS</a></td>
      </tr>
      <tr>
        <td><div id="google_translate_element"></div>
          <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'es',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
      </script>
          <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> </td>
        <td></td>
      </tr>
    </table></td>
  </tr>
  <tr >
    <td height="25" colspan="3"><table width="963px" border="0" cellpadding="00" cellspacing="0" class="headermodulos">
      <tr>
        <td width="293" align="center"><table width="259" border="0" cellpadding="00">
          <tr>
            <td width="36">&nbsp;</td>
            <td width="217"><a href="http://www.invemar.org.co" target="_blank" ><img   src="../../plantillaSitio/img/icono_invemar.png" width="217" height="81" border='0' alt="icon invemar" longdesc="http://www.invemar.org.co" title="Pagina Principal de Invemar" /></a></td>
          </tr>
        </table></td>
        <td colspan="2" align="center" class="titulosubportal"><span class="titulosubportal nombrecortosubportal">-<%=nombrecorto%>- <%=nombresitio%></span>
          <table width="676" border="0" cellpadding="00">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td width="293" align="right" class="titulosubportal nombrecortosubportal">&nbsp;</td>
        <td width="351">&nbsp;</td>
        <td width="319" style='padding-bottom:0px;vertical-align:bottom'><div id="cse-search-form" style="width: 200px;">Loading</div>
          <script src="http://www.google.es/jsapi" type="text/javascript"></script>
          <script type="text/javascript"> 
  google.load('search', '1', {language : 'es'});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('009335532891503054452:wgboazck2um');
    customSearchControl.setResultSetSize(google.search.Search.LARGE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.enableSearchboxOnly("http://siam.invemar.org.co/siam/resultadobusqueda.jsp");
    customSearchControl.draw('cse-search-form', options);
  }, true);
      </script>
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="36" height="25">&nbsp;</td>
 <td width="936" align="right" ><a href="http://www.invemar.org.co/" class="migadepan">invemar.org.co &gt; </a><a href="http://siam.invemar.org.co/siam/index.jsp" class="migadepan">SIAM &gt; </a><a href="<%=urlsubportal%>" class="migadepan"><%=nombrecorto%></a></td>
 <td width="28" align="right" >&nbsp;</td>
  </tr>
</table>
</div>