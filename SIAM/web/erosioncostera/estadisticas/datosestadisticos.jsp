<%@page contentType="text/html; charset=iso-8859-1"%>
<%@page import="co.org.invemar.costero.DashBoardEstadisticasCosteroV2"%>
<%  DashBoardEstadisticasCosteroV2 dashBoardEstadisticasCostero = new DashBoardEstadisticasCosteroV2();
    String data = dashBoardEstadisticasCostero.getDataDashboardCostero();
    String perfilesporDefecto = null;
    String nombreproyecto = null;
    String nombreestacion = null;

    perfilesporDefecto = request.getParameter("perfiles");
    if (perfilesporDefecto == null) {
        perfilesporDefecto = "'perfil 100 - 1','perfil 100 - 2','perfil 100 - 3'";
    }

    nombreproyecto = request.getParameter("proyecto");
    if (nombreproyecto == null) {
        nombreproyecto = "geo_Evolución geohistorica Sierra Nevada de Santa Marta";
    }

    nombreestacion = request.getParameter("estacion");
    if (nombreestacion == null) {
        nombreestacion = "Santa Marta - Rodadero - Punta Gloria";
    }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>       
        <link href="css/estadisticas.css" rel="stylesheet" type="text/css" />     
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Datos de Granulometr&iacute;a (COSTERO) </title>
        <script type="text/javascript">
            // Load the Visualization API and the controls package.
            google.load('visualization', '1.0', {
                'packages':['controls']
            });
            
            // Set a callback to run when the Google Visualization API is loaded.
            google.setOnLoadCallback(drawDashboard);

            // Callback that creates and populates a data table, 
            // instantiates a dashboard, a range slider and a pie chart,
            // passes in the data and draws it.
            function drawDashboard() {

    
                var data = google.visualization.arrayToDataTable(<%=data%>);   
      
                var dashboard = new google.visualization.Dashboard(
                document.getElementById('dashboard_div'));

                // Define a category picker control for the Gender column
                var estPicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'estaciones',
                    
                    'options': {
                        'filterColumnLabel': 'ESTACION',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'allowMultiple': true,
                            'selectedValuesLayout': 'belowStacked'
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': [<%=perfilesporDefecto%>]
                    }
			
                });		

                // Define a category picker control for the PROYECTO column
                var proyPicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'nombreproyectos',
                    'options': {
                        'filterColumnLabel': 'NOMPROYECTO',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'allowMultiple': false
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': ['<%=nombreproyecto%>']
                    }
			
                });		

                // Define a category picker control for the SECTOR column
                var sectorPicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'sectores',
                    'options': {
                        'filterColumnLabel': 'SECTOR',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'allowMultiple': false
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': ['<%=nombreestacion%>']
                    }
			
                });		
		
                var fechasPicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'fechas',
                    'options': {
                        'filterColumnLabel': 'FECHA',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'allowMultiple': false
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': ['<%=nombreestacion%>']
                    }
			
                });		



                // Create a pie chart, passing some options para la data de población
                var areaChart2 = new google.visualization.ChartWrapper({
                    'chartType': 'ColumnChart',
                    'containerId': 'relativaestaciones',
                    'options': {
                        'width': 900,
                        'height': 400,
                        'legend': 'yes',
                        'title': 'Frecuencia Relativa de tamanos de grano por estacion',
                        'hAxis': {
                            'title': 'Estacion',
                            'slantedText': false,
                            'slantedTextAngle': 0,
                            'maxAlternation':1
                        },
                        'vAxis': {
                            'title': '%'
                        },
                        'fontSize': 11
			  
                        //			  'chartArea': {'left': 15, 'top': 15, 'right': 0, 'bottom': 0},
                        //			  'pieSliceText': 'label'
                    },
                    // Instruct the piechart to use colums 6 and 4
                    // from the 'data' DataTable.
                    'view': {
                        'columns': [2,8,9,10,11,12,13,14]
                    }
                });		

                // Create a pie chart, passing some options para la data de población
                var areaChart3 = new google.visualization.ChartWrapper({
                    'chartType': 'ColumnChart',
                    'containerId': 'acumuladaestaciones',
                    'options': {
                        'width': 900,
                        'height': 400,
                        'legend': 'yes',
                        'title': 'Frecuencia Acumulada de tamanos de grano por estacion',
                        'hAxis': {
                            'title': 'Estacion',
                            'slantedText': false,
                            'slantedTextAngle': 0,
                            'maxAlternation':1
                        },
                        'vAxis': {
                            'title': '%'
                        },
                        'fontSize': 11
			  
                        //			  'chartArea': {'left': 15, 'top': 15, 'right': 0, 'bottom': 0},
                        //			  'pieSliceText': 'label'
                    },
                    // Instruct the piechart to use colums 6 and 4
                    // from the 'data' DataTable.
                    'view': {
                        'columns': [2,15,16,17,18,19,20,21]
                    }
                });		
		  
                // Define a table
                var table = new google.visualization.ChartWrapper({
                    'chartType': 'Table',
                    'containerId': 'table1',
                    'options': {
                        //			  'width': '1500px'
                        'fontSize': 9

                    },
                    'view': {
                        'columns': [23, 2, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22]
                    }
                });		
		  
                google.visualization.events.addListener(proyPicker, 'statechange',desativarGraficas);               
                google.visualization.events.addListener(sectorPicker, 'statechange',desativarGraficas);
                
                google.visualization.events.addListener(fechasPicker, 'statechange',function () {
                    document.getElementById("mensaje").innerHTML='<em>Seleccion las estaciones</em>';
                    document.getElementById("mensaje").style.display='inline';
                    document.getElementById("pie").style.display='none';
                    document.getElementById("relativaestaciones").style.display='none';
                    document.getElementById("acumuladaestaciones").style.display='none';
                    document.getElementById("table1").style.display='none';  
                });
                
                google.visualization.events.addListener(estPicker, 'statechange',function () {
                    document.getElementById("mensaje").innerHTML='<em>Seleccion las estaciones</em>';
                    document.getElementById("mensaje").style.display='inline';
                    document.getElementById("pie").style.display='none';
                    document.getElementById("relativaestaciones").style.display='none';
                    document.getElementById("acumuladaestaciones").style.display='none';
                    document.getElementById("table1").style.display='none';  
                });
                
                google.visualization.events.addListener(estPicker, 'statechange',function () {
                    document.getElementById("mensaje").style.display='none';
                    document.getElementById("pie").style.display='inline';
                    document.getElementById("relativaestaciones").style.display='inline';
                    document.getElementById("acumuladaestaciones").style.display='inline';
                    document.getElementById("table1").style.display='inline';
                });
  
  
  
                dashboard.bind(proyPicker, sectorPicker);
                // dashboard.bind(sectorPicker, estPicker);
                dashboard.bind(sectorPicker, fechasPicker);
                dashboard.bind(fechasPicker, estPicker);
                //dashboard.bind([estPicker, proyPicker], [areaChart2, areaChart3,table]);
                dashboard.bind([estPicker], [areaChart2, areaChart3,table]);
                dashboard.draw(data);
            }
        
            function desativarGraficas() {
                document.getElementById("mensaje").innerHTML='<em>Seleccione sector y luego estaciones</em>';
                document.getElementById("mensaje").style.display='inline';
                document.getElementById("pie").style.display='none';
                document.getElementById("relativaestaciones").style.display='none';
                document.getElementById("acumuladaestaciones").style.display='none';
                document.getElementById("table1").style.display='none';  
            }

            function click(url) {
                window.open(url);
            } 
            
            function exportarAExcel() {
                
                datosdeldiv=document.getElementById("table1").innerHTML;                            
                document.exporta.mihtml.value=datosdeldiv;
                document.exporta.submit();
                
            }
        </script>
    </head>
    <body>
        <div id="contenedorestadistico">
            <div id="cabecera">
                <h1>Datos de Granulometr&iacute;a (COSTERO)</h1>

            </div>           
            <div id="menu">
                <div id="nombreproyectos" ></div>
                <div id="sectores" ></div>
                <div id="fechas" ></div>                
                <div id="estaciones"></div>
            </div>

            <div id="dashboard_div">
                <div id="mensaje"></div>
                <div id="principal">
                    <div id="relativaestaciones" ></div>
                </div>
                <div id="secundario">
                    <div id="acumuladaestaciones" ></div>
                </div>
            </div>

            <div id="pie">
                <div  id="exportar" style="text-align:right;width:600px;margin:auto;cursor:pointer">
                    <img src="img/exportaraexcel.png" onclick="exportarAExcel();" />
                    <a href="" onclick="exportarAExcel();">Exportar A Excel.</a>
                    <form action="exportaraExcel.jsp" method="post" name="exporta" >
                        <input type="hidden" id="mihtml" name="mihtml" value="" />
                    </form>
                </div>
                <div id="table1" ></div>
            </div>
        </div>

    </body>
</html>