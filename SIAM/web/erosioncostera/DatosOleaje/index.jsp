<%-- 
    Document   : index
    Created on : 13/09/2012, 01:38:23 PM
    Author     : usrsig15
--%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.OutputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="java.util.ArrayList"%>
<%@page import="co.org.invemar.siam.erosioncostera.SimulacionOleaje"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%


    String estacion = request.getParameter("estacion");
    int cantidaddatos = 0;
    if (estacion != null) {


        SimulacionOleaje simulacionoleaje = new SimulacionOleaje();

        HSSFWorkbook wb = simulacionoleaje.getByteArchivoDatosSimulacionEstacion(request.getRealPath("/erosioncostera/DatosOleaje/tempFile/"), estacion);
        cantidaddatos = simulacionoleaje.getCantidadDatos();
        if (cantidaddatos != 0) {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=Datos.xls");
            response.setHeader("Cache-Control", "");
            response.setHeader("Pragma", "");

            ServletOutputStream out1 = response.getOutputStream();

            try {
                wb.write(out1);

            } catch (IOException ioe) {
            }
            out1.flush();
            out1.close();
        }
    }

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Datos Simulaci&oacute;n Oleaje</title>
        <link type="text/css" rel="stylesheet" href="css/erosion.css"/>            
    </head>
    <body>
        <%if (estacion == null || cantidaddatos == 0) {

        %>
        <div id="contenedor">
            <img src="../img/logocostero.png" />
            <div id="message">

                No hay datos brutos de la simulaci&oacute;n.
            </div>
        </div>
        <%            }
        %>
    </body>
</html>
