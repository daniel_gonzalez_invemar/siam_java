<META name="Keywords" CONTENT="Calidad Ambiental Marina, Bioremediacion, Biodegradacion, Bacterias marinas, Cepario, Bacterias ">
<%@ page import="java.lang.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="conecccbm.*" %>
<%@ page import="ccbm_cepas.*" %>
<%
  ccbm_cepas.Ccbm_cepas     var  = new ccbm_cepas.Ccbm_cepas();
  ccbm_cepas.Ccbm_contcepas cvar = new ccbm_cepas.Ccbm_contcepas();
  ccbm_cepas.Ccbm_contcepas cdepar = new ccbm_cepas.Ccbm_contcepas();
  ccbm_cepas.Ccbm_contcepas cdegrada = new ccbm_cepas.Ccbm_contcepas();
  ccbm_cepas.Ccbm_contcepas cmorfo = new ccbm_cepas.Ccbm_contcepas();
  ccbm_cepas.Ccbm_contcepas cgram = new ccbm_cepas.Ccbm_contcepas();


  conecccbm.Cconexionccbm   con2 = new conecccbm.Cconexionccbm();
  Connection                conn2 = null;

  String ndepar     = request.getParameter("ndepar");
  String ndegrada   = request.getParameter("ndegrada");
  String nmorfo     = request.getParameter("nmorfologia");
  String ngram      = request.getParameter("ngram");


  conn2 = con2.getConn();

  cdepar.contenedorndepar(conn2);
  cdegrada.contenedorndegrada(conn2);
  cmorfo.contenedornmorfologia(conn2);
  cgram.contenedorngram(conn2);

  if(ngram != null || ndepar != null || nmorfo != null || ndegrada != null) {
    cvar.contenedor(conn2, ndepar, ndegrada, nmorfo, ngram);
   }



  int ind = 0;

if (con2!= null)
    con2.close();
%>

<html>
<head>
<title>M�dulo de Consultas - CCBM</title>
</head>

<body bgcolor="#FFFFFF" text="#666666" link="#0033CC" vlink="#006666" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form action="ccbm_consulta.jsp" method="post" name="formaccbm" id="formaccbm">

  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#123F5E">
    <tr>
      <td><a href="ccbm_consulta.jsp"><img src="images/ccbm_head_der.jpg" height="100" border="0"></a></td>
      <td>&nbsp;</td>
      <td><div align="right"><img src="images/ccbm_head_izq.jpg" height="100"></div></td>
    </tr>
  </table>
  <div align="left">
<table width="100%" border="0" cellpadding="3" cellspacing="0">
<tr>
        <td width="100%">
          <div align="left">
            <p><font size="2" face="Arial, Helvetica, sans-serif">En esta p&aacute;gina
                encontrar&aacute; informaci&oacute;n de
              las caracter&iacute;sticas macrosc&oacute;picas y microsc&oacute;picas de las
              bacterias nativas marinas de Colombia aisladas en el marco del
              proyecto financiado
              por Colciencias C&oacute;digo N&deg; <strong>2105-09-13524</strong> y
              registrado en el Museo de Historia Natural Marina de Colombia (MHNCM).</font></p>
          </div>          
          <table width="65%" height="244" border="0" align="left">
            <tr bgcolor="#123F5E"> 
              <td height="8" colspan="6"><div align="left"><font color="#CCCCCC" size="1" face="Arial, Helvetica, sans-serif">Las</font><font color="#CCCCCC" size="1" face="Arial, Helvetica, sans-serif"> 4
                    listas desplegables con los par�metros de consulta
                      (Departamento de aislamiento, compuesto que degradan, morfologia
                      y Gram de las bacterias) se pueden seleccionar de acuerdo
              al interes de b�squeda de forma individual o combinada.</font></div></td>
            </tr>
            <tr> 
              <td width="2%" rowspan="4" bgcolor="#123F5E">&nbsp;</td>
              <td height="8" colspan="4" bordercolor="#000066" bgcolor="#CCCCCC"><b><font color="#99CCFF" face="Verdana, Arial, Helvetica, sans-serif" size="-1"><font color="#FF0000">::</font> 
                <font color="#333333">Par&aacute;metros de Consulta</font>                  <font color="#FF0000">::</font></font></b></td>
              <td width="2%" rowspan="4" bgcolor="#123F5E">&nbsp;</td>
            </tr>
            <tr> 
              <td width="16%" height="24" bordercolor="#000066" bgcolor="#999999"><font color="#FFFFFF" size="-1" face="Verdana, Arial, Helvetica, sans-serif">Departamento</font></td>
              <td width="20%" bordercolor="#000066" bgcolor="#E6E6E6"> <select name="ndepar">
                  <option value="">--</option>
                  <%
                      for (int j=0;j<cdepar.gettamano();j++){
                          var = new ccbm_cepas.Ccbm_cepas();
                          var = cdepar.getccbm_cepas(j);
                    %>
                  <option value="<%=var.get_departamento()%>"><%=var.get_departamento()%></option>
                  <%}%>
                    
                </select></td>
              <td width="13%" bordercolor="#000066" bgcolor="#999999"><font color="#FFFFFF" size="-1" face="Verdana, Arial, Helvetica, sans-serif">Degrada</font></td>
              <td width="47%" bordercolor="#000066" bgcolor="#E6E6E6"><select name="ndegrada" id="select">
                  <option value="">--</option>
                  <%
                      for (int j=0;j<cdegrada.gettamano();j++){
                          var = new ccbm_cepas.Ccbm_cepas();
                          var = cdegrada.getccbm_cepas(j);
                    %>
                  <option value="<%=var.get_degrada()%>"><%=var.get_degrada()%></option>
                  <%}%>
                </select>
</td>
            </tr>
            <tr> 
              <td bordercolor="#000066" bgcolor="#999999"><font color="#FFFFFF" size="-1" face="Verdana, Arial, Helvetica, sans-serif">Morfolog&iacute;a</font></td>
              <td bordercolor="#000066" bgcolor="#E6E6E6"> <select name="nmorfologia" id="select5">
                  <option value="">--</option>
                  <%
                      for (int j=0;j<cmorfo.gettamano();j++){
                          var = new ccbm_cepas.Ccbm_cepas();
                          var = cmorfo.getccbm_cepas(j);
                    %>
                  <option value="<%=var.get_morfologia()%>"><%=var.get_morfologia()%></option>
                  <%}%>
              </select></td>
              <td bordercolor="#000066" bgcolor="#999999"><font color="#FFFFFF" size="-1" face="Verdana, Arial, Helvetica, sans-serif">Gram</font></td>
              <td height="2" bordercolor="#000066" bgcolor="#E6E6E6"> <select name="ngram" id="select5">
                  <option value="">--</option>
                  <%
                      for (int j=0;j<cgram.gettamano();j++){
                          var = new ccbm_cepas.Ccbm_cepas();
                          var = cgram.getccbm_cepas(j);
                    %>
                  <option value="<%=var.get_gram()%>"><%=var.get_gram()%></option>
                  <%}%>
                </select></td>
            </tr>
            <tr> 
              <td height="10" colspan="4" bordercolor="#000066" bgcolor="#CCCCCC"> 
                <div align="right"><font size="-2"> 
                  <input type="submit" name="Submit" value="Buscar">
              </font></div></td>
            </tr>
            <tr bgcolor="#123F5E"> 
              <td height="21" colspan="6"><div align="right"><font size="-2"> 
              </font></div></td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td width="100%" valign="top"> 
          <table width="65%" border="0">
            <tr> 
              <td colspan="5
			  " height="37"><font face="Arial, Helvetica, sans-serif" size="-1">Listado de cepas registradas en el sistema</font></td>
            </tr>
            <tr bgcolor="#CCCCCC"> 
              <td width="230"> <div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"> 
                  <font color="#CCFFFF">::</font> Depto<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
              <td width="155"> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">C&oacute;digo</font><font face="Verdana, Arial, Helvetica, sans-serif"><b><font color="#CCFFFF">::</font></b> 
                  </font></b></font></div></td>
              <td> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Morfolog&iacute;a</font><font face="Verdana, Arial, Helvetica, sans-serif"><b><font color="#CCFFFF">::</font></b> 
                  </font></b></font></div></td>
              <td width="173"><div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font> 
                  Gram<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
              <td width="173"><div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font> 
                  Degrada<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
            </tr>

            <%
              for(ind = 0; ind < cvar.gettamano(); ind++){
                  var = new ccbm_cepas.Ccbm_cepas();
                  var = cvar.getccbm_cepas((int)ind);
            %>
            <tr> 
              <td width="230" bgcolor="#F4F4F4"><font size="1" face="Arial, Helvetica, sans-serif">
              <%if (var.get_departamento()!=null) {%>
                  <%=var.get_departamento()%> 
              <%}%>
              </font></td>
              <td width="155" bgcolor="#F4F4F4"><font size="1" face="Arial, Helvetica, sans-serif"><b><a href="ccbm_infocepa.jsp?nnumcepa=<%=var.get_numcepa()%>" title="Clic para ver informaci�n de la cepa">CCBM-<%=var.get_numcepa()%></a></b></font></td>
              <td width="137" bgcolor="#F4F4F4" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_morfologia()%></font></td>
              <td width="173" bgcolor="#F4F4F4"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_gram()%></font></td>
              <td width="173" bgcolor="#F4F4F4"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_degrada()%></font></td>
            </tr>
            <% } %>
            <tr> 
              <td width="230">&nbsp;</td>
              <td width="155">&nbsp;</td>
              <td width="137">&nbsp;</td>
              <td width="173">&nbsp;</td>
              <td width="173">&nbsp;</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</form>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#123F5E">
    <tr>
      <td><div align="right"><a href="mailto:snarvaez@invemar.org.co">  <span class="Tfichacepa"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif"><strong>cont@ctenos</strong></font> </span></a></div></td>
    </tr>
</table>
<table width=100% border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td><img src="images/shim.gif" width="5" height="5"></td>
  </tr>
  <tr> 
    <td bgcolor="#71A3CD"><img src="images/shim.gif" width="2" height="2"></td>
  </tr>
  <tr> 
    <td><img src="images/shim.gif" width="5" height="5"></td>
  </tr>
</table>
<table width=100% border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="24"><a href="http://www.invemar.org.co" target="_new"><img src="images/logo_invemar_small.gif" width="24" height="27" alt="INVEMAR" border="0"></a></td>
    <td width="616"> 
      <div align="center"><font size="1" face="Arial, Helvetica, sans-serif" color="#999999">&copy; 
        Derechos reservados Instituto de Investigaciones Marinas y Costeras �Jos� Benito Vives De Andr�is"-INVEMAR 2005<br>
        Este sitio esta optimizado para resoluciones de 800 x 600 pixeles y para Internet Explorer 5</font></div>
    </td>
    <td width="25"> 
      <div align="right"><a href="http://www.colciencias.gov.co/" target="_new"><img src="images/colciencias_color_small.gif" width="24" height="24" border="0" alt="Ministerio del Medio Ambiente"></a></div>
    </td>
  </tr>
  <tr>
    <td colspan="3"><img src="images/shim.gif" width="5" height="5"></td>
  </tr>
  <tr bgcolor="#71A3CD">
    <td colspan="3"><img src="images/shim.gif" width="2" height="2"></td>
  </tr>
</table>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2300620-1");
pageTracker._trackPageview();
} catch(err) {}
</script>

</body>
</html>