
<%@ page import="java.lang.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="conecccbm.*" %>
<%@ page import="ccbm_cepas.*" %>
<%

    ccbm_cepas.Ccbm_cepas var = new ccbm_cepas.Ccbm_cepas();
    ccbm_cepas.Ccbm_contcepas cvar = new ccbm_cepas.Ccbm_contcepas();
    ccbm_cepas.Ccbm_contcepas cdepar = new ccbm_cepas.Ccbm_contcepas();
    ccbm_cepas.Ccbm_contcepas cdegrada = new ccbm_cepas.Ccbm_contcepas();
    ccbm_cepas.Ccbm_contcepas cmorfo = new ccbm_cepas.Ccbm_contcepas();
    ccbm_cepas.Ccbm_contcepas cgram = new ccbm_cepas.Ccbm_contcepas();

    conecccbm.Cconexionccbm con2 = new conecccbm.Cconexionccbm();
    Connection conn2 = null;

    String ndepar = request.getParameter("ndepar");
    String ndegrada = request.getParameter("ndegrada");
    String nmorfo = request.getParameter("nmorfologia");
    String ngram = request.getParameter("ngram");

    conn2 = con2.getConn();
    int ind = 0;
    try {
        cdepar.contenedorndepar(conn2);
        cdegrada.contenedorndegrada(conn2);
        cmorfo.contenedornmorfologia(conn2);
        cgram.contenedorngram(conn2);

        if (ngram != null || ndepar != null || nmorfo != null || ndegrada != null) {
            cvar.contenedor(conn2, ndepar, ndegrada, nmorfo, ngram);
        }





        if (con2 != null) {
            con2.close();
        }
    } catch (Exception e) {
    }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
    <head>
        <title>M�dulo de Consultas - CCBM</title>
        <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css" />
        <script type="text/javascript" src="js/monitoreo.js" ></script>
        <meta name="Keywords" content="Calidad Ambiental Marina, Bioremediacion, Biodegradacion, Bacterias marinas, Cepario, Bacterias " />       
    </head>

    <body >
        <form action="ccbm_consultaPV1.jsp" method="post" name="formaccbm" id="formaccbm">

            <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td><jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=46"/></td>
                </tr>
            </table>
            <div align="center">
                <table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
                    <tr>
                        <td width="963px">

                            <table width="963px"  border="0" align="center">            
                                <tr bgcolor="#8DCCE3">
                                    <td height="8" colspan="6" bgcolor="#0077AF"><div align="center">
                                            <p style="text-align:justify;color:#FFF"><span style="font-size:0.93em;font-family:Arial, Helvetica, sans-serif">En esta p&aacute;gina
                                                    encontrar&aacute; informaci&oacute;n de
                                                    las caracter&iacute;sticas macrosc&oacute;picas y microsc&oacute;picas de las
                                                    bacterias nativas marinas de Colombia aisladas en el marco del
                                                    proyecto "Selecci�n y aplicaci�n de bacterias marinas con capacidad degradadora de compuestos org�nicos persistentes (COP) en el Pac�fico y Caribe Colombiano" financiado
                                                    por Colciencias C&oacute;digo N&deg; <strong>2105-09-13524</strong> y
                                                    registrado en el Museo de Historia Natural Marina de Colombia (MHNCM).
                                                    <br><strong>Ultima Actualizaci�n 18/sep/2005<strong></span></p>
                                                                </div>   </td>
                                                                </tr>
                                                                <tr bgcolor="#8DCCE3"> 
                                                                    <td height="8" colspan="6" bgcolor="#0077AF"><div align="left"><span style="color:#CCCCCC;font-size:0.83em;font-family:Arial, Helvetica, sans-serif">Las</span><span style="color:#CCCCCC;font-size:0.83em;font-family:Arial, Helvetica, sans-serif"> 4 listas desplegables con los par�metros de consulta
                                                                                (Departamento de aislamiento, compuesto que degradan, morfologia
                                                                                y Gram de las bacterias) se pueden seleccionar de acuerdo
                                                                                al interes de b�squeda de forma individual o combinada.</span></div></td>
                                                                </tr>
                                                                <tr> 
                                                                    <td width="2%" rowspan="4" bgcolor="#0077AF">&nbsp;</td>
                                                                    <td height="8" colspan="4" bgcolor="#CCCCCC"><strong><span style="color:#333333;font-family:Arial, Helvetica, sans-serif;font-size:0.92em">Par&aacute;metros de Consulta</span></strong></td>
                                                                    <td width="2%" rowspan="4" bgcolor="#0077AF">&nbsp;</td>
                                                                </tr>
                                                                <tr> 
                                                                    <td width="16%" height="24" bgcolor="#999999"><span style="color:#FFFFFF;font-size:1em;font-family:Arial, Helvetica, sans-serif">Departamento</span></td>
                                                                    <td width="20%"  bgcolor="#E6E6E6"> <select name="ndepar" title="Departamento">
                                                                            <option value="">--</option>
                                                                            <%
                                                                                for (int j = 0; j < cdepar.gettamano(); j++) {
                                                                                    var = new ccbm_cepas.Ccbm_cepas();
                                                                                    var = cdepar.getccbm_cepas(j);
                                                                            %>
                                                                            <option value="<%=var.get_departamento()%>"><%=var.get_departamento()%></option>
                                                                            <%}%>

                                                                        </select></td>
                                                                    <td width="13%"  bgcolor="#999999"><span style="color:#FFFFFF;font-size:1em;font-family:Arial, Helvetica, sans-serif">Degrada</span></td>
                                                                    <td width="47%"  bgcolor="#E6E6E6"><select name="ndegrada" id="select" title="degrada">
                                                                            <option value="">--</option>
                                                                            <%
                                                                                for (int j = 0; j < cdegrada.gettamano(); j++) {
                                                                                    var = new ccbm_cepas.Ccbm_cepas();
                                                                                    var = cdegrada.getccbm_cepas(j);
                                                                            %>
                                                                            <option value="<%=var.get_degrada()%>"><%=var.get_degrada()%></option>
                                                                            <%}%>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr> 
                                                                    <td  bgcolor="#999999"><span style="color:#FFFFFF;font-size:1em;font-family:Arial, Helvetica, sans-serif">Morfolog&iacute;a</span></td>
                                                                    <td bgcolor="#E6E6E6"> <select name="nmorfologia" id="select5" title="morfologia">
                                                                            <option value="">--</option>
                                                                            <%
                                                                                for (int j = 0; j < cmorfo.gettamano(); j++) {
                                                                                    var = new ccbm_cepas.Ccbm_cepas();
                                                                                    var = cmorfo.getccbm_cepas(j);
                                                                            %>
                                                                            <option value="<%=var.get_morfologia()%>"><%=var.get_morfologia()%></option>
                                                                            <%}%>
                                                                        </select></td>
                                                                    <td  bgcolor="#999999"><span style="color:#FFFFFF;font-size:1em;font-family:Arial, Helvetica, sans-serif">Gram</span></td>
                                                                    <td height="2"  bgcolor="#E6E6E6"><select name="ngram" title="Gram">
                                                                            <option value="">--</option>
                                                                            <%
                                                                                for (int j = 0; j < cgram.gettamano(); j++) {
                                                                                    var = new ccbm_cepas.Ccbm_cepas();
                                                                                    var = cgram.getccbm_cepas(j);
                                                                            %>
                                                                            <option value="<%=var.get_gram()%>"><%=var.get_gram()%></option>
                                                                            <%}%>
                                                                        </select></td>
                                                                </tr>
                                                                <tr> 
                                                                    <td height="10" colspan="4" bgcolor="#CCCCCC"> 
                                                                        <div align="right">
                                                                            <input type="submit" name="Submit" value="Buscar"/>
                                                                        </div></td>
                                                                </tr>
                                                                <tr bgcolor="#123F5E"> 
                                                                    <td height="21" colspan="6" bgcolor="#0077AF"><div align="right"></div></td>
                                                                </tr>
                                                                </table>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="100%" valign="top"> 
                                                                        <table width="963px" border="0" align="center">
                                                                            <tr> 
                                                                                <td colspan="5
                                                                                    " height="37"><span style="font-size:1em;font-family:Arial, Helvetica, sans-serif">Listado de cepas registradas en el sistema</span></td>
                                                                            </tr>
                                                                            <tr bgcolor="#CCCCCC"> 
                                                                                <td width="230"> <div align="center"><span style="font-size:0.92em;color:#333333;font-family:Arial, Helvetica, sans-serif"><strong>Depto</strong></span></div></td>
                                                                                <td width="155"> <div align="center"><span style="font-size:0.92em;color:#333333;font-family:Arial, Helvetica, sans-serif"><strong>C&oacute;digo</strong></span></div></td>
                                                                                <td> <div align="center"><span style="font-size:0.92em;color:#333333;font-family:Arial, Helvetica, sans-serif"><strong>Morfolog&iacute;a</strong></span></div></td>
                                                                                <td width="173"><div align="center"><span style="font-size:0.92em;color:#333333;font-family:Arial, Helvetica, sans-serif"><strong>Gram</strong></span></div></td>
                                                                                <td width="173"><div align="center"><span style="font-size:0.92em;color:#333333;font-family:Arial, Helvetica, sans-serif"><strong>Degrada</strong></span></div></td>
                                                                            </tr>

                                                                            <%
                                                                                for (ind = 0; ind < cvar.gettamano(); ind++) {
                                                                                    var = new ccbm_cepas.Ccbm_cepas();
                                                                                    var = cvar.getccbm_cepas((int) ind);
                                                                            %>
                                                                            <tr> 
                                                                                <td width="230" bgcolor="#F4F4F4"><span style="font-size:0.83em;font-family:Arial, Helvetica, sans-serif">
                                                                                        <%if (var.get_departamento() != null) {%>
                                                                                        <%=var.get_departamento()%> 
                                                                                        <%}%>
                                                                                    </span></td>
                                                                                <td width="155" bgcolor="#F4F4F4"><span style="font-size:0.83em;font-family:Arial, Helvetica, sans-serif"><strong><a href="ccbm_infocepaPV1.jsp?nnumcepa=<%=var.get_numcepa()%>" title="Clic para ver informaci�n de la cepa">CCBM-<%=var.get_numcepa()%></a></strong></span></td>
                                                                                <td width="137" bgcolor="#F4F4F4" height="20"><span style="font-size:0.83em;font-family:Arial, Helvetica, sans-serif"><%=var.get_morfologia()%></span></td>
                                                                                <td width="173" bgcolor="#F4F4F4"><span style="font-size:0.83em;font-family:Arial, Helvetica, sans-serif"><%=var.get_gram()%></span></td>
                                                                                <td width="173" bgcolor="#F4F4F4"><span style="font-size:0.83em;font-family:Arial, Helvetica, sans-serif"><%=var.get_degrada()%></span></td>
                                                                            </tr>
                                                                            <% }%>
                                                                            <tr>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                            <tr> 
                                                                                <td colspan="5"><%@ include file="../plantillaSitio/footermodulesV3.jsp" %></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </table>
                                                                </div>
                                                                </form>
                                                                <table width="101%" border="0" cellpadding="0" cellspacing="0" >
                                                                    <tr>
                                                                        <td><div align="right"></div></td>
                                                                    </tr>
                                                                </table>
                                                                <table width='100%' border="0" cellspacing="0" cellpadding="0" align="center">
                                                                    <tr> 
                                                                        <td width="63">&nbsp;</td>
                                                                        <td width="42"> 

                                                                        </td>
                                                                        <td width="960"></td>
                                                                        <td width="107"> 
                                                                            <div align="right"></div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4"><img src="images/shim.gif" width="5" height="5" alt="" /></td>
                                                                    </tr>
                                                                    <tr bgcolor="#71A3CD">
                                                                        <td colspan="4"><img src="images/shim.gif" width="2" height="2" alt="" /></td>
                                                                    </tr>
                                                                </table>
                                                                <h1></h1>
                                                                </body>
                                                                </html>