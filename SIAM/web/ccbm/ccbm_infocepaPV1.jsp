<%@ page import="java.lang.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="conecccbm.*" %>
<%@ page import="ccbm_cepas.*" %>
<%
  ccbm_cepas.Ccbm_cepas     var  = new ccbm_cepas.Ccbm_cepas();
  ccbm_cepas.Ccbm_contcepas cvar = new ccbm_cepas.Ccbm_contcepas();
  ccbm_cepas.Ccbm_contcepas censa = new ccbm_cepas.Ccbm_contcepas();
  ccbm_cepas.Ccbm_contcepas cfoto = new ccbm_cepas.Ccbm_contcepas();

  conecccbm.Cconexionccbm   con2 = new conecccbm.Cconexionccbm();
  Connection                conn2 = null;

  String nnumcepa     = request.getParameter("nnumcepa");
  conn2 = con2.getConn();

  if(nnumcepa != null) {
    cvar.contenedor_infocepa(conn2, nnumcepa);
    censa.contenedor_infoensayos(conn2, nnumcepa);
    cfoto.contenedor_infofoto(conn2, nnumcepa);
   }

int ind = 0;

if (con2!= null)
    con2.close();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>M�dulo de Consultas - CCBM</title>
<link href="ccbm_estilos.css" rel="stylesheet" type="text/css" />
 <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css" />
</head>

<body >
<table width="100%" border="0" cellpadding="0" cellspacing="0" >
    <tr>
      <td><jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=46"/></td>
    </tr>
</table>
  <div align="left">
<table width="100%" border="0" cellpadding="3" cellspacing="0" >
      <tr>
        <td width="100%" height="160" valign="top"> 
          
          <table width="90%" border="0" align="center">
            <%
              for(ind = 0; ind < cvar.gettamano(); ind++){
                  var = new ccbm_cepas.Ccbm_cepas();
                  var = cvar.getccbm_cepas((int)ind);
            %>
            <tr>
              <td height="20" colspan="2" class="Tfila"><div align="left">
            <p><font size="2" face="Arial, Helvetica, sans-serif">En esta p&aacute;gina
                encontrar&aacute; informaci&oacute;n de
              las caracter&iacute;sticas macrosc&oacute;picas y microsc&oacute;picas de las
              bacterias nativas marinas de Colombia aisladas en el marco del
              proyecto financiado
              por Colciencias C&oacute;digo N&deg; <strong>2105-09-13524</strong> y
              registrado en el Museo de Historia Natural Marina de Colombia (MHNCM).</font></p>
          </div></td>
            </tr>
            <tr>
              <td height="20" colspan="2" class="Tfila"><font face="Arial, Helvetica, sans-serif" size="-1"><strong>Ficha descriptiva de la cepa</strong></font><strong></strong></td>
            </tr>
            <tr>
              <td height="20" colspan="2" class="Tfila"><div align="center"><font size="+2">CCBM-<%=var.get_numcepa()%></font></div></td>
            </tr>
            <tr>
              <td width="190" height="20" class="Tfila">DEPARTAMENTO</td>
              <td width="379" class="Tfichacepa">
			  <%if (var.get_departamento()!=null) {%>
                  <%=var.get_departamento()%> 
              <%}%>
			  </td>
            </tr>
            <tr>
              <td height="20" class="Tfila">estacion</td>
              <td class="Tfichacepa">
			  <%if (var.get_estacion()!=null) {%>
                  <%=var.get_estacion()%> 
              <%}%>
			  </td>
            </tr>
            <tr>
              <td height="20" class="Tfila">nombre</td>
              <td class="Tfichacepa"><i>
			  <%if (var.get_genero()!=null) {%> <%=var.get_genero()%> <%}%> 
			  <%if (var.get_especie()!=null) {%> <%=var.get_especie()%> <%}%>
			  </i>
			  </td>
            </tr>
            <tr>
              <td height="20" class="Tfila">fuente</td>
              <td class="Tfichacepa"><%=var.get_fuente()%></td>
            </tr>
            <tr>
              <td height="20" class="Tfila">fecha aislamiento</td>
              <td class="Tfichacepa"><%=var.get_fecha()%> <font size="-2">(DD / MM / AAAA)</font></td>
            </tr>
            <tr>
              <td height="20" class="Tfila">morfolog&iacute;a</td>
              <td class="Tfichacepa"><%=var.get_morfologia()%></td>
            </tr>
            <tr>
              <td height="20" class="Tfila">gram</td>
              <td class="Tfichacepa"><%=var.get_gram()%></td>
            </tr>
            <tr>
              <td height="20" class="Tfila">conservado en</td>
              <td class="Tfichacepa"><%=var.get_conservacion()%></td>
            </tr>
            <tr>
              <td height="20" class="Tfila">Registro MHNMC</td>
              <td class="Tfichacepa"><%=var.get_registrada()%></td>
            </tr>
            <% } %>
          </table>
          
		  <br/>
		  <br/>

		<table width="90%" border="0" align="center">
            <tr>
              <td height="20" colspan="3" class="Tfila"><div align="center">TIENE CAPACIDAD PARA DEGRADAR...</div></td>
            </tr>
            <tr>
              <td height="20" class="Tfichacepa"><strong><font size="-1">Elemento</font></strong></td>
              <td class="Tfichacepa"><div align="center"><font size="-1"><strong>CMI minimo</strong></font></div></td>
              <td class="Tfichacepa"><div align="center"><font size="-1"><strong>CMI maximo</strong></font></div></td>
            </tr>
            <%
              for(ind = 0; ind < censa.gettamano(); ind++){
                  var = new ccbm_cepas.Ccbm_cepas();
                  var = censa.getccbm_cepas((int)ind);
            %> <tr>
              <td width="379" height="20" class="Tfichacepa">
			  <%if (var.get_degrada()!=null) {%> <%=var.get_degrada()%> <%}%>
			  </td>
              <td width="379" class="Tfichacepa"><div align="center">
                <%if (var.get_min()!=null) {%> 
                <%=var.get_min()%>                <%}%>
              </div></td>
              <td width="379" class="Tfichacepa"><div align="center">
                <%if (var.get_max()!=null) {%> 
                <%=var.get_max()%>                <%}%>
              </div></td>
            </tr>
            <% } %>
          </table>
		  
		  <br/><br/>

		<table width="90%" border="0" align="center">
            <tr>
              <td height="20" colspan="3" class="Tfila"><div align="center">FOTOGRAF&Iacute;AS</div></td>
            </tr>
            <tr>
              <td width="50%" height="20" class="Tfichacepa"><div align="center"><strong><font size="-1">Foto</font></strong></div></td>
              <td width="10%" class="Tfichacepa"><div align="center"><font size="-1"><strong>Tipo</strong></font></div></td>
              <td class="Tfichacepa"><div align="center"><font size="-1"><strong>Descripci�n</strong></font></div></td>
            </tr>
            
            <%
              for(ind = 0; ind < cfoto.gettamano(); ind++){
                  var = new ccbm_cepas.Ccbm_cepas();
                  var = cfoto.getccbm_cepas((int)ind);
            %><tr>
              <td width="379" height="20" class="Tfichacepa">
				<img alt="" src="<%=var.get_directorio_foto()%>/<%=var.get_archivo_foto()%>" width="100%" />			  </td>
              <td width="379" class="Tfichacepa"><div align="center">
                <%if (var.get_tipo_foto()!=null) {%> <%=var.get_tipo_foto()%> <%}%>
              </div></td>
              <td width="379" class="Tfichacepa"><div align="center">
                <%if (var.get_descripcion_foto()!=null) {%>
                <%=var.get_descripcion_foto()%>
                <%}%>
              </div></td>
            </tr>
            <% } %>
          </table>			  
        <hr size="1" />
</td>
      </tr>
    </table>
  </div>

<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#123F5E">
    <tr>
      <td bgcolor="#0077AF"><div align="right"></div></td>
    </tr>
  </table>


<table width='100%' border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="24">&nbsp;</td>
    <td width="616"> 
      <%@ include file="../plantillaSitio/footermodulesV3.jsp" %>
    </td>
    <td width="25"> 
      <div align="right"></div>
    </td>
  </tr>
  <tr>
    <td colspan="3"><img src="images/shim.gif" width="5" height="5" alt="" /></td>
  </tr>
  <tr bgcolor="#71A3CD">
    <td colspan="3"><img src="images/shim.gif" width="2" height="2" alt="" /></td>
  </tr>
</table>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2300620-1");
pageTracker._trackPageview();
} catch(err) {}
</script>

</body>
</html>