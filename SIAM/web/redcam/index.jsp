<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
    <head>
        <title>REDCAM</title>
        <link href="../plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8859-1">
            <link type="text/css" href="../libreriascomunes/css/redmond/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
            <script type="text/javascript" src="../libreriascomunes/js/jquery-1.8.0.min.js"></script>
            <script type="text/javascript" src="js/monitoreo.js"></script>
            <script type="text/javascript" src="../libreriascomunes/js/jquery-ui-1.8.23.custom.min.js"></script>
            <script>
                $(function() {
                    // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
                    $( "#dialog:ui-dialog" ).dialog( "destroy" );
	
                    $( "#dialog-modal" ).dialog({
                        height: 380,
                        width:770,
                        modal: true
                    });                
                
                    $( "#tabs" ).tabs();            

                
                });
            </script>

    </head>
    <body>
        <div id="dialog-modal" title="Sistema de estad&iacute;sticas REDCAM -Colombia">    
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">INTRODUCCI&Oacute;N</a></li>
                    <li><a href="#tabs-2">INFORMACI&Oacute;N GENERAL</a></li>
                    <li><a href="#tabs-3">CONSIDERACIONES DE USO</a></li>
                    <li><a href="#tabs-4">D&Iacute;GANOS QUE PIENSA</a></li>
                </ul>
                <div id="tabs-1">
                    <p style="text-align: justify;font-size: 0.92em">La Red de Monitoreo de la Calidad de aguas marinas y costeras de Colombia (REDCAM), es un programa de car&aacute;cter interinstitucional y es una herramienta &uacute;til para la gesti&oacute;n y toma de decisiones del Ministerio del Ambiente y Desarrollo Sostenible (MADS) y de las Corporaciones Autonomas Regionales con injerencia costeras y en general del SINA, en temas de contaminaci&oacute;n marina-costera.<br/>


                    </p>
                </div>
                <div id="tabs-2" style="text-align: justify;font-size: 0.92em">
                    <p>La informaci&oacute;n aqu&iacute; contenida proviene de los nodos de la REDCAM, como son MADS, CIOH, CORPONARI�O, CRC, CVC, CODECHOC&Oacute;, CORPOGUAJIRA,    CORPAMAG, CRA, CARDIQUE, CARSUCRE, CVS, CORPOURAB&Aacute;, CORALINA, EPA Cartagena, que desde el a�o 2001 han aportado datos al sistema de monitoreo, adem�s de la informaci&oacute;n primaria colectada durante los monitoreos de la REDCAM liderados por el INVEMAR. De esta manera la REDCAM se ha convertido en un programa pionero en su clase no solo en Colombia, sino en otros pa&iacute;ses de la regi&oacute;n. </p>
                </div>
                <div id="tabs-3" style="text-align: justify;font-size: 0.92em">
                    <p>No se garantiza al usuario la precisi&oacute;n global de los datos, es advertido de posibles errores en los datos suministrados, y asume la responsabilidad de realizar los chequeos necesarios para detectarlos, corregirlos e interpretarlos de manera correcta. El usuario hace uso de los datos bajo su propia responsabilidad y acepta las limitaciones existentes en los datos.<br/><br/>
                        El usuario debe reconocer que el conjunto de datos suministrado est� sujeto a las leyes que reglamentan los derechos de autor y de propiedad intelectual y se comprometen a respetarlas.<br/><br/>
                        Si tiene alguna sugerencia o comentario por favor cont�ctese con labsis@invemar.org.co.<br/>
                    </p>

                </div>
                <div id="tabs-4" style="text-align: justify;font-size: 0.92em">
                    <p>Una vez usado o revisado nuestros servicios,  llene esta <a href="https://spreadsheets0.google.com/embeddedform?formkey=dHB1ZHdIVk5nbEs0VlRNaW9VdjU2X0E6MA" target="_blank" style="color:red">Encuesta de satisfacci�n de usuarios  </a>para enviar una queja, mejora o sugerencias de los servicios ofrecidos <br/></br>
                        La encuesta de satisfacci�n es una herramienta para mejorar los servicios ofrecidos por la Red de vigilancia de calidad ambiental marina REDCAM.<br/></br>
<span style="text-align: center">Tu opini&oacute; es importante. Ay&uacute;danos a mejorar nuestro servicios.</span>
                        
                        
                    </p>
                </div>
            </div>
        </div>
        <jsp:include page="../plantillaSitio/headermodulosv3.jsp?idsitio=60"/> 
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td></td>
            </tr>
        </table>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr bgcolor="#8DC0E3">
                <td width="21">&nbsp;</td>
                <td width="729" bgcolor="#8DC0E3" class="linksnegros">Red de Calidad Ambiental
                    Marina REDCAM</td>

                <td width="729" bgcolor="#8DC0E3" class="linksnegros">&nbsp;</td>
            </tr>
        </table>

        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td ></td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0" style="">
            <tr style="" >
                <td width="8" bgcolor="B5D7DE">&nbsp;</td>
                <td width="53"  bgcolor="B5D7DE" class="texttablas" ><a href="http://cinto.invemar.org.co/argos/services?proy=2050&action=GenericProyecto"><img src="images/map-48x48.png" alt="" width="48" height="48" border='0' /></a></td>
                <td width="678"  bgcolor="B5D7DE" class="texttablas" ><a href="http://cinto.invemar.org.co/argos/services?proy=2050&action=GenericProyecto" target="_blank" >Estaciones de
                        monitoreo </a></td>
                <td width="11" bgcolor="B5D7DE">&nbsp;</td>
            </tr>
            <tr style="">
                <td bgcolor="#E6E6E6">&nbsp;</td>
                <td colspan="2" bgcolor="#E6E6E6"><table border="0" cellpadding="0" cellspacing="0" width="730">
                        <tr style="display:none">
                            <td width="16"><a href="#" class="texttablas"><img src="../images/grey_arrow.jpg" width="16" height="16" border="0" alt="" /></a></td>
                            <td width="714"><a href="http://cinto.invemar.org.co/argos/services?proy=2050&action=GenericProyecto" target="_blank" class="texttablas" style="display:none">Listado
                                    de estaciones de monitoreo 
                                    o sectores.</a></td>
                        </tr>
                    </table></td>
                <td bgcolor="#E6E6E6">&nbsp;</td>
            </tr>
        </table>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0" style="display:none;">
            <tr style="diplay:none">
                <td width="8" bgcolor="B5D7DE">&nbsp;</td>
                <td width="53"  bgcolor="B5D7DE" class="texttablas" ><a href="estacioncampPV1.jsp"><img src="images/map-48x48.png" alt="" width="48" height="48" border='0' /></a></td>
                <td width="678"  bgcolor="B5D7DE" class="texttablas" >Estaciones de
                    monitoreo </td>
                <td width="11" bgcolor="B5D7DE">&nbsp;</td>
            </tr>
            <tr>
                <td bgcolor="#E6E6E6">&nbsp;</td>
                <td colspan="2" bgcolor="#E6E6E6"><table border="0" cellpadding="0" cellspacing="0" width="730">
                        <tr>
                            <td width="16"><a href="#" class="texttablas"><img src="../images/grey_arrow.jpg" width="16" height="16" border="0" alt="" /></a></td>
                            <td width="714"><a href="estacioncampPV1.jsp" target="_blank" class="texttablas">Listado
                                    de estaciones de monitoreo agrupadas por &aacute;reas geogr&aacute;ficas
                                    o sectores.</a></td>
                        </tr>
                    </table></td>
                <td bgcolor="#E6E6E6">&nbsp;</td>
            </tr>
        </table>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td ></td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="10" bgcolor="B5D7DE">&nbsp;</td>
                <td width="53" bgcolor="B5D7DE" class="texttablas" valign="middle" ><a href="estadisticas/tiposvariables.jsp"><img src="images/science2.png" alt="" width="48" height="48" border='0' /></a></td>
                <td width="678" bgcolor="B5D7DE" class="texttablas" valign="middle" ><a href="estadisticas/tiposvariables.jsp" target="_blank">Variables
                        muestreadas </a></td>
                <td width="10" bgcolor="B5D7DE">&nbsp;</td>
            </tr>
            <tr>
                <td bgcolor="#E6E6E6">&nbsp;</td>
                <td colspan="2" bgcolor="#E6E6E6"><table border="0" cellpadding="0" cellspacing="0" width="730">
                        <tr style="display:none">
                            <td width="16"><a href="#" class="texttablas"><img src="../images/grey_arrow.jpg" width="16" height="16" border='0' alt="" /></a></td>
                            <td> <a href="estadisticas/tiposvariables.jsp" target="_blank" class="texttablas">Listado de variables muestreadas en los monitoreos.</a></td>
                        </tr>
                    </table></td>
                <td bgcolor="#E6E6E6">&nbsp;</td>
            </tr>
        </table>


        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td ></td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="10" bgcolor="B5D7DE">&nbsp;</td>
                <td width="53" bgcolor="B5D7DE" class="texttablas" valign="middle" ><a href="estadisticas/index.jsp"><img src="images/Volume Manager.png"  alt="" width="48" height="48"  border='0' /></a></td>
              <td width="678" bgcolor="B5D7DE" class="texttablas" valign="middle" ><a href="estadisticas/index.jsp" target="_blank">M&oacute;dulo
                        de estad&iacute;sticas: consulta de datos</a></td>
                <td width="10" bgcolor="B5D7DE">&nbsp;</td>
            </tr>
            <!--
              <tr>
                <td bgcolor="#E6E6E6" height="27">&nbsp;</td>
                <td colspan="2" bgcolor="#E6E6E6" height="27"><table border="0" cellpadding="0" cellspacing="0" width="730">
                  <tr>
                    <td width="16"><a href="#" class="texttablas"><img src="../images/grey_arrow.jpg" width="16" height="16" border="0"  alt='' /></a></td>
                    <td> <a href="estadisticas/parametros.jsp" target="_blank" class="texttablas">Filtre, busque y vea el comportamiento de las variales en las estaciones monitoreadas.</a></td>
                  </tr>
                </table></td>
                <td bgcolor="#E6E6E6" height="27">&nbsp;</td>
              </tr>
            -->
            <tr>
                <td bgcolor="#E6E6E6">&nbsp;</td>
                <td colspan="2" bgcolor="#E6E6E6"><table border="0" cellpadding="0" cellspacing="0" width="730">
                        <tr style="display:none">
                            <td width="16"><a href="#" class="texttablas"></a></td>
                            <td> <a href="estadisticas/index.jsp"  class="texttablas">Filtre
                                    y busque las estad&iacute;sticas
                                    precalculadas de los par&aacute;metros predefinidos.</a></td>
                        </tr>
                    </table></td>
                <td bgcolor="#E6E6E6">&nbsp;</td>
            </tr>

        </table>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="10" bgcolor="B5D7DE">&nbsp;</td>
                <td width="53" bgcolor="B5D7DE" class="texttablas" valign="middle" ><a href="indicadores/index.jsp"><img src="images/semaforo.gif"  alt="" width="48" height="48"  border='0' /></a></td>
                <td width="678" bgcolor="B5D7DE" class="texttablas" valign="middle" ><a href="indicadores/index.jsp" target="_blank">Indicadores: Indicador de la Calidad de las Aguas Marinas - ICAM</a></td>
                <td width="10" bgcolor="B5D7DE">&nbsp;</td>
          </tr>
            <!--
              <tr>
                <td bgcolor="#E6E6E6" height="27">&nbsp;</td>
                <td colspan="2" bgcolor="#E6E6E6" height="27"><table border="0" cellpadding="0" cellspacing="0" width="730">
                  <tr>
                    <td width="16"><a href="#" class="texttablas"><img src="../images/grey_arrow.jpg" width="16" height="16" border="0"  alt='' /></a></td>
                    <td> <a href="estadisticas/parametros.jsp" target="_blank" class="texttablas">Filtre, busque y vea el comportamiento de las variales en las estaciones monitoreadas.</a></td>
                  </tr>
                </table></td>
                <td bgcolor="#E6E6E6" height="27">&nbsp;</td>
              </tr>
            -->
            <tr>
                <td bgcolor="#E6E6E6">&nbsp;</td>
                <td colspan="2" bgcolor="#E6E6E6"><table border="0" cellpadding="0" cellspacing="0" width="730">
                        <tr style="display:none">
                            <td width="16"><a href="#" class="texttablas"></a></td>
                            <td> <a href="indicadores/index.jsp"  class="texttablas">Filtre
                                    y busque las estad&iacute;sticas
                                    precalculadas de los par&aacute;metros predefinidos.</a></td>
                        </tr>
                    </table></td>
                <td bgcolor="#E6E6E6">&nbsp;</td>
            </tr>

        </table>

        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td ></td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>

        <!-- ACA VA EL ENLACE DEL ICAM 
        <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10" bgcolor="B5D7DE">&nbsp;</td>
            <td width="53" bgcolor="B5D7DE" class="texttablas" valign="middle" ><a href="icam/index.jsp"><img src="images/semaforo.gif" alt="" width="48" height="48" valign="middle" border='0' /></a></td>
            <td width="678" bgcolor="B5D7DE" class="texttablas" valign="middle" >:: Indicador de la Calidad de las Aguas Marinas - ICAM::</td>
            <td width="10" bgcolor="B5D7DE">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#E6E6E6">&nbsp;</td>
            <td colspan="2" bgcolor="#E6E6E6"><table border="0" cellpadding="0" cellspacing="0" width="730">
              <tr>
                <td width="16"><a href="#" class="texttablas"><img src="../images/grey_arrow.jpg" width="16" height="16" border="0" alt='' /></a></td>
                <td> <a href="icam/icam.html" target="_blank" class="texttablas">Filtre, busque y vea en el mapa el ICAM para los par&aacute;metros indicados.</a></td>
              </tr>
            </table></td>
            <td bgcolor="#E6E6E6">&nbsp;</td>
          </tr>
        </table>
        <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td><img src="../images/spacer.gif" width="5" height="5" alt='' /></td>
          </tr>
          <tr>
            <td ></td>
          </tr>
          <tr>
            <td><img src="../images/spacer.gif" width="5" height="5" alt=0'' /></td>
          </tr>
        </table>
-->
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="10" bgcolor="B5D7DE">&nbsp;</td>
                <td width="53" bgcolor="B5D7DE" class="texttablas" valign="middle" ><a href="login.htm"  target="_blank"><img src="images/login_48.png" alt="" width="48" height="48"  border='0' /></a></td>
                <td width="678" bgcolor="B5D7DE" class="texttablas" valign="middle" ><a href="login.htm" target="_blank">Ingreso de datos </a></td>
                <td width="10" bgcolor="B5D7DE">&nbsp;</td>
            </tr>
            <tr>
                <td bgcolor="#E6E6E6">&nbsp;</td>
                <td colspan="2" bgcolor="#E6E6E6"><table border="0" cellpadding="0" cellspacing="0" width="730">
                        <tr>
                            <td width="16"><img src="../images/grey_arrow.jpg" width="16" height="16" border="0" alt="login" /></td>
                            <td> Solo
                                para ingresar, actualizar y consultar datos puntuales obtenidos en
                                cada uno de los muestreos, filtrando la informaci&oacute;n. <br/>
                                <strong><font color="#0066CC">(Solo disponible para usuarios registrados en el sistema)</font></strong></td>
                        </tr>
                    </table></td>
                <td bgcolor="#E6E6E6">&nbsp;</td>
            </tr>
        </table>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td ></td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="10" bgcolor="B5D7DE">&nbsp;</td>
                <td width="53" bgcolor="B5D7DE" class="texttablas" valign="middle" ><a href="http://gis.invemar.org.co/redcam_colombia/" target="_blank"><img src="images/browser-48x48.png" width="48" height="48"  alt="cartografia dinamica" /></a></td>
                <td width="678" bgcolor="B5D7DE" class="texttablas" valign="middle" ><a href="http://gis.invemar.org.co/redcam_colombia/" target="_blank">Cartograf&iacute;a
                        din&aacute;mica en l&iacute;nea </a></td>
                <td width="10" bgcolor="B5D7DE">&nbsp;</td>
            </tr>
            <tr style="display:none;">
                <td bgcolor="#E6E6E6">&nbsp;</td>
                <td colspan="2" bgcolor="#E6E6E6">
                    <table border="0" cellpadding="0" cellspacing="0" width="730">
                        <tr>
                            <td width="16"><img style="display:none;" src="../images/grey_arrow.jpg" width="16" height="16" border="0" alt="calidad sanitaria de las playas"  /></td>
                            <td> Calidad
                                sanitaria de las playas
                            </td>
                        </tr>
                    </table>
                </td>
                <td bgcolor="#E6E6E6">&nbsp;</td>
            </tr>
            <tr>
                <td bgcolor="#E6E6E6">&nbsp;</td>
                <td colspan="2" bgcolor="#E6E6E6">
                    <table border="0" cellpadding="0" cellspacing="0" width="730">
                        <tr>
                            <td width="16"><img src="../images/grey_arrow.jpg" width="16" height="16" border="0" alt="calidad de las aguas marinas y costera de colombia" /></td>
                            <td> Calidad de aguas marinas y costeras de Colombia 
                            </td>
                        </tr>
                    </table>
                </td>
                <td bgcolor="#E6E6E6">&nbsp;</td>

            </tr>

        </table>

        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td ></td>
            </tr>
            <tr>
                <td><img src="../images/spacer.gif" width="5" height="5" alt="" /></td>
            </tr>
        </table>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td bgcolor="#8DC0E3"></td>
            </tr>
        </table>
        <%@ include file="../plantillaSitio/footermodulesV3.jsp" %>


    </body>
</html>