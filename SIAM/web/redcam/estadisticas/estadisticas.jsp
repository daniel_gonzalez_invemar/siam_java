<%@page import="java.net.URLEncoder"%>
<%@page import="co.org.invemar.siam.redcam.NivelesEstadisticos"%>
<%@page import="java.io.BufferedOutputStream"%>
<%@page import="com.sun.org.apache.bcel.internal.generic.AALOAD"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.File"%>
<%@page import="co.org.invemar.siam.redcam.VariableGeog"%>
<%@page import="co.org.invemar.siam.redcam.ServicioEstadistica"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@ page import="co.org.invemar.siam.redcam.coneccam.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%  request.setCharacterEncoding("UTF-8");
    String nuser = "ljarias";
    String exportaAExcel = "false";

    VariableGeog var = new VariableGeog();
    ServicioEstadistica servicioEstadistica = new ServicioEstadistica();

    ConexionCam con2 = new ConexionCam();
    Connection conn2 = null;
    conn2 = con2.getConn();

    String opcion = request.getParameter("opcion");
    String nproy = request.getParameter("nproy");
    String nconsulta = request.getParameter("nconsulta");
    String ntipo = request.getParameter("ntipo");
    String ndepar = request.getParameter("ndepar");
    String nsector = request.getParameter("nsector");
    String nsector1 = request.getParameter("nsector1");
    String ntemporada = request.getParameter("ntemporada");
    String nprofundidad = request.getParameter("nprofundidad");
    String nsustrato = request.getParameter("nsustrato");
    String nregion = request.getParameter("nregion");
    String zona   =request.getParameter("zona");
    String tam   = request.getParameter("tam");
    String nivel = request.getParameter("nivel");
    String sampletypesubstratum = request.getParameter("tsm");
    
    
    String ano1 = "", ano2 = "";
    if (request.getParameter("ano1") != null && !request.getParameter("ano1").equals("")) {
        ano1 = request.getParameter("ano1");
    }
    if (request.getParameter("ano2") != null && !request.getParameter("ano2").equals("")) {
        ano2 = request.getParameter("ano2");
    }

    /*
     * recuperacion de la cadena de variables CADE
     */
    String cadena = request.getParameter("cadena");
    String cad = "";
    Vector cade = new Vector();
    if (cadena != null && !cadena.equals("")) {
        cad = cadena;
        String cadena1 = "";
        for (int h = 0; h < cad.length(); h++) {
            if (cad.charAt(h) == ',') {
                cade.addElement(cadena1);
                cadena1 = "";
            } else {
                cadena1 = cadena1 + cad.charAt(h);
            }
        }
       
    }
  
    /*
     * recuperacion de la cadena de estaciones SCADE
     */
    String cadest = request.getParameter("cadest");
    String scad = "";
    Vector scade = new Vector();
    if (cadest != null && !cadest.equals("")) {
        scad = cadest;
        String cadest1 = "";
        for (int z = 0; z < scad.length(); z++) {
            if (scad.charAt(z) == ',') {
                scade.addElement(cadest1);
                cadest1 = "";
            } else {
                cadest1 = cadest1 + scad.charAt(z);
            }
        }
    }

    
    servicioEstadistica.generaEstadistica(Integer.parseInt(nivel),nuser, "REDCAM", ntipo, ndepar, nsector, ntemporada, ano1, ano2, cade, "S", "SA", scade, nregion,zona,sampletypesubstratum);
    NivelesEstadisticos nivelestadistico = servicioEstadistica.getNivelestadistico();
    
        if (servicioEstadistica!= null &&  servicioEstadistica.gettamano() == 0) {
            out.println("<div id='msnestadistica' class='mensaje'");
            out.println("<span style=''><img src='../../plantillaSitio/img/message.png' /> Nivel estad&iacute;stico b&uacute;scado: Cod:" + nivelestadistico.getCod() + " -" + nivelestadistico.getDescripcion() + "</span><br/>");
            out.println("<span style=''>");
            out.println("No hay estad&iacute;sticas para esta variable.");
            out.println("</span>");
            out.println("</div>");

        } else {
            out.println("<div class='mensaje' >");
            out.println("<span >Nivel estad&iacute;stico b&uacute;scado:" + nivelestadistico.getCod() + " -" + nivelestadistico.getDescripcion() + "</span>");
            out.println("</div>");           
        }


%>
<table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">                   
    <tr >
        <td width="963px"  >
            <p style="font-weight:bold;font-size:1.0em;color:black;font-family: Arial">Listado
                de datos por ubicaci&oacute;n geogr&aacute;fica<br/><span style="font-size:0.8em;color:black;font-family: Arial">
                    La informaci&oacute;n aca representada, corresponde al proyecto REDCAM,
                    en datos
                    tomados en AGUA a profundidad SOMERA</span><br/>
            <p style="font-size:1.0em;color:red;font-family: Arial">Epoca (1 = Seca, 2= Lluviosa)</p></td>
    </tr>

</table>        
<div align="left">
    <table width="100%" border="0" cellpadding="3" cellspacing="0" height="150"
           align="center">
        <td height="13" colspan="6">
            <div align="left"><font face="Arial, Helvetica, sans-serif">
                <strong>
                    <font color="#FF0000">
                    <%=servicioEstadistica.gettamano()%> 
                    </font></strong><font color="#000066">registro(s) encontrado(s)</font></font></div>
        </td>
        <tr colspan="10">
            <td>
                <%
                 String s =cadena+"&tsm="+sampletypesubstratum+"&cadest="+cadest+"&nregion="+nregion+"&ndepar="+ndepar+"&nsector="+nsector+"&ntipo="+ntipo+"&ntemporada="+ntemporada+"&ano1="+ano1+"&ano2="+ano2+"&nproy="+nproy+"&opcion="+opcion+"&nuser="+nuser+"&tam="+tam+"&zona="+zona+"&nivel="+nivel;
                 %>
                <a href="/siam/ServicioComponentes?componente=redcam&action=ExportStatistic&cadena=<%=s%>" ><img src="../images/excel2013.png" alt="Excel " width="60px" height="60px" title="Descargar datos en Excel" /></a>
            </td>
        </tr>

        <tr>
            <td width="100%" height="71" valign="top"> 
                <table width="1849" border="0">
                    <tr bgcolor="#CCCCCC">
                        <td width="54"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif">Region</font></b></font></td> 
                        <td width="126"> <div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"> 
                                    Depto<b></b></font></b></font></div></td>
                        <td width="164"> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Sector<b></b></font></b></font></div></td>
                        <td width="164"> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Zona<b></b></font></b></font></div></td>
                        <td colspan="2"> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Estacion<b></b></font></b></font></div></td>
                        <td width="34"><div align="center"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>A&ntilde;o</b></font></div></td>
                        <td width="48"><div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"> 
                                    Temporada</font></b></font></div></td>
                        <td width="43"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif">Sustrato</font></b></font></td>
                        <td width="43"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif">Tipo Sustrato Muestra</font></b></font></td>
                        <td width="43">
                            <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Variable<b></b></font></b></font></div>
                        </td>
                        <td width="126"><div align="right"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif">Prom</font></b></font><font size="1" color="#333333"><b></b></font></div></td>
                        <td width="28"> <div align="right"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif">N</font></b></font></div></td>
                        <td width="96"><div align="right"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif">Max</font></b></font><font size="1" color="#333333"><b></b></font></div></td>
                        <td width="76"><div align="right"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif">Min</font></b></font></div></td>
                        <td width="93"><div align="right"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>StdDv</b></font></div></td>
                        <td width="90"><div align="right"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>Error</b></font></div></td>
                        <td width="77"><div align="right"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>Mediana</b></font></div></td>
                        <td width="83"><div align="right"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>Moda</b></font></div></td>
                        <td width="69"><div align="right"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>Varianza</b></font></div></td>
                        <td width="63"><div align="right"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif">Cuart_10</font></b></font></div></td>
                        <td width="63"><div align="right"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif">Cuart_25</font></b></font></div></td>
                        <td width="54"><div align="right"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>Cuart_75</b></font></div></td>
                        <td width="66"><div align="right"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif">Cuart_90</font></b></font></div></td>
                    </tr>

                    <%
                        for (int ind = 0; ind < servicioEstadistica.gettamano(); ind++) {
                            var = servicioEstadistica.getvariablegeog((int) ind);
                    %>
                    <tr bgcolor="#F7F7F7">
                    </tr>
                    <tr bgcolor="#E9E9E9"> 
                        <td width="54"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">
                            <% if (var.get_region() != null) {%>
                            <%=var.get_region()%> 
                            <%}%>
                            </font></td> 
                        <td width="126"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">
                            <% if (var.get_depto() != null) {%>
                            <%=var.get_depto()%> 
                            <%}%>
                            </font></td>
                        <td width="164"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">
                            <% if (var.get_sector() != null) {%>
                            <%=var.get_sector()%> 
                            <%}%>
                            </font></td>
                         <td width="164"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">
                            <% if (var.getZona()!= null) {%>
                            <%=var.getZona()%> 
                            <%}%>
                            </font></td>
                        <td width="39" height="20"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">
                            <% if (var.get_codest() != null) {%>
                            <%=var.get_codest()%> 
                            <%}%>
                            </font></td>
                        <td width="178" height="20"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">
                            <% if (var.get_nomest() != null) {%>
                            <%=var.get_nomest()%> 
                            <%}%>
                            </font></td>
                        <td width="34"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">
                            <% if (var.get_ano() != null) {%>
                            <%=var.get_ano()%> 
                            <%}%>
                            </font></td>
                        <td width="48"><div align="center"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">
                                <% if (var.get_temporada() != null) {%>
                                <%=var.get_temporada()%> 
                                <%}%>
                                </font></div></td>
                        <td><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_sustrato()%></font></td>
                        <td><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.getSampletypesubstratum()%></font></td>
                        <td height="20"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_nombre()%></font></td>
                        <td height="20"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_prom()%></font></div></td>
                        <td height="20"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_num()%></font></div></td>
                        <td height="20"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_max()%></font></div></td>
                        <td width="76"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_min()%></font></div></td>
                        <td width="93"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_stddv()%></font></div></td>
                        <td width="90"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_error()%></font></div></td>
                        <td width="77"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_mediana()%></font></div></td>
                        <td width="83"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_moda()%></font></div></td>
                        <td width="69"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_varianza()%></font></div></td>
                        <td width="63"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_cuart10()%></font></div></td>
                        <td width="63"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_cuart25()%></font></div></td>
                        <td width="54"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_cuart75()%></font></div></td>
                        <td width="66"><div align="right"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=var.get_cuart90()%></font></div></td>
                    </tr>
                    <%
                            //fin del cliclo FOR
                        }
                    %>
                </table>
                <hr size="1">
            </td>
        </tr>
    </table>
</div>

<table width="963px" border="0" align="left" cellpadding="0" cellspacing="0">
    <tr>
        <td></td>
    </tr>
    <tr>
        <td ></td>
    </tr>
    <tr>
        <td></td>
    </tr>
</table>
<table width="963px" border="0" align="left" cellpadding="0" cellspacing="0">
    <tr>
        <td bgcolor="#8DC0E3"></td>
    </tr>
</table>

