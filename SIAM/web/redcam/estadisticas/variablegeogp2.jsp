
<%@page import="co.org.invemar.siam.redcam.ServiceVariableRedCam"%>
<%@page import="co.org.invemar.siam.redcam.TipoVariable"%>
<%@page import="co.org.invemar.siam.redcam.DepartamentosVo"%>
<%@page import="co.org.invemar.siam.redcam.ServiceDepartamentos"%>
<%@ page import="coneccam.*" %>
<%@ page import="cvariablegeog.*" %>
<%@ page import="utilidades.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>.::Sistema de Informaci&oacute;n Ambiental Marina de Colombia - SIAM::.</title>
        <meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, REDCAM, INVEMAR, mapas" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="cache-control" content="no-cache" />
        <script type="text/javascript" src="js/jquery.min.js"></script>   
        <link href="../../plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" >
            
            function llenarParametrosOpcionales( ) {    
                 
                var vari = document.formacam.cadena.value;
                var ndepar = document.formacam.ndepar.value;
                var nsector = document.formacam.nsector.value;
                var tipovariable = document.formacam.ntipo.value;
                
                if(ndepar==undefined ){
                    ndepar="null";
                }
                if(nsector==undefined ){
                    nsector="null";
                }  
                if(tipovariable==undefined ){
                    tipovariable="null";
                } 
                if(vari==undefined ){
                    vari="null";
                }  
                
                
               
               
       	        $.ajax({
                    type: "POST",
                    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                    url:"parametrosProfundidadPorEstaciones.jsp",
                    data: "tipovariable="+tipovariable+"&variable="+vari+"&departamento="+ndepar+"&sector="+nsector,
                    beforeSend: function(msg){                            
                        $('#panelprofundidad').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='img/loader.gif' alt='Loading...'/>");
  
                    },
                    success: function(msg){
                        $('#panelprofundidad').html('');
                        $("#panelprofundidad").html(msg);
                

                    },
                    error: function(xml,msg){
                           
                        $("#panelprofundidad").html('Error cargando la profundidad...');
                  
                    }
                });               
            }
          
            function BuscarYExportarAExcel() {
                
                document.getElementById("exportar").value="true";         
                
                
              
                var departamento= document.formacam.ndepar.value;
                var tipovariable = document.formacam.ntipo.value;
                if (departamento=="" || tipovariable=="") {
                    alert('Se requiere parametros de b�squedas minimos.Los campos en rojos son obligatorios.');
                }else{
                    document.forms.formacam.submit();
                }
                /* var exportar ="true";
                var cadest = document.formacam.cadena.value;  
                var ndepar = document.formacam.ndepar.value;
                var nsector = document.formacam.nsector.value;
                var tipovariable = document.formacam.ntipo.value;                 
                var vari = document.formacam.cadena.value;               
                var temporada = document.formacam.ntemporada.value;
                var profundidad = document.formacam.nprofundidad.value;
                var dia1 = document.formacam.dia1.value;
                var mes1 = document.formacam.mes1.value;
                var ano1 = document.formacam.ano1.value;
                var dia2 = document.formacam.dia1.value;
                var mes2 = document.formacam.mes1.value;
                var ano2 = document.formacam.ano1.value;
                
                
                
                
                $.ajax({
                    type: "POST",
                    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                    url:"variablegeogp2.jsp",
                    data: "exportar="+exportar+"&cadest="+cadest+"&ndepar="+ndepar+"&nsector="+nsector+"&tipovariable="+tipovariable&+"&vari="+vari+"&temporada="+temporada+"&profunidad="+profundidad+"&dia1="+dia1+"&mes1="+mes1+"&ano1="+ano1+"&dia2="+dia2+"&mes2="+mes2+"&ano2="+ano2,
                    beforeSend: function(msg){                            
                        $('#panelcarga').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='img/loader.gif' alt='Loading...'/>");
  
                    },
                    success: function(msg){
                        $('#panelcarga').html('');
                        $("#panelcarga").html(msg);
                

                    },
                    error: function(xml,msg){
                           
                        $("#panelcarga").html('Error cargando el archivo excel...');
                  
                    }
                }); */      
            }
            
            
        </script>
    </head>
    <%
        HttpSession session1 = request.getSession(true);
        String usu = (String) session.getAttribute("usu_id1");
        if (usu == null) {%>
    <script language="JavaScript">
        alert("No tiene permisos para ingresar a esta p�gina");
        window.parent.location="../login.htm";
    </script>
    <%       }
    %>

    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <jsp:include page="../../plantillaSitio/headermodulosv3.jsp?idsitio=60"/>
        <div  id="menu" style="margin:auto;width:963px;">
            <a href="menuOpcionesPrivadas.jsp"><img src="../images/back.png" alt="" />Regresar al menu opciones </a>| <a href="menuOpcionesPrivadas.jsp?salir=ok"><img src="../images/botonsalir.png" alt="" />Salir de forma segura.</a>
        </div>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0"> 
            <tr>
                <td colspan="2"><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td>
            </tr>
            <tr bgcolor="#8DC0E3">
                <td width="21" height="20"></td>
                <td width="729" bgcolor="#8DC0E3" class="linksnegros">Red de Calidad Ambiental
                    Marina REDCAM :: M&oacute;dulo de b&uacute;squeda de Microdatos</td>

            </tr>
        </table>



        <%

            String ntipo = request.getParameter("ntipo");
            String ndepar = request.getParameter("ndepar");
            String nsector = request.getParameter("nsector");
            String ntemporada = request.getParameter("ntemporada");
            String nprofundidad = request.getParameter("nprofundidad");
            String nsustrato = request.getParameter("nsustrato");
            String exportaAExcel = request.getParameter("exportar");          

            ServiceDepartamentos serviceDepartamentos = new ServiceDepartamentos();
            ArrayList departamentos = serviceDepartamentos.getTodosDepartamentos();


            cvariablegeog.Cvariablegeog var = new cvariablegeog.Cvariablegeog();
            cvariablegeog.Ccontvariablegeog cvar = new cvariablegeog.Ccontvariablegeog();       


            coneccam.Cconexioncam con2 = new coneccam.Cconexioncam();
            Connection conn2 = con2.getConn();

            Calendar fecha1 = null;
            Calendar fecha2 = null;
            int xx = 0;
            String ano1 = "", mes1 = "", dia1 = "", ano2 = "", mes2 = "", dia2 = "";
            if (request.getParameter("ano1") != null && !request.getParameter("ano1").equals("")) {
                ano1 = request.getParameter("ano1");
                mes1 = request.getParameter("mes1");
                dia1 = request.getParameter("dia1");
                fecha1 = new GregorianCalendar(Integer.parseInt(ano1), Integer.parseInt(mes1), Integer.parseInt(dia1));
                xx = fecha1.get(Calendar.MONTH);
            }
            if (request.getParameter("ano2") != null && !request.getParameter("ano2").equals("")) {
                ano2 = request.getParameter("ano2");
                mes2 = request.getParameter("mes2");
                dia2 = request.getParameter("dia2");
                fecha2 = new GregorianCalendar(Integer.parseInt(ano2), Integer.parseInt(mes2), Integer.parseInt(dia2));
            }

            float ind = 0;
            int tam = 0;
            int paginas = 1;

            //pagina actual del resultado 1= valor inicial
            int pag = 1;

            //numero de registros por pagina
            float registros = 10;
            String pagina = request.getParameter("pagina");


            int cont = 0;
            int h = 0;
            utilidades.Cutilidades util = new utilidades.Cutilidades();

            String cadena = request.getParameter("cadena");
            String cad = "";
            Vector cade = new Vector();

            if (cadena != null && !cadena.equals("")) {
                cad = cadena;
                String cadena1 = "";
                int kl = 0;
                for (h = 0; h < cad.length(); h++) {
                    if (cad.charAt(h) == ',') {
                        cade.addElement(cadena1);
                        cadena1 = "";
                    } else {
                        cadena1 = cadena1 + cad.charAt(h);
                    }
                }
            }

            /*recuperacion de la cadena de estaciones*/
            String cadest = request.getParameter("cadest");
            String scad = "";
            Vector scade = new Vector();
            int z = 0;
            if (cadest != null && !cadest.equals("")) {
                scad = cadest;
                String cadest1 = "";
                //    int zl =0;
                for (z = 0; z < scad.length(); z++) {
                    if (scad.charAt(z) == ',') {
                        scade.addElement(cadest1);
                        cadest1 = "";
                    } else {
                        cadest1 = cadest1 + scad.charAt(z);
                    }
                }
            }


            if (ntipo != null || ndepar != null || nsector != null || ntemporada != null) {
                cvar.setPathFile(request.getRealPath("/redcam/estadisticas/tempFile/"));
                cvar.contenedor(conn2, ntipo, ndepar, nsector, ntemporada, fecha1, fecha2, cade, nprofundidad, nsustrato, scade);

            }




        %>

        <script language="JavaScript">
            function validar(){
                if(document.formacam.ano1.value != '' && document.formacam.mes1.value != '' && document.formacam.dia1.value != ''){
                   /* if(!ValidarFecha(document.formacam.ano1.value, document.formacam.mes1.value, document.formacam.dia1.value)){
                        return false;
                    }*/
                }
                if(document.formacam.ano2.value != '' && document.formacam.mes2.value != '' && document.formacam.dia2.value != ''){
                    /*if(!ValidarFecha(document.formacam.ano2.value, document.formacam.mes2.value, document.formacam.dia2.value)){
                        return false;
                    }*/
                }
   
            }
            function abrir1(){
                var vari = document.formacam.ntipo.value;
                var nombre = "variables.jsp?vari=" + vari  ;
                openPopup(nombre);                
            }
            function abrir2(){
                var ndepar = document.formacam.ndepar.value;
                var nombre = "sectores.jsp?&opcion=2&ndepar=" + ndepar  ;
                openPopup(nombre);
  
            }
            function openPopup(url) {
                myPopup = window.open(url,'popupWindow','width=530,height=280');
                if (!myPopup.opener)
                    myPopup.opener = self; 
            }
        </script>


    <body bgcolor="#FFFFFF" text="#666666" link="#0033CC" vlink="#006666" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">



        <form action="variablegeogp2.jsp" method="post" name="formacam" id="formacam" onsubmit="return validar();" >
            <input type="hidden" name="exportar" id="exportar" value="false" />
            <input type="hidden" name="cadena" value=""/>
            <input type="hidden" name="cadest" value=""/>
            <table width="963px" height="199" border="0" align="center">
                <tr> 
                    <td height="8" colspan="4" bordercolor="#000066" bgcolor="#CCCCCC"><b><font color="#99CCFF" face="Verdana, Arial, Helvetica, sans-serif" size="-1"><font color="#FF0000">::</font> 
                                <font color="#333333">Datos de Variables<font color="#6699FF"> 
                                        <%if (ntipo != null) {%>
                                        <%=ntipo%> 
                                        <%}%>
                                    </font></font><font color="#FF0000">::</font></font></b></td>
                </tr>
                <tr> 
                    <td width="13%" height="24" bordercolor="#000066" bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Departamento</font></td>
                    <td width="17%" bordercolor="#000066" bgcolor="#E6E6E6">
                        <select name="ndepar" onChange="abrir2();">
                            <option value="">--</option>
                            <%
                                Iterator itdepartamentos = departamentos.iterator();
                                while (itdepartamentos.hasNext()) {
                                    DepartamentosVo dapartamento = (DepartamentosVo) itdepartamentos.next();


                            %>
                            <option value="<%=dapartamento.getId()%>"><%=dapartamento.getNombre()%></option>
                            <%}%>
                        </select>
                        <span style="color:red;font-size: 11px">(* Campo obligatorio)</span>
                    </td>
                    <td width="11%" bordercolor="#000066" bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Sector</font></td>
                    <td width="40%" height="24" bordercolor="#000066" bgcolor="#E6E6E6"> 
                        <input type="text" name="nsector1" value="" size="35" disabled></td>
                <input type="hidden" name="nsector" value="" >
                </tr>
                <tr> 
                    <td height="2" bordercolor="#000066" bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Tipo 
                            de variable</font></td>
                    <td bordercolor="#000066" bgcolor="#E6E6E6"> 
                        <select name="ntipo" id="select4" onChange="abrir1();">
                            <option value="">--</option>
                            <%
                                ServiceVariableRedCam servicevariables = new ServiceVariableRedCam();
                                ArrayList tiposVariableslist = servicevariables.getTiposVariablesCAM();

                                Iterator it = tiposVariableslist.iterator();
                                while (it.hasNext()) {
                                    TipoVariable tipovariable = (TipoVariable) it.next();
                                    out.println("<option value='" + tipovariable.getTipo() + "'>" + tipovariable.getNombre() + "</option>");
                                }%>
                        </select>
                        <span style="color:red;font-size: 11px">(* Campo obligatorio)</span>
                    </td>
                    <td bordercolor="#000066" bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Temporada</font></td>
                    <td height="2" bordercolor="#000066" bgcolor="#E6E6E6">
                        <select name="ntemporada" id="select5">
                            <option value="">--</option>
                            <option value="1">Seca</option>
                            <option value="2">Lluviosa</option>
                        </select>
                        <span style="color:blue;font-size: 11px">( Campo opcional.)</span>
                    </td>
                </tr>
                <tr> 
                    <td bordercolor="#000066" bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Profundidad</font></td>
                    <td height="2" bordercolor="#000066" bgcolor="#E6E6E6">
                        <div id="panelprofundidad">
                            <select name="nprofundidad" id="select5">
                                <option value="">--</option>
                                <option value="S">Superficial</option>
                                <option value="M">Intermedia</option>
                                <option value="F">Fondo</option>
                            </select>
                        </div>    
                        <span style="color:blue;font-size: 11px">( Campo opcional)</span>
                    </td>
                    <td bordercolor="#000066" bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Sustrato</font></td>
                    <td height="2" bordercolor="#000066" bgcolor="#E6E6E6">
                        <select name="nsustrato" id="select5">
                            <option value="">--</option>
                            <option value="Agua Marina">Agua Marina</option>
                            <option value="Agua Estuarina">Agua Estuarina</option>
                            <option value="Agua Fluvial">Agua Fluvial</option>
                            <option value="Sedimento">Sedimento</option>
                            <option value="Organismo Animal">Organismo Animal</option>
                            <option value="Organismo Vegetal">Organismo Vegetal</option>
                        </select>
                        <input type="hidden" id="exportar" name="exportar" value="false" />
                        <span style="color:blue;font-size: 11px">( Campo opcional)</span>
                    </td>
                </tr>
                <tr> 
                    <td height="2" bordercolor="#000066" bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Rango 
                            de fechas</font></td>
                    <td height="2" colspan="3" bordercolor="#000066" bgcolor="#E6E6E6"><font face="Verdana, Arial, Helvetica, sans-serif" size="-2">De: 
                            <select name="dia1">
                                <option value = "">DD</option>
                                <%
                                    for (h = 1; h <= 31; h++) {
                                %>
                                <option value="<%=h%>" ><%=h%></option>
                                <% }%>
                            </select>
                            <select name="mes1">
                                <option value = "">MM</option>
                                <%
                                    for (h = 1; h <= 12; h++) {
                                %>
                                <option value="<%=h%>" ><%=util.mes(h)%></option>
                                <% }%>
                            </select>
                            <select name="ano1">
                                <option value="">AAAA</option>
                                <%
                                    int year= Calendar.getInstance().get(Calendar.YEAR);
                                    for (h = 2001; h <= year; h++) {
                                %>
                                <option value="<%=h%>" ><%=h%></option>
                                <% }%>
                            </select>
                        </font><font face="Verdana, Arial, Helvetica, sans-serif" size="-2">&nbsp 
                            A: 
                            <select name="dia2">
                                <option value = "">DD</option>
                                <%
                                    for (h = 1; h <= 31; h++) {
                                %>
                                <option value="<%=h%>" ><%=h%></option>
                                <% }%>
                            </select>
                            <select name="mes2">
                                <option value = "">MM</option>
                                <%
                                    for (h = 1; h <= 12; h++) {
                                %>
                                <option value="<%=h%>" ><%=util.mes(h)%></option>
                                <% }%>
                            </select>
                            <select name="ano2">
                                <option value="">AAAA</option>
                                <%
                                    year= Calendar.getInstance().get(Calendar.YEAR);
                                    for (h = 2001; h <= year; h++) {
                                %>
                                <option value="<%=h%>" ><%=h%></option>
                                <% }%>
                            </select>
                        </font>
                        <span style="color:blue;font-size: 11px">( Campo opcional)</span>
                    </td>
                </tr>
                <tr> 
                    <td height="26" colspan="4" bordercolor="#000066" bgcolor="#CCCCCC"> 
                        <div align="right"><font size="-2"> 
                                <input type="reset" name="Submit2" value="Limpiar formulario"/>
                                <input type="submit" name="Submit" value="Buscar datos &gt;&gt;"/>
                                <input type="button" name="buscaryexportar" id="buscaryexportar" value="Buscar y Exportar a Excel" onclick="BuscarYExportarAExcel()" style="display:none" />
                            </font></div></td>
                </tr>
            </table>
            <br/>
            <div id="panelcarga"></div>
            <table width="120%" border="0" cellpadding="3" cellspacing="0" height="163">
                <tr>
                    <td width="100%" height="163" valign="top"> 
                        <table width="100%" border="0">
                            <tr> 
                                <td colspan="14" height="37"><font face="Arial, Helvetica, sans-serif" size="-1">
                                        <span style="color:blue;font-size: 11px">( Los campos opcionales ayudan  a filtrar los campos obligatorios.)</span><br/>
                                        Listado de variables muestreadas en el sistema, el sector puede ser seleccionado 
                                        en la ventana que se abrir� luego de seleccionar el departamento.Las variables se podr�n seleccionar en la ventana nueva <br/>
                                        que se abrir� luego de seleccionar uno de los tipos de variable. <br/>
                                        <span style="color:red">Temporada: <strong>1</strong> es Seca  <strong>2</strong> es Lluviosa</span>

                                    </font></td>
                            <tr><td colspan="14"><span style="color:black">Cantidad de registros encontrados:<%=cvar.gettamano()%></span></td></tr>       
                            </tr>
                            <%


                                if (exportaAExcel != null && exportaAExcel.equalsIgnoreCase("true")) {

                                    String nombre = "reporte.xls";

                                    response.setContentType("application/vnd.ms-excel");
                                    response.setHeader("Content-type", "application/vnd.ms-excel");
                                    response.setHeader("Content-Disposition", "attachment; filename=\"" + nombre + "\"");

                            %>
                            <table width="100%" border="0">
                                <tr bgcolor="#CCCCCC"> 
                                    <td width="9%"> <div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"> 
                                                        <font color="#CCFFFF">::</font> Depto<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
                                    <td width="98"> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Sector<b><font color="#CCFFFF">::</font></b> 
                                                    </font></b></font></div></td>
                                    <td colspan="2"> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Estacion<b><font color="#CCFFFF">::</font></b> 
                                                    </font></b></font></div></td>
                                    <td width="7%"><div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font> 
                                                        Fecha<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
                                    <td width="6%"><div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font> 
                                                        Epoca<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
                                    <td width="5%"><div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font> 
                                                        Prof<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
                                    <td width="96"> <div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"> 
                                                        <font color="#CCFFFF">::</font> Sustrato<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
                                    <td colspan="2"> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Variable<b><font color="#CCFFFF">::</font></b> 
                                                    </font></b></font></div></td>
                                    <td width="67"><div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Valor<b><font color="#CCFFFF">::</font></b> 
                                                    </font></b></font></div></td>
                                    <td width="8%"> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Unidad<b><font color="#CCFFFF">::</font></b> 
                                                    </font></b></font></div></td>
                                    <td width="8%"><div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Latitud<b><font color="#CCFFFF">::</font></b> 
                                                    </font></b></font></div></td>
                                    <td width="8%"><div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Longitud<b><font color="#CCFFFF">::</font></b> 
                                                    </font></b></font></div></td>
                                </tr>
                                <%
                                    for (ind = 0; ind < cvar.gettamano(); ind++) {

                                        var = new cvariablegeog.Cvariablegeog();
                                        var = cvar.getvariablegeog((int) ind);
                                %>
                                <tr> 
                                    <td width="9%" bgcolor="#E6E6E6"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_depto()%></font></td>
                                    <td width="98" bgcolor="#E6E6E6"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_sector()%></font></td>
                                    <td width="6%" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_codest()%></font></td>
                                    <td width="8%" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_nomest()%></font></td>
                                    <td width="7%" bgcolor="#E6E6E6"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_fecha()%></font></td>
                                    <td width="6%" bgcolor="#E6E6E6"><div align="center"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_temporada()%></font></div></td>
                                    <td width="5%" bgcolor="#E6E6E6"><div align="center"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_profundidad()%></font></div></td>
                                    <td width="96" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_sustrato()%></font></td>
                                    <td width="34" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_cod()%></font></td>
                                    <td width="100" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_nombre()%></font></td>
                                    <td width="67" bgcolor="#E6E6E6"><div align="right"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_valor()%></font></div></td>
                                    <td width="8%" bgcolor="#E6E6E6" height="20"> <div align="right"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_unidad()%></font></div></td>
                                    <td width="8%" bgcolor="#E6E6E6"><div align="right"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_latitud()%></font></div></td>
                                    <td width="8%" bgcolor="#E6E6E6"><div align="right"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_longitud()%></font></div></td>
                                </tr>
                                <%

                                    }
                                %>
                                <tr> 
                                    <td width="9%">&nbsp;</td>
                                    <td width="98">&nbsp;</td>
                                    <td width="6%">&nbsp;</td>
                                    <td width="8%">&nbsp;</td>
                                    <td width="7%">&nbsp;</td>
                                    <td width="6%">&nbsp;</td>
                                    <td width="5%">&nbsp;</td>
                                    <td width="96">&nbsp;</td>
                                    <td width="34">&nbsp;</td>
                                    <td width="100">&nbsp;</td>
                                    <td width="67">&nbsp;</td>
                                    <td width="8%">&nbsp;</td>
                                    <td width="8%">&nbsp;</td>
                                    <td width="8%">&nbsp;</td>
                                </tr>
                                <tr> 
                                    <td height="21" colspan="14"><div align="center"><font color="#333333" size="-2" face="Verdana, Arial, Helvetica, sans-serif"> 
                                                <!--
                                                <% if (pag > 1) {
                                                %>
                                                  <a href="variablegeogp2.jsp?pagina=<%=pag - 1%>&ntipo=<%=ntipo%>&ndepar=<%=ndepar%>&nsector=<%=nsector%>"><< 
                                                  Anterior</a> - 
                                                <% }%>
                                                P&aacute;gina <%=pag%> de <%=paginas%> 
                                                <% if (pag < paginas) {%>
                                                - <a href="variablegeogp2.jsp?pagina=<%=pag + 1%>&ntipo=<%=ntipo%>&ndepar=<%=ndepar%>&nsector=<%=nsector%>">Siguiente 
                                                >></a> 
                                                <% }%>
                                                </font> </div> 
                                                -->
                                                </td>
                                                </tr>
                                                </table>                                           
                                               

                                                <%
                                                    //ServletOutputStream out1 = response.getOutputStream();
                                                    //out1.write(cvar.getArchivo());
                                                    //out1.flush();
                                                    //out1.close();
                                                } else {

                                                %>
                                                <tr bgcolor="#CCCCCC"> 
                                                    <td width="9%"> <div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"> 
                                                                        <font color="#CCFFFF">::</font> Depto<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
                                                    <td width="98"> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Sector<b><font color="#CCFFFF">::</font></b> 
                                                                    </font></b></font></div></td>
                                                    <td colspan="2"> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Estacion<b><font color="#CCFFFF">::</font></b> 
                                                                    </font></b></font></div></td>
                                                    <td width="7%"><div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font> 
                                                                        Fecha<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
                                                    <td width="6%"><div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font> 
                                                                        Epoca<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
                                                    <td width="5%"><div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font> 
                                                                        Prof<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
                                                    <td width="96"> <div align="center"><font size="1" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"> 
                                                                        <font color="#CCFFFF">::</font> Sustrato<b><font color="#CCFFFF">::</font></b></font></b></font></div></td>
                                                    <td colspan="2"> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Variable<b><font color="#CCFFFF">::</font></b> 
                                                                    </font></b></font></div></td>
                                                    <td width="67"><div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Valor<b><font color="#CCFFFF">::</font></b> 
                                                                    </font></b></font></div></td>
                                                    <td width="8%"> <div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Unidad<b><font color="#CCFFFF">::</font></b> 
                                                                    </font></b></font></div></td>
                                                    <td width="8%"><div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Latitud<b><font color="#CCFFFF">::</font></b> 
                                                                    </font></b></font></div></td>
                                                    <td width="8%"><div align="center"><font size="1" color="#333333"><b><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Longitud<b><font color="#CCFFFF">::</font></b> 
                                                                    </font></b></font></div></td>
                                                </tr>
                                                <%
                                                    for (ind = 0; ind < cvar.gettamano(); ind++) {

                                                        var = new cvariablegeog.Cvariablegeog();
                                                        var = cvar.getvariablegeog((int) ind);
                                                %>
                                                <tr> 
                                                    <td width="9%" bgcolor="#E6E6E6"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_depto()%></font></td>
                                                    <td width="98" bgcolor="#E6E6E6"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_sector()%></font></td>
                                                    <td width="6%" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_codest()%></font></td>
                                                    <td width="8%" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_nomest()%></font></td>
                                                    <td width="7%" bgcolor="#E6E6E6"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_fecha()%></font></td>
                                                    <td width="6%" bgcolor="#E6E6E6"><div align="center"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_temporada()%></font></div></td>
                                                    <td width="5%" bgcolor="#E6E6E6"><div align="center"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_profundidad()%></font></div></td>
                                                    <td width="96" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_sustrato()%></font></td>
                                                    <td width="34" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_cod()%></font></td>
                                                    <td width="100" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_nombre()%></font></td>
                                                    <td width="67" bgcolor="#E6E6E6"><div align="right"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_valor()%></font></div></td>
                                                    <td width="8%" bgcolor="#E6E6E6" height="20"> <div align="right"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_unidad()%></font></div></td>
                                                    <td width="8%" bgcolor="#E6E6E6"><div align="right"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_latitud()%></font></div></td>
                                                    <td width="8%" bgcolor="#E6E6E6"><div align="right"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_longitud()%></font></div></td>
                                                </tr>
                                                <%

                                                    }
                                                %>
                                                <tr> 
                                                    <td width="9%">&nbsp;</td>
                                                    <td width="98">&nbsp;</td>
                                                    <td width="6%">&nbsp;</td>
                                                    <td width="8%">&nbsp;</td>
                                                    <td width="7%">&nbsp;</td>
                                                    <td width="6%">&nbsp;</td>
                                                    <td width="5%">&nbsp;</td>
                                                    <td width="96">&nbsp;</td>
                                                    <td width="34">&nbsp;</td>
                                                    <td width="100">&nbsp;</td>
                                                    <td width="67">&nbsp;</td>
                                                    <td width="8%">&nbsp;</td>
                                                    <td width="8%">&nbsp;</td>
                                                    <td width="8%">&nbsp;</td>
                                                </tr>
                                                <tr> 
                                                    <td height="21" colspan="14"><div align="center"><font color="#333333" size="-2" face="Verdana, Arial, Helvetica, sans-serif"> 
                                                                <!--
                                                                <% if (pag > 1) {
                                                                %>
                                                                  <a href="variablegeogp2.jsp?pagina=<%=pag - 1%>&ntipo=<%=ntipo%>&ndepar=<%=ndepar%>&nsector=<%=nsector%>"><< 
                                                                  Anterior</a> - 
                                                                <% }%>
                                                                P&aacute;gina <%=pag%> de <%=paginas%> 
                                                                <% if (pag < paginas) {%>
                                                                - <a href="variablegeogp2.jsp?pagina=<%=pag + 1%>&ntipo=<%=ntipo%>&ndepar=<%=ndepar%>&nsector=<%=nsector%>">Siguiente 
                                                                >></a> 
                                                                <% }%>
                                                                </font> </div> 
                                                                -->
                                                                </td>
                                                                </tr>
                                                                </table>
                                                                <hr size="1" />
                                                                </td>
                                                                </tr>
                                                                </table>
                                                                <%}%>
                                                                
                                                                <input type="hidden" name="tam" value="<%=tam%>" >
                                                                </form>
                                                                <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td><img src="../../images/spacer.gif" width="5" height="5" alt=""></td>
                                                                    </tr> 
                                                                    <tr>
                                                                        <td><img src="../../images/spacer.gif" width="5" height="5" alt="" ></td>
                                                                    </tr>
                                                                </table>
                                                                <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td bgcolor=""><img src="../../images/spacer.gif" width="2" height="2" alt=""></td>
                                                                    </tr>
                                                                </table>
                                                                <%@ include file="../../plantillaSitio/footermodulesV3.jsp" %>
                                                                <script type="text/javascript">
                                                                    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
                                                                    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
                                                                </script>
                                                                <script type="text/javascript">
                                                                    try {
                                                                        var pageTracker = _gat._getTracker("UA-2300620-4");
                                                                        pageTracker._trackPageview();
                                                                    } catch(err) {}
                                                                </script>


                                                                </body>
                                                                </html>

