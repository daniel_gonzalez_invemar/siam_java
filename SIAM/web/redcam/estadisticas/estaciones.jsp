<%@ page import="co.org.invemar.siam.redcam.coneccam.*" %>
<%@ page import="co.org.invemar.siam.redcam.estadisticas.cvariablegeog.*" %>
<%@ page import="co.org.invemar.siam.redcam.utilidades.Cutilidades.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.net.URLEncoder" %>
<%@page pageEncoding="ISO-8859-1" contentType="text/html;charset=ISO-8859-1"%>
<%

    VariableGeog est = new VariableGeog();
    ContVariableGeog cest = new ContVariableGeog();
    ConexionCam con2 = new ConexionCam();
    Connection conn2 = null;
    conn2 = con2.getConn();
    String codigosector = request.getParameter("sect");
   
    //cest.contenedorestac(conn2, URLDecoder.decode(new String(request.getParameter("sect").getBytes("ISO-8859-1"), "UTF-8")), request.getParameter("nproy"), request.getParameter("nuser"), request.getParameter("opcion"));
    cest.contenedorestac(conn2, codigosector, request.getParameter("nproy"), request.getParameter("nuser"), request.getParameter("opcion"));
    int i = 0;
    if (con2 != null) {
        con2.close();
    }
%>
<html>
    <head>
        <title>:: Estaciones del sector "<%=URLDecoder.decode(request.getParameter("sect"), "ISO-8859-1")%>"::</title>
        <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>   
        <link type='text/css' rel="stylesheet" href="css/RedCamtheme.css"/>
        <script language="JavaScript">
            window.focus();
            function seleccionarest(){
                var cadest = "";
                for(i = 0; i < document.forma.estacion.length ; i++){
                    if(document.forma.estacion.options[i].selected == true){
                        cadest = cadest + document.forma.estacion.options[i].value  + ",";
                    }
                }
                window.opener.document.formacam.cadest.value=cadest;
                window.close();
            }
           function RenderizarEstaciones(){ 
                 
            var estaactivas=false;
            var estadesactivas = false; 
           var codigosector="'<%=codigosector%>'";
            
            if($('input[name=active]').is(':checked')){
               estaactivas=true;
            }
            
            if($('input[name=desactive]').is(':checked')){
               estadesactivas=true;
            }
            
            $.ajax({
                    type: "get",
                    url: "estacionesFiltradas.jsp",                   
                    data: "proyecto='REDCAM'&estaactivas="+estaactivas+"&estadesactivas="+estadesactivas+"&codigosector="+codigosector,
                    beforeSend: function(msg){ 
                        $('#resultsetEstaciones').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='img/loader.gif' alt='Loading...'/>");
  
                    },
                    success: function(msg){
                        $('#resultsetEstaciones').html('');
                        $('#resultsetEstaciones').html(msg);                 

                    },
                    error: function(xml,msg){
                        $('#resultsetEstaciones').html('Error cargando las estaciones. Pongase en contacto con el laboratorio de sistemas de informacion.');
                  
                    }
                });
                    
            }
  
        </script>
    </head>

    <body bgcolor="#336699">
          <div  id="activeVariables" style="color:white;">
            <input type="checkbox" id="active" name="active" value="true"  /><span >Estaciones activas |</span>
            <input type="checkbox" id="desactive" name="desactive"  value="false" /> <span>Estaciones  no activas</span>
            <input type="button" id="consultar" name="consulta" onclick="RenderizarEstaciones()" value="Filtrar estaciones por vigencia..." />
        </div>
        
        <%//=cest.geterror()%>
        <form name="forma" method="post" action="">
            <table width="496" border="0">
                <tr bgcolor="#333333"> 
                    <td colspan="2"><b><font color="#99CCFF" face="Verdana, Arial, Helvetica, sans-serif" size="-1"><font color="#FF0000">::</font> 
                            <font color="#FFFFFF">Estaciones</font><font color="#FF0000">::</font></font></b></td>
                </tr>
                <tr bgcolor="#CCEEFF"> 
                    <td colspan="2"><font face="Verdana, Arial, Helvetica, sans-serif" size="-1">Seleccione 
                        las estaciones de la lista, para seleccionar mas de una presione <strong>Crtl</strong> 
                        + <strong>Click</strong> y al final el bot�n <strong><em>Enviar</em></strong></font></td>
                </tr>
                <tr> 
                    <td colspan="2">
                        <div id="resultsetEstaciones">
                        <select name="estacion" size="10" multiple id="estacion">
                            <%
                                for (i = 0; i < cest.gettamano(); i++) {
                                    est = cest.getvariablegeog(i);
                            %>
                            <option value="<%=est.get_codest()%>"><%=est.get_nomest()%></option>
                            <% }%>
                        </select>
                        </div>
                        </td>
                </tr>
                <tr>
                    <td width="271"><input type="button" name="Button" value="Enviar" onClick="seleccionarest();"></td>
                    <td width="215">&nbsp;</td>
                </tr>
                <tr> 
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </form>
    </body>
</html>
