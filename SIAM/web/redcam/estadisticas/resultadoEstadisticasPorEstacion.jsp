<%@ page import="coneccam.*" %>
<%@ page import="cvariablegeog.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<% HttpSession session1 = request.getSession(true);
//  String nuser = (String)session.getAttribute("usu_id1");
    String nuser = "ljarias";
    if (nuser == null) {
%>
<script type="text/javascript" language="JavaScript">
    alert("No tiene permisos para ingresar a esta pagina");
            window.parent.location = "login.htm";</script>

<%  }%>

<%
    cvariablegeog.Cvariablegeog var = new cvariablegeog.Cvariablegeog();
    cvariablegeog.Ccontvariablegeog cvar = new cvariablegeog.Ccontvariablegeog();

    coneccam.Cconexioncam con2 = new coneccam.Cconexioncam();
    Connection conn2 = null;
    conn2 = con2.getConn();

//  String nconsulta  = request.getParameter("nconsulta");
//  String ntipo  = request.getParameter("ntipo");
//  String nprofundidad = request.getParameter("nprofundidad");
//  String nsustrato = request.getParameter("nsustrato");
    String nproy = request.getParameter("nproy");
    String ndepar = request.getParameter("ndepar");
    String nsector = request.getParameter("nsector");
    String ntemporada = request.getParameter("ntemporada");
    String nregion = request.getParameter("nregion");

    String sampleSubtratum = request.getParameter("samplesubtratum");

    String ano1 = "", ano2 = "";
    if (request.getParameter("ano1") != null && !request.getParameter("ano1").equals("")) {
        ano1 = request.getParameter("ano1");
    }
    if (request.getParameter("ano2") != null && !request.getParameter("ano2").equals("")) {
        ano2 = request.getParameter("ano2");
    }

    float ind = 0;
    int tam = 0;
    int paginas = 1;

    //pagina actual del resultado 1= valor inicial
    int pag = 1;

    //numero de registros por pagina
    int registros = 10;
    String pagina = request.getParameter("pagina");

    /*recuperacion de la cadena de variables CADE*/
    String variable = request.getParameter("variable");

    /*recuperacion de la cadena de estaciones SCADE*/
    String estacion = request.getParameter("estacion");

    int cont = 0;
    int h = 0, z = 0;

    /**
     * Llamada al contenedor de estadisticas
     */
    cvar.estadisticas3(conn2, nuser, "REDCAM", ndepar, nsector, ntemporada, ano1, ano2, variable, estacion, nregion, sampleSubtratum);

    if (con2 != null) {
        con2.close();
    }

%>

<html>
    <head>
        <title>Modulo de Consulta de datos asociados a una estacion - RedCAM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

            <link href="../../plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css" />
            <script src="js/lib/prototype/prototype.js" type="text/javascript"></script>
            <script src="js/lib/excanvas/excanvas.js" type="text/javascript"></script>   
          
           
            <script src="js/plotr.js" type="text/javascript"></script>

            <!-- API para http://cinto.invemar.org.co/siam/redcam/ -->
            <!--		<script 
                            type="text/javascript" src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAA-XBw67So1zoGdbqjgP_YAhTmykkScV0TwSmtcPnRJu4H87rq5hSik6yceX0iHVulA3fYkO6Xd_QpyA"></script>
            -->

            <!-- API para http://www.invemar.org.co/siam/redcam/ -->
            <script 
            type="text/javascript" src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAsTAu6MkeiB4rKmk8oyx1gRSYigaDQ3w0lHoxjjtDCcSvGwTszBQ_djMdzCxwFPyDcoLsPGDih4f5bw"></script>
            
           



    </head>


    <body onload="load()" onunload="GUnload()">
        <jsp:include page="../../plantillaSitio/headermodulosv3.jsp?idsitio=60"/>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td><img src="../../images/spacer.gif" width="5" height="5" alt='spacer'></td>
            </tr>
        </table>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">  
            <tr bgcolor="#8DC0E3">
                <td width="21">&nbsp;</td>
                <td width="729" bgcolor="#8DC0E3" class="linksnegros">Estadisticas por Estaci&oacute;n:</td>

                <td width="729" bgcolor="#8DC0E3" class="linksnegros">&nbsp;</td>
            </tr>
        </table>

        <%
       
       if (cvar.gettamano()>=1) {
              
        
    
        %>
        <table width="100%" border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td width="100%" height="58">          
                    <div align="right">
                        <p>
                            <%                                // for(ind = 0; ind < 1; ind++){
                                var = new Cvariablegeog();
                                
                                var = cvar.getvariablegeog(0);    
                                
                                
                            %>
                        </p>

                        <table width="963px" border="0" align="center" id="cabezote">
                            <tr>
                                <td colspan="2"><strong><font face="Arial, Helvetica, sans-serif">Datos asociados a la estacion</font></strong></td>
                                <td>&nbsp;</td>
                                <td colspan="2"><strong><font face="Arial, Helvetica, sans-serif">Datos de
                                            la variable representada</font></strong></td>
                            </tr>
                            <tr>
                                <td width="15%" bgcolor="#CCFFFF" class="texttitulo"><div align="left">C&oacute;digo:</div></td>
                                <td width="21%" class="textnormal"><%=var.get_codest()%></td>
                                <td width="21%">&nbsp;</td>
                                <td width="9%" bgcolor="#CCFFFF" class="texttitulo"><div align="left">Nombre</div></td>
                                <td width="34%" class="textnormal"><%=var.get_cod()%> - <%=var.get_nombre()%></td>
                            </tr>
                            <tr>
                                <td bgcolor="#CCFFFF" class="texttitulo"><div align="left">Nombre:</div></td>
                                <td class="textnormal"><%=var.get_nomest()%></td>
                                <td>&nbsp;</td>
                                <td bgcolor="#CCFFFF" class="texttitulo"><div align="left">Unidades:</div></td>
                                <td class="textnormal"><%=var.get_unidad()%></td>

                            </tr>

                            <tr>
                                <td bgcolor="#CCFFFF" class="texttitulo"><div align="left">Departamento:</div></td>
                                <td class="textnormal"><%=var.get_depto()%></td>
                                <td>&nbsp;</td>                               
                                <td bgcolor="#CCFFFF" class="texttitulo"><div align="left">Tipo de sustrato Muestra:</div></td>
                                <td class="textnormal"><%=var.getTipoSustratoMuestra()%></td>
                            </tr>
                            <tr>
                                <td bgcolor="#CCFFFF" class="texttitulo"><div align="left">Sector:</div></td>
                                <td class="textnormal"><%=var.get_sector()%></td>
                                <td>&nbsp;</td>
                                <td><div align="left"></div></td>
                                <td class="titulosnegros">&nbsp;</td>
                            </tr>
                            <tr>
                                <td bgcolor="#CCFFFF" class="texttitulo"><div align="left">Coordenadas:</div></td>
                                <td class="textnormal"><%=var.get_latitud()%> / <%=var.get_longitud()%></td>
                                <td>&nbsp;</td>
                                <td><div align="left"></div></td>
                                <td class="titulosnegros">&nbsp;</td>
                            </tr>
                        </table>                      

                        <p align="left"><b><font color="#6699FF" size="-1" face="Verdana, Arial, Helvetica, sans-serif"><br>
                                </font></b>            
                            <p align="left">&nbsp;            
                                <p align="left">

                                    <%
                                                    //fin del cliclo FOR
                                        //}
                                    %>
                                    </div></td>
                                    </tr>
                                    <tr> 
                                        <td colspan="15" height="37"><font face="Arial, Helvetica, sans-serif" size="-1">                   La
                                                informaci&oacute;n aca representada, corresponde al proyecto REDCAM,
                                                en datos tomados en <%=var.getTipoSustratoMuestra()%>  a profundidad SOMERA</font></td>
                                    </tr>
                                    <tr> 
                                        <td>

                                            <table  width="1200" border="0" cellpadding="0" cellspacing="1" align="center" bgcolor="#E1E1E1">
                                                <tr>
                                                    <td width="60" align="right" class="titulosnegros"><%=var.get_unidad()%></td>
                                                    <td width="600" align="left"><div><canvas id="lines1" height="400" width="600"></canvas></div>

                                                        <script type="text/javascript">

                                                                    var dataset = {
                                                                    'datosvariable': 	[
                                                            <%
                                                                for (ind = 0; ind < cvar.gettamano(); ind++) {
                                                                    var = new Cvariablegeog();
                                                                    var = cvar.getvariablegeog((int) ind);
                                                            %>
                                                                    [<%=ind%>, <%=var.get_prom()%>]

                                                            <%if (ind + 1 < cvar.gettamano()) {%>
                                                                    ,
                                                            <%}
                                                                }%>

                                                                    ]
                                                                    };
                                                                    // Define options.
                                                                    var options = {
                                                                    // Define a padding for the canvas node
                                                                    padding: {left: 30, right: 0, top: 10, bottom: 30},
                                                                            // Background color to render.
                                                                            backgroundColor: '#f2f2f2',
                                                                            shouldFill: false,
                                                                            // Use the predefined blue colorscheme.
                                                                            colorScheme: 'blue',
                                                                            //barOrientation: 'vertical',
                                                                            // Set the labels.
                                                                            xTicks: [

                                                            <%
                                                                for (ind = 0; ind < cvar.gettamano(); ind++) {
                                                                    var = new Cvariablegeog();
                                                                    var = cvar.getvariablegeog((int) ind);
                                                            %>
                                                                            {v:<%=ind%>, label:'<%=var.get_anotemp()%>'}
                                                            <%if (ind + 1 < cvar.gettamano()) {%>
                                                                            ,
                                                            <%}
                                                                }%>
                                                                            ]
                                                                    };
                                                                    // Instantiate a new BarCart.
                                                                    var line = new Plotr.LineChart('lines1', options);
                                                                    // Add a dataset to it.
                                                                    line.addDataset(dataset);
                                                                    // Render it.
                                                                    line.render();                                                        </script>        </td>
                                                    <td width="12">&nbsp;</td>
                                                    <td width="300" align="center" valign="bottom" >
                                                        <script type="text/javascript">
                                                                    //<![CDATA[
                                                                            function load() {
                                                                            if (GBrowserIsCompatible()) {
                                                                            var map = new GMap2(document.getElementById("map"));
                                                                                    map.setCenter(new GLatLng(<%=var.get_latitud()%>, <%=var.get_longitud()%>), 7);
                                                                                    map.addControl(new GLargeMapControl());
                                                                                    map.setMapType(G_NORMAL_MAP);
                                                                                    var point = new GPoint (<%=var.get_longitud()%>,<%=var.get_latitud()%>);
                                                                                    var marker = new GMarker(point);
                                                                                    map.addOverlay(marker);
                                                                            }
                                                                            }
                                                                    //]]>
                                                        </script>
                                                        <div id="map" style="width: 300px; height: 300px"></div>
                                                    </td>
                                                    <td width="15" valign="middle" >&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td align="center" class="titulosnegros">Muestreos</td>
                                                    <td class="titulosnegros">&nbsp;</td>
                                                    <td align="center" class="titulosnegros">Ubicaci&oacute;n espacial de la estaci&oacute;n</td>
                                                    <td class="titulosnegros">&nbsp;</td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>			
                                    <td height="13" colspan="6"><div align="left"><font face="Arial, Helvetica, sans-serif">
                                                <strong>
                                                    <font color="#FF0000">
                                                        <%=cvar.gettamano()%> 
                                                    </font></strong><font color="#000066">registro(s) encontrado(s)</font></font></div>
                                    </td>


                                    <tr>
                                        <td width="100%" height="71" valign="top">                                                 
                                            <center>
                                                <a target="_blank" href="/siam/exportStationToExcel?ntemporada=<%=ntemporada%>&variable=<%=variable%>&estacion=<%=estacion%>&ano1=<%=ano1%>&ano2=<%=ano2%>&samplesubtratum=<%=sampleSubtratum%>"  ><img src="../images/excel2013.png" style="width:50px;height:50px"/>
                                                    Descargar Excel </a>
                                                


                                            </center>
                                            <table width="500" border="1" id="datos" align="center">
                                                <tr bgcolor="#CCCCCC">
                                                    <td width="150" class="linksnegros"><div align="center">Variable</div></td>
                                                    <td width="50" class="linksnegros"><div align="center">Muestreo</div></td>
                                                    <td width="150" class="linksnegros"><div align="center">Valor</div></td>
                                                    <td width="150" class="linksnegros"><div align="center">Unidad</div></td>
                                                </tr>

                                                <%
                                                    for (ind = 0; ind < cvar.gettamano(); ind++) {
                                                        var = new Cvariablegeog();
                                                        var = cvar.getvariablegeog((int) ind);
                                                %>
                                                <tr> 
                                                    <td class="textnormaindex"><div align="center"><%=var.get_nombre()%></div></td>
                                                    <td><div align="right"><font color="#333333" size="2" face="Arial, Helvetica, sans-serif"><%=var.get_anotemp()%></font></div></td>
                                                    <td><div align="right"><font color="#333333" size="2" face="Arial, Helvetica, sans-serif"><%=var.get_prom()%></font></div></td>
                                                    <td class="textnormaindex"><%=var.get_unidad()%></td>
                                                </tr>
                                                <%
                                                        //fin del cliclo FOR
                                                    }
                                                %>
                                            </table>
                                            <hr size="1">
                                                <div align="right"> </div>
                                        </td>
                                    </tr>
                                    </table>
        <%                                    
        }else{
          out.println("No se econtraron datos...");
        }
        %>
      
      
                                    </div>
                                    <font face="Arial, Helvetica, sans-serif"><b><a href="javascript: self.close ()"><font size="2" face="Arial, Helvetica, sans-serif">Cerrar esta ventana</font></a></b>
                                    </font><hr size="1">

                                        <script type="text/javascript">
                                                            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
                                                            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));                                        </script>
                                        <script type="text/javascript">
                                                            try {
                                                            var pageTracker = _gat._getTracker("UA-2300620-4");
                                                                    pageTracker._trackPageview();
                                                            } catch (err) {}
                                        </script>

                                        <%@ include file="../../plantillaSitio/footermodulesV3.jsp" %>
                                        </body>
                                        </html>