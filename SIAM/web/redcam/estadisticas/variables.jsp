<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ page import="co.org.invemar.siam.redcam.utilidades.Cutilidades.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.net.URLDecoder" %>
<%@page  contentType="text/html; charset=iso-8859-1" pageEncoding="iso-8859-1"%>
<%@page  import="co.org.invemar.siam.redcam.*;"%>

<%
    request.setCharacterEncoding("utf-8");
    //VariableGeog var = new VariableGeog();
    //ContVariableGeog cvar = new ContVariableGeog();
    //ConexionCam con2 = new ConexionCam();
    //Connection conn2 = null;
    //conn2 = con2.getConn();
    String vari = request.getParameter("vari");
    ServiceVariableRedCam serviciosvariables = new ServiceVariableRedCam();
    //Connection con = serviciosvariables.getCon();
   //con=null;

    // String vari2 =URLDecoder.decode(request.getParameter("vari"), "UTF-8");
    //cvar.contenedorvaric(conn2, URLDecoder.decode(new String(vari.getBytes("iso-8859-1"), "UTF-8")), request.getParameter("nproy"), request.getParameter("nuser"), request.getParameter("opcion"));
    //cvar.contenedorvaric(conn2, vari, request.getParameter("nproy"), request.getParameter("nuser"), request.getParameter("opcion"));

    //if (con == null) {
   //     response.sendRedirect("../paginaError.jsp");
    //}
%>

<html>
    <head>
        <title>Variables vari%></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>   
        <link type='text/css' rel="stylesheet" href="css/RedCamtheme.css"/>
        <script language="JavaScript">
            window.focus();
            function seleccionar(){
                var cadena = "";
                for(i = 0; i < document.forma.variable.length ; i++){
                    if(document.forma.variable.options[i].selected == true){
                        cadena = cadena + document.forma.variable.options[i].value  + ",";
                    }

                }
                opener.document.formacam.cadena.value=cadena;
                window.close();
            }
                 
            
            function RenderizarVariables(){ 
                 
                var variactivas=false;
                var vardesactivas = false; 
            
                if($('input[name=active]').is(':checked')){
                    variactivas=true;
                }
            
                if($('input[name=desactive]').is(':checked')){
                    vardesactivas=true;
                }
            
                $.ajax({
                    type: "get",
                    url: "variablesFiltradas.jsp",                   
                    data: "tipovariable=<%=vari%>&varActivas="+variactivas+"&varDesactivas="+vardesactivas,
                    beforeSend: function(msg){ 
                        $('#resultsetVariables').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='img/loader.gif' alt='Loading...'/>");
  
                    },
                    success: function(msg){
                        $('#resultsetVariables').html('');
                        $('#resultsetVariables').html(msg);                 

                    },
                    error: function(xml,msg){
                        $('#resultsetVariables').html('Error cargando los variables. Consulte con el Laboratorio de Sistemas de informacion.');
                  
                    }
                });
                    
            }
        </script>
    </head>
    <body bgcolor="#336699">  
        <div  id="activeVariables" style="display:none">
            <input type="checkbox" id="active" name="active" value="true"  /><span >Variables activas |</span>
            <input type="checkbox" id="desactive" name="desactive"  value="false" /> <span>Variables  No activas</span>
            <input type="button" id="consultar" name="consulta" onclick="RenderizarVariables()" value="Filtrar variables..." />
        </div>
        <form name="forma" method="post" action="">
            <table width="496" border="0">
                <tr bgcolor="#333333"> 
                    <td colspan="2"><b><font color="#99CCFF" face="Verdana, Arial, Helvetica, sans-serif" size="-1"><font color="#FF0000">::</font> 
                            <font color="#FFFFFF">Variables </font><font color="#FF0000">::</font></font></b></td>
                </tr>
                <tr bgcolor="#CCEEFF"> 
                    <td colspan="2"><font face="Verdana, Arial, Helvetica, sans-serif" size="-1">Seleccione 
                        las variables de la lista, para seleccionar mas de una presione <strong>Crtl</strong> 
                        + <strong>Click</strong> y al final el bot�n <strong><em>Enviar</em></strong></font></td>
                </tr>
                <tr> 
                    <td colspan="2">
                        <div id="resultsetVariables">
                            <select id="variable" name="variable" size="10" multiple>
                                <%
                                    ArrayList variables = serviciosvariables.getVariablesPorTipo(vari);
                                    Iterator it = variables.iterator();
                                    while (it.hasNext()) {
                                      Variable variable = (Variable)it.next();
                                       out.println("<option value='"+variable.getNombre()+"'>"+variable.getNombre()+"</option>");
     
                                   }
                                %>


                            </select>
                        </div>
                    </td>
                </tr>
                <tr> 
                    <td width="271"><input type="button" name="Button" value="Enviar" onclick="seleccionar();"></td>
                    <td width="215">&nbsp;</td>
                </tr>
                <tr> 
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </form>
    </body>
</html>

