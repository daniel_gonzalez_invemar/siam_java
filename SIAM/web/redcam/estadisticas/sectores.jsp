<%@page import="co.org.invemar.siam.redcam.SectoresVo"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="co.org.invemar.siam.redcam.ServiceSectoresRedCam"%>
<%@ page import="co.org.invemar.siam.redcam.coneccam.*" %>
<%@ page import="co.org.invemar.siam.redcam.utilidades.Cutilidades.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.net.URLEncoder" %>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%


    ConexionCam con2 = new ConexionCam();
    Connection conn2 = null;
    conn2 = con2.getConn();


    if (con2 != null) {
        con2.close();
    }
%>
<script language="JavaScript">
    window.focus();

    function seleccionar(){
        var cadena = document.forma.variable.value;
        var nombresector = document.forma.variable;
  
        opener.document.formacam.nsector.value=cadena;  
        opener.document.formacam.nsector1.value=nombresector.options[nombresector.selectedIndex].text;
        window.close();

    }

    function openPopup(url) {
        myPopup = window.open(url,'popupWindow','width=530,height=280');
        if (!myPopup.opener)
            myPopup.opener = self; 
    }


</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<html>
    <head>
        <title>:: Sectores ::</title>

    </head>

    <body bgcolor="#336699">
        <%//=csector.geterror()%>
        <form name="forma" method="post" action="">
            <table width="513" border="0">
                <tr bgcolor="#333333"> 
                    <td colspan="2"><b><font color="#99CCFF" face="Verdana, Arial, Helvetica, sans-serif" size="-1"><font color="#FF0000">::</font> 
                            <font color="#FFFFFF">Sectores </font><font color="#FF0000">::</font></font></b></td>
                </tr>
                <tr bgcolor="#CCEEFF"> 
                    <td colspan="2"><font face="Verdana, Arial, Helvetica, sans-serif" size="-1">Seleccione
                        un sector de la lista y proceda luego a seleccionar la(s) estacion(es)
                        que desee ver para el mismo.</font></td>
                </tr>
                <tr> 
                    <td colspan="2"><select name="variable"  onchange="seleccionar();">
                            <option value="">Seleccione un valor</option>
                            <%

                                ServiceSectoresRedCam serviceSectores = new ServiceSectoresRedCam();
                                String vari = request.getParameter("ndepar");
                                String depart = URLDecoder.decode(new String(vari.getBytes("iso-8859-1"), "UTF-8"));
                                
                               
                                
                                ArrayList Listsectores = serviceSectores.getSectoresRedCam(depart, request.getParameter("nproy"), request.getParameter("opcion"), request.getParameter("nuser"));
                                Iterator it = Listsectores.iterator();
                                while (it.hasNext()) {
                                    SectoresVo sector = (SectoresVo) it.next();
                                    out.println("<option value='" + sector.getCodigo() + "'>" + sector.getNombre() + "</option>");
                                }
                            %>
             //           </select>
                    </td>
                </tr>
                <tr> 
                    <td><span style="color:white;font-size:14px;font-weight: bold">(El sector es opcional de seleccionar.  Pero si es obligatorio cuando se desea hacer una b&uacute;queda por estaciones o por sectores.)</span></td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </form>
    </body>
</html>

