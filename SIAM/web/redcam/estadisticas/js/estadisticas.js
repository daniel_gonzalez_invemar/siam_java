var cadest;
var nregion;
var ndepar;
var nsector1;
var nsector;
var ntipo;
var ntemporada;
var ano1;
var ano2;
var nproy;
var opcion;
var nuser;
var tam;
var cadena;
var nivelSeleccionado;
var vari;
var nombre;
var secto;
var nivelEstadisticoActual = 0;
$(function() {
    ocultarParametros();
    $("#sectorssdialog").css("display", "none");
    $("#variablesdialog").css("display", "none");
    $("#estacionesdialog").css("display", "none");
    $("#dialog:ui-dialog").dialog("destroy");
    $("#sectorssdialog").dialog("destroy");
    visualizarCuadroModal();
    $("#tabs").tabs();
});
function ocultarParametros() {
    document.getElementById("ntipo").style.display = 'none';
    document.getElementById("nregion").style.display = 'none';
    document.getElementById("ntipo").style.display = 'none';
    document.getElementById("nsector1").style.display = 'none';
    document.getElementById("selestaciones").style.display = 'none';
    document.getElementById("ntemporada").style.display = 'none';
    document.getElementById("msgvariables").style.display = 'none';

    if (document.getElementById("msganos") != null) {

        document.getElementById("msganos").style.display = 'none';
    }


    document.getElementById("msgregion").style.display = 'none';
    document.getElementById("msgestaciones").style.display = 'none';
    document.getElementById("msgdepartamento").style.display = 'none';
    document.getElementById("msgtemporada").style.display = 'none';
    document.getElementById("msgzona").style.display = 'none';
    document.getElementById("szonas").style.display = 'none';
    if (document.getElementById("ndepar") != null) {
        document.getElementById("ndepar").style.display = 'none';
    }
    if (document.getElementById("ano1") != null) {
        document.getElementById("ano1").style.display = 'none';
    }
    if (document.getElementById("ano2") != null) {
        document.getElementById("ano2").style.display = 'none';
    }

    $("#estacionesdialog").css("display", "none");
    $("#sectorssdialog").css("display", "none");


}
function visualizarCuadroModal() {
    $("#dialog-modal").dialog({
        height: 380,
        width: 470,
        position: [780, 95]
    });
}
function validar() {
    var retorno = true;
    if (document.formacam.ano1.value != '') {
        if (!ValidarFecha(document.formacam.ano1.value)) {
            retorno = false;
        }
    }
    if (document.formacam.ano2.value != '') {
        if (!ValidarFecha(document.formacam.ano2.value)) {
            retorno = false;
        }
    }
    return retorno;
}
function abrirprj() {
    nproy = document.formacam.nproy.value;
    document.location.href = "index.jsp?nproy=" + nproy;
}
/**
 * Comment
 */

function abrir1() {
    vari = document.formacam.ntipo.value;
    nproy = document.formacam.nproy.value;
    opcion = document.formacam.opcion.value;
    nuser = document.formacam.nuser.value;


    $("#sampletypesubstratum").css("display", "inline");

    //openPopup(nombre, 'variables');
    $("#variable").empty();
    $.post("/siam/ServicioComponentes?componente=redcam&action=Variables", {
        vari: vari,
    }, function(data) {
        $.each(data, function(index, item) {
            $("#variable").append("<option value='" + item.nombre + "'>" + item.nombre + "</option>");
        });
    }, "json");

    $("#variablesdialog").dialog({
        height: 380,
        width: 470,
        position: [780, 95]
    });

}
function abrir2() {

    nproy = document.formacam.nproy.value;
    opcion = document.formacam.opcion.value;
    nuser = document.formacam.nuser.value;
    ndepar = document.formacam.ndepar.value;
    $("#sectores").empty();
    if (ndepar != null) {
        $.post("/siam/ServicioComponentes?componente=redcam&action=Sector", {
            depart: ndepar,
        }, function(data) {
            $.each(data, function(index, item) {
                $("#sectores").append("<option value=" + item.codigo_sc + ">" + item.sector + "</option>");
            });
        }, "json");
    }

    $("#sectorssdialog").dialog({
        height: 380,
        width: 470,
        position: [780, 95]
    });
    periodosConAnosPorDepartamento();
}

function findTypeSubtratumSample() {
   var typevariable=document.formacam.ntipo.value;   
   var variables = $("#cadena").val();
   
  // alert(ano1+"-"+ano2+"-"+typevariable);
    
    $("#sampletypesubstratum").empty();
   
    
    $.post("/siam/ServicioComponentes?componente=redcam&action=TipoSustratoMuestra", {a1:ano1,a2:ano2,v:variables}, function(data) {
        $.each(data, function(index, item) {
            $("#sampletypesubstratum").append("<option value='" + item.id + "'>" + item.name + "</option>");
        });
    }, "json");

   
}

function seleccionarEstaciones() {
    secto = document.formacam.nsector.value;
    $("#estacion").empty();
    $("#estacionesdialog").attr("display", "block");

    $.post("/siam/ServicioComponentes?componente=redcam&action=Estaciones", {
        sect: secto,
    }, function(data) {
       
            $("#estacion").append("<option value='" + item.codestaciones + "'>" + item.nomestaciones + "</option>");
       
    }, "json");


    $("#estacionesdialog").dialog({
        height: 380,
        width: 470,
        position: [780, 95]
    });
}
function mostrarNivel(nivel) {
    ocultarParametros();
    LimpiarVariables();
    nivelSeleccionado = nivel;
    if (nivel <= 8) {
        document.location.href = "#nivel1";
    } else {
        document.location.href = "#nivel31";
    }

    if (nivel == 1) {
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("nsector1").style.display = 'inline';
        document.getElementById("ntemporada").style.display = 'inline';
        document.getElementById("selestaciones").style.display = 'inline';
        document.getElementById("msgvariables").style.display = 'inline';
        document.getElementById("msgregion").style.display = 'inline';
        document.getElementById("msgestaciones").style.display = 'inline';
        document.getElementById("msgdepartamento").style.display = 'inline';
        document.getElementById("msgtemporada").style.display = 'inline';
        if (document.getElementById("ndepar") != null) {
            document.getElementById("ndepar").style.display = 'inline';
        }
        if (document.getElementById("ano1") != null) {
            document.getElementById("ano1").style.display = 'inline';
        }
        if (document.getElementById("ano2") != null) {
            document.getElementById("ano2").style.display = 'inline';
        }

        nivelEstadisticoActual = 1;
    }
    if (nivel == 2) {
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("nsector1").style.display = 'inline';
        document.getElementById("ntemporada").style.display = 'inline';
        document.getElementById("msgvariables").style.display = 'inline';
        document.getElementById("msgregion").style.display = 'inline';
        document.getElementById("msgdepartamento").style.display = 'inline';
        document.getElementById("msgtemporada").style.display = 'inline';
        if (document.getElementById("ndepar") != null) {
            document.getElementById("ndepar").style.display = 'inline';
        }
        if (document.getElementById("ano1") != null) {
            document.getElementById("ano1").style.display = 'inline';
        }
        if (document.getElementById("ano2") != null) {
            document.getElementById("ano2").style.display = 'inline';
        }
        nivelEstadisticoActual = 2;
    }

    if (nivel == 3) {
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("ntemporada").style.display = 'inline';
        document.getElementById("msgvariables").style.display = 'inline';
        document.getElementById("msgregion").style.display = 'inline';
        document.getElementById("msgdepartamento").style.display = 'inline';
        document.getElementById("msgtemporada").style.display = 'inline';
        if (document.getElementById("ndepar") != null) {
            document.getElementById("ndepar").style.display = 'inline';
        }
        if (document.getElementById("ano1") != null) {
            document.getElementById("ano1").style.display = 'inline';
        }
        if (document.getElementById("ano2") != null) {
            document.getElementById("ano2").style.display = 'inline';
        }
        nivelEstadisticoActual = 3;
    }

    if (nivel == 4) {
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("msgvariables").style.display = 'inline';
        document.getElementById("msgregion").style.display = 'inline';
        document.getElementById("msgtemporada").style.display = 'inline';
        document.getElementById("ntemporada").style.display = 'inline';
        if (document.getElementById("ano1") != null) {
            document.getElementById("ano1").style.display = 'inline';
        }
        if (document.getElementById("ano2") != null) {
            document.getElementById("ano2").style.display = 'inline';
        }
        nivelEstadisticoActual = 4;
    }

    if (nivel == 5) {
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("msgdepartamento").style.display = 'inline';
        document.getElementById("nsector1").style.display = 'inline';
        document.getElementById("selestaciones").style.display = 'inline';
        document.getElementById("msgestaciones").style.display = 'inline';
        document.getElementById("msgvariables").style.display = 'inline';
        document.getElementById("msgregion").style.display = 'inline';
        //document.getElementById("msgtemporada").style.display='inline'; 
        //document.getElementById("ntemporada").style.display='inline';                    


        if (document.getElementById("ndepar") != null) {
            document.getElementById("ndepar").style.display = 'inline';
        }

        if (document.getElementById("ano1") != null) {
            document.getElementById("ano1").style.display = 'inline';
        }
        if (document.getElementById("ano2") != null) {
            document.getElementById("ano2").style.display = 'inline';
        }
        nivelEstadisticoActual = 5;
    }

    if (nivel == 6) {
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("msgdepartamento").style.display = 'inline';
        document.getElementById("nsector1").style.display = 'inline';
        // document.getElementById("selestaciones").style.display='inline';              
        // document.getElementById("msgestaciones").style.display='inline';                 
        document.getElementById("msgvariables").style.display = 'inline';
        document.getElementById("msgregion").style.display = 'inline';
        //document.getElementById("msgtemporada").style.display='inline'; 
        //document.getElementById("ntemporada").style.display='inline';                    


        if (document.getElementById("ndepar") != null) {
            document.getElementById("ndepar").style.display = 'inline';
        }

        if (document.getElementById("ano1") != null) {
            document.getElementById("ano1").style.display = 'inline';
        }
        if (document.getElementById("ano2") != null) {
            document.getElementById("ano2").style.display = 'inline';
        }
        nivelEstadisticoActual = 6;
    }
    if (nivel == 7) {
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("msgdepartamento").style.display = 'inline';
        //document.getElementById("nsector1").style.display='inline';
        // document.getElementById("selestaciones").style.display='inline';              
        // document.getElementById("msgestaciones").style.display='inline';                 
        document.getElementById("msgvariables").style.display = 'inline';
        //document.getElementById("msgregion").style.display='inline';       
        //document.getElementById("msgtemporada").style.display='inline'; 
        //document.getElementById("ntemporada").style.display='inline';                    


        if (document.getElementById("ndepar") != null) {
            document.getElementById("ndepar").style.display = 'inline';
        }

        if (document.getElementById("ano1") != null) {
            document.getElementById("ano1").style.display = 'inline';
        }
        if (document.getElementById("ano2") != null) {
            document.getElementById("ano2").style.display = 'inline';
        }
        nivelEstadisticoActual = 7;
    }

    if (nivel == 8) {
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("msgvariables").style.display = 'inline';
        if (document.getElementById("ano1") != null) {
            document.getElementById("ano1").style.display = 'inline';
        }
        if (document.getElementById("ano2") != null) {
            document.getElementById("ano2").style.display = 'inline';
        }
        nivelEstadisticoActual = 8;
    }

    if (nivel == 29) {
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("msgdepartamento").style.display = 'inline';
        document.getElementById("nsector1").style.display = 'inline';
        document.getElementById("selestaciones").style.display = 'inline';

        if (document.getElementById("msganos") != null) {
            document.getElementById("msganos").style.display = 'inline';
        }


        document.getElementById("msgestaciones").style.display = 'inline';
        document.getElementById("msgvariables").style.display = 'inline';
        document.getElementById("msgregion").style.display = 'inline';
        document.getElementById("msgtemporada").style.display = 'inline';
        document.getElementById("ntemporada").style.display = 'inline';
        if (document.getElementById("ndepar") != null) {
            document.getElementById("ndepar").style.display = 'inline';
        }
        if (document.getElementById("msganos") != null) {
            document.getElementById("msganos").style.display = 'inline';
        }
        nivelEstadisticoActual = 29;
    }

    if (nivel == 30) {
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("msgdepartamento").style.display = 'inline';
        document.getElementById("nsector1").style.display = 'inline';
        document.getElementById("msgvariables").style.display = 'inline';
        document.getElementById("msgregion").style.display = 'inline';
        document.getElementById("msgtemporada").style.display = 'inline';
        document.getElementById("ntemporada").style.display = 'inline';
        if (document.getElementById("ndepar") != null) {
            document.getElementById("ndepar").style.display = 'inline';
        }

        nivelEstadisticoActual = 30;
    }
    if (nivel == 31) {

        document.location.href = "#nivel31";
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("msgdepartamento").style.display = 'inline';
        document.getElementById("msgvariables").style.display = 'inline';
        document.getElementById("msgregion").style.display = 'inline';
        document.getElementById("msgtemporada").style.display = 'inline';
        document.getElementById("ntemporada").style.display = 'inline';
        if (document.getElementById("ndepar") != null) {
            document.getElementById("ndepar").style.display = 'inline';
        }
        nivelEstadisticoActual = 31;
    }
    if (nivel == 32) {
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("msgvariables").style.display = 'inline';
        document.getElementById("msgregion").style.display = 'inline';
        document.getElementById("msgtemporada").style.display = 'inline';
        document.getElementById("ntemporada").style.display = 'inline';
        nivelEstadisticoActual = 32;
    }




    if (nivel == 33) {
        document.getElementById("ntipo").style.display = 'inline';
        nivelEstadisticoActual = 33;
    }

    if (nivel == 38) {
        document.getElementById("ntipo").style.display = 'inline';
        document.getElementById("nregion").style.display = 'inline';
        document.getElementById("nsector1").style.display = 'inline';
        document.getElementById("ntemporada").style.display = 'inline';
        document.getElementById("msgvariables").style.display = 'inline';
        document.getElementById("msgregion").style.display = 'inline';
        document.getElementById("msgdepartamento").style.display = 'inline';
        document.getElementById("msgtemporada").style.display = 'inline';
        document.getElementById("szonas").style.display = 'inline';
        document.getElementById("msgzona").style.display = 'inline';
        if (document.getElementById("ndepar") != null) {
            document.getElementById("ndepar").style.display = 'inline';
        }

        if (document.getElementById("ano1") != null) {
            document.getElementById("ano1").style.display = 'inline';
        }
        if (document.getElementById("ano2") != null) {
            document.getElementById("ano2").style.display = 'inline';
        }


        nivelEstadisticoActual = 38;
    }

}
function openPopup(url, lanzado) {
    if (lanzado == 'estaciones') {
        myPopup = window.open(url, 'popupWindow', 'width=630,height=380,scrollbars=yes,status=no');
        if (!myPopup.opener) {
            myPopup.opener = self;
        }
    } else if (lanzado == 'variables') {
        myPopup = window.open(url, 'popupWindow', 'width=630,height=380,scrollbars=yes,status=no');
        if (!myPopup.opener) {
            myPopup.opener = self;
        }
    } else if (lanzado == 'sectores') {
        if (nivelEstadisticoActual != 3 && nivelEstadisticoActual != 7 && nivelEstadisticoActual != 31) {
            myPopup = window.open(url, 'popupWindow', 'width=630,height=380,scrollbars=yes,status=no');
            if (!myPopup.opener) {
                myPopup.opener = self;
            }
        }
    }

}
function generarEstadistica(exportar) {

    cadena = document.getElementById("cadena").value;
    sampletypesubstratum = $("#sampletypesubstratum").val();
    
   
   
    if (cadena == null)
        cadena = "";
    cadest = document.getElementById("cadest").value;
    nregion = document.getElementById("nregion").value;

    if (document.getElementById("ndepar") != null) {
        ndepar = document.getElementById("ndepar").value;
    } else {
        ndepar = null;
    }

    nsector1 = document.getElementById("nsector1").value;
    nsector = document.getElementById("nsector").value;
    ntipo = document.getElementById("ntipo").value;
    ntemporada = document.getElementById("ntemporada").value;
    if (document.getElementById("ano1") != null) {
        ano1 = document.getElementById("ano1").value;
    } else {
        ano1 = null;
    }
    if (document.getElementById("ano2") != null) {
        ano2 = document.getElementById("ano2").value;
    } else {
        ano2 = null;
    }


    nproy = document.getElementById("nproy").value;
    opcion = document.getElementById("opcion").value;
    nuser = document.getElementById("nuser").value;
    tam = document.getElementById("tam").value;

    szonas = document.getElementById("szonas").value;

//    alert('nivelEstadisticoActual:'+nivelEstadisticoActual);
//    alert('nivelSeleccionado:'+nivelSeleccionado);

    $.ajax({
        type: "POST",
        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
        url: "estadisticas.jsp",
        data: "nivel=" + nivelSeleccionado + "&tsm="+sampletypesubstratum+"&cadena=" + cadena + "&cadest=" + cadest + "&nregion=" + nregion + "&ndepar=" + ndepar + "&nsector1=" + nsector1 + "&nsector=" + nsector + "&ntipo=" + ntipo + "&ntemporada=" + ntemporada + "&ano1=" + ano1 + "&ano2=" + ano2 + "&nproy=" + nproy + "&opcion=" + opcion + "&nuser=" + nuser + "&tam=" + tam + "&zona=" + szonas,
        beforeSend: function(msg) {
            $('#loading').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='img/loader.gif' alt='Loading...'/><br/>Cargando datos");
        },
        success: function(msg) {
            $('#loading').html('');
            $("#resultado").html(msg);
        },
        error: function(xml, msg) {

            if (msg == 'false') {
                $("#resultado").html('<div id="msgerror" class="mensajeerror" >Revise los par&aacute;metros de Consulta</div>');
            } else {
                $("#resultado").html('<div id="msgerror" class="mensajeerror" >Error generando las estad&iacute;sticas </div>');
            }

        }
    });





}
function LimpiarVariables() {
    var cadest = "";
    var nregion = "";
    var ndepar = "";
    var nsector1 = "";
    var nsector = "";
    var ntipo = "";
    var ntemporada = "";
    var ano1 = "";
    var ano2 = "";
    var nproy = "";
    var opcion = "";
    var nuser = "";
    var tam = "";
    
   
    
    if (document.getElementById('msganos') != null) {
        document.getElementById('msganos').style.display = 'none';
    }


    if (document.getElementById('msnestadistica') != null) {
        document.getElementById('msnestadistica').innerHTML = '';
    }
    if (document.getElementById('divndepar') != null) {
        document.getElementById('divndepar').value = null;
    }

    if (document.getElementById('cadest') != null) {
        document.getElementById('cadest').value = null;
    }
    if (document.getElementById('nsector') != null) {
        document.getElementById('nsector').value = null;
    }
    if (document.getElementById('nsector1') != null) {
        document.getElementById('nsector1').value = null;
    }
    if (document.getElementById('ntemporada') != null) {
        document.getElementById('ntemporada').value = null;
    }
    if (document.getElementById('selestaciones') != null) {
        document.getElementById('selestaciones').style.diplay = 'none';
    }

    document.getElementById('cadena').value = '';

    $("#sampletypesubstratum option[value!='-1']").remove();

}
function getTemporadasPorDepartamento() {
    var departamento = document.getElementById("ndepar").value;
    getTemporadasPorRegion(departamento);
}
function getTemporadasPorRegion(departamento) {
    var region = document.getElementById("nregion").value;
}
function periodosConAnosPorDepartamento() {

    var region = document.getElementById("nregion").value;
    var departamento = document.getElementById("ndepar").value;
    if (nivelEstadisticoActual != 29 && nivelEstadisticoActual != 30 && nivelEstadisticoActual != 31 && nivelEstadisticoActual != 32 && nivelEstadisticoActual != 33) {
        $.ajax({
            type: "POST",
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            url: "anoPorParametros.jsp",
            data: "region=" + region + "&departamento=" + departamento + "&nivel=" + nivelSeleccionado,
            beforeSend: function(msg) {
                $('#loading').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='img/loader.gif' alt='Loading...'/>");
            },
            success: function(msg) {
                $('#loading').html('');
                $("#anosbusqueda").html(msg);
            },
            error: function(xml, msg) {

                $("#anosbusqueda").html('Error consultando los dapartamentos');
            }
        });
    }
}
function DepartamentosPorRegion() {

    var region = document.getElementById("nregion").value;
    //alert(nivelEstadisticoActual);
    if (region != '-') {
        if (nivelEstadisticoActual == 1 || nivelEstadisticoActual == 2 || nivelEstadisticoActual == 3 || nivelEstadisticoActual == 5 || nivelEstadisticoActual == 6 || nivelEstadisticoActual == 7 || nivelEstadisticoActual == 29 || nivelEstadisticoActual == 30 || nivelEstadisticoActual == 31 || nivelEstadisticoActual == 38) {

            $.ajax({
                type: "POST",
                contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                url: "departamentosPorRegion.jsp",
                data: "region=" + region + "&nivel=" + nivelSeleccionado,
                beforeSend: function(msg) {
                    $('#loading').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='img/loader.gif' alt='Loading...'/>");
                },
                success: function(msg) {
                    $('#loading').html('');
                    $("#divndepar").html(msg);
                },
                error: function(xml, msg) {

                    $("#divndepar").html('Error consultando los dapartamentos');
                }
            });
        }

        if (nivelEstadisticoActual != 29 && nivelEstadisticoActual != 30 && nivelEstadisticoActual != 31 && nivelEstadisticoActual != 32 && nivelEstadisticoActual != 33) {
            $.ajax({
                type: "POST",
                contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                url: "anoPorParametros.jsp",
                data: "region=" + region + "&nivel=" + nivelSeleccionado,
                beforeSend: function(msg) {
                    $('#loading').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='img/loader.gif' alt='Loading...'/>");
                },
                success: function(msg) {
                    $('#loading').html('');
                    $("#anosbusqueda").html(msg);
                },
                error: function(xml, msg) {

                    $("#anosbusqueda").html('Error consultando los dapartamentos');
                }
            });
        }
//getTemporadasPorRegion(null);               

    }

}
function zonesOfSector()
{
    var nsector = document.getElementById("nsector").value;
    $("#szonas").empty();
    $("#szonas").attr("display:", "block");
    if (nsector != null) {
        $.post("/siam/ServicioComponentes?componente=redcam&action=Zones", {
            sector: nsector,
        }, function(data) {
            $.each(data, function(index, item) {
//                console.log(item.codigo + "-" + item.nombre);
                $("#szonas").append("<option value=" + item.codigo + ">" + item.nombre + "</option>");
            });
        }, "json");
    }

}
function sectorSelect() {
    var cadena = document.forma.sectores.value;
    var nombresector = document.forma.sectores;
    document.formacam.nsector.value = cadena;
    document.formacam.nsector1.value = nombresector.options[nombresector.selectedIndex].text;
    if (nivelEstadisticoActual == 38) {
        zonesOfSector();
    }
}
function seleccionarVariables() {
    var cadena = "";

    for (i = 0; i < document.formavariables.variable.length; i++) {
        if (document.formavariables.variable.options[i].selected == true) {
            cadena = cadena + document.formavariables.variable.options[i].value + ",";
        }

    }
    document.formacam.cadena.value = cadena;

    findTypeSubtratumSample();

    $("#variablesdialog").dialog("destroy");

}
function RenderizarVariables() {

    var variactivas = false;
    var vardesactivas = false;

    if ($('input[name=active]').is(':checked')) {
        variactivas = true;
    }

    if ($('input[name=desactive]').is(':checked')) {
        vardesactivas = true;
    }

    $.ajax({
        type: "get",
        url: "variablesFiltradas.jsp",
        data: "tipovariable=<%=vari%>&varActivas=" + variactivas + "&varDesactivas=" + vardesactivas,
        beforeSend: function(msg) {
            $('#resultsetVariables').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='img/loader.gif' alt='Loading...'/>");

        },
        success: function(msg) {
            $('#resultsetVariables').html('');
            $('#resultsetVariables').html(msg);

        },
        error: function(xml, msg) {
            $('#resultsetVariables').html('Error cargando los variables. Consulte con el Laboratorio de Sistemas de informacion.');

        }
    });

}
function seleccionarest() {
    var cadest = "";
    for (i = 0; i < document.formaestaciones.estacion.length; i++) {
        if (document.formaestaciones.estacion.options[i].selected == true) {
            cadest = cadest + document.formaestaciones.estacion.options[i].value + ",";
        }
    }
    document.formacam.cadest.value = cadest;
    $("#estacionesdialog").dialog("destroy");

}
function RenderizarEstaciones() {

    var estaactivas = false;
    var estadesactivas = false;
    var codigosector = document.formacam.nsector.value;

    if ($('input[name=active]').is(':checked')) {
        estaactivas = true;
    }

    if ($('input[name=desactive]').is(':checked')) {
        estadesactivas = true;
    }

    $.ajax({
        type: "get",
        url: "estacionesFiltradas.jsp",
        data: "proyecto='REDCAM'&estaactivas=" + estaactivas + "&estadesactivas=" + estadesactivas + "&codigosector=" + codigosector,
        beforeSend: function(msg) {
            $('#resultsetEstaciones').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='img/loader.gif' alt='Loading...'/>");

        },
        success: function(msg) {
            $('#resultsetEstaciones').html('');
            $('#resultsetEstaciones').html(msg);

        },
        error: function(xml, msg) {
            $('#resultsetEstaciones').html('Error cargando las estaciones. Pongase en contacto con el laboratorio de sistemas de informacion.');

        }
    });

}



