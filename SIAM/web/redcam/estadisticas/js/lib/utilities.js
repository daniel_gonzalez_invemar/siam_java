
var FormUtil = new Object;

/**
 * The method to set the focus on the first field first checks to ensure that a form exists on the page. 
 * It does this by checking the value of document.forms.length:
 * 
 * Be careful when using this method. In slow-loading pages, it is possible that the
 * user may start typing into a field before the page has been fully loaded. When the
 * focus is then set to the first field, it disrupts the users input. To prepare for this
 * issue, first check for a value in the first field; if one is there, dont set the focus to it.
 */
FormUtil.focusOnFirst = function () {
	if (document.forms.length > 0) {
		for (var i=0; i < document.forms[0].elements.length; i++) {
			var oField = document.forms[0].elements[i];
			if (oField.type != "hidden") {
				oField.focus();
				return;
			}
		}
	}
};

/**
 * 
 */
FormUtil.setTextboxes = function() {
	var colInputs = document.getElementsByTagName("input");
	var colTextAreas = document.getElementsByTagName("textarea");
	for (var i=0; i < colInputs.length; i++){
		if (colInputs[i].type == "text" || colInputs [i].type == "password") {
			colInputs[i].onfocus = function () { this.select(); };
		}
	}
	for (var i=0; i < colTextAreas.length; i++){
		colTextAreas[i].onfocus = function () { this.select(); };
	}
};

/**
 * 
 * @param {Object} oTextbox
 */

FormUtil.tabForward = function(oTextbox) {
	var oForm = oTextbox.form;
//make sure the textbox is not the last field in the form
	if (oForm.elements[oForm.elements.length-1] != oTextbox
			&& oTextbox.value.length == oTextbox.getAttribute("maxlength")) {
		for (var i=0; i < oForm.elements.length; i++) {
			if (oForm.elements[i] == oTextbox) {
				for(var j=i+1; j < oForm.elements.length; j++) {
					if (oForm.elements[j].type != "hidden") {
						oForm.elements[j].focus();
						return;
					}
				}
				return;
			}
		}
	}
};



var TextUtil = new Object();

TextUtil.isNotMax = function(oTextArea) {
	return oTextArea.value.length != oTextArea.getAttribute("maxlength");
}



var ListUtil = new Object();

/**
 * var oListbox = document.getElementById("selListbox");
 * var arrIndexes = ListUtil.getSelectedIndexes(oListbox);
 * alert("There are " + arrIndexes.length + " option selected."
 * + "The options have the indexes " + arrIndexes + ".");
 * 
 * @param {Object} oListbox
 */
ListUtil.getSelectedIndexes = function (oListbox) {
	var arrIndexes = new Array;
	for (var i=0; i < oListbox.options.length; i++) {
		if (oListbox.options[i].selected) {
				arrIndexes.push(i);
		}
	}
	return arrIndexes;
};

/**
 * 
 * @param {Object} oListbox
 */
ListUtil.getSelectedIndex= function(oListbox){
	
	return oListbox.selectedIndex;
}

/**
 * 
 * @param {Object} oListbox
 */
ListUtil.getValueSelected= function (oListbox){
	return oListbox.options[oListbox.selectedIndex].value;
}

/**
 * 
 * @param {Object} oListbox
 * @param {Object} i
 */
ListUtil.getValueByIndex= function (oListbox, i){
	return oListbox.options[i].value;
}

/**
 * 
 * @param {Object} oListbox
 */
ListUtil.getTextSelected=function (oListbox){
	return oListbox.options[oListbox.selectedIndex].text;
}

/**
 * 
 * @param {Object} oListbox
 * @param {Object} i
 */
ListUtil.getTextByIndex= function (oListbox, i){
	return oListbox.options[i].text;
}


/**
 * var oListbox = document.getElementById("selListbox");
 * ListUtil.add(oListbox, "New Display Text"); //add option with no value
 * ListUtil.add(oListbox, "New Display Text 2", "New value"); //add option with value
 * 
 * @param {Object} oListbox
 * @param {Object} sName
 * @param {Object} sValue
 */
ListUtil.add = function (oListbox, sName, sValue) {
	var oOption = document.createElement("option");
	oOption.appendChild(document.createTextNode(sName));
	if (arguments.length == 3) {
		oOption.setAttribute("value", sValue);
	}
	oListbox.appendChild(oOption);
}
/**
 * var oListbox = document.getElementById("selListbox");
 * ListUtil.remove(oListbox, 0); //remove the first option
 * @param {Object} oListbox
 * @param {Object} iIndex
 */
ListUtil.remove = function (oListbox, iIndex) {
	//oListbox.remove(iIndex);
	oListbox.removeChild(oListbox.childNodes[iIndex]);
}

/**
 * 
 * 
 * @param {Object} oListbox
 */
ListUtil.clear = function (oListbox) {
	for (var i=oListbox.options.length-1; i >= 0; i--) {
		ListUtil.remove(oListbox, i);
	}
};

/**
 * var oListbox1 = document.getElementById("selListbox1");
 * var oListbox2 = document.getElementById("selListbox2");
 * ListUtil.move(oListbox1, oListbox2, 0); //move the first option
 * @param {Object} oListboxFrom
 * @param {Object} oListboxTo
 * @param {Object} iIndex
 */
ListUtil.move = function (oListboxFrom, oListboxTo, iIndex) {
	var oOption = oListboxFrom.options[iIndex];
	if (oOption != null) {
		oListboxTo.appendChild(oOption);
	}
}
/**
 * var oListbox = document.getElementById("selListbox");
 * ListUtil.shiftUp(oListbox,1); //move the option in position 1 up one spot
 * @param {Object} oListbox
 * @param {Object} iIndex
 */
ListUtil.shiftUp = function (oListbox, iIndex) {
	if (iIndex > 0) {
		var oOption = oListbox.options[iIndex];
		var oPrevOption = oListbox.options[iIndex-1];
		oListbox.insertBefore(oOption, oPrevOption);
	}
};
/**
 * var oListbox = document.getElementById("selListbox");
 * ListUtil.shiftDown(oListbox,2); //move the option in position 2 down one spot
 * @param {Object} oListbox
 * @param {Object} iIndex
 */
ListUtil.shiftDown = function (oListbox, iIndex) {
	if (iIndex < oListbox.options.length - 1) {
		var oOption = oListbox.options[iIndex];
		var oNextOption = oListbox.options[iIndex+1];
		oListbox.insertBefore(oNextOption, oOption);
	}
};




