<%@page import="java.net.URLDecoder"%>
<%@page import="co.org.invemar.siam.redcam.ServicioEstadistica"%>
<html>
    <head>
        <!--Load the AJAX API-->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="js/excellentexport.min.js"></script>
        <link href="../../plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css" />

        <%
            HttpSession session1 = request.getSession(true);

            String department = request.getParameter("department");
            System.out.println("department:"+department);
            String depart = URLDecoder.decode(new String(department.getBytes("UTF-8"), "ISO-8859-1"));
           System.out.println("depart:"+depart);
            
            String year = request.getParameter("year");
            
            
            String usu = (String) session.getAttribute("usu_id1");
            if (usu == null) {%>
        <script language="JavaScript">
            alert("No tiene permisos para ingresar a esta p�gina");
            window.parent.location = "../login.htm";
        </script>
        <%       }
        %>

        <script type="text/javascript">

            google.load("visualization", "1.1", {'packages': ['controls', 'table']});
            google.setOnLoadCallback(drawChart);



            

            function drawChart() {


            <%
                ServicioEstadistica se = new ServicioEstadistica();

            %>


                var dashboard = new google.visualization.Dashboard(document.getElementById('dashboard_div'));


                var datum = new google.visualization.DataTable();
                datum.addColumn('string', 'Departamentos');
                datum.addColumn('string', 'Sectores');
                datum.addColumn('string', 'Estaciones');
                datum.addColumn('string', 'Fecha');
                datum.addColumn('string', 'Variables');
                datum.addColumn('string', 'Valor');
                datum.addColumn('string', 'Unidad');
                datum.addColumn('string', 'Sustrato muestra');
                datum.addColumn('string', 'Sustrato');
                datum.addColumn('string', 'Profundidad');
                datum.addColumn('string', 'Temporada');

                datum.addRows(<%=se.getAllDataRedCamStationView(department,year)%>);


              /*  var departamentPicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'departamentos',
                    'options': {
                        'filterColumnLabel': 'Departamentos',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'caption': 'Seleccione...',
                            'allowMultiple': false,
                            'selectedValuesLayout': 'belowStacked'
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': ['ANTIOQUIA']
                    }

                });*/



                var estPicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'estaciones',
                    'options': {
                        'filterColumnLabel': 'Estaciones',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'caption': 'Seleccione...',
                            'allowMultiple': false,
                            'selectedValuesLayout': 'belowStacked'
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': ['C05003018 - (S) Km Arriba rio Currulao-DGI012']
                    }

                });



                var variablesPicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'variables',
                    'options': {
                        'filterColumnLabel': 'Variables',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'allowMultiple': true,
                            'caption': 'Seleccione...',
                            'selectedValuesLayout': 'belowStacked'
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': ['']
                    }

                });

                var sectoresPicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'sectores',
                    'options': {
                        'filterColumnLabel': 'Sectores',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'allowMultiple': false,
                            'caption': 'Seleccione...',
                            'selectedValuesLayout': 'belowStacked'
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': ['Turbo al Atrato']
                    }

                });


                var sustratumSamplePicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'sustrato_muestra',
                    'options': {
                        'filterColumnLabel': 'Sustrato muestra',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'allowMultiple': false,
                            'caption': 'Seleccione...',
                            'selectedValuesLayout': 'belowStacked'
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': ['']
                    }

                });


                var sustratumPicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'sustrato',
                    'options': {
                        'filterColumnLabel': 'Sustrato',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'allowMultiple': false,
                            'caption': 'Seleccione...',
                            'selectedValuesLayout': 'belowStacked'
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': ['']
                    }

                });



                var profundidadPicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'profundidad',
                    'options': {
                        'filterColumnLabel': 'Profundidad',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'allowMultiple': false,
                            'caption': 'Seleccione...',
                            'selectedValuesLayout': 'belowStacked'
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': ['']
                    }

                });


                var temporadaPicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'temporada',
                    'options': {
                        'filterColumnLabel': 'Temporada',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'allowMultiple': false,
                            'caption': 'Seleccione...',
                            'selectedValuesLayout': 'belowStacked'
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': ['']
                    }

                });

               /* var yearPicker = new google.visualization.ControlWrapper({
                    'controlType': 'CategoryFilter',
                    'containerId': 'anio',
                    'options': {
                        'filterColumnLabel': 'Fecha',
                        'ui': {
                            'labelStacking': 'vertical',
                            'allowTyping': false,
                            'allowMultiple': true,
                            'caption': 'Seleccione...',
                            'selectedValuesLayout': 'belowStacked'
                        }
                    }
                    ,
                    'state': {
                        'selectedValues': ['']
                    }

                });*/

                // var table = new google.visualization.Table(document.getElementById('table'));
                // Define a table
                var table = new google.visualization.ChartWrapper({
                    'chartType': 'Table',
                    'containerId': 'table',
                    'options': {
                        //			  'width': '1500px'
                        'fontSize': 9

                    },
                    'view': {
                        'columns': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }
                });
                
                //dashboard.bind(, [sectoresPicker, estPicker, variablesPicker, sustratumSamplePicker, sustratumPicker, table]);
                dashboard.bind(sectoresPicker, [estPicker, sustratumSamplePicker, sustratumPicker, profundidadPicker, temporadaPicker,  table]);
                dashboard.bind(estPicker, [variablesPicker, sustratumSamplePicker, sustratumPicker, profundidadPicker, temporadaPicker,  table]);
                dashboard.bind(variablesPicker, [sustratumSamplePicker, sustratumPicker, profundidadPicker, temporadaPicker,  table]);
                dashboard.bind(sustratumSamplePicker, [sustratumPicker, profundidadPicker, temporadaPicker,  table]);
                dashboard.bind(profundidadPicker, [temporadaPicker, table]);
                dashboard.bind(temporadaPicker, [table]);             
                dashboard.draw(datum);
            }


        </script>
        <title>Microdatos por Departamento y A�o </title>
    </head>

    <body>
        <jsp:include page="../../plantillaSitio/headermodulosv3.jsp?idsitio=60"/>
        <div  id="menu" style="margin:auto;width:963px;">
            <a href="menuOpcionesPrivadas.jsp?salir=ok"><img src="../images/botonsalir.png" alt="" />Salir de forma segura.</a>
        </div>
        <div id="contenedorestadistico">
            <div id="cabecera" style="margin-left: 220px">
                <h1>Estadisticas por sectores y  estaciones</h1>

            </div>           
            <div id="menu" style="margin-left: 220px">
                <div id="departamentos"></div>   
                <div id="sectores" ></div>
                <div id="estaciones"></div>               
                <div id="variables" ></div>  
                <div id="sustrato_muestra"></div>
                <div id="sustrato" ></div>               
                <div id="profundidad" ></div> 
                <div id="temporada" ></div>    
                <div id="anio" ></div>  
            </div>

            <div id="dashboard_div" >
                <a download="data.xls" style="margin-left: 220px"  href="#" onclick="return ExcellentExport.excel(this, 'table', 'Data');">
                    <img src="../images/Excel-icon.png" width="50px" height="60px"/></a>

                <center><div id="table" ></div></center>
            </div>

            <div id="pie">               



            </div>
        </div>
        <%@ include file="../../plantillaSitio/footermodulesV3.jsp" %>
        <script type="text/javascript">
            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
            try {
                var pageTracker = _gat._getTracker("UA-2300620-4");
                pageTracker._trackPageview();
            } catch (err) {
            }
        </script>
    </body>
</html>