<%@ page import="coneccam.*" %>
<%@ page import="cvariablegeog.*" %>
<%@ page import="utilidades.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>


<%// HttpSession session1 = request.getSession(true);
 //  String nuser = (String)session.getAttribute("usu_id1");
    String nuser = "ljarias";
    if (nuser == null) {
%>

<script type="text/javascript" language="JavaScript">
    alert("No tiene permisos para ingresar a esta pagina");
    window.parent.location = "login.htm";
</script>

<%  }%>
<%
    HttpSession session1 = request.getSession(true);
    String usu = (String) session.getAttribute("usu_id1");
    if (usu == null) { %>
<script language="JavaScript">
    alert("No tiene permisos para ingresar a esta p�gina");
    window.parent.location = "login.htm";
</script>
<%
    }
%>
<%
    cvariablegeog.Cvariablegeog var = new cvariablegeog.Cvariablegeog();
    cvariablegeog.Ccontvariablegeog cvar = new cvariablegeog.Ccontvariablegeog();
    cvariablegeog.Ccontvariablegeog ctipo = new cvariablegeog.Ccontvariablegeog();
    cvariablegeog.Ccontvariablegeog cdepto = new cvariablegeog.Ccontvariablegeog();
//  cvariablegeog.Ccontvariablegeog csector= new cvariablegeog.Ccontvariablegeog();

    cvariablegeog.Cuserproj user = new cvariablegeog.Cuserproj();
    cvariablegeog.Ccontuserproj cuser = new cvariablegeog.Ccontuserproj();

    coneccam.Cconexioncam con2 = new coneccam.Cconexioncam();
    Connection conn2 = null;

    String opcion = "2";
    String nproy = "REDCAM";
    // request.getParameter("nproy");
    String nomproy = request.getParameter("nomproy");

    String nconsulta = request.getParameter("nconsulta");

    String nsector = null;
    String ntemporada = request.getParameter("ntemporada");

    conn2 = con2.getConn();
    ctipo.contenedortipovaric(conn2, nproy, nuser, opcion);
    cdepto.contenedordeptoestac(conn2, nsector, nproy, nuser, opcion);
    cuser.contenedornproyectos(conn2, nuser);

    Calendar fecha1 = null;
    Calendar fecha2 = null;
    int xx = 0;
    String ano1 = "", mes1 = "", dia1 = "", ano2 = "", mes2 = "", dia2 = "";
    if (request.getParameter("ano1") != null && !request.getParameter("ano1").equals("")) {
        ano1 = request.getParameter("ano1");
    }
    if (request.getParameter("ano2") != null && !request.getParameter("ano2").equals("")) {
        ano2 = request.getParameter("ano2");
    }

    float ind = 0;
    int tam = 0;
    int paginas = 1;

    //pagina actual del resultado 1= valor inicial
    int pag = 1;

    //numero de registros por pagina
    float registros = 10;
    String pagina = request.getParameter("pagina");

    if (con2 != null) {
        con2.close();
    }

    String sele = "";
%>

<html>
    <title>.::Sistema de Informaci&oacute;n Ambiental Marina de Colombia - SIAM::.</title>
    <meta name="keywords" content="Sistema de Informacin Ambiental Marina de Colombia, biologia, bases de datos, metadatos, SIAM, SIAC, Colombia, calidad ambiental, REDCAM, INVEMAR, mapas" />
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" src="js/lib/prototype/prototype.js"></script>  
    <script type="text/javascript" language="JavaScript">
    function validar() {
        if (document.formacam.ano1.value != '') {
            if (!ValidarFecha(document.formacam.ano1.value)) {
                return false;
            }
        }
        if (document.formacam.ano2.value != '') {
            if (!ValidarFecha(document.formacam.ano2.value)) {
                return false;
            }
        }
        return true;
    }

    function abrirprj() {
        var nproy = document.formacam.nproy.value;
        document.location.href = "estadisticasPorEstaciones.jsp?nproy=" + nproy;
    }


    function openPopup(url) {
        myPopup = window.open(url, 'popupWindow', 'width=530,height=280,scrollbars=yes,status=no');
        if (!myPopup.opener)
            myPopup.opener = self;
    }


    /**
     * 
     * @param {Object} value
     * @param {Object} page
     */
    function fillVariable(value, page) {
        var url = "variablesPorEstaciones.jsp?action=variable&estacion=" + value;
        new Ajax.Request(url,
                {method: "post",
                    onComplete: function (transport) {
                        //					alert(transport);
                        fillSelects('variable', transport, true);
                    }
                });
    }

    function fillAnos(value, page) {
        
        var url = "variablesPorEstaciones.jsp?action=ano&variable=" + value + "&estacion=" + $('estacion').value;
        $('samplesubtratum').empty();
        new Ajax.Request(url,
                {method: "post",
                    onComplete: function (transport) {
                        fillSelectsAnos('ano1', 'ano2', transport, true);
                    }
                });
        
    }

    function fillSampleSusbtratum() {
           
           var selectTag = document.getElementById("estacion");
           var variableTag = document.getElementById("variable");
           var year1Tag = document.getElementById("ano1");
           var year2Tag = document.getElementById("ano2");
           
           var codStation =selectTag.options[selectTag.selectedIndex].value;
           var codVar =variableTag.options[variableTag.selectedIndex].value;
           var year1=year1Tag.options[year1Tag.selectedIndex].value;
           var year2 = year2Tag.options[year2Tag.selectedIndex].value;         
           
           var URL ="/siam/ServicioComponentes?componente=redcam&action=GetSampleSubtratum&codVariable="+codVar+"&codStation="+codStation+"&year1="+year1+"&year2="+year2;
           new Ajax.Request(URL,{
               onSuccess: function(data) {                   
                   var json = data.response.evalJSON(true);                
                   $('samplesubtratum').empty();
                   document.getElementById("samplesubtratum").options.length = 0;
                   for (var i = 0;i < json.length; i++) {
                      
                       var opt = document.createElement('option');
                       opt.text = json[i].name;
                       opt.value =json[i].id;
                       $('samplesubtratum').options.add(opt);
                   }
               }
           });
           }
    function fillSelects(id, transport, fline) {
        //var opts=transport.responseText.evalJSON(true);

        //	var responses=transport.responseXML;
        if (window.ActiveXObject) { // If IE Windows
            var responses = new ActiveXObject("Microsoft.XMLDOM");
            responses.loadXML(transport.responseText);
        } else {
            var responses = transport.responseXML;
        }

        //	alert(responses);

        //	console.log(responses);					
        var sel = $(id);
        //	console.log(sel.id);

        sel.length = 0;
        if (fline) {
            sel.options[0] = new Option("--", "");
        }

        var variables = responses.getElementsByTagName("variables")[0];

        for (i = 0; i < variables.childNodes.length; i++) {
            var variable = variables.childNodes[i];
            var codigo = variable.getElementsByTagName("codigo")[0];
            var nombre = variable.getElementsByTagName("nombre")[0];
            appendOpcionesSelect(codigo.childNodes[0].nodeValue, nombre.childNodes[0].nodeValue, sel);
        }

    }

    function fillSelectsAnos(id1, id2, transport, fline) {
        //var opts=transport.responseText.evalJSON(true);

        var responses = transport.responseXML;
        //	console.log(responses);					
        var sel1 = $(id1);
        var sel2 = $(id2);
        //	console.log(sel.id);

        sel1.length = 0;
        sel2.length = 0;
        if (fline) {
            sel1.options[0] = new Option("--", "");
            sel2.options[0] = new Option("--", "");
        }

        var anos = responses.getElementsByTagName("anos")[0];

        for (i = 0; i < anos.childNodes.length; i++) {
            var ano = anos.childNodes[i];
            //                  var codigo = variable.getElementsByTagName("codigo")[0];
            //                  var nombre = variable.getElementsByTagName("nombre")[0];
            appendOpcionesSelect(ano.childNodes[0].nodeValue, ano.childNodes[0].nodeValue, sel1);
            appendOpcionesSelect(ano.childNodes[0].nodeValue, ano.childNodes[0].nodeValue, sel2);
        }

    }

    function appendOpcionesSelect(valor, name, pp)
    {
        var optionElement;
        //			console.log(valor);
        //			console.log(pp.name);
        optionElement = document.createElement("option");
        optionElement.setAttribute("value", valor);
        optionElement.appendChild(document.createTextNode(name));
        pp.appendChild(optionElement);
    }
    </script>



    <style type="text/css">
        <!--
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }
        -->
    </style>
    <link href="../../plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <jsp:include page="../../plantillaSitio/headermodulosv3.jsp?idsitio=60"/>
    <div  id="menu" style="margin:auto;width:963px;">
        <a href="menuOpcionesPrivadas.jsp"><img src="../images/back.png" alt=""  />Regresar al menu opciones </a>| <a href="estadisticasPorEstaciones.jsp?salir=ok">
            <img src="../images/botonsalir.png" alt="" />Salir de forma segura.</a>
    </div>
    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td><img src="../../images/spacer.gif" width="5" height="5" alt="" /></td>
        </tr>
    </table>
    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">  
        <tr>
            <td colspan="2"><img src="../../images/spacer.gif" width="5" height="5" alt=""/></td>
        </tr>
        <tr bgcolor="#8DC0E3">
            <td width="20"></td>
            <td width="423" bgcolor="#8DC0E3" class="linksnegros">Red de Calidad Ambiental
                Marina REDCAM :: Estad&iacute;sticas por estaci&oacute;n</td>
        </tr>
    </table>


    <form target="_blank" action="resultadoEstadisticasPorEstacion.jsp" method="get" name="formacam" id="formacam" onSubmit="return validar();" >
        <input type="hidden" name="cadena" value="">
        <input type="hidden" name="cadest" value="">
        <div align="left">
            <table width="100%" border="0" cellpadding="3" cellspacing="0" height="185">
                <tr>
                    <td width="100%" height="182">          <div align="right">

                            <table width="963px" border="0" align="center">
                                <tr>
                                    <td height="8" colspan="4" bordercolor="#000066" bgcolor="#CCCCCC"><span style="font-weight:bold;color:black;font-size:0.7em;font-family:Verdana, Arial, Helvetica, sans-serif">
                                            Par&aacute;metros  de Consulta</span></td>
                                </tr>

                                <tr> 
                                    <td width="17%" height="24" bordercolor="#000066" bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Depto / Estacion</font></td>
                                    <td width="30%" bordercolor="#000066" bgcolor="#F9F9F9"><select name="estacion" id="estacion" onChange="fillVariable(this.value, 'estadisticasPorEstacines');">
                                            <option value="0">--</option>
                                            <%
                                                for (int j = 0; j < cdepto.gettamano(); j++) {
                                                    var = new cvariablegeog.Cvariablegeog();
                                                    var = cdepto.getvariablegeog(j);
                                            %>
                                            <option value="<%=var.get_codest()%>"><%=var.get_depto()%> - <%=var.get_nomest()%></option>
                                            <%}%>
                                        </select></td>
                                    <td width="9%" bordercolor="#000066" bgcolor="#999999">&nbsp;</td>
                                    <td width="44%" height="24" bordercolor="#000066" bgcolor="#F9F9F9">&nbsp;</td>
                                <input type="hidden" name="nsector" value="" >
                                </tr>
                                <tr> 
                                    <td height="2" bordercolor="#000066" bgcolor="#999999"><span style="color:#FFFFFF;font-size:0.7em;font-family:Verdana, Arial, Helvetica, sans-serif">Tipo / Variable</span></td>
                                    <td bordercolor="#000066" bgcolor="#F9F9F9"> <select name="variable" id="variable" onChange="fillAnos(this.value, 'estadisticasPorEstacines');">
                                            <%//onChange="fillAnos(this.value,'consul_estadisticas_estacion_jx');"%>
                                            <option value="">--</option>
                                        </select></td>
                                    <td bordercolor="#000066" bgcolor="#999999"></td>
                                    <td height="2" bordercolor="#000066" bgcolor="#F9F9F9"> </td>
                                </tr>
                                <tr> 
                                    <td height="2" bordercolor="#000066" bgcolor="#999999"><span style="color:#FFFFFF;font-size:0.7em;font-family:Verdana, Arial, Helvetica, sans-serif">Rango
                                            de a&ntilde;os</span></td>
                                    <td height="2" bordercolor="#000066" bgcolor="#F9F9F9"><span style="color:black;font-size:0.7em;font-family:Verdana, Arial, Helvetica, sans-serif">De:</span> 


                                        <select name="ano1" id="ano1" onchange='fillSampleSusbtratum()'>
                                            <option value="">--</option>                  
                                        </select>

                                        A: 


                                        <select name="ano2" id="ano2" onchange='fillSampleSusbtratum()'>
                                            <option value="">--</option>                  
                                        </select>
                                        </font> </td>
                                    <td width="9%" bordercolor="#000066" bgcolor="#999999"><span style="color:#FFFFFF;font-size:0.7em;font-family:Verdana, Arial, Helvetica, sans-serif">Temporada</span></td>
                                    <td width="44%" height="24" bordercolor="#000066" bgcolor="#F9F9F9"><select name="ntemporada" id="ntemporada">
                                            <option value="">--</option>
                                            <option value="1">Seca</option>
                                            <option value="2">Lluviosa</option>
                                        </select></td>              
                                </tr>
                                <tr> 
                                    <td height="2" bordercolor="#000066" bgcolor="#999999"><span style="color:#FFFFFF;font-size:0.7em;font-family:Verdana, Arial, Helvetica, sans-serif">Sustrato muestra
                                        </span></td>
                                    <td height="2" bordercolor="#000066" bgcolor="#F9F9F9"><span style="color:black;font-size:0.7em;font-family:Verdana, Arial, Helvetica, sans-serif"></span> 


                                        <select name="samplesubtratum" id="samplesubtratum">
                                            <option value="">--</option>                  
                                        </select>


                                    <td width="9%" bordercolor="#000066" bgcolor="#999999"><span style="color:#FFFFFF;font-size:0.7em;font-family:Verdana, Arial, Helvetica, sans-serif"></span></td>
                                    <td width="44%" height="24" bordercolor="#000066" bgcolor="#F9F9F9">                                        
                                    </td>              
                                </tr>
                                <tr> 
                                    <td height="28" colspan="4" bordercolor="#000066" bgcolor="#CCCCCC"> 
                                        <div align="right"  style="color:black;font-size:0.7em;font-family:Verdana, Arial, Helvetica, sans-serif">
                                            <input name="nproy" type="hidden" value="REDCAM" />
                                            <input name="opcion" type="hidden" value="2" />
                                            <input name="nuser" type="hidden" value="ljarias" />
                                            <input type="reset" name="Submit2" value="Limpiar" />
                                            <input type="submit" name="Submit" value="Buscar &gt;&gt;" />
                                        </div></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="100%" height="2" valign="top">
                    </td>
                </tr>
            </table>
        </div>
        <input type="hidden" name="tam" value="<%=tam%>" />
    </form>

    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td><img src="../../images/spacer.gif" width="5" height="5" alt=""/></td>
        </tr>
        <tr>
            <td ></td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>
    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td></td>
        </tr>


    </table>
    <%@ include file="../../plantillaSitio/footermodulesV3.jsp" %>


    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var pageTracker = _gat._getTracker("UA-2300620-4");
            pageTracker._trackPageview();
        } catch (err) {
        }
    </script>

</body>
</html>