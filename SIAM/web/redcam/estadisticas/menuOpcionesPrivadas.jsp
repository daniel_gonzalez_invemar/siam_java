<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>.::Sistema de Informaci&oacute;n Ambiental Marina de Colombia - SIAM::.</title>
        <meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, REDCAM, INVEMAR, mapas">
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="cache-control" content="no-cache" />
        <link href="../../plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css" />

       

    </head>
    <%

        HttpSession session1 = request.getSession(true);
        String usu = (String) session.getAttribute("usu_id1");
        // System.out.println("contect path"+request.getContextPath());
        if (usu == null) {%>
    <script language="JavaScript">
        alert("No tiene permisos para ingresar a esta p�gina");
        window.parent.location = "../login.htm";
    </script>
    <%       }
        String command = (String) request.getParameter("salir");
        if (command != null && command.equals("ok")) {
            session.invalidate();
            session = null;
            response.sendRedirect("../login.htm");
        }
    %>

    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <jsp:include page="../../plantillaSitio/headermodulosv3.jsp?idsitio=60"/>
        <table  style="display:none;"width="750" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="3"><img name="head_r1_c1" src="../../images/head_r1_c1.jpg" width="3" height="81" border="0" alt=""></td>
                <td width="105" bgcolor="#096A95"><img src="../../images/invemar_azul.gif" width="105" height="81"></td>
                <td width="507" bgcolor="#096A95"><p class="titulohead">Sistema de Informaci&oacute;n
                        Ambiental<br>
                        Marina de Colombia - SIAM</p>
                </td>
                <td width="38" align="right" bgcolor="#096A95"><a href="../../index.htm"><img name="head_r1_c4" src="../../images/head_r1_c4.jpg" width="37" height="81" border="0" alt="Home"></a></td>
                <td width="43" align="right" bgcolor="#096A95"><a href="../../contactenos.htm"><img name="head_r1_c5" src="../../images/head_r1_c5.jpg" width="43" height="81" border="0" alt="Cont&aacute;ctenos"></a></td>
                <td width="34" align="right" bgcolor="#096A95"><a href="../../mapadelsitio.htm"><img name="head_r1_c6" src="../../images/head_r1_c6.jpg" width="34" height="81" border="0" alt="Mapa del sitio"></a></td>
                <td colspan="2" align="right" valign="top" bgcolor="#096A95"><img name="head_r1_c7" src="../../images/head_r1_c7.jpg" width="3" height="81" border="0" alt=""/></td>
            </tr>
            <tr>
                <td width="3"></td>
                <td width="105"></td>
                <td></td>
                <td width="38"></td>
                <td width="43"></td>
                <td width="34"></td>
                <td width="12"></td>
                <td width="8"></td>
            </tr>
        </table>
        <table width="750" style="display:none;" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td><img src="../../images/spacer.gif" width="5" height="5"></td>
            </tr>
        </table>
        <table width="750" border="0" align="center" cellpadding="0" cellspacing="0" style="display:none;">
            <tr bgcolor="#096A95">
                <td colspan="3" class="titulo_ubicacion"><img src="../../images/spacer.gif" width="5" height="5"/><a href="../index.htm" class="titulo_ubicacion">Inicio</a> &gt; <a href="../siam.htm" class="titulo_ubicacion">Productos de Informaci&oacute;n</a> &gt; <a href="../redcam/index.htm" class="titulo_ubicacion">RedCAM </a> &gt; <a href="menuOpcionesPrivadas.jsp" class="titulo_ubicacion">Secci&oacute;n de usuarios registrados</a></td>
            </tr>
            <tr>
                <td colspan="3"><img src="../../images/spacer.gif" width="5" height="5"/></td>
            </tr>
            <tr bgcolor="#8DC0E3">
                <td width="20"></td>
                <td width="368" bgcolor="#8DC0E3" class="linksnegros">Red de Calidad Ambiental
                    Marina REDCAM</td>

                <td width="362" bgcolor="#8DC0E3" class="linksnegros">
                    <table width="300" border="0" align="right" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="17"><a href="index.htm"><img src="../../images/orange_home.gif" width="17" height="17" border="0"></a></td>
                            <td width="4">&nbsp;</td>
                            <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="41"><a href="index.htm" class="linksnegros"><img src="../../images/spacer.gif" width="4" height="4" border="0"/>Inicio
                                </a></td>
                            <td width="20"><a href="legal.htm"><img src="../../images/orange_legal.gif" width="17" height="17" border="0"/></a></td>
                            <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="44"><a href="legal.htm" class="linksnegros"><img src="../../images/spacer.gif" width="4" height="4" border="0"/>Legal</a></td>
                            <td width="17"><a href="contactenos.htm"><img src="../../images/orange_contact.gif" width="17" height="17" border="0"></a></td>
                            <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="81"><div align="left"><a href="contactenos.htm" class="linksnegros"><img src="../../images/spacer.gif" width="4" height="4" border="0">Contactenos</a></div>
                            </td>
                            <td width="19"><a href="contactenos.htm"><img src="../images/help.gif" width="17" height="17" border="0"></a></td>
                            <td bgcolor="#8DC0E3" onMouseOver="this.bgColor = '#B5D7DE'" onMouseOut="this.bgColor = '#8DC0E3'" width="57"><div align="left"><a href="ayuda.htm" class="linksnegros"><img src="../../images/spacer.gif" width="4" height="4" border="0">Ayuda</a></div>
                            </td>
                        </tr>
                    </table></td>
            </tr>
        </table>




        <div class="centrado">
            <table width="963px;" border="0" >
                <tr> 
                    <td colspan="3"> <div align="left"><font face="Arial, Helvetica, sans-serif" size="3"><span class="justificados"><font face="Arial, Helvetica, sans-serif" size="2">Consultas 
                                        al </font><font face="Arial, Helvetica, sans-serif" size="3"><b><font color="#000000" size="2">&quot;Sistema 
                                                de Informaci&oacute;n Ambiental Marino (SIAM)&quot; - RedCAM</font></b><font color="#000000" size="2">, 
                                            Para realizar su consulta haga click sobre el vinculo de la misma</font></font></span></font></div></td>
                </tr>
                <tr> 
                    <td colspan="3">&nbsp;</td>
                </tr>
                <form method="post" action="">
                    <tr bgcolor="#6699CC"> 
                        <td colspan="2"><span style="font-family:Arial, Helvetica, sans-serif;color:white;font-weight:bold;text-align:justify;font-size:0.8em">Opciones</span></td>
                        <td width="541"><font face="Arial, Helvetica, sans-serif" size="3"><span class="justificados"><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><b>Descripci&oacute;n</b></font></span></font></td>
                    </tr>
                    <!--
                        <tr> 
                          <td width="32"><font face="Arial, Helvetica, sans-serif" size="3"><span class="justificados"><img src="images/add-32.png" width="32" height="32" valign="middle"></span></font></td>
                          <td width="143" valign="middle"><font face="Arial, Helvetica, sans-serif" size="3"><span class="justificados"><font face="Arial, Helvetica, sans-serif" size="2"><a href="ingreso_datos_redcam.jsp">Ingresar
                          Datos</a></font></span></font></td>
                          <td><font face="Arial, Helvetica, sans-serif" size="3"><font size="2">M&oacute;dulo para subir datos al sistema.</font></font></td>
                        </tr>
                        <tr>
                          <td width="32"><font face="Arial, Helvetica, sans-serif" size="3"><span class="justificados"><img src="images/db_update-32.png" width="32" height="32" valign="middle"></span></font></td>
                          <td valign="middle"><font face="Arial, Helvetica, sans-serif" size="3"><span class="justificados"><font face="Arial, Helvetica, sans-serif" size="2"><a href="buscar_muestrascam.jsp">Actualizar
                          Datos</a></font></span></font></td>
                          <td><font face="Arial, Helvetica, sans-serif" size="3"><font size="2">M&oacute;dulo para actualizar los datos subir al sistema por el usuario registrado</font></font></td>
                        </tr>
                    -->
                    <tr> 
                        <td width="32"><a href="microdatos.jsp"><img src="../images/search_32.png" width="32" height="32" style="text-align:center;border:0px;" /></a></td>
                        <td style="text-align:left">
                            Consultar microdatos
                            <select id="department" name="department" onchange="onChangeDepartment()"> 
                                <option value="Seleccione">Seleccione...</option>
                                <option value="ANTIOQUIA">ANTIOQUIA</option>
                                <option value="Archipi�lago de San Andr�s, Providencia y Santa Ca">ARCHIPIELAGO SAN ANDRES Y PROVIDENDCIA Y SANTA CATALINA</option>
                                <option value="ATLANTICO">ATLANTICO</option>
                                <option value="BOLIVAR">BOLIVAR</option>
                                <option value="CAUCA">CAUCA</option>
                                <option value="CHOCO">CHOCO</option>
                                <option value="CORDOBA">CORDOBA</option>
                                <option value="LA GUAJIRA">LA GUAJIRA</option>
                                <option value="MAGDALENA">MAGDALENA</option> 
                                <option value="NARI�O">NARI�O</option>
                                <option value="SUCRE">SUCRE</option>
                                <option value="VALLE DEL CAUCA">VALLE DEL CAUCA</option>
                            </select>
                            <select id="yearcmb" name="yearcmb" > 

                            </select>    
                            <button id="consultbtn" name="consultbtn" type="button" onclick="consultMicroData()">Consultar >></button>
                        </td>
                        <td><span style="text-align:justify;font-family:Arial, Helvetica, sans-serif;font-size:75%">Consulta 
                                de los microdatos de las variables muestreadas en los monitoreos de la red de Vigilancia 
                                de acuerdo al tipo en determinadas estaciones y &eacute;pocas.</span></td>
                    </tr>
                    <tr> 
                        <td width="32"><a href="/siam/faces/redcam/cargadordatos/cargadordatos.xhtml" ><img src="../images/loader.png" width="32" height="32" style="text-align:center;border:0px;" /></a></td>
                        <td style="text-align:left"><a style="font-family:Arial, Helvetica, sans-serif;font-size:75%" href="/siam/faces/redcam/cargadordatos/cargadordatos.xhtml" target="_blank">M&oacute;dulo de carga de datos
                            </a></font></span></font></td>
                        <td><span style="text-align:justify;font-family:Arial, Helvetica, sans-serif;font-size:75%">Este m&oacute;dulo permite actualizar la base de datos de monitoreo de calida ambiental de aguas.</span></td>
                    </tr>
                    <tr> 
                        <td width="32"><a href="estadisticasPorEstaciones.jsp"><img src="../images/estadisticasestaciones.png"  style="text-align:center;border:0px;"></a></td>
                        <td style="text-align:left"><a  style="font-family:Arial, Helvetica, sans-serif;font-size:75%" href="estadisticasPorEstaciones.jsp">Estad&iacute;ticas por estaciones
                            </a></td>
                        <td><span  style="text-align:justify;font-family:Arial, Helvetica, sans-serif;font-size:75%">Consulte el comportamiento de las variables muestradas durante un peri&oacute;do de tiempo de la red de vigilancia en determinadas estaciones.
                            </span></td>
                    </tr>
                    <tr> 
                        <td width="32"><a href="menuOpcionesPrivadas.jsp?salir=ok"><img src="../images/botonsalir.png"  style="text-align:center;border:0px;"></a></td>
                        <td style="text-align:left"><a  style="font-family:Arial, Helvetica, sans-serif;font-size:75%" href="menuOpcionesPrivadas.jsp?salir=ok">Salir de forma segura.
                            </a></td>
                        <td><span  style="text-align:justify;font-family:Arial, Helvetica, sans-serif;font-size:75%">Utilice esta opci&oacute;n para salir de forma segura de su aplicaci&oacute;n, si no esta en un PC de confianza.
                            </span></td>
                    </tr>
                </form>
            </table>    
            <table width="963px;" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td bgcolor="#8DC0E3"><img src="../../images/spacer.gif" width="2" height="2"></td>
                </tr>
            </table>
        </div>
        <%@ include file="../../plantillaSitio/footermodulesV3.jsp" %>
        <div align="center" style="display:none;"><a href="../index.htm" class="textfooter">Sistema de Informaci&oacute;n
                Ambiental Marina</a><span class="textfooter">-</span> <a href="../legal.htm" class="textfooter">Condiciones
                de acceso y uso</a> <span class="textfooter">-</span> <span class="textfooter">Instituciones
                participantes:</span><a href="http://www.invemar.org.co" class="textfooter">INVEMAR</a></div>


        <script type="text/javascript">
            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
            try {
                var pageTracker = _gat._getTracker("UA-2300620-4");
                pageTracker._trackPageview();
            } catch (err) {
            }
        </script>
 <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
        <script>

            $("#yearcmb").hide();
            function consultMicroData() {

                var department = $("#department option:selected").val();
                var year = $("#yearcmb option:selected").val();
                window.open('microdatos.jsp?department=' + department + "&year=" + year);

            }

            function onChangeDepartment() {
                $("#yearcmb").show();
                var department_ = $("#department option:selected").val();
                var url = "/siam/ServicioComponentes?componente=redcam&action=YearByDepartment";
                $("#yearcmb").empty();
                $.post(url, {department: department_}, function (data) {
                    $.each(data, function (index, item) {
                        $("#yearcmb").append("<option value='" + item.year + "'>" + item.year + "</option>");
                    });

                }, "json");
            }
        </script>

    </body>
</html>