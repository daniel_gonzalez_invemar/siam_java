<%@page import="co.org.invemar.siam.ConnectionFactory"%>
<%@ page import="co.org.invemar.siam.redcam.coneccam.*" %>
<%@ page import="co.org.invemar.siam.redcam.estadisticas.cvariablegeog.*" %>
<%@ page import="co.org.invemar.siam.redcam.utilidades.*" %>
<%@ page import="co.org.invemar.siam.redcam.*" %>
<%@page import="co.org.invemar.siam.redcam.ServiceSectoresRedCam"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.net.URLEncoder" %>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head> 
        <title>Sistema de Informaci&oacute;n Ambiental Marina de Colombia - SIAM.</title> 
        <meta name="keywords" content="Sistema de Informacin Ambiental Marina de Colombia, biologa, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, REDCAM, INVEMAR, mapas" /> 
        <link href="../../plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css" />
        <meta http-equiv="pragma" content="no-cache" /> 
        <meta http-equiv="cache-control" content="no-cache" />         
        <link type="text/css" rel="stylesheet" href="css/divflotante.css"/>              
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <script type="text/javascript" src="js/jquery.min.js"></script>   
        <script type="text/javascript" src="js/divflotante.js"></script>

        <link type="text/css" href="../../libreriascomunes/css/redmond/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="../../libreriascomunes/js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="../../libreriascomunes/js/jquery-ui-1.8.23.custom.min.js"></script>
        <script type="text/javascript" src="js/estadisticas.js"></script>
        <script type="text/javascript" src="../js/monitoreo.js"></script>
        <% HttpSession session1 = request.getSession(true);

            //  String nuser = (String)session.getAttribute("usu_id1");
            String nuser = "ljarias";
            if (nuser == null) {
        %>

        <script language="JavaScript" type="text/javascript">
            alert("No tiene permisos para ingresar a esta p&aacute;gina");
            window.parent.location = "login.htm";
        </script>

        <%  }%>



        <%

            ServiceVariableRedCam servicevariables = new ServiceVariableRedCam();
            String opcion = "2";
            String nproy = "REDCAM";
            String nomproy = request.getParameter("nomproy");

            String nconsulta = request.getParameter("nconsulta");

            String ntipo = request.getParameter("ntipo");
            String ndepar = request.getParameter("ndepar");
            String nsector = request.getParameter("nsector");
            String ntemporada = request.getParameter("ntemporada");
            String nprofundidad = request.getParameter("nprofundidad");
            String nsustrato = request.getParameter("nsustrato");
            Calendar fecha1 = null;
            Calendar fecha2 = null;
            int xx = 0;
            String ano1 = "", mes1 = "", dia1 = "", ano2 = "", mes2 = "", dia2 = "";
            if (request.getParameter("ano1") != null && !request.getParameter("ano1").equals("")) {
                ano1 = request.getParameter("ano1");
            }
            if (request.getParameter("ano2") != null && !request.getParameter("ano2").equals("")) {
                ano2 = request.getParameter("ano2");
            }

            float ind = 0;
            int tam = 0;
            int paginas = 1;

            //pagina actual del resultado 1= valor inicial
            int pag = 1;

            //numero de registros por pagina
            float registros = 10;
            String pagina = request.getParameter("pagina");

            int cont = 0;
            int h = 0, Z = 0;
            Cutilidades util = new Cutilidades();

            String cadena = request.getParameter("cadena");
            String cad = "";
            Vector cade = new Vector();

            if (cadena != null && !cadena.equals("")) {
                cad = cadena;
                String cadena1 = "";
                for (h = 0; h < cad.length(); h++) {
                    if (cad.charAt(h) == ',') {
                        cade.addElement(cadena1);
                        cadena1 = "";
                    } else {
                        cadena1 = cadena1 + cad.charAt(h);
                    }
                }
            }

            /*
             * recuperacion de la cadena de estaciones
             */
            String cadest = request.getParameter("cadest");
            String scad = "";
            Vector scade = new Vector();
            int z = 0;
            if (cadest != null && !cadest.equals("")) {
                scad = cadest;
                String cadest1 = "";
                for (z = 0; z < scad.length(); z++) {
                    if (scad.charAt(z) == ',') {
                        scade.addElement(cadest1);
                        cadest1 = "";
                    } else {
                        cadest1 = cadest1 + scad.charAt(z);
                    }
                }
            }


            /* if (con2 != null) {
             con2.close();
             }*/
            String sele = "";
            String vari = "";
        %>       

    </head> 
    <body>
        <div id="dialog-modal" style="font-size: 0.8em;" title="Ayuda para generar las estad&iacute;ticas REDCAM">    
            <%
                ServicioNivelesEstadisticos servicionivelestadistica = new ServicioNivelesEstadisticos();
                ArrayList listaniveles = servicionivelestadistica.getTododNiveles("REDCAM");
                Iterator itniveles = listaniveles.iterator();
                while (itniveles.hasNext()) {
                    NivelesEstadisticos ne = (NivelesEstadisticos) itniveles.next();
                    out.println("<div id='nivel" + ne.getCod() + "'><span style='color:red'>Nivel:" + ne.getCod() + "</span> <span style='color:blue'>" + ne.getDescripcion()+ " " + ":</span><br/><span style='font-weight:bold'>Par&aacute;metros a seleccionar:</span>" + ne.getAtributos_minimos() + " </div><br/>");

                }
            %>
        </div>    
        <jsp:include page="../../plantillaSitio/headermodulosv3.jsp?idsitio=60"/>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr bgcolor="#8DC0E3">
                <td width="22"></td>
                <td width="538" bgcolor="#8DC0E3" class="linksnegros">Red de Calidad Ambiental
                    Marina REDCAM :: Consultas Estad&iacute;sticas<a onclick="visualizarCuadroModal()"> Consultar niveles estad&iacute;ticos</a></td>

                <td width="190" bgcolor="#8DC0E3" class="linksnegros">&nbsp;</td>
            </tr>
        </table>


        <form action="estadisticas.jsp" method="" name="formacam" id="formacam"  >
            <input type="hidden" name="cadena" value="" id="cadena"/>
            <input type="hidden" name="exportar" value="" id="exportar"/>
            <input type="hidden" name="cadest" value="" id="cadest" />
            <input type="hidden" name="nivelestadistico" value="" id="nivelestadistico" />
            <div align="left">
                <table width="100%" border="0" cellpadding="3" cellspacing="0" >
                    <tr>
                        <td width="100%" height="182">          <div align="right">

                                <table width="963px" border="0" align="center">
                                    <tr>
                                        <td height="8" colspan="4"  bgcolor="#CCCCCC">
                                            <b><font color="#99CCFF" face="Verdana, Arial, Helvetica, sans-serif" size="-1">
                                                    <font color="#FF0000">::</font>
                                                    <font color="#333333">Par&aacute;metros  de Consulta</font>
                                                    <font color="#FF0000">::</font></font>
                                            </b>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: #999999;color:white">Nivel</td>
                                        <td>
                                            <select name="nivel" id="nivel" onchange="mostrarNivel(this.value)">
                                                <option value="">--</option>
                                                <%
                                                    itniveles = listaniveles.iterator();
                                                    while (itniveles.hasNext()) {
                                                        NivelesEstadisticos ne = (NivelesEstadisticos) itniveles.next();
                                                        out.println("<option value='" + ne.getCod() + "' >Nivel:" + ne.getCod() + " - " + ne.getDescripcion() + "</option>");

                                                    }

                                                %>


                                            </select> 
                                        </td>  
                                    </tr>
                                    <tr> 
                                        <td height="2" bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Tipo 
                                                de variable</font></td>
                                        <td   style="background-color:#F9F9F9;text-align: left"> 
                                            <select name="ntipo" id="ntipo" onchange="abrir1();">
                                                <option value="">--</option>
                                                <%                                                    ArrayList tiposVariableslist = servicevariables.getTiposVariablesCAM();
                                                    // System.out.println(tiposVariableslist.size());
                                                    Iterator it = tiposVariableslist.iterator();
                                                    while (it.hasNext()) {
                                                        TipoVariable tipovariable = (TipoVariable) it.next();
                                                        out.println("<option value='" + tipovariable.getTipo() + "'>" + tipovariable.getNombre() + "</option>");
                                                    }%>


                                            </select>
                                            <br/> <span style="color:red;font-size:11px" id="msgvariables">( Seleccione el tipo de variable)</span>
                                        </td>                                                                
                                    </tr>
                                    <tr> 
                                        <td height="2"  bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Tipo de sustrato muestra</font></td>
                                        <td height="2" colspan="3"   style="background-color:#F9F9F9;text-align: left;font-family:Verdana, Arial, Helvetica, sans-serif;font-size: 0.8em">
                                            <div id="divtiposustratomuestra">
                                                <select id="sampletypesubstratum" style="display:none">
                                                    <option value="-1" >Seleccione ...</option>                                                                        
                                                </select>
                                            </div>
                                        </td>
                                    </tr>           
                                    <tr  style="">
                                        <td height="2"  bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Regi&oacute;n </font></td>
                                        <td height="2" colspan="3"   style="text-align:left;background-color:#F9F9F9">

                                            <select name="nregion" id="nregion" onclick="DepartamentosPorRegion()" >
                                                <option value="-" selected="selected" style="">--
                                                    <option value="CARIBE">Caribe
                                                        <option value="PACIFICO">Pac&iacute;fico                  
                                                            </select>
                                                            <span style="color:red;font-size:11px" id="msgregion">(Seleccione la regi&oacute;n)</span>
                                                            </td>
                                                            </tr>
                                                            <tr> 
                                                                <td width="13%" height="24" bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Departamento</font></td>
                                                                <td width="29%"  bgcolor="#F9F9F9"><div id="divndepar"> <span style="color:blue;font-size:11px" id="msgdepartamento">Para seleccionar el departamento debe seleccionar primero la regi&oacute;n.</span></div></td>                                                             

                                                            </tr>
                                                            <tr>
                                                                <td width="9%"  bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Sector</font></td>
                                                                <td width="49%" height="24"  style="background-color:#F9F9F9;text-align: left"> 

                                                                    <input type="text" name="nsector1" id="nsector1" value="" size="35" disabled />
                                                                    <input type="button" name="selestaciones" id="selestaciones" value="Seleccionar estaciones" onclick="seleccionarEstaciones();" />
                                                                    <br/>
                                                                    <span style="color:red;font-size:11px" id="msgestaciones"> Nota: Para seleccionar las estaciones, debe primer haber seleccionado el Departamento y el sector</span>
                                                                </td>
                                                                <input type="hidden" name="nsector" value="" id="nsector" /> 

                                                            </tr>
                                                            <tr>
                                                                <td width="9%"  bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Zonas:</font></td>
                                                                <td width="49%" height="24"  style="background-color:#F9F9F9;text-align: left"> 
                                                                    <select name="szonas" id="szonas">    
                                                                        <option value="sel" >-</option>
                                                                    </select>
                                                                    <span style="color:red;font-size:11px" id="msgzona"> Seleccione la zona, luego de haber seleccionado el Departamento.</span>
                                                                </td>                                                           

                                                            </tr>  
                                                            <tr> 
                                                                <td height="2"  bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">Rango
                                                                        de a&ntilde;os</font></td>
                                                                <td height="2" colspan="3"   style="background-color:#F9F9F9;text-align: left;font-family:Verdana, Arial, Helvetica, sans-serif;font-size: 0.8em">
                                                                    <div id="anosbusqueda" > 


                                                                    </div>
                                                                </td>
                                                            </tr>              

                                                            <tr>
                                                                <td  bgcolor="#999999"><font color="#FFFFFF" size="-2" face="Verdana, Arial, Helvetica, sans-serif">&Eacute;poca</font></td>
                                                                <td height="2"  style="text-align:left;background-color:#F9F9F9 ">
                                                                    <div id="paneltemporada">
                                                                        <select name="ntemporada" id="ntemporada">
                                                                            <option value="">-</option>
                                                                            <option value="2">LLuviosa</option>
                                                                            <option value="1">Seca</option>
                                                                        </select>
                                                                        <span style="color:red;font-size:11px" id="msgtemporada">Seleccione la temporada</span>
                                                                    </div></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <span style="color:red;display:none"> * Campos obligatorios.</span>
                                                                </td>
                                                            </tr>
                                                            <tr> 
                                                                <td height="28" colspan="4" style="background-color:#CCCCCC" > 
                                                                    <div  style="text-align:left;font-size: 0.7em" >
                                                                        <input name="nproy" id="nproy" type="hidden" value="REDCAM" />
                                                                        <input name="opcion" id="opcion" type="hidden" value="2" />
                                                                        <input name="nuser"  id="nuser" type="hidden" value="ljarias" />
                                                                        <input type="reset"  name="Submit2" value="Limpiar" onclick="LimpiarVariables()" />
                                                                        <input type="button" name="Submit" value="Buscar &gt;&gt;" onclick="generarEstadistica('false')" />

                                                                    </div></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4"><a href="#" onclick="revealModal('modalPage')" style="display:none" >Consulte los niveles de b&uacute;squeda estad&iacute;tico</a>

                                                                </td>
                                                            </tr>
                                                            </table>
                                                            </div>
                                                            </td>
                                                            </tr>                                                            
                                                            </table>
                                                            </div>
                                                            <input type="hidden" id="tam" name="tam" value="<%=tam%>" />
                                                            </form>
                                                            </tr>
                                                            </td>
                                                            <div id="loading" style="widht:100px;margin:auto;text-align:center">

                                                            </div>
                                                            <div id="resultado"></div>
                                                            <%@ include file="../../plantillaSitio/footermodulesV3.jsp" %>

                                                            <div id="sectorssdialog" style="font-size: 1.0em;" title="Sectores">    
                                                                <form name="forma" method="post" action="">
                                                                    <table width="513" border="0">
                                                                        <tr bgcolor="#333333"> 
                                                                            <td colspan="2"><b><font color="#99CCFF" face="Verdana, Arial, Helvetica, sans-serif" size="-1"><font color="#FF0000">::</font> 
                                                                                        <font color="#FFFFFF">Sectores </font><font color="#FF0000">::</font></font></b></td>
                                                                        </tr>
                                                                        <tr bgcolor="#CCEEFF"> 
                                                                            <td colspan="2"><font face="Verdana, Arial, Helvetica, sans-serif" size="-1">Seleccione
                                                                                    un sector de la lista</font></td>
                                                                        </tr>
                                                                        <tr> 
                                                                            <td colspan="2">
                                                                                <select name="sectores" id="sectores"  onclick="sectorSelect();">
                                                                                    <option value="">Seleccione un valor</option>                                                                                 
                                                                                </select>
                                                                            </td>
                                                                        </tr>
                                                                        <tr bgcolor="#CCEEFF"> 
                                                                            <td colspan="2"><font face="Verdana, Arial, Helvetica, sans-serif" size="-1"><input type="button" value="Seleccionar" name="seleccionar" onclick="hideModal('sectorssdialog')" />
                                                                                </font></td>
                                                                        </tr>
                                                                    </table>
                                                                </form>
                                                            </div>

                                                            <div id="variablesdialog" style="font-size: 1.0em;" title="Variables">    
                                                                <div  id="activeVariables" style="display:none">
                                                                    <input type="checkbox" id="active" name="active" value="true"  /><span >Variables activas |</span>
                                                                    <input type="checkbox" id="desactive" name="desactive"  value="false" /> <span>Variables  No activas</span>
                                                                    <input type="button" id="consultar" name="consulta" onclick="RenderizarVariables()" value="Filtrar variables..." />
                                                                </div>
                                                                <form name="formavariables" method="post" action="">
                                                                    <table width="496" border="0">
                                                                        <tr bgcolor="#333333"> 
                                                                            <td colspan="2"><b><font color="#99CCFF" face="Verdana, Arial, Helvetica, sans-serif" size="-1"><font color="#FF0000">::</font> 
                                                                                        <font color="#FFFFFF">Variables </font><font color="#FF0000">::</font></font></b></td>
                                                                        </tr>
                                                                        <tr bgcolor="#CCEEFF"> 
                                                                            <td colspan="2"><font face="Verdana, Arial, Helvetica, sans-serif" size="-1">Seleccione 
                                                                                    las variables de la lista, para seleccionar mas de una presione <strong>Crtl</strong> 
                                                                                    + <strong>Click</strong> y al final el botón <strong><em>Enviar</em></strong></font></td>
                                                                        </tr>
                                                                        <tr> 
                                                                            <td colspan="2">
                                                                                <div id="resultsetVariables">
                                                                                    <select id="variable" name="variable" size="10" multiple>
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr> 
                                                                            <td width="271"><input type="button" name="Button" value="Seleccionar" onclick="seleccionarVariables();"></td>
                                                                            <td width="215">&nbsp;</td>
                                                                        </tr>
                                                                        <tr> 
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                    </table>
                                                                </form>
                                                            </div>


                                                            <div id="estacionesdialog" style="font-size: 1.0em;" title="Variables">    
                                                                <div  id="activeVariables" style="">
                                                                    <input type="checkbox" id="active" name="active" value="true"  /><span >Estaciones activas |</span>
                                                                    <input type="checkbox" id="desactive" name="desactive"  value="false" /> <span>Estaciones  no activas</span>
                                                                    <input type="button" id="consultar" name="consulta" onclick="RenderizarEstaciones()" value="Filtrar estaciones por vigencia..." />
                                                                </div>

                                                                <%//=cest.geterror()%>
                                                                <form name="formaestaciones" method="post" action="">
                                                                    <table width="496" border="0">
                                                                        <tr bgcolor="#333333"> 
                                                                            <td colspan="2"><b><font color="#99CCFF" face="Verdana, Arial, Helvetica, sans-serif" size="-1"><font color="#FF0000">::</font> 
                                                                                        <font color="#FFFFFF">Estaciones</font><font color="#FF0000">::</font></font></b></td>
                                                                        </tr>
                                                                        <tr bgcolor="#CCEEFF"> 
                                                                            <td colspan="2"><font face="Verdana, Arial, Helvetica, sans-serif" size="-1">Seleccione 
                                                                                    las estaciones de la lista, para seleccionar mas de una presione <strong>Crtl</strong> 
                                                                                    + <strong>Click</strong> y al final el botón <strong><em>Enviar</em></strong></font></td>
                                                                        </tr>
                                                                        <tr> 
                                                                            <td colspan="2">
                                                                                <div id="resultsetEstaciones">
                                                                                    <select name="estacion" size="10" multiple id="estacion">                                                                                       
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="271"><input type="button" name="Button" value="Enviar" onClick="seleccionarest();"></td>
                                                                            <td width="215">&nbsp;</td>
                                                                        </tr>
                                                                        <tr> 
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                    </table>
                                                                </form>
                                                            </div>

                                                            </body>



                                                            </html>