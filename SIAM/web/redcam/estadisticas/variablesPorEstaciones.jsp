<%@page import="co.org.invemar.siam.redcam.Variable"%>
<%@page import="co.org.invemar.siam.redcam.ServiceVariableRedCam"%>
<%@ page import="coneccam.*" %>
<%@ page import="cvariablegeog.*" %>
<%@ page import="utilidades.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>


<%
    StringBuffer SXML;
    SXML = new StringBuffer();
    String nuser = "ljarias";
    cvariablegeog.Cvariablegeog var = new cvariablegeog.Cvariablegeog();
    coneccam.Cconexioncam con2 = new coneccam.Cconexioncam();
    Connection conn2 = null;
    String opcion = "2";
    String proyecto = "REDCAM";
    String estacion = request.getParameter("estacion");
    String variable = request.getParameter("variable");
    String action = request.getParameter("action");

    if (action.equals("variable")) {
        //cvariablegeog.Ccontvariablegeog cvariable = new cvariablegeog.Ccontvariablegeog();
        
        ServiceVariableRedCam serviciovariable = new ServiceVariableRedCam();
        
        SXML = new StringBuffer();
        //conn2 = con2.getConn();
       // cvariable.contenedorvariable_estacion(conn2, nproy, nuser, opcion, estacion);
        ArrayList listavariables = serviciovariable.getVariablesMedidasPorEstacion(estacion,proyecto);
        Iterator it = listavariables.iterator();
        SXML.append("<variables>");
        while(it.hasNext()){          
            Variable v = (Variable)it.next();
            SXML.append("<variable>");
            SXML.append("<codigo>" + v.getCod() + "</codigo>");
            SXML.append("<nombre>" + v.getNombre()+"</nombre>");
            SXML.append("</variable>"); 
        }
         SXML.append("</variables>");
         
       
       
    } else if (action.equals("ano")) {
        cvariablegeog.Ccontvariablegeog cfecha = new cvariablegeog.Ccontvariablegeog();
        SXML = new StringBuffer();
        conn2 = con2.getConn();
        cfecha.contenedorano_estacion(conn2, proyecto, nuser, opcion, estacion, variable);
        SXML.append("<anos>");
        for (int k = 0; k < cfecha.gettamano(); k++) {
            var = new cvariablegeog.Cvariablegeog();
            var = cfecha.getvariablegeog(k);
            SXML.append("<ano>" + var.get_ano() + "</ano>");
        }
        SXML.append("</anos>");
    }

    if (con2 != null) {
        con2.close();
    }

    response.setContentType("text/text; charset=ISO-8859-1");
    response.setContentType("application/xml");
    response.setHeader("Cache-Control", "no-cache");
    response.getWriter().write(SXML.toString());

%>
