<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>..:: Sistema de Gesti�n de Indicadores Ambientales Marinos y Costeros ::..</title>

<link type="text/css" rel="stylesheet" href="plantillaSitio/css/styles.css"/>

    <!-- JAVASCRIPT FOR AND GOOGLE ANALYTICS -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19778914-18']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">

      // Load the Visualization API and the controls package.
      google.load('visualization', '1.1', {'packages':['controls']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawDashboard_icam);
//      google.setOnLoadCallback(drawDashboard_parametros);

 
    function drawDashboard_icam() {

//		var query = new google.visualization.Query('http://spreadsheets.google.com/spreadsheet/tq?authkey=CJGN3ogI&range=A1:G177&key=0AjFQQGlBZ-btdFpZa19zRUxkYjJrWTc5QUNuRHRQX2c&gid=0');
		
		var query = new google.visualization.Query('https://docs.google.com/spreadsheet/tq?range=A1%3AW2564&key=0AjFQQGlBZ-btdDdFeXo1RWNVc1NYanlwQm1BMnNBU3c&gid=16&headers=-1');
  
		// Send the query with a callback function.
		query.send(handleQueryResponse);
	  
	  
      // Callback that creates and populates a data table, 
      // instantiates a dashboard, a range slider and a pie chart,
      // passes in the data and draws it.

        // Create a dashboard.
        var dashboard_icam = new google.visualization.Dashboard(
          document.getElementById('dashboard_divicam'));

	
        // Create a range slider, passing some options
        var Slider = new google.visualization.ControlWrapper({
          'controlType': 'NumberRangeFilter',
          'containerId': 'filter_div1',
          'options': {
            'filterColumnLabel': 'year'
          }
        });

		// Define a category picker control for the Gender column
		  var yearPicker = new google.visualization.ControlWrapper({
			'controlType': 'CategoryFilter',
			'containerId': 'filter_div2',
			'options': {
			  'filterColumnLabel': 'MUESTREO',
			  'ui': {
			  'labelStacking': 'vertical',
				'allowTyping': false,
				'allowMultiple': false
			  }
			}
//			,'state': {'selectedValues': [2010-2]}
		  });		

		  // Define a category picker control for the DEPARTAMENTO column
		  var deptoPicker = new google.visualization.ControlWrapper({
			'controlType': 'CategoryFilter',
			'containerId': 'filter_div3',
			'options': {
			  	'filterColumnLabel': 'DEPARTAMENTO',
			  	'ui': {
			  	'labelStacking': 'vertical',
				'allowTyping': false,
				'allowMultiple': false
			  }
			}
			,'state': {'selectedValues': ['VALLE DEL CAUCA']}
		  });		

		  // Define a category picker control for the SECTOR column
		  var sectorPicker = new google.visualization.ControlWrapper({
			'controlType': 'CategoryFilter',
			'containerId': 'filter_div4',
			'options': {
			  'filterColumnLabel': 'SECTOR',
			  'ui': {
			  'labelStacking': 'vertical',
				'allowTyping': false,
				'allowMultiple': false
			  }
			},
			'state': {'selectedValues': ['Bah�a Malaga hasta Bah�a de Buenaventura']}
		  });		
	  

		  // Define a category picker control for the ESTACION column
		  var estacionPicker = new google.visualization.ControlWrapper({
			'controlType': 'CategoryFilter',
			'containerId': 'filter_div5',
			'options': {
			  'filterColumnLabel': 'ESTACION',
			  'ui': {
			  'labelStacking': 'vertical',
				'allowTyping': false,
				'allowMultiple': false
			  }
			},
			'state': {'selectedValues': ['F. Anchicay�']}
		  });		

        // Create a pie chart, passing some options
		var pieChart = new google.visualization.ChartWrapper({
			'chartType': 'PieChart',
			'containerId': 'chart1',
			'options': {
			  'width': 250,
			  'height': 250,
			  'legend': 'none',
//			  'title': 'ICAM',
//			  'chartArea': {'left': 15, 'top': 15, 'right': 0, 'bottom': 0},
			  'pieSliceText': 'label'
			},
			// Instruct the piechart to use colums 6 and 4
			// from the 'data' DataTable.
			'view': {'columns': [3,16]}
		  });		



        


        // Create a pie chart, passing some options para la data de poblaci�n
		var areaChart = new google.visualization.ChartWrapper({
			'chartType': 'LineChart',
			'containerId': 'chart2',
			'options': {
			  'width': 600,
			  'height': 400,
			  'legend': 'none',
			  'hAxis': {'title': 'Muestreo',
			  			'slantedText': true,
			  			'slantedTextAngle': 30,
			  			'maxAlternation':1
			  },
			  'vAxis': {'title': 'ICAM',
			  			'minValue':1,
			  			'maxValue':100
			  },
			  
			  'fontSize': 11,
			  'chartArea':{	'left':	60,
			  				'top': 	10,
							'width': 	'90%',
							'height':	'80%'
							}
			  

//			  'cht': 'bvs', 
//			  'chco': colors, 
//			  max: 100
//			  'title': 'ICAM',
//			  'chartArea': {'left': 15, 'top': 15, 'right': 0, 'bottom': 0},
//			  'pieSliceText': 'label'
			},
			// Instruct the piechart to use colums 6 and 4
			// from the 'data' DataTable.
			'view': {'columns': [3,16]}
		  });		


		// Define a table
		  var table = new google.visualization.ChartWrapper({
			'chartType': 'Table',
			'containerId': 'table1',
			'options': {
//			  'width': '300px'
			},
			'view': {'columns': [0, 1, 3, 7, 16]}
		  });		
		  
		
    // Register a listener to be notified once the dashboard is ready.
//        google.visualization.events.addListener(dashboard_icam, 'ready', dashboardReady);
		
		// Establish dependencies, declaring that 'filter' drives 'pieChart',
        // so that the pie chart will only display entries that are let through
        // given the chosen slider range.
//        dashboard_icam.bind(yearPicker, deptoPicker);
        dashboard_icam.bind(deptoPicker, sectorPicker);
        dashboard_icam.bind(sectorPicker, estacionPicker);
        dashboard_icam.bind([deptoPicker, sectorPicker, estacionPicker], [areaChart, table]);
//        dashboard.bind(deptoPicker, areaChart);


		function dashboardReady() {
        // The dashboard is ready to accept interaction. Configure the buttons to
        // programmatically affect the dashboard when clicked.
        
        // Change the pie chart rendering options when clicked.
        document.getElementById('colores').onclick = function() {
		
			var blue = '0000ff';
          	var green = '00ff00';
          	var yellow = 'ffff00';
          	var orange = 'ff9900';
          	var red = 'ff0000';
        	// Loop over the data table to create the color specification. 
        	var colors = [];

			var data = response.getDataTable();
        	for (var i = 0; i < data.getNumberOfRows(); i++) {
          		var value = data.getValue(i, 1);
          		var color = value < 25 ? red : (value < 50 ? orange : (value < 70 ? yellow : ( value < 90 ? green : blue)));
          		colors.push(color);
        	}
        	colors = colors.join('|');
		
//          areaChart.setOption('chco', colors);
//          areaChart.draw();
		  
        }; 
      }
		
		function handleQueryResponse(response) {
			if (response.isError()) {
				alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
				return;
			}		
			// Draw the dashboard.
			var data = response.getDataTable();

			var formatter = new google.visualization.TableColorFormat();
			  formatter.addRange(0, 25, 'black', 'red');
			  formatter.addRange(25, 50, 'black', 'orange');
			  formatter.addRange(50, 70, 'black', 'yellow');
			  formatter.addRange(70, 90, 'white', 'green');
			  formatter.addRange(90, 100, 'white', 'blue');
			  formatter.format(data, 16);
			  
/*
			var blue = '0000ff';	
          	var green = '00ff00';
          	var yellow = 'ffff00';
          	var orange = 'ff9900';
          	var red = 'ff0000';
        	// Loop over the data table to create the color specification. 
        	var colors = [];
        	for (var i = 0; i < data.getNumberOfRows(); i++) {
          		var value = data.getValue(i, 1);
          		var color = value < 25 ? red : (value < 50 ? orange : (value < 70 ? yellow : ( value < 90 ? green : blue)));
          		colors.push(color);
        	}
        	colors = colors.join('|');
          	areaChart.setOption('chco', colors);
//          areaChart.draw(data);
*/		  
          	table.setOption('allowHtml', true);
          	table.setOption('showRowNumber', true);
			dashboard_icam.draw(data);



		}
	}
	

    </script>
<link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />

</head>



<body>


<div class="principal">
  <table style="width:100%;">
    <!-- CABEZOTE -->
    <%@ include file="header_spincam2.jsp"%>
    <!-- FIN CABEZOTE -->
    <!-- SOMBRA CABEZOTE -->
    <tr>
      <td><table style="width:100%;">
          <tr>
            <td width="5" class="sombra_izquierda_contenido"></td>
            <td><div id="sombra1"></div></td>
            <td width="5" class="sombra_derecha_contenido"></td>
          </tr>
      </table></td>
    </tr>
    <!-- FIN SOMBRA CABEZOTE -->
    <tr>
      <td><table width="100%" align="center">
          <tr>
            <td width="5" valign="top"></td>
            <!-- MENU -->
            <td></td>
            <%/*@ include file="leftmenu_spincam2.jsp" */%>
            <!-- FIN MENU -->
            <td style="vertical-align:top;" class="colorfondo"><table style="width:100%;">
                <!-- BARRA DE MIGAS -->
                <tr>
                  <td style="vertical-align:top; padding-right:4px;"><table style="width:100%;">
                      <tr>
                        <td width="5" ></td>
                        <td><div class="migas"><img src="images/icono-folder.gif" alt="en" width="20" height="20" />Estas en: <a href="index.jsp">Inicio </a> / <a href="javascript:void(0)">Datos</a></div></td>
                      </tr>
                  </table></td>
                </tr>
                <!-- FIN BARRA DE MIGAS  -->
                <tr>
                  <td  style="padding-right:4px;"><table style="width:100%;">
                      <tr>
                        <td><table style="width:100%;" >
                            <tr>
                              <td class="titulo_secciones"><h1>Indicador de Calidad de Agua Marina - ICAM</h1></td>
                            </tr>
                            <tr>
                              <td class="titulo_sec_smb"></td>
                            </tr>
                            <tr>
                              <td style="height:10px;" ><table width="100%" border="0">
                                  <tr>
                                    <td width="75%" valign="top" ><div id="dashboard_divicam">
                                        <table width="100%" border="1" class="colorfondo2">
<tr>
                                            <td colspan="2" class="calendario_dia_sel">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr>
                                                <td><div class="izquierda" ><a href="ier_icam.jsp">Tendencia en una estacion</a></div></td>
                                                <td><div class="centrado" ><span class="izquierda"><a href="ier_icam_parametros.jsp">Par&aacute;metros por estacion/muestreo</a></span></div></td>
                                                <td><div class="derecha" ><a href="ier_icam_r.jsp">An&aacute;lisis del indicador</a></div></td>
                                              </tr>
                                            </table>
                                            </td>
                                          </tr
                                          ><tr>                                        
                                          <tr>
                                            <td colspan="2" class="calendario_dia_sel"><div class="tabla_ind_fondo">An&aacute;lisis del comportamiento del indicador en una estaci&oacute;n</div></td>
                                          </tr>
                                          <tr>
                                            <td class="textonormal3">Filtros:</td>
                                            <td width="93%" class="calendario"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr>
                                                <td><div id="filter_div3"></div></td>
                                                <td>&nbsp;</td>
                                                <td><div id="filter_div4"></div></td>
                                              </tr>
                                              <tr>
                                                <td><div id="filter_div5"></div></td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                              </tr>
                                            </table>                                                </td>
                                          </tr>
                                          <tr>
                                            <td width="7%" class="textonormal3">Graficos:</td>
                                            <td><!--                                            <div style="float: left;" id="chart1"></div>-->
                                                <div style="float: left;" id="chart2"></div></td>
                                          </tr>
                                          <tr>
                                            <td class="textonormal3">Datos:</td>
                                            <td align="center" class="textonormal2"><div style="float: center;" id="table1"></div></td>
                                          </tr>
                                        </table>
                                    </div>
                                        <!--                                      <div id="dashboard_divparametros">
                                <table width="100%" border="1" class="colorfondo2">
                                  <tr>
                                    <td colspan="2" class="calendario_dia_sel">An&aacute;lisis de densidad de poblaci&oacute;n por municipio</td>
                                  </tr>
                                  <tr>
                                    <td class="textonormal3">Filtros:</td>
                                    <td width="93%" valign="middle" class="calendario">
                                            <div style="vertical-align:middle; float:left; margin-bottom:auto"  id="filter_divmcpio1"></div>
                                            <div style="float:right;" id="filter_divmcpio2"></div>	</td>
                                  </tr>
                                  <tr>
                                    <td width="7%" class="textonormal3">Graficos:</td>
                                    <td>
                                            <div style="float: left;" id="chartmunicipio"></div>	</td>
                                  </tr>
                                  <tr>
                                    <td class="textonormal3">Datos:</td>
                                    <td class="textonormal2"><div style="float: center;" id="tablemunicipio"></div>	</td>
                                  </tr>
                                </table>
                                
                                </div>
-->
                                    </td>
                                    <td width="1%" >&nbsp;</td>
                                    <td width="24%" valign="top" ><table width="100%" border="0">
                                        <tr>
                                          <td height="39" class="titulo_secciones"><h1>Acerca del indicador</h1></td>
                                        </tr>
                                        <tr>
                                          <td class="textonormal2"><p >El indicador, facilita la interpretaci&oacute;n de la  calidad del ambiente marino, la evaluaci&oacute;n el impacto de las actividades  antropog&eacute;nicas y la toma medidas de prevenci&oacute;n y recuperaci&oacute;n para valorar la  calidad de las aguas marinas es decir, su capacidad de soportar la vida marina  y los procesos biol&oacute;gicos. </p>
                                              <p >El indicador es un n&uacute;mero adimensional que  representa la calidad del recurso h&iacute;drico marino, en forma de porcentaje  (valores entre 0 y 100). </p>
                                            <p>Valores del indicador relativamente bajos pueden ser 
                                              interpretados como fuertes presiones sobre el entorno f&iacute;sico 
                                              y natural circundante.</p>
                                                                                            <div class="centrado" ><strong>Leyenda del indicador</strong><br /><img src="images/legend_icam_corta.png" alt="Leyenda" width="50%"/></div>
                                            <p><strong>A&Ntilde;O</strong>: 2001 - 2011</p>
                                            <p><strong>FUENTE</strong>: Instituto de Investigaciones Marinas y Costeras - INVEMAR.</p></td>
                                        </tr>
                                        <tr>
                                          <th align="center" ><p><a href="http://www.invemar.org.co/" target="_blank"><img src="plantillaSitio/images/logo_invemar2.png" alt="INVEMAR" width="50" height="50" /></a></p></th>
                                        </tr>
                                        <tr>
                                          <td align="center" ><a href="atlas_ier_icam.jsp"><img src="images/boton_mapa.png" alt="Ver Datos" width="30%" border="0" /></a><a href="http://cinto.invemar.org.co/geonetwork/srv/es/metadata.show?id=12"><img src="images/boton_metadato.png" alt="Ver Metadato" width="30%" border="0" /></a><a href="http://cinto.invemar.org.co/alfresco/d/a/workspace/SpacesStore/be5e0ea4-fe93-4bfe-8fda-75059fac6b0c/Hoja Metodologica ICAM"><img src="images/boton_hm.png" alt="Ver Hoja Metodol&oacute;gica" width="30%" border="0" /></a></td>
                                        </tr>
                                    </table></td>
                                  </tr>
                              </table></td>
                            </tr>
                            <tr>
                              <td><table style="width:100%;">
                                  <tr>
                                    <td style="width:11px;"></td>
                                    <td></td>
                                    <td style="width:11px;"></td>
                                  </tr>
                              </table></td>
                            </tr>
                            <tr></tr>
                            <tr></tr>
                        </table></td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td style="width:5px; height:4px;"></td>
                </tr>
            </table></td>
            <td width="5" style="vertical-align:top;" ></td>
          </tr>
      </table></td>
    </tr>
    <!-- PIE -->
    <%@ include file="footer_spincam2.jsp" %>
    <!-- FIN PIE -->
  </table>
</div>



</body>
</html>

        