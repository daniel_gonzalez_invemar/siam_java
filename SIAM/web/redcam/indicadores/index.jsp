<!DOCTYPE html>
<html>
    <head>
        <link href="../../plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css" />
        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>

        <script>
            $(document).ready(function() {
                $("#tabs").tabs();
                sustratoChange();
            });


            function sustratoChange() {
               var value= $("#sustrato").val();
               
                if (value=="Agua Marina") {
                    $("#idcla").hide();
                    $("#idhdd").show();
               }else if (value=="Agua Estuarina"){
                    $("#idcla").show();                   
                    $("#idhdd").hide();
               }    
               
                  
                                
            }
        </script>


        <script type='text/javascript' src='https://www.google.com/jsapi'></script>
        <script type='text/javascript'>
          google.load('visualization', '1', {packages: ['gauge']});
          google.setOnLoadCallback(drawChart);
          function drawChart() {
              var data1 = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['OD', 80],
              ]);

              var data2 = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['NO3', 80],
              ]);

              var data3 = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['SST', 80],
              ]);
              var data4 = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['CTE', 80],
              ]);
              var data5 = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['PH', 80],
              ]);
              var data6 = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['HDD', 80],
              ]);

              var data7 = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['DBO', 80],
              ]);

              var data8 = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['PO4', 80],
              ]);

              var options = {
                  width: 400, height: 180,
                  redFrom: 0, redTo: 60,
                  yellowFrom: 60, yellowTo: 80,
                  greenFrom: 80, greenTo: 100,
                  minorTicks: 5


              };

              var chart1 = new google.visualization.Gauge(document.getElementById('chart_div1'));
              chart1.draw(data1, options);

              var chart2 = new google.visualization.Gauge(document.getElementById('chart_div2'));
              chart2.draw(data2, options);

              var chart3 = new google.visualization.Gauge(document.getElementById('chart_div3'));
              chart3.draw(data3, options);

              var chart4 = new google.visualization.Gauge(document.getElementById('chart_div4'));
              chart4.draw(data4, options);

              var chart5 = new google.visualization.Gauge(document.getElementById('chart_div5'));
              chart5.draw(data5, options);

              var chart6 = new google.visualization.Gauge(document.getElementById('chart_div6'));
              chart6.draw(data6, options);

              var chart7 = new google.visualization.Gauge(document.getElementById('chart_div7'));
              chart7.draw(data7, options);

              var chart8 = new google.visualization.Gauge(document.getElementById('chart_div8'));
              chart8.draw(data8, options);


          }
        </script>  
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><title>REDCAM :: Indicadores</title>
    </head>

    <!--<body style="font-size:82.5%;">-->
    <jsp:include page="../../plantillaSitio/headermodulosv3.jsp?idsitio=60"/> 
    <table width="963" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>

                <div id="tabs" >
                    <ul>
                        <li><a href="#fragment-0"><span>Acerca del indicador</span></a></li>
                        <li><a href="#fragment-1"><span>Ingreso de datos</span></a></li>
                    </ul>
                    <div id="fragment-0">
                        <h3>&iquest;Qu&eacute; es?</h3>
                        <p>	    Es una Herramienta de informaci&oacute;n estad&iacute;stica que permite evaluar los cambios del  estado de la calidad de agua marina y costera, basadas en unos criterios o est&aacute;ndares que permite cuantificar el estado de conservaci&oacute;n o deterioro del agua de acuerdo con sus caracter&iacute;sticas y en funci&oacute;n de su uso o destinaci&oacute;n en el lugar y tiempo espec&iacute;fico.</p>
                        <h3>&iquest;Para qu&eacute;? </h3>
                        <p>	    Los an&aacute;lisis de las variables individuales no permite visualizar claramente cu&aacute;l es la calidad del recurso h&iacute;drico, por lo cual se busca generar un indicador de calidad de agua que sirva como base para interpretar los niveles de concentraci�n de los par�metros fisicoqu�micos y microbiol�gicos presentes en el medio.</p>
                        <p>&nbsp;</p>
                        <ul>
                        		<li>Pruebe el indicador &quot;con sus propios datos&quot;...<a href="http://siam.invemar.org.co/siam/redcam/indicadores/#fragment-1"><img src="images/1419393559_document_edit.png" width="64" height="64" border="0"></a></li>
                            	</ul>
                        <ul>
                        		<li>Conozca el resultado del indicador en las estaciones
                        				de la REDCAM...<a href="http://indicadores.invemar.org.co/icam"><img src="images/1419393567_network-workgroup-64.png" width="64" height="64" border="0"></a></li>
                        		</ul>
                        <p align="center"><img src="images/rdc1.png" alt=""></p>
                    </div>

                    <div id="fragment-1">
                        <p style="font-weight:bold">Seleccione el indice a aplicar, uso del mismo y sustrato donde se aplica: </p>
                        <form action="index_2.jsp" method="post" name="parametros_indice" target="_blank" id="parametros_indice">
                            <p>
                                <span style="font-weight:bold">Sustrato</span>
                                <select name="sustrato" id="sustrato"  onchange="sustratoChange()" >
                                    <option value="Agua Marina" selected>Agua Marina</option>
                                    <option value="Agua Estuarina">Agua Estuarina</option>
                                </select> 
                                <span style="font-weight:bold">Indice</span>
                                <select name="indice" id="indice">
                                    <option value="INCOL" selected>ICAM 2011</option>
                                </select>
                               <span style="font-weight:bold"> Uso</span>
                                <select name="uso" id="uso">
                                    <option value="PFF" selected>Preservacion de Flora y Fauna</option>
                                </select>
                                      
                            </p>
                            <p><em>El sistema hace los c&aacute;lculos asumiendo que &quot;su&quot; informaci&oacute;n est&aacute; en las unidades ac&aacute; expresadas...</em></p>
                            <table width="75%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>Ox�geno disuelto (OD)</td>
                                    <td><input name="vod" type="number" id="vod" value="" step="any" min="0"  >
                                        (mg/L) Ejemplo:1.0 � 9</td>
                                </tr>
                                <tr>
                                    <td>Nitratos (NO3)</td>
                                    <td><input name="vno3" type="number" id="vno3" min="0"  value="" step="any" >
                                        (&micro;g/L) Ejemplo:10.30 o 34</td>
                                </tr>
                                <tr>
                                    <td>Solidos Suspendidos Totales (SST)</td>
                                    <td><input name="vsst" type="number" id="vsst" min="0" value="" step="any">
                                        (mg/L)  Ejemplo:15.30 o 20</td>
                                </tr>
                                <tr>
                                    <td>Coliformes Termotolerantes (CTE) </td>
                                    <td><input name="vcte" type="number" id="vcte" min="0"  value="" step="any">
                                        (NMP/100ml)  Ej:20 o 200</td>
                                </tr>
                                <tr>
                                    <td>pH (PH) </td>
                                    <td><input name="vph" type="number" id="vph"  min="0" value="" step="any">
                                        Unidad Ej:6.8 � 6 </td>
                                </tr>
                                <tr id="idhdd" >
                                    <td>Hidrocarburos disueltos y dispersos (HDD)</td>
                                    <td><input name="vhdd" type="number" id="vhdd"  min="0" value="" step="any">
                                        (&micro;g/L)  Ejemplo:10 � 15 </td>
                                </tr>
                                <tr id="idcla">
                                    <td>Clorofila A (CLA)</td>
                                    <td><input name="vcla" type="number" id="vcla"  min="0" value="" step="any">
                                        (&micro;g/L) Ejemplo: 85.96 o 15</td>
                                </tr>
                                <tr>
                                    <td>Demanda bioqu�mica de ox�geno (DBO)</td>
                                    <td><input name="vdbo" type="number" id="vdbo" min="0" value="" step="any">
                                        (mg/L) Ejemplo: 23.5 o 45</td>
                                </tr>
                                <tr>
                                    <td>Fosfatos (PO4)</td>
                                    <td><input name="vpo4" type="number" id="vpo4"  min="0" value="" step="any">
                                        (&micro;g/L) Ejmplo: 96.63 o 15</td>
                                </tr>
                            </table>
                            <p>
                                <input name="limpiar" type="reset" class="ui-button-text" id="limpiar" value="Limpiar">
                                <input name="enviar" type="submit" class="ui-button-text" id="enviar" value="Enviar y probar los datos">
                            </p>
                        </form>
                        <p>&nbsp;</p>
                        <!--      <pre><code>$('#example').tabs();</code></pre> -->
                    </div>
                </div>    </td>
        </tr>
    </table>



    <%@ include file="../../plantillaSitio/footermodulesV3.jsp" %>
</body>
</html>