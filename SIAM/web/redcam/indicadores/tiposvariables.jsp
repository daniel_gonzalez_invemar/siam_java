<%@ page import="co.org.invemar.siam.redcam.coneccam.*" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="co.org.invemar.siam.redcam.estadisticas.cvariable.*" %>
<%
    Variable var = new Variable();
    ContVariable cvar = new ContVariable();
    ContVariable ctipo = new ContVariable();
    ConexionCam con2 = new ConexionCam();
    Connection conn2 = null;

    String ntipo = request.getParameter("ntipo");

    conn2 = con2.getConn();
    ctipo.contenedorntipo(conn2);


    float ind = 0;
    int tam = 0;
    int paginas = 1;

    //pagina actual del resultado 1= valor inicial
    int pag = 1;

    //numero de registros por pagina
    float registros = 10;
    String pagina = request.getParameter("pagina");

    if (ntipo != null) {
        cvar.contenedor(conn2, ntipo);
        paginas = (int) Math.ceil((cvar.gettamano() / registros));
        if (pagina != null) {
            pag = Integer.parseInt(pagina);
        }
        if (paginas == 0) {
            paginas = 1;
        }
    }

    int cont = 0;

    if (con2 != null) {
        con2.close();
    }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
    <head>
        <title>Tipos de variables REDCAM</title>
        <link href="../../plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
        <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1" />
        <script type="text/javascript" src="../js/monitoreo.js"></script>
    </head>
    <jsp:include page="../../plantillaSitio/headermodulosv3.jsp?idsitio=60"/> 
    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr bgcolor="#8DC0E3">
            <td width="20"></td>
            <td width="394" bgcolor="#8DC0E3" class="linksnegros">Red de Calidad Ambiental
                Marina REDCAM :: Consulta de Variables</td>

            <td width="336" bgcolor="#8DC0E3" class="linksnegros">&nbsp;</td>
        </tr>
    </table>



    <form action="tiposvariables.jsp" method="post" name="formacam" id="formacam">
        <div align="left">




            <table width="963px" border="0" align="center" cellpadding="3" cellspacing="0">
                <tr>
                    <td width="963px" height="98">
                        <table width="963px" border="0" align="center" >
                            <tr>
                                <td colspan="2" bgcolor="#CCCCCC" height="8"><b><font color="#99CCFF" face="Verdana, Arial, Helvetica, sans-serif" size="-1"><font color="#FF0000">::</font>
                                            <font color="#333333">Datos de Variables<font color="#6699FF">
                                                    <%if (ntipo != null) {%>
                                                    <%=ntipo%>
                                                    <%}%>
                                                </font></font><font color="#FF0000">::</font></font></b></td>
                            </tr>
                            <tr>
                                <td height="24" width="14%"><font face="Verdana, Arial, Helvetica, sans-serif" size="-2">Tipo
                                        de variable</font></td>
                                <td height="24" width="86%">
                                    <select name="ntipo" id="ntipo">
                                        <option value="">--</option>
                                        <%
                                            for (int j = 0; j < ctipo.gettamano(); j++) {

                                                var = ctipo.getvariable(j);
                                        %>
                                        <option value="<%=var.get_tipo()%>"><%=var.get_nombre()%></option>
                                        <%}%>
                                    </select> </td>
                            </tr>
                            <tr>
                                <td height="2" width="14%">&nbsp;</td>
                                <td height="2" width="86%"><font size="-2">
                                        <input type="submit" name="Submit" value="Buscar" />
                                    </font></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="963px" height="159" valign="top">
                        <hr size="1"/>
                        <table width="963px" border="0" align="center">
                            <tr>
                                <td colspan="4" height="18"><font face="Verdana, Arial, Helvetica, sans-serif" size="-1">Listado
                                        de Variable muestreadas en el sistema</font></td>
                            </tr>
                            <tr bgcolor="#CCCCCC">
                                <td width="156"> 
                                    <div align="center"><font size="-2" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif">
                                                    <font color="#CCFFFF">::</font> Tipo</font><font size="-2" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif">
                                                            <font color="#CCFFFF">::</font></font></b></font></b></font></div></td>
                                <td colspan="2"> <div align="center"><font size="-2" color="#333333"><b><font size="-2" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Variable<b>
                                                            <font color="#CCFFFF">::</font></b></font></font><font face="Verdana, Arial, Helvetica, sans-serif">
                                                </font></b></font></div></td>
                                <td width="164">
                                    <div align="center"><font size="-2" color="#333333"><b><font size="-2" color="#333333"><b><font face="Verdana, Arial, Helvetica, sans-serif"><font color="#CCFFFF">::</font></font></b><font face="Verdana, Arial, Helvetica, sans-serif">Unidad 
                                                        de Medici&oacute;n<b> <font color="#CCFFFF">::</font></b></font></font><font face="Verdana, Arial, Helvetica, sans-serif"> 
                                                </font></b></font></div></td>
                            </tr>
                            <%
                                for (ind = (pag - 1) * registros; (ind < pag * registros) && ind < cvar.gettamano(); ind++) {

                                    var = cvar.getvariable((int) ind);
                            %>
                            <tr>
                                <td width="156" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_tipo()%></font></td>
                                <td width="82" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_cod()%></font></td>
                                <td width="336" bgcolor="#E6E6E6" height="20"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_nombre()%></font></td>
                                <td width="164" bgcolor="#E6E6E6" height="20"> 
                                    <div align="center"><font size="1" face="Arial, Helvetica, sans-serif"><%=var.get_unidad()%></font></div></td>
                            </tr>
                            <%
                                    tam = tam + 1;
                                }
                            %>
                            <tr> 
                                <td width="156">&nbsp;</td>
                                <td width="82">&nbsp;</td>
                                <td width="336">&nbsp;</td>
                                <td width="164">&nbsp;</td>
                            </tr>
                            <tr> 
                                <td colspan="4"> <div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="-2" color="#333333"> 
                                            <% if (pag > 1) {
                                            %>
                                            <a href="tiposvariables.jsp?pagina=<%=pag - 1%>&ntipo=<%=ntipo%>"><< 
                                                Anterior</a> - 
                                                <% }%>
                                            P&aacute;gina <%=pag%> de <%=paginas%> 
                                            <% if (pag < paginas) {%>
                                            - <a href="tiposvariables.jsp?pagina=<%=pag + 1%>&ntipo=<%=ntipo%>">Siguiente 
                                                >></a> 
                                                <% }%>
                                        </font> </div></td>
                            </tr>
                        </table>
                        <div align="right"> </div>
                        <hr size="1" />
                    </td>
                </tr>
            </table>
        </div>
        <input type="hidden" name="tam" value="<%=tam%>" />
    </form>
    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td></td>
        </tr>
        <tr>
            <td ></td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>
    <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td bgcolor="#8DC0E3"></td>
        </tr>
    </table>
    <%@ include file="../../plantillaSitio/footermodulesV3.jsp" %>
</body>
</html>