<%@ page import="co.org.invemar.siam.redcam.coneccam.*" %>
<%@ page import="cindicador.*" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>

<%
    String nod, nno3, nsst, ncte, nph, nhdd, ndbo, npo4, ncla;

    nod = request.getParameter("vod");
    nno3 = request.getParameter("vno3");
    nsst = request.getParameter("vsst");
    ncte = request.getParameter("vcte");
    nph = request.getParameter("vph");
    nhdd = request.getParameter("vhdd");
    ndbo = request.getParameter("vdbo");
    npo4 = request.getParameter("vpo4");
    ncla = request.getParameter("vcla");

    /*  	if(request.getParameter("vod") != null && !request.getParameter("vod").equals("")){
     nod = Float.parseFloat(request.getParameter("vod"));
     }
     if(request.getParameter("vno3") != null && !request.getParameter("vno3").equals("")){
     nno3 = Float.parseFloat(request.getParameter("vno3"));
     }
     if(request.getParameter("vsst") != null && !request.getParameter("vsst").equals("")){
     nsst = Float.parseFloat(request.getParameter("vsst"));
     }
     if(request.getParameter("vcte") != null && !request.getParameter("vcte").equals("")){
     ncte = Float.parseFloat(request.getParameter("vcte"));
     }
     if(request.getParameter("vph") != null && !request.getParameter("vph").equals("")){
     nph = Float.parseFloat(request.getParameter("vph"));
     }
     if(request.getParameter("vhdd") != null && !request.getParameter("vhdd").equals("")){
     nhdd = Float.parseFloat(request.getParameter("vhdd"));
     }
     if(request.getParameter("vdbo") != null && !request.getParameter("vdbo").equals("")){
     ndbo = Float.parseFloat(request.getParameter("vdbo"));
     }
     if(request.getParameter("vpo4") != null && !request.getParameter("vpo4").equals("")){
     npo4 = Float.parseFloat(request.getParameter("vpo4"));
     }

     */
    String nsustrato = request.getParameter("sustrato");

%>
<%    cindicador.Cindicador_icam var = new cindicador.Cindicador_icam();
    cindicador.Ccontindicador_icam cvar = new cindicador.Ccontindicador_icam();

    ConexionCam con2 = new ConexionCam();
    Connection conn2 = null;
    conn2 = con2.getConn();

    String pdecimales = null;
    String nindicador = "SPINCAM";
    String decimales = "0";

    /*  if(request.getParameter("pdecimales") != null && !request.getParameter("pdecimales").equals("")){
     decimales = pdecimales;
     }
     if(request.getParameter("ano1") != null && !request.getParameter("ano1").equals("")){
     ano1 = request.getParameter("ano1");
     }
     if(request.getParameter("ano2") != null && !request.getParameter("ano2").equals("")){
     ano2 = request.getParameter("ano2");
     }
     if(request.getParameter("porden") != null && !request.getParameter("porden").equals("")){
     norden = request.getParameter("porden");
     }
     */
    float ind = 0;
    int tam = 0;
    int paginas = 1;

    //pagina actual del resultado 1= valor inicial
    int pag = 1;

    //numero de registros por pagina
    int registros = 10;

    int cont = 0;
    int h = 0, z = 0;

    if (nsustrato != null && nsustrato.equalsIgnoreCase("Agua Estuarina")) {
        nhdd = null;
    } else {
        ncla = null;
    }

    /**
     * Llamada al contenedor de estadisticas
     */
    if (conn2 != null) {
        cvar.estadisticas_icam_valores(conn2, nsustrato, nod, nno3, npo4, nsst, nhdd, ncte, nph, ndbo, ncla);
        conn2.close();
    }

    /*try
     {
     if (conn2!= null)
     {
     //	out.println("error:...");
     //	out.println("indicador: "+nindicador);
     //	out.println("nparam_nombre: "+nparam_nombre);
     //	out.println("uer: "+nuer);
     //	out.println("a?o1: "+ano1);
     //	out.println("a?o2: "+ano2);
     //	out.println("conn: "+con2.toString());

     cvar.estadisticas_icam_valores(conn2, nsustrato, nod, nno3, npo4, nsst, nhdd, ncte, nph, ndbo);
     conn2.close();
     }
     else{
     out.println("no conexion");
     }

     }
     finally{
     //Este c�digo se ejecuta se produzca o no una excepci�n
     }
	
     */
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <link href="../../plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css">
        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>

        <script>
            $(document).ready(function() {
                $("#tabs").tabs();
            <%        if (nsustrato != null && nsustrato.equalsIgnoreCase("Agua Estuarina")) {
            %>
                $("#showindicadorclorofilaa").show();
                $("#hidrocarburospetroleo").hide();
            <%
            } else {
            %>
                $("#hidrocarburospetroleo").show();
                $("#showindicadorclorofilaa").hide();

            <%
                }
            %>

            });
        </script>


        <script type='text/javascript' src='https://www.google.com/jsapi'></script>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><title>REDCAM :: Indicadores</title>
    </head>

    <!--<body style="font-size:82.5%;">-->
    <jsp:include page="../../plantillaSitio/headermodulosv3.jsp?idsitio=60"/> 
    <table width="963" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>

                <div id="tabs" >
                    <ul>
                        <li><a href="#fragment-2"><span>Resultados de Curvas Ajustadas</span></a></li>
                        <li><a href="#fragment-3"><span>Indicador</span></a></li>
                    </ul>
                    <div id="fragment-2">
                        <p>El ICAM eval&uacute;a la calidad de las aguas mediante  una ecuaci&oacute;n algebraica que califica cada par&aacute;metro y son ponderados seg&uacute;n su  importancia ambiental y de acuerdo a los criterios de calidad de la legislaci&oacute;n.  los par&aacute;metros seleccionados para el c&aacute;lculo del indicador en sustrato <%=nsustrato%> son:</p>

                        <!-- -->

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>....
                                <%
                                    for (ind = 0; ind < cvar.gettamano(); ind++) {
                                        var = new Cindicador_icam();
                                        var = cvar.getindicador((int) ind);
                                %>   

                            <script type='text/javascript'>
            google.load('visualization', '1', {packages: ['gauge']});
            google.setOnLoadCallback(drawChart);
            function drawChart() {
                var data1 = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['OD', <%=var.get_ca_od()%>],
                ]);

                var data2 = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['NO3', <%=var.get_ca_no3()%>],
                ]);

                var data3 = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['SST', <%=var.get_ca_sst()%>],
                ]);
                var data4 = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['CTE', <%=var.get_ca_cte()%>],
                ]);
                var data5 = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['PH', <%=var.get_ca_ph()%>],
                ]);
                var data6 = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['HDD', <%=var.get_ca_hdd()%>],
                ]);

                var data7 = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['DBO', <%=var.get_ca_dbo()%>],
                ]);

                var data8 = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['PO4', <%=var.get_ca_po4()%>],
                ]);

                var data9 = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['CLA', <%=var.get_ca_cla()%>],
                ]);
                var dataicam = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['ICAM', <%=var.get_icampff()%>],
                ]);

                var options = {
                    width: 400, height: 180,
                    redFrom: 0, redTo: 60,
                    yellowFrom: 60, yellowTo: 80,
                    greenFrom: 80, greenTo: 100,
                    minorTicks: 5


                };

                var chart1 = new google.visualization.Gauge(document.getElementById('chart_div1'));
                chart1.draw(data1, options);

                var chart2 = new google.visualization.Gauge(document.getElementById('chart_div2'));
                chart2.draw(data2, options);

                var chart3 = new google.visualization.Gauge(document.getElementById('chart_div3'));
                chart3.draw(data3, options);

                var chart4 = new google.visualization.Gauge(document.getElementById('chart_div4'));
                chart4.draw(data4, options);

                var chart5 = new google.visualization.Gauge(document.getElementById('chart_div5'));
                chart5.draw(data5, options);

                var chart6 = new google.visualization.Gauge(document.getElementById('chart_div6'));
                chart6.draw(data6, options);

                var chart7 = new google.visualization.Gauge(document.getElementById('chart_div7'));
                chart7.draw(data7, options);

                var chart8 = new google.visualization.Gauge(document.getElementById('chart_div8'));
                chart8.draw(data8, options);

 var chart9 = new google.visualization.Gauge(document.getElementById('chart_div9'));
                chart9.draw(data9, options);

                var charticam = new google.visualization.Gauge(document.getElementById('chart_divicam'));
                charticam.draw(dataicam, options);


            }
                            </script>  


                            <td><table width="100%" border="1" cellspacing="3" cellpadding="0">
                                    <tr>
                                        <td colspan="2" class="ui-widget-header">An&aacute;lisis del Par&aacute;metro... &quot;Ox&iacute;geno Disuelto&quot;</td>
                                    </tr>
                                    <tr>
                                        <td width="67%"><div align="justify">
                                                <p>Este par&aacute;metro es necesario  para la respiraci&oacute;n de los microorganismos, as&iacute; como para otras formas de vida,  es una medida de la capacidad del agua para sostener vida acu&aacute;tica; esta  variable es necesaria para medir y controlar los niveles de ox&iacute;geno, conocer la  supervivencia de las especies y los procesos biol&oacute;gicos de producci&oacute;n. Las  reducciones por debajo del porcentaje de saturaci&oacute;n generan efectos negativos  sobre la biodiversidad, el crecimiento, la reproducci&oacute;n y la actividad de &eacute;stas  especies. El ox&iacute;geno disuelto determina si en los procesos de degradaci&oacute;n  dominan los organismos aerobios o los anaerobios, lo que marca la capacidad del  agua para llevar a cabo procesos de auto purificaci&oacute;n.</p>
                                            </div></td>
                                        <td width="33%">
                                            <%
                                                if (request.getParameter("vod").equals("")) {
                                            %>
                                            <div align="center" id='chart_div1' style="visibility:hidden"></div>
                                            <%
                                            } else {
                                            %>
                                            <div align="center" id='chart_div1'></div>
                                            <% } %>				</td>
                                    </tr>
                                    <tr>
                                        <td><div align="right">Calificaci&oacute;n del valor ajustado...</div></td>
                                        <td>
                                            <%
                                                if (request.getParameter("vod") == null || request.getParameter("vod").equals("")) {
                                            %>
                                            <div align="center"><strong>PAR&Aacute;METRO NO INGRESADO</strong></div>
                                            <%
                                            } else {
                                            %>
                                            <div align="center"><%=var.get_ca_od()%> <strong>&nbsp; <%=var.get_od_calificacion()%> </strong></div>
                                            <% } %>				</td>
                                    </tr>
                                </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="100%" border="1" cellspacing="3" cellpadding="0">
                                        <tr>
                                            <td colspan="2" class="ui-widget-header">An&aacute;lisis del Par&aacute;metro... &quot;Nitratos&quot;</td>
                                        </tr>
                                        <tr>
                                            <td width="67%"><div align="justify">Variable que sirve para medir la concentraci&oacute;n  de nitratos procedentes de aguas residuales o de fertilizantes que contaminan  el recurso h&iacute;drico provocando la eutrofizaci&oacute;n. La presencia de niveles altos  de nitrato en algunos cuerpos de agua indica aportes antropog&eacute;nicos como la  contaminaci&oacute;n del agua subterr&aacute;nea, ya que el nitrato es el producto final de  estabilizaci&oacute;n de los desechos.</div></td>
                                            <td width="33%">
                                                <%
                                                    if (request.getParameter("vno3").equals("")) {
                                                %>
                                                <div align="center" id='chart_div2' style="visibility:hidden"></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center" id='chart_div2'></div>
                                                <% } %>				</td>
                                        </tr>
                                        <tr>
                                            <td><div align="right">Calificaci&oacute;n del valor ajustado...</div></td>
                                            <td>
                                                <%
                                                    if (request.getParameter("vno3") == null || request.getParameter("vno3").equals("")) {
                                                %>
                                                <div align="center"><strong>PAR&Aacute;METRO NO INGRESADO</strong></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center"><%=var.get_ca_no3()%> <strong>&nbsp; <%=var.get_no3_calificacion()%> </strong></div>
                                                <% } %>				</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="100%" border="1" cellspacing="3" cellpadding="0">
                                        <tr>
                                            <td colspan="2" class="ui-widget-header">An&aacute;lisis del Par&aacute;metro... &quot;Solidos Suspendidos Totales&quot;</td>
                                        </tr>
                                        <tr>
                                            <td width="67%"><div align="justify">Este par&aacute;metro se refiere a la cantidad de s&oacute;lidos  suspendidos que corren o albergan en un cuerpo de agua. Los altos niveles de  s&oacute;lidos suspendidos totales pueden resultar da&ntilde;inos a los h&aacute;bitats y causar  condiciones anaerobias en los lagos, r&iacute;os y mares, debido a la descomposici&oacute;n  de los s&oacute;lidos, adem&aacute;s reduce la penetraci&oacute;n de luz solar al cuerpo de agua,  disminuye la columna de agua y generar problemas de colmataci&oacute;n.</div></td>
                                            <td width="33%">
                                                <%
                                                    if (request.getParameter("vsst").equals("")) {
                                                %>
                                                <div align="center" id='chart_div3' style="visibility:hidden"></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center" id='chart_div3'></div>
                                                <% } %>				</td>
                                        </tr>
                                        <tr>
                                            <td><div align="right">Calificaci&oacute;n del valor ajustado...</div></td>
                                            <td>
                                                <%
                                                    if (request.getParameter("vsst") == null || request.getParameter("vsst").equals("")) {
                                                %>
                                                <div align="center"><strong>PAR&Aacute;METRO NO INGRESADO</strong></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center"><%=var.get_ca_sst()%> <strong>&nbsp; <%=var.get_sst_calificacion()%> </strong></div>
                                                <% } %>				</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="100%" border="1" cellspacing="3" cellpadding="0">
                                        <tr>
                                            <td colspan="2" class="ui-widget-header">An&aacute;lisis del Par&aacute;metro... &quot;Coliformes Termotolerantes&quot;</td>
                                        </tr>
                                        <tr>
                                            <td width="67%"><div align="justify">Es un indicador de contaminaci&oacute;n biol&oacute;gico de  contaminaci&oacute;n que se incorpor&oacute; para evaluar la presencia en determinadas  concentraciones asociadas a la ocurrencia de agentes pat&oacute;genos y a un riesgo de  afectaci&oacute;n en la salud de las personas.</div></td>
                                            <td width="33%">
                                                <%
                                                    if (request.getParameter("vcte").equals("")) {
                                                %>
                                                <div align="center" id='chart_div4' style="visibility:hidden"></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center" id='chart_div4'></div>
                                                <% } %>				</td>
                                        </tr>
                                        <tr>
                                            <td><div align="right">Calificaci&oacute;n del valor ajustado...</div></td>
                                            <td><div align="center">
                                                    <%
                                                        if (request.getParameter("vcte") == null || request.getParameter("vcte").equals("")) {
                                                    %>
                                                    <div align="center"><strong>PAR&Aacute;METRO NO INGRESADO</strong></div>
                                                    <%
                                                    } else {
                                                    %>
                                                    <div align="center"><%=var.get_ca_cte()%> <strong>&nbsp; <%=var.get_cte_calificacion()%> </strong></div>
                                                <% } %>				</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="100%" border="1" cellspacing="3" cellpadding="0">
                                        <tr>
                                            <td colspan="2" class="ui-widget-header">An&aacute;lisis del Par&aacute;metro... &quot;pH&quot;</td>
                                        </tr>
                                        <tr>
                                            <td width="67%"><div align="justify">El pH es considerado una variable com&uacute;n entre  los ICA por su potencial como indicador de la calidad del agua en general. El  pH controla las cantidades en que se disuelven muchas sustancias, un  mantenimiento del pH apropiado en el agua ayuda a prevenir el grado de  afectaci&oacute;n por agentes contaminantes y tambi&eacute;n sirve para conocer la  subsistencia de las comunidades que habitan dentro de un sistema lagunar.</div></td>
                                            <td width="33%">
                                                <%
                                                    if (request.getParameter("vph").equals("")) {
                                                %>
                                                <div align="center" id='chart_div5' style="visibility:hidden"></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center" id='chart_div5'></div>
                                                <% } %>				</td>
                                        </tr>
                                        <tr>
                                            <td><div align="right">Calificaci&oacute;n del valor ajustado...</div></td>
                                            <td>

                                                <%
                                                    if (request.getParameter("vph") == null || request.getParameter("vph").equals("")) {
                                                %>
                                                <div align="center"><strong>PAR&Aacute;METRO NO INGRESADO</strong></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center"><%=var.get_ca_ph()%> <strong>&nbsp; <%=var.get_ph_calificacion()%> </strong></div>
                                                <% } %>				</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr id="hidrocarburospetroleo">
                                <td><table width="100%" border="1" cellspacing="3" cellpadding="0">
                                        <tr>
                                            <td colspan="2" class="ui-widget-header">An&aacute;lisis del Par&aacute;metro... &quot;Hidrocarburos del petr&oacute;leo&quot;</td>
                                        </tr>
                                        <tr>
                                            <td width="67%"><div align="justify">La evaluaci&oacute;n de hidrocarburos se centra en los compuestos arom&aacute;ticos disueltos y dispersos, por ser los m&aacute;s t&oacute;xicos para el medio marino. Los hidrocarburos impiden el intercambio gaseoso con la atm&oacute;sfera, iniciando una serie de procesos f&iacute;sico-qu&iacute;micos simult&aacute;neos, como evaporaci&oacute;n y penetraci&oacute;n, que dependiendo del tipo de hidrocarburo, temperatura y cantidad vertida pueden ser procesos m&aacute;s o menos lentos lo que ocasiona una mayor toxicidad; adem&aacute;s impiden la entrada de luz y el intercambio gaseoso, dando comienzo a la solubilizaci&oacute;n de compuestos hidrosolubles y a la afecci&oacute;n de diferentes poblaciones</div></td>
                                            <td width="33%">

                                                <%
                                                    if (request.getParameter("vhdd").equals("")) {
                                                %>
                                                <div align="center" id='chart_div6' style="visibility:hidden"></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center" id='chart_div6'></div>
                                                <% } %>				</td>
                                        </tr>
                                        <tr>
                                            <td><div align="right">Calificaci&oacute;n del valor ajustado...</div></td>
                                            <td>
                                                <%
                                                    if (request.getParameter("vhdd") == null || request.getParameter("vhdd").equals("")) {
                                                %>
                                                <div align="center"><strong>PAR&Aacute;METRO NO INGRESADO</strong></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center"><%=var.get_ca_hdd()%> <strong>&nbsp; <%=var.get_hdd_calificacion()%> </strong></div>
                                                <% } %>				</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr id="showindicadorclorofilaa">
                                <td><table width="100%" border="1" cellspacing="3" cellpadding="0">
                                        <tr>
                                            <td colspan="2" class="ui-widget-header">An&aacute;lisis del Par&aacute;metro... Clorofila A</td>
                                        </tr>
                                        <tr>
                                            <td width="67%"><div align="justify">La concentraci&ocute;n de clorofila-a (Cl-a) es utilizada universalmente como una medida de la biomasa de fitoplancton por su relaci&oacute;n con la calidad del agua en estuarios, como &iacute;ndice de condiciones tr&oacute;ficas (Torres,_2009), en funci&oacute;n de abundancia o escasez de organismos, nutrientes y materia org&oacute;nica para designar ambientes en estado eutr&oacute;fico y oligotr&oacute;fico.
En este sentido lagos eutr&oacute;ficos son aquellos en que la disponibilidad alimentaria permite sustentar una abundante biomasa y por el contrario resultan oligotr&oacute;ficos los ambientes de escasa disponibilidad, la cual limita el desarrollo de la actividad biol&oacute;gica. 
</div></td>
                                            <td width="33%">

                                                <%
                                                    if (request.getParameter("vcla").equals("")) {
                                                        System.out.println("VCLA no es vacia");
                                                %>
                                                <div align="center" id='chart_div9' style="visibility:hidden"></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center" id='chart_div9'></div>
                                                <% } %>				</td>
                                        </tr>
                                        <tr>
                                            <td><div align="right">Calificaci&oacute;n del valor ajustado...</div></td>
                                            <td>
                                                <%
                                                    if (request.getParameter("vcla") == null || request.getParameter("vcla").equals("")) {
                                                %>
                                                <div align="center"><strong>PAR&Aacute;METRO NO INGRESADO</strong></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center"><%=var.get_ca_cla()%> <strong>&nbsp; <%=var.get_cla_calificacion()%> </strong></div>
                                                <% } %>				</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="100%" border="1" cellspacing="3" cellpadding="0">
                                        <tr>
                                            <td colspan="2" class="ui-widget-header">An&aacute;lisis del Par&aacute;metro... &quot;Demanda bioqu&iacute;mica de ox&iacute;geno&quot;</td>
                                        </tr>
                                        <tr>
                                            <td width="67%"><div align="justify">Es un indicador de contaminaci&oacute;n org&aacute;nica que se  usa para medir el contenido de materia org&aacute;nica y determina el oxigeno  requerido por los organismos para su degradaci&oacute;n. El aumento de la DBO ocasiona  disminuci&oacute;n del ox&iacute;geno disuelto, afectando la vida acu&aacute;tica. La putrefacci&oacute;n  de la materia org&aacute;nica en el agua produce una disminuci&oacute;n de la cantidad de  ox&iacute;geno que causa graves da&ntilde;os a la flora y fauna acu&aacute;tica, pero que desaparece  al t&eacute;rmino del proceso de putrefacci&oacute;n.</div></td>
                                            <td width="33%">
                                                <%
                                                    if (request.getParameter("vdbo").equals("")) {
                                                %>
                                                <div align="center" id='chart_div7' style="visibility:hidden"></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center" id='chart_div7'></div>
                                                <% } %>				</td>
                                        </tr>
                                        <tr>
                                            <td><div align="right">Calificaci&oacute;n del valor ajustado...</div></td>
                                            <td>
                                                <%
                                                    if (request.getParameter("vdbo") == null || request.getParameter("vdbo").equals("")) {
                                                %>
                                                <div align="center"><strong>PAR&Aacute;METRO NO INGRESADO</strong></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center"><%=var.get_ca_dbo()%> <strong>&nbsp; <%=var.get_dbo_calificacion()%> </strong></div>
                                                <% } %>				</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="100%" border="1" cellspacing="3" cellpadding="0">
                                        <tr>
                                            <td colspan="2" class="ui-widget-header">An&aacute;lisis del Par&aacute;metro... &quot;Fosfatos&quot;</td>
                                        </tr>
                                        <tr>
                                            <td width="67%"><div align="justify">Es un elemento necesario para la vida, los  excesos de fosfatos causan desarrollo excesivo de las algas y la eutrofizaci&oacute;n  de las aguas. El fosfato entra a las fuentes de agua por escurrimiento del  suelo, operaciones industriales y aguas negras o residuales.</div></td>
                                            <td width="33%">
                                                <%
                                                    if (request.getParameter("vpo4").equals("")) {
                                                %>
                                                <div align="center" id='chart_div8' style="visibility:hidden"></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center" id='chart_div8'></div>
                                                <% } %>				</td>
                                        </tr>
                                        <tr>
                                            <td><div align="right">Calificaci&oacute;n del valor ajustado...</div></td>
                                            <td>
                                                <%
                                                    if (request.getParameter("vpo4") == null || request.getParameter("vpo4").equals("")) {
                                                %>
                                                <div align="center"><strong>PAR&Aacute;METRO NO INGRESADO</strong></div>
                                                <%
                                                } else {
                                                %>
                                                <div align="center"><%=var.get_ca_po4()%> <strong>&nbsp; <%=var.get_po4_calificacion()%> </strong></div>
                                                <% }%>				</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        <p>&nbsp;    </p>
                    </div>
                    <div id="fragment-3">
                        <p>Analisis del indicador aplicado a sustrato <strong> <%=nsustrato%></strong></p>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td><table width="100%" border="1" cellspacing="3" cellpadding="0">
                                        <tr>
                                            <td colspan="2" class="ui-widget-header">Resultado del ejercicio...</td>
                                        </tr>
                                        <tr>
                                            <td width="67%"><div align="justify">
                                                    <p>El indicador, facilita la interpretaci&oacute;n de la calidad
                                                        del ambiente marino, la evaluaci&oacute;n el impacto de las
                                                        actividades antropog&eacute;nicas y la toma medidas de prevenci&oacute;n
                                                        y recuperaci&oacute;n para valorar la calidad de las aguas
                                                        marinas es decir, su capacidad de soportar la vida marina y
                                                        los procesos biol&oacute;gicos.</p>
                                                    <p>El indicador es un n&uacute;mero adimensional que representa
                                                        la calidad del recurso h&iacute;drico marino, en forma de porcentaje
                                                        (valores entre 0 y 100).</p>
                                                    <p>Se califica de la siguiente forma:</p>
                                                    <p><img src="http://siam.invemar.org.co/indicadores/images/legend_icam_corta.png" alt="">&nbsp;</p>
                                                    <p>Valores del indicador relativamente bajos pueden ser interpretados
                                                        como fuertes presiones sobre el entorno f&iacute;sico y natural
                                                        circundante.</p>
                                                </div></td>
                                            <td width="33%"><div align="center" id='chart_divicam'> </div></td>
                                        </tr>
                                        <tr>
                                            <td><div align="right">Calificaci&oacute;n del indicador...</div></td>
                                            <td><div align="center"><%=var.get_icampff()%> <strong>&nbsp; <%=var.get_icampff_calificacion()%></strong></div></td>
                                        </tr>
                                        <tr>
                                            <td><div align="right">Par&aacute;metros ingresados...</div></td>
                                            <td><div align="center"><%=var.get_count_pesos()%></div></td>
                                        </tr>
                                        <tr>
                                            <td><div align="right">Confianza del resultado...</div></td>
                                            <td><div align="center"><%=var.get_peso()%>%</div></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table>
                        <%				}
                        %>				

                        <p align="center"><img src="images/rdc2.png" alt=""></p>
                    </div>
                </div>    </td>
        </tr>
    </table>


    <%@ include file="../../plantillaSitio/footermodulesV3.jsp" %>
</body>
</html>