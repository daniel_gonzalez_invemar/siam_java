<%@ include file="common.jsp" %>
<%@ page import="conex.*" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="especies.*" %>
<jsp:directive.page import="co.org.invemar.siam.sibm.model.CountFacade"/>
<%

  especies.Cficha_cat fi = new especies.Cficha_cat();

  //Cconexsib con = new Cconexsib();
  Connection conn = null;
  //conn = con.getConn();
  conn = sibmDS.getConnection();

  especies.Ccontficha_cat confi = new especies.Ccontficha_cat();
	confi.contenedor(conn, request.getParameter("clave"), request.getParameter("numero"));

  float ind = 0;
  int tam = 0;
  int paginas = 1;

  //pagina actual del resultado 1= valor inicial
  int pag = 1;

  //numero de registros por pagina
  float registros = 1;
  String pagina = request.getParameter("pagina");

    paginas =  (int)Math.ceil((confi.gettamano() / registros));
    if(pagina != null)
      pag = Integer.parseInt(pagina);
    if(paginas == 0)
      paginas = 1;

  int cont = 0;

  /*if (con!= null)
    con.close();*/
  conn.close();
%>
<jsp:include page="../../plantillaSitio/headermodulosv3.jsp?idsitio=30"/>

<%
	for(ind = (pag -1)*registros; (ind < pag*registros) && ind < confi.gettamano(); ind++){

  fi = new especies.Cficha_cat();
		fi = confi.getficha_cat((int)ind);

  if(fi.get_autor() == null)
    fi.setautor("");
  if(fi.get_seccion() == null)
    fi.setseccion("");
  if(fi.get_noorig_dest() == null)
	  fi.setnoorig_dest("");
  if(fi.get_ubicacion() == null)
    fi.setubicacion("");
  if(fi.get_ubi_geo() == null)
  	fi.setubi_geo("");
  if(fi.get_especie() == null)
  	fi.setespecie("");
  if(fi.get_nombre_comun() == null)
  	fi.setnombre_comun("");
  if(fi.get_familia() == null)
	  fi.setfamilia("");
  if(fi.get_orden() == null)
  	fi.setorden("");
  if(fi.get_clase() == null)
	  fi.setclase("");
  if(fi.get_fecha_recibido() == null)
  	fi.setfecha_recibido("");
  if(fi.get_tipo_especimen() == null)
	  fi.settipo_especimen("");
  if(fi.get_preservativo() == null)
  	fi.setpreservativo("");
  if(fi.get_adquisicion() == null)
  	fi.setadquisicion("");
  if(fi.get_proyecto() == null)
  	fi.setproyecto("");
  if(fi.get_procedencia() == null)
  	fi.setprocedencia("");
  if(fi.get_colectado_por() == null)
  	fi.setcolectado_por("");
  if(fi.get_fechacap() == null)
  	fi.setfechacap("");
  if(fi.get_metodo_captura() == null)
  	fi.setmetodo_captura("");
  if(fi.get_identificado_por() == null)
  	fi.setidentificado_por("");
  if(fi.get_fechaide() == null)
  	fi.setfechaide("");
  if(fi.get_tecnica_ident() == null)
  	fi.settecnica_ident("");
  if(fi.get_pais() == null)
  	fi.setpais("");
  if(fi.get_depto() == null)
  	fi.setdepto("");
  if(fi.get_mcpio() == null)
  	fi.setmcpio("");
  if(fi.get_localidad() == null)
  	fi.setlocalidad("");
  if(fi.get_cuer_agua() == null)
  	fi.setcuer_agua("");
  if(fi.get_nombrecagua() == null)
  	fi.setnombrecagua("");
  if(fi.get_latini() == null)
  	fi.setlatini("");
  if(fi.get_latfin() == null)
  	fi.setlatfin("");
  if(fi.get_lonini() == null)
  	fi.setlonini("");
  if(fi.get_lonfin() == null)
  	fi.setlonfin("");
  if(fi.get_barco() == null)
  	fi.setbarco("");
  if(fi.get_camp() == null)
	  fi.setcamp("");
  if(fi.get_estacion() == null)
  	fi.setestacion("");
  if(fi.get_prof_agua() == null)
  	fi.setprof_agua("");
  if(fi.get_ambiente() == null)
  	fi.setambiente("");
  if(fi.get_temp_aire() == null)
  	fi.settemp_aire("");
  if(fi.get_temp_agua() == null)
	  fi.settemp_agua("");
  if(fi.get_prof_captura() == null)
  	fi.setprof_captura("");
  if(fi.get_objetos() == null)
  	fi.setobjetos("");

%>
<% 
  		String numero=request.getParameter("numero");
  		CountFacade fc=new CountFacade();
  		int count=0;
  		count=fc.countImages(numero);
  		String phylum=request.getParameter("phylum");
  		String nombre=request.getParameter("nombre");
  		String autor=request.getParameter("autor");
  		String clave=request.getParameter("clave");
  		String catalogo=request.getParameter("catalogo");
 %>
<table width="738" align="center">

   <tr>
      <td width="165" bgcolor="#B5D7DE" class="txttaxon1">M&aacute;s informaci&oacute;n...</td>
	
      <td bgcolor="#e6e6e6" class="texttablas">
        <%if(count>0){ %>
      	<a href="zsibfichaimagen.jsp?clave=<%=request.getParameter("clave")%>&amp;nombre=<%=fi.get_especie()%>&amp;numero=<%=request.getParameter("numero")%>" target="_blank">Im&aacute;genes
	  en SIBM</a>
	  <%}
	  
	  count=fc.countAutores(numero);
		if(count>0){	  
	   %>
	  <br/><a href="zsibficharefbib.jsp?clave=<%=request.getParameter("clave")%>&amp;nombre=<%=fi.get_especie()%>&amp;fichamuseo=<%=true%>" target="_blank">Referencias bibliograficas </a>
   	<%}	
  	count=fc.countDiagnosis(numero);
  	if(count>0){ %>
   <br/><a name="diagnosis" href="fichaCatagoloService?action=Diagnosis&amp;phylum=<%=phylum%>&amp;nombre=<%=nombre%>&amp;autor=<%=autor%>&amp;clave=<%=clave %>&amp;catalogo=<%=catalogo %>&amp;numero=<%=numero %>">Caracteres diagnosticos evaluados</a> 
   <%}
   	count=fc.countObjetoRelacionados(numero);
   	if(count>0){
   %>
   <br/><a name="objs" href="fichaCatagoloService?action=ObjetosRelacionados&phylum=<%=phylum%>&nombre=<%=nombre%>&autor=<%=autor%>&clave=<%=clave %>&amp;catalogo=<%=catalogo %>&numero=<%=numero %>">Objetos Relacionados</a>    
   <%}
   	count=fc.countNomenclaturas(numero);
   	if(count>0){
    %>
   <br/><a name="nomenc" href="fichaCatagoloService?action=Nomenclaturas&phylum=<%=phylum%>&nombre=<%=nombre%>&autor=<%=autor%>&clave=<%=clave %>&amp;catalogo=<%=catalogo %>&numero=<%=numero %>">Historial de nomenclaturas</a>    
    <%}
   	count=fc.countResponsables(numero);
   	if(count>0){
    %>
   <br/><a name="nomenc" href="fichaCatagoloService?action=Responsables&phylum=<%=phylum%>&nombre=<%=nombre%>&autor=<%=autor%>&clave=<%=clave %>&amp;catalogo=<%=catalogo %>&numero=<%=numero %>">Listado de autorias</a>    
    <%}
   	count=fc.countAtributos(numero);
   	if(count>0){
    %>
   <br/><a name="nomenc" href="fichaCatagoloService?action=Atributos&phylum=<%=phylum%>&nombre=<%=nombre%>&autor=<%=autor%>&clave=<%=clave %>&amp;catalogo=<%=catalogo %>&numero=<%=numero %>">Atributos de inter&eacute;s</a>    
       <%}
   	count=fc.countNotas(numero);
   	if(count>0){
    %>
   <br/><a name="nomenc" href="fichaCatagoloService?action=Notas&phylum=<%=phylum%>&nombre=<%=nombre%>&autor=<%=autor%>&clave=<%=clave %>&amp;catalogo=<%=catalogo %>&numero=<%=numero %>">Notas</a>    
  <%} %>
    
    
      </td>
  </tr>
</table>


  <table width="738" height="405" border="3" cellspacing="1" align="center">
    <tr>
      <td height="397"><table width="738" border="1" cellspacing="0">
          <tr>
            <td colspan="7"><div align="center"><font color="#666666" size="1" face="Arial, Helvetica, sans-serif"><strong>MHNMC
                - INVEMAR</strong></font></div></td>
          </tr>
          <tr valign="top">
            <td width="12%" height="32"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Secci&oacute;n:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_seccion()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><br>
              </font></td>
            <td width="10%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>No. Cat:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_nocat()%></font></td>
            <td width="13%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>No. Inv:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_noinv()%></font></td>

          <td width="17%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>No. Orig. Dest:</strong><br>
            </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_noorig_dest()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
            </font></td>

          <td width="15%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Ejemplares:</strong><br>
            </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_ejemplares()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
            </font></td>

<%  if(fi.get_tipo() != null)
	{
	%>
          <td width="14%" bgcolor="#99CCFF"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong><font color="#000000">Tipo:</font></strong><font color="#000000"><br>
            </font></font><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_tipo()%></font><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
            </font></td>
	<% } %>

<%  if(fi.get_tipo() == null)
	{  fi.settipo("");

	%>

          <td width="14%" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Tipo:</strong><br>
            </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_tipo()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
            </font></td>
	<% } %>


          <td width="19%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Ubicaci&oacute;n:</strong><br>
            </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_ubicacion()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
            </font></td>
          </tr>
          <tr valign="top">

          <td colspan="3"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Especie:</strong><br>
            </font><font size="2" face="Arial, Helvetica, sans-serif"><strong><i><%=fi.get_especie()%></i></strong>
			<font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_autor()%></font></font></td>

          <td colspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Familia:</strong><br>
            </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_familia()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
            </font></td>

          <td colspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Orden:</strong><br>
            </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_orden()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
            </font></td>
          </tr>
          <tr valign="top">

          <td height="35" colspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Clase:</strong><br>
            </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_clase()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
            </font></td>

          <td colspan="5"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Nombres
            comunes:</strong><br>
            </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_nombre_comun()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
            </font></td>
          </tr>
          <tr valign="top">
            <td height="35" colspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Fecha
              recibido:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_fecha_recibido()%></font></td>
            <td colspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Tipo
              especimen:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_tipo_especimen()%></font></td>
            <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Preservativo:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_preservativo()%></font></td>
            <td colspan="2" rowspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Objetos
                  relacionados:</strong><br></font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_objetos()%></font>
              </td>
          </tr>
          <tr valign="top">
            <td height="35"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Adquisici&oacute;n:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_adquisicion()%></font></td>
            <td colspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Proyecto:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_proyecto()%></font></td>
            <td colspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Procedencia:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_procedencia()%></font></td>
          </tr>
          <tr valign="top">
            <td height="33" colspan="3"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Colectado
              por:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_colectado_por()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
              </font></td>
            <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Fecha
              captura:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_fechacap()%></font></td>
            <td colspan="3"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>M&eacute;todo
              captura:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_metodo_captura()%></font></td>
          </tr>
          <tr valign="top">
            <td height="32" colspan="3"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Identificado
              por:</strong><br>
            </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_identificado_por()%></font></td>
            <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Fecha
              identificaci&oacute;n:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_fechaide()%></font></td>
            <td colspan="3"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>T&eacute;cnica
              identificaci&oacute;n:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_tecnica_ident()%></font></td>
          </tr>
          <tr valign="top">
            <td height="38"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Pa&iacute;s:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_pais()%></font></td>

          <td colspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Departamento:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_depto()%></font></td>
            <td colspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Municipio:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_mcpio()%></font></td>
            <td colspan="2"><table width="100%" border="1" cellspacing="0" bordercolor="#CCCCCC">
                <tr>
                  <td colspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Cuerpo
                    de Agua:</strong></font></td>
                </tr>
                <tr>
                  <td height="16"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_cuer_agua()%></font></td>
                  <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_nombrecagua()%></font></td>
                </tr>
              </table></td>
          </tr>
          <tr valign="top">
            <td height="34" colspan="4"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Localidad:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_localidad()%></font></td>
            <td colspan="3" rowspan="2"><p><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Coordenadas:</strong><br>
                </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_latini()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
                - </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_latfin()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
                Iniciales <br>
                </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_lonini()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
                - </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_lonfin()%>
                Finales </font></p></td>
          </tr>
          <tr valign="top">
            <td height="28"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Barco:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_barco()%></font></td>
            <td colspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Campa&ntilde;a:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_camp()%></font></td>
            <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Estaci&oacute;n:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_estacion()%></font></td>
          </tr>
          <tr valign="top">
            <td height="35"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Prof.
              Agua:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_prof_agua()%></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><br>
              </font></td>
            <td colspan="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Prof.
              Captura:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_prof_captura()%></font></td>
            <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Ambiente:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_ambiente()%></font></td>
            <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Temp.
              Aire:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_temp_aire()%></font></td>
            <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Temp.
              Agua:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_temp_agua()%></font></td>
            <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Salinidad:</strong><br>
              </font><font size="1" face="Arial, Helvetica, sans-serif"><%=fi.get_salinidad()%></font></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <br>
<%
tam= tam+1;
}
%>


  <p align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="-2" color="#333333">
    <% if(pag > 1){
                %>
    <a href="ficha_catalogo.jsp?pagina=<%=pag-1%>&clave=<%=request.getParameter("clave")%>&numero=<%=request.getParameter("numero")%>"><<
    Anterior</a> -
    <% } %>
    P&aacute;gina <%=pag%> de <%=paginas%>
    <% if(pag < paginas){%>
    - <a href="ficha_catalogo.jsp?pagina=<%=pag+1%>&clave=<%=request.getParameter("clave")%>&numero=<%=request.getParameter("numero")%>">Siguiente
    >></a>
    <% } %>
    </font></p>
  <p>
    <input type="hidden" name="tam" value="<%=tam%>" >
  </p>

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2300620-1";
urchinTracker();
</script>

<%@ include file="sibm_fondo.jsp" %>