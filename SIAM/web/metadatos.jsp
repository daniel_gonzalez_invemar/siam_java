<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>SIAM -Sistema de informacion Marino de Colombia</title>
        <link href="plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    </head>
    <body>
        <jsp:include page="plantillaSitio/headerv2.jsp?idsitio=214"/>
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr bgcolor="#8DC0E3">
                <td width="21"></td>
                <td width="729" bgcolor="#8DC0E3" class="linksnegros">Catalogador SIB Sobre Informaci&oacute;n Ambiental </td>


            </tr>
        </table>

        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="370"><table border="0" cellpadding="0" cellspacing="0" width="385">

                        <tr>
                            <td  width="385" align="right" valign="middle"><a href="http://cinto.invemar.org.co/geonetwork/srv/eng/main.home" class="titulosnegros"><img src="images/geonetwork-logo.png" width="300" height="150" border="0" alt="" /></a></td>
                        </tr>
                    </table>
                </td>
                <td align="right">
                    <table border="0" cellpadding="0" cellspacing="0" width="370">
                        <tr>
                            <td width="130" align="right" valign="middle"><a href="http://cinto.invemar.org.co/metabuscador/searchPV1.jsp"><img src="images/metabuscadorAmbiental.png" width="369" height="150" border="0"></a></td>
                    </table>
                </td>   
            </tr>

        </table>

        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr bgcolor="#8DC0E3">
                <td width="21">&nbsp;</td>
                <td width="729" bgcolor="#8DC0E3" class="linksnegros">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
        </table>
        <table width="963px" style="border:0px solid #333333;" align="center" class="texttablas">
            <tr bgcolor="#e6e6e6" class="texttitulo" >
                <td width="80%" bgcolor="#e6e6e6" style="border-bottom:1px solid #333333;">Documentos de Metadatos Ambientales</td>
                <td width="20%" style="border-bottom:1px solid #333333;"><div align="center">Tama&ntilde;o</div>
                </td>
            </tr>
            <tr>
                <td height="33"><a href="docs/estandar_metadatos.pdf">EST&Aacute;NDAR PARA
                        LA DOCUMENTACI&Oacute;N DE METADATOS DE CONJUNTOS DE DATOS<br/>
                        RELACIONADOS CON BIODIVERSIDAD</a></td>
                <td><div align="center">328Kb</div>
                </td>
            </tr>
            <tr>
                <td><a href="docs/Identificaciondeconjuntos.pdf">Pasos para la identificaci&oacute;n
                        de conjuntos de datos</a></td>
                <td><div align="center">22Kb</div>
                </td>
            </tr> 
            <tr>
                <td><a href="http://cinto.invemar.org.co/cassia/" target="_blank">Ingreso antiguo CASSIA.(Repositorio de metadatos).</a></td>
                <td></div>
                </td>
            </tr>    
        </table>  

        <%@ include file="plantillaSitio/footer.jsp" %>

    </body>
</html>
