<%@ page contentType="application/jsonrequest" %>
<%@ page import="org.json.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.net.*" %>
<%@ page import="javax.sql.*" %>
<%@ page import="javax.naming.*" %>
<%@ page import="org.apache.commons.dbutils.*" %>
<%@ page import="org.apache.commons.lang.*" %>
<%@ page import="org.apache.commons.dbutils.handlers.*" %>
<%!
/*
Class DB
Clase para ejecutar las consultas SQL.
Utiliza la API dbUtils de Jakarta Commons
http://jakarta.apache.org/commons/dbutils
El metodo query:
  Ejecuta una consulta SELECT a la Base de Datos.
  Parametros:
  String query: La consulta SQL.
  Object[] params (opcional): los parámetros para la consulta SQL.
  ResultSetHandler rsh: Objeto manejador de los resultados de la consulta.
  boolean json (opcional - por defecto es true): Si el resultado debe ser devuelto en formato JSON
  Valor de retorno:
  El objeto devuelto por el rsh en formato JSON o no, dependiendo del parametro json.
*/
class DB {
  QueryRunner db;
  Object query(String query, ResultSetHandler rsh) throws SQLException {
    return query(query, null, rsh, true);
  }
  Object query(String query, ResultSetHandler rsh, boolean json) throws SQLException {
    return query(query, null, rsh, json);
  }
  Object query(String query, Object[] params, ResultSetHandler rsh) throws SQLException {
    return query(query, params, rsh, true);
  }
  Object query(String query, Object[] params, ResultSetHandler rsh, boolean json) throws SQLException {
    Object res=null;
//    try{
    	res = db.query(query, params, rsh);
//    }catch(SQLException e){
//    	throw new SQLException("Error Interno en la aplicacion");
    
//    }
    return json? res instanceof List? new JSONArray((List)res): res instanceof String? JSONObject.quote((String)res): res instanceof Map? new JSONObject((Map)res): res: res;
  }
}

private static String escape(Object value) {
  if (value == null) return "";
  return StringEscapeUtils.escapeHtml(value.toString());
}

/*
Inicializa los objetos de conexion a la Base de Datos
*/
public void jspInit() {
  try {
    
    Context envContext  = (Context)new InitialContext().lookup("java:/comp/env");
    sibmDS = (DataSource)envContext.lookup("jdbc/sibm");
    sibm.db = new QueryRunner(sibmDS);
    sismacDS = (DataSource)sibmDS;//(DataSource)envContext.lookup("jdbc/sismac");
    sismac.db = new QueryRunner(sismacDS);
    
  }
  catch (Exception e) {
    e.printStackTrace();
  }
}

public void jspDestroy() {
  sismac.db = null;
  sibm.db = null;
  sismacDS = null;
  sibmDS = null;
}

/*
Devuelve el parametro name del request, o en su defecto def
*/
String param(String name, String def) {
  HttpServletRequest request = (HttpServletRequest)local.get();
  return StringUtils.defaultIfEmpty(request.getParameter(name), def);
}

/*
Igual a la anterior, pero el valor lo devuelve como un numero entero
*/
int intParam(String name, int def) {
  return Integer.parseInt(param(name, String.valueOf(def)));
}

/*
Modifica el String SQL para que devuelva solo una fraccion de los registros
*/
String limit(String query, int begin, int length) {
  return "SELECT * FROM (SELECT a.*, ROWNUM AS LIMIT FROM (" + query + ") a) WHERE LIMIT BETWEEN " + begin + " AND " + (begin + length - 1);
}

/*
Modifica el String SQL para que devuelva el numero total de registros de la consulta
*/
String count(String query) {
  return "SELECT COUNT(*) FROM (" + query + ")";
}

/*
Objeto para poder obtener el request de una peticion sin necesidad de tener que pasarlo como parametro
*/
ThreadLocal local = new ThreadLocal();

/* Conexiones a las Base de Datos del SISMAC y del SIBM */
DB sismac = new DB();
DB sibm = new DB();

/* Datasource de las conexiones anteriores */
DataSource sismacDS;
DataSource sibmDS;

/* Longitud por defecto de la cantidad de registros que deben retornarse */
int defaultLength = 10;

/*
Manejador de ResultSet usados por los metodos query de la Clase DB
*/

/* Para retornar todos los registros de una consulta como un List de JSONObject */
ResultSetHandler all = new BeanListHandler(null, new BasicRowProcessor() {
  public Object toBean(ResultSet rs, Class type) throws SQLException {
    return new JSONObject(toMap(rs));
  }
});

/* Retorna el primer registro de la consulta como un Map */
ResultSetHandler row = new MapHandler();

/* Retorna toda la primera columna de la consulta como un List */
ResultSetHandler col = new ColumnListHandler();

/* Retorna la primera columna del primer registro de la consulta. El tipo depende del tipo de la columna en la Base de Datos */
ResultSetHandler one = new ScalarHandler();

/* Retorna un Map donde las claves es la primerra columna y la segunda el valor */
ResultSetHandler assoc = new KeyedHandler() {
  protected Object createRow(ResultSet rs) throws SQLException {
    return rs.getMetaData().getColumnCount() == 2? rs.getObject(2): new JSONObject((Map)super.createRow(rs));
  }
};

/* Igual a la anterior, excepto que los valores son un List de todos los valores de la segunda columna */
ResultSetHandler group = new ResultSetHandler() {
  public Object handle(ResultSet rs) throws SQLException {
    final HashMap groups = new HashMap();
    return new KeyedHandler() {
      protected Object createRow(ResultSet rs) throws SQLException {
        Object key = createKey(rs);
        JSONArray array = (JSONArray)groups.get(key);
        if (array == null) {
          array = new JSONArray();
          groups.put(key, array);
        }
        array.put(rs.getMetaData().getColumnCount() == 2? rs.getObject(2): new JSONObject((Map)super.createRow(rs)));
        return array;
      }
    }.handle(rs);
  }
};
%>
<%
Object json = null;
local.set(request);

if(null!=request.getHeader("Content-Type")){
	if ("application/jsonrequest".equals(request.getHeader("Content-Type")) || request.getHeader("Content-Type").startsWith("application/jsonrequest")) {
	  BufferedReader reader = request.getReader();
	  StringBuffer buffer = new StringBuffer();
	  String line;
	  while ((line = reader.readLine()) != null) buffer.append(line);
	  json = new JSONTokener(buffer.toString()).nextValue();
	}
}
%>
