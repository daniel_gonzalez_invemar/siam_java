<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
<title>Mapa del Sitio SIAM</title>
<meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1" />
<link href="plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css"/>
 <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
 <script type="text/javascript" src="plantillaSitio/js/monitoreo.js"></script>
</head>
<body>
<jsp:include page="plantillaSitio/headerv2.jsp?idsitio=10"/>
<div class="centrado">
<table width="1001" style="border:0;padding:0">
  <tr>
    <td>Mapa del Sitio:</td>
  </tr>
  <tr>
    <td>
    <ul>
    <li>SIAM-Sistema de Informacion Marino de Colombia
      
      <ul type="circle">
        <li>Biodiversidad Marina
          <ul type="disc">
            <li><a href="http://siam.invemar.org.co/siam/sibm/index.jsp?idsitio=6&amp;idsubsitio=1">SIBM- Sistema de Informaci&oacute;n sobre Biodirvesidad Marina de Colombia.</a></li>
            <li><a href="http://siam.invemar.org.co/siam/sismac/index.jsp?idsitio=1">SISMAC-Sistema al soporte de Areas Coralinas de Colombia.</a></li>
            <li><a href="http://cinto.invemar.org.co/invasoresmarinos/">Especies Marinas Introducidas-Pez Le&oacute;n.</a></li>
            <li><a href="http://siam.invemar.org.co/siam/ccbm/ccbm_consultaPV1.jsp">Cepario Colombiano de Bacterias Marinas.</a></li>
          </ul>
        </li>            
       </ul>
       
       
        <ul type="circle">
           <li>Manejo Integrado de Zonas Costeras
             <ul type="disc">
               <li><a href="http://siam.invemar.org.co/siam/searcharticulo.jsp?idsitio=2&amp;action=search&amp;text1=mizc&amp;text2=gez&amp;operator=or">Politicas, planes, programas y publicaciones científicas relacionadas con MIZC</a></li>
             </ul>
           </li>
        </ul>
        
         <ul type="circle">
            <li>Uso de los Recursos Pesqueros
              <ul type="disc">
                <li><a href="http://siam.invemar.org.co/siam/sipein/index.jsp">SIPEIN-Sistema de información Pesquera sobre la Cienaga Grande Santa Marta.</a></li>
                <li><a href="http://gis.invemar.org.co/anh_caladerospesca/">Geovisor ANH-Caladeros Pesca.</a></li>
            </ul>
            </li>
       </ul>
       
       <ul type="circle">
        <li>Monitoreo de los Ambientes Marinos
 			<ul type="disc">
  				<li><a href="http://siam.invemar.org.co/siam/redcam/index.jsp">RED-CAM Red Monitoreo de la Calidad Ambiental. </a></li>
                <li><a href="http://cinto.invemar.org.co/argos/login.jsp">ARGOS-Sistema de Soporte Multitemático para el monitoreo Ambiental.</a></li>
                <li><a href="http://siam.invemar.org.co/siam/sismac/index.jsp">SISMAC. Sistema  de Soporte de Áreas Coralinas.</a></li>
                <li><a href="http://cambioclimatico.invemar.org.co/sistema-de-consulta-de-informacion-ambiental-goos">Sistema de Información Global de los Oc&eacute;anos.   </a></li>
            </ul>
         </li>   
       </ul>    
       
      <ul type="circle">   
     <li>Servicios de Información Documental
        <ul type="disc">
         	<li><a href="http://siam.invemar.org.co/siam/metadatosPV1.jsp">CASSIA. Metadatos de Cojuntos de Datos digitales,Imagénes provenientes de sensores remotos y cartográfia disponible.</a></li>
        	<li><a href="http://siam.invemar.org.co/siam/biblioteca/CDDIECL.jsp">CEDIECL. Centro de Documentación "Ivan Enrique Caycedo Lara".</a></li>
       		<li><a href="http://www.invemar.org.co/boletin/index.jsp">Boletín de Investigaciones Marinas y Costeras.</a></li>
            <li><a href="http://siam.invemar.org.co/siam/searcharticulo.jsp?idsitio=2&action=search&text1=mizc&text2=gez&operator=or">Buscador de Conocimiento Ambiental del SIAM.</a></li>
            <li><a href="http://siam.invemar.org.co/siam/tutoriales.jsp">Tutoriales</a></li>
            <li><a href="http://siam.invemar.org.co/siam/tesauro_ambiental/naveg.htm">Diccionario Ambiental.</a></li>
            <li><a href="http://cinto.invemar.org.co:8081/geonetwork/srv/en/main.home">Sistema de Administración Documental.</a></li>
        </ul>
      </li>
      </ul>
       
       <ul type="circle">
       <li>
          Servicios de Información Geográfica
  			<ul type="disc">
  			  <li><a href="http://siam.invemar.org.co/siam/geoportal/index.jsp">Geoportal SIAM.</a></li>
			  <li><a href="http://cinto.invemar.org.co/metabuscador/searchPV1.jsp?idsitio=5&amp;idsubsitio=1&amp;dirSearch=metINVEMAR">CASSIA.</a></li>
			  <li><a href="http://www.invemar.org.co/noticias.jsp?id=2570&amp;idcat=105">Mapa Calidad Sanitaria de las Playas.</a></li>
			  <li><a href="http://www.invemar.org.co/noticias.jsp?id=2387&amp;idcat=105">Mapa de la Calidad de Aguas Marinas  y Costeras de Colombia.</a></li>
			  <li><a href="http://gis.invemar.org.co/PERCaribe/">Mapa para  la planificación Ecoregional del Caribe.</a></li>
			  <li><a href="http://gis.invemar.org.co/PERPacifico/">Mapa para la planificación Ecoregional Pacífico.</a></li>
			  <li><a href="http://gis.invemar.org.co/anh_caladerospesca/">ANH-Caladeros Pesca.</a></li>
           </ul>
       </li>
       </ul>
       
       <ul type="circle">
        <li>Amenazas Naturales
			<ul type="disc">
		      <li><a href="http://www.invemar.org.co/cambioclimatico/pcategorias.jsp?idcat=145">Investigación sobre cámbio climático de los mares y costas de colombia.</a></li>
 			  <li><a href="http://siam.invemar.org.co/siam/searcharticulo.jsp?action=search&amp;text1=erosion">Documentación sobre erosión costera en Colombia.</a></li>
          </ul>
        </li>           
       </ul>
        
          <ul type="circle">
          <li>Portales Temáticos
            <ul type="disc">
       			 <li><a href="http://www.invemar.org.co/cambioclimatico/pcategorias.jsp?idcat=145">Porta Cámbio climático de los mares y costas de Colombia.</a></li>
				 <li><a href="http://cinto.invemar.org.co/rla7012/">Aplicación de técnicas nucleares en la solución de problemas específicos del MIZC en el Caribe.</a></li>
				 <li><a href="http://cinto.invemar.org.co/anh/">ANH-INVEMAR - Áreas significativas para la biodiversidad marina.</a></li>
				 <li><a href="http://www.coastman.net.co/">COASTMAN - Red Global para el manejo integrado de zonas costeras.</a></li>
				 <li><a href="http://www.invemar.org.co/pcategorias.jsp?idcat=121">Red Costera. Red Nacional de Menejo Integrado de Zonas Costeras.</a></li>
            </ul>
          </li>
          </ul>       
       </li>   
        <li><a href="http://siam.invemar.org.co/siam/mapadelsitio.jsp">Mapa del Sitio.</a></li>
        <li><a href="http://siam.invemar.org.co/siam/contactenos.jsp?idsitio=10">Contactenos.</a></li>
        <li><a href="http://www.invemar.org.co/pciudadania.jsp">Servicio al ciudadano.</a></li>
      </ul> 
    </td>
  </tr>
 <tr>
 <td><%@ include file="plantillaSitio/footer.jsp" %></td>
 </tr>
</table>
</div>
<h1 style="display:none"></h1>
</body>
</html>
          
