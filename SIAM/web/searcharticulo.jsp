<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
    <head>
      <!-- base href="<%=basePath%>" -->

        <title>.::Sistema de Informaci&oacute;n Ambiental Marina de Colombia - SIAM::.</title>

        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="0">    
        <meta http-equiv="keywords" content="Metabuscador del SIAM">
        <meta http-equiv="description" content="Metabuscador de conocimiento ambiental">
        <script type="text/javascript" src="./1ibre/prototype.js"></script>
        <script type="text/javascript" src="js/search.js"></script>
        <link href="plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css">

    <style TYPE="text/css" >

        body  {font-size:60%; }
        .headertexto {
            font-family: "Trebuchet MS";
            color: white;
            font-size: 70%;
            text-align: left;
            font-weight: bold;
            text-decoration: none;
        }
        .textfooter {
            text-align: right;
            color: #004E76;
            text-transform: uppercase;
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 80%;
            font-weight: bold;
            padding-right: 50px;
        }

    </style>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19778914-33']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script></head>
    <body>
        <jsp:include page="plantillaSitio/headerv2.jsp?idsitio=260"/>
        <form method="get" action="" id="formarte" name="formarte" style="margin: 0px" onSubmit="return doSearch()">
            <table width="963px" align="center" style="border:0px;padding:0px;border-spacing:0px">
                <tr>
                    <td width="375"><table style="border:0px;padding:0px;border-spacing:0px" width="200" >
                            <tr>
                                <td width="2"><img src="images/orange_box_r1_c1.gif" width="2" height="2" alt='orange box' ></td>
                                <td width="10"><img src="images/orange_box_r1_c3.gif" width="2" height="2" alt="box" ></td>
                            </tr>
                            <tr>        
                                <td width="196" style="text-align:center;vertical-align:top"><table width="371" align="center" style="border:0px;padding:0px;border-spacing:0px">
                                        <tr>
                                            <td width="254" class="titulosnegros">Ingrese palabras claves</td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" id="searchtext1" size="40" title="Palabra1"></td>
                                            <td width="110">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><div style="text-align:center">
                                                    <select name="operator" id="operator" title="Operador">
                                                        <option value="AND"> Y </option>
                                                        <option value="OR"> O </option>
                                                    </select>
                                                </div></td>
                                            <td width="110">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" id="searchtext2" size="40" title="Palabra2" ></td>
                                            <td width="110"><input name="button" type="button" onClick="doSearch();" value="Search"></td>
                                        </tr>
                                    </table></td>        
                            </tr>
                            <tr>       
                                <td width="10"><img src="images/orange_box_r3_c3.gif" width="2" height="2" alt='orange box'></td>
                            </tr>
                        </table></td>
                </tr>
            </table>
            <table width="750" align="center" style="border:0px;padding:1px;border-spacing:1px">  
                <tr>
                    <td>
                        <table align="center" style="border:0px;border-spacing:2px" width="963px">     
                            <tr bgcolor="#B5D7DE">
                                <td colspan="2" class="texttablas" style="text-align:left">&Uacute;ltimos documentos </td>
                            </tr>
                            <tr>
                                <td width="1">&nbsp;</td>
                                <td class="texttablas" style="text-align:left; font-size:0.83em" ><ul id="lastdocs"></ul></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>	
        </form>


        <table width="963px" align="center" style="border:0px;padding:0px;border-spacing:0px" >
            <tr>
                <td><div id="showrecord" style="text-align:right;display:none;" class="linksnegros"><span id="record" style="color: #cc3300;font-weight: bold; "> </span> de <span id="totalrecord" style="color: #cc3300;font-weight: bold;"> </span> registros</div></td>
            </tr>
            <tr>
                <td>
                    <table id="tdatas"  style="display:none;border:0px;border-spacing:2px"  align="center" width="963px">
                        <thead>
                            <tr bgcolor="#B5D7DE" >
                                <td width="289" class="titulostablas"><span ><a href="#" id="sortTitulo" onClick="javascript:sorted();" class="titulostablas" style="text-decoration:none;"> ::Titulo:: </a></span><br></td>
                                <td width="205" class="titulostablas" style="display:none">::Categor&iacute;a::<br/></td>
                                <td width="242" class="titulostablas">::Palablas Claves::<br/></td>
                            </tr>
                        </thead>      
                        <tbody id="bdatas"></tbody>
                    </table></td>
            </tr>
            <tr>
                <td >&nbsp;

                </td>
            </tr>
            <tr>
                <td >&nbsp;</td>
            </tr>
        </table>
        <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
        </script>
        <%@ include file="plantillaSitio/footer.jsp" %>	
    </body><h1></h1>
</html>
