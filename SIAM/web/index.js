/*function showLoadingPane() {
  Element.show("loadingPane");
}
function hideLoadingPane() {
  Element.hide("loadingPane");
}*/
Event.observe(window, "load", function () {
  document.body.onprogress = function (event) {
    var node = $("loadingPane");
    if (!node) {
      new Insertion.Top(document.body, "<table style='font-size: 12px; width: 100%; height: 100%; background-image: url(/invemar/img/fondo-negro-50x50.gif); position: absolute;' id='loadingPane'><tr><td align='center'><table style='width: 250px; height: 10px; background-color: white; border: 2px solid black' cellspacing='8' cellpadding='0'><tr id='loadingPaneValue'><td style='background-color: #eee'><div style='background-color: #B5D7DE; height: 100%; margin: 0px'><img width='1'></div></td><td width='10'></td></tr></table></td></tr></table>");
      node = document.body.firstChild;
    }
    var cells = $("loadingPaneValue").cells;
    cells[0].firstChild.style.width = event.progress + "%";
    setInnerText(cells[1], Math.round(event.progress) + "%");
    if (event.progress == 100) ibreSetTimeout(Element.hide, 400, node);
    else if (event.progress == 0) Element.show(node);
  }
});
/*function testValue(value, select, list) {
  select = $(select);
  return !select.selectedIndex || list[value] && list[value].contains(select.value);
}
function testSelect(value, select, list) {
  select = $(select);
  return !select.selectedIndex || list[select.value] && list[select.value].contains(value);
}*/