<?php
require_once "1ibre/json/lib.php";
//$debug = TRUE;
if (!isset($_DEFS)) require_once "defs.php";
header("Content-Type: text/html; charset=ISO-8859-1");
session_start();
$user_id = $_SESSION["user_id"];
/*if ($user_id) set_user(ibre_get_object_by_id("User", $user_id));
else $user = NULL;*/
if ($_DEFS["User"]) $user = $user_id? ibre_get_object_by_id("User", $user_id/*, NULL, TRUE*/): NULL;
define("DATE_ONLY", "Y-m-d");
function check_user() {
  if (!$GLOBALS["user"]) exit;
}
function addstrslashes($str) {
  return addcslashes($str, "\\\"'\n\r\t");
}
function php_to_js($array) {
  echo "{";
  $first = TRUE;
  foreach ($array as $id => $value) {
    if ($id{0} == "_") continue;
    if ($first) $first = FALSE;
    else echo ", ";
    echo "'$id': ";
    if (is_array($value)) echo php_to_js($value);
    elseif (is_numeric($value)) echo $value;
    else echo "'$value'";
  }
  echo "}";
}
function add_entities($str) {
  for ($i = 128; $i < 255; $i++)
    $str = str_replace(chr($i), "&#$i;", $str);
  return $str;
}
/*function set_user($obj) {
  global $user;
  $user = $obj;
  if ($user["_TYPE"]["name"] == "Person") {
    $user["name"] = "$user[firstname] $user[lastname]";
    $user["is_person"] = TRUE;
  }
  else $user["is_enterprise"] = TRUE;
  ibre_get_fields($user, array("address" => array("city" => array("state" => "country"))));
  unset($user["password"]);
}*/
function echo_r($value, $level = 3, $spaces = 0) {
  if ($level <= 0) {
    if (is_object($value)) echo "object\n";
    else echo "$value\n";
    return;
  }
  $pad = str_pad("", $spaces);
  if (is_array($value) || is_object($value)) {
    echo "Array\n$pad{\n";
    foreach ($value as $key => $item) {
      echo "$pad  [$key] => ";
      echo_r($item, $level - 1, $spaces + 4);
    }
    echo "$pad}\n";
  }
  else echo "$value\n";
}
function array_by_key($array, $key) {
  $res = array();
  foreach ($array as $value) $res[] = $value[$key];
  return $res;
}
function item_by_key($array, $key, $value) {
  foreach ($array as $item) if ($item[$key] == $value) return $item;
  return NULL;
}
function index_by_key($array, $key, $value) {
  foreach ($array as $i => $item) if ($item[$key] == $value) return $i;
  return NULL;
}
function date_to_timestamp($date) {
  $date = explode("-", $date);
  return mktime(0, 0, 0, $date[1], $date[2], $date[0]);
}
?>