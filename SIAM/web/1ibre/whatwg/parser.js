document._context = {
  childs: [],
  getVars: function () { return {}; },
  parsed: true
};
var _numParseHTMLChildNodes = 0;
var _numWF2HTMLChildNodes = 0;
ibre.datasource._eval = function (context, expr) {
  var vars = {};
  var nodes = ibre.datasource.nodes;
  for (var j = 0; j < nodes.length; j++) {
    var name = nodes[j]._name;
    if (name) vars[name] = nodes[j].datasource;
  }
  return ibre.datasource._nativeEval($H(vars).merge(context.parentContext.getVars()), expr);
};
ibre.datasource._nativeEval = function () {
  with (arguments[0]) {
    return eval(arguments[1]);
  }
};
ibre.datasource._parseString = function (node, property, parent) {
   var propertyValue = node[property];
  if (ie && node.nodeType != 3) {
    if (parent.tagName == "IMG" && node.name == "src") propertyValue = unescape(propertyValue);
  }
  var tokens = propertyValue.toString().split("`");

  if (tokens.length > 1) {
    parentContext = ibre.datasource._getContext(parent);
    parentContext.childs.push({
      node: node,
      parent: parent,
      property: property,
      tokens: tokens,
      parentContext: parentContext,
      toString: function () {
        return "value-of '" + this.tokens.toString() + "'";
      },
      parse: function () {
        var value = "";
        for (var i = 0; i < this.tokens.length; i++) {
          var token = this.tokens[i];
          if (i % 2) {
            try {
              //if (token.contains("info")) alert(token);
              var val = ibre.datasource._eval(this, token);
              if (val != null) value += val;
            }
            catch (e) {
              value += e.message;
            }
          }
          else value += token;
        }
        if (this.node.nodeType == 3) {
          var parent = Element.parentElement(this.node);
          if (parent) {
            if (parent.tagName == "OPTION" && Element.parentElement(parent))
              parent = Element.parentElement(parent);
            var style = parent.style;
            var display = style.display;
            style.display = "none";
            this.node.data = value;
            style.display = display;
          }
          else this.node.data = value;
        }
        else if (this.parent[this.node.name] !== undefined) this.parent[this.node.name] = value;
        else this.parent.setAttribute(this.node.name, value);
        this.parsed = true;
      }
    });
    if (node.nodeType == 3) node.data = "...";
    else if (parent[node.name] !== undefined) parent[node.name] = "...";
    else parent.setAttribute(node.name, "...");
	}
};
ibre.datasource.update = function () {
  ibre.datasource._parseContext(document._context);
}
ibre.datasource._parseContext = function (context) {
  context.parsed = false;
  runFor(function () {
    this.parsed = false;
    this.parse();
    return function () {
      return this.parsed;
    }.bind(this);
  }, context.childs, null, function (context) { context.parsed = true; }, [context]);
};
ibre.datasource._getContext = function (node) {
  while (!node._context) node = node.parentNode;
  return node._context;
};
function wf2ParseHTMLChilds(node, showing, sync) {
  var list = Element.children(node);
  _numWF2HTMLChildNodes += list.length;
  if (sync) {
    for (var i = 0; i < list.length; i++) {
      var node = list[i];
      wf2ParseHTML(node, showing, true);
      //else ibreSetTimeout(wf2ParseHTML, 0, node, showing, sync);
    }
  }
  else runFor(function () {
    wf2ParseHTML(this, showing);
  }, list);
}
function ibreParseHTMLChilds(node, sync) {
  var list = node.childNodes;
  var length = list.length;
  for (var i = 0; i < length; i++) {
    var node = list[i];
    switch (node.nodeType) {
      case 1:
        _numParseHTMLChildNodes++;
        if (sync) ibreParseHTML(node, true);
        else ibreSetTimeout(ibreParseHTML, 1, node);
        //ibreParseHTML(node);
        break;
      case 3:
        ibre.datasource._parseString(node, "data", node.parentNode);
        break;
    }
  }
}
function ibreParseHTML(node, sync) {
  if (_numParseHTMLChildNodes) _numParseHTMLChildNodes--;
  var forEach = node.getAttribute("for-each");
  if (forEach != null) {
    if (node._context) return;
    node._item = node.getAttribute("item");
    var parentContext = ibre.datasource._getContext(node);
    var text = document.createTextNode("");
    domReplaceNode(text, node);
    node._context = {
      node: node,
      referenceNode: text,
      onlyOnce: node.getAttribute("only-once") != null,
      nodes: {},
      items: null,
      forEach: forEach,
      index: node.getAttribute("key"),
      parentContext: parentContext,
      toString: function () {
        return "for-each '" + this.forEach + "'";
      },
      parse: function () {
        if (this.onlyOnce && this.noFirst) {
          this.parsed = true;
          return;
        }
        if (this.previousHTML != null) this.node.innerHTML = this.previousHTML;
        this.noFirst = true;
        var oldItems = this.items;
        try {
          this.items = ibre.datasource._eval(this, this.forEach);
          this.previousHTML = null;
        }
        catch (e) {
          this.previousHTML = this.node.innerHTML;
          setInnerText(this.node, e.message);
          this.parsed = true;
          return;
        }
        var oldNodes = this.nodes;
        this.nodes = {};
        this.numContexts = 0;
        for (var key in this.items) {
          if (typeof this.items[key] == "function") continue;
          var node;
          if (!oldItems || !oldNodes[key] || !equals(this.items[key], oldItems[key])) {
            node = this.node.cloneNode(true);
            this.nodes[key] = node;
            node.removeAttribute("for-each");
            domInsertBefore(node, this.referenceNode);
            node._context = {
              context: this,
              node: node,
              index: key,
              childs: [],
              getVars: function () {
                var obj = {};
                obj[this.context.node._item] = this.context.items[this.index];
                if (this.context.index) obj[this.context.index] = this.index;
                return $H(obj).merge(this.context.parentContext.getVars());
              }
            };
            runWhen(function (node) {
              ibreParseHTML(node);
              runWhen(function (node) {
                ibre.datasource._parseContext(node._context);
                runWhile(function (node) {
                  if (node._context.parsed) {
                    if (ibre.parsed) wf2ParseHTML(node);
                    node._context.context.numContexts--;
                    return true;
                  }
                }, [node]);
              }, "!_numParseHTMLChildNodes", [node]);
            }, "!_numParseHTMLChildNodes", [node]);
          }
          else {
            node = oldNodes[key];
            delete oldNodes[key];
            ibre.datasource._parseContext(node._context);
            runWhile(function (node) {
              if (node._context.parsed) {
                if (ibre.parsed) wf2ParseHTML(node, true);
                node._context.context.numContexts--;
                return true;
              }
            }, [node]);
          }
          this.numContexts++;
          this.nodes[key] = node;
        }
        for (var i in oldNodes)
          if (oldNodes[i].nodeType == 1)
            Element.remove(oldNodes[i]);
        runWhile(function (context) {
          if (!context.numContexts) {
            context.parsed = true;
            return true;
          }
        }, [this]);
      }
    };
    parentContext.childs.push(node._context);
    return;
  }
  var test = node.getAttribute("if");
  if (test != null) {
    if (node._context) return;
    var parentContext = ibre.datasource._getContext(node);
    Element.hide(node);
    node._context = {
      node: node,
      parentContext: parentContext,
      test: test,
      childs: [],
      getVars: function () {
        return this.parentContext.getVars();
      },
      toString: function () {
        return "if '" + this.test + "'";
      },
      parse: function () {
        var result;
        try {
          result = ibre.datasource._eval(this, this.test);
          //alert(this.test + "\n" + result);
        }
        catch (e) {
          Element.show(this.node);
          setInnerText(this.node, e.message);
          this.parsed = true;
          return;
        }
        if (result) {
          if (this.elseContext) Element.hide(this.elseContext.node);
          Element.show(this.node);
          ibre.datasource._parseContext(this.node._context);
          runWhile(function (context) {
            if (context.node._context.parsed) {
              context.parsed = true;
              if (ibre.parsed) wf2ParseHTML(context.node, true);
              return true;
            }
          }, [this]);
        }
        else {
          Element.hide(node);
          if (this.elseContext) {
            Element.show(this.elseContext.node);
            ibre.datasource._parseContext(this.elseContext);
            runWhile(function (context) {
              if (context.elseContext.parsed) {
                context.parsed = true;
                if (ibre.parsed) wf2ParseHTML(context.node, true);
                return true;
              }
            }, [this]);
          }
          else this.parsed = true;
        }
      }
    };
    parentContext.childs.push(node._context);
  }
  if (node.getAttribute("else") != null) {
    if (node._context) return;
    var test = Element.previousElement(node);
    if (!test || !test.getAttributeNode("if")) {
      setInnerText(node, langStrings.missingIf);
      return
    }
    Element.hide(node);
    test._context.elseContext = node._context = {
      node: node,
      parentContext: test._context.parentContext,
      childs: [],
      getVars: function () {
        return this.parentContext.getVars();
      }
    };
  }
  var attrs = node.attributes;
  for (var i = 0; i < attrs.length; i++)
    ibre.datasource._parseString(attrs[i], "value", node);
  ibreParseHTMLChilds(node, sync);
}
function wf2ParseHTML(node, showing, sync) {
  _wf2ParseHTML(node, showing);
  wf2ParseHTMLChilds(node, showing, sync);
}
function _wf2ParseHTML(node, showing) {
  if (_numWF2HTMLChildNodes) _numWF2HTMLChildNodes--;
  if (/*node._ignore || */isInRepititionBlock(node)) return;
  function updateIndexRepititionBlock(node, id, index) {
    if (node.nodeType != 1) return;
    var attrs = node.attributes;
    for (var i = 0; i < attrs.length; i++) {
      var attr = attrs[i];
      var value = attr.value.toString();
      if (value.contains("[" + id + "]")) {
        var antes = value;
        value = value.replace(new RegExp("\\[" + id + "\\]", "g"), index);
        if (attr.name.startsWith("on"))
          eval("node[attr.name] = function () { with (this) { " + value + " } }");
        else if (node[attr.name] != null) node[attr.name] = value;
        else attr.value = value;
        //alert(node.tagName+" "+attr.name+"\nantes="+antes+"\ndespues="+attr.value+"\nattr="+node.getAttribute(attr.name));
      }
    }
    var children = Element.children(node);
    for (var i = 0; i < children.length; i++) updateIndexRepititionBlock(children[i], id, index);
  }
  function addRepetitionBlock(refNode) {
    var node = this.cloneNode(true);
    node.setAttribute("repeat", "");
    if (node.addRepetitionBlock) node.addRepetitionBlock = null;
    var id = node.id;
    node.removeRepetitionBlock = removeRepetitionBlock;
    domShowNode(node);
    node.repetitionTemplate = this;
    node.repetitionIndex = this.repetitionIndex;
    this.repetitionIndex++;
    node.wf2RepetitionIndex = this.repetitionBlocks.length;
    this.repetitionBlocks.push(node);
    if (id) {
      node.removeAttribute("id");
      updateIndexRepititionBlock(node, id, node.repetitionIndex);
    }
		//if (this.parentNode)
    domInsertBefore(node, this);
		//else $(this.id).parentNode.insertBefore(node, $(this.id));
    wf2ParseHTML(node, false, true);
		//alertObj(node);
    //var form = Element.up(node, "form");
		var form = null;
		var parent = Element.parentElement(node);
		while (parent) {
			if (parent.tagName == "FORM") {
				form = parent;
				break;
			}
			parent = Element.parentElement(parent);
		}
    if (form) {
      var elements = form.formEventElements;
      var inputs = $A(node.getElementsByTagName("input"));
      var textareas = $A(node.getElementsByTagName("textarea"));
      var selects = $A(node.getElementsByTagName("select"));
      var newElements = inputs.concat(textareas).concat(selects);
      for (var i = 0; i < elements.length; i++)
        _whatwgApplyFormEvents(elements[i], newElements);
    }
    return node;
  }
  function removeRepetitionBlocks() {
    var list = this.repetitionBlocks;
    while (list.length) list[list.length - 1].removeRepetitionBlock();
  }
  function removeRepetitionBlock() {
    domRemoveNode(this);
    var repetitionBlocks = this.repetitionTemplate.repetitionBlocks;
    var last = repetitionBlocks.pop();
    if (this.wf2RepetitionIndex < repetitionBlocks.length) {
      repetitionBlocks[this.wf2RepetitionIndex] = last;
      last.wf2RepetitionIndex = this.wf2RepetitionIndex;
    }
  }
  function sortColumn() {
    var th = this;
    if (ibre._callEventHandler(th, "sort") === false) return;
    var table = th.parentNode.parentNode.parentNode;
    var datagrid = table.parentNode;
    if (th.sorted) th.reversed = !th.reversed;
    else {
      th.sorted = true;
      th.reversed = false;
    }
    datagrid.reversed = th.reversed;
    var currentColumn = datagrid.currentSortedColumn;
    if (currentColumn != th) {
      if (currentColumn) {
        currentColumn.sorted = false;
        currentColumn.reversed = false;
        Element.removeClassName(currentColumn, "sorted");
        Element.removeClassName(currentColumn, "reversed");
      }
      datagrid.currentSortedColumn = th;
    }
    Element.addClassName(th, "sorted");
    Element[th.reversed? "addClassName": "removeClassName"](th, "reversed");
    if (datagrid.datasource && th.getAttribute("field")) {
      datagrid.field = th.getAttribute("field");
      if (datagrid.datasource.href) datagrid.datasource.loadDatasource(null, {position: datagrid.position, length: datagrid._length(), field: datagrid.field, reversed: datagrid.reversed});
      return;
    }
    function getCellValue(row) {
      var cell = row.cells[cellIndex];
      var data = cell.getAttribute("value");
      return data == null? getInnerText(cell): data;
    }
    var tBodies = table.tBodies;
    if (tBodies.length == 0 || tBodies[0].rows.length == 0)
      return;
    var cellIndex = th.cellIndex;
    var data = getInnerText(tBodies[0].rows[0].cells[cellIndex]);
    var sortColumnFunction;
    switch (th.getAttribute("type")) {
      case "number":
      case "currency":
        sortColumnFunction = function (r1, r2) {
          var d1 = parseFloat(getCellValue(r1));
          var d2 = parseFloat(getCellValue(r2));
          if (isNaN(d1)) d1 = 0;
          if (isNaN(d2)) d2 = 0;
          return th.reversed? d2 - d1: d1 - d2;
        };
        break;
      case "date":
        break;
      default:
        sortColumnFunction = function (r1, r2) {
          var d1 = getCellValue(r1).toLowerCase();
          var d2 = getCellValue(r2).toLowerCase();
          if (d1 == d2) return 0;
          if (d1 < d2) return th.reversed? 1: -1
          return th.reversed? -1: 1
        };
    }
    for (var i = 0; i < tBodies.length; i++) {
      var tBody = tBodies[i];
      var rows = tBody.rows;
      var list = [];
      var templates = {};
      var length = rows.length;
      for (var j = 0; j < length; j++)
        if (rows[j].addRepetitionBlock/* == "template"*/) templates[j] = rows[j];
        else list.push(rows[j]);
      list.sort(sortColumnFunction);
      //domRemoveChildNodes(tBody);
      var k = 0;
      for (var j = 0; j < length; j++)
        tBody.appendChild(templates[j]? templates[j]: list[k++]);
    }
  }
  function onDetails() {
    toogleShowHide(Element.childAt(this.parentNode, 1));
  }
  function switchSetActive(node) {
    if (this.activeElement)
      domHideNode(this.activeElement);
    domShowNode(node);
    this.activeElement = node;
  }
  function sectionSetActive() {
    this.parentNode.setActive(this);
  }
  function hrefOnClick() {
    var target = this.getAttribute("target");
    var href = this.getAttribute("href").strip();
    if (target != null) {
      switch (target) {
        case "_popup":
          requestPopup(href).showPopup();
          break;
        case "_top":
          top.location.href = href;
          break;
        case "_self":
          location.href = href;
          break;
        case "_parent":
          parent.location.href = href;
          break;
        case "_blank":
          open(href);
          break;
        default:
          if (window.frames[target]) document.frames[target].location.href = href;
          else {
            var node = ibre.namedNodes[target];
            if (node) {
              requestInnerHTML(href, node);
            }
            else if (href.toLowerCase().startsWith("javascript:")) eval(href.substring(11));
            else location.href = href;
          }
      }
    }
    else location.href = href;
  }
  function inputAddRepetitionBlock() {
    $(this.getAttribute("template")).addRepetitionBlock();
  }
  function inputRemoveRepetitionBlock() {
    var node = this;
    while (!node.removeRepetitionBlock) node = node.parentNode;
    node.removeRepetitionBlock();
  }
  if (!showing) {
    var whatwg = node.getAttribute("whatwg");
    if (whatwg != null) {
      switch (whatwg) {
        case "output":
          node.form = Element.up(node, "form");
          break;
        case "script":
          domHideNode(node);
          var src = node.getAttribute("src");
          eval(src != null? requestText(src): getInnerText(node));
          break;
        case "include":
          var src = node.getAttribute("src");
          if (src != null) requestInnerHTML(src, node);
          break;
        case "datalist":
          domHideNode(node);
          return;
        case "switch":
          if (node.whatwgAttr == "switch") break;
          var children = Element.children(node);
          node.activeElement = null;
          node.setActive = switchSetActive;
          for (var i = 0; i < children.length; i++) {
            var section = children[i];
            section.setActive = sectionSetActive;
            if (i == 0 || section.getAttribute("active") != null) {
              section.setActive();
            }
            else domHideNode(section);
          }
          break;
        case "details":
          var legend = Element.firstElement(node);
          if (legend.getAttribute("whatwg") != "legend") {
            legend = document.createElement("div");
            legend.innerHTML = langStrings.detailsLegend;
            legend.setAttribute("whatwg", "legend");
            node.insertBefore(legend, node.firstChild);
          }
          legend.style.cursor = "pointer";
          Event.observe(legend, "click", onDetails.bind(legend));
          var div = document.createElement("div");
          while (legend.nextSibling) div.appendChild(legend.nextSibling);
          node.appendChild(div);
          domHideNode(div);
          break;
        case "datagrid":
          var table = node.getElementsByTagName("table")[0];
          if (!table) break;
          node.tableData = table;
          var datasource = node.getAttribute("datasource");
          if (datasource) {
            node.datasource = ibre.datasource.namedNodes[datasource];
            var position = node.getAttribute("position");
            node.position = position == null? 0: parseInt(position);
            //var length = node.getAttribute("length");
            //node.length = length == null? null: parseInt(length);
            node._length = function () {
              return parseInt(this.getAttribute("length"));
            }
            node._total = function () {
              //return parseInt(this.getAttribute("total"));
              return this.datasource.datasource? this.datasource.datasource.total: 0;
            }
            node.hasPrevious = function () {
              return this.position > 0;
            }
            node.hasNext = function () {
              return this.position + this._length() < this._total();
            }
            node.nextPage = function () {
              var length = this._length();
              this.position += length;
              this.reloadDatasource();
            }
            node.previousPage = function () {
              var length = this._length();
              if (this.position > length) this.position -= length;
              else this.position = 0;
              this.reloadDatasource();
            }
            node.firstPage = function () {
              this.position = 0;
              this.reloadDatasource();
            }
            node.lastPage = function () {
              this.position = this._total() - this._length();
              if (this.position < 0) this.position = 0;
              this.reloadDatasource();
            }
            node.loadDatasource = function (href) {
              this.position = 0;
              this.datasource.loadDatasource(href, {position: this.position, length: this._length(), field: this.field, reversed: this.reversed});
            }
            node.reloadDatasource = function () {
              this.datasource.loadDatasource(null, {position: this.position, length: this._length(), field: this.field, reversed: this.reversed});
            }
          }
          if (table.tHead && table.tHead.rows.length) {
            var tHeadCells = table.tHead.rows[0].cells;
            for (var i = 0; i < tHeadCells.length; i++) {
              var cell = tHeadCells[i];
              var classes = cell.className.split(" ");
              if (classes.contains("sortable")) {
                ibre._makeEventHandler(cell, "sort");
                if (!cell.style.cursor) cell.style.cursor = "pointer";
                //wf2AddCSSStyle(cell, "sortable");
                cell.toggleSortState = sortColumn.bind(cell);
                Event.observe(cell, "click", cell.toggleSortState);
                if (classes.contains("sorted")) {
                  var reversed = classes.contains("reversed");
                  if (node.datasource) {
                    cell.sorted = true;
                    cell.reversed = reversed;
                    node.currentSortedColumn = cell;
                  }
                  else {
                    if (reversed) cell.sorted = true;
                    cell.toggleSortState();
                  }
                }
              }
            }
          }
          break;
      }
      node.whatwgAttr = whatwg;
    }
    if (!node.form) {
      var name = node.getAttribute("name");
      if (name != null) {
        ibre.namedNodes[name] = node;
        //if (!document[name]) document[name] = node; //bugs en IE8
      }
    }
    var importURI = node.getAttribute("import");
    if (importURI != null) {
      requestInnerHTML(importURI, node, true);
    }
    var href = node.getAttribute("href");
    if (href != null && node.tagName.toLowerCase() != "a" && node.tagName.toLowerCase() != "link" && !node.whatwgHRef && (!ie || node.tagName.toLowerCase() != "img" || node.src != href)) {
      node.whatwgHRef = true;
      if (!node.style.cursor) node.style.cursor = "pointer";
      if (!node.style.color) node.style.color = "blue";
      if (!node.style.textDecoration) node.style.textDecoration = "underline";
      Event.addListener(node, "click", hrefOnClick);
      Event.observe(node, "mouseover", function () {
        if (!window.status) window.status = href;
        return false;
      });
      Event.observe(node, "mouseout", function () {
        window.status = "";
        return false;
      });
    }
    var onformchange = node.getAttribute("onformchange");
    if (onformchange != null) {
      if (typeof node.onformchange != "function") {
        eval("node.onformchange = function () { with (this) { " + onformchange + " } }");
        node.form.formEventElements.push(node);
        _whatwgApplyFormEvents(node, node.form.elements);
        /*var inputs = node.form.elements;
        for (var i = 0; i < inputs.length; i++)
          if (!isInRepititionBlock(inputs[i]))
            Event.addListener(inputs[i], "change", function () {
              this.onformchange();
            }.bind(node));*/
      }
    }
    function setOnchange(node, attr, handler) {
      node.newRelevantNodes = [];
      var whatwg$ = $;
      $ = function (element) {
        var res = whatwg$(element);
        if (res && !this.relevantNodes.contains(res)) {
          this.relevantNodes.push(res);
          this.newRelevantNodes.push(res);
        }
        return res;
      }.bind(node);
      try {
        handler(eval("(function () { with (this) { return " + attr + "; } }).bind(node)()"));
      }
      catch (e) { ibre.errors.push(attr + "\n" + e.message); }
      $ = whatwg$;
      for (var i = 0; i < node.newRelevantNodes.length; i++) {
        var inputs = domGroupInputs(node.newRelevantNodes[i]);
        for (var j = 0; j < inputs.length; j++) {
          var input = inputs[j];
          Event.addListener(input, "change", function () {
            setOnchange(this, attr, handler);
          }.bind(node));
        }
      }
    }
    var visibleWhen = node.getAttribute("visibleWhen");
    if (visibleWhen != null) {
      node.relevantNodes = [];
      node._visibleParsed = false;
      node.refNode = document.createTextNode("");
      setOnchange(node, visibleWhen, function (res) {
        if (res) {
          if (!Element.parentElement(this)) {
            domReplaceNode(this, this.refNode);
            wf2ParseHTML(this, this._visibleParsed);
            if (!this._visibleParsed) this._visibleParsed = true;
          }
        }
        else {
          if (!this.refNode.parentNode) domReplaceNode(this.refNode, this);
        }
        //Element.setVisible(this, res);
      }.bind(node));
    }
    var enabledWhen = node.getAttribute("enabledWhen");
    if (enabledWhen != null) {
      node.relevantNodes = [];
      setOnchange(node, enabledWhen, function (res) {
        this.disabled = !res;
      }.bind(node));
    }
    if (node.getAttribute("required") != null)
      wf2AddCSSStyle(node, "required");
    var repeat = node.getAttribute("repeat");
    if (repeat != null) {
      if (repeat == "template") {
        domHideNode(node);
        node.addRepetitionBlock = addRepetitionBlock;
        node.removeRepetitionBlocks = removeRepetitionBlocks;
        if (!node.repetitionBlocks) {
          node.repetitionBlocks = [];
          node.repetitionIndex = 1;
        }
      }
      else {
        var repeatIndex = parseInt(repeat);
        if (!isNaN(repeatIndex)) {
          var nextSibling = node.nextSibling;
          while (nextSibling) {
            if (nextSibling.nodeType == 1 && nextSibling.getAttribute("repeat") == "template") {
              nextSibling.repetitionIndex = repeatIndex + 1;
              if (!nextSibling.repetitionBlocks) nextSibling.repetitionBlocks = [];
              node.removeRepetitionBlock = removeRepetitionBlock;
              node.repetitionTemplate = nextSibling;
              node.repetitionIndex = repeatIndex;
              if ("number" != typeof node.wf2RepetitionIndex) {
                node.wf2RepetitionIndex = nextSibling.repetitionBlocks.length;
                nextSibling.repetitionBlocks.push(node);
              }
              break;
            }
            nextSibling = nextSibling.nextSibling;
          }
        }
      }
    }
    switch (node.tagName.toLowerCase()) {
      case "form":
        if (node.wf2OnSubmit) break;
        node.elementLabels = node.getAttribute("elementLabels");
        node.elementNames = node.getAttribute("elementNames");
        node.prefilled = node.getAttribute("prefilled") != null;
        node.formEventElements = [];
        node.wf2OnSubmit = node.onsubmit;
        ibre._makeEventHandler(node, "received");
        node.whatwgSubmit = function () {
          var target = this.target;
          var hasTarget = false;
          if (target) {
            switch (target) {
              case "_popup":
              case "_top":
              case "_self":
              case "_parent":
              case "_blank":
                break;
              default:
                if (window.frames[target]) break;
                if (ibre.namedNodes[target]) hasTarget = true;
            }
          }
          if (this.onreceived || this.getAttribute("datasource") != null || hasTarget) {
            new Insertion.Bottom(document.body, "<iframe name='wf2Frame' style='display: none'></iframe>");
            var frame = document.body.lastChild;
            frame.wf2Target = this.target;
            this.target = "wf2Frame";
            //frame.form = this;
            Event.observe(frame, "load", function onload() {
              //var form = frame.form;
              var doc = frame.contentWindow.document;
              if (doc.location == "about:blank") return;
              if (doc.XMLDocument) doc = doc.XMLDocument;
              this.target = frame.wf2Target;
              var json = getInnerText(doc.documentElement).evalJSON();
              if (this.loadDatasource) {
                if (this._jsonHidden) {
                  Element.remove(this._jsonHidden);
                  this._jsonHidden = null;
                }
                if (this._oldAction) {
                  this.action = this._oldAction;
                  this._oldAction = null;
                }
                this._receivedHandler(null, json);
              }
              runWhen(function (node) {
                var result = node.onreceived? node.onreceived({receivedDocument: doc, json: json}): true;
                if (!node.loadDatasource && result !== false) {
                  var target = node.target;
                  var targetDoc = null, targetNode = null;
                  if (target) {
                    switch (target) {
                      case "_popup":
                        break;
                      case "_top":
                        targetDoc = targetNode = top.document;
                        break;
                      case "_self":
                        targetDoc = targetNode = document;
                        break;
                      case "_parent":
                        targetDoc = targetNode = parent.document;
                        break;
                      case "_blank":
                        break;
                      default:
                        if (window.frames[target])
                          targetDoc = targetNode = window.frames[target].contentWindow.document;
                        else if (ibre.namedNodes[target]) {
                          targetDoc = document;
                          targetNode = ibre.namedNodes[target];
                        }
                    }
                  }
                  else targetDoc = targetNode = document;
                  if (targetDoc) {
                    domRemoveChildNodes(targetNode);
                    targetNode.appendChild(targetDoc.importNode(doc.documentElement, true));
                    ibre.parseHTMLChilds(targetNode);
                  }
                }
                domRemoveNode(frame);
              }, "!_numParseHTMLChildNodes && document._context.parsed && !_numWF2HTMLChildNodes", [this]);
            }.bind(this));
          }
          var noSubmitElements = this.noSubmitElements;
          if (noSubmitElements) {
            for (var i = 0; i < noSubmitElements.length; i++) {
              var element = noSubmitElements[i];
              noSubmitElements[i] = {
                parent: element.parentNode,
                index: domNodeIndex(element),
                element: element
              };
            }
            noSubmitElements.sort(function (a, b) {
              return a.index - b.index;
            });
            for (var i = 0; i < noSubmitElements.length; i++)
              domRemoveNode(noSubmitElements[i].element);
          }
          this.submit();
          if (noSubmitElements) {
            for (var i = 0; i < noSubmitElements.length; i++) {
              var item = noSubmitElements[i];
              domInsertAt(item.element, item.parent, item.index);
            }
            this.noSubmitElements = null;
          }
        }.bind(node);
        node.onsubmit = function () {
          if (!wf2FormSubmit(this) || this.wf2OnSubmit && this.wf2OnSubmit() === false) return false;
          this.whatwgSubmit();
					return false;
        }
        if (node.getAttribute("datasource") != null) {
          ibre.datasource.make(node, "datasource");
          node.loadDatasource = function (uri, data) {
            if (data) {
              this._jsonHidden = jsToDom("input", {type: "hidden", name: "json", value: Object.toJSON(data)});
              this.appendChild(this._jsonHidden);
            }
            if (uri) {
              this._oldAction = this.action;
              this.action = uri;
            }
            this.whatwgSubmit();
          }.bind(node);
        }
        break;
      case "label":
        var element = $(node.htmlFor);
        if (element) {
          if (element.label) {
            var parent = element.label.parentNode;
            parent.removeChild(element.label);
            domMoveChildNodes(element.label, parent);
          }
          element.label = node;
        }
        break;
      case "textarea":
        _setFormElement(node);
        break;
      case "input":
        if (node.whatwgInput) break;
        node.whatwgInput = true;
        if (node.form && node.form.prefilled && ibre.params[node.name] != null)
          node.value = ibre.params[node.name];
        var onvalidate = node.getAttribute("onvalidate");
        if (onvalidate)
          eval("node.onvalidate = function () { with (this) { " + onvalidate + " } }.bind(node)");
        var validate = node.getAttribute("validate");
        if (validate)
          eval("node.validate = function () { with (this) { return " + validate + "; } }");
        var oninput = node.getAttribute("oninput");
        if (oninput) {
          node._oldValue = node.value;
          node._oninputTimeout = function() {
            if (this._oldValue != this.value) {
              this._oldValue = this.value;
              this.oninput();
            }
            this._timeout = setTimeout(this._oninputTimeout, 1000);
          }.bind(node);
          Event.observe(node, "focus", function () {
            this._oninputTimeout();
          }.bind(node));
          Event.observe(node, "blur", function () {
            clearTimeout(this._timeout);
          }.bind(node));
          eval("node.oninput = function () { with (this) { " + oninput + " } }");
        }
        if (!node.whatwgType) node.whatwgType = node.getAttribute("type");
        switch (node.whatwgType) {
          case "add":
          case "remove":
          case "hidden":
          case "move-up":
          case "move-down":
          case "button":
          case "submit":
          case "reset":
          case "image":
            break;
          default:
          _setFormElement(node);
        }
        switch (node.whatwgType) {
          case "add":
            var button = document.createElement("input");
            var label = node.getAttribute("value");
            button.type = "button";
            domReplaceNode(button, node);
            button.value = label? label: langStrings.addLabel;
            button.onclick = node.onclick;
            button.title = node.title;
            button.className = node.className;
            var style = node.getAttribute("style");
            if (style) button.setAttribute("style", style.toString());
            Event.observe(button, "click", inputAddRepetitionBlock.bind(button));
            button.setAttribute("template", node.getAttribute("template"));
            button.name = node.name;
            node = button;
            break;
          case "remove":
            var button = document.createElement("input");
            var label = node.getAttribute("value");
            button.type = "button";
            domReplaceNode(button, node);
            button.value = label? label: langStrings.removeLabel;
            button.onclick = node.onclick;
            button.title = node.title;
            button.className = node.className;
            var style = node.getAttribute("style");
            if (style) button.setAttribute("style", style.toString());
            Event.observe(button, "click", inputRemoveRepetitionBlock.bind(button));
            button.name = node.name;
            node = button;
            break;
          case "checkbox":
          case "radio":
            if (node.onchange) {
              Event.observe(node, "click", node.onchange.bind(node));
              node.onchange = null;
            }
          case "hidden":
          case "move-up":
          case "move-down":
          case "button":
          case "submit":
          case "reset":
          case "range":
          case "password":
          case "file":
            break;
          case "image":
            var rollover = node.getAttribute("rollover");
            if (rollover) {
              node.rollover = rollover;
              Event.observe(node, "mouseover", function () {
                this.originalSrc = this.src;
                this.src = this.rollover;
              }.bind(node));
              Event.observe(node, "mouseout", function () {
                this.src = this.originalSrc;
              }.bind(node));
            }
            break;
          case "date":
            var button = jsToDom("button", {className: node.className});
            button.innerHTML = "<img src='" + ibre.basePath + "images/calendar.png'>";
            domInsertAfter(button, node);
            Calendar.setup({inputField: node, button: button, ifFormat: "%Y-%m-%d"});
            _inputAddList(node);
            break;
          case "datetime":
          case "datetime-local":
            var button = jsToDom("button", {className: node.className});
            button.innerHTML = "<img src='" + ibre.basePath + "images/calendar.png'>";
            domInsertAfter(button, node);
            Calendar.setup({inputField: node, button: button, showsTime: true, ifFormat: "%Y-%m-%dT%H:%MZ", timeFormat: 12});
            _inputAddList(node);
            break;
          case "month":
            var button = jsToDom("button", {className: node.className});
            button.innerHTML = "<img src='" + ibre.basePath + "images/calendar.png'>";
            domInsertAfter(button, node);
            Calendar.setup({inputField: node, button: button, ifFormat: "%Y-%m"});
            _inputAddList(node);
            break;
          case "week":
            var button = jsToDom("button", {className: node.className});
            button.innerHTML = "<img src='" + ibre.basePath + "images/calendar.png'>";
            domInsertAfter(button, node);
            Calendar.setup({inputField: node, button: button, ifFormat: "%Y-W%W"});
            _inputAddList(node);
            break;
          case "time":
            var button = jsToDom("button", {className: node.className});
            button.innerHTML = "<img src='" + ibre.basePath + "images/calendar.png'>";
            domInsertAfter(button, node);
            Calendar.setup({inputField: node, button: button, ifFormat: "%H:%M", showsTime: true, timeFormat: 12});
            _inputAddList(node);
            break;
          case "number":
          //  if (!node.style.textAlign) node.style.textAlign = "right";
          case "email":
          case "url":
          default:
            _inputAddList(node);
        }
        break;
      case "select":
        _setFormElement(node);
        if (node.getAttribute("datasourceName") != null) {
          ibre.datasource.make(node, "datasourceName");
          Event.observe(node, "change", function () {
            this.loadDatasource(this.getAttribute("datasource"), $F(this));
          }.bind(node));
        }
        var list = node.getAttribute("list");
        if (list) {
          var options = Element.children($(list));
          for (var i = 0; i < options.length; i++) {
            var option = options[i];
            domAddOption(node, option.getAttribute("label"), option.getAttribute("value"));
          }
        }
        if (node.form && node.form.prefilled && ibre.params[node.name] != null) {
          for (var i = 0; i < node.options.length; i++) {
            var option = node.options[i];
            if (option.value == ibre.params[node.name]) {
              option.selected = true;
              break;
            }
          }
        }
        break;
      case "img":
        var rollover = node.getAttribute("rollover");
        if (rollover) {
          node.rollover = rollover;
          Event.observe(node, "mouseover", function () {
            this.originalSrc = this.src;
            this.src = this.rollover;
          }.bind(node));
          Event.observe(node, "mouseout", function () {
            this.src = this.originalSrc;
          }.bind(node));
        }
        break;
    }
    if (!ie && domIsFocusable(node)) {
      Event.observe(node, "focus", function () { document.activeElement = this; }.bind(node));
      Event.observe(node, "blur", function () { document.activeElement = null; });
    }
  }
  if (node.getAttribute("autofocus") != null && domIsFocusable(node) && domIsVisible(node))
    domFocus(node);
}
function _inputAddList(node) {
  if (node.getAttribute("list") || node.getAttribute("datalist")) {
    node.autocomplete = false;
    var div = jsToDom("div");
    div.style.backgroundColor = "white";
    div.style.border = "1px solid black";
    div.style.zIndex = "2";
    div.style.overflow = "auto";
    /*div.style.position = "absolute";
    div.style.marginLeft = node.offsetLeft + "px";
    div.style.marginTop = (node.offsetTop + node.offsetHeight) + "px";*/
    domInsertBefore(div, node);
    //domInsertBefore(div, node.parentNode.firstChild);
    //document.firstChild.appendChild(div);
    var tmp = new AutocompleterWhatwg(node, div/*, list*/);
    Event.observe(node, "click", function () {
      if (this.active) {
        this.active = false;
        this.hide();
      }
      else this.activate();
    }.bind(tmp));
    return;
  }
}
function _setFormElement(node) {
  if (!node.form) return;
  if (!node.label) {
    var elementLabels = node.getAttribute("label") || node.form.elementLabels;
    if (elementLabels) {//alert(elementLabels);
      var labelParent = function () { with (this) { return eval(elementLabels); } }.bind(node)();
      if (labelParent && getInnerText(labelParent).strip()) {
        //var refLabel = labelParent.firstChild;
        if (!labelParent._refLabel/*!refLabel || refLabel.tagName != "LABEL"*/) {
          if (!node.id) node.id = uniqueId();
          var label = jsToDom("label", {"for": node.id});
          domMoveChildNodes(labelParent, label);
          labelParent.appendChild(label);
          node.label = labelParent._refLabel = label;
        }
        else node.label = labelParent._refLabel/*refLabel*/;
      }
    }
  }
  if (!node.name) {
    var elementNames = node.form.elementNames;
    if (elementNames)
      node.name = function () {
        with (this) {
          try {
            return eval(elementNames);
          }
          catch (e) {
            setTimeout(function () { throw e; }, 1);
            return null;
          }
        }
      }.bind(node)();
  }
}