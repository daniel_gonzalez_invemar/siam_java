<?php
require_once "../defs.php";
/*function type_is_recursivity($type, $name) {
  foreach ($type["fields"] as $field)
    if (field_is_recursivity($field, $name)) return TRUE;
  return FALSE;
}
function field_is_recursivity($field, $name) {
  if ($field["dependent"]) {
    switch ($field["type"]) {
      case "reference":
        if ($field["referencedType"]["name"] == $name) return TRUE;
        return type_is_recursivity($field["referencedType"], $name);
        break;
      case "list":
      case "set":
        if ($field["elementType"]["name"] == $name) return TRUE;
        return type_is_recursivity($field["elementType"], $name);
        break;
    }
  }
  return FALSE;
}*/
$mdb2->loadModule("Manager");
$mdb2->loadModule("Reverse");
$manager = $mdb2->manager;
//$reverse = $mdb2->reverse;
$all_tables = $tables = array_flip($manager->listTables());
$errors = array();
$extra_tables = array();
?>
<body>
<head><title>Test</title></head>
<body>
<pre>
<?php
echo "\n";
foreach ($_DEFS as $type) {
  $table = $type["table"];
  //echo "$table\n";
  $constraints = $manager->listTableConstraints($table);
  $indexes = $manager->listTableIndexes($table);
  if ($type["mainField"] && is_string($type["mainField"])) {
    $errors[] = "Missing main field in type '$type[name]'";
  }
  if (!isset($tables[$table])) {
    $errors[] = "Missing table '$table'";
    continue;
  }
  unset($tables[$table]);
  $columns = array_flip($manager->listTableFields($table));
  /*if (isset($columns["id"])) {
    if ($constraints[0] == "primary")
  }*/
  if (isset($columns["id"])) unset($columns["id"]);
  else $errors[] = "Missing column 'id' in table '$table'";
  foreach ($type["references"] as $reference) {
    $index = $reference["index"];
    if ($reference["elementMappedField"] && $index) {
      if (isset($columns[$index])) {
        if (!in_array($index, array_by_key($type["fields"], "column"))) unset($columns[$index]);
      }
      else $errors[] = "Missing column '$index' in table '$table'";
    }
  }
  if ($type["extends"]) {
    $reference_table = $type["extends"]["table"];
    if (isset($all_tables[$reference_table])) {
      $records = sql_all("SELECT `_a`.`id` FROM `$table` as `_a` LEFT JOIN `$reference_table` as `_b` ON `_a`.`id` = `_b`.`id` WHERE `_a`.`id` IS NOT NULL AND `_b`.`id` IS NULL");
      if (PEAR::isError($records)) $errors[] = $records;
      else foreach ($records as $record)
        $errors[] = "Missing record in table '$reference_table'. Table: '$table', Record ID: '$record[id]'";
    }
  }
  foreach ($type["fields"] as $field) {
    if ($field["dinamic"] || $field["referenceMappedField"]) continue;
    $field_type = $field["type"];
    //if (field_is_recursivity($field, $type["name"])) $errors[] = "Recursive dependency in type '$type[name]' with field '$field[name]'";
    if ($field_type == "list" || $field_type == "set") {
      $field_table = $field["table"];
      if ($field_table) {
        $extra_table = $field_table["name"];
        if (!isset($tables[$extra_table])) {
          $errors[] = "Missing table '$extra_table'";
          continue;
        }
        $extra_tables[] = $extra_table;
        $extra_columns = array_flip($manager->listTableFields($extra_table));
        $join = $field_table["join"];
        if (isset($extra_columns[$join]))
          unset($extra_columns[$join]);
        else
          $errors[] = "Missing column '$join' in table '$extra_table'";
        $element = $field_table["element"];
        if (isset($extra_columns[$element]))
          unset($extra_columns[$element]);
        else
          $errors[] = "Missing column '$element' in table '$extra_table'";
        $index = $field["index"];
        if ($index) {
          if (isset($extra_columns[$index]))
            unset($extra_columns[$index]);
          else
            $errors[] = "Missing column '$index' in table '$extra_table'";
        }
        foreach ($extra_columns as $extra_column => $tmp)
          $errors[] = "Nonused column '$extra_column' in table '$extra_table'";
      }
      continue;
    }
    $column = $field["column"];
    if (!isset($columns[$column])) {
      $errors[] = "Missing column '$column' in table '$table'";
      continue;
    }
    unset($columns[$column]);
    if ($field_type == "reference") {
      $reference_table = $field["referencedType"]["table"];
      if (!isset($all_tables[$reference_table])) continue;
      $records = sql_all("SELECT `_a`.`id`, `_a`.`$column` FROM `$table` as `_a` LEFT JOIN `$reference_table` as `_b` ON `_a`.`$column` = `_b`.`id` WHERE `_a`.`$column` IS NOT NULL AND `_b`.`id` IS NULL");
      if (PEAR::isError($records)) $errors[] = $records;
      else foreach ($records as $record)
        $errors[] = "Invalid reference value '$record[$column]'. Table: '$table', Column: '$column', Record ID: '$record[id]'";
    }
    /*echo "$table => $column\n";
    print_r($reverse->getTableFieldDefinition($table, $column));*/
  }
  foreach ($columns as $column => $tmp) $errors[] = "Nonused column '$column' in table '$table'";
}
foreach ($extra_tables as $table) unset($tables[$table]);
if (!$_1IBRE["skip_nonused_table"])
  foreach ($tables as $table => $tmp)
    $errors[] = "Nonused table '$table'";
print_r($errors);
echo "</pre>\n";
if ($errors) echo "<a href='fix.php'>Corregir</a>";
?>
</body>
</html>