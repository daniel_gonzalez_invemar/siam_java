<?php
require_once "../defs.php";
$type = $_DEFS[$_GET["type"]];
$fields = array();
foreach ($type["allFields"] as $field)
  $fields[] = array_merge($field, array(
    "dinamic" => (boolean)$field["dinamic"],
    "referencedType" => $field["referencedType"]["name"],
    "mainField" => $field["referencedType"]["mainField"]["name"],
    "elementType" => $field["elementType"]["name"],
    "keyType" => $field["keyType"]["name"],
    "valueType" => $field["valueType"]["name"],
    "referenceMappedField" => $field["referenceMappedField"]["name"],
    "elementMappedField" => $field["elementMappedField"]["name"],
    "valueMappedField" => $field["valueMappedField"]["name"],
    "keyMappedField" => $field["keyMappedField"]["name"],
    "ownerType" => NULL
  ));
$references = array();
foreach ($fields as $field)
  if ($field["type"] == "reference" || $field["dinamic"] && $field["type"] != "set" && $field["type"] != "list")
    $references[] = $field["name"];
$mapped_field = NULL;
if ($_GET["id"]) {
  $object_type = $_DEFS[$_GET["objectType"]];
  $mapped_field = $object_type["allFields"][$_GET["field"]]["elementMappedField"]["name"];
  $object = ibre_get_object_by_id($object_type, $_GET["id"]);
  sql_limit(51, $_GET["begin"]);
  $objects = ibre_get_field($object, $_GET["field"], $references);
}
else {
  sql_limit(51, $_GET["begin"]);
  $objects = ibre_get_objects($type, "1", NULL, $references/*, TRUE*/);
}
foreach ($fields as $field)
  if ($field["type"] == "reference")
    foreach ($objects as $i => $object)
      if ($object[$field["name"]])
        $objects[$i][$field["name"]]["mainField"] = ibre_get_main_field_value($objects[$i][$field["name"]]);
json_send(array(
  "mainField" => $type["mainField"]["name"],
  "mappedField" => $mapped_field,
  "objects" => $objects,
  "fields" => $fields
));
?>