<?php
function test($res) {
  echo "<br>\n";
  if (PEAR::isError($res)) {
    echo "Message: ".$res->getMessage()."<br>User Info: ".$res->getUserInfo()."<br>";
    exit;
  }
  echo "OK<br>\n";
}
require_once "../defs.php";
$manager = $mdb2->loadModule("Manager");
$tables = array_flip($manager->listTables());
$extra_tables = array();
foreach ($_DEFS as $type) {
  $table = $type["table"];
  $table_exists = isset($tables[$table]);
  if (!$table_exists) {
    echo "Creating table '$table'";
    test($manager->createTable($table, array("id" => array("type" => "integer", "length" => 20)), array("primary" => array("id" => array()))));
  }
  else unset($tables[$table]);
  $columns = array_flip($manager->listTableFields($table));
  if (!isset($columns["id"])) {
    echo "Creating id for table '$table'";
    test($manager->alterTable($table, array("add" => array("id" => array("type" => "integer", "length" => 20))), FALSE));
    test($manager->createConstraint($table, "primary", array("fields" => array("id" => array()))));
  }
  else unset($columns["id"]);
  foreach ($type["references"] as $reference) {
    $index = $reference["index"];
    if ($reference["elementMappedField"] && $index) {
      if (isset($columns[$index])) {
        if (!in_array($index, array_by_key($type["fields"], "column")))
          unset($columns[$index]);
      }
      else {
        echo "Creating column '$index' for table '$table'";
        test($manager->alterTable($table, array("add" => array($index => array("type" => "integer", "notnull" => TRUE))), FALSE));
        if (in_array($index, array_by_key($type["fields"], "column")))
          $columns[$index] = TRUE;
      }
    }
  }
  foreach ($type["fields"] as $field) {
    if ($field["dinamic"] || $field["referenceMappedField"]) continue;
    $field_type = $field["type"];
    if ($field_type == "list" || $field_type == "set") {
      $field_table = $field["table"];
      if ($field_table) {
        $extra_table = $field_table["name"];
        $element = $field_table["element"];
        $join = $field_table["join"];
        if (!isset($tables[$extra_table])) {
          echo "Creating table '$extra_table'";
          test($manager->createTable($extra_table, array($element => array("type" => "integer", "length" => 20, "notnull" => TRUE), $join => array("type" => "integer", "length" => 20, "notnull" => TRUE)), array("primary" => array($element => array(), $join => array()))));
          $tables[$extra_table] = TRUE;
          continue;
        }
        $extra_tables[] = $extra_table;
        $extra_columns = array_flip($manager->listTableFields($extra_table));
        if (!isset($extra_columns[$join])) {
          echo "Creating column '$join' for table '$extra_table'";
          test($manager->alterTable($extra_table, array("add" => array($join => array("type" => "integer", "length" => 20, "notnull" => TRUE))), FALSE));
        }
        else unset($extra_columns[$join]);
        if (!isset($extra_columns[$element])) {
          echo "Creating column '$element' for table '$extra_table'";
          test($manager->alterTable($extra_table, array("add" => array($element => array("type" => "integer", "length" => 20, "notnull" => TRUE))), FALSE));
        }
        else unset($extra_columns[$element]);
        $index = $field["index"];
        if (isset($index))
          if (!isset($extra_columns[$index])) {
            echo "Creating column '$index' for table '$extra_table'";
            test($manager->alterTable($extra_table, array("add" => array($index => array("type" => "integer", "notnull" => TRUE))), FALSE));
          }
          else unset($extra_columns[$index]);
        foreach ($extra_columns as $extra_column => $tmp) {
          echo "Droping column '$extra_column' for table '$extra_table'";
          test($manager->alterTable($extra_table, array("remove" => array($extra_column => array())), FALSE));
        }
      }
      continue;
    }
    $column = $field["column"];
    if (!isset($columns[$column])) {
      $length = NULL;
      switch ($field_type) {
        case "number":
          $column_type = "integer";
          break;
        case "reference":
          $column_type = "integer";
          $length = 20;
          break;
        case "string":
          $column_type = "text";
          $length = 255;
          break;
        case "datetime":
          $column_type = "timestamp";
          break;
        /*case "enum":
          $column_type = "timestamp";
          break;*/
        default:
          $column_type = $field_type;
      }
      echo "Creating column '$column' for table '$table'";
      test($manager->alterTable($table, array("add" => array($column => array("type" => $column_type, "length" => $length, "notnull" => !$field["null"]))), FALSE));
      if ($field_type == "reference")
        test($manager->createIndex($table, $column, array("fields" => array($column => array()))));
    }
    else unset($columns[$column]);
  }
  foreach ($columns as $column => $tmp) {
    echo "Droping column '$column' for table '$table'";
    test($manager->alterTable($table, array("remove" => array($column => array())), FALSE));
  }
}
if (!$_1IBRE["skip_nonused_table"]) {
  foreach ($extra_tables as $table) unset($tables[$table]);
  foreach ($tables as $table => $tmp) {
    echo "Droping table '$table'";
    test($manager->dropTable($table));
  }
}
?>