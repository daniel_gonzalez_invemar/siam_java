<?php
require_once "../defs.php";
require_once "1ibre/common-xml.php";
function get_fields() {
  $type = $GLOBALS["_DEFS"][$_GET["type"]];
  $fields = array();
  foreach ($type["allFields"] as $field)
    $fields[] = array(
      "name" => $field["name"],
      "displayName" => $field["displayName"],
      "type" => $field["type"],
      "dinamic" => (boolean)$field["dinamic"],
      "referencedType" => $field["referencedType"]["name"],
      "mainField" => $field["referencedType"]["mainField"]["name"],
      "elementType" => $field["elementType"]["name"],
      "keyType" => $field["keyType"]["name"],
      "valueType" => $field["valueType"]["name"],
      "elementMappedField" => $field["elementMappedField"]["name"],
      "valueMappedField" => $field["valueMappedField"]["name"],
      "keyMappedField" => $field["keyMappedField"]["name"]
    );
  return $fields;
}
function check_error($err) {
  if (PEAR::isError($err)) {
    //print_r($err);
    $error = array(
      "message" => $err->getMessage(),
      "code" => $err->getCode(),
      "userInfo" => $err->getUserInfo(),
      "debugInfo" => $err->getDebugInfo()
    );
    item_to_xml("error", $error);
    return FALSE;
  }
  return TRUE;
}
?>