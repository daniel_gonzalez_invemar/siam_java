<?php
require_once "MDB2.php";
function sql_connect($dsn, $options = NULL) {
 $GLOBALS["mdb2"] = &MDB2::factory($dsn, $options);
 global $mdb2, $debug;
 if ($debug && PEAR::isError($mdb2)) print_r($mdb2);
 $mdb2->loadModule('Extended');
 $mdb2->setFetchMode(MDB2_FETCHMODE_ASSOC);
}
function sql_close() {
 global $mdb2;
 $mdb2->disconnect();
}
function sql_query($sql, $params = NULL) {
 global $mdb2, $debug;
 $sth = $mdb2->prepare($sql);
 if (PEAR::isError($sth)) {
   if ($debug) print_r($sth);
   return $sth;
 }
 $res = $sth->execute((array)$params);
 if ($debug && PEAR::isError($res)) print_r($res);
 return $res;
}
function sql_one($sql, $params = NULL) {
 global $mdb2, $debug;
 $res = $mdb2->extended->getOne($sql, NULL, (array)$params);
 if ($debug && PEAR::isError($res)) print_r($res);
 return $res;
}
function sql_row($sql, $params = NULL, $types = NULL) {
 global $mdb2, $debug;
 $res = $mdb2->extended->getRow($sql, (array)$types, (array)$params);
 if ($debug && PEAR::isError($res)) print_r($res);
 return $res;
}
function sql_all($sql, $params = NULL) {
 global $mdb2, $debug;
 $res = $mdb2->extended->getAll($sql, NULL, (array)$params);
 if ($debug && PEAR::isError($res)) print_r($res);
 return $res;
}
function sql_assoc($sql, $params = NULL) {
 global $mdb2, $debug;
 $res = $mdb2->extended->getAssoc($sql, NULL, (array)$params);
 if ($debug && PEAR::isError($res)) print_r($res);
 return $res;
}
function sql_group($sql, $params = NULL) {
 global $mdb2, $debug;
 $res = $mdb2->extended->getAssoc($sql, NULL, (array)$params, NULL, NULL, false, true);
 if ($debug && PEAR::isError($res)) print_r($res);
 return $res;
}
function sql_col($sql, $params = NULL) {
 global $mdb2, $debug;
 $res = $mdb2->extended->getCol($sql, 0, (array)$params);
 if ($debug && PEAR::isError($res)) print_r($res);
 return $res;
}
function sql_empty($sql, $params = NULLl) {
  return sql_row($sql, $params) == NULL;
}
function sql_fetch($res) {
 return $res->fetchRow();
}
/*function sql_num_rows() {
 global $mdb2;
 return $mdb2->affectedRows();
}*/
function sql_escape($str) {
 global $mdb2;
 return $mdb2->escape($str);
}
function sql_id() {
 global $mdb2;
 return $mdb2->lastInsertID();
}
function sql_limit($limit, $offset = NULL) {
 global $mdb2;
 return $mdb2->setLimit($limit, $offset);
}
?>
