function parseXMLToJS(node) {
  var obj = {};
  var i;
  var childs = node.childNodes;
  var length = childs.length;
  for (i = 0; i < length; i++) {
    var child = childs[i];
    if (child.nodeType != 1) continue;
    var name = child.nodeName;
    var res = parseXMLToJS(child);
    var names = name + "s";
    if (!obj[name]) obj[names] = [];
    obj[names].push(res);
    obj[name] = res;
  }
  var attrs = node.attributes;
  length = attrs.length;
  for (i = 0; i < length; i++) {
    var attr = attrs[i];
    obj[attr.nodeName] = /^(\d+(\.\d*)?|\.\d+)([eE][+-]\d+)?$/.test(attr.nodeValue)? parseFloat(attr.nodeValue): attr.nodeValue;
  }
  //obj.elementNode = node;
  return obj;
}
function setInnerHTML(node, text) {
  node.innerHTML = text;
  ibre.parseHTMLChilds(node);
}
ibre.parseHTMLChilds = function (node) {
  ibre.datasource.init(document.documentElement);
  runWhen(function (node) {
    ibreParseHTML(node, true);
    runWhen(function (node) {
      wf2ParseHTMLChilds(node, false, true);
    }, "!_numParseHTMLChildNodes && document._context.parsed", [node]);
  }, "!_numParseHTMLChildNodes", [node]);
}
var timeout = 30;
function requestHttp(uri, handler, statusHandler, cache, errorHandler) {
  if (cache && requestHttp.caches[uri]) {
    if (handler) handler(requestHttp.caches[uri]);
    return requestHttp.caches[uri];
  }
  //var ajax = new Ajax.Request(uri, {method: "get", asynchronous: !!handler});
  //var req = new XMLHttpRequest();
  var req = Ajax.getTransport();
  req.open("GET", uri, !!handler);
  var timer = setTimeout(function () {
		req.abort();
		if (errorHandler) errorHandler(0, uri + "\nConnection timeout", req);
		else alert(uri + "\nConnection timeout");
  }, 1000 * timeout);
  if (handler) req.onreadystatechange = function () {
    if (statusHandler) statusHandler(req);
    if (req.readyState == 4) {
			clearTimeout(timer);
      var isOK;
      try { isOK = req.status == 200; } catch (e) { return; }
      if (isOK) {
        if (handler) try {
          handler(req);
        } catch (e) {
          //alert(e.stack);
          throw e;
        }
      }
      else if (errorHandler) errorHandler(req.status, req.statusText, req);
      else alert("There was a problem retrieving the data for uri '" + uri + "':\n" + req.statusText);
    }
  };
  else if (statusHandler) statusHandler(req);
  req.send(null);
	if (!handler) {
    clearTimeout(timer);
    if (statusHandler) statusHandler(req);
  }
  if (cache) requestHttp.caches[uri] = req;
  return req;
}
function requestXML(uri, handler, statusHandler, cache, errorHandler) {
  if (cache && requestXML.caches[uri]) {
    if (handler) handler(requestXML.caches[uri]);
    return requestXML.caches[uri];
  }
  if (handler) return requestHttp(uri, function (req) {
    if (cache) requestXML.caches[uri] = req.responseXML;
    if (!req.responseXML) {
      if (errorHandler) errorHandler(0, "bad xml", req);
      else alert("There was a problem retrieving the data for uri '" + uri + "':\n" + req.responseText);
    }
    else handler(req.responseXML);
  }, statusHandler, false, errorHandler);
  var xml = requestHttp(uri, null, statusHandler, false, errorHandler).responseXML;
	if (!xml) {
    if (errorHandler) errorHandler(0, "bad xml", req);
    else alert("There was a problem retrieving the data for uri '" + uri + "':\n" + req.responseText);
  }
  if (cache) requestXML.caches[uri] = xml;
  return xml
}
function requestText(uri, handler, statusHandler, cache, errorHandler) {
  if (cache && requestText.caches[uri]) {
    if (handler) handler(requestText.caches[uri]);
    return requestText.caches[uri];
  }
  if (handler) return requestHttp(uri, function (req) {
    if (cache) requestText.caches[uri] = req.responseText;
    handler(req.responseText);
  }, statusHandler, false, errorHandler);
  var text = requestHttp(uri, null, statusHandler, false, errorHandler).responseText;
  if (cache) requestText.caches[uri] = text;
  return text;
}
function requestJS(uri, handler, statusHandler, cache, errorHandler) {
  if (cache && requestJS.caches[uri]) {
    if (handler) handler(requestJS.caches[uri]);
    return requestJS.caches[uri];
  }
  if (handler) return requestXML(uri, function (doc) {
    if (cache) requestJS.caches[uri] = parseXMLToJS(doc.documentElement);
    handler(parseXMLToJS(doc.documentElement));
  }, statusHandler, false, errorHandler);
  var obj = parseXMLToJS(requestXML(uri, null, statusHandler, false, errorHandler).documentElement);
  if (cache) requestJS.caches[uri] = obj;
  return obj;
}
/*function requestHtml(uri, node) {
  requestText(uri, function (text) {
    if (document.createRange) {
      var r = document.createRange();
      r.selectNodeContents(node);
      r.deleteContents();
      var df = r.createContextualFragment(text);
      alert(df.childNodes[0].innerHTML);
      node.appendChild(df);
    }
    else node.innerHTML = text;
  });
}*/
function requestInnerHTML(uri, node, handler, statusHandler, cache, errorHandler) {
	if (node.uriCacheInnerHTML != null) {
		var div = jsToDom("div");
		domMoveChildNodes(node, div);
		requestInnerHTML.caches[node.uriCacheInnerHTML] = div;
		node.uriCacheInnerHTML = null;
	}
	if (requestInnerHTML.caches[uri]) {
		if (requestInnerHTML.caches[uri] != node) {
			node.innerHTML = "";
			domMoveChildNodes(requestInnerHTML.caches[uri], node);
			requestInnerHTML.caches[uri] = node;
			node.uriCacheInnerHTML = uri;
		}
		if (handler) handler();
    return;
	}
	if (handler) {
		return requestText(uri, function (text) {
      setInnerHTML(node, text);
			if (!requestInnerHTML.caches[uri]) {
				var scripts = node.getElementsByTagName("script");
				for (var i = 0; i < scripts.length; i++) {
					var script = scripts[i];
					if (script.type != "text/javascript") continue;
					eval(script.src? requestText(script.src, null, null, false, function () {
						alert("no se pudo cargar " + script.src);
					}): script.innerHTML);
				}
			}
			if (cache) {
				requestInnerHTML.caches[uri] = node;
				node.uriCacheInnerHTML = uri;
				//domMoveChilds(node, requestInnerHTML.caches[uri]);
			}
			if (handler instanceof Function) handler();
		}, statusHandler, false, errorHandler);
	}
	var text = requestText(uri, null, statusHandler, false, errorHandler);
	setInnerHTML(node, text);
	if (!requestInnerHTML.caches[uri]) {
		var scripts = node.getElementsByTagName("script");
		for (var i = 0; i < scripts.length; i++) {
			var script = scripts[i];
			if (script.type != "text/javascript") continue;
			eval(script.src? requestText(script.src, null, null, false, function () {
				alert("no se pudo cargar " + script.src);
			}): script.innerHTML);
		}
	}
	if (cache) {
		requestInnerHTML.caches[uri] = node;
		node.uriCacheInnerHTML = uri;
		//domMoveChilds(node, requestInnerHTML.caches[uri]);
	}
}
/*function requestAdjacentHTML(uri, node, where, handler) {
  if (handler) return requestText(uri, function (text) {
    node.insertAdjacentHTML(where, text);
    if (typeof handler == "function") handler();
  });
  node.insertAdjacentHTML(where, requestText(uri));
}*/
function requestPopup(uri, handler, statusHandler, cache, errorHandler) {
  if (cache && requestPopup.caches[uri]) {
    if (handler) handler(requestPopup.caches[uri]);
    return requestPopup.caches[uri];
  }
  var body = document.body;
  new Insertion.Bottom(body, "<div style='position: absolute; top: 0px; margin: 0px; padding: 0px; border-width: 0px'><table style='position: relative; margin: 0px; padding: 0px' border='0'><tr><td align='center'><table><tr><td></td></tr></table></td></tr></table></div>");
  var popup = body.lastChild;
  var firstTable = popup.firstChild;
  var table = Element.firstElement(firstTable.rows[0].cells[0]);
  popup.draggableTable = table;
  var node = table.rows[0].cells[0];
  popup.container = node;
  node.popup = popup;
  popup.selects = [];
  popup.showPopup = function () {
    popup.selects = [];
    /*var div = document.createElement("div");
    domMoveChildNodes(document.body, div);
    document.body.appendChild(div);
    div.style.overflow = "hidden";
    div.style.width = "100%";
    div.style.height = "100%";*/
    if (ie) {
      var selects = document.getElementsByTagName("select");
      for (var i = 0; i < selects.length; i++)
        if (selects[i].style.visibility != "hidden") {
          selects[i].style.visibility = "hidden";
          popup.selects.push(selects[i]);
        }
    }
    body.appendChild(this);
    var offset = Position.scrollOffset();
    firstTable.style.left = offset[0] + "px";
    firstTable.style.top = offset[1] + "px";
    var size = Position.clientSize();
    firstTable.style.width = size[0] + "px";
    firstTable.style.height = size[1] + "px";
    /*body.firstChild.onclick = function () {
      return false;
    };*/
    Position.clone(body, this);
    /*this.style.width = body.firstChild.offsetWidth + "px";
    this.style.height = body.firstChild.offsetHeight + "px";*/
    domSetCheckables(popup.whatwgInputs);
    //this.style.display = "";
    wf2ParseHTMLChilds(node, true);
  };
  popup.hidePopup = function () {
		//alertObj(popup);
    this.whatwgInputs = domGetCheckables(this);
    /*var div = this.previousSibling;
    domRemoveNode(div);
    domMoveChildNodes(div, document.body);*/
    domRemoveNode(this);
    for (var i = 0; i < this.selects.length; i++) this.selects[i].style.visibility = "";
    /*this.style.display = "none";*/
  };
  popup.isVisible = function () {
    return this.parentNode != null;
  };
  if (handler) return requestInnerHTML(uri, node, function () {
    if (cache) requestPopup.caches[uri] = popup;
    var table = popup.draggableTable;
    new Draggable(table, {handle: "draggable"});
    var draggables = Element.getElementsByClassName(table, "draggable");
    (draggables.length? draggables[0]: table).style.cursor = "move";
    popup.hidePopup();
    handler(popup);
  }, statusHandler, false, errorHandler);
  if (cache) requestPopup.caches[uri] = popup;
  requestInnerHTML(uri, node);
  new Draggable(table, {handle: "draggable"});
  var draggables = Element.getElementsByClassName(table, "draggable");
  (draggables.length? draggables[0]: table).style.cursor = "move";
  popup.hidePopup();
  return popup;
}
function clearRequestCache() {
	requestHttp.caches = {};
	requestXML.caches = {};
	requestText.caches = {};
	requestJS.caches = {};
	requestInnerHTML.caches = {};
	requestPopup.caches = {};
}
clearRequestCache();
