function ibreInit() {
  var div = document.createElement("div");
  domMoveChildNodes(document.body, div);
  document.body.appendChild(div);
  div.style.overflow = "auto";
  //div.style.border = "1px red solid";
  div.style.width = "100%";
  div.style.height = "100%";
  //Element.hide(div);
  var onprogress = document.body.getAttribute("onprogress");
  if (onprogress && !(onprogress instanceof Function)) eval("document.body.onprogress = function (event) { with (this) {" + onprogress + "} }.bind(document.body)");
  var onload = document.body.getAttribute("ibre-onload");
  if (onload) {
    runWhen(function () {
      eval("(function () { with (this) {" + onload + "} }.bind(document.body))()");
    }, "document._context.parsed && !_numWF2HTMLChildNodes");
  }
  var style = jsToDom("style", {id: "_1ibreCSS", type: "text/css"});
  document.head.appendChild(style);
  var sheet = domSheet(style);
  //domInsertRule(sheet, ".sorted:after", "content: '\\002193'");
  //domInsertRule(sheet, ".reversed:after", "content: '\\002191'");
  domInsertRule(sheet, "table", "font: inherit; border-color: black");
  //domInsertRule(sheet, "body", "width: 100%; height: 100%; margin: 0; padding: 0; overflow: hidden");
  domInsertRule(sheet, "body", "margin: 0; padding: 0");
  /*style = jsToDom("style", {type: "text/css", rel: "stylesheet", href: ibre.basePath + "jscalendar/calendar-system.css"});
  document.head.appendChild(style);*/
}
//Event.observe(window, "load", ibreInit);
