<?php
require_once "1ibre/common.php";
header("Content-Type: text/xml; charset=ISO-8859-1");
function xmlspecialchars($str) {
  $str = htmlspecialchars($str);
//  $str = str_replace(chr(225), "&#225;", $str);
 // $str = str_replace(chr(237), "&#237;", $str);
  return $str;
}
function item_to_xml($tag, $item) {
  if (!$item) return;
  if (is_array($item) && is_numeric(key($item))) {
    $len = strlen($tag) - 1;
    if ($tag[$len] == "s") $tag = substr($tag, 0, $len);
    list_to_xml($tag, $item);
  }
  else {
    echo "<$tag";
    $childs = array();
    foreach ($item as $name => $value) {
      //if ($name{0} == "_") continue;
      if ($value === NULL || is_scalar($value)) echo " $name=\"".xmlspecialchars($value)."\"";
      else $childs[$name] = $value;
    }
    if ($childs) {
      echo ">\n";
      foreach ($childs as $name => $value) item_to_xml($name, $value);
      echo "</$tag>\n";
    }
    else echo "/>\n";
  }
}
function list_to_xml($tag, $list) {
  foreach ($list as $item) item_to_xml($tag, $item);
}
/*function user_to_xml($user) {
  $_SESSION["user_id"] = $user["id"];
  set_user($user);
  item_to_xml("data", $GLOBALS["user"]);
}*/
echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
?>
