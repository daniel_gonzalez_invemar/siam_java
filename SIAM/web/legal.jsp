<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head><title>Compromiso de Uson SIAM Sistema de Información Marino de Colombia</title>
<link href="plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css"/>
 <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
 <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19778914-22']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<jsp:include page="plantillaSitio/headerv2.jsp?idsitio=10"/>
          <div class="centrado">
          <table width="963px" style="border:0px;" >

            <tr>

               <td><table style="width:100%;">
                 <tr>
                   <td><table style="width:100%;text-align:center" class="colorfondo2"  >
                     <tr>
                       <td class="titulo_secciones" style="text-align:left"><h1>COMPROMISO DE USO</h1></td>
                     </tr>
                     <tr>
                       <td class="titulo_sec_smb"></td>
                     </tr>
                     <tr>
                       <td style="height:10px;padding-right:5px;padding-left:5px" ><p style='text-align:justify;'> El usuario se compromete a cumplir las siguientes
                         condiciones de uso de la informaci&oacute;n proporcionada: </p>
                         <ul>
                           <li style='text-align:justify;'>Los datos no deben ser traspasados de ninguna manera a
                             terceros.</li>
                           <li style='text-align:justify;'>No podr&aacute;n vender la informaci&oacute;n que
                             les sea suministrada.</li>
                           <li style='text-align:justify;'> En los productos de
                             informaci&oacute;n que de el se generen, sean impresos o
                             electr&oacute;nicos, se debe reconocer la coautoria al
                             Instituto de Investigaciones Marinas y Costeras &ldquo;Jos&eacute;
                             Benito Vives de Andr&eacute;is&rdquo; - INVEMAR.</li>
                           <li style='text-align:justify;'> Se debe
                             citar en d&oacute;nde quiera que se usen los datos o
                             informaci&oacute;n y en los productos de informaci&oacute;n
                             que de el se generen sean impresos o electr&oacute;nicos de
                             la siguiente manera: &quot;INVEMAR. NOMBRE_SISTEMAXXX [en l&iacute;nea]:
                             NOMBRE_SISTEMAXXX.
                             [Santa Marta]: Instituto de investigaciones Marinas y
                             Costeras &ldquo;Jos&eacute; Benito Vives de
                             Andr&eacute;is&rdquo;, [Abr. FECHA_CONSULTA].
                             &lt;URL_PAGINA; [Consulta:
                             FECHA_HORA ]&quot; </li>
                           <li style='text-align:justify;'> Cu&aacute;ndo se trate de una
                             publicaci&oacute;n impresa o video se debe enviar al
                             Instituto de Investigaciones Marinas y Costeras una copia
                             del trabajo final. </li>
                         </ul>
                        <p style="text-align:justify"> No se garantiza la precisi&oacute;n
                         global de los datos. El solicitante es advertido de posibles
                         errores en los datos suministrados, y asume la
                         responsabilidad de realizar los chequeos necesarios para
                         detectarlos, corregirlos e interpretarlos de manera acorde.
                         El solicitante hace uso de los datos bajo su propia
                         responsabilidad. El solicitante acepta las limitaciones
                         existentes en los datos, provengan de su naturaleza o que
                         sean impuestas como resultado de este acuerdo.
                         </p>
                         <p class="textnormal">&nbsp;</p></td>
                     </tr>
                     <tr>
                       <td><table style="width:100%;">
                         <tr>
                           <td style="width:11px;"></td>
                           <td></td>
                           <td style="width:11px;"></td>
                         </tr>
                       </table></td>
                     </tr>
                   </table></td>
                 </tr>
               </table></td>

            </tr>         
         
</table>
</div>
 <%@ include file="plantillaSitio/footer.jsp" %>
          </body>
          </html>